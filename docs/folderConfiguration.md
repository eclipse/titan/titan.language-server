# Folder configurations

## Introduction

When opening a project in **vscode** or analyzing a project by executing the language server's compiler in command line mode, it is possible to set project specific settings without modifying global settings.

This specific configuration can even be added to the git repository of the project as a shared resource, eliminating the need of individually adjusting the settings for the given project.

## Adding project specific settings

Create a file in the project root named ```.TITAN_config.json```. This file will be automatically parsed when the project is opened in **vscode** or analyzed using the command line.

The file is a standard **json** file where the required settings can be set. If this file is edited in **vscode** with the Titan extension installed, autocompletion is supported.

## Available JSON objects

The following main object are supported:

* ```definedSymbols``` list of symbols to be *defined*. If a symbol is listed here, **#ifdef SYMBOLNAME** will evaluate to _true_ in *ttcnpp* files.
* ```undefinedSymbols``` list of symbols to be *undefined*. If a symbol is listed here, **#ifdef SYMBOLNAME** will evaluate to _false_  in *ttcnpp* files.
* ```compilerFlags``` settings for the Titan core compiler. These flags will be used if the external Titan core compiler is executed from **vscode** as a _build task_.
* ```namingConventions``` override the default patterns for naming convention checks. patters are interpreted as _regular expressions_
* ```codeSmells``` set severity or disable individual _code smells_

For full list of options open the folder config file in **vscode** with the Titan extension installed and use autocompletion. This will show all the available settings with their descriptions.

## Example

    {
     "definedSymbols": [
          "MYSYMBOL"
     ],
     "undefinedSymbols": [
          "undefsym",
          "sym2"
     ],
     "compilerFlags": {
          "disableBER": false,
          "forceXER": true 
     },
     "namingConventions": {
          "externalFunction": "f_.*",
          "formalParameter": "p_.*",
          "localVariable": "vl_.*",
          "port": "^[^_]+_PT$",
          "testcase": "TC_.*",
          "variable": "vl_.*",
          "componentVariable": "vg_.*",
          "localTimer": "^Tl_[A-Z]+$",
          "globalTimer": "Tg_.*",
          "globalConstant": "cg_.*",
          "localConstant": "cl_.*",
          "function": "f_.*" 
     },
     "codeSmells": {
          "identifierNameTooLongOrShort": "error",
          "circularImportation": "ignore"
     }
} 