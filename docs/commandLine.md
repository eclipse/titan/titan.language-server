# Running the language server's compiler in command line mode

## Introduction

The language server used by the **Titan vscode extension** contains a *TTCN3/ASN1* compiler that can be executed in a standalone mode using command line.

In this scenario, the compiler analyzes the given project and prints out all the markers (errors/warnings/infos). By default, this printout is sent to standard output.

Using this standalone compiler has the following advantages:

* can be used in a single terminal, even no **vscode** is required
* easy to automate the execution, for example adding it to a **CI pipeline**
* the output format is customizable and can be easily parsed by scripts

## Installation

### Requirements

Running the compiler requires *Java 11* or any higher version. There are no other requirements, all the library dependencies are included in the zip archive.

As the compiler is written in *Java*, it can be run on all platforms having the *Java* runtime (**Windows**, **Linux** etc).

### Installation procedure

The installation process is rather simple.

Download the *zip* archive containing the compiler and unzip it. The compiler is ready to run, no additional installation is required.

## Running the compiler

The compiler can be executed on **Linux** with the following command. It is assumed that the java runtime is located in the **PATH** of the current shell:

    java -jar /path/to/compiler/org.eclipse.titan.lsp.jar --cmd ARGUMENTS

The following arguments are supported:

* ```--root /path/to/project/root``` Mandatory. Specifies the folder where the *ttcn3* project is located.
* ```--outfile FILE``` Output to FILE instead of standard output.
* ```--sa``` / ```--nosa``` enables/disables titanium, the static code analyzer. *On* by default.
* ```--ttcn``` / ```-nottcn``` enables/disables reporting of ttcn3 or asn1 errors/warnings. *On* by default.
* ```--naming``` / ```--nonaming``` enables/disables reporting of naming convention violations. *Off* by default.
* ```--oop``` / ```--nooop``` enables/disables ttcn3's *object oriented extension*. *Off* by default.
* ```--ttcnFormat FORMAT_STRING``` Specify the formatting of errors/warnings. The following *specifiers* are supported:
     * ```%f``` path of the file the report belongs to
     * ```%l``` / ```%L``` start/end line number where the error/warning occured
     * ```%c``` / ```%C``` start/end column number where the error/warning occured
     * ```%m``` the error/warning message
     * ```%s``` one letter code of the severity of the message

     Any other characters in the format specifier string are printed literally.

If no format string specified, the reports use the following format:

    "%f:::%m:::%l:::%s"

This prints messages like this:

    second.ttcn:::integer value was expected:::5:::E


