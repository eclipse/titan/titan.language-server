/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests;

import static org.junit.Assert.assertFalse;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import org.junit.Test;

import tests.MarkerToCheck.MarkerSeverity;

/**
 * Base class for unit tests
 * 
 * @author Miklos Magyari
 */
public abstract class TestBase {
	public static final String REGEXPREFIX = "{REGEX}"; 
	
	private List<String> cmdCompilerFlags = new ArrayList<>();
	protected Map<String,List<MarkerToCheck>> markers = new HashMap<>();
	private final static String LOGSEPARATOR = ":::";
	private Map<String,List<MarkerToCheck>> markersFoundByLS = new HashMap<>();
	private String lsBaseDir;
	private String testBaseDir;
	protected boolean reportMissing = true;
	List<String> origMarkers = new ArrayList<>();
	
	/**
	 * This can be set to false in child class tests to skip all assert() calls. In this case, all
	 * missing and unexpected markers will be listed on the console.
	 */
	protected boolean assertOnFailure = true;
	
	public enum CompilerFlags {
		EnableNamingConventionChecks
	}
	
	protected void setcompilerFlags(CompilerFlags... flags) {
		for (int i = 0; i < flags.length; i++) {
			switch(flags[i]) {
			case EnableNamingConventionChecks:
				cmdCompilerFlags.add("--naming");
			default:
				break;
			}
		}
	}
	
	/** Returns the list of all expected markers for all files. */ 
	protected Map<String, List<MarkerToCheck>> getMarkers() {
		return markers;
	}
	
	/** Returns the root directory of test files */
	public abstract String getRoot();
	
	/**
	 * Returns extra compiler flags required for this test case
	 * @return
	 */
	public List<String> getCommandLineCompilerFlags() {
		return cmdCompilerFlags;
	}
	
	public void setBaseDir(final String baseDir) {
		this.testBaseDir = baseDir;
	}
	
	public String getBaseDir() {
		return testBaseDir;
	}
	
	protected class InStream implements Runnable {
	    private InputStream inputStream;
	    private Consumer<String> consumer;

	    public InStream(InputStream inputStream, Consumer<String> consumer) {
	        this.inputStream = inputStream;
	        this.consumer = consumer;
	    }

	    @Override
	    public void run() {
	        new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(consumer);
	    }
	}
	
	public TestBase() {
		Path currentRelativePath = Paths.get("");
		lsBaseDir = currentRelativePath.toAbsolutePath().toString();
		
		initMarkers();
		processOrigMarkers();
	}
	
	private void ConsumeInput(String str) {
		String[] line = str.split(LOGSEPARATOR);
		if (line.length != 4) {
			return;
		}
		MarkerSeverity severity;
		switch (line[3]) {
		case "E":
		default:
			severity = MarkerSeverity.Error;
			break;
		case "W":
			severity = MarkerSeverity.Warning;
			break;
		}
		final String file = line[0];
		MarkerToCheck marker = new MarkerToCheck(line[1], Integer.parseInt(line[2]), severity);
		
		/** On Windows, we need to remove double backslashed. On Linux it probably has no effect */
		final String normalizedFile = file.replaceAll("\\\\\\\\", Matcher.quoteReplacement("\\"));
		
		if (!markersFoundByLS.containsKey(normalizedFile)) {
			markersFoundByLS.put(normalizedFile, new ArrayList<>());
		}
		List<MarkerToCheck> existing = markersFoundByLS.get(normalizedFile);
		existing.add(marker);
		markersFoundByLS.put(normalizedFile, existing);
	}

	protected void processOrigMarkers() {
		for (String orig : origMarkers) {
			String[] line = orig.split(":::");
			MarkerSeverity severity = MarkerSeverity.Error;
			switch (line[2]) {
			case "e":
				severity = MarkerSeverity.Error;
				break;
			case "w":
				severity = MarkerSeverity.Warning;
				break;
			}
			if (!markers.containsKey(line[0])) {
				markers.put(line[0], new ArrayList<>());
			}

			List<MarkerToCheck> existing = markers.get(line[0]);
			existing.add(new MarkerToCheck(line[3], Integer.parseInt(line[1]), severity));
			markers.put(line[0], existing);
		}
	}
	
	protected void addSubdirectory(final String subdir) {
		addSubdirectory(subdir, new ArrayList<>());
	}
	
	protected void addSubdirectory(final String subdir, final List<String> filemarkers) {
		for (String filemarker : filemarkers) {
			String separator = File.separator.equals("\\\\") ? "\\" : File.separator;
			final String markertext = subdir + separator + filemarker;
			origMarkers.add(markertext);
		}
	}
	
	protected abstract void initMarkers();
	
	@Test
	public void executeTest() {
		final String testRoot = lsBaseDir
				+ File.separatorChar + "src"
				+ File.separatorChar + "test"
				+ File.separatorChar + "java"
				+ File.separatorChar + "titan-common-tests"
				+ File.separatorChar + getRoot();
		
		setBaseDir(testRoot);
		final Map<String,List<MarkerToCheck>> testMarkers = getMarkers();
		
		List<String> builderList = new ArrayList<>(
			List.of("java", "-jar", lsBaseDir + File.separatorChar + "target\\org.eclipse.titan.lsp.jar",
					"--cmd", "--root", testRoot, "--nosa", "--ttcnFormat",
					"%f" + LOGSEPARATOR + "%m" + LOGSEPARATOR + "%l" + LOGSEPARATOR + "%s"
			)
		);
		builderList.addAll(getCommandLineCompilerFlags());
		ProcessBuilder builder = new ProcessBuilder(builderList);
		
		try {
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InStream stream = new InStream(process.getInputStream(), this::ConsumeInput);
			ExecutorService executor = Executors.newSingleThreadExecutor();
			Future<?> future = executor.submit(stream);
	
			int code =	process.waitFor();
			if (code != 0) {
				System.out.println("Check failed");
				System.exit(-1);
			}
			executor.shutdown();
			future.get();
			
			boolean problemMissingMarkers = false;
			boolean problemUnexpectedMarkers = false;
			boolean problemNoMarkersAtAll = false;
			final StringBuilder sb = new StringBuilder();
			for (Entry<String,List<MarkerToCheck>> testMarker : testMarkers.entrySet()) {
				final List<MarkerToCheck> markersFound = new ArrayList<>();
				final List<MarkerToCheck> unexpectedMarkers = new ArrayList<>();
				final List<MarkerToCheck> missingMarkers = new ArrayList<>();
				List<MarkerToCheck> fileMarkers = markersFoundByLS.get(testMarker.getKey());
				if (fileMarkers == null && testMarker.getValue().size() > 0) {
					sb.append("LS did not find any markers for " + testMarker.getKey()).append("\n");
					problemNoMarkersAtAll = true;
					continue;
				}
				fileMarkers.stream()
					.forEach(f -> {
						/* line numbers are starting from 1 so we need to adjust */
						f.incrementLineNumber();
						if (testMarker.getValue().contains(f)) {
							markersFound.add(f);
						} else if (reportMissing){
							if (f.getMessageSeverity() == MarkerSeverity.Error) {
								/** FIXME
								 * We only collect markers with severity 'error' as unexpected. Is this correct?
								 */
								unexpectedMarkers.add(f);
							}
						}
					});
				
				for (MarkerToCheck marker : testMarker.getValue()) {
					if (markersFound.stream()
						.filter(entry -> { 
							return entry.equals(marker); 
						})
						.collect(Collectors.toList()).isEmpty()) {
							missingMarkers.add(marker);
						}
				}
				final String path = getRoot() + File.separator + testMarker.getKey();
				if (missingMarkers.size() != 0) {
					problemMissingMarkers = true;
				}
				boolean fileReported = false;
				if (!missingMarkers.isEmpty()) {
					fileReported = true;
					
					sb.append("In ").append(testRoot).append(testMarker.getKey()).append("\n")
						.append("  The following markers were not found:\n");
					for (MarkerToCheck missingMarker : missingMarkers) {
						sb.append("    ").append(missingMarker.getLineNumber()).append(": ")
							.append(missingMarker.getMessageToCheck()).append("\n");
					}
				}
				if (unexpectedMarkers.size() != 0) {
					problemUnexpectedMarkers = true;
				}
				
				if (reportMissing && !unexpectedMarkers.isEmpty()) {
					if (!fileReported) {
						sb.append("In ").append(testRoot).append(testMarker.getKey()).append("\n");
					}
					sb.append("  The following markers were unexpected:\n");
					for (MarkerToCheck unexpectedMarker : unexpectedMarkers) {
						sb.append("    ").append(unexpectedMarker.getLineNumber()).append(": ")
							.append(unexpectedMarker.getMessageToCheck()).append("\n");
					}
				}
			}
			assertFalse(sb.toString(), problemMissingMarkers || problemNoMarkersAtAll || problemUnexpectedMarkers);
		} catch (IOException | InterruptedException | ExecutionException e) {
			System.out.println(e);
		} 
	}
}
