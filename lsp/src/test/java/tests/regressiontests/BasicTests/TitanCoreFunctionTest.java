/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests;

import java.util.List;

import tests.TestBase;

public class TitanCoreFunctionTest extends TestBase {
	@Override
	public String getRoot() {
		return "TitanCore\\function_test\\Semantic_Analyzer\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("any_from", any_fromMarkers());
		
		/** @default is not yet implemented; it is part of standard uplift */ 
		//addSubdirectory("defaultAlternative", defaultAlternativeMarkers());
		
		addSubdirectory("defpars", defparsMarkers());
		addSubdirectory("deprecated", deprecatedMarkers());
		addSubdirectory("deterministic", deterministicMarkers());
		addSubdirectory("encode", encodeMarkers());
		addSubdirectory("encode_legacy", encode_legacyMarkers());
		addSubdirectory("erroneous_attributes", erroneous_attributesMarkers());
		addSubdirectory("float", floatMarkers());
		addSubdirectory("standard", standardMarkers());
		addSubdirectory("template", templateMarkers());
	}
	
	private List<String> floatMarkers() {
		return
			List.of(
				"subtype_SE.ttcn:::32:::E:::The subtype restriction is not a subset of the restriction on the parent type. .*",
				"subtype_SE.ttcn:::33:::E:::The subtype restriction is not a subset of the restriction on the parent type. Subtype .*",
				"subtype_SE.ttcn:::35:::E:::upper boundary cannot be not_a_number in float subtype range",
				"subtype_SE.ttcn:::61:::E:::[^ ]* is not a valid value for type `float' which has subtype .*",
				"subtype_SE.ttcn:::62:::E:::[^ ]* is not a valid value for type `float' which has subtype .*",
				"subtype_SE.ttcn:::65:::E:::[^ ]* is not a valid value for type `float' which has subtype .*",
				"subtype_SE.ttcn:::68:::E:::[^ ]* is not a valid value for type `float' which has subtype .*",
				"subtype_SE.ttcn:::79:::E:::[^ ]* is not a valid value for type `float' which has subtype .*",
				"subtype_SE.ttcn:::82:::E:::[^ ]* is not a valid value for type `float' which has subtype .*",
				"subtype_SE.ttcn:::85:::E:::[^ ]* is not a valid value for type `float' which has subtype .*",
				"subtype_SE.ttcn:::86:::E:::[^ ]* is not a valid value for type `float' which has subtype .*"
			);
	}
	
	private List<String> any_fromMarkers() {
		return
			List.of(
				"any_from_SE.ttcn:::59:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::63:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::67:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::71:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::75:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::79:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::83:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::87:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::91:::E:::Reference to a port array was expected instead of a port",
				"any_from_SE.ttcn:::95:::E:::Reference to a port array was expected instead of a port"
			);
	}
	
	private List<String> defaultAlternativeMarkers() {
		return
				List.of(
						"defaultAlternative_SE.ttcn:::21:::E:::Field `ageInYears' of the default alternative is here",
						"defaultAlternative_SE.ttcn:::28:::E:::Duplicate union field name `string'",
						"defaultAlternative_SE.ttcn:::17:::E:::Field `string' of the default alternative is here",
						"defaultAlternative_SE.ttcn:::34:::E:::Multiple union fields defined with the `@default' modifier",
						"defaultAlternative_SE.ttcn:::32:::E:::The `@default' modifierx was already used here"
						
						);
	}
	
	private List<String> defparsMarkers() {
		return
			List.of(
				"HQ30279A.ttcn:::54:::E:::default value cannot refer to a template field of the component in the `runs on' clause",
				"HQ30279A.ttcn:::68:::E:::default value can not be a function invocation"

		);
	}
	
	private List<String> standardMarkers() {
		return
			List.of(
				"ImportRename_SE.ttcn:::15:::E:::Imported module with identifier `Imp1' already exists and refers to a different module",
				"ImportRename_SE.ttcn:::14:::E:::Previous imported module with identifier `Imp1' is here",
				"ImportRename_SE.ttcn:::18:::E:::Imported module with identifier `Empty4_OK' already exists and refers to a different module",
				"ImportRename_SE.ttcn:::17:::E:::Previous imported module with identifier `Empty4_OK' is here",
				"LangSpecImp_SW.ttcn:::14:::W:::The TTCN-3 language specification of the imported module is newer than the language specification of the importing module",
				"LangSpecImp_SW.ttcn:::16:::W:::The TTCN-3 language specification of the imported module is newer than the language specification of the importing module",
				"ReuseEnumName_SW.ttcn:::17:::W:::Definition `enumX' reuses the identifier of an enumerated value declared in this module. This is forbidden by the TTCN-3 standard, but is allowed in TITAN for backward compatibility",
				"ReuseEnumName_SW.ttcn:::14:::W:::Reused enumerated value is here"
			);
	}
	
	private List<String> templateMarkers() {
		return
			List.of(
				"TempDynamic_SE.ttcn:::17:::E:::Reference to `value' is only allowed inside a dynamic template or property setter",
				"TempDynamic_SE.ttcn:::20:::E:::The dynamic template's statement block does not have a return statement",
				"TempDynamic_SE.ttcn:::22:::E:::Missing return value. The dynamic template's statement block should return a boolean value",
				"TempDynamic_SE.ttcn:::46:::E:::Restriction on template definition does not allow usage of dynamic match",
				"TempDynamic_SE.ttcn:::48:::E:::Restriction on template definition does not allow usage of dynamic match",
				"TempImplication_SE.ttcn:::25:::E:::`ifpresent' is not allowed here",
				"TempNoDefaultOrPort_SE.ttcn:::15:::W:::A template should not contain a field with default type at any level.",
				"TempNoDefaultOrPort_SE.ttcn:::19:::W:::A template should not contain a field with default type at any level.",
				"TempOmit_SE.ttcn:::18:::E:::`omit' value is not allowed in this context",
				"TempOmit_SE.ttcn:::19:::E:::`omit' value is not allowed in this context",
				"TempRes_SE.ttcn:::42:::E:::Restriction on return template does not allow usage of value range match",
				"TempRes_SE.ttcn:::38:::E:::Referenced template is here",
				"TempRes_SE.ttcn:::31:::W:::Inadequate restriction on the referenced template parameter `pt_f', this may cause a dynamic test case error at runtime",
				"TempRes_SE.ttcn:::29:::E:::Referenced template parameter is here",
				"TempRes_SE.ttcn:::23:::W:::Inadequate restriction on the referenced template parameter `pt_i', this may cause a dynamic test case error at runtime",
				"TempRes_SE.ttcn:::22:::E:::Referenced template parameter is here",
				"TempRes_SE.ttcn:::50:::E:::Restriction on template formal parameter does not allow usage of any value",
				"TempRes_SE.ttcn:::50:::E:::Restriction on template formal parameter does not allow usage of any value",
				"TempRes_SE.ttcn:::53:::W:::Inadequate restriction on the referenced template variable `vt_i', this may cause a dynamic test case error at runtime",
				"TempRes_SE.ttcn:::48:::E:::Referenced template variable is here",
				"TempRes_SE.ttcn:::23:::W:::Inadequate restriction on the referenced template parameter `pt_i', this may cause a dynamic test case error at runtime",
				"TempRes_SE.ttcn:::22:::E:::Referenced template parameter is here",
				"TempRes_SE.ttcn:::53:::E:::Restriction on template formal parameter does not allow usage of any value",
				"TempRes_SE.ttcn:::53:::E:::Restriction on template formal parameter does not allow usage of any value",
				"TempRes_SE.ttcn:::53:::E:::Restriction on template formal parameter does not allow usage of any value",
				"TempRes_SE.ttcn:::58:::E:::Restriction on template does not allow usage of `ifpresent'",
				"TempRes_SE.ttcn:::39:::E:::Referenced template is here"
		);
	}
	
	private List<String> deprecatedMarkers() {
		return
			List.of(
				"DeprecatedCompat_SE.ttcn:::64:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Rec2' was expected instead of `@DeprecatedCompat_SE.Rec1'",
				"DeprecatedCompat_SE.ttcn:::65:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.RecOf' was expected instead of `@DeprecatedCompat_SE.Rec1'",
				"DeprecatedCompat_SE.ttcn:::66:::E:::Type mismatch: a value of type `integer[2]' was expected instead of `@DeprecatedCompat_SE.RecOf'",
				"DeprecatedCompat_SE.ttcn:::68:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Set2' was expected instead of `@DeprecatedCompat_SE.Set1'",
				"DeprecatedCompat_SE.ttcn:::69:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.SetOf' was expected instead of `@DeprecatedCompat_SE.Set2'",
				"DeprecatedCompat_SE.ttcn:::70:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Rec1' was expected instead of `@DeprecatedCompat_SE.Rec2'",
				"DeprecatedCompat_SE.ttcn:::70:::E:::Type mismatch: a value of type `integer[2]' was expected instead of `@DeprecatedCompat_SE.Rec2'",
				"DeprecatedCompat_SE.ttcn:::71:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Nested2' was expected instead of `@DeprecatedCompat_SE.Nested1'",
				"DeprecatedCompat_SE.ttcn:::72:::E:::Type mismatch: a value of type `integer[2]' was expected instead of `@DeprecatedCompat_SE.Rec2'",
				"DeprecatedCompat_SE.ttcn:::73:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Rec1' was expected instead of `integer[2]'",
				"DeprecatedCompat_SE.ttcn:::74:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Rec2' was expected instead of `@DeprecatedCompat_SE.RecOf'",
				"DeprecatedCompat_SE.ttcn:::75:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Set1' was expected instead of `@DeprecatedCompat_SE.SetOf'",
				"DeprecatedCompat_SE.ttcn:::79:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Rec2' was expected instead of `@DeprecatedCompat_SE.Rec1'",
				"DeprecatedCompat_SE.ttcn:::80:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.RecOf' was expected instead of `@DeprecatedCompat_SE.Rec1'",
				"DeprecatedCompat_SE.ttcn:::81:::E:::Type mismatch: a value or template of type `integer[2]' was expected instead of `@DeprecatedCompat_SE.RecOf'",
				"DeprecatedCompat_SE.ttcn:::83:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Set2' was expected instead of `@DeprecatedCompat_SE.Set1'",
				"DeprecatedCompat_SE.ttcn:::84:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.SetOf' was expected instead of `@DeprecatedCompat_SE.Set2'",
				"DeprecatedCompat_SE.ttcn:::85:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Rec1' was expected instead of `@DeprecatedCompat_SE.Rec2'",
				"DeprecatedCompat_SE.ttcn:::85:::E:::Type mismatch: a value or template of type `integer[2]' was expected instead of `@DeprecatedCompat_SE.Rec2'",
				"DeprecatedCompat_SE.ttcn:::86:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Nested2' was expected instead of `@DeprecatedCompat_SE.Nested1'",
				"DeprecatedCompat_SE.ttcn:::87:::E:::Type mismatch: a value or template of type `integer[2]' was expected instead of `@DeprecatedCompat_SE.Rec2'",
				"DeprecatedCompat_SE.ttcn:::88:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Rec1' was expected instead of `integer[2]'",
				"DeprecatedCompat_SE.ttcn:::89:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Rec2' was expected instead of `@DeprecatedCompat_SE.RecOf'",
				"DeprecatedCompat_SE.ttcn:::90:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Set1' was expected instead of `@DeprecatedCompat_SE.SetOf'",
				"DeprecatedCompat_SE.ttcn:::93:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.RecOf' was expected instead of `@DeprecatedCompat_SE.Rec1'",
				"DeprecatedCompat_SE.ttcn:::94:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Rec2' was expected instead of `integer[2]'",
				"DeprecatedCompat_SE.ttcn:::94:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Rec2' was expected instead of `@DeprecatedCompat_SE.RecOf'",
				"DeprecatedCompat_SE.ttcn:::95:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Set1' was expected instead of `@DeprecatedCompat_SE.Set2'",
				"DeprecatedCompat_SE.ttcn:::95:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Set1' was expected instead of `@DeprecatedCompat_SE.SetOf'",
				"DeprecatedCompat_SE.ttcn:::96:::E:::Type mismatch: a value of type `integer[2]' was expected instead of `@DeprecatedCompat_SE.Rec1'",
				"DeprecatedCompat_SE.ttcn:::97:::E:::Type mismatch: a value of type `@DeprecatedCompat_SE.Set2' was expected instead of `@DeprecatedCompat_SE.SetOf'",
				"DeprecatedCompat_SE.ttcn:::101:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Rec1' was expected instead of `@DeprecatedCompat_SE.RecOf'",
				"DeprecatedCompat_SE.ttcn:::102:::E:::Type mismatch in value redirect: A variable of type `@DeprecatedCompat_SE.Rec1' was expected instead of `integer[2]'",
				"DeprecatedCompat_SE.ttcn:::103:::E:::Type mismatch: a value or template of type `@DeprecatedCompat_SE.Nested2' was expected instead of `@DeprecatedCompat_SE.Nested1'",
				"DeprecatedCompat_SE.ttcn:::104:::E:::Redirecting multiple values is not supported in the Load Test Runtime."
		);
	}
	
	private List<String> deterministicMarkers() {
		return
			List.of(
				"Deterministic_OK.ttcn:::23:::E:::Please ensure that the `ef' external function complies with the requirements in clause 16.1.4 of the TTCN-3 core language standard (ES 201 873-1)"
		);
	}
	
	private List<String> encodeMarkers() {
		return
			List.of(
				"encode_grp_mod_SE.ttcn:::20:::E:::Variant attribute is not related to JSON encoding",
				"encode_grp_mod_SE.ttcn:::28:::E:::Variant attribute is not related to RAW encoding",
				"encode_grp_mod_SE.ttcn:::30:::E:::Variant attribute is not related to RAW encoding",
				"encode_grp_mod_SE.ttcn:::24:::W:::Attribute 'name as ...' will be ignored, because parent union is encoded without field names.",
				"encode_grp_mod_SE.ttcn:::38:::E:::Variant attribute is not related to XER encoding",
				"encode_grp_mod_SE.ttcn:::39:::E:::Variant attribute is not related to XER encoding",
				"encode_grp_mod_SE.ttcn:::48:::E:::Variant attribute is not related to RAW encoding",
				"encode_grp_mod_SE.ttcn:::58:::E:::Variant attribute is not related to TEXT encoding",
				"encode_grp_mod_SE.ttcn:::63:::E:::Variant attribute is not related to TEXT encoding",
				"encode_modifier_SE.ttcn:::74:::E:::Input type `@encode_modifier.Rec' does not support TEXT encoding.",
				"encode_modifier_SE.ttcn:::76:::E:::Input type `@encode_modifier.Rec' does not support JSON encoding.",
				"encode_modifier_SE.ttcn:::79:::E:::Input type `@encode_modifier.Enum' does not support RAW encoding.",
				"encode_modifier_SE.ttcn:::81:::E:::Input type `@encode_modifier.Enum' does not support XER encoding.",
				"encode_modifier_SE.ttcn:::83:::E:::Input type `@encode_modifier.Enum' does not support TEXT encoding.",
				"encode_modifier_SE.ttcn:::88:::E:::Input type `@encode_modifier.RoI' does not support XER encoding.",
				"encode_modifier_SE.ttcn:::90:::E:::Input type `@encode_modifier.RoI' does not support TEXT encoding.",
				"encode_modifier_SE.ttcn:::95:::E:::Input type `@encode_modifier.Enum' does not support RAW encoding.",
				"encode_modifier_SE.ttcn:::100:::E:::Input type `@encode_modifier.RoI' does not support XER encoding.",
				"encode_modifier_SE.ttcn:::102:::E:::Input type `@encode_modifier.RoI' does not support JSON encoding.",
				"encode_modifier_SE.ttcn:::109:::E:::Input type `@encode_modifier.Set' does not support XER encoding.",
				"encode_modifier_SE.ttcn:::114:::E:::Input type `@encode_modifier.SoOS' does not support JSON encoding.",
				"encode_modifier_SE.ttcn:::116:::E:::Input type `@encode_modifier.SoOS' does not support XER encoding.",
				"encode_modifier_SE.ttcn:::119:::E:::Input type `@encode_modifier.RoI' does not support JSON encoding.",
				"encode_modifier_SE.ttcn:::126:::E:::All 'encode' attributes of a type must have the same modifier ('override', '@local' or none)",
				"encode_modifier_SE.ttcn:::127:::E:::All 'encode' attributes of a type must have the same modifier ('override', '@local' or none)",
				"encode_modifier_SE.ttcn:::142:::E:::All 'encode' attributes of a group or module must have the same modifier ('override', '@local' or none)",
				"encode_modifier_SE.ttcn:::150:::E:::All 'encode' attributes of a group or module must have the same modifier ('override', '@local' or none)",
				"encode_modifier_SE.ttcn:::158:::E:::All 'encode' attributes of a group or module must have the same modifier ('override', '@local' or none)",
				"encode_modifier_SE.ttcn:::167:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_modifier_SE.ttcn:::168:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_modifier_SE.ttcn:::168:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_modifier_SE.ttcn:::171:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_modifier_SE.ttcn:::171:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_modifier_SE.ttcn:::174:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_modifier_SE.ttcn:::174:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_modifier_SE.ttcn:::175:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_SE.ttcn:::19:::E:::No encoding rules defined for type `@encode_SE.Rec1'",
				"encode_SE.ttcn:::27:::E:::in variant attribute, at or before token `uncapitalizeded': syntax error",
				"encode_SE.ttcn:::35:::E:::Variant attribute is not related to JSON encoding",
				"encode_SE.ttcn:::36:::E:::This format is not supported for the JSON codec",
				"encode_SE.ttcn:::36:::E:::Variant attribute is not related to JSON encoding",
				"encode_SE.ttcn:::47:::E:::Variant attribute is not related to XER encoding",
				"encode_SE.ttcn:::50:::E:::The encoding reference is mandatory for variant attributes of type `@encode_SE.Rec2', which has multiple encodings",
				"encode_SE.ttcn:::52:::E:::Variant attribute is not related to JSON encoding",
				"encode_SE.ttcn:::53:::E:::Type `@encode_SE.Rec2' does not support XER encoding",
				"encode_SE.ttcn:::54:::E:::Variant attribute is not related to JSON encoding",
				"encode_SE.ttcn:::73:::W:::Encode attribute is ignored, because it refers to a type from a different type definition",
				"encode_SE.ttcn:::74:::W:::Variant attribute is ignored, because it refers to a type from a different type definition",
				"encode_SE.ttcn:::58:::E:::Invalid attribute, 'omit as null' requires optional field of a record or set.",
				"encode_SE.ttcn:::60:::E:::Invalid attribute, 'name as ...' requires field of a record, set or union.",
				"encode_SE.ttcn:::60:::E:::Invalid JSON default value for type `@encode_SE.Rec5.f3'. Only the empty array is allowed.",
				"encode_SE.ttcn:::59:::E:::USE-UNION can only be applied to a CHOICE/union type",
				"encode_SE.ttcn:::81:::W:::Variant attributes related to custom encoding are ignored",
				"encode_SE.ttcn:::82:::W:::Variant attributes related to custom encoding are ignored",
				"encode_SE.ttcn:::83:::E:::Type `integer' does not support custom encoding `xyz'",
				"encode_SE.ttcn:::117:::E:::No coding rule specified for type '@encode_SE.Rec1'",
				"encode_SE.ttcn:::118:::E:::The encoding string does not match any encodings of type `@encode_SE.Rec2'",
				"encode_SE.ttcn:::119:::E:::The encoding string does not match any encodings of type `@encode_SE.Rec3'",
				"encode_SE.ttcn:::120:::E:::Fifth operand of operation `decvalue_unichar' should be (universal) charstring value",
				"encode_SE.ttcn:::135:::E:::The type argument has no encoding rules defined",
				"encode_SE.ttcn:::136:::W:::The type argument has only one encoding rule defined. The 'setencode' statement will be ignored",
				"encode_SE.ttcn:::136:::E:::The encoding string does not match any encodings of type `@encode_SE.Rec2'",
				"encode_SE.ttcn:::137:::W:::The type argument has only one encoding rule defined. The 'setencode' statement will be ignored",
				"encode_SE.ttcn:::137:::E:::character string value was expected",
				"encode_SE.ttcn:::138:::E:::The encoding string does not match any encodings of type `@encode_SE.Rec2'",
				"encode_SE.ttcn:::114:::W:::Multiple `xy' encoder functions defined for type `@encode_SE.SetOf2'",
				"encode_SE.ttcn:::115:::W:::No `xy' decoder function defined for type `@encode_SE.SetOf3'"
		);
	}
	
	private List<String> encode_legacyMarkers() {
		return
			List.of(
				"encode_asn_SE.ttcn:::33:::E:::The input type of PER decoding should be `bitstring' or `octetstring' instead of `charstring'",
				"encode_asn_SE.ttcn:::127:::E:::There is no local or imported definition with name `NonExistent'",
				"encode_asn_SE.ttcn:::127:::E:::Input type `Erroneous type' does not support JSON encoding.",
				"encode_asn_SE.ttcn:::130:::E:::There is no local or imported definition with name `NonExistent'",
				"encode_asn_SE.ttcn:::130:::E:::Input type `Erroneous type' does not support PER encoding.",
				"encode_asn_SE.ttcn:::133:::E:::There is no local or imported definition with name `NonExistent'",
				"encode_asn_SE.ttcn:::136:::E:::There is no local or imported definition with name `NonExistent'",
				"encode_asn_SE.ttcn:::136:::E:::Output type `Erroneous type' does not support PER encoding",
				"encode_ttcn_SE.ttcn:::37:::E:::Only `prototype(convert)' is allowed for custom encoding functions",
				"encode_ttcn_SE.ttcn:::40:::E:::The output type of custom encoding should be `bitstring' instead of `octetstring'",
				"encode_ttcn_SE.ttcn:::43:::E:::Only `prototype(sliding)' is allowed for custom decoding functions",
				"encode_ttcn_SE.ttcn:::46:::E:::Only `prototype(sliding)' is allowed for custom decoding functions",
				"encode_ttcn_SE.ttcn:::49:::E:::Only `prototype(sliding)' is allowed for custom decoding functions",
				"encode_ttcn_SE.ttcn:::52:::E:::The input type of custom decoding should be `bitstring' instead of `charstring'",
				"encode_ttcn_SE.ttcn:::150:::E:::Input type `@encode_ttcn_SE.C2' does not support XER encoding.",
				"encode_ttcn_SE.ttcn:::158:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_ttcn_SE.ttcn:::159:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_ttcn_SE.ttcn:::159:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_ttcn_SE.ttcn:::162:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_ttcn_SE.ttcn:::162:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_ttcn_SE.ttcn:::165:::W:::The '@local' modifier only affects 'encode' attributes. Modifier ignored.",
				"encode_ttcn_SE.ttcn:::165:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_ttcn_SE.ttcn:::166:::E:::Invalid attribute format. Dot notation is only allowed for variant attributes when using the new codec handling.",
				"encode_ttcn_SE.ttcn:::26:::W:::No `nonexistent' decoder function defined for type `integer'",
				"encode_ttcn_SE.ttcn:::25:::W:::No `whatever' decoder function defined for type `@encode_ttcn_SE.ARecord'",
				"encode_ttcn_SE.ttcn:::74:::W:::Multiple `codec2' encoder functions defined for type `@encode_ttcn_SE.AUnion'",
				"encode_ttcn_SE.ttcn:::74:::W:::Multiple `codec2' decoder functions defined for type `@encode_ttcn_SE.AUnion'"
		);
	}
	
	private List<String> erroneous_attributesMarkers() {
		return
			List.of(
				"ErroneousAttributes_SE.ttcn:::37:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::37:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::39:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::39:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::41:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::41:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::43:::E:::field qualifiers are only allowed for record, set and union types",
				"ErroneousAttributes_SE.ttcn:::45:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::45:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::47:::E:::field qualifiers are only allowed for record, set and union types",
				"ErroneousAttributes_SE.ttcn:::49:::E:::field qualifiers are only allowed for record, set and union types",
				"ErroneousAttributes_SE.ttcn:::51:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::51:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::53:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::53:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::55:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::55:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::57:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::57:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::59:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::59:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::61:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::61:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::63:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::63:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::65:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::65:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::67:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::67:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::69:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::69:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::71:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::71:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::73:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::73:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::75:::E:::`erroneous' attributes can be used only with the Function Test Runtime",
				"ErroneousAttributes_SE.ttcn:::75:::E:::If you need negative testing use the -R flag when generating the makefile",
				"ErroneousAttributes_SE.ttcn:::79:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::80:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::81:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::82:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::83:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::84:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::85:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::86:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::87:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::88:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::89:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::91:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::92:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::93:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::94:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::95:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::97:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::98:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::99:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::101:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::102:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::103:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::104:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::108:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::127:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::130:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::134:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::137:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::141:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::146:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::151:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::154:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::162:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::168:::E:::The @update statement is only available in the Function Test runtime",
				"ErroneousAttributes_SE.ttcn:::174:::E:::The @update statement is only available in the Function Test runtime"
		);
	}
}
