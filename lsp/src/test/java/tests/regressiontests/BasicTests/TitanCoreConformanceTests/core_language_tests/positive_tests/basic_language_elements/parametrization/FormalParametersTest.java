/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.basic_language_elements.parametrization;

import java.util.List;

import tests.TestBase;

public class FormalParametersTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\05_basic_language_elements\\0504_parametrization\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("050401_formal_parameters", formalParameterMarkers());
	}
	
	private List<String> formalParameterMarkers() {
		return
			List.of(
				"05040101_parameters_of_kind_value\\NegSem_05040101_parameters_of_kind_value_014.ttcn:::31:::E:::at or before token `@lazy': syntax error, unexpected LazyKeyword",
				"05040101_parameters_of_kind_value\\NegSem_05040101_parameters_of_kind_value_015.ttcn:::31:::E:::at or before token `@fuzzy': syntax error, unexpected FuzzyKeyword",
				"05040101_parameters_of_kind_value\\NegSem_05040101_parameters_of_kind_value_016.ttcn:::31:::E:::at or before token `@lazy': syntax error, unexpected LazyKeyword",
				"05040101_parameters_of_kind_value\\NegSem_05040101_parameters_of_kind_value_017.ttcn:::31:::E:::at or before token `@fuzzy': syntax error, unexpected FuzzyKeyword",
				"05040101_parameters_of_kind_value\\NegSyn_05040101_parameters_of_kind_value_006.ttcn:::29:::E:::at or before token `integer': syntax error, unexpected IntegerKeyword",
				"05040101_parameters_of_kind_value\\NegSyn_05040101_parameters_of_kind_value_007.ttcn:::29:::E:::at or before token `integer': syntax error, unexpected IntegerKeyword",
				"05040101_parameters_of_kind_value\\NegSyn_05040101_parameters_of_kind_value_011.ttcn:::29:::E:::at or before token `integer': syntax error, unexpected IntegerKeyword",
				"05040102_parameters_of_kind_template\\NegSem_05040102_parameters_of_kind_template_014.ttcn:::31:::E:::at or before token `@lazy': syntax error, unexpected LazyKeyword",
				"05040102_parameters_of_kind_template\\NegSem_05040102_parameters_of_kind_template_015.ttcn:::31:::E:::at or before token `@fuzzy': syntax error, unexpected FuzzyKeyword",
				"05040102_parameters_of_kind_template\\NegSem_05040102_parameters_of_kind_template_016.ttcn:::31:::E:::at or before token `@lazy': syntax error, unexpected LazyKeyword",
				"05040102_parameters_of_kind_template\\NegSem_05040102_parameters_of_kind_template_017.ttcn:::31:::E:::at or before token `@fuzzy': syntax error, unexpected FuzzyKeyword",
				"05040103_parameters_of_kind_timer\\NegSem_05040103_parameters_of_kind_timer_003.ttcn:::31:::E:::at or before token `timer': syntax error, unexpected TimerKeyword",
				"05040103_parameters_of_kind_timer\\NegSem_05040103_parameters_of_kind_timer_004.ttcn:::31:::E:::at or before token `timer': syntax error, unexpected TimerKeyword",
				"05040103_parameters_of_kind_timer\\NegSyn_05040103_parameters_of_kind_timer_001.ttcn:::30:::E:::at or before token `timer': syntax error, unexpected TimerKeyword",
				"05040103_parameters_of_kind_timer\\NegSyn_05040103_parameters_of_kind_timer_002.ttcn:::31:::E:::at or before token `timer': syntax error, unexpected TimerKeyword"
		);
	}
}
