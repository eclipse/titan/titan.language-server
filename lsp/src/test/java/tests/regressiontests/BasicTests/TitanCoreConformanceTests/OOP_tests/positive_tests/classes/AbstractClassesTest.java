/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.OOP_tests.positive_tests.classes;

import tests.TestBase;

public class AbstractClassesTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\OOP_tests\\positive_tests\\501_classes_and_objects\\50101_classes";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("5010102_abstract_classes");
	}
}
