/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.test_configurations;

import java.util.List;

import tests.TestBase;

public class TestSystemInterfaceTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\09_test_configurations\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0902_test_system_interface",
			List.of(
				"NegSem_0902_Communication_ports_001.ttcn:::45:::W:::Port type `@NegSem_0902_Communication_ports_001.MyMessagePortType' was marked as `internal'",
				"NegSem_0902_Communication_ports_001.ttcn:::45:::W:::Port type `@NegSem_0902_Communication_ports_001.loopbackPort' was marked as `internal'",
				"NegSem_0902_Communication_ports_001.ttcn:::46:::W:::Port type `@NegSem_0902_Communication_ports_001.MyMessagePortType' was marked as `internal'",
				"NegSem_0902_Communication_ports_001.ttcn:::46:::W:::Port type `@NegSem_0902_Communication_ports_001.loopbackPort' was marked as `internal'",
				"NegSem_0902_Communication_ports_002.ttcn:::46:::W:::Port type `@NegSem_0902_Communication_ports_002.MyMessagePortType' was marked as `internal'",
				"NegSem_0902_Communication_ports_002.ttcn:::46:::W:::Port type `@NegSem_0902_Communication_ports_002.loopbackPort' was marked as `internal'",
				"NegSem_0902_Communication_ports_002.ttcn:::47:::W:::Port type `@NegSem_0902_Communication_ports_002.MyMessagePortType' was marked as `internal'",
				"NegSem_0902_Communication_ports_002.ttcn:::47:::W:::Port type `@NegSem_0902_Communication_ports_002.loopbackPort' was marked as `internal'",
				"NegSem_0902_Communication_ports_003.ttcn:::37:::E:::Both endpoints of the mapping are system ports",
				"NegSem_0902_Communication_ports_004.ttcn:::48:::W:::Port type `@NegSem_0902_Communication_ports_004.IntegerOutputPortType' was marked as `internal'",
				"NegSem_0902_Communication_ports_004.ttcn:::48:::W:::Port type `@NegSem_0902_Communication_ports_004.IntegerOutputPortType' was marked as `internal'",
				"Syn_0902_Communication_ports_001.ttcn:::41:::W:::Port type `@Syn_0902_Communication_ports_001.myPort' was marked as `internal'",
				"Syn_0902_Communication_ports_001.ttcn:::41:::W:::Port type `@Syn_0902_Communication_ports_001.myPort' was marked as `internal'",
				"Syn_0902_Communication_ports_001.ttcn:::42:::W:::Port type `@Syn_0902_Communication_ports_001.myPort' was marked as `internal'",
				"Syn_0902_Communication_ports_001.ttcn:::42:::W:::Port type `@Syn_0902_Communication_ports_001.myPort' was marked as `internal'"
			)
		);
	}
}
