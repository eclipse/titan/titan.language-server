/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.modules;

import java.util.List;

import tests.TestBase;

public class ModuleControlPartTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\08_modules\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0803_module_control_part",
			List.of(
				"NegSyn_0803_ModuleControlPart_001.ttcn:::28:::E:::" + REGEXPREFIX + "Syntax error"
			)
		);
	}
}
