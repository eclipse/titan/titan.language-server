/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.OOP_tests.positive_tests.classes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	AbstractClassesTest.class,
	ConstructorTest.class,
	DestructorTest.class,
	ExternalClassesTest.class,
	FinalClassesTest.class,
	MethodInvocationTest.class,
	MethodsTest.class,
	ScopeRulesTest.class, 
	TopLevelTest.class,
	VisibilityTest.class
})

public class ClassTests {

}
