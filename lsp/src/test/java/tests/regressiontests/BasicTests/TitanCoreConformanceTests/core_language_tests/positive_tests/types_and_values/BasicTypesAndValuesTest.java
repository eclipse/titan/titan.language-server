/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.types_and_values;

import java.util.List;

import tests.TestBase;

public class BasicTypesAndValuesTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\06_types_and_values";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0601_basic_types_and_values",
			List.of(
				"60101_basic_string_types_and_values\\060101_toplevel\\NegSyn_060101_TopLevel_001.ttcn:::22:::E:::Bitstring value contains invalid character",
				"60101_basic_string_types_and_values\\060101_toplevel\\NegSyn_060101_TopLevel_007.ttcn:::22:::E:::Octetstring value contains odd number of hexadecimal digits",
				"60101_basic_string_types_and_values\\060101_toplevel\\Sem_060101_TopLevel_014.ttcn:::27:::E:::Invalid escape sequence: `\\ '",
				"60101_basic_string_types_and_values\\060101_toplevel\\Sem_060101_TopLevel_014.ttcn:::28:::E:::Invalid escape sequence: `\\ '",
				"60101_basic_string_types_and_values\\060101_toplevel\\Sem_060101_TopLevel_015.ttcn:::27:::E:::Invalid escape sequence: `\\ '"
			)
		);
	}
}
