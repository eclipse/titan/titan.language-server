/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.types_and_values;

import java.util.List;

import tests.TestBase;

public class StructuredTypesAndValuesTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\06_types_and_values";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0602_structured_types_and_values",
			List.of(
				"60204_enumerated_type_and_values\\Sem_060204_enumerated_type_and_values_007.ttcn:::35:::E:::at or before token `..': syntax error, unexpected ..",
				"60205_unions\\06020502_option_and_union\\NegSyn_06020502_option_and_union_001.ttcn:::29:::E:::at or before token `optional': syntax error, unexpected OptionalKeyword, expecting ',' or '}'",
				"60209_comm_port_types\\NegSem_060209_CommunicationPortTypes_001.ttcn:::41:::E:::Multiple address type declarations in port type definition",
				"60209_comm_port_types\\NegSem_060209_CommunicationPortTypes_002.ttcn:::43:::E:::Multiple `map' parameter lists in port type definition",
				"60209_comm_port_types\\NegSem_060209_CommunicationPortTypes_003.ttcn:::44:::E:::Multiple `unmap' parameter lists in port type definition",
				"60209_comm_port_types\\NegSem_060209_CommunicationPortTypes_006.ttcn:::47:::E:::Multiple address type declarations in port type definition",
				"60209_comm_port_types\\NegSem_060209_CommunicationPortTypes_007.ttcn:::50:::E:::Multiple `map' parameter lists in port type definition",
				"60209_comm_port_types\\NegSem_060209_CommunicationPortTypes_008.ttcn:::50:::E:::Multiple `unmap' parameter lists in port type definition",
				"60213_subtyping_of_structured_types\\06021302_list_subtyping\\Sem_06021302_ListSubtyping_003.ttcn:::33:::E:::at or before token `pattern': syntax error, unexpected PatternKeyword",
				"60213_subtyping_of_structured_types\\06021302_list_subtyping\\Sem_06021302_ListSubtyping_003.ttcn:::34:::E:::at or before token `..': syntax error, unexpected ..",
				"602_toplevel\\NegSyn_0602_TopLevel_003.ttcn:::29:::E:::at or before token `\"': syntax error, unexpected Cstring"
			)
		);
	}
}
