/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates;

import java.util.List;

import tests.TestBase;

public class MatchOperationTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\15_templates\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1509_match_operation",
			List.of(
				"NegSem_1509_MatchOperation_001.ttcn:::30:::E:::Reference to a value was expected instead of template `@NegSem_1509_MatchOperation_001.m_second'",
				"NegSem_1509_MatchOperation_003.ttcn:::34:::E:::Type mismatch: a value of type `@NegSem_1509_MatchOperation_003.B_enum' was expected instead of `@NegSem_1509_MatchOperation_003.A_enum'",
				"Sem_1509_MatchOperation_005.ttcn:::39:::E:::Type mismatch: a value of type `@Sem_1509_MatchOperation_005.MyRecord' was expected instead of `integer'",
				"Sem_1509_MatchOperation_009.ttcn:::39:::E:::Type mismatch: a value of type `@Sem_1509_MatchOperation_009.MySet' was expected instead of `integer'",
				"Sem_1509_MatchOperation_014.ttcn:::33:::E:::Type mismatch: a value of type `@Sem_1509_MatchOperation_014.B_enum' was expected instead of `@Sem_1509_MatchOperation_014.A_enum'"
			)
		);
	}
}
