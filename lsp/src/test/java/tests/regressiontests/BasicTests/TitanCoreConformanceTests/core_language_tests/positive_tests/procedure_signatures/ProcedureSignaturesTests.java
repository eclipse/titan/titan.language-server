/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.procedure_signatures;

import java.util.List;

import tests.TestBase;

public class ProcedureSignaturesTests extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("14_procedure_signatures",
			List.of(
				"Sem_13_declaring_msg_003.ttcn:::126:::W:::Function invocation 'make_union()' may change the actual snapshot."
			)
		);
	}
}
