/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.expressions;

import java.util.List;

import tests.TestBase;

public class TopLevelTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\07_expressions\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("07_toplevel",
			List.of(
				"NegSem_07_toplevel_001.ttcn:::36:::E:::Reference to a value was expected instead of a call of function `@NegSem_07_toplevel_001.f', which does not have return type",
				"NegSem_07_toplevel_002.ttcn:::39:::E:::Reference to a value was expected instead of template variable `vm_num'"
			)
		);
	}
}
