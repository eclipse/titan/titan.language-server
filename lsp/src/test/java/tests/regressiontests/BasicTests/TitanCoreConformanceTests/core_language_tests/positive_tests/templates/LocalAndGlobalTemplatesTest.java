/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates;

import java.util.List;

import tests.TestBase;

public class LocalAndGlobalTemplatesTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\15_templates\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1503_global_and_local_templates",
			List.of(
				"NegSem_1503_GlobalAndLocalTemplates_004.ttcn:::33:::E:::Parameterized local template `t' not supported",
				"NegSyn_1503_GlobalAndLocalTemplates_001.ttcn:::30:::E:::Missing template body",
				"NegSyn_1503_GlobalAndLocalTemplates_002.ttcn:::33:::E:::Missing template body",
				"NegSyn_1503_GlobalAndLocalTemplates_003.ttcn:::30:::E:::Missing template body",
				"NegSyn_1503_GlobalAndLocalTemplates_004.ttcn:::33:::E:::Parameterized local template `t' not supported",
				"NegSyn_1503_GlobalAndLocalTemplates_004.ttcn:::33:::E:::Missing template body"
			)
		);
	}
}
