/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.test_configurations;

import java.util.List;

import tests.TestBase;

public class CommunicationPortsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\09_test_configurations\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0901_communication_ports",
			List.of(
				"NegSem_0901_Communication_ports_002.ttcn:::45:::W:::Port type `@NegSem_0901_Communication_ports_002.loopbackPort' was marked as `internal'",
				"NegSem_0901_Communication_ports_002.ttcn:::45:::W:::Port type `@NegSem_0901_Communication_ports_002.loopbackPort' was marked as `internal'",
				"NegSem_0901_Communication_ports_002_wrong.ttcn:::51:::E:::Both endpoints of the mapping are system ports",
				"NegSem_0901_Communication_ports_004.ttcn:::50:::W:::Port type `@NegSem_0901_Communication_ports_004.MyPort' was marked as `internal'",
				"NegSem_0901_Communication_ports_004.ttcn:::50:::W:::Port type `@NegSem_0901_Communication_ports_004.MyPort' was marked as `internal'",
				"NegSem_0901_Communication_ports_006.ttcn:::49:::W:::Port type `@NegSem_0901_Communication_ports_006.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_006.ttcn:::49:::W:::Port type `@NegSem_0901_Communication_ports_006.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_006.ttcn:::50:::W:::Port type `@NegSem_0901_Communication_ports_006.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_006.ttcn:::50:::W:::Port type `@NegSem_0901_Communication_ports_006.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_007.ttcn:::38:::W:::Port type `@NegSem_0901_Communication_ports_007.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_007.ttcn:::38:::W:::Port type `@NegSem_0901_Communication_ports_007.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_008.ttcn:::46:::W:::Port type `@NegSem_0901_Communication_ports_008.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_008.ttcn:::46:::W:::Port type `@NegSem_0901_Communication_ports_008.P' was marked as `internal'",
				"NegSem_0901_Communication_ports_009.ttcn:::37:::E:::Both endpoints of the mapping are system ports",
				"NegSyn_0901_Communication_ports_001.ttcn:::43:::E:::The `system' component reference shall not be used in `connect' operation",
				"NegSyn_0901_Communication_ports_001.ttcn:::43:::E:::The `system' component reference shall not be used in `connect' operation",
				"Sem_0901_Communication_ports_011.ttcn:::51:::W:::Port type `@Sem_0901_Communication_ports_011.MyPort' was marked as `internal'",
				"Sem_0901_Communication_ports_011.ttcn:::51:::W:::Port type `@Sem_0901_Communication_ports_011.MyPort' was marked as `internal'",
				"Sem_0901_Communication_ports_011.ttcn:::52:::W:::Port type `@Sem_0901_Communication_ports_011.MyPort' was marked as `internal'",
				"Sem_0901_Communication_ports_011.ttcn:::52:::W:::Port type `@Sem_0901_Communication_ports_011.MyPort' was marked as `internal'"
			)
		);
	}
}
