/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.variables;

import java.util.List;

import tests.TestBase;

public class TemplateVariablesTest extends TestBase {

	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\11_variables\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1102_template_variables",
			List.of(
				"NegSyn_1102_TemplateVars_001.ttcn:::22:::E:::" + REGEXPREFIX + ".*Syntax error.*",
				"NegSem_1102_TemplateVars_003.ttcn:::38:::E:::Duplicate incoming message type `integer'",
				"NegSem_1102_TemplateVars_003.ttcn:::38:::E:::Type `integer' is already listed here",
				"NegSem_1102_TemplateVars_003.ttcn:::38:::E:::Duplicate outgoing message type `integer'",
				"NegSem_1102_TemplateVars_003.ttcn:::38:::E:::Type `integer' is already listed here",
				"NegSyn_1102_TemplateVars_002.ttcn:::31:::E:::Type mismatch: a value or template of type `float' was expected instead of `integer'"
			)
		);
	}

}
