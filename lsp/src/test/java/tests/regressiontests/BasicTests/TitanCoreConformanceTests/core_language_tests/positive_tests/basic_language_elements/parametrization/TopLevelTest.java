/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.basic_language_elements.parametrization;

import java.util.List;

import tests.TestBase;

public class TopLevelTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\05_basic_language_elements\\0504_parametrization\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0504_toplevel", topLevelMarkers());
	}
	
	private List<String> topLevelMarkers() {
		return
			List.of(
				"NegSyn_0504_forbidden_parametrization_002.ttcn:::23:::E:::" + REGEXPREFIX + ".*Syntax error.*"
		);
	}
}
