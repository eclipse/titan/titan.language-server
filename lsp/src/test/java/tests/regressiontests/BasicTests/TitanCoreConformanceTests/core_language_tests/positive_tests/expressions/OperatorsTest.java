/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.expressions;

import java.util.List;

import tests.TestBase;

public class OperatorsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\07_expressions\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0701_operators",
			List.of(
				"070101_arithmetic_operators\\NegSem_070101_ArithmeticOperators_001.ttcn:::31:::E:::Second operand of operation `*' should be integer or float value",
				"070101_arithmetic_operators\\NegSem_070101_ArithmeticOperators_001.ttcn:::31:::E:::The operands of operation `*' should be of same type",
				"070101_arithmetic_operators\\NegSem_070101_ArithmeticOperators_002.ttcn:::31:::E:::The operands of operation `*' should be of same type",
				"070101_arithmetic_operators\\NegSem_070101_ArithmeticOperators_003.ttcn:::31:::E:::Left operand of operation `mod' should be integer value",
				"070101_arithmetic_operators\\NegSem_070101_ArithmeticOperators_003.ttcn:::31:::E:::Right operand of operation `mod' should be integer value",
				"070101_arithmetic_operators\\NegSem_070101_ArithmeticOperators_004.ttcn:::31:::E:::Left operand of operation `rem' should be integer value",
				"070101_arithmetic_operators\\NegSem_070101_ArithmeticOperators_004.ttcn:::31:::E:::Right operand of operation `rem' should be integer value",
				"070101_arithmetic_operators\\Sem_070101_ArithmeticOperators_052.ttcn:::43:::E:::There is no local or imported definition with name `TC_Sem_070101_ArithmeticOperators_051'",
				"070101_arithmetic_operators\\Sem_070101_ArithmeticOperators_053.ttcn:::46:::E:::" + REGEXPREFIX + "There is no.*definition with name `TC_Sem_070101_ArithmeticOperators_052'.*",
				"070102_list_operator\\Sem_070102_ListOperator_005.ttcn:::34:::E:::Left operand of operation `&' should be a string, `record of' or `set of' value",
				"070102_list_operator\\Sem_070102_ListOperator_005.ttcn:::34:::E:::Right operand of operation `&' should be a string, `record of' or `set of' value",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_002.ttcn:::54:::E:::Left operand of operation `&' should be a string, `record of' or `set of' value",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_002.ttcn:::54:::E:::Right operand of operation `&' should be a string, `record of' or `set of' value",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_002.ttcn:::54:::E:::The operands of operation `&' should be of compatible types",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_003.ttcn:::55:::E:::Left operand of operation `&' should be a string, `record of' or `set of' value",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_003.ttcn:::55:::E:::Right operand of operation `&' should be a string, `record of' or `set of' value",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_003.ttcn:::55:::E:::The operands of operation `&' should be of compatible types",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_004.ttcn:::54:::E:::Left operand of operation `&' should be a string, `record of' or `set of' value",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_004.ttcn:::54:::E:::Right operand of operation `&' should be a string, `record of' or `set of' value",
				"070103_relational_operators\\NegSem_070103_RelationalOperators_004.ttcn:::54:::E:::The operands of operation `&' should be of compatible types",
				"070103_relational_operators\\NegSyn_070103_RelationalOperators_001.ttcn:::34:::E:::Right operand of operation `>' should be integer, float or enumerated value",
				"070103_relational_operators\\NegSyn_070103_RelationalOperators_002.ttcn:::34:::E:::Right operand of operation `<' should be integer, float or enumerated value",
				"070103_relational_operators\\NegSyn_070103_RelationalOperators_003.ttcn:::34:::E:::Right operand of operation `<=' should be integer, float or enumerated value",
				"070103_relational_operators\\NegSyn_070103_RelationalOperators_004.ttcn:::34:::E:::Right operand of operation `>=' should be integer, float or enumerated value"
			)
		);
	}
}
