package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.test_configurations;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	CommunicationPortsTest.class, 
	TestSystemInterfaceTest.class 
})
public class TestConfigurationsTests {

}
