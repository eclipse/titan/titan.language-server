/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.functions_altsteps_testcases;

import java.util.List;

import tests.TestBase;

public class TestcasesTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\16_functions_altsteps_testcases\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1603_testcases",
			List.of(
				"NegSem_1603_testcases_001.ttcn:::27:::E:::A definition that has `runs on' clause cannot execute testcases",
				"NegSem_1603_testcases_002.ttcn:::27:::E:::Reference to a function or altstep was expected instead of testcase `@NegSem_1603_testcases_002.TC_fail', which cannot be invoked"
			)
		);
	}
}
