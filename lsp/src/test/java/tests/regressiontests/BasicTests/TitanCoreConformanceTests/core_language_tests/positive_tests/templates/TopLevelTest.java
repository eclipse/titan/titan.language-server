/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates;

import java.util.List;

import tests.TestBase;

public class TopLevelTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\15_templates\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("15_toplevel",
			List.of(
				"NegSem_15_TopLevel_001.ttcn:::25:::W:::A template should not contain a field with default type at any level.",
				"NegSem_15_TopLevel_003.ttcn:::25:::W:::A template should not contain a field with default type at any level.",
				"NegSem_15_TopLevel_004.ttcn:::27:::E:::Port type `@NegSem_15_TopLevel_004.MyPort' cannot be embedded into another type",
				"NegSem_15_TopLevel_004.ttcn:::24:::W:::A template should not contain a field with port type at any level.",
				"NegSyn_15_TopLevel_001.ttcn:::32:::E:::integer value was expected"
			)
		);
	}
}
