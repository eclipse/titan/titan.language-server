/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.functions_altsteps_testcases;

import java.util.List;

import tests.TestBase;

public class AltstepsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\16_functions_altsteps_testcases\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1602_altsteps",
			List.of(
				"1602_toplevel\\NegSyn_1602_toplevel_001.ttcn:::48:::E:::"  + REGEXPREFIX + ".*Syntax error.*"
			)
		);
	}
}
