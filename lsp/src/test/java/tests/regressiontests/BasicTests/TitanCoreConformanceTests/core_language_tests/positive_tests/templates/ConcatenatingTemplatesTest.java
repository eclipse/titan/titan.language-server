/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates;

import java.util.List;

import tests.TestBase;

public class ConcatenatingTemplatesTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\15_templates\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1511_concatenating_templates_of_string_and_list_types",
			List.of(
				"NegSem_1511_ConcatenatingTemplatesOfStringAndListTypes_001.ttcn:::28:::E:::at or before token `'O': syntax error, unexpected OctetStringMatch",
				"NegSem_1511_ConcatenatingTemplatesOfStringAndListTypes_001.ttcn:::28:::E:::Octetstring match contains half octet(s)",
				"NegSem_1511_ConcatenatingTemplatesOfStringAndListTypes_002.ttcn:::28:::E:::at or before token `pattern': syntax error, unexpected PatternKeyword",
				"NegSem_1511_ConcatenatingTemplatesOfStringAndListTypes_002.ttcn:::28:::E:::Invalid binary string literal. Expecting `B', `H' or `O' after the closing `''",
				"NegSem_1511_ConcatenatingTemplatesOfStringAndListTypes_003.ttcn:::28:::E:::Octetstring value contains odd number of hexadecimal digits",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_003.ttcn:::30:::E:::at or before token `'B': syntax error, unexpected BitStringMatch",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_003.ttcn:::33:::E:::at or before token `else': syntax error, unexpected ElseKeyword",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_004.ttcn:::28:::E:::at or before token `'O': syntax error, unexpected OctetStringMatch",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_004.ttcn:::32:::E:::at or before token `else': syntax error, unexpected ElseKeyword",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_005.ttcn:::28:::E:::at or before token `'O': syntax error, unexpected OctetStringMatch",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_005.ttcn:::32:::E:::at or before token `else': syntax error, unexpected ElseKeyword",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_011.ttcn:::28:::E:::at or before token `'O': syntax error, unexpected OctetStringMatch",
				"Sem_1511_ConcatenatingTemplatesOfStringAndListTypes_011.ttcn:::31:::E:::at or before token `else': syntax error, unexpected ElseKeyword"
			));
	}
}
