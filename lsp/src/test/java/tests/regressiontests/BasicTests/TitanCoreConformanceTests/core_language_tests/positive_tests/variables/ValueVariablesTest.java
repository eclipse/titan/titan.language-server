/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.variables;

import java.util.List;

import tests.TestBase;

public class ValueVariablesTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\11_variables\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1101_value_variables",
			List.of(
//				"NegSyn_0803_ModuleControlPart_001.ttcn:::28:::E:::" + REGEXPREFIX + "Syntax error"
				"NegSyn_1101_ValueVars_001.ttcn:::22:::E:::at or before token `var': syntax error, unexpected VarKeyword"
			)
		);
	}
}
