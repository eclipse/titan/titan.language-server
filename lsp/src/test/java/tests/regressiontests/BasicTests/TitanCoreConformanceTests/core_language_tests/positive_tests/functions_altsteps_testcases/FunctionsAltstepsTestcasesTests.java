package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.functions_altsteps_testcases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	AltstepsTest.class, 
	FunctionsTest.class, 
	TestcasesTest.class
})
public class FunctionsAltstepsTestcasesTests {

}
