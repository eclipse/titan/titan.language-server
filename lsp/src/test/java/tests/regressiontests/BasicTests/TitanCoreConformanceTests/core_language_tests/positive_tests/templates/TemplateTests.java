package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	ConcatenatingTemplatesTest.class, 
	DeclaringMessageTemplatesTest.class,
	DeclaringSignatureTemplatesTest.class, 
	InlineTemplatesTest.class, 
	LocalAndGlobalTemplatesTest.class,
	MatchOperationTest.class, 
	ModifiedTemplatesTest.class, 
	ReferencingElementsTest.class,
	TemplateRestrictionsTest.class, 
	TopLevelTest.class, 
	ValueOfOperationTest.class
})
public class TemplateTests {

}
