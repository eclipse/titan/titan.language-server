/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.constants;

import java.util.List;

import tests.TestBase;

public class ConstantsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("10_constants",
			List.of(
				"NegSem_10_Constants_001.ttcn:::23:::E:::A static value was expected instead of operation `rnd (seed)'",
				"NegSem_10_Constants_001.ttcn:::24:::E:::Referenced constant value cannot be evaluated at compile-time",
				"NegSem_10_Constants_002.ttcn:::29:::E:::Reference to a variable or template variable was expected instead of constant `@NegSem_10_Constants_002.c_i'",
				"NegSem_10_Constants_004.ttcn:::44:::E:::" + REGEXPREFIX + "Reference to a \\w+ value was expected instead of variable `R1'",
				"NegSem_10_Constants_005.ttcn:::41:::E:::" + REGEXPREFIX + "Reference to a \\w+ value was expected instead of variable `R2'",
				"NegSem_10_Constants_005.ttcn:::42:::E:::" + REGEXPREFIX + "Reference to a \\w+ value was expected instead of variable `R1'"
			)
		);
	}
}
