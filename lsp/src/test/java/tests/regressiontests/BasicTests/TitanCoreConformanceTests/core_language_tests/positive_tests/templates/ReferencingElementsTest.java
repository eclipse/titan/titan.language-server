/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates;

import java.util.List;

import tests.TestBase;

public class ReferencingElementsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\15_templates\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1506_referencing_elements_of_templates_or_template_fields",
			List.of(
				"150601_referencing_individual_string_elements\\NegSem_150601_ReferencingIndividualStringElements_001.ttcn:::31:::E:::Reference to template variable `m_char1' can not be indexed",
				"150603_referencing_record_of_and_set_elements\\Sem_150603_ReferencingRecordOfAndSetElements_007.ttcn:::37:::E:::Length restriction cannot be used in a template of type `integer'"
			)
		);
	}
}
