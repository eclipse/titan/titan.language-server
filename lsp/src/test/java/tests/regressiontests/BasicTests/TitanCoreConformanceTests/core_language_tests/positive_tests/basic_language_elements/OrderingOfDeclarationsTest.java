/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.basic_language_elements;

import java.util.List;

import tests.TestBase;

public class OrderingOfDeclarationsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\05_basic_language_elements\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0503_ordering_of_declarations", orderingDeclarationMarkers());
	}
	
	private List<String> orderingDeclarationMarkers() {
		return
			List.of(
				"NegSem_0503_Ordering_001.ttcn:::35:::E:::" + REGEXPREFIX + "There is no.*definition with name.*"
		);
	}
}
