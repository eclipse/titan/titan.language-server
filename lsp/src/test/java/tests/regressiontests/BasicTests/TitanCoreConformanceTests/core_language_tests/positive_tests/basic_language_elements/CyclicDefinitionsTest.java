/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.basic_language_elements;

import java.util.List;

import tests.TestBase;

public class CyclicDefinitionsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\05_basic_language_elements\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0505_cyclic_definitions", cyclicDefinitionsMarkers());
	}
	
	private List<String> cyclicDefinitionsMarkers() {
		return
			List.of(
				"NegSem_0505_cyclic_definitions_002.ttcn:::33:::W:::Unsupported selective import statement was treated as `import all'",
				"Sem_0505_cyclic_definitions_002.ttcn:::24:::W:::Unsupported selective import statement was treated as `import all'",
				"Sem_0505_cyclic_definitions_002_import.ttcn:::23:::W:::Unsupported selective import statement was treated as `import all'",
				"Sem_0505_cyclic_definitions_003.ttcn:::32:::W:::Unsupported selective import statement was treated as `import all'",
				"Sem_0505_cyclic_definitions_003_import.ttcn:::32:::W:::Unsupported selective import statement was treated as `import all'",
				"NegSem_0505_cyclic_definitions_001.ttcn:::33:::E:::Circular reference in constant definition `c_test1'",
				"NegSem_0505_cyclic_definitions_001.ttcn:::34:::E:::Circular reference in constant definition `c_test2'",
				"NegSem_0505_cyclic_definitions_001.ttcn:::34:::E:::While searching referenced value: Circular reference: `@NegSem_0505_cyclic_definitions_001.c_test2.b' -> `@NegSem_0505_cyclic_definitions_001.c_test1.b' -> `@NegSem_0505_cyclic_definitions_001.c_test2.b'",
				"NegSem_0505_cyclic_definitions_002_import.ttcn:::34:::E:::Circular reference in constant definition `c_test1'",
				"NegSem_0505_cyclic_definitions_002.ttcn:::35:::E:::Circular reference in constant definition `c_test2'",
				"NegSem_0505_cyclic_definitions_002.ttcn:::35:::E:::While searching referenced value: Circular reference: `@NegSem_0505_cyclic_definitions_002.c_test2.b' -> `@NegSem_0505_cyclic_definitions_002_import.c_test1.b' -> `@NegSem_0505_cyclic_definitions_002.c_test2.b'"
		);
	}
}
