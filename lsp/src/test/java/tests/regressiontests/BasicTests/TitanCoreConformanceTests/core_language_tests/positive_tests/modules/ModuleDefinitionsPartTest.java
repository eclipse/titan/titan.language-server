/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.modules;

import java.util.List;

import tests.TestBase;

public class ModuleDefinitionsPartTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\08_modules\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("0802_module_definitions_part",
			List.of(
				"080201_module_parameters\\NegSyn_080201_ModuleParameters_001.ttcn:::27:::E:::at or before token `modulepar': syntax error, unexpected ModuleParKeyword",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_006.ttcn:::29:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_007.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_008.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_009.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_010.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_011.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_012.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_012_coredump.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_013.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_014.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_015.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_016.ttcn:::29:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_017.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_018.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_019.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_020.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_021.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_022.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_023.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_024.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_025.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_026.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_027.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_028.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_029.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_030.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_031.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_032.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_033.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_034.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_035.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_036.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_037.ttcn:::31:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSem_08020301_GeneralFormatOfImport_038.ttcn:::29:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSyn_08020301_GeneralFormatOfImport_001.ttcn:::32:::E:::at or before token `import': syntax error, unexpected ImportKeyword",
				"080203_importing_from_modules\\08020301_general_format_of_import\\NegSyn_08020301_GeneralFormatOfImport_002.ttcn:::36:::E:::at or before token `import': syntax error, unexpected ImportKeyword",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_008.ttcn:::34:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_009.ttcn:::34:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_010.ttcn:::34:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_011.ttcn:::34:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_012.ttcn:::34:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_013.ttcn:::34:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_014.ttcn:::34:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_015.ttcn:::32:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_016.ttcn:::32:::W:::Unsupported selective import statement was treated as `import all'",
				"080203_importing_from_modules\\08020301_general_format_of_import\\Sem_08020301_GeneralFormatOfImport_020.ttcn:::29:::W:::Unsupported selective import statement was treated as `import all'"
			)
		);
	}
}
