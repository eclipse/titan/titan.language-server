/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.basic_language_elements.scope_rules;

import java.util.List;

import tests.TestBase;

public class UniquenessOfIdentifiersTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\05_basic_language_elements\\0502_scope_rules\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("050202_Uniqueness_of_identifiers", 
			List.of(
				"NegSem_050202_Uniqueness_012.ttcn:::23:::W:::Unsupported selective import statement was treated as `import all'",
				"NegSem_050202_Uniqueness_001.ttcn:::27:::E:::Definition with identifier `cl_int' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_001.ttcn:::23:::E:::Previous definition with identifier `cl_int' in higher scope unit is here",
				"NegSem_050202_Uniqueness_004.ttcn:::30:::E:::Definition with identifier `c_int' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_004.ttcn:::22:::E:::Previous definition with identifier `c_int' in higher scope unit is here",
				"NegSem_050202_Uniqueness_005.ttcn:::28:::E:::Definition with identifier `c_int' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_005.ttcn:::22:::E:::Previous definition with identifier `c_int' in higher scope unit is here",
				"NegSem_050202_Uniqueness_006.ttcn:::27:::E:::Definition with identifier `repeatedIdentifier' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_006.ttcn:::23:::E:::Previous definition with identifier `repeatedIdentifier' in higher scope unit is here",
				"NegSem_050202_Uniqueness_007.ttcn:::27:::E:::Definition with identifier `repeatedIdentifier' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_007.ttcn:::23:::E:::Previous definition with identifier `repeatedIdentifier' in higher scope unit is here",
				"NegSem_050202_Uniqueness_008.ttcn:::26:::E:::Definition with identifier `repeatedIdentifier' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_008.ttcn:::25:::E:::Previous definition with identifier `repeatedIdentifier' in higher scope unit is here",
				"NegSem_050202_Uniqueness_009.ttcn:::30:::E:::Definition with identifier `repeatedIdentifier' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_009.ttcn:::22:::E:::Previous definition with identifier `repeatedIdentifier' in higher scope unit is here",
				"NegSem_050202_Uniqueness_010.ttcn:::28:::E:::Definition with identifier `repeatedIdentifier' is not unique in the scope hierarchy",
				"NegSem_050202_Uniqueness_010.ttcn:::22:::E:::Previous definition with identifier `repeatedIdentifier' in higher scope unit is here",
				"NegSem_050202_Uniqueness_011.ttcn:::26:::W:::Definition with name `NegSem_050202_Uniqueness_011' hides a module identifier",
				"NegSem_050202_Uniqueness_012.ttcn:::29:::W:::Definition with name `NegSem_050202_Uniqueness_012_import' hides a module identifier"
			)	
		);
	}
}
