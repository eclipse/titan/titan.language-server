/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates;

import java.util.List;

import tests.TestBase;

public class TemplateRestrictionsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\15_templates\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1508_template_restrictions",
			List.of(
				"NegSem_1508_TemplateRestrictions_002.ttcn:::29:::E:::Restriction on template definition does not allow usage of value list match",
				"NegSem_1508_TemplateRestrictions_004.ttcn:::29:::E:::Restriction on template definition does not allow usage of value range match",
				"NegSem_1508_TemplateRestrictions_004.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_005.ttcn:::29:::E:::Restriction on template definition does not allow usage of superset match",
				"NegSem_1508_TemplateRestrictions_005.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_006.ttcn:::29:::E:::Restriction on template definition does not allow usage of subset match",
				"NegSem_1508_TemplateRestrictions_006.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_007.ttcn:::29:::E:::Restriction on template definition does not allow usage of character string pattern",
				"NegSem_1508_TemplateRestrictions_007.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_008.ttcn:::29:::E:::Restriction on template definition does not allow usage of any value",
				"NegSem_1508_TemplateRestrictions_009.ttcn:::29:::E:::Restriction on template definition does not allow usage of any or omit",
				"NegSem_1508_TemplateRestrictions_010.ttcn:::29:::E:::permutation match cannot be used for `set of' type `@NegSem_1508_TemplateRestrictions_010.ExampleType.a'",
				"NegSem_1508_TemplateRestrictions_010.ttcn:::29:::E:::Restriction on template definition does not allow usage of permutation match",
				"NegSem_1508_TemplateRestrictions_010.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_011.ttcn:::28:::E:::There are more (5) elements in the string than it is allowed by the length restriction (at most 3)",
				"NegSem_1508_TemplateRestrictions_011.ttcn:::28:::E:::Restriction on template definition does not allow usage of length restriction",
				"NegSem_1508_TemplateRestrictions_011.ttcn:::28:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_012.ttcn:::28:::E:::`ifpresent' is not allowed here",
				"NegSem_1508_TemplateRestrictions_012.ttcn:::28:::E:::Restriction on template definition does not allow usage of `ifpresent'",
				"NegSem_1508_TemplateRestrictions_012.ttcn:::28:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_013.ttcn:::28:::E:::Restriction on template definition does not allow usage of complemented list match",
				"NegSem_1508_TemplateRestrictions_013.ttcn:::28:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_015.ttcn:::29:::E:::Restriction on template definition does not allow usage of value list match",
				"NegSem_1508_TemplateRestrictions_017.ttcn:::29:::E:::Restriction on template definition does not allow usage of value range match",
				"NegSem_1508_TemplateRestrictions_017.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_018.ttcn:::29:::E:::Restriction on template definition does not allow usage of superset match",
				"NegSem_1508_TemplateRestrictions_018.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_019.ttcn:::29:::E:::Restriction on template definition does not allow usage of subset match",
				"NegSem_1508_TemplateRestrictions_019.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_020.ttcn:::29:::E:::Restriction on template definition does not allow usage of character string pattern",
				"NegSem_1508_TemplateRestrictions_020.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_021.ttcn:::29:::E:::Restriction on template definition does not allow usage of any value",
				"NegSem_1508_TemplateRestrictions_022.ttcn:::29:::E:::permutation match cannot be used for `set of' type `@NegSem_1508_TemplateRestrictions_022.ExampleType.a'",
				"NegSem_1508_TemplateRestrictions_022.ttcn:::29:::E:::Restriction on template definition does not allow usage of permutation match",
				"NegSem_1508_TemplateRestrictions_022.ttcn:::29:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_023.ttcn:::28:::E:::There are more (5) elements in the string than it is allowed by the length restriction (at most 3)",
				"NegSem_1508_TemplateRestrictions_023.ttcn:::28:::E:::Restriction on template definition does not allow usage of length restriction",
				"NegSem_1508_TemplateRestrictions_023.ttcn:::28:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_024.ttcn:::28:::E:::`ifpresent' is not allowed here",
				"NegSem_1508_TemplateRestrictions_024.ttcn:::28:::E:::Restriction on template definition does not allow usage of `ifpresent'",
				"NegSem_1508_TemplateRestrictions_024.ttcn:::28:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_025.ttcn:::29:::E:::Restriction on template definition does not allow usage of omit value",
				"NegSem_1508_TemplateRestrictions_026.ttcn:::29:::E:::Restriction on template definition does not allow usage of omit value",
				"NegSem_1508_TemplateRestrictions_029.ttcn:::42:::E:::Restriction on template does not allow usage of any value",
				"NegSem_1508_TemplateRestrictions_030.ttcn:::43:::E:::Restriction on template does not allow usage of any value",
				"NegSem_1508_TemplateRestrictions_031.ttcn:::40:::E:::Restriction on template does not allow usage of omit value",
				"NegSem_1508_TemplateRestrictions_031.ttcn:::36:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_032.ttcn:::42:::E:::Restriction on template does not allow usage of any or omit",
				"NegSem_1508_TemplateRestrictions_033.ttcn:::42:::E:::Restriction on template does not allow usage of any or omit",
				"NegSem_1508_TemplateRestrictions_034.ttcn:::38:::E:::Restriction on template does not allow usage of omit value",
				"NegSem_1508_TemplateRestrictions_034.ttcn:::34:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_035.ttcn:::38:::E:::Restriction on template does not allow usage of omit value",
				"NegSem_1508_TemplateRestrictions_035.ttcn:::34:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_036.ttcn:::32:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_036.ttcn:::31:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_036.ttcn:::31:::E:::Formal parameter with template restriction `present' not allowed here",
				"NegSem_1508_TemplateRestrictions_037.ttcn:::33:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_037.ttcn:::32:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_037.ttcn:::32:::E:::Formal parameter with template restriction `present' not allowed here",
				"NegSem_1508_TemplateRestrictions_038.ttcn:::32:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_038.ttcn:::31:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_038.ttcn:::31:::E:::Formal parameter with template restriction `present' not allowed here",
				"NegSem_1508_TemplateRestrictions_039.ttcn:::32:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_039.ttcn:::31:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_039.ttcn:::31:::E:::Formal parameter without template restriction not allowed here",
				"NegSem_1508_TemplateRestrictions_040.ttcn:::32:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_040.ttcn:::31:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_040.ttcn:::31:::E:::Formal parameter with template restriction `present' not allowed here",
				"NegSem_1508_TemplateRestrictions_041.ttcn:::33:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_041.ttcn:::32:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_041.ttcn:::32:::E:::Formal parameter without template restriction not allowed here",
				"NegSem_1508_TemplateRestrictions_042.ttcn:::33:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_042.ttcn:::32:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_042.ttcn:::32:::E:::Formal parameter with template restriction `present' not allowed here",
				"NegSem_1508_TemplateRestrictions_043.ttcn:::33:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"NegSem_1508_TemplateRestrictions_043.ttcn:::32:::E:::Referenced template parameter is here",
				"NegSem_1508_TemplateRestrictions_043.ttcn:::32:::E:::Formal parameter without template restriction not allowed here",
				"NegSem_1508_TemplateRestrictions_044.ttcn:::52:::E:::The restriction of parameter is not the same or more restrictive as in base template `@NegSem_1508_TemplateRestrictions_044.m_baseTemplate'",
				"NegSem_1508_TemplateRestrictions_045.ttcn:::52:::E:::The restriction of parameter is not the same or more restrictive as in base template `@NegSem_1508_TemplateRestrictions_045.m_baseTemplate'",
				"NegSem_1508_TemplateRestrictions_046.ttcn:::52:::E:::The restriction of parameter is not the same or more restrictive as in base template `@NegSem_1508_TemplateRestrictions_046.m_baseTemplate'",
				"NegSem_1508_TemplateRestrictions_047.ttcn:::52:::E:::The restriction of parameter is not the same or more restrictive as in base template `@NegSem_1508_TemplateRestrictions_047.m_baseTemplate'",
				"NegSem_1508_TemplateRestrictions_048.ttcn:::52:::E:::The restriction of parameter is not the same or more restrictive as in base template `@NegSem_1508_TemplateRestrictions_048.m_baseTemplate'",
				"NegSem_1508_TemplateRestrictions_049.ttcn:::52:::E:::The restriction of parameter is not the same or more restrictive as in base template `@NegSem_1508_TemplateRestrictions_049.m_baseTemplate'",
				"NegSem_1508_TemplateRestrictions_050.ttcn:::58:::E:::Referenced template is here",
				"NegSem_1508_TemplateRestrictions_051.ttcn:::54:::E:::Referenced template is here",
				"Sem_1508_TemplateRestrictions_045.ttcn:::22:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"Sem_1508_TemplateRestrictions_045.ttcn:::21:::E:::Referenced template parameter is here",
				"Sem_1508_TemplateRestrictions_045.ttcn:::21:::E:::Formal parameter without template restriction not allowed here",
				"Sem_1508_TemplateRestrictions_046.ttcn:::24:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"Sem_1508_TemplateRestrictions_046.ttcn:::23:::E:::Referenced template parameter is here",
				"Sem_1508_TemplateRestrictions_046.ttcn:::23:::E:::Formal parameter with template restriction `present' not allowed here",
				"Sem_1508_TemplateRestrictions_048.ttcn:::22:::W:::Inadequate restriction on the referenced template parameter `MyintTemplate', this may cause a dynamic test case error at runtime",
				"Sem_1508_TemplateRestrictions_048.ttcn:::21:::E:::Referenced template parameter is here",
				"Sem_1508_TemplateRestrictions_048.ttcn:::21:::E:::Formal parameter without template restriction not allowed here"
			)
		);
	}
}
