/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.timers;

import java.util.List;

import tests.TestBase;

public class TimersTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\12_timers\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("12_toplevel",
			List.of(
				"NegSyn_12_toplevel_timer_002.ttcn:::29:::E:::" + REGEXPREFIX + ".*Syntax error.*",
				"NegSyn_12_toplevel_timer_004.ttcn:::29:::E:::" + REGEXPREFIX + ".*Syntax error.*",
				"NegSyn_12_toplevel_timer_006.ttcn:::29:::E:::" + REGEXPREFIX + ".*Syntax error.*",
				"NegSyn_12_toplevel_timer_007.ttcn:::29:::E:::" + REGEXPREFIX + ".*Syntax error.*"
			));
	}
}
