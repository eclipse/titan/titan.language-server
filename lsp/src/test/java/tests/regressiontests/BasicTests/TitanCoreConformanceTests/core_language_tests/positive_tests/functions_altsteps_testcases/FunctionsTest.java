/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.functions_altsteps_testcases;

import java.util.List;

import tests.TestBase;

public class FunctionsTest extends TestBase {
	@Override
	public String getRoot() {
		return "conformance_test\\core_language_tests\\positive_tests\\16_functions_altsteps_testcases\\";
	}

	@Override
	protected void initMarkers() {
		addSubdirectory("1601_functions",
			List.of(
				"160102_predefined_functions\\NegSem_160102_predefined_functions_039.ttcn:::31:::E:::" + REGEXPREFIX + ".*Syntax error.*",
				"160102_predefined_functions\\NegSem_160102_predefined_functions_040.ttcn:::31:::E:::" + REGEXPREFIX + ".*Syntax error.*",
				"160102_predefined_functions\\Sem_160102_predefined_functions_093.ttcn:::31:::E:::" + REGEXPREFIX + ".*Syntax error.*",
				"160102_predefined_functions\\Sem_160102_predefined_functions_094.ttcn:::31:::E:::" + REGEXPREFIX + ".*Syntax error.*"
			)
		);
	}
}
