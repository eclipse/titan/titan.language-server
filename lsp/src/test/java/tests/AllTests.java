/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import tests.regressiontests.BasicTests.TitanCoreConformanceTests.OOP_tests.positive_tests.classes.ClassTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.OOP_tests.positive_tests.objects.ObjectTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.basic_language_elements.BasicLanguageElementsTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.constants.ConstantsTest;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.expressions.ExpressionsTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.functions_altsteps_testcases.FunctionsAltstepsTestcasesTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.messages.MessagesTest;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.modules.ModulesTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.procedure_signatures.ProcedureSignaturesTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.templates.TemplateTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.test_configurations.TestConfigurationsTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.timers.TimersTest;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.types_and_values.TypesAndValuesTests;
import tests.regressiontests.BasicTests.TitanCoreConformanceTests.core_language_tests.positive_tests.variables.VariablesTests;

@RunWith(Suite.class)
@SuiteClasses({
//	TitanCoreFunctionTest.class,
	
	// conformance_test\OOP_tests\positive_tests
	ClassTests.class,
	ObjectTests.class,
	
	// conformance_test\core_language_tests\positive_tests
	BasicLanguageElementsTests.class,
	TypesAndValuesTests.class,
	ExpressionsTests.class,
	ModulesTests.class,
	TestConfigurationsTests.class,
	ConstantsTest.class,
	VariablesTests.class,
	TimersTest.class,
	MessagesTest.class,
	ProcedureSignaturesTests.class,
	TemplateTests.class,
	FunctionsAltstepsTestcasesTests.class
})
public class AllTests {

}
