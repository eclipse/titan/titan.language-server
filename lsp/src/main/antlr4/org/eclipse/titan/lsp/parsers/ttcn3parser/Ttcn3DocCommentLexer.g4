lexer grammar Ttcn3DocCommentLexer;

/*
 ******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************
*/

/*
 * Lexer grammar for documentation comments (ETSI ES 201 873-10 V4.5.1)
 *
 * author Miklos Magyari
 */
 
 @header {
import java.io.File;
import org.eclipse.titan.lsp.parsers.TITANMarker;
import org.eclipse.titan.lsp.AST.*;
}

@members {
	private File actualFile;
			
	public void setActualFile(final File actualFile) {
		this.actualFile = actualFile;
	}
}

BLOCK_BEGIN:        '/**' STAR*;
BLOCK_END:          '*/' ;

WS:	                [ \t]+ ;
AUTHOR:             '@author';
CONFIG:             '@config';
DESC:               '@desc';
EXCEPTION:          '@exception';
MEMBER:             '@member';
PARAM:              '@param';
PRIORITY:           '@priority';
PURPOSE:            '@purpose';
REFERENCE:          '@reference';
REMARK:             '@remark';
REQUIREMENT:        '@requirement';
RETURN:             '@return';
SEE:                '@see';
SINCE:              '@since';
STATUS:             '@status';
URL:                '@url';
VERDICT:            '@verdict';
VERSION:            '@version';

// non-standard tags
CATEGORY:			'@category';

STAR:               '*' ;
NEWLINE:            (WS* STAR* WS*)? '\r'? '\n' (WS? (STAR { _input.LA(1) != '/'}?)+)?;

LINE:               WS* LINE_BEGIN STAR* WS* ;

IDENTIFIER:         [A-Za-z][A-Za-z0-9_.\-]* ;

FREETEXT:           FREETEXTCHAR+ ;
fragment FREETEXTCHAR:  [\u0021-\u0029\u002b-\u002e\u0030-\u003f\u0041-\u007e\u00a1-\u00ac\u00ae-\u2fff]
                        | '@@'
                        | '*' { _input.LA(1) != '/'}? 
                        | '/' { _input.LA(2) != '*' || (_input.LA(1) != '*' && _input.LA(1) != '/') }?     
                        ;

fragment LINE_BEGIN:    '//*' STAR*;

BROKEN_AT:			'@' { /* createMarker("Character '@' should be escaped like '@@'"); */ } -> skip;
