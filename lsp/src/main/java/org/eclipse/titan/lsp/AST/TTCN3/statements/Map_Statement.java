/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.PortReference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ActualParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.types.PortTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.PortTypeBody.PortType_type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * */
public final class Map_Statement extends Statement {
	public static final String BOTHENDSARETESTCOMPONENTPORTS = "Both endpoints of the mapping are test component ports";
	public static final String BOTHENDSARESYSTEMPORTS = "Both endpoints of the mapping are system ports";
	public static final String INCONSISTENTMAPPING1 = "The mapping between test component port type `{0}'' and system port type `{1}''"
			+ " is not consistent";
	public static final String INCONSISTENTMAPPING2 = "The mapping between system port type `{0}'' and test component port type `{1}''"
			+ " is not consistent";
	public static final String INCONSISTENTMAPPING3 = "The mapping between port types `{0}'' and `{1}'' is not consistent";
	public static final String INTERNALPORT = "Port type `{0}'' was marked as `internal''";
	public static final String COMPONENTTYPEERROR = "Cannot determine the type of the component in the {0} parameter. The port translation will not work.";
	public static final String SENDRECEIVEERROR = "Port type `{0}'' cannot send or receive from system port type `{1}''.";
	public static final String TRANSLATIONMODE1 = "This mapping is not done in translation mode";
	public static final String TRANSLATIONMODE2 = TRANSLATIONMODE1 + ", because the {0} endpoint is unknown";
	public static final String SYSTEMCOMPONENTERROR = "Cannot determine system component in `{0}'' operation with `param'' clause";

	private static final String FULLNAMEPART1 = ".componentreference1";
	private static final String FULLNAMEPART2 = ".portreference1";
	private static final String FULLNAMEPART3 = ".componentreference2";
	private static final String FULLNAMEPART4 = ".portreference2";
	private static final String FULLNAMEPART5 = ".parsedParameterList";
	private static final String STATEMENT_NAME = "map";

	private final Value componentReference1;
	private final PortReference portReference1;
	private final Value componentReference2;
	private final PortReference portReference2;
	private boolean translate = false;

	private final ParsedActualParameters parsedParameterList;
	private ActualParameterList actualParameterList; // generated
	private FormalParameterList formalParList; // not owned

	public Map_Statement(final Value componentReference1, final PortReference portReference1, final Value componentReference2,
			final PortReference portReference2, final ParsedActualParameters parameters) {
		this.componentReference1 = componentReference1;
		this.portReference1 = portReference1;
		this.componentReference2 = componentReference2;
		this.portReference2 = portReference2;
		this.parsedParameterList = parameters;

		if (componentReference1 != null) {
			componentReference1.setFullNameParent(this);
		}
		if (portReference1 != null) {
			portReference1.setFullNameParent(this);
		}
		if (componentReference2 != null) {
			componentReference2.setFullNameParent(this);
		}
		if (portReference2 != null) {
			portReference2.setFullNameParent(this);
		}
		if (parameters != null) {
			parameters.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Statement_type getType() {
		return Statement_type.S_MAP;
	}

	@Override
	/** {@inheritDoc} */
	public String getStatementName() {
		return STATEMENT_NAME;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (componentReference1 == child) {
			return builder.append(FULLNAMEPART1);
		} else if (portReference1 == child) {
			return builder.append(FULLNAMEPART2);
		} else if (componentReference2 == child) {
			return builder.append(FULLNAMEPART3);
		} else if (portReference2 == child) {
			return builder.append(FULLNAMEPART4);
		} else if (parsedParameterList == child) {
			return builder.append(FULLNAMEPART5);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (componentReference1 != null) {
			componentReference1.setMyScope(scope);
		}
		if (portReference1 != null) {
			portReference1.setSubreferencesScope(scope);
		}
		if (componentReference2 != null) {
			componentReference2.setMyScope(scope);
		}
		if (portReference2 != null) {
			portReference2.setSubreferencesScope(scope);
		}
		if (parsedParameterList != null) {
			parsedParameterList.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		IType portType1;
		IType portType2;
		PortTypeBody body1 = null;
		PortTypeBody body2 = null;
		boolean cref1IsTestcomponents = false;
		boolean cref1IsSystem = false;
		boolean cref2IsTestcomponent = false;
		boolean cref2IsSystem = false;

		portType1 = Port_Utility.checkConnectionEndpoint(timestamp, this, componentReference1, portReference1, true);
		if (portType1 != null) {
			body1 = ((Port_Type) portType1).getPortBody();
			if (body1.isInternal()) {
				componentReference1.getLocation().reportSemanticWarning(
						MessageFormat.format(INTERNALPORT, portType1.getTypename()));
			}
			// sets the referenced assignment of this reference
			portReference1.getRefdAssignment(timestamp, false);
		}

		if (componentReference1 != null) {
			final IValue configReference1 = componentReference1.getValueRefdLast(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null);
			if (Value_type.EXPRESSION_VALUE.equals(configReference1.getValuetype())) {
				switch (((Expression_Value) configReference1).getOperationType()) {
				case MTC_COMPONENT_OPERATION:
					cref1IsTestcomponents = true;
					break;
				case SELF_COMPONENT_OPERATION:
					cref1IsTestcomponents = true;
					break;
				case COMPONENT_CREATE_OPERATION:
					cref1IsTestcomponents = true;
					break;
				case SYSTEM_COMPONENT_OPERATION:
					cref1IsSystem = true;
					break;
				default:
					break;
				}
			}
		}

		portType2 = Port_Utility.checkConnectionEndpoint(timestamp, this, componentReference2, portReference2, true);
		if (portType2 != null) {
			body2 = ((Port_Type) portType2).getPortBody();
			if (body2.isInternal()) {
				componentReference2.getLocation().reportSemanticWarning(
						MessageFormat.format(INTERNALPORT, portType2.getTypename()));
			}
			// sets the referenced assignment of this reference
			portReference2.getRefdAssignment(timestamp, false);
		}

		if (componentReference2 != null) {
			final IValue configReference2 = componentReference2.getValueRefdLast(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null);
			if (Value_type.EXPRESSION_VALUE.equals(configReference2.getValuetype())) {
				switch (((Expression_Value) configReference2).getOperationType()) {
				case MTC_COMPONENT_OPERATION:
					cref2IsTestcomponent = true;
					break;
				case SELF_COMPONENT_OPERATION:
					cref2IsTestcomponent = true;
					break;
				case COMPONENT_CREATE_OPERATION:
					cref2IsTestcomponent = true;
					break;
				case SYSTEM_COMPONENT_OPERATION:
					cref2IsSystem = true;
					break;
				default:
					break;
				}
			}
		}

		lastTimeChecked = timestamp;

		if (cref1IsTestcomponents && cref2IsTestcomponent) {
			location.reportSemanticError(BOTHENDSARETESTCOMPONENTPORTS);
			return;
		}

		if (cref1IsSystem && cref2IsSystem) {
			location.reportSemanticError(BOTHENDSARESYSTEMPORTS);
			return;
		}

		if (body1 == null || body2 == null) {
			if (body1 != null && body1.isInternal()) {
				portReference1.getLocation().reportSemanticWarning(MessageFormat.format(INTERNALPORT, portType1.getTypename()));
			}
			if (body2 != null && body2.isInternal()) {
				portReference2.getLocation().reportSemanticWarning(MessageFormat.format(INTERNALPORT, portType2.getTypename()));
			}

			if ((body1 != null && !body1.isLegacy() && body1.getPortType() == PortType_type.PT_USER) ||
					(body2 != null && !body2.isLegacy() && body2.getPortType() == PortType_type.PT_USER)) {
				getLocation().reportSemanticWarning(MessageFormat.format(TRANSLATIONMODE2, body1 != null ? "second" : "first"));
			}

			check_map_params(timestamp, cref1IsSystem ? body1 : (cref2IsSystem ? body2 : null));

			return;
		}

		if (cref1IsTestcomponents || cref2IsSystem) {
			translate = !body1.isLegacy() && body1.isTranslate(body2);
			if (!translate && !body1.isMappable(timestamp, body2)) {
				location.reportSemanticError(MessageFormat.format(INCONSISTENTMAPPING1, portType1.getTypename(),
						portType2.getTypename()));
				body1.reportMappingErrors(timestamp, body2);
			}
			if (!translate && !body1.mapCanReceiveOrSend(body2)) {
				getLocation().reportSemanticWarning(MessageFormat.format(SENDRECEIVEERROR, portType1.getTypename(), portType2.getTypename()));
			}
		} else if (cref2IsTestcomponent || cref1IsSystem) {
			translate = !body2.isLegacy() && body2.isTranslate(body1);
			if (!translate && !body2.isMappable(timestamp, body1)) {
				location.reportSemanticError(MessageFormat.format(INCONSISTENTMAPPING2, portType1.getTypename(),
						portType2.getTypename()));
				body2.reportMappingErrors(timestamp, body1);
			}
			if (!translate && !body2.mapCanReceiveOrSend(body1)) {
				getLocation().reportSemanticWarning(MessageFormat.format(SENDRECEIVEERROR, portType2.getTypename(), portType1.getTypename()));
			}
		} else {
			// we don't know which one is the system port
			final boolean firstMappedToSecond = !body1.isLegacy() && body1.isTranslate(body2);
			final boolean secondMappedToFirst = !body2.isLegacy() && body2.isTranslate(body1);
			translate = firstMappedToSecond || secondMappedToFirst;

			if (!translate && !body1.isMappable(timestamp, body2) && !body2.isMappable(timestamp, body1)) {
				location.reportSemanticError(MessageFormat.format(INCONSISTENTMAPPING3, portType1.getTypename(),
						portType2.getTypename()));
			}
		}

		if (!translate) {
			if (body1.isInternal()) {
				portReference1.getLocation().reportSemanticWarning(MessageFormat.format(INTERNALPORT, portType1.getTypename()));
			}
			if (body2.isInternal()) {
				portReference2.getLocation().reportSemanticWarning(MessageFormat.format(INTERNALPORT, portType2.getTypename()));
			}
			if ((!body1.isLegacy() && body1.getPortType() == PortType_type.PT_USER) ||
					(!body2.isLegacy() && body2.getPortType() == PortType_type.PT_USER)) {
				getLocation().reportSemanticWarning(TRANSLATIONMODE1);
			}
		} else {
			if (componentReference1.getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE) != null) {
				componentReference1.getLocation().reportSemanticWarning(MessageFormat.format(COMPONENTTYPEERROR, "first"));
			}
			if (componentReference2.getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE) != null) {
				componentReference2.getLocation().reportSemanticWarning(MessageFormat.format(COMPONENTTYPEERROR, "second"));
			}
		}

		check_map_params(timestamp, cref1IsSystem ? body1 : (cref2IsSystem ? body2 : null));
	}

	private void check_map_params(final CompilationTimeStamp timestamp, final PortTypeBody system_port) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (parsedParameterList == null) {
			return;
		}

		if (system_port != null) {
			formalParList = system_port.getMapParameters();
		} else {
			getLocation().reportSemanticError(MessageFormat.format(SYSTEMCOMPONENTERROR, STATEMENT_NAME));
		}

		if (formalParList != null) {
			actualParameterList = new ActualParameterList();
			if (formalParList.checkActualParameterList(timestamp, parsedParameterList, actualParameterList)) {
				actualParameterList = null;
			} else {
				actualParameterList.setFullNameParent(this);
				actualParameterList.setMyScope(getMyScope());
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (componentReference1 != null) {
			componentReference1.updateSyntax(reparser, false);
			reparser.updateLocation(componentReference1.getLocation());
		}

		if (portReference1 != null) {
			portReference1.updateSyntax(reparser, false);
			reparser.updateLocation(portReference1.getLocation());
		}

		if (componentReference2 != null) {
			componentReference2.updateSyntax(reparser, false);
			reparser.updateLocation(componentReference2.getLocation());
		}

		if (portReference2 != null) {
			portReference2.updateSyntax(reparser, false);
			reparser.updateLocation(portReference2.getLocation());
		}

		if (parsedParameterList != null) {
			parsedParameterList.updateSyntax(reparser, false);
			reparser.updateLocation(parsedParameterList.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (componentReference1 != null) {
			componentReference1.findReferences(referenceFinder, foundIdentifiers);
		}
		if (portReference1 != null) {
			portReference1.findReferences(referenceFinder, foundIdentifiers);
		}
		if (componentReference2 != null) {
			componentReference2.findReferences(referenceFinder, foundIdentifiers);
		}
		if (portReference2 != null) {
			portReference2.findReferences(referenceFinder, foundIdentifiers);
		}
		if (parsedParameterList != null) {
			parsedParameterList.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (componentReference1 != null && !componentReference1.accept(v)) {
			return false;
		}
		if (portReference1 != null && !portReference1.accept(v)) {
			return false;
		}
		if (componentReference2 != null && !componentReference2.accept(v)) {
			return false;
		}
		if (portReference2 != null && !portReference2.accept(v)) {
			return false;
		}
		if (parsedParameterList != null && !parsedParameterList.accept(v)) {
			return false;
		}
		return true;
	}
}
