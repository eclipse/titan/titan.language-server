/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1;

import org.eclipse.titan.lsp.AST.IGovernor;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.Setting;
import org.eclipse.titan.lsp.AST.ASN1.Object.FieldSpecifications;
import org.eclipse.titan.lsp.AST.ASN1.Object.ObjectClassSyntax_root;
import org.eclipse.titan.lsp.AST.ASN1.Object.ObjectClass_Definition;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Class to represent ObjectClass.
 *
 * @author Kristof Szabados
 */
public abstract class ObjectClass extends Setting implements ICancelBuild, IGovernor {

	@Override
	/** {@inheritDoc} */
	public final Setting_type getSettingtype() {
		return Setting_type.S_OC;
	}

	public abstract ObjectClass_Definition getRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain);

	/** @return a new instance of this reference */
	public abstract ObjectClass newInstance();

	/**
	 * Does the semantic checking of the provided object with this setting.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param object
	 *                the object to check.
	 * */
	public abstract void checkThisObject(final CompilationTimeStamp timestamp, ASN1Object object);

	/**
	 * Checks and returns the syntax of the object class.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return the root of the syntax of this ObjectClass
	 */
	public abstract ObjectClassSyntax_root getObjectClassSyntax(final CompilationTimeStamp timestamp);

	/** @return the field specifications of this ObjectClass */
	public abstract FieldSpecifications getFieldSpecifications();
}
