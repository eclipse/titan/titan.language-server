/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a template that holds a hexstring pattern.
 *
 * @author Kristof Szabados
 * */
public final class HexString_Pattern_Template extends TTCN3Template {

	private final String pattern;

	public HexString_Pattern_Template(final String pattern) {
		this.pattern = pattern;
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.HSTR_PATTERN;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder("'");
		builder.append(pattern);
		builder.append("'H");
		return addToStringRepresentation(builder).toString();
	}

	public boolean containsAnyornoneSymbol() {
		return pattern.indexOf(ASTERIX) > -1;
	}

	public int getMinLengthOfPattern() {
		int starCount = 0;
		int index = pattern.indexOf(ASTERIX, 0);
		while (index != -1) {
			++index;
			++starCount;
			index = pattern.indexOf(ASTERIX, index);
		}

		return pattern.length() - starCount;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (getIsErroneous(timestamp)) {
			return Type_type.TYPE_UNDEFINED;
		}

		return Type_type.TYPE_HEXSTRING;
	}

	@Override
	/** {@inheritDoc} */
	protected void checkTemplateSpecificLengthRestriction(final CompilationTimeStamp timestamp, final Type_type typeType) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (Type_type.TYPE_HEXSTRING.equals(typeType)) {
			final boolean hasAnyOrNone = containsAnyornoneSymbol();
			lengthRestriction.checkNofElements(timestamp, getMinLengthOfPattern(), hasAnyOrNone, false, hasAnyOrNone, this);
		}
	}
}
