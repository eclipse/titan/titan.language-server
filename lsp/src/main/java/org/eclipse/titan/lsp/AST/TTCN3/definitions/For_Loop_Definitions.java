/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignments;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IOutlineElement;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.IAppendableSyntax;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.common.utils.IteratorFactory;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The For_Loop_Definitions class represents the initial definitions of for loops.
 * <p>
 * These definitions differ from the simple ones, in that the order of their
 * declaration matters, and the can hide higher level definitions. And still
 * they are not Definition_Statements, as there is no exact statement block they
 * could belong to.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class For_Loop_Definitions extends Assignments implements ILocateableNode, IIncrementallyUpdatable {
	/** The list of definitions contained in this scope. */
	private final List<Definition> definitions;

	/**
	 * A hashmap of definitions, used to find multiple declarations, and to speed up searches.
	 */
	private Map<String, Definition> definitionMap;

	/**
	 * Holds the last time when these definitions were checked, or {@code null} if never.
	 */
	private CompilationTimeStamp lastCompilationTimeStamp;

	/**
	 * Holds the last time when the uniqueness of these definitions were
	 * checked, or {@code null} if never.
	 */
	private CompilationTimeStamp lastUniquenessCheckTimeStamp;

	public For_Loop_Definitions() {
		definitions = new CopyOnWriteArrayList<Definition>();
		scopeName = DEFINITIONS;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final Definition definition : definitions) {
			if (definition == child) {
				final Identifier identifier = definition.getIdentifier();

				return builder.append(INamedNode.DOT).append(identifier.getDisplayName());
			}
		}

		return builder;
	}

	@Override
	public int size() {
		return definitions.size();
	}

	@Override
	public Definition get(final int index) {
		return definitions.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		final List<IOutlineElement> outlineDefinitions = new ArrayList<IOutlineElement>();
		// Take care of ordering.
		outlineDefinitions.addAll(definitions);
		Collections.sort(outlineDefinitions, IOutlineElement.getComparator());
		return outlineDefinitions.toArray();
	}

	@Override
	public List<Definition> getVisibleDefinitions() {
		return Collections.unmodifiableList(definitions);
	}

	@Override
	public boolean add(final Assignment def) {
		return addAll(Arrays.asList(def));
	}

	/**
	 * Adds a list of definitions to the list of definitions.
	 * <p>
	 * The scope of the newly added definitions are set to this scope scope here.
	 *
	 * @param definitionList the definitions to be added
	 */
	@Override
	public boolean addAll(final Collection<? extends Assignment> definitionList) {
		if (definitionList == null) {
			return false;
		}
		boolean changed = false;
		for (final Assignment definition : definitionList) {
			if (definition != null && definition.getIdentifier() != null
					&& definition.getIdentifier().getLocation() != null
					&& definition instanceof Definition) {
				definition.setMyScope(this);
				definition.setFullNameParent(this);
				changed |= definitions.add((Definition)definition);
			}
		}
		return changed;
	}

	/**
	 * Checks the uniqueness of the definitions, and also builds a hashmap
	 * of them to speed up further searches.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	protected void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastUniquenessCheckTimeStamp != null && !lastUniquenessCheckTimeStamp.isLess(timestamp)) {
			return;
		}

		lastUniquenessCheckTimeStamp = timestamp;

		if (definitionMap == null) {
			definitionMap = new HashMap<String, Definition>(definitions.size());
		} else {
			definitionMap.clear();
		}

		String definitionName;
		for (final Definition definition : definitions) {
			final Identifier identifier = definition.getIdentifier();
			definitionName = identifier.getName();
			if (definitionMap.containsKey(definitionName)) {
				final Location otherLocation = definitionMap.get(definitionName).getIdentifier().getLocation();
				otherLocation.reportSingularSemanticError(MessageFormat.format(DUPLICATEDEFINITIONFIRST, identifier.getDisplayName()));
				identifier.getLocation().reportSemanticError(
						MessageFormat.format(DUPLICATEDEFINITIONREPEATED, identifier.getDisplayName()));
			} else {
				definitionMap.put(definitionName, definition);
				if (parentScope != null && definition.getLocation() != null) {
					if (parentScope.hasAssignmentWithId(timestamp, identifier)) {
						definition.getLocation().reportSemanticError(
								MessageFormat.format(StatementBlock.HIDINGSCOPEELEMENT, identifier.getDisplayName()));

						final List<ISubReference> subReferences = new ArrayList<ISubReference>();
						subReferences.add(new FieldSubReference(identifier));
						final Reference reference = new Reference(null, subReferences);
						final Assignment assignment = parentScope.getAssBySRef(timestamp, reference);
						if (assignment != null && assignment.getLocation() != null) {
							assignment.getLocation().reportSingularSemanticError(
									MessageFormat.format(StatementBlock.HIDDENSCOPEELEMENT,
											identifier.getDisplayName()));
						}
					} else if (parentScope.isValidModuleId(identifier)) {
						definition.getLocation().reportSemanticWarning(
								MessageFormat.format(StatementBlock.HIDINGMODULEIDENTIFIER,
										identifier.getDisplayName()));
					}
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;
		lastUniquenessCheckTimeStamp = timestamp;

		if (definitionMap == null) {
			definitionMap = new HashMap<String, Definition>(definitions.size());
		} else {
			definitionMap.clear();
		}

		String definitionName;
		Identifier identifier;
		for (final Definition definition : definitions) {
			identifier = definition.getIdentifier();
			definitionName = identifier.getName();
			if (definitionMap.containsKey(definitionName)) {
				final Location otherLocation = definitionMap.get(definitionName).getIdentifier().getLocation();
				otherLocation.reportSingularSemanticError(MessageFormat.format(DUPLICATEDEFINITIONFIRST, identifier.getDisplayName()));
				identifier.getLocation().reportSemanticError(
						MessageFormat.format(DUPLICATEDEFINITIONREPEATED, identifier.getDisplayName()));
			} else {
				definitionMap.put(definitionName, definition);
				if (parentScope != null && definition.getLocation() != null) {
					if (parentScope.hasAssignmentWithId(timestamp, identifier)) {
						definition.getLocation().reportSemanticError(
								MessageFormat.format(StatementBlock.HIDINGSCOPEELEMENT, identifier.getDisplayName()));

						final List<ISubReference> subReferences = new ArrayList<ISubReference>();
						subReferences.add(new FieldSubReference(identifier));
						final Reference reference = new Reference(null, subReferences);
						final Assignment assignment = parentScope.getAssBySRef(timestamp, reference);
						if (assignment != null && assignment.getLocation() != null) {
							assignment.getLocation().reportSingularSemanticError(
									MessageFormat.format(StatementBlock.HIDDENSCOPEELEMENT,
											identifier.getDisplayName()));
						}
					} else if (parentScope.isValidModuleId(identifier)) {
						definition.getLocation().reportSemanticWarning(
								MessageFormat.format(StatementBlock.HIDINGMODULEIDENTIFIER,
										identifier.getDisplayName()));
					}
				}
			}

			definition.check(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (isBuildCancelled()) {
			return;
		}

		for (final Definition definition : definitions) {
			definition.postCheck();
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		if (reference.getModuleIdentifier() != null) {
			return getModuleScope().getAssBySRef(timestamp, reference);
		}

		final Identifier identifier = reference.getId();
		if (identifier == null) {
			return getModuleScope().getAssBySRef(timestamp, reference);
		}

		if (lastUniquenessCheckTimeStamp == null) {
			checkUniqueness(timestamp);
		}

		final Definition result = definitionMap.get(identifier.getName());
		if (result != null) {
			return result;
		}

		return getParentScope().getAssBySRef(timestamp, reference);
	}

	/**
	 * Searches the definitions for one with a given Identifier.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param id the identifier used to find the definition
	 * @return the definition if found, or null otherwise
	 */
	@Override
	public Definition getLocalAssignmentByID(final CompilationTimeStamp timestamp, final Identifier id) {
		if (lastUniquenessCheckTimeStamp == null) {
			checkUniqueness(timestamp);
		}
		return definitionMap.get(id.getName());
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasLocalAssignmentWithID(final CompilationTimeStamp timestamp, final Identifier identifier) {
		if (lastUniquenessCheckTimeStamp == null) {
			checkUniqueness(timestamp);
		}
		return definitionMap.containsKey(identifier.getName());
	}
	
	/**
	 * Handles the incremental parsing of this list of definitions.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged {@code true} if the location contains the damaged area,
	 *                {@code false} if only its' location needs to be updated.
	 */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (!isDamaged) {
			// handle the simple case quickly
			for (final Definition temp : definitions) {
				final Location temporalLocation = temp.getLocation();
				final Location cumulativeLocation = temp.getCumulativeDefinitionLocation();
				if (reparser.isAffected(temporalLocation)) {
					temp.updateSyntax(reparser, false);

					if(!temporalLocation.equals(cumulativeLocation)) {
						reparser.updateLocation(cumulativeLocation);
					}
					reparser.updateLocation(temporalLocation);
				}
			}

			return;
		}

		// calculate damaged region
		int result = 0;

		lastCompilationTimeStamp = null;
		boolean enveloped = false;
		int nofDamaged = 0;
		Position leftBoundary = location.getStartPosition();
		Position rightBoundary = location.getEndPosition();
		final Position damageOffset = reparser.getDamageStartPosition();
		IAppendableSyntax lastAppendableBeforeChange = null;
		IAppendableSyntax lastPrependableBeforeChange = null;

		for (int i = 0, size = definitions.size(); i < size && !enveloped; i++) {
			final Definition temp = definitions.get(i);
			final Location tempLocation = temp.getLocation();
			final Location cumulativeLocation = temp.getCumulativeDefinitionLocation();
			if (tempLocation.equals(cumulativeLocation) && reparser.envelopsDamage(cumulativeLocation)) {
				enveloped = true;
				leftBoundary = cumulativeLocation.getStartPosition();
				rightBoundary = cumulativeLocation.getEndPosition();
			} else if (reparser.isDamaged(cumulativeLocation)) {
				nofDamaged++;
				if (reparser.getDamageStartPosition().equals(cumulativeLocation.getEndPosition())) {
					lastAppendableBeforeChange = temp;
				} else if (reparser.getDamageEndPosition().equals(cumulativeLocation.getStartPosition())) {
					lastPrependableBeforeChange = temp;
				}
			} else {
				if (cumulativeLocation.getEndPosition().before(damageOffset) && cumulativeLocation.getEndPosition().after(leftBoundary)) {
					leftBoundary = cumulativeLocation.getEndPosition();
					lastAppendableBeforeChange = temp;
				}
				if (cumulativeLocation.getStartPosition().afterOrEquals(damageOffset) && cumulativeLocation.getStartPosition().before(rightBoundary)) {
					rightBoundary = cumulativeLocation.getStartPosition();
					lastPrependableBeforeChange = temp;
				}
			}
		}

		// extend the reparser to the calculated values if the damage
		// was not enveloped
		if (!enveloped && isDamaged) {
			reparser.extendDamagedRegion(leftBoundary, rightBoundary);

			// if there is an element that is right now being
			// extended we should add it to the damaged domain as
			// the extension might be correct
			if (lastAppendableBeforeChange != null) {
				final boolean isBeingExtended = reparser.startsWithFollow(lastAppendableBeforeChange.getPossibleExtensionStarterTokens());
				if (isBeingExtended) {
					leftBoundary = lastAppendableBeforeChange.getLocation().getStartPosition();
					nofDamaged++;
					enveloped = false;
					reparser.extendDamagedRegion(leftBoundary, rightBoundary);
				}
			}

			if (lastPrependableBeforeChange != null) {
				final List<Integer> temp = lastPrependableBeforeChange.getPossiblePrefixTokens();

				if (reparser.endsWithToken(temp)) {
					rightBoundary = lastPrependableBeforeChange.getLocation().getEndPosition
							();
					nofDamaged++;
					enveloped = false;
					reparser.extendDamagedRegion(leftBoundary, rightBoundary);
				}
			}

			if (nofDamaged != 0) {
				// remove damaged elements
				removeElementsInRange(reparser);

				lastUniquenessCheckTimeStamp = null;
			}
		}

		// update what is left
		for (final Definition temp : definitions) {
			final Location temporalLocation = temp.getLocation();
			final Location cumulativeLocation = temp.getCumulativeDefinitionLocation();
			if (reparser.isAffected(cumulativeLocation)) {
				try {
					temp.updateSyntax(reparser, enveloped && reparser.envelopsDamage(temporalLocation));
					if (reparser.getNameChanged()) {
						lastUniquenessCheckTimeStamp = null;
						reparser.setNameChanged(false);
					}
				} catch (ReParseException e) {
					if (e.getDepth() == 1) {
						enveloped = false;
						definitions.remove(temp);
						reparser.extendDamagedRegion(cumulativeLocation);
						result = 1;
					} else {
						throw new ReParseException(e);
					}
				}
			}
		}

		if (result == 1) {
			removeElementsInRange(reparser);
			lastUniquenessCheckTimeStamp = null;
		}

		for (final Definition temp : definitions) {
			final Location temporalLocation = temp.getLocation();
			final Location cumulativeLocation = temp.getCumulativeDefinitionLocation();
			if (reparser.isAffected(temporalLocation)) {
				if(!temporalLocation.equals(cumulativeLocation)) {
					reparser.updateLocation(cumulativeLocation);
				}
				reparser.updateLocation(temporalLocation);
			}
		}

		if (!enveloped && reparser.envelopsDamage(location)) {
			// right now this can not be processed efficiently, the
			// whole definition region has to be re-parsed
			throw new ReParseException();
		}

		if (result != 0) {
			lastUniquenessCheckTimeStamp = null;
			throw new ReParseException(result);
		}
	}

	/**
	 * Destroy every element trapped inside the damage radius.
	 * @param reparser the parser doing the incremental parsing.
	 */
	private void removeElementsInRange(final TTCN3ReparseUpdater reparser) {
		final ArrayList<Definition> toBeRemoved = new ArrayList<Definition>();
		for (final Definition temp : definitions) {
			if (reparser.isDamaged(temp.getCumulativeDefinitionLocation())) {
				reparser.extendDamagedRegion(temp.getCumulativeDefinitionLocation());
				toBeRemoved.add(temp);
			}
		}
		definitions.removeAll(toBeRemoved);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		if (definitions == null) {
			return null;
		}
		for (final Definition definition : definitions) {
			if (definition.getLocation().containsPosition(offset)) {
				return definition;
			}
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final Definition definition : definitions) {
			definition.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final Definition definition : definitions) {
			if (!definition.accept(v)) {
				return false;
			}
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}

	@Override
	/** {@inheritDoc} */
	public Iterator<Assignment> iterator() {
		return IteratorFactory.getIterator((List<? extends Assignment>)definitions);
	}
}
