/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.statements.AltGuard.altguard_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The AltGuards class represents the list of branches in a TTCN3
 * altstep/alt/interleave statement.
 *
 * @see AltGuard
 * @see Alt_Statement
 * @see Interleave_Statement
 * @see Def_Altstep
 *
 * @author Kristof Szabados
 * */
public final class AltGuards extends ASTNode implements IIncrementallyUpdatable {
	private static final String FULLNAMEPART = ".alt_guard_";
	private final ArrayList<AltGuard> altGuardsList;

	private boolean hasRepeat = false;
	private String label;
	private boolean isAltstep = false;

	/**
	 * The location of the whole assignment. This location encloses the
	 * assignment fully, as it is used to report errors to.
	 **/
	private Location location = Location.getNullLocation();

	public AltGuards() {
		altGuardsList = new ArrayList<AltGuard>();
	}

	public void setLocation(final Location location) {
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = altGuardsList.size(); i < size; i++) {
			if (altGuardsList.get(i) == child) {
				return builder.append(FULLNAMEPART).append(String.valueOf(i + 1));
			}
		}

		return builder;
	}

	public void addAltGuard(final AltGuard altGuard) {
		if (altGuard != null) {
			altGuardsList.add(altGuard);
			altGuard.setFullNameParent(this);
		}
	}

	/**
	 * @return the number of altguards.
	 * */
	public int getNofAltguards() {
		return altGuardsList.size();
	}

	/**
	 * Returns the altguard at the specified position in this altguard list.
	 *
	 * @param i
	 *                the index of the altguard to return.
	 *
	 * @return the altguard at the index.
	 * */
	public AltGuard getAltguardByIndex(final int i) {
		return altGuardsList.get(i);
	}

	/**
	 * Sets the scope of the guard statement.
	 *
	 * @param scope
	 *                the scope to be set.
	 * */
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		altGuardsList.trimToSize();
		for (final AltGuard guard : altGuardsList) {
			guard.setMyScope(scope);
		}
	}

	public void setMyStatementBlock(final StatementBlock statementBlock, final int index) {
		for (int i = 0, size = altGuardsList.size(); i < size; i++) {
			altGuardsList.get(i).setMyStatementBlock(statementBlock, index);
		}
	}

	public void setMyDefinition(final Definition definition) {
		for (final AltGuard guard : altGuardsList) {
			guard.setMyDefinition(definition);
		}
	}

	public void setMyAltguards(final AltGuards altGuards) {
		for (final AltGuard altGuard : this.altGuardsList) {
			altGuard.setMyAltguards(altGuards);
		}
	}

	/**
	 * Used to tell break and continue statements if they are located with an altstep, a loop or none.
	 *
	 * @param pAltGuards the altguards set only within altguards
	 * @param pLoopStmt the loop statement, set only within loops.
	 * */
	public void setMyLaicStmt(final AltGuards pAltGuards, final Statement pLoopStmt) {
		for (final AltGuard altGuard : altGuardsList) {
			altGuard.setMyLaicStmt(pAltGuards, pLoopStmt);
		}
	}

	public void repeatFound() {
		hasRepeat = true;
	}

	public String getLabel() {
		return label;
	}

	public void setIsAltstep() {
		isAltstep = true;
	}

	public boolean getIsAltstep() {
		return isAltstep;
	}

	/**
	 * Checks whether there is an else branch among the altguards.
	 *
	 * @return true if there is an else branch, false otherwise.
	 * */
	public boolean hasElse() {
		for (final AltGuard guard : altGuardsList) {
			if (guard.getType() == altguard_type.AG_ELSE) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks whether the altguards have a return statement, either directly
	 * or embedded.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return the return status of the altguards.
	 * */
	public StatementBlock.ReturnStatus_type hasReturn(final CompilationTimeStamp timestamp) {
		StatementBlock.ReturnStatus_type result = StatementBlock.ReturnStatus_type.RS_MAYBE;

		for (final AltGuard guard : altGuardsList) {
			switch (guard.hasReturn(timestamp)) {
			case RS_NO:
				if (result == StatementBlock.ReturnStatus_type.RS_YES) {
					return StatementBlock.ReturnStatus_type.RS_MAYBE;
				}

				result = StatementBlock.ReturnStatus_type.RS_NO;
				break;
			case RS_YES:
				if (result == StatementBlock.ReturnStatus_type.RS_NO) {
					return StatementBlock.ReturnStatus_type.RS_MAYBE;
				}

				result = StatementBlock.ReturnStatus_type.RS_YES;
				break;
			default:
				return StatementBlock.ReturnStatus_type.RS_MAYBE;
			}

			if (guard instanceof Else_Altguard) {
				break;
			}
		}

		return result;
	}

	/**
	 * Used when generating code for interleaved statement.
	 * If the block has no receiving statements, then the general code generation can be used
	 *  (which may use blocks).

	 * */
	public boolean hasReceivingStatement() {
		for (int i = 0; i < altGuardsList.size(); i++) {
			if (altGuardsList.get(i).getStatementBlock().hasReceivingStatement(0)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Does the semantic checking of the alt guard list.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		for (final AltGuard guard : altGuardsList) {
			guard.check(timestamp);
		}
	}

	/**
	 * Checks if some statements are allowed in an interleave or not
	 * */
	public void checkAllowedInterleave() {
		if (isBuildCancelled()) {
			return;
		}
		
		for (final AltGuard guard : altGuardsList) {
			guard.checkAllowedInterleave();
		}
	}

	/**
	 * Checks the properties of the statement, that can only be checked
	 * after the semantic check was completely run.
	 */
	public void postCheck() {
		if (isBuildCancelled()) {
			return;
		}
		
		for (final AltGuard guard : altGuardsList) {
			guard.postCheck();
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			boolean enveloped = false;
			int nofDamaged = 0;
			Position leftBoundary = location.getStartPosition();
			Position rightBoundary = location.getEndPosition();
			final Position damageOffset = reparser.getDamageStartPosition();

			for (int i = 0, size = altGuardsList.size(); i < size && !enveloped; i++) {
				final AltGuard altGuard = altGuardsList.get(i);
				final Location temporalLocation = altGuard.getLocation();

				if (reparser.envelopsDamage(temporalLocation)) {
					enveloped = true;
					leftBoundary = temporalLocation.getStartPosition();
					rightBoundary = temporalLocation.getEndPosition();
				} else if (reparser.isDamaged(temporalLocation)) {
					nofDamaged++;
				} else {
					if (temporalLocation.getEndPosition().before(damageOffset) && temporalLocation.getEndPosition().after(leftBoundary)) {
						leftBoundary = temporalLocation.getEndPosition();
					}
					if (temporalLocation.getStartPosition().afterOrEquals(damageOffset) && temporalLocation.getStartPosition().before(rightBoundary)) {
						rightBoundary = temporalLocation.getStartPosition();
					}
				}
			}

			if (nofDamaged != 0) {
				throw new ReParseException();
			}

			for (final Iterator<AltGuard> iterator = altGuardsList.iterator(); iterator.hasNext();) {
				final AltGuard altGuard = iterator.next();
				final Location temporalLocation = altGuard.getLocation();

				if (reparser.isAffectedAppended(temporalLocation)) {
					altGuard.updateSyntax(reparser, enveloped && reparser.envelopsDamage(temporalLocation));
					reparser.updateLocation(altGuard.getLocation());
				}
			}

			return;
		}

		for (final AltGuard guard : altGuardsList) {
			guard.updateSyntax(reparser, false);
			reparser.updateLocation(guard.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (altGuardsList == null) {
			return;
		}

		for (final AltGuard ag : altGuardsList) {
			ag.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (altGuardsList != null) {
			for (final AltGuard ag : altGuardsList) {
				if (!ag.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}
}
