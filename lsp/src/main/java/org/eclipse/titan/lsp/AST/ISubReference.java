/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;

/**
 * Defines the interface of TTCN3 or ASN.1 sub-references (reference parts).
 *
 * @author Kristof Szabados
 * */
public interface ISubReference extends INamedNode, IIncrementallyUpdatable, IIdentifierContainer, IVisitableNode, IIdentifiable {
	String INVALIDSUBREFERENCE = "Unsupported subreference kind.";

	enum Subreference_type {
		fieldSubReference,
		arraySubReference,
		parameterisedSubReference
	}

	/**
	 * @return the type of the sub-reference.
	 * */
	Subreference_type getReferenceType();

	/**
	 * @return the identifier of the sub-reference
	 * */
	Identifier getId();

	@Override
	default Identifier getIdentifier() {
		return getId();
	}

	/**
	 * @return the location, the sub-reference was declared at
	 * */
	Location getLocation();

	/**
	 * Sets the actual scope of this node.
	 *
	 * @param scope the scope to be set
	 * */
	void setMyScope(Scope scope);

	/**
	 * Appends the description to be displayed of the sub-reference to the parameter, and returns it.
	 * Or if the parameter is <code>null</code> creates a new <code>StringBuilder</code> and returns that.
	 * <p>
	 * If the parameter is not <code>null</code> and contains elements, prefixes might also be inserted.
	 *
	 * @param builder the stringBuilder to append the result to, or null
	 * */
	void appendDisplayName(final StringBuilder builder);
}
