/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.DiagnosticTag;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Group;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.PropertyBody;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.semantichighlighting.ISemanticInformation;

/**
 * The assignment class represents TTCN3 or ASN.1 assignments.
 * <p>
 * All TTCN3 definitions extend this class.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public abstract class Assignment extends ASTNode
	implements IOutlineElement, ILocateableNode, IReferenceChainElement, IReferencingElement, ISemanticInformation, ICancelBuild {
	protected static final String GLOBALLY_UNUSED = "The {0} seems to be never used globally";
	protected static final String LOCALLY_UNUSED = "The {0} seems to be never used locally";
	protected static final String BASIC_ASSIGNMENT = "basic assignment";

	public enum Assignment_type {
		/** type assignment. */																A_TYPE,

		/** altstep assignment (TTCN-3). */											A_ALTSTEP,
		/** constant assignment (TTCN-3). */										A_CONST,
		/**  external constant (TTCN-3). */											A_EXT_CONST,
		/** function without return statement (TTCN-3).*/					A_FUNCTION ,
		/** function returning a value (TTCN-3). */								A_FUNCTION_RVAL,
		/** function returning a template (TTCN-3). */							A_FUNCTION_RTEMP,
		/** external function without return statement  (TTCN-3). */	A_EXT_FUNCTION,
		/** external function returning a value (TTCN-3). */					A_EXT_FUNCTION_RVAL,
		/** external function returning a template (TTCN-3). */			A_EXT_FUNCTION_RTEMP,
		/** a module parameter (TTCN-3). */											A_MODULEPAR,
		/** a template module parameter (TTCN-3). */								A_MODULEPAR_TEMPLATE,
		/** port (TTCN-3). */																	A_PORT ,
		/** variable  (TTCN-3). */															A_VAR,
		/** template  (TTCN-3). */															A_TEMPLATE,
		/** template variable, dynamic template (TTCN-3). */				A_VAR_TEMPLATE,
		/** timer  (TTCN-3). */																A_TIMER,
		/** testcase  (TTCN-3). */															A_TESTCASE,
		/** constructor (TTCN-3 oop extension) */							A_CONSTRUCTOR,
		/** formal parameter (value) (TTCN3). */									A_PAR_VAL,
		/** formal parameter (in value) (TTCN3). */								A_PAR_VAL_IN,
		/** formal parameter (out value) (TTCN3). */							A_PAR_VAL_OUT,
		/** formal parameter (inout value) (TTCN3). */							A_PAR_VAL_INOUT,
		/** formal parameter (in template) (TTCN3). */							A_PAR_TEMP_IN,
		/** formal parameter (out template) (TTCN3). */						A_PAR_TEMP_OUT,
		/** formal parameter (inout template) (TTCN3). */					A_PAR_TEMP_INOUT,
		/** formal parameter (timer) (TTCN3). */									A_PAR_TIMER,
		/** formal parameter (port) (TTCN3). */									A_PAR_PORT,

		/**< undefined/undecided (ASN.1). */			A_UNDEF,
		/**< value set (ASN.1). */								A_VS,
		/**< information object class (ASN.1). */		A_OC,
		/**< information object (ASN.1). */				A_OBJECT,
		/**< information object set (ASN.1). */			A_OS;

		public final boolean semanticallyEquals(final Assignment_type other) {
			if(this == A_PAR_VAL || this == A_PAR_VAL_IN) {
				return other == A_PAR_VAL || other == A_PAR_VAL_IN;
			}

			return this == other;
		}
	}

	/** The identifier of the assignment. */
	protected Identifier identifier;

	/**
	 * The location of the whole assignment.
	 * This location encloses the assignment fully, as it is used to report errors to.
	 **/
	protected Location location = NULL_Location.INSTANCE;

	/** The enclosing group of the whole assignment. */
	protected Group parentGroup;

	/** the time when this assignment was checked the last time. */
	protected CompilationTimeStamp lastTimeChecked;

	protected boolean isErroneous;

	/** used by the incremental processing to signal if the assignment can be the root of a change */
	private boolean canBeCheckRoot = true;
	
	//* List of references referring to this assignment
	private Set<Reference> references = ConcurrentHashMap.newKeySet();
	
	protected Assignment(final Identifier identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Adds a reference to the list of references referring to this assignment.
	 * If it is the first reference, the 'unused' diagnostic tag is removed. 
	 * 
	 * @param reference
	 */
	public synchronized void addReference(Reference reference) {
		references.add(reference);

		if (references.size() == 1) {
			if (location instanceof NULL_Location) {
				return;
			}
			final ProjectItem projectItem = Project.INSTANCE.getProjectItem(location.getFile().toPath());
			final Location idLocation = getIdentifier().getLocation();
//			projectItem.removeDiagnosticTag(idLocation.getStartPosition(), idLocation.getEndPosition(), DiagnosticTag.Unnecessary);
//			projectItem.removeMarkers(MarkerType.CompilerWarning, idLocation.getStartPosition(), idLocation.getEndPosition());
		}
	}
	
	/**
	 * Returns the number of references referring to this assignment.
	 * To prevent counting of past references, we only consider references that have the same compilation timestamp as this assignment.
	 * @return
	 */
	public int getReferenceCount() {
		int count = 0;
		for (Reference ref : references) {
			if (ref.getLastTimeChecked() == lastTimeChecked) {
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Returns the list of references referring to this assignment.
	 * To prevent listing past references, we only consider references that have the same compilation timestamp as this assignment.
	 * @return
	 */
	public List<Location> getReferenceLocationList() {
		return references.stream()
			.filter(f -> f.getLastTimeChecked() == lastTimeChecked)
			.map(m -> m.getLocation())
			.collect(Collectors.toList());
	}
	
	public List<Reference> getReferences() {
		return new ArrayList<>(references);
	}

	public final CompilationTimeStamp getLastTimeChecked() {
		return lastTimeChecked;
	}

	/**
	 * returns true if the assignment is the root of a change.
	 * */
	public final boolean isCheckRoot() {
		return canBeCheckRoot;
	}

	/**
	 * Signals that the assignment can serve as a change root for the incremental analysis.
	 * */
	public final void checkRoot() {
		canBeCheckRoot = true;
	}

	/**
	 * Signals that the assignment can not serve as a change root for the incremental analysis.
	 * */
	public final void notCheckRoot() {
		canBeCheckRoot = false;
	}

	public final boolean getIsErroneous() {
		return isErroneous;
	}

	/**
	 * Returns a string containing the Java reference pointing to this assignment.
	 * */
	public abstract String getGenName();

	@Override
	/** {@inheritDoc} */
	public final void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getChainLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public final String chainedDescription() {
		return getFullName();
	}
	
	/** @return whether the assignment is local or not */
	public boolean isLocal() {
		return false;
	}

	/**
	 * Sets the parent group of the assignment.
	 *
	 * @param parentGroup the group to be set as the parent.
	 * */
	public final void setParentGroup(final Group parentGroup) {
		this.parentGroup = parentGroup;
	}

	/** @return the parent group of the assignment. */
	public final Group getParentGroup() {
		return parentGroup;
	}

	/**
	 * @return the kind of the assignment.
	 * */
	public abstract Assignment_type getAssignmentType();

	/**
	 * {@inheritDoc}
	 * @return the identifier of the assignment.
	 */
	@Override
	public final Identifier getIdentifier() {
		return identifier;
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		return new Object[]{};
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		return GeneralConstants.EMPTY_STRING;
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		return getAssignmentType().ordinal() * IType.Type_type.values().length;
	}

	/**
	 * @return the name of the assignment
	 * */
	public abstract String getAssignmentName();

	/**
	 * @see DeclarationCollector#addDeclaration(org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition)
	 * @return a short description of the assignment, used by the declaration collector.
	 * */
	public String getProposalDescription() {
		return BASIC_ASSIGNMENT;
	}

	/**
	 * Returns a short description of the assignment to be used in error reports.
	 *
	 * @return a short description of the assignment, used by error reports.
	 * */
	public String getDescription() {
		final StringBuilder builder = new StringBuilder(getAssignmentName());
		builder.append(" `").append(getFullName()).append('\'');
		return builder.toString();
	}

	/**
	 * Calculates the setting of this assignment.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 *
	 * @return the setting of this assignment
	 * */
	public ISetting getSetting(final CompilationTimeStamp timestamp) {
		return null;
	}

	/**
	 * Calculates the type of this assignment.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 *
	 *  @return the type of the assignment if it has one, otherwise null
	 * */
	public IType getType(final CompilationTimeStamp timestamp) {
		return null;
	}

	/**
	 * Performs the semantic checking of the assignment.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * */
	public abstract void check(final CompilationTimeStamp timestamp);

	/**
	 * Performs the semantic checking of the assignment.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param refChain the reference chain to detect circular references.
	 * */
	public abstract void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain);

	/** Checks the properties of the assignment,
	 * that can only be checked after the semantic check was completely run. */
	public void postCheck() {
		// Do nothing
	}

	@Override
	/** {@inheritDoc} */
	public IDeclaration getDeclaration() {
		return IDeclaration.createInstance(this);
	}
	
	/**
	 * Checks whether the assignment is a class property
	 * 
	 * @return
	 */
	public boolean isProperty() {
		return false;
	}

	public boolean isAbstract() {
		TitanLogger.logFatal("Assignment::isAbstract()");
		return false;
	}
	
	public boolean isFinal() {
		TitanLogger.logFatal("Assignment::isFinal()");
		return false;
	}
	
	public PropertyBody getPropertyBody() {
		TitanLogger.logFatal("Assignment::getPropertyBody()");
		return null;
	}
	
	public boolean isInternal() {
		return false;
	}
	
	protected void addCodeLens(CodeLens codelens) {
		final Project project = Project.INSTANCE;
		final ProjectItem projectItem = project.getProjectItem(location.getFile().toPath());
		if (projectItem == null) {
			return;
		}
		projectItem.addCodeLens(codelens);
	}
	
	protected void updateCodeLens(CodeLens codelens) {
		final Project project = Project.INSTANCE;
		final ProjectItem projectItem = project.getProjectItem(location.getFile().toPath());
		if (projectItem == null) {
			return;
		}
		projectItem.updateCodeLens(codelens);
	}
	
	protected void clearCodeLens() {
		final Project project = Project.INSTANCE;
		final ProjectItem projectItem = project.getProjectItem(location.getFile().toPath());
		if (projectItem == null) {
			return;
		}
		projectItem.clearCodeLenses();
	}
	
	protected void addDiagnosticTags(List<DiagnosticTag> tags) {
//		final Project project = Project.INSTANCE;
//		final ProjectItem projectItem = project.getProjectItem(location.getFile().toPath());
//		for (DiagnosticTag tag : tags) {
//			final Diagnostic diagnostic = new Diagnostic();
//			diagnostic.setRange(identifier.getLocation().getRange());
//			diagnostic.setTags(tags);
//			diagnostic.setSeverity(DiagnosticSeverity.Warning);
//			String message = "";
//			switch (tag) {
//			case Deprecated:
//				message = "This definition is deprecated";
//				break;
//			case Unnecessary:
//				message = "This definition is unused";
//				break;
//			}
//			diagnostic.setMessage(message);
//		
//			projectItem.addMarker(new LspMarker( diagnostic));
//		}
	}
}
