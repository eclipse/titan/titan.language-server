/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.util.concurrent.CompletableFuture;

import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.jsonrpc.services.JsonNotification;
import org.eclipse.lsp4j.jsonrpc.services.JsonRequest;
import org.eclipse.lsp4j.jsonrpc.services.JsonSegment;
import org.eclipse.titan.lsp.lspextensions.DocumentCommentParams;
import org.eclipse.titan.lsp.lspextensions.EnvironmentParams;
import org.eclipse.titan.lsp.lspextensions.ExcludedStatesChangedParams;
import org.eclipse.titan.lsp.lspextensions.FolderConfigParams;
import org.eclipse.titan.lsp.lspextensions.GenerateDocumentCommentParams;
import org.eclipse.titan.lsp.lspextensions.MetricDataParams;
import org.eclipse.titan.lsp.lspextensions.ActivateTpdParams;
import org.eclipse.titan.lsp.lspextensions.TpdProjectFiles;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricDataProvider.MetricsResponse;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricDataProvider.MetricsStatsResponse;

/**
 * This interface defines custom json-rpc messages that extend the base LSP protocol  
 *  
 * @author Miklos Magyari
 *
 */
@JsonSegment("titan")
public interface LspExtensionsService {
	/**
	 * The documentComment request is sent from the titan client to the server to ask
	 * for the document comment info for the given cursor position. This is nearly
	 * the same as textDocument/hover, but used for the document comment view so it
	 * is more clear to define a custom request to avoid confusion.
	 * 
	 * @param param
	 * @return
	 */
	@JsonRequest
	default CompletableFuture<String> documentComment(DocumentCommentParams params) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * This notification is sent from the client indicating that the excluded state
	 * for the given resources are changed.
	 * <br><br>
	 * Expected json:
	 * 
	 * <pre>
	 *  "excludedStates": [
	 *	{
	 *		"identifier": { "uri": "file:///whateverA" },
	 *		"isExcluded": true
	 *	},
	 *	{
	 *		"identifier": { "uri": "file:///whateverB" },
	 *		"isExcluded": false
	 *	}, ...
	 *	]
	 * </pre>
	 * 
	 * @param params
	 */
	@JsonNotification
	default void setExcludedState(ExcludedStatesChangedParams params) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * The activateTpd request is sent from the titan client to the server to
	 * request activating a TPD file
	 */
	@JsonRequest
	default CompletableFuture<TpdProjectFiles> activateTpd(ActivateTpdParams params) {
		throw new UnsupportedOperationException();
	}
	
	@JsonRequest
	default CompletableFuture<String> deactivateTpd() {
		throw new UnsupportedOperationException();
	}
	
	@JsonRequest
	default CompletableFuture<String> generateDocumentComment(GenerateDocumentCommentParams params) {
		throw new UnsupportedOperationException();
	}
	
	@JsonRequest
	default CompletableFuture<Either<MetricsResponse, MetricsStatsResponse>> getMetricData(MetricDataParams params) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * This notification is sent from the client with all environment variables available for the
	 * process of vscode.
	 * <br><br>
	 * Expected json:
	 * 
	 * <pre>
	 *  "variables": [
	 *	{
	 *		"name": "VARIABLE1",
	 *		"value": "valueX"
	 *	},
	 *	{
	 *		"name": "VARIABLE2",
	 *		"value": "valueY"
	 *	}, ...
	 *	]
	 * </pre>
	 * @param params
	 */
	@JsonNotification
	default void environmentVariables(EnvironmentParams params) {
		throw new UnsupportedOperationException();
	}
	
	@JsonNotification
	default void folderConfig(FolderConfigParams params) {
		throw new UnsupportedOperationException();
	}
}
