/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Array_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimension;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.SequenceOf_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Values;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a list of templates.
 *
 * @author Kristof Szabados
 * */
public final class Template_List extends CompositeTemplate {
	/** Indicates whether the embedded templates contain PERMUTATION_MATCH. */
	private boolean hasPermutation = false;

	// cache storing the value form of this list of templates if already
	// created, or null
	private SequenceOf_Value asValue = null;

	// if assigned to a record/set the semantic checking will create a converted value.
	private TTCN3Template converted = null;

	public Template_List(final ListOfTemplates templates) {
		super(templates);

		for (final TTCN3Template t : templates) {
			if (Template_type.PERMUTATION_MATCH.equals(t.getTemplatetype())) {
				hasPermutation = true;
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.TEMPLATE_LIST;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		builder.append("{ ");
		for (int i = 0, size = templates.size(); i < size; i++) {
			if (i > 0) {
				builder.append(", ");
			}

			final ITTCN3Template template = templates.get(i);
			builder.append(template.createStringRepresentation());
		}
		builder.append(" }");

		return addToStringRepresentation(builder).toString();
	}

	public boolean hasAllFrom() {
		for (final TTCN3Template t : templates) {
			if (Template_type.ALL_FROM.equals(t.getTemplatetype())) {
				return true;
			}
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public ITTCN3Template setTemplatetype(final CompilationTimeStamp timestamp, final Template_type newType) {
		switch (newType) {
		case NAMED_TEMPLATE_LIST:
			converted =  Named_Template_List.convert(timestamp, this);
			return converted;
		default:
			return super.setTemplatetype(timestamp, newType);
		}
	}

	@Override
	public TTCN3Template getTemplateReferencedLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (converted == null || converted.getIsErroneous(timestamp)) {
			return this;
		}

		return converted.getTemplateReferencedLast(timestamp, referenceChain);
	}

	@Override
	/** {@inheritDoc} */
	protected ITTCN3Template getReferencedArrayTemplate(final CompilationTimeStamp timestamp, final IValue arrayIndex,
			final IReferenceChain referenceChain, final boolean silent) {
		IValue indexValue = arrayIndex.setLoweridToReference(timestamp);
		indexValue = indexValue.getValueRefdLast(timestamp, referenceChain);
		if (indexValue.getIsErroneous(timestamp)) {
			return null;
		}

		long index = 0;
		if (!indexValue.isUnfoldable(timestamp)) {
			if (Value_type.INTEGER_VALUE.equals(indexValue.getValuetype())) {
				index = ((Integer_Value) indexValue).getValue();
			} else {
				if (!silent) {
					arrayIndex.getLocation().reportSemanticError("An integer value was expected as index");
				}
				return null;
			}
		} else {
			return null;
		}

		final IType tempType = myGovernor.getTypeRefdLast(timestamp);
		if (tempType.getIsErroneous(timestamp)) {
			return null;
		}

		switch (tempType.getTypetype()) {
		case TYPE_SEQUENCE_OF: {
			if (index < 0) {
				if (!silent) {
					final String message = MessageFormat
						.format("A non-negative integer value was expected instead of {0} for indexing a template of `sequence of'' type `{1}''",
								index, tempType.getTypename());
					arrayIndex.getLocation().reportSemanticError(message);
				}
				return null;
			}

			final int nofElements = size();
			if (index >= nofElements) {
				if (!silent) {
					final String message = MessageFormat
						.format("Index overflow in a template of `sequence of'' type `{0}'': the index is {1}, but the template has only {2} elements",
								tempType.getTypename(), index, nofElements);
					arrayIndex.getLocation().reportSemanticError(message		);
				}
				return null;
			}
			break;
		}
		case TYPE_SET_OF: {
			if (index < 0) {
				if (!silent) {
					final String message = MessageFormat
						.format("A non-negative integer value was expected instead of {0} for indexing a template of `set of'' type `{1}''",
								index, tempType.getTypename());
					arrayIndex.getLocation().reportSemanticError(message);
				}
				return null;
			}

			final int nofElements = size();
			if (index >= nofElements) {
				if (!silent) {
					final String message = MessageFormat
						.format("Index overflow in a template of `set of'' type `{0}'': the index is {1}, but the template has only {2} elements",
								tempType.getTypename(), index, nofElements);
					arrayIndex.getLocation().reportSemanticError(message);
				}
				return null;
			}
			break;
		}
		case TYPE_ARRAY: {
			final ArrayDimension dimension = ((Array_Type) tempType).getDimension();
			dimension.checkIndex(timestamp, indexValue, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
			if (!dimension.getIsErroneous(timestamp)) {
				// re-base the index
				index -= dimension.getOffset();
				if (index < 0 || index >= size()) {
					if (!silent) {
						arrayIndex.getLocation().reportSemanticError(
							MessageFormat.format("The index value {0} is outside the array indexable range", index
									+ dimension.getOffset()));
					}
					return null;
				}
			} else {
				return null;
			}
			break;
		}
		default:{
			if (!silent) {
				final String message = MessageFormat.format("Invalid array element reference: type `{0}'' cannot be indexed",
					tempType.getTypename());
				arrayIndex.getLocation().reportSemanticError(message);
			}
			return null;
		}
		}

		final ITTCN3Template returnValue = get((int) index);
		if (Template_type.TEMPLATE_NOTUSED.equals(returnValue.getTemplatetype())) {
			if (baseTemplate != null) {
				return baseTemplate.getTemplateReferencedLast(timestamp, referenceChain).getReferencedArrayTemplate(timestamp,
						indexValue, referenceChain, silent);
			}

			return null;
		}

		return returnValue;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isValue(final CompilationTimeStamp timestamp) {
		if (lengthRestriction != null || isIfpresent || getIsErroneous(timestamp)) {
			return false;
		}

		for (final TTCN3Template t : templates) {
			if (!t.isValue(timestamp)) {
				return false;
			}
		}

		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IValue getValue() {
		if (asValue != null) {
			return asValue;
		}

		if (converted != null) {
			return converted.getValue();
		}

		final Values values = new Values(false);
		for (final TTCN3Template t : templates) {
			values.addValue(t.getValue());
		}

		asValue = new SequenceOf_Value(values);
		asValue.setLocation(getLocation());
		asValue.setMyScope(getMyScope());
		asValue.setFullNameParent(getNameParent());
		asValue.setMyGovernor(getMyGovernor());

		return asValue;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		for (final TTCN3Template t : templates) {
			if(t.checkExpressionSelfReferenceTemplate(timestamp, lhs)) {
				return true;
			}
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		for (final TTCN3Template t : templates) {
			t.checkSpecificValue(timestamp, true);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected void checkTemplateSpecificLengthRestriction(final CompilationTimeStamp timestamp, final Type_type typeType) {
		if (Type_type.TYPE_SEQUENCE_OF.equals(typeType) || Type_type.TYPE_SET_OF.equals(typeType)) {
			final int nofTemplatesGood = getNofTemplatesNotAnyornone(timestamp); //at least !

			final boolean hasAnyOrNone = templateContainsAnyornone();

			lengthRestriction.checkNofElements(timestamp, nofTemplatesGood, hasAnyOrNone, false, hasAnyOrNone, this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, omitAllowed, usageLocation);

		boolean needsRuntimeCheck = false;
		for (final TTCN3Template t : templates) {
			if (t.checkValueomitRestriction(timestamp, definitionName, true, usageLocation)) {
				needsRuntimeCheck = true;
			}
		}
		return needsRuntimeCheck;
	}

	@Override
	/** {@inheritDoc} */
	public ITTCN3Template getReferencedSetSequenceFieldTemplate(final CompilationTimeStamp timestamp, final Identifier fieldIdentifier,
			final Reference reference, final IReferenceChain referenceChain, final boolean silent) {
		if (converted != null) {
			return converted.getReferencedSetSequenceFieldTemplate(timestamp, fieldIdentifier, reference, referenceChain, silent);
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (lengthRestriction != null) {
			lengthRestriction.findReferences(referenceFinder, foundIdentifiers);
		}

		if (asValue != null) {
			asValue.findReferences(referenceFinder, foundIdentifiers);
		} else if (templates != null) {
			templates.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (asValue != null && !asValue.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	protected String getNameForStringRep() {
		return EMPTY_STRING;
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		if (converted != null) {
			return converted.needsTemporaryReference();
		}

		if (lengthRestriction != null || isIfpresent) {
			return true;
		}

		// temporary reference is needed if the template has at least one
		// element (excluding not used symbols)
		for (final TTCN3Template template : templates) {
			if (template.getTemplatetype() != Template_type.TEMPLATE_NOTUSED) {
				return true;
			}
		}

		return false;
	}
}
