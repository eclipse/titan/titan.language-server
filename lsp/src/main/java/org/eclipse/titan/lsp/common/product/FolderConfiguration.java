/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.product;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Map;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.lspextensions.FolderConfigParams.CodeSmells;
import org.eclipse.titan.lsp.lspextensions.FolderConfigParams.NamingConventions;

public class FolderConfiguration {
	private String namingAltstep;
	private String namingComponentConstant;
	private String namingComponentTimer;
	private String namingComponentVariable;
	private String namingExternalFunction;
	private String namingFormalParameter;
	private String namingFunction;
	private String namingGlobalConstant;
	private String namingGlobalTimer;
	private String namingLocalConstant;
	private String namingLocalTimer;
	private String namingLocalVariable;
	private String namingLocalVarTemplate;
	private String namingPort;
	private String namingTestcase;
	private String namingVariable;
	
	private Map<String, String> smellSeverities = new HashMap<>();

	public FolderConfiguration() {
		setFolderNamingConventions(new NamingConventions());
	}
	
	public void setFolderNamingConventions(final NamingConventions conventions) {
		namingAltstep = Objects.requireNonNullElse(conventions.altstep, GeneralConstants.NAMINGCONV_ALTSTEP);
		namingComponentConstant = Objects.requireNonNullElse(conventions.componentConstant, GeneralConstants.NAMINGCONV_COMPONENT_CONST);
		namingComponentTimer = Objects.requireNonNullElse(conventions.componentTimer, GeneralConstants.NAMINGCONV_COMPONENT_TIMER);
		namingComponentVariable = Objects.requireNonNullElse(conventions.componentVariable, GeneralConstants.NAMINGCONV_COMPONENT_VARIABLE);
		namingExternalFunction = Objects.requireNonNullElse(conventions.externalFunction, GeneralConstants.NAMINGCONV_EXTFUNCTION);
		namingFormalParameter = Objects.requireNonNullElse(conventions.formalParameter, GeneralConstants.NAMINGCONV_FORMALPAR);
		namingFunction = Objects.requireNonNullElse(conventions.function, GeneralConstants.NAMINGCONV_EXTFUNCTION);
		namingGlobalConstant = Objects.requireNonNullElse(conventions.globalConstant, GeneralConstants.NAMINGCONV_GLOBAL_CONST);
		namingGlobalTimer = Objects.requireNonNullElse(conventions.globalTimer, GeneralConstants.NAMINGCONV_GLOBAL_TIMER);
		namingLocalConstant = Objects.requireNonNullElse(conventions.localConstant, GeneralConstants.NAMINGCONV_LOCAL_CONST);
		namingLocalTimer = Objects.requireNonNullElse(conventions.localTimer, GeneralConstants.NAMINGCONV_LOCAL_TIMER);
		namingLocalVariable = Objects.requireNonNullElse(conventions.localVariable, GeneralConstants.NAMINGCONV_LOCAL_VARIABLE);
		namingLocalVarTemplate = Objects.requireNonNullElse(conventions.localVarTemplate, GeneralConstants.NAMINGCONV_LOCAL_VARTEMPLATE);
		namingTestcase = Objects.requireNonNullElse(conventions.testcase, GeneralConstants.NAMINGCONV_TESTCASE);
		namingPort = Objects.requireNonNullElse(conventions.port, GeneralConstants.NAMINGCONV_PORT);
		namingVariable = Objects.requireNonNullElse(conventions.variable, GeneralConstants.NAMINGCONV_VARIABLE);
	}
	
	public void setCodeSmellSeverities(final CodeSmells smells) {
		if (smells == null) {
			return;
		}
		final List<Field> fields = Arrays.asList(CodeSmells.class.getFields());
		for (final Field field : fields) {
			try {
				final Object value = field.get(smells);
				if (value instanceof String) {
					smellSeverities.put(field.getName(), (String)value);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getCodeSmellSeverity(final String smellName) {
		if (smellSeverities.containsKey(smellName)) {
			return smellSeverities.get(smellName);
		}
		
		return Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, "warning");
	}

	public String getNamingLocalVariable() {
		return namingLocalVariable;
	}

	public String getNamingVariable() {
		return namingVariable;
	}

	public String getNamingFormalParameter() {
		return namingFormalParameter;
	}

	public String getNamingTestcase() {
		return namingTestcase;
	}

	public String getNamingExternalFunction() {
		return namingExternalFunction;
	}

	public String getNamingPort() {
		return namingPort;
	}

	public String getNamingAltstep() {
		return namingAltstep;
	}

	public String getNamingComponentConstant() {
		return namingComponentConstant;
	}

	public String getNamingComponentTimer() {
		return namingComponentTimer;
	}

	public String getNamingComponentVariable() {
		return namingComponentVariable;
	}

	public String getNamingGlobalConstant() {
		return namingGlobalConstant;
	}

	public String getNamingGlobalTimer() {
		return namingGlobalTimer;
	}

	public String getNamingLocalConstant() {
		return namingLocalConstant;
	}

	public String getNamingLocalTimer() {
		return namingLocalTimer;
	}

	public String getNamingLocalVarTemplate() {
		return namingLocalVarTemplate;
	}

	public String getNamingFunction() {
		return namingFunction;
	}
}
