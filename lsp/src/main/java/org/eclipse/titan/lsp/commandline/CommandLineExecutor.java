/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.commandline;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.WorkspaceFolder;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.product.TpdImporter;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;

/**
 * Entry point for the command line mode of the language server.
 * 
 * @author Miklos Magyari
 */
public class CommandLineExecutor {
	private static final String FILE_PREFIX = "file:///";

	private CommandLineConfiguration cmdConfig;
	private final Project project;
	final Configuration cfg;
	private PrintStream errorOut = System.out; 
	private PrintStream markerOut = System.out;

	public CommandLineExecutor(final CommandLineConfiguration config) {
		project = Project.INSTANCE;
		cfg = Configuration.INSTANCE;
		cmdConfig = config;
	}

	public void runCommandLineAnalyzer() {
		if (cmdConfig.cfgFile != null && !cmdConfig.cfgFile.isEmpty()) {
			Path cfgPath;
			String cfgText = "";
			try {
				cfgPath = Paths.get(cmdConfig.cfgFile);
				cfgText = Files.readString(cfgPath);
			} catch (InvalidPathException e) {
				errorOut.println("Wrong config path.");
				System.exit(-1);
			} catch (IOException e) {
				errorOut.println("Failed to read config file.");
				System.exit(-1);
			}
			
			if (!cfg.parseConfiguration(cfgText)) {
				errorOut.println("Failed to parse config file.");
				System.exit(-1);
			}
			getSettings();
		}

		setSettings();

		final List<Object> folders = cfg.getList("folders", new ArrayList<>());
		final List<WorkspaceFolder> wsFolders = new ArrayList<>();

		String projectRoot = "";
		if (cmdConfig.rootFolder != null && !cmdConfig.rootFolder.isEmpty()) {
			final Path folderPath = Paths.get(cmdConfig.rootFolder);
			if (Files.exists(folderPath)) {
				final WorkspaceFolder wsf = new WorkspaceFolder(FILE_PREFIX + cmdConfig.rootFolder, cmdConfig.rootFolder);
				wsFolders.add(wsf);
			} else {
				errorOut.println("Invalid root folder: " + cmdConfig.rootFolder);
				System.exit(-1);
			}
			projectRoot = cmdConfig.rootFolder;
		} else {
			projectRoot = cfg.getString(Configuration.PROJECTROOT, null);
			final String root = projectRoot;
			folders.forEach(folder -> {
				if (folder instanceof String) {
					String folderStr = (String)folder;
					final String folderName = root != null ?
							root + File.separator + (folderStr) : (folderStr);
					try {
						final Path folderPath = Paths.get(folderName);
						if (Files.exists(folderPath)) {
							final WorkspaceFolder wsf = new WorkspaceFolder(FILE_PREFIX + folderName, folderName);
							wsFolders.add(wsf);
						} else {
							errorOut.println("Cannot add file: " + folderName);
						}
					} catch (InvalidPathException e) {
						errorOut.println("Broken path: " + folderName);
					}
				}
			});
		}
		final CommandLineFolderConfiguration fc = new CommandLineFolderConfiguration(projectRoot);
		System.out.println(fc);

		if (cfg.getString(Configuration.TPDFILE, null) == null && folders.isEmpty() && (projectRoot == null || projectRoot.isEmpty())) {
			errorOut.println("No files/folders specified");
			System.exit(-1);
		}

		project.addWorkspaceFolders(wsFolders);
		analyze();
	}

	private void analyze() {
		final String titanLogFormat = cmdConfig.ttcnFormat != null ? cmdConfig.ttcnFormat : "%f:::%m:::%l:::%s"; 
		final String titaniumLogFormat = cfg.getString("staticAnalyzerMarkerFormat", "%f (%r): %m");
		final String projectRoot = cfg.getString(Configuration.PROJECTROOT, null);
		final String tpdFromConfig = cfg.getString(Configuration.TPDFILE, null);
		String tpdFileName = "";
		boolean isTpdImported;
		if (tpdFromConfig != null) {
			final WorkspaceFolder wsf = new WorkspaceFolder(FILE_PREFIX + projectRoot, projectRoot);
			final List<WorkspaceFolder> wsFolders = new ArrayList<>();
			wsFolders.add(wsf);
			project.addWorkspaceFolders(wsFolders);
			tpdFileName = projectRoot != null ?
				projectRoot + File.separator + cfg.getString(Configuration.TPDFILE, null) :
				cfg.getString(Configuration.TPDFILE, null);
			final File tpdFile = new File(tpdFileName);
			TpdImporter importer = new TpdImporter(tpdFile);
			importer.processTpd(false, false, false, null);

			final List<Path> fileList = importer.getFileNames();
			project.setTpdFiles(fileList);
			project.outdateAllItems();

			if (!fileList.isEmpty()) {
				isTpdImported = true;
				cfg.addPreference(Configuration.ACTIVATED_TPD, tpdFileName);
				TitanLanguageServer.analyzeProject(project, true, tpdFileName);
			}
		} else {
			TitanLanguageServer.analyzeProject(project);
		}

		project.getAllProjectItems().entrySet().stream().forEach(projectItem -> {
			final ProjectItem item = project.getProjectItem(projectItem.getKey());
			if (item.isDirectory()) {
				return;
			}
			final List<Diagnostic> markerList = projectItem.getValue().getMarkerList(); 
			Path uri = projectItem.getKey();
			if (markerList != null) {
				for (final Diagnostic diag : markerList) {
					if (diag.getSource().equals(GeneralConstants.TITAN)) {
						if (diag.getSeverity() == DiagnosticSeverity.Error && Boolean.FALSE.equals(cmdConfig.ttcnErrorMarkers)) {
							continue;
						}
						if (diag.getSeverity() == DiagnosticSeverity.Warning && Boolean.FALSE.equals(cmdConfig.ttcnWarningMarkers)) {
							continue;
						}
					} else if (diag.getSource().equals(GeneralConstants.TITANIUM)) {
						if (diag.getSeverity() == DiagnosticSeverity.Error && Boolean.FALSE.equals(cmdConfig.titaniumErrorMarkers)) {
							continue;
						}
						if (diag.getSeverity() == DiagnosticSeverity.Warning && Boolean.FALSE.equals(cmdConfig.titaniumWarningMarkers)) {
							continue;
						}
					}
					final Path root = Paths.get(cmdConfig.rootFolder); 
					final String filePath = root.relativize(uri).toString();
					final Range range = diag.getRange();
					final String startLine = Integer.toString(range.getStart().getLine());
					final String startColumn = Integer.toString(range.getStart().getCharacter());
					final String endLine = Integer.toString(range.getEnd().getLine());
					final String endColumn = Integer.toString(range.getEnd().getCharacter());
					final String message = diag.getMessage();

					String logLine = "";
					if (diag.getSource().equals(GeneralConstants.TITAN)) {
						logLine = titanLogFormat;
					}
					if (diag.getSource().equals(GeneralConstants.TITANIUM)) {
						logLine = titaniumLogFormat;
					}
					final String severity;
					switch (diag.getSeverity()) {
					case Warning:
						severity = "W";
						break;
					case Error:
					default:
						severity = "E";
						break;
					}
					final String formattedLine = logLine
							.replace("%f", Matcher.quoteReplacement(filePath))
							.replace("%l", startLine)
							.replace("%c", startColumn)
							.replace("%L", endLine)
							.replace("%C", endColumn)
							.replace("%p", startLine + ":" + startColumn)
							.replace("%P", endLine + ":" + endColumn)
							.replace("%r", startLine + ":" + startColumn + "-" + endLine + ":" + endColumn)
							.replace("%m", message)
							.replace("%s", severity);

					markerOut.println(formattedLine);
				}
			}
		});
	}

	private void getSettings() {
		if (cmdConfig.ttcnErrorMarkers == null) {
			cmdConfig.ttcnErrorMarkers = cfg.getBooleanObject("ttcnErrorMarkers", true);
		}
		if (cmdConfig.ttcnWarningMarkers == null) {
			cmdConfig.ttcnWarningMarkers = cfg.getBooleanObject("ttcnWarningMarkers", true);
		}
		if (cmdConfig.titaniumErrorMarkers == null) {
			cmdConfig.titaniumErrorMarkers = cfg.getBooleanObject("staticAnalyzerErrorMarkers", true);
		}
		if (cmdConfig.ttcnWarningMarkers == null) {
			cmdConfig.titaniumWarningMarkers = cfg.getBooleanObject("staticAnalyzerWarningMarkers", true);
		}
	}

	private void setSettings() {
		if (cmdConfig.ttcnErrorMarkers == null) {
			cmdConfig.ttcnErrorMarkers = Boolean.TRUE;
		}
		if (cmdConfig.ttcnWarningMarkers == null) {
			cmdConfig.ttcnWarningMarkers = Boolean.TRUE;
		}
		if (cmdConfig.titaniumErrorMarkers == null) {
			cmdConfig.titaniumErrorMarkers = Boolean.TRUE;
		}
		if (cmdConfig.titaniumWarningMarkers == null) {
			cmdConfig.titaniumWarningMarkers = Boolean.TRUE;
		}
		if (cmdConfig.oopEnabled == null) {
			cmdConfig.oopEnabled = Boolean.FALSE;
		}
		if (cmdConfig.outFile != null && !cmdConfig.outFile.isEmpty()) {
			try {
				markerOut = new PrintStream(new File(cmdConfig.outFile));
			} catch (FileNotFoundException e) {
				final PrintWriter pw = new PrintWriter(new StringWriter());
				e.printStackTrace(pw);
				errorOut.println(pw.toString());
			}
		}
		if (Boolean.TRUE.equals(cmdConfig.titaniumErrorMarkers) ||
				Boolean.TRUE.equals(cmdConfig.titaniumWarningMarkers)) {
			cfg.addPreference(Configuration.ENABLE_TITANIUM, true);
		}
		if (Boolean.TRUE.equals(cmdConfig.oopEnabled)) {
			cfg.addPreference(Configuration.ENABLE_OOP_EXTENSION, true);
		}
		if (Boolean.TRUE.equals(cmdConfig.namingConventionsEnabled)) {
			cfg.addPreference(Configuration.REPORT_NAMING_CONVENTION_PROBLEMS, true);
		}
	}
}
