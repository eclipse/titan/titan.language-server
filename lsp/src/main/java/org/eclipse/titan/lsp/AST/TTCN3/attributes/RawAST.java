/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.types.CharString_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.CharString_Type.CharCoding;

/**
 * Represents the RAW encoding related setting extracted from variant attributes.
 *
 * @author Kristof Szabados
 * */
public class RawAST extends RawAST_Base {

	public static class rawAST_ext_bit_group {
		public int ext_bit;
		public Identifier from;
		public Identifier to;
	}

	public static class rawAST_field_list {
		public List<Identifier> names = new ArrayList<Identifier>();
	}

	public static class rawAST_tag_field_value {
		public rawAST_field_list keyField;
		public String value;
		public Value v_value;
	}

	public static class rawAST_force_omit {
		public List<rawAST_field_list> lists = new ArrayList<RawAST.rawAST_field_list>();
	}

	public static class rawAST_single_tag {
		public Identifier fieldName;
		public List<rawAST_tag_field_value> keyList;
	}

	// rawAST_tag_list is needed in the compiler to store the pointer list head and size together
	// not needed in the Designer
//	public static class rawAST_tag_list {
//		public ArrayList<rawAST_single_tag> tag;
//	}

	//ext_bit_goup_num is not stored as it is the sie of ext_bit_groups
	public List<rawAST_ext_bit_group> ext_bit_groups;
	public int prepadding;
	public int padding_pattern_length;
	public int paddall;
	public String padding_pattern;
	public List<Identifier> lengthto; //list of fields to generate length for
	public Identifier pointerto; //pointer to the specified field is contained in this field

	/**< offset to the pointer value in bits
	 Actual position will be:
	 pointerto*ptrunit + ptroffset */
	public Identifier ptrbase; //the identifier in PTROFFSET(identifier)
	public rawAST_field_list lengthindex; // stores subattribute of the lengthto attribute
	// field IDs in form of [unionField.sub]field_N, keyField.subfield_M = tagValue multiple tagValues may be specified
	public List<rawAST_single_tag> crosstaglist;
	public List<rawAST_single_tag> taglist;

	public rawAST_single_tag presence = new rawAST_single_tag(); // Presence indicator expressions for an optional field
	public rawAST_force_omit forceOmit = new rawAST_force_omit(); //forces lower level optional fields to be omitted
	public int length_restriction;
	public boolean intX; //IntX encoding for integers
	public CharString_Type.CharCoding stringformat; //String serialization type for universal charstrings
	public boolean csn1lh; // use CSN.1 L/H values

	public RawAST(final int defaultLength) {
		init_rawast(defaultLength);
	}

	/**
	 * Sort of copy- constructor
	 * */
	public RawAST(final RawAST other, final int defaultLength) {
		if (other == null) {
			init_rawast(defaultLength);
		} else {
			fieldlength = other.fieldlength;
			comp = other.comp;
			byteorder = other.byteorder;
			align = other.align;
			bitorderinfield = other.bitorderinfield;
			bitorderinoctet = other.bitorderinoctet;
			extension_bit = other.extension_bit;
			ext_bit_groups = null;
			hexorder = other.hexorder;
			padding = other.padding;
			prepadding = other.prepadding;
			padding_pattern_length = other.padding_pattern_length;
			paddall = other.paddall;
			repeatable = other.repeatable;
			padding_pattern = other.padding_pattern;
			fieldorder = other.fieldorder;
			lengthto = null;
			lengthto_offset = 0;
			pointerto = null;
			ptroffset = other.ptroffset;
			ptrbase = null;
			unit = other.unit;
			lengthindex = null;
			crosstaglist = null;
			taglist = null;
			presence = new rawAST_single_tag();
			forceOmit = new rawAST_force_omit();
			toplevelind = other.toplevelind;
			toplevel.bitorder = other.toplevel.bitorder;
			length_restriction = other.length_restriction;
			intX = other.intX;
			stringformat = other.stringformat;
			csn1lh = other.csn1lh;
		}
	}

	private void init_rawast(final int defaultLength) {
		fieldlength = defaultLength;
		comp = XDEFDEFAULT;
		byteorder = XDEFDEFAULT;
		align = XDEFDEFAULT;
		bitorderinfield = XDEFDEFAULT;
		bitorderinoctet = XDEFDEFAULT;
		extension_bit = XDEFDEFAULT;
		ext_bit_groups = null;
		hexorder = XDEFDEFAULT;
		padding = 0;
		prepadding = 0;
		padding_pattern_length = 0;
		paddall = XDEFDEFAULT;
		repeatable = XDEFDEFAULT;
		padding_pattern = null;
		fieldorder = XDEFDEFAULT;
		lengthto = null;
		lengthto_offset = 0;
		pointerto = null;
		ptroffset = 0;
		ptrbase = null;
		unit = 8;
		lengthindex = null;
		crosstaglist = null;
		taglist = null;
		presence = new rawAST_single_tag();
		forceOmit = new rawAST_force_omit();
		toplevelind = 0;
		toplevel.bitorder = XDEFDEFAULT;
		length_restriction = -1;
		intX = false;
		stringformat = CharCoding.UNKNOWN;
		csn1lh = false;
	}
}
