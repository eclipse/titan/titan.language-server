/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template.Template_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.PatternString.PatternType;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.UnivCharString_Pattern_Template;
import org.eclipse.titan.lsp.AST.TTCN3.types.Function_Type;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;

/**
 * @author Kristof Szabados
 * */
public final class Def_ModulePar_Template extends Definition {
	private static final String FULLNAMEPART1 = ".<type>";
	private static final String FULLNAMEPART2 = ".<default_template>";
	private static final String KIND = "template module parameter";

	private final Type type;
	private final TTCN3Template defaultTemplate;
	private ITTCN3Template realTemplate;

	public Def_ModulePar_Template(final Identifier identifier, final Type type, final TTCN3Template defaultTemplate) {
		super(identifier);
		this.type = type;
		this.defaultTemplate = defaultTemplate;
		if (type != null) {
			type.setOwnertype(TypeOwner_type.OT_MODPAR_DEF, this);
			type.setFullNameParent(this);
		}
		if (defaultTemplate != null) {
			defaultTemplate.setFullNameParent(this);
		}
	}

	/**
	 * Checks this template module parameter and returns it's template as
	 * assigned to it..
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 *
	 * @return the template if it exists, otherwise null
	 * */
	public ITTCN3Template getDefaultTemplate(final CompilationTimeStamp timestamp){
		if (lastTimeChecked == null) {
			check(timestamp);
		}

		return defaultTemplate;
	}

	/**
	 * Check this template module parameter, tries to calculate the real
	 * template body and returns it.
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 *
	 * @return the template if it exists, otherwise null
	 * */
	public ITTCN3Template getRealTemplate(final CompilationTimeStamp timestamp){
		if (lastTimeChecked == null) {
			check(timestamp);
		}

		return realTemplate;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_MODULEPAR_TEMPLATE;
	}

	public static String getKind() {
		return " " + KIND;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);
		if (type == child) {
			return builder.append(FULLNAMEPART1);
		} else if (defaultTemplate == child) {
			return builder.append(FULLNAMEPART2);
		}
		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (type != null) {
			type.setMyScope(scope);
		}
		if (defaultTemplate != null) {
			defaultTemplate.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		int result = super.category();
		if (type != null) {
			result += type.category();
		}
		return result;
	}

	@Override
	/** {@inheritDoc} */
	public Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return type;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;

		NamingConventionHelper.checkConvention(NamingConventionElement.ModuleParameter, identifier, this);
		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

		if (type == null) {
			return;
		}

		type.check(timestamp);


		final IType lastType = type.getTypeRefdLast(timestamp);
		switch (lastType.getTypetype()) {
		case TYPE_PORT:
			location.reportSemanticError(MessageFormat.format(Def_ModulePar.PORTNOTALLOWED, lastType.getFullName()));
			break;
		case TYPE_SIGNATURE:
			location.reportSemanticError(MessageFormat.format(Def_ModulePar.SIGNATURENOTALLOWED, lastType.getFullName()));
			break;
		case TYPE_FUNCTION:
		case TYPE_ALTSTEP:
		case TYPE_TESTCASE:
			if (((Function_Type) lastType).isRunsOnSelf()) {
				location.reportSemanticError(MessageFormat.format(Def_ModulePar.RUNSONSELF_NOT_ALLOWED, lastType.getFullName()));
			}
			break;
		default:
			break;
		}

		if (defaultTemplate != null) {
			realTemplate = defaultTemplate;
			realTemplate.setMyGovernor(type);

			// Needed in case of universal charstring templates
			if (defaultTemplate.getTemplatetype() == Template_type.CSTR_PATTERN
					&& lastType.getTypetype() == IType.Type_type.TYPE_UCHARSTRING) {
				realTemplate = defaultTemplate.setTemplatetype(timestamp, Template_type.USTR_PATTERN);
				if (realTemplate instanceof UnivCharString_Pattern_Template) {
					((UnivCharString_Pattern_Template)realTemplate).getPatternstring().setPatterntype(PatternType.UNIVCHARSTRING_PATTERN);
				}
			}

			final ITTCN3Template temporalTemplate = type.checkThisTemplateRef(timestamp, realTemplate);
			temporalTemplate.checkThisTemplateGeneric(timestamp, type, false, true, true, true, false, null);
			final IReferenceChain tempReferenceChain = ReferenceChain.getInstance(Def_Template.CIRCULAREMBEDDEDRECURSION, true);
			tempReferenceChain.add(this);
			temporalTemplate.checkRecursions(timestamp, tempReferenceChain);
			tempReferenceChain.release();
		}
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		final StringBuilder builder = new StringBuilder();
		if (type != null) {
			type.getProposalDescription(builder);
		}
		builder.append(" ").append(KIND);
		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		final StringBuilder text = new StringBuilder(getIdentifier().getDisplayName());
		text.append(" : ");
		text.append(type.getTypename());
		return text.toString();
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		final List<Integer> result = super.getPossibleExtensionStarterTokens();

		if (defaultTemplate == null) {
			result.add(Ttcn3Lexer.ASSIGNMENTCHAR);
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					throw new ReParseException(result);
				}
			}

			if (type != null) {
				if (enveloped) {
					type.updateSyntax(reparser, false);
					reparser.updateLocation(type.getLocation());
				} else if (reparser.envelopsDamage(type.getLocation())) {
					type.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(type.getLocation());
				}
			}

			if (defaultTemplate != null) {
				if (enveloped) {
					defaultTemplate.updateSyntax(reparser, false);
					reparser.updateLocation(defaultTemplate.getLocation());
				} else if (reparser.envelopsDamage(defaultTemplate.getLocation())) {
					defaultTemplate.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(defaultTemplate.getLocation());
				}
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					withAttributesPath.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(withAttributesPath.getLocation());
				}
			}

			if (!enveloped) {
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}

		if (defaultTemplate != null) {
			defaultTemplate.updateSyntax(reparser, false);
			reparser.updateLocation(defaultTemplate.getLocation());
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (type != null) {
			type.findReferences(referenceFinder, foundIdentifiers);
		}
		if (defaultTemplate != null) {
			defaultTemplate.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (type != null && !type.accept(v)) {
			return false;
		}
		if (defaultTemplate != null && !defaultTemplate.accept(v)) {
			return false;
		}
		return true;
	}


	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}
		
		final IType assType = getType(getLastTimeChecked());
		hoverContent.addText(KIND).addText(" ");
		hoverContent.addStyledText(assType != null ? assType.getTypename() : "<?>")
			.addStyledText(" ").addStyledText(getFullName());

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addMembersContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding)
			.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = super.getCompletionItem();
		item.setKind(CompletionItemKind.Class);
		return item;
	}
}
