/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.TitanLanguageServer.ServerMode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.parsers.cfg.CfgAnalyzer;
import org.eclipse.titan.lsp.common.parsers.cfg.CfgDefinitionInformation;
import org.eclipse.titan.lsp.common.parsers.cfg.CfgLocation;
import org.eclipse.titan.lsp.common.parsers.cfg.CfgParseResult;
import org.eclipse.titan.lsp.common.parsers.cfg.CfgParseResult.IncludeFileEntry;
import org.eclipse.titan.lsp.common.parsers.cfg.CfgParseResult.Macro;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.utils.LSPUtils;
import org.eclipse.titan.lsp.core.LspMarker;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.core.Range;
import org.eclipse.titan.lsp.parsers.Status.FutureStatus;


/**
 * This is project level root of all parsing related activities. Every data that
 * was extracted from files while parsing them has its root here.
 * <p>
 * In not noted elsewhere all operations that modify the internal states are
 * executed in a parallel WorkspaceJob, which will have scheduling rules
 * required to access related resources.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public final class ProjectConfigurationParser {
	private static final String SOURCE_ANALYSING = "Analysing the config file";
	private static final String PARSING = "parsing";
	private final Project project;
	private final Map<Path, String> uptodateFiles;
	private final Set<Path> highlySyntaxErroneousFiles;
	private final Map<Path, String> fileMap;
	private final Map<String, CfgDefinitionInformation> definitions;

	/**
	 * Counts how many parallel analyzer threads are running. Should not be
	 * more than 2. It can be 2 if there were changes while the existing
	 * analyzes run, which have to be checked by a subsequent check.
	 * */
	private final AtomicInteger analyzersRunning = new AtomicInteger();

	/**
	 * Basic constructor initializing the class's members, for the given
	 * project.
	 *
	 * @param project
	 *                the project for which this instance will be
	 *                responsible for.
	 * */
	public ProjectConfigurationParser(final Project project) {
		this.project = project;
		uptodateFiles = new ConcurrentHashMap<Path, String>();
		highlySyntaxErroneousFiles = Collections.synchronizedSet(new HashSet<Path>());
		fileMap = new ConcurrentHashMap<Path, String>();
		definitions = new ConcurrentHashMap<String, CfgDefinitionInformation>();
	}

	/**
	 * Get all constant definitions with location information for the
	 * current project.
	 *
	 * @return the constant definitions.
	 */
	public Map<String, CfgDefinitionInformation> getAllDefinitions() {
		return definitions;
	}

	/**
	 * Checks if a given file is already identified as a Runtime
	 * Configuration file.
	 *
	 * @param file
	 *                the file to check
	 * @return true if it is known by the on-the-fly configuration file
	 *         parser to be a valid configuration file
	 * */
	public boolean isFileKnown(final Path file) {
		return (fileMap.get(file) != null);
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date.
	 * <ul>
	 * <li>If on-the-fly parsing is enabled re-analyzes the out-dated file
	 * <li>If it is not enabled, only stores that this file is out of date
	 * for later
	 * </ul>
	 *
	 * @param outdatedFile
	 *                the file which seems to have changed
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> reportOutdating(final Path outdatedFile) {
		return CompletableFuture.supplyAsync(() -> {
			if (uptodateFiles.containsKey(outdatedFile)) {
				uptodateFiles.remove(outdatedFile);
			}
			return FutureStatus.OK_STATUS;
		});
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date.
	 * <ul>
	 * <li>If on-the-fly parsing is enabled re-analyzes the out-dated file
	 * <li>If it is not enabled, only stores that this file is out of date
	 * for later
	 * </ul>
	 *
	 * @param outdatedFiles
	 *                the file which seems to have changed
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> reportOutdating(final List<Path> outdatedFiles) {
		return CompletableFuture.supplyAsync(() -> {
			for (final Path file : outdatedFiles) {
				if (uptodateFiles.containsKey(file)) {
					uptodateFiles.remove(file);
				}
			}
			return FutureStatus.OK_STATUS;
		});
	}

	/**
	 * Removes data related to modules, that were deleted or moved.
	 **/
	private void removedReferencestoRemovedFiles() {
		final List<Path> filesToRemove = new ArrayList<Path>();
		
//		FIXME for (final File file : fileMap.keySet()) {
//			if (!file.isAccessible()) {
//				uptodateFiles.remove(file);
//				filesToRemove.add(file);
//			}
//		}

		for (final Path file : filesToRemove) {
			fileMap.remove(file);
		}
	}

	private FutureStatus internalDoAnalyzeSyntactically() {
		removedReferencestoRemovedFiles();

		//final IContainer[] workingDirectories = ProjectBasedBuilder.getProjectBasedBuilder(project).getWorkingDirectoryResources(false);

		final OutdatedFileCollector visitor = new OutdatedFileCollector(uptodateFiles, highlySyntaxErroneousFiles);
		project.collectOutdatedFiles(visitor);
		
		final List<Path> filesToCheck = visitor.getCFGFilesToCheck();

		for (final Path file : uptodateFiles.keySet()) {
			MarkerHandler.removeMarkers(file.toFile());
		}

		// Remove all markers and definitions from the files that need
		// to be parsed. The innermost loop usually executes once.
		// It's not that expensive. :)
		for ( final Path file : filesToCheck ) {
			removeMarkersAndDefinitions( file );
		}

		// parsing the files
		final List<Macro> macros = new ArrayList<Macro>();
		final Map<String, String> env = System.getenv();

		final List<Path> filesChecked = new ArrayList<Path>();
		while( !filesToCheck.isEmpty() ) {
			final Path file = filesToCheck.get( 0 );
			if( !filesChecked.contains( file ) ) {
				if (!file.toFile().exists()) {
					TitanLogger.logWarning("The file " + file.toString() + " does not seem to exist.");
				} else if (!uptodateFiles.containsKey(file)) {
					// parse the contents of the file
					fileBasedAnalysis( file, macros, filesToCheck, filesChecked );
				}
				filesChecked.add( file );
			}
			filesToCheck.remove( 0 );
		}

		filesToCheck.clear();

		// Check if macro references are valid.
		// This can be done only after all of the files are parsed
		checkMacroErrors( macros, definitions, env );

		// Semantic checking will start here

		if (TitanLanguageServer.getMode() == ServerMode.LanguageServer) {
			for (final Path path : uptodateFiles.keySet()) {
				publishDiagnostics(path);
			}
	
			for (final Path path : filesToCheck) {
				if (!uptodateFiles.containsKey(path)) {
					publishDiagnostics(path);
				}
			}
		}

		return FutureStatus.OK_STATUS;
	}
	
	/**
	 * Remove markers and definitions of file
	 * @param aFile file
	 */
	private void removeMarkersAndDefinitions( final Path aFile ) {
//		MarkerHandler.markAllOnTheFlyMarkersForRemoval( aFile );
//		MarkerHandler.markAllTaskMarkersForRemoval( aFile );
//		final Set<Map.Entry<String, CfgDefinitionInformation>> entries = definitions.entrySet();
//		for ( final Iterator<Map.Entry<String, CfgDefinitionInformation>> mapIter = entries.iterator(); mapIter.hasNext(); ) {
//			final Map.Entry<String, CfgDefinitionInformation> entry = mapIter.next();
//			final CfgDefinitionInformation info = entry.getValue();
//			final List<CfgLocation> list = info.getLocations();
//			for ( final Iterator<CfgLocation> listIter = list.iterator(); listIter.hasNext(); ) {
//				final CfgLocation location = listIter.next();
//				if ( location.getFile().equals( aFile ) ) {
//					listIter.remove();
//				}
//			}
//			if (list.isEmpty()) {
//				mapIter.remove();
//			}
//		}
	}

	public CompletableFuture<FutureStatus> doSyntaticAnalysis() {
		final CompletableFuture<FutureStatus> lastAnalysis = CompletableFuture.supplyAsync(() -> {
			try {
				internalDoAnalyzeSyntactically();
			} finally {
				analyzersRunning.decrementAndGet();
			}

			return FutureStatus.OK_STATUS;
		});
		analyzersRunning.incrementAndGet();

		return lastAnalysis;
	}

	/**
	 * Parses the provided file.
	 *
	 * @param file (in) the file to be parsed
	 * @param aMacros (in/out) collected macro references
	 * @param aFilesChecked files, which are already processed (there are no duplicates)
	 * @param aFilesToCheck files, which will be processed (there are no duplicates)
	 */
	private void fileBasedAnalysis( final Path file,
									final List<Macro> aMacros,
									final List<Path> aFilesToCheck,
									final List<Path> aFilesChecked ) {
		final ProjectItem item = Project.INSTANCE.getProjectItem(file);
		if (item == null) {
			return;
		}
		final String code = item.getSource();
		List<LspMarker> warningsAndErrors = null;
		List<SyntacticErrorStorage> errorsStored = null;

		final String oldConfigFilePath = fileMap.get(file);
		if (oldConfigFilePath != null) {
			fileMap.remove(file);
		}

		final CfgAnalyzer cfgAnalyzer = new CfgAnalyzer();
		cfgAnalyzer.parse(file, code);
		errorsStored = cfgAnalyzer.getErrorStorage();
		final CfgParseResult cfgParseResult = cfgAnalyzer.getCfgParseResult();
		if ( cfgParseResult != null ) {
			warningsAndErrors = cfgParseResult.getWarningsAndErrors();
			aMacros.addAll( cfgParseResult.getMacros() );
			definitions.putAll( cfgParseResult.getDefinitions() );

			// add included files to the aFilesToCheck list
			final List<IncludeFileEntry> includeFiles = cfgParseResult.getIncludeFiles();
			for ( final IncludeFileEntry includeFileEntry : includeFiles ) {
				final String includeFileName = includeFileEntry.getIncludeFileName();
				// example value: includeFileName == MyExample2.cfg
				// example value: file == L/hw/src/MyExample.cfg
				Path includeFilePath;
				try {
					includeFilePath = Path.of(file.toString(), includeFileName);
				} catch (Exception e) {
					TitanLogger.logWarning(oldConfigFilePath);
					continue;
				}
				// example value: includeFilePath == src/MyExample2.cfg
				if ( includeFilePath != null ) {
					try {
						// example value: includeFile == L/hw/src/MyExample2.cfg
						// includeFile is null if the file does not exist in the project
						if ( includeFilePath != null ) {
							if( !uptodateFiles.containsKey( includeFilePath ) &&
								!aFilesChecked.contains( includeFilePath ) &&
								!aFilesToCheck.contains( includeFilePath ) ) {
								//removeMarkersAndDefinitions( includeFile );
								aFilesToCheck.add( includeFilePath );
							}
						} 
					} catch ( final IllegalArgumentException e ) {
						final CfgLocation cfgLocation = includeFileEntry.getLocation();
						final Location location = new Location(file.toFile(), cfgLocation.getStartLine(), cfgLocation.getStartColumn(), 
							cfgLocation.getEndLine(), cfgLocation.getEndColumn());
						location.reportSemanticError(e.getMessage());
					}
				}
			}

//			if (editor != null && editor.getDocument() != null) {
//				final ConfigEditor parentEditor = editor.getParentEditor();
//				if ( errorsStored == null || errorsStored.isEmpty() ) {
//					parentEditor.setParseTreeRoot(cfgParseResult.getParseTreeRoot());
//					parentEditor.setTokens(cfgParseResult.getTokens());
//					parentEditor.refresh(cfgAnalyzer);
//					parentEditor.setErrorMessage(null);
//				} else {
//					if(errorsStored.size()>1) {
//						parentEditor.setErrorMessage("There were " + errorsStored.size() + " problems found while parsing");
//					} else {
//						parentEditor.setErrorMessage("There was 1 problem found while parsing");
//					}
//				}
//			}
		}

		fileMap.put(file, file.toString());
		uptodateFiles.put(file, file.toString());
//
//		if (document != null) {
//			GlobalIntervalHandler.putInterval(document, cfgAnalyzer.getRootInterval());
//		}
//
		if (warningsAndErrors != null) {
			for (final LspMarker marker : warningsAndErrors) {
				if (file.toFile().exists()) {
					final Location location = new Location(file.toFile(), new Range(marker.getDiagnostic().getRange()));
					location.reportProblem(marker.getDiagnostic().getMessage(), marker.getDiagnostic().getSeverity());
				}
			}
		}

		if (errorsStored != null && !errorsStored.isEmpty()) {
			final DiagnosticSeverity severity = LSPUtils.getDiagnosticSeverity(Configuration.INSTANCE.getString(
					Configuration.REPORT_ERRORS_IN_EXTENSION_SYNTAX, GeneralConstants.WARNING));
			for (final SyntacticErrorStorage storage : errorsStored) {
				storage.reportProblem(severity);
			}
		}
	}

	public void doSemanticCheck()
	{
		// TODO semantic checking is not yet supported for configuration files.
		//for (Map.Entry<IFile,List<ModuleParameterSectionHandler.ModuleParameter>> entry : fileToModuleParameters.entrySet())
		//{
		//	doSemanticCheckForFile(entry.getKey());
		//}
	}

	/**
	 * Gets the value of a macro or an environment variable
	 * @param aDefinition macro or environment variable
	 * @param aDefines definitions from the [DEFINE] sections
	 * @param aEnv environment variables
	 * @return macro or environment variable value, or null if there is no such definition
	 */
	private String getDefinitionValue( final String aDefinition,
									   final Map<String, CfgDefinitionInformation> aDefines,
									   final Map<String, String> aEnv){
		if ( aDefines != null && aDefines.containsKey( aDefinition ) ) {
			return aDefines.get( aDefinition ).getValue();
		} else if ( aEnv != null && aEnv.containsKey( aDefinition ) ) {
			return aEnv.get( aDefinition );
		} else {
			return null;
		}
	}

	/**
	 * Checks if all the collected macros are valid,
	 * puts error markers if needed
	 * @param aMacros collected macro references
	 * @param aDefines definitions from the [DEFINE] sections
	 * @param aEnv environment variables
	 */
	public void checkMacroErrors( final List<Macro> aMacros,
								  final Map<String, CfgDefinitionInformation> aDefines,
								  final Map<String, String> aEnv ) {
		for ( final Macro macro : aMacros ) {
			final String value = getDefinitionValue( macro.getMacroName(), aDefines, aEnv );
			if ( value == null ) {
				final File file = macro.getFile();
				if (file != null) {
					final LspMarker marker = macro.getErrorMarker();
					final Location location = new Location(file, new Range(marker.getDiagnostic().getRange()));
					location.reportProblem(marker.getDiagnostic().getMessage(), marker.getDiagnostic().getSeverity(), true);
				}
			}
		}
	}

	private void publishDiagnostics(final Path path) {
		final ProjectItem item = project.getProjectItem(path);
		if (item == null) {
			return;
		}
		final List<Diagnostic> markerList = item.getMarkerList(); 
		PublishDiagnosticsParams diagpar;
		if (item.isUntitled()) {
			final String fname = "untitled:" + path.getFileName().toString();
			diagpar = new PublishDiagnosticsParams(fname, markerList);
		} else {
			diagpar = new PublishDiagnosticsParams(path.toUri().toString(), markerList);
		}
		TitanLanguageServer.getClient().publishDiagnostics(diagpar);
	}
}
