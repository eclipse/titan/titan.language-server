/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsp4j.DiagnosticRelatedInformation;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.codeAction.DiagnosticData;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * An abstraction over code smell markers
 * <p>
 * <code>Marker</code> is an immutable value class, encapsulating any
 * information that is required to create and show a diagnostic with a
 * code smell problem.
 *
 * @author poroszd
 * @author Miklos Magyari
 *
 */
public class Marker {
	/** The location where the marker indicates the problem */
	private final Location location;
	/** The message to show */
	private final String message;
	/** The severity of marked problem */
	private final DiagnosticSeverity severity;
	/** The code smell type */
	private final CodeSmellType problemType;
	/** related diagnostic for paired markers */
	private List<DiagnosticRelatedInformation> relatedInformations;
	/** quick fix data */
	private DiagnosticData diagnosticData = null;

	/**
	 * Creates a marker on a given exact location.
	 *
	 * @param location
	 *            might not be null, but can be {@link NULL_Location}
	 * @param message
	 *            the warning to show in Eclipse
	 * @param severity
	 *            one of {@link IMarker#SEVERITY_INFO},
	 *            {@link IMarker#SEVERITY_WARNING} or
	 *            {@link IMarker#SEVERITY_ERROR}
	 * @param problemType
	 *            the type of the code smell
	 */
	public Marker(final Location location, final String message, final DiagnosticSeverity severity, final CodeSmellType problemType) {
		this.location = location;
		this.message = message;
		this.severity = severity;
		this.problemType = problemType;
	}
	
	public <E extends Enum<E>> Marker(
		final Location location, final String message, final DiagnosticSeverity severity, final CodeSmellType problemType, 
		final List<E> actionTypes, final CodeActionProviderType providerType, final String data) {
		this(location, message, severity, problemType);
		
		diagnosticData = new DiagnosticData(providerType, actionTypes, data);
	}

	public ProjectItem getResource() {
		final Project project = Project.INSTANCE;
		// TODO: Check in which cases file can be null 
		// (Remove null check, then turn Configuration.ENABLE_TITANIUM on and off)
		final File file = location.getFile();
		if (file == null) {
			return null;
		}
		return project.getProjectItem(file.toPath());
	}

	public Position getStartPosition() {
		return location.getStartPosition();
	}

	public Position getEndPosition() {
		return location.getEndPosition();
	}
	
	public Location getLocation() {
		return location;
	}

	public String getMessage() {
		return message;
	}

	public CodeSmellType getProblemType() {
		return problemType;
	}

	public DiagnosticSeverity getSeverity() {
		return severity;
	}

	public void addRelatedInformation(final Location location, final String message) {
		if (relatedInformations == null) {
			relatedInformations = new ArrayList<>();
		}
		relatedInformations.add(new DiagnosticRelatedInformation(location.getLspLocation(), message));
	}

	public List<DiagnosticRelatedInformation> getRelatedInformations() {
		return relatedInformations;
	}
	
	public boolean hasRelatedInformations() {
		return relatedInformations != null;
	}

	public boolean hasQuickFix() {
		return diagnosticData != null;
	}
	
	public DiagnosticData getDiagnosticData() {
		return diagnosticData;
	}
}
