/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class ModuleNameFileNameMismatch extends BaseModuleCodeSmellSpotter {
	private static final String ERROR_MESSAGE = "Module name `{0}'' should match file name `{1}''";
	
	public ModuleNameFileNameMismatch() {
		super(CodeSmellType.MODULE_NAME_FILE_NAME_MISMATCH);
		addStartNode(TTCN3Module.class);
	}
	
	@Override
	public void process(IVisitableNode node, Problems problems) {
		if (node instanceof TTCN3Module) {
			final TTCN3Module module = (TTCN3Module)node;
			if (module.getLocation() == null) {
				return;
			}
			final String modulename = module.getIdentifier().getDisplayName();
			final String filename = 
				module.getLocation().getFile().getName().replaceFirst("[.]ttcn3?$", "");
			if (!modulename.equals(filename)) {
				problems.report(module.getIdentifier().getLocation(), 
					MessageFormat.format(ERROR_MESSAGE, modulename, filename));
			}
		}
	}
}
