/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.InsertReplaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.Range;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Module;

public class ImportContext extends CompletionContext {

	public ImportContext(CompletionContextInfo contextInfo) {
		super(contextInfo);
	}

	@Override
	public Either<List<CompletionItem>, CompletionList> getCompletions() {
		final String identifier = contextInfo.matcher.group(2);
		final Project project = contextInfo.module.getProject();
		final List<CompletionItem> completions = new ArrayList<>();

		project.getAllProjectItems().entrySet().forEach(projectItem -> {
			if (projectItem.getValue().isExcluded()) {
				return;
			}
			final CompletionItem completionItem = new CompletionItem();
			final Module importableModule = projectItem.getValue().getModule();
			if (importableModule == null || importableModule == contextInfo.module) {
				return;
			}

			if (contextInfo.module.getImportedModules().contains(importableModule)) {
				return;
			}

			final Identifier moduleId = importableModule.getIdentifier();
			if (moduleId == null) {
				return;
			}
			final String moduleName = moduleId.getDisplayName();
			completionItem.setLabel(moduleName);
			completionItem.setInsertText(moduleName);
			completionItem.setKind(CompletionItemKind.Module);
			
			if (identifier != null) {
				final Position replacementStartAt = new Position(contextInfo.cursorPosition.getLine(), contextInfo.cursorPosition.getCharacter() - identifier.length());
				final Range replacementRange = new Range(replacementStartAt, contextInfo.cursorPosition);
				final InsertReplaceEdit insRepEdit = new InsertReplaceEdit();
				insRepEdit.setInsert(replacementRange);
				insRepEdit.setReplace(replacementRange);
				insRepEdit.setNewText(completionItem.getInsertText());
				completionItem.setTextEdit(Either.forRight(insRepEdit));
			}
			completions.add(completionItem);
		});
		return Either.forLeft(completions);
	}
}
