/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IIdentifiable;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IType.ValueCheckingOptions;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction.Restriction_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.Function_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Bitstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Charstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value.Operation_type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Function_Reference_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Hexstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Octetstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Referenced_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.SequenceOf_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.SetOf_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Undefined_LowerIdentifier_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.UniversalCharstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ApplyExpression;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a template with a specific value.
 *
 * @author Kristof Szabados
 * */
public final class SpecificValue_Template extends TTCN3Template implements IIdentifiable {

	private final IValue specificValue;

	private ITTCN3Template realTemplate;

	public SpecificValue_Template(final IValue specificValue) {
		this.specificValue = specificValue;

		if (specificValue != null) {
			specificValue.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.SPECIFIC_VALUE;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return new Location(specificValue.getLocation());
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		//Do nothing
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		if (specificValue == null) {
			return ERRONEOUS_TEMPLATE;
		}

		final StringBuilder builder = new StringBuilder();
		builder.append(specificValue.createStringRepresentation());
		return addToStringRepresentation(builder).toString();
	}

	public IValue getSpecificValue() {
		return specificValue;
	}

	@Override
	public Identifier getIdentifier() {
		if (Value_type.UNDEFINED_LOWERIDENTIFIER_VALUE.equals(specificValue.getValuetype())) {
			return ((Undefined_LowerIdentifier_Value) specificValue).getIdentifier();
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (specificValue != null) {
			specificValue.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public final void setMyGovernor(final IType governor) {
		myGovernor = governor;
		if (realTemplate != null && realTemplate != this) {
			realTemplate.setMyGovernor(governor);
		}
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			if (myGovernor != null) {
				return myGovernor;
			}
		}

		if (realTemplate != null && realTemplate != this) {
			return realTemplate.getExpressionGovernor(timestamp, expectedValue);
		}

		if (specificValue != null) {
			specificValue.setMyGovernor(null);
			final IValue temp = specificValue.setLoweridToReference(timestamp);//FIXME erroneousness should be applied here too
			final IType type = temp.getExpressionGovernor(timestamp, expectedValue);
			if (temp.getIsErroneous(timestamp)) {
				isErroneous = true;
			}
			return type;
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (getIsErroneous(timestamp) || specificValue == null) {
			return Type_type.TYPE_UNDEFINED;
		}

		if (realTemplate != null && realTemplate != this) {
			return realTemplate.getExpressionReturntype(timestamp, expectedValue);
		}

		specificValue.setLoweridToReference(timestamp);
		return specificValue.getExpressionReturntype(timestamp, expectedValue);
	}

	@Override
	/** {@inheritDoc} */
	public ITTCN3Template setLoweridToReference(final CompilationTimeStamp timestamp) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return realTemplate;
		}

		lastTimeChecked = timestamp;
		realTemplate = this;
		isErroneous = false;

		final IValue temp = specificValue.setLoweridToReference(timestamp);

		if (Value_type.REFERENCED_VALUE.equals(temp.getValuetype())) {
			final Assignment assignment = ((Referenced_Value) temp).getReference().getRefdAssignment(timestamp, false);

			if (assignment != null) {
				switch (assignment.getAssignmentType()) {
				case A_TEMPLATE:
				case A_VAR_TEMPLATE:
				case A_PAR_TEMP_IN:
				case A_PAR_TEMP_OUT:
				case A_PAR_TEMP_INOUT:
				case A_FUNCTION_RTEMP:
				case A_EXT_FUNCTION_RTEMP:
					realTemplate = setTemplatetype(timestamp, Template_type.TEMPLATE_REFD);
					break;
				default:
					break;
				}
			} else {
				isErroneous = true;
			}
		}

		return realTemplate;
	}

	@Override
	/** {@inheritDoc} */
	public TTCN3Template getTemplateReferencedLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (realTemplate != null && realTemplate != this) {
			return realTemplate.getTemplateReferencedLast(timestamp, referenceChain);
		}

		return this;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		if (realTemplate != null && realTemplate != this) {
			return realTemplate.checkExpressionSelfReferenceTemplate(timestamp, lhs);
		}

		final IType governor = specificValue.getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
		if (governor == null) {
			return false;
		}

		return governor.checkThisValue(timestamp, specificValue, lhs, new ValueCheckingOptions(Expected_Value_type.EXPECTED_DYNAMIC_VALUE, true, true, false, false, false));
	}

	@Override
	/** {@inheritDoc} */
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		if (isBuildCancelled() || specificValue == null) {
			return;
		}

		final IValue last = specificValue.setLoweridToReference(timestamp);
		switch (last.getValuetype()) {
		case EXPRESSION_VALUE:
			// checked later
			break;
		case OMIT_VALUE:
			if (!allowOmit) {
				getLocation().reportSemanticError(SPECIFIC_VALUE_EXPECTED_OMIT);
			}
			return;
		case REFERENCED_VALUE: {
			final ITTCN3Template template = getTemplateReferencedLast(timestamp);
			switch (template.getTemplatetype()) {
			case OMIT_VALUE:
				if (!allowOmit) {
					getLocation().reportSemanticError(SPECIFIC_VALUE_EXPECTED_OMIT);
				}
				return;
			case SPECIFIC_VALUE: {
				final IValue refd = ((SpecificValue_Template)template).getSpecificValue();
				if (Value_type.OMIT_VALUE.equals(refd.getValuetype()) && !allowOmit) {
					getLocation().reportSemanticError(SPECIFIC_VALUE_EXPECTED_OMIT);
				}
				return;
			}
			default:
				return;
			}
		}
		default:
			return;
		}

		final Expression_Value expressionValue = (Expression_Value) specificValue;
		if (!Operation_type.APPLY_OPERATION.equals(expressionValue.getOperationType())) {
			return;
		}

		expressionValue.setLoweridToReference(timestamp);
		IType type = ((ApplyExpression) expressionValue).getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
		if (type == null) {
			return;
		}

		type = type.getTypeRefdLast(timestamp);

		if (Type_type.TYPE_FUNCTION.equals(type.getTypetype()) && ((Function_Type) type).returnsTemplate()) {
			final ITTCN3Template template = setTemplatetype(timestamp, Template_type.TEMPLATE_INVOKE);
			template.checkSpecificValue(timestamp, allowOmit);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected void checkTemplateSpecificLengthRestriction(final CompilationTimeStamp timestamp, final Type_type typeType) {
		if (isBuildCancelled()) {
			return;
		}
		
		IValue value = getValue();
		final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		value = value.getValueRefdLast(timestamp, referenceChain);
		referenceChain.release();

		switch (value.getValuetype()) {
		case BITSTRING_VALUE:
			if (Type_type.TYPE_BITSTRING.equals(typeType)) {
				lengthRestriction.checkNofElements(timestamp, ((Bitstring_Value) value).getValueLength(), false, false, false, this);
			}
			break;
		case HEXSTRING_VALUE:
			if (Type_type.TYPE_HEXSTRING.equals(typeType)) {
				lengthRestriction.checkNofElements(timestamp, ((Hexstring_Value) value).getValueLength(), false, false, false, this);
			}
			break;
		case OCTETSTRING_VALUE:
			if (Type_type.TYPE_OCTETSTRING.equals(typeType)) {
				lengthRestriction
				.checkNofElements(timestamp, ((Octetstring_Value) value).getValueLength(), false, false, false, this);
			}
			break;
		case CHARSTRING_VALUE:
			if (Type_type.TYPE_CHARSTRING.equals(typeType)) {
				lengthRestriction.checkNofElements(timestamp, ((Charstring_Value) value).getValueLength(), false, false, false, this);
			} else if (Type_type.TYPE_UCHARSTRING.equals(typeType)) {
				value = value.setValuetype(timestamp, Value_type.UNIVERSALCHARSTRING_VALUE);
				lengthRestriction.checkNofElements(timestamp, ((UniversalCharstring_Value) value).getValueLength(), false, false,
						false, this);
			}
			break;
		case UNIVERSALCHARSTRING_VALUE:
			if (Type_type.TYPE_UCHARSTRING.equals(typeType)) {
				lengthRestriction.checkNofElements(timestamp, ((UniversalCharstring_Value) value).getValueLength(), false, false,
						false, this);
			}
			break;
		case SEQUENCEOF_VALUE:
			if (Type_type.TYPE_SEQUENCE_OF.equals(typeType)) {
				lengthRestriction.checkNofElements(timestamp, ((SequenceOf_Value) value).getNofComponents(), false, false, false,
						this);
			}
			break;
		case SETOF_VALUE:
			if (Type_type.TYPE_SET_OF.equals(typeType)) {
				lengthRestriction.checkNofElements(timestamp, ((SetOf_Value) value).getNofComponents(), false, false, false, this);
			}
			break;
		case OMIT_VALUE:
			lengthRestriction.getLocation().reportSemanticError(LENGTHRESTRICTIONERROR_OMIT);
			break;
		default:
			// we cannot verify anything on other value types,
			// they are either correct or not applicable to the type
			break;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (referenceChain.add(this) && realTemplate != null && realTemplate != this && !realTemplate.getIsErroneous(timestamp)) {
			realTemplate.checkRecursions(timestamp, referenceChain);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {
		
		if (getIsErroneous(timestamp)) {
			return false;
		}

		if (type == null) {
			return false;
		}

		boolean selfReference = false;
		type.check(timestamp);

		if (specificValue != null) {
			specificValue.setMyGovernor(type);
			final IValue temporalValue = type.checkThisValueRef(timestamp, specificValue);
			temporalValue.setMyGovernor(specificValue.getMyGovernor());
			selfReference = type.checkThisValue(timestamp, temporalValue, lhs, new ValueCheckingOptions(Expected_Value_type.EXPECTED_TEMPLATE, isModified,
					allowOmit, true, implicitOmit, false));
		}

		checkLengthRestriction(timestamp, type);
		if (!allowOmit && isIfpresent) {
			if (location != null && !NULL_Location.getInstance().equals(location)) {
				location.reportSemanticError(IFPRESENT_NOT_ALLOWED);
			} else if (specificValue != null && !NULL_Location.getInstance().equals(specificValue.getLocation())) {
				specificValue.getLocation().reportSemanticError(IFPRESENT_NOT_ALLOWED);
			}
		}
		if (subCheck) {
			type.checkThisTemplateSubtype(timestamp, this);
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isValue(final CompilationTimeStamp timestamp) {
		if (lengthRestriction != null || isIfpresent || getIsErroneous(timestamp)) {
			return false;
		}

		if (realTemplate != null && realTemplate != this) {
			return realTemplate.isValue(timestamp);
		}

		if (Value_type.FUNCTION_REFERENCE_VALUE.equals(specificValue.getValuetype())) {
			final IType governor = ((Function_Reference_Value) specificValue).getExpressionGovernor(timestamp,
					Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
			if (governor == null) {
				return true;
			}

			final IType last = governor.getTypeRefdLast(timestamp);
			if (Type_type.TYPE_FUNCTION.equals(last.getTypetype()) && ((Function_Type) last).returnsTemplate()) {
				return false;
			}
		} else if (Value_type.REFERENCED_VALUE.equals(specificValue.getValuetype()) ) {
			final Reference reference = ((Referenced_Value) specificValue).getReference();
			final Assignment assignment = reference.getRefdAssignment(timestamp, true);
			if (assignment == null) {
				return true;
			}

			switch (assignment.getAssignmentType()) {
			case A_CONST:
			case A_TIMER:
			case A_EXT_CONST:
			case A_PAR_VAL:
			case A_PAR_VAL_IN:
			case A_PAR_VAL_OUT:
			case A_PAR_VAL_INOUT:
			case A_VAR:
			case A_FUNCTION_RVAL:
			case A_EXT_FUNCTION_RVAL:
			case A_MODULEPAR:
			case A_MODULEPAR_TEMPLATE:
				return true; //runtime evaluation!
			case A_TEMPLATE:
			case A_PAR_TEMP_IN:
			case A_PAR_TEMP_INOUT:
			case A_FUNCTION_RTEMP:
			case A_VAR_TEMPLATE:
				boolean result = true;
				final Restriction_type rt = ((Definition) assignment).getTemplateRestriction();
				if (TemplateRestriction.Restriction_type.TR_OMIT.equals(rt) ||
						TemplateRestriction.Restriction_type.TR_PRESENT.equals(rt)) {
					result = false;
				}

				final ITTCN3Template ttemplate = getTemplateReferencedLast(timestamp);
				if (!ttemplate.equals(this) && ttemplate.isValue(timestamp)) {
					return result; //ok
				} else {
					return false;
				}
			default:
				return false;
			}
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IValue getValue() {
		if (realTemplate != null && realTemplate != this) {
			return realTemplate.getValue();
		}

		return specificValue;
	}

	/**
	 * @return true if the template is a reference.
	 * */
	public boolean isReference() {
		if (lengthRestriction != null || isIfpresent || specificValue == null) {
			return false;
		}

		switch (specificValue.getValuetype()) {
		case UNDEFINED_LOWERIDENTIFIER_VALUE:
			return true;
		case REFERENCED_VALUE:
			return ((Referenced_Value) specificValue).getReference() != null;
		default:
			return false;
		}
	}

	/**
	 * Returns the reference that should be used where this template is used
	 * as a referencing template.
	 *
	 * @return the reference.
	 * */
	public Reference getReference() {
		if (lengthRestriction != null || isIfpresent || specificValue == null) {
			return null;
		}

		switch (specificValue.getValuetype()) {
		case UNDEFINED_LOWERIDENTIFIER_VALUE:
			return ((Undefined_LowerIdentifier_Value) specificValue).getAsReference();
		case REFERENCED_VALUE:
			return ((Referenced_Value) specificValue).getReference();
		default:
			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public ITTCN3Template setTemplatetype(final CompilationTimeStamp timestamp, final Template_type newType) {
		lastTimeChecked = timestamp;

		switch (newType) {
		case TEMPLATE_REFD:
			realTemplate = new Referenced_Template(timestamp, this);
			break;
		case TEMPLATE_INVOKE:
			realTemplate = new Invoke_Template(timestamp, this);
			break;
		default:
			realTemplate = super.setTemplatetype(timestamp, newType);
		}

		return realTemplate;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, omitAllowed, usageLocation);
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (specificValue != null) {
			specificValue.updateSyntax(reparser, false);
			reparser.updateLocation(specificValue.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (specificValue == null) {
			return;
		}

		specificValue.findReferences(referenceFinder, foundIdentifiers);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (specificValue != null && !specificValue.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyDefinition(Definition definition) {
		if (realTemplate != null && realTemplate != this) {
			realTemplate.setMyDefinition(definition);
		}
	}
}
