/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.IIdentifierContainer;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a single attribute qualifier.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class Qualifier
	implements ILocateableNode, IIncrementallyUpdatable, IIdentifierContainer, IVisitableNode, ICollection<ISubReference> {
	public static final String INVALID_INDEX_QUALIFIER = "Invalid field qualifier [-]";
	public static final String INVALID_FIELD_QUALIFIER = "Invalid field qualifier {0}";

	/**
	 * The location of the whole qualifier. This location encloses the
	 * qualifier fully, as it is used to report errors to.
	 */
	private Location location = NULL_Location.INSTANCE;

	private final List<ISubReference> subReferences = new ArrayList<ISubReference>(1);

	/** Definition is set during semantic check or null */
	private Definition definition = null;

	public void setDefinition(final Definition definition) {
		this.definition = definition;
	}

	public String getDisplayName() {
		final StringBuilder sb = new StringBuilder();
		for (final ISubReference subref : subReferences) {
			subref.appendDisplayName(sb);
		}
		return sb.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	public boolean add(final ISubReference subReference) {
		if (subReference != null) {
			return subReferences.add(subReference);
		}
		return false;
	}

	@Override
	public int size() {
		return subReferences.size();
	}

	@Override
	public ISubReference get(final int index) {
		return subReferences.get(index);
	}

	/**
	 * Handles the incremental parsing of this list of qualifiers.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged {@code true} if the location contains the damaged area,
	 *                {@code false} if only its' location needs to be updated.
	 */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final ISubReference subReference : subReferences) {
			subReference.updateSyntax(reparser, false);
			reparser.updateLocation(subReference.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (definition != null) {
			// TODO: the following for() does not work because qualifiers were not semantically analyzed
			for (final ISubReference sr : subReferences) {
				sr.findReferences(referenceFinder, foundIdentifiers);
			}
			if (referenceFinder.fieldId != null) {
				// we are searching for a field of a type
				final IType t = definition.getType(CompilationTimeStamp.getBaseTimestamp());
				if (t == null) {
					return;
				}
				final List<IType> typeArray = new ArrayList<IType>();
				final Reference reference = new Reference(null);
				reference.addSubReference(new FieldSubReference(definition.getIdentifier()));
				for (final ISubReference sr : subReferences) {
					reference.addSubReference(sr);
				}
				reference.setLocation(location);
				reference.setMyScope(definition.getMyScope());
				final boolean success = t.getFieldTypesAsArray(reference, 1, typeArray);
				if (!success) {
					// TODO: maybe a partially erroneous reference could be searched too
					return;
				}
				if (subReferences.size() != typeArray.size()) {
					TitanLogger.logFatal();
					return;
				}
				for (int i = 0; i < subReferences.size(); i++) {
					if (typeArray.get(i) == referenceFinder.type && !(subReferences.get(i) instanceof ArraySubReference)
							&& subReferences.get(i).getId().equals(referenceFinder.fieldId)) {
						foundIdentifiers.add(new Hit(subReferences.get(i).getId()));
					}
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final ISubReference sr : subReferences) {
			if (!sr.accept(v)) {
				return false;
			}
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}

	@Override
	public Iterator<ISubReference> iterator() {
		return subReferences.iterator();
	}
}
