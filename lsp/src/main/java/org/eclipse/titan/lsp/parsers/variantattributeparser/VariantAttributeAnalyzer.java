/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.variantattributeparser;

import java.io.StringReader;
import java.util.concurrent.atomic.AtomicBoolean;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.BerAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.JsonAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST;

/**
 * Variant attribute parser analyzer
 *
 * TODO currently does only syntactic checking,
 * only BER, JSON, RAW coding data structure is extracted
 *
 * @author Kristof Szabados
 *
 */
public final class VariantAttributeAnalyzer {

	private VariantAttributeAnalyzer() {
		//intentionally empty
	}

	public static void parse(final RawAST rawAST, final JsonAST jsonAST,final BerAST berAST, final AttributeSpecification specification, 
						final int lengthMultiplier, final AtomicBoolean raw_found, final AtomicBoolean json_found, final AtomicBoolean ber_found) {
		final Location location = specification.getLocation();
		final StringReader reader = new StringReader(specification.getSpecification());
		final CharStream charStream = new UnbufferedCharStream(reader);
		final VariantAttributeLexer lexer = new VariantAttributeLexer(charStream);
		lexer.setTokenFactory(new CommonTokenFactory(true));
		final TitanErrorListener lexerListener = new TitanErrorListener(location.getFile());
		lexer.removeErrorListeners();
		lexer.addErrorListener(lexerListener);

		final CommonTokenStream tokenStream = new CommonTokenStream( lexer );
		final VariantAttributeParser parser = new VariantAttributeParser( tokenStream );
		parser.setBuildParseTree(false);

		final TitanErrorListener parserListener = new TitanErrorListener(location.getFile());
		parser.removeErrorListeners();
		parser.addErrorListener(parserListener);
		parser.setActualFile(location.getFile());
		parser.setLine(location.getStartLine());
		parser.setOffsetPosition(location.getStartPosition());

		MarkerHandler.removeMarkers(location.getFile(),
				location.getStartPosition(), location.getEndPosition());

		parser.setRawAST(rawAST);
		parser.setJsonAST(jsonAST);
		parser.setBerAST(berAST);
		parser.setLengthMultiplier(lengthMultiplier);
		parser.pr_AttribSpec();

		for (final SyntacticErrorStorage ses : lexerListener.getErrorsStored()) {
			ses.reportSyntacticError();
		}
		for (final SyntacticErrorStorage ses : parserListener.getErrorsStored()) {
			ses.reportSyntacticError();
		}

		if (!raw_found.get()) {
			raw_found.set(parser.getRawFound());
		}

		if (!json_found.get()) {
			json_found.set(parser.getJsonFound());
		}
		
		if (!ber_found.get()) {
			ber_found.set(parser.getBerFound());
		}
	}
}
