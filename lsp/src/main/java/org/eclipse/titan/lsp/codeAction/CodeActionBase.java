/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;

import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.common.logging.TitanLogger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Miklos Magyari
 * @author Csilla Farkas
 */
public class CodeActionBase implements ICodeActionProvider {
	protected TextDocumentIdentifier doc;
	protected Diagnostic diagnostic;

	public CodeActionBase(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		this.doc = Objects.requireNonNull(doc);
		this.diagnostic = Objects.requireNonNull(diagnostic);
	}

	/**
	 * Generic method to provide the list of code actions for a given code action type.
	 * 
	 * @param <T>
	 * @param codeActionList
	 * @param classType
	 * @return
	 */
	protected <T extends Enum<T>> List<Either<Command, CodeAction>> provideCodeActions(
		final Map<T, BiFunction<TextDocumentIdentifier, Diagnostic, CodeAction>> codeActionList,
		final Class<T> classType
	) {
		final List<Either<Command, CodeAction>> codeActions = new ArrayList<>();
		final Object data = diagnostic.getData();
		if (data instanceof DiagnosticData) {
			final Optional<List<T>> actionTypes = ((DiagnosticData) data).getCodeActionTypes(classType);
			actionTypes.ifPresent(types -> {
				for (T type : types) {
					try {
						final CodeAction codeAction = codeActionList.get(type).apply(doc, diagnostic);
						codeActions.add(Either.forRight(codeAction));
					} catch (IllegalArgumentException e) {
						TitanLogger.logError(e);
					}
				}
			});
		}
		return codeActions;
	}

	/**
	 * Generic method to extract code action data from the diagnostic. 
	 * @param <T>
	 * @param diagnostic
	 * @param classType
	 * @return
	 */
	protected static <T> T getCodeActionData(final Diagnostic diagnostic, Class<T> classType) {
		final DiagnosticData diagnosticData = (DiagnosticData)diagnostic.getData();
		final Optional<Object> actionData = diagnosticData.getCodeActionData();
		GsonBuilder builder = new GsonBuilder();
		CodeActionHelpers.registerTypeAdapters(builder);
		Gson gson = builder.create();
		return gson.fromJson((String)actionData.orElse(""), classType);
	}
}
