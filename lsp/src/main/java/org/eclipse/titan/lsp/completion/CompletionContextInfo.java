/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.regex.Matcher;

import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Statement;
import org.eclipse.titan.lsp.core.Position;

/**
 * Helper class for storing common information
 * 
 * @author Adam Knapp
 * @author Csilla Farkas
 *
 */
public class CompletionContextInfo {
	/** Position in the document, where proposals are requested */
	public Position cursorPosition;
	/** Document in which proposals are requested */
	public TextDocumentIdentifier document;
	/** Module in which proposals are requested */
	public Module module;
	/** Scope in which proposals are requested */
	public Scope scope;
	/** The text between the cursor position and the last instruction */
	public String context;
	public String cleanedContext;
	/** matcher for the regex that matched */
	public Matcher matcher;
	/** Dot context */
	public boolean isDotContext;
	
	public Statement actualStatement;
	public Definition actualDefinition;
	
	/** Create an unfilled object */
	public CompletionContextInfo() {}
	
	/**
	 * Fills the fields according to the input parameters
	 * @param cursorPosition Position in the document, where proposals are requested
	 * @param document Document in which proposals are requested
	 * @param module Module in which proposals are requested
	 * @param scope Scope in which proposals are requested
	 * @param context The text between the position and the last instruction
	 */
	public CompletionContextInfo(
			final Position cursorPosition,
			final TextDocumentIdentifier document,
			final Module module,
			final Scope scope,
			final String context) {
		this.cursorPosition = cursorPosition;
		this.document = document;
		this.module = module;
		this.scope = scope;
		this.context = context;
		this.cleanedContext = cleanContext(context);
		this.isDotContext = context != null && context.endsWith(GeneralConstants.DOT);
	}
	
	public String cleanContext(String rawContext) {
		if (rawContext == null) {
			return null;
		}
		// remove comments
		String regex = "//[^\n]*|/\\*(.|\\R)*?\\*/";
		return rawContext
				.replaceAll(regex, GeneralConstants.EMPTY_STRING)
				.replace(";", GeneralConstants.EMPTY_STRING)
				.trim();
	}
}
