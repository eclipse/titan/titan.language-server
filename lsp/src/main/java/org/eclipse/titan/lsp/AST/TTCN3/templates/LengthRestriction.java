/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimension;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a range restriction.
 *
 * @author Kristof Szabados
 * */
public abstract class LengthRestriction extends ASTNode implements ILocateableNode, IIncrementallyUpdatable, ICancelBuild {

	/**
	 * The location of the whole restriction. This location encloses the
	 * restriction fully, as it is used to report errors to.
	 **/
	private Location location = NULL_Location.INSTANCE;

	@Override
	/** {@inheritDoc} */
	public final void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getLocation() {
		return location;
	}

	/**
	 * Creates and returns a string representation if the length
	 * restriction.
	 *
	 * @return the string representation of the length restriction.
	 * */
	public abstract String createStringRepresentation();

	/**
	 * Check that the length restriction is a correct value, and at is
	 * allowed at a given location.
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 * @param expectedValue
	 *                the value kind expected.
	 * */
	public abstract void check(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue);

	/**
	 * Checks if the length restriction is valid for the array type.
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 * @param dimension
	 *                the dimension of the array type.
	 * */
	public abstract void checkArraySize(final CompilationTimeStamp timestamp, final ArrayDimension dimension);

	/**
	 * Checks if the provided amount of elements can be valid or not
	 * according to this length restriction.
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 * @param nofElements
	 *                the number of elements the checked template has.
	 * @param lessAllowed
	 *                wheter less elements should be accepted (subset
	 *                template)
	 * @param moreAllowed
	 *                wheter more elements should be accepted (the template
	 *                has anyornone elements).
	 * @param hasAnyornone
	 *                whether the template has anyornone elements.
	 * @param locatable
	 *                the location errors should be reported to if found.
	 * */
	public abstract void checkNofElements(final CompilationTimeStamp timestamp, final int nofElements, boolean lessAllowed,
			final boolean moreAllowed, final boolean hasAnyornone, final ILocateableNode locatable);
}
