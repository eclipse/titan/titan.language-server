/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;
import java.util.regex.Matcher;

import org.eclipse.titan.lsp.AST.Reference;

/**
 * Helper class for handling assignment completion proposals
 * 
 * @author Miklos Magyari
 * @author Csilla Farkas
 * */
public class AssignmentMatch {

	private String name;
	private String prefix;
	private String typeName;

	public AssignmentMatch(CompletionContextInfo contextInfo) {
		final Matcher matcher = contextInfo.matcher;
		name =  matcher.group(3);
		prefix = matcher.group(4);
		typeName = getUnfinishedAssTypeName(contextInfo);
	}

	private String getUnfinishedAssTypeName(CompletionContextInfo contextInfo) {
		final String varType = contextInfo.matcher.group(2);
		if (varType == null || varType.isEmpty()) {
			final Definition referencedDefinition = getReferencedDefinition(contextInfo);
			if (referencedDefinition == null) {
				return null;
			}

			return getDefinitionTypeName(contextInfo.module.getLastCompilationTimeStamp(), referencedDefinition);
		}

		return varType;
	}

	private Definition getReferencedDefinition(CompletionContextInfo contextInfo) {
		final Reference reference = TTCN3ReferenceAnalyzer.parseForCompletion(contextInfo.module.getLocation().getFile(), name);
		if (reference == null) {
			return null;
		}

		final Assignment assignment = contextInfo.scope.getAssBySRef(contextInfo.module.getLastCompilationTimeStamp(), reference);
		if (assignment == null) {
			return null;
		}

		if (assignment instanceof Definition) {
			return (Definition)assignment;
		}

		return null;
	}

	public String getDefinitionTypeName(CompilationTimeStamp timestamp, Definition definition) {
		IType defType = definition.getType(timestamp);
		if (defType == null) {
			return null;
		}
		String defTypeName = defType.getTypename();

		if (defType instanceof Referenced_Type) {
			defTypeName = ((Referenced_Type)defType).getReference().getDisplayName();
		}

		return defTypeName;
	}

	public boolean isCompletionMatch(CompilationTimeStamp timestamp, Definition definition) {
		if (definition instanceof Def_Type) {
			return false;
		}

		final String checkedDefinitionTypeName = getDefinitionTypeName(timestamp, definition);
		if (checkedDefinitionTypeName != null && typeName != null) {
			return typeName.equals(checkedDefinitionTypeName);
		}

		return false;
	}

	public boolean isTypeDefinitionMatch(Definition definition) {
		if (definition instanceof Def_Type) {
			return ((Def_Type)definition).getIdentifier().getDisplayName().equals(typeName);
		}
		return false;
	}
}
