/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.core;

/** 
 * An interface for comparing location objects (positions, ranges)
 * 
 * @author Miklos Magyari
 *
 */
public interface ILocationComparer {
	/**
	 * Checks if this LSP location is located before the other location specified by the parameter.
	 * In case the two locations point to the same location, false is returned.
	 * For ranges, start positions are compared.
	 * 
	 * @param other
	 * @return
	 */
	default boolean before(final ILocationComparer other) {
		return false;
	}
	
	/**
	 * Checks if this LSP location is located before or at the same location specified by the parameter.
	 * For ranges, start positions are compared.
	 * @param other
	 * @return
	 */
	default boolean beforeOrEquals(final ILocationComparer other) {
		return false;
	}
	
	/**
	 * Checks if this LSP location is located after the other location specified by the parameter.
	 * In case the two locations point to the same location, false is returned.
	 * For ranges, start positions are compared.
	 * @param other
	 * @return
	 */
	default boolean after(final ILocationComparer other) {
		return false;
	}
	
	/**
	 * Checks if this LSP location is located after or at the same location specified by the parameter.
	 * For ranges, start positions are compared.
	 * @param other
	 * @return
	 */
	default boolean afterOrEquals(final ILocationComparer other) {
		return false;
	}
}
