/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Choice_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Sequence_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Set_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.Open_Type;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.BerAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.JsonAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Modifier_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.types.AbstractOfType;
import org.eclipse.titan.lsp.AST.TTCN3.types.Anytype_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Array_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.CompField;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Set_Seq_Choice_BaseType;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.IParsedSubType;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.SubType;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ExpressionStruct;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public interface IType extends IGovernor, IIdentifierContainer, IVisitableNode, IReferenceChainElement, IIncrementallyUpdatable {

	String UNKNOWN_REFERRED_TYPE = "<unknown_referred_type>";

	enum Type_type {
		// special values never instantiated
		/** undefined. */
		TYPE_UNDEFINED("<undefined>"),

		// common types (they reside among the TTCN-3 type package)
		/** bitstring. */
		TYPE_BITSTRING("bitstring"),
		/** boolean. */
		TYPE_BOOL("boolean"),
		/** integer. */
		TYPE_INTEGER("integer"),
		/** octetstring. */
		TYPE_OCTETSTRING("octetstring"),
		/** object identifier. */
		TYPE_OBJECTID("objid"),
		/** real / float. */
		TYPE_REAL("float"),
		/** referenced directly by pointer. */
		TYPE_REFD_SPEC("referenced type"),
		/** referenced. */
		TYPE_REFERENCED("referenced type"),
		/** sequence of. */
		TYPE_SEQUENCE_OF("sequence of"),
		/** set of. */
		TYPE_SET_OF("set of"),

		// TTCN-3 types
		/** address (TTCN-3). */
		TYPE_ADDRESS("address"),
		/** altstep (TTCN-3). */
		TYPE_ALTSTEP("altstep"),
		/** anytype (TTCN-3). */
		TYPE_ANYTYPE("anytype"),
		/** array (TTCN-3). */
		TYPE_ARRAY("array"),
		/** character string (TTCN-3). */
		TYPE_CHARSTRING("charstring"),
		/** class (TTCN-3 extension) */
		TYPE_CLASS("class"),
		/** component (TTCN-3). */
		TYPE_COMPONENT("component"),
		/** default (TTCN-3). */
		TYPE_DEFAULT("default"),
		/** function (TTCN-3). */
		TYPE_FUNCTION("function"),
		/** hexadecimal string (TTCN-3). */
		TYPE_HEXSTRING("hexstring"),
		/** map type (TTCN-3) */
		TYPE_MAP("map"),
		/** port (TTCN-3). */
		TYPE_PORT("port"),
		/** property (TTCN-3 OOP extension) */
		TYPE_PROPERTY("property"),
		/** signature (TTCN-3). */
		TYPE_SIGNATURE("signature"),
		/** testcase (TTCN-3). */
		TYPE_TESTCASE("testcase"),
		/** Union (TTCN-3). */
		TYPE_TTCN3_CHOICE("union"),
		/** enumeration (TTCN-3). */
		TYPE_TTCN3_ENUMERATED("enumerated"),
		/** Sequence (TTCN-3). */
		TYPE_TTCN3_SEQUENCE("record"),
		/** Set (TTCN-3). */
		TYPE_TTCN3_SET("set"),
		/** unversal charstring (TTCN-3). */
		TYPE_UCHARSTRING("universal charstring"),
		/** verdict type (TTCN-3). */
		TYPE_VERDICT("verdicttype"),

		// ASN.1 types
		/** ANY (ASN.1). */
		TYPE_ANY("ANY"),
		/** choice (ASN.1). */
		TYPE_ASN1_CHOICE("choice"),
		/** enumeration (ASN.1). */
		TYPE_ASN1_ENUMERATED("enumerated"),
		/** sequence (ASN.1). */
		TYPE_ASN1_SEQUENCE("sequence"),
		/** set (ASN.1). */
		TYPE_ASN1_SET("set"),
		/** bitstring (ASN.1). */
		TYPE_BITSTRING_A("bitstring"),
		/** bmpString (ASN.1). */
		TYPE_BMPSTRING("BMPString"),
		/** embedded_pdv (ASN.1). */
		TYPE_EMBEDDED_PDV("EMBEDDED PDV"),
		/** external (ASN.1). */
		TYPE_EXTERNAL("EXTERNAL"),
		/** generalised time (ASN.1). */
		TYPE_GENERALIZEDTIME("GeneralizedTime"),
		/** generalString (ASN.1). */
		TYPE_GENERALSTRING("GeneralString"),
		/** graphicstring (ASN.1). */
		TYPE_GRAPHICSTRING("GraphicString"),
		/** IA5String (ASN.1). */
		TYPE_IA5STRING("IA5String"),
		/** Integer type (ASN.1). */
		TYPE_INTEGER_A("integer"),
		/** null type (ASN.1). */
		TYPE_NULL("NULL"),
		/** numericString (ASN.1). */
		TYPE_NUMERICSTRING("NumericString"),
		/** ObjectClassField (ASN.1). */
		TYPE_OBJECTCLASSFIELDTYPE("object class field type"),
		/** objectdescriptor. */
		TYPE_OBJECTDESCRIPTOR("ObjectDescriptor"),
		/** opentype (ASN.1). */
		TYPE_OPENTYPE("open type"),
		/** printablestring (ASN.1). */
		TYPE_PRINTABLESTRING("PrintableString"),
		/** relative object identifier (ASN.1). */
		TYPE_ROID("objid"),
		/** selection (ASN.1). */
		TYPE_SELECTION("selection type"),
		/** teletexstring (ASN.1). */
		TYPE_TELETEXSTRING("TeletexString"),
		/** universalString (ASN.1). */
		TYPE_UNIVERSALSTRING("UniversalString"),
		/** unrestrictedString (ASN.1). */
		TYPE_UNRESTRICTEDSTRING("CHARACTER STRING"),
		/** UTCtime (ASN.1). */
		TYPE_UTCTIME("UTCTime"),
		/** UTF8String (ASN.1). */
		TYPE_UTF8STRING("UTF8String"),
		/** videotexstring (ASN.1). */
		TYPE_VIDEOTEXSTRING("VideotexString"),
		/** visiblestring (ASN.1). */
		TYPE_VISIBLESTRING("VisibleString");

		final String name;

		Type_type(final String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	/**
	 * Encoding types.
	 *
	 * TODO only RAW is supported for now
	 * */
	enum MessageEncoding_type {
		/** not yet defined */
		UNDEFINED("<unknown encoding>"),
		/** ASN.1 BER encoding (built-in) */
		BER("BER"),
		/** ASN.1 PER encoding (through user defined coder functions) */
		PER("PER"),
		/** ASN.1 OER (built-in) */
		OER("OER"),
		/** raw encoding (built-in) */
		RAW("RAW"),
		/** text encoding (built-in) */
		TEXT("TEXT"),
		/** xer encoding (built-in) */
		XER("XER"),
		/** json encoding (built-in) */
		JSON("JSON"),
		/** custom encoding (through user defined coder functions) */
		CUSTOM("CUSTOM");

		private String name;

		MessageEncoding_type(final String name) {
			this.name = name;
		}

		public String getEncodingName() {
			return name;
		}

		@Override
		public String toString() {
			return name;
		}

		/**
		 * Returns the encoding type belonging to the provided name.
		 * Similar than the implicit {@code valueOf}, but some additional string value is also allowed.
		 * @param encoding the name of the encoding type to identify
		 * @return encoding type, never returns {@code null}
		 */
		public static MessageEncoding_type getEncodingType(final String encoding) {
			if (encoding == null) {
				return UNDEFINED;
			}
			switch(encoding) {
			case("BER"):
			case("BER:2002"):
			case("CER:2002"):
			case("DER:2002"):
				return BER;
			case("OER"):
				return OER;
			case("PER"):
				return PER;
			case("JSON"):
				return JSON;
			case("RAW"):
				return RAW;
			case("TEXT"):
				return TEXT;
			case("XER"):
			case("XML"):
				return XER;
			default:
				return CUSTOM;
			}
		}
	}

	/**
	 * Enumeration to represent the owner of the type.
	 */
	enum TypeOwner_type {
		OT_UNKNOWN,
		/** ASN.1 type assignment (Ass_T) */
		OT_TYPE_ASS,
		/** ASN.1 variable assignment (Ass_V) */
		OT_VAR_ASS,
		/** ASN.1 value set assignment (Ass_VS) */
		OT_VSET_ASS,
		/** ASN.1 TypeFieldSpec (FieldSpec_T) */
		OT_TYPE_FLD,
		/** ASN.1 FixedTypeValueFieldSpec (FieldSpec_V_FT) */
		OT_FT_V_FLD,
		/** TTCN-3 TypeMapping */
		OT_TYPE_MAP,
		/** TTCN-3 TypeMappingTarget */
		OT_TYPE_MAP_TARGET,
		/** TTCN-3 type definition (Def_Type) */
		OT_TYPE_DEF,
		/** TTCN-3 constant definition (DefConst, Def_ExtCOnst) */
		OT_CONST_DEF,
		/** TTCN-3 module parameter definition (Def_Modulepar) */
		OT_MODPAR_DEF,
		/** TTCN-3 variable definition (Def_Var) */
		OT_VAR_DEF,
		/** TTCN-3 var template definition (Def_Var_Template) */
		OT_VARTMPL_DEF,
		/** TTCN-3 function (Def_Function, Def_ExtFunction) */
		OT_FUNCTION_DEF,
		/** TTCN-3 template definition (Def_Template) */
		OT_TEMPLATE_DEF,
		/** another Type: TTCN-3 array(T_ARRAY) */
		OT_ARRAY,
		/** another Type (T_SEQOF, T_SETOF), ASN.1 or TTCN-3 */
		OT_RECORD_OF,
		/** another Type: TTCN-3 function (T_FUNCTION) */
		OT_FUNCTION,
		/** another Type: TTCN-3 signature (T_SIGNATURE) */
		OT_SIGNATURE,
		/** another Type (T_REFD) */
		OT_REF,
		/** another Type (T_REFDSPEC) */
		OT_REF_SPEC,
		/** a field of a record/set/union (CompField) */
		OT_COMP_FIELD,
		/** ASN.1 "COMPONENTS OF" (CT_CompsOf) */
		OT_COMPS_OF,
		/** formal parameter (FormalPar), TTCN-3 */
		OT_FORMAL_PAR,
		/** TypeList for a 'with "extension anytype t1,t2..." ' */
		OT_TYPE_LIST,
		/** ASN.1 FieldSetting_Type */
		OT_FIELDSETTING,
		/** another Type (T_SELTYPE), ASN.1 selection type */
		OT_SELTYPE,
		/** another Type (T_OCFT), ASN.1 obj.class field type */
		OT_OCFT,
		/** a TemplateInstance (TTCN-3) */
		OT_TEMPLATE_INST,
		/** a RunsOnScope (TTCN-3) */
		OT_RUNSON_SCOPE,
		/** a port scope */
		OT_PORT_SCOPE,
		/** exception Specification (ExcSpec) */
		OT_EXC_SPEC,
		/** signature parameter (SignatureParam) */
		OT_SIG_PAR//,
		// OT_POOL no pool type is used here
	}

	public enum CompatibilityLevel {
		INCOMPATIBLE_TYPE, INCOMPATIBLE_SUBTYPE, COMPATIBLE
	}

	/** Stores information about the custom encoder or decoder function of a type */
	public static class CoderFunction_Type {
		/** definition of the encoder or decoder function */
		Assignment functionDefinition;
		/** indicates whether there are multiple encoder/decoder functions for this type and codec */
		boolean conflict;
	}

	/**
	 * Stores information related to an encoding type (codec), when using new codec handling.
	 * */
	public static class Coding_Type {
		/** built-in or user defined codec */
		public boolean builtIn;
		/** the 'encode' attribute's modifier */
		public Attribute_Modifier_type modifier;
		/** built-in codec ,when builtIn is true */
		public MessageEncoding_type builtInCoding;

		public CustomCoding_type customCoding;

		/** custom codec fields, when builtIn is false */
		public static class CustomCoding_type {
			/** name of the user defined codec (the string in the 'encode' attribute) */
			public String name;

			/** the map of encoder functions per type */
			public Map<IType, CoderFunction_Type> encoders;
			/** the map of decoder functions per type */
			public Map<IType, CoderFunction_Type> decoders;
		}
	}

	/**
	 * Represents the options that drive the value checking mechanisms. </p>
	 * All members have to be final, as it is not allowed to change the
	 * options during analysis. If this would be needed for a branch of the
	 * analysis, a copy should be made.
	 * */
	public static class ValueCheckingOptions {
		/** The kind of the value to be expected */
		public final Expected_Value_type expected_value;
		/**
		 * true if an incomplete value can be accepted at the given
		 * location, false otherwise
		 */
		public final boolean incomplete_allowed;
		/**
		 * true if the omit value can be accepted at the given location,
		 * false otherwise
		 */
		public final boolean omit_allowed;
		/** true if the subtypes should also be checked */
		public final boolean sub_check;
		/**
		 * true if the implicit omit optional attribute was set for the
		 * value, false otherwise
		 */
		public final boolean implicit_omit;
		/** true if the value to be checked is an element of a string */
		public final boolean str_elem;
		/** whether the value checking is initiated by a subtype */
		public final boolean from_subtype;

		public ValueCheckingOptions(final Expected_Value_type expectedValue, final boolean incompleteAllowed, final boolean omitAllowed,
				final boolean subCheck, final boolean implicitOmit, final boolean strElem) {
			this(expectedValue, incompleteAllowed, omitAllowed, subCheck, implicitOmit, strElem, false);
		}

		public ValueCheckingOptions(final Expected_Value_type expectedValue, final boolean incompleteAllowed, final boolean omitAllowed,
				final boolean subCheck, final boolean implicitOmit, final boolean strElem, final boolean fromSubtype) {
			this.expected_value = expectedValue;
			this.incomplete_allowed = incompleteAllowed;
			this.omit_allowed = omitAllowed;
			this.sub_check = subCheck;
			this.implicit_omit = implicitOmit;
			this.str_elem = strElem;
			this.from_subtype = fromSubtype;
		}
	}

	/** @return the internal type of the type */
	Type_type getTypetype();

	/** @return the parent type of the actual type */
	IType getParentType();

	/**
	 * Sets the parent type of the actual type.
	 * @param type the type to set.
	 */
	void setParentType(final IType type);

	/**
	 * @return the with attribute path element of this type. If it did not
	 *         exist it will be created.
	 */
	WithAttributesPath getAttributePath();

	/**
	 * Sets the parent path for the with attribute path element of this type.
	 * Also, creates the with attribute path node if it did not exist before.
	 *
	 * @param parent the parent to be set.
	 */
	void setAttributeParentPath(final WithAttributesPath parent);

	/**
	 * Clears the with attributes assigned to this type.
	 * <p>
	 * Should only be used on component fields.
	 */
	void clearWithAttributes();

	/**
	 * Sets the with attributes for this type.
	 * @param attributes the attributes to set.
	 */
	void setWithAttributes(final MultipleWithAttributes attributes);

	/** @return true if the done extension was assigned to this type */
	boolean hasDoneAttribute();

	/** @return true if this type has tags attached to it. */
	boolean isTagged();

	/** @return true if this type has a constraint attached to it */
	boolean isConstrained();

	/**
	 * Add constraints to this type.
	 * @param constraints the constraints to be added.
	 */
	void addConstraints(final Constraints constraints);

	/**
	 * @return the constraints set on this type, null if none.
	 */
	Constraints getConstraints();

	/**
	 * @return the sub-type restriction of the actual type, or null if it
	 *         does not have one.
	 */
	SubType getSubtype();

	/**
	 * Sets the type restrictions as they were parsed.
	 * @param parsedRestrictions the restrictions to set on this type.
	 */
	void setParsedRestrictions(final List<IParsedSubType> parsedRestrictions);

	/**
	 * Returns the type referred last in case of a referred type, or itself in any other case.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @return the actual or the last referred type
	 */
	default IType getTypeRefdLast(final CompilationTimeStamp timestamp) {
		final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final IType result = getTypeRefdLast(timestamp, referenceChain);
		referenceChain.release();

		return result;
	}

	/**
	 * Returns the type referred last in case of a referred type, or itself in any other case.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param referenceChain the ReferenceChain used to detect circular references
	 * @return the actual or the last referred type
	 */
	default IType getTypeRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		return this;
	}

	/**
	 * Returns the referenced field type for structured types, or itself in any other case.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param reference the reference used to select the field.
	 * @param actualSubReference the index used to tell, which element of the reference
	 *                to use as the field selector.
	 * @param expectedIndex the expected kind of the index value
	 * @param interruptIfOptional if true then returns null when reaching an optional field
	 * @return the type of the field, or self. In case of error a null reference is returned.
	 */
	default IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final boolean interruptIfOptional) {
		final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final IType temp = getFieldType(timestamp, reference, actualSubReference, expectedIndex, chain, interruptIfOptional);
		chain.release();

		return temp;
	}

	/**
	 * Returns the referenced field type for structured types, or itself in any other case.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param reference the reference used to select the field.
	 * @param actualSubReference the index used to tell, which element of the reference
	 *                to use as the field selector.
	 * @param expectedIndex the expected kind of the index value
	 * @param refChain a chain of references used to detect circular references.
	 * @param interruptIfOptional if true then returns null when reaching an optional field
	 * @return the type of the field, or self.
	 */
	IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final IReferenceChain refChain, final boolean interruptIfOptional);

	/**
	 * Calculates the list of field types traversed, in type_array and their
	 * local indices in subrefsArray parameters. Must be used only after
	 * getFieldType() was already successfully invoked. It can be used only
	 * when all array indexes are foldable, otherwise it returns false.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param reference the reference used to select the field.
	 * @param actualSubReference the index used to tell, which element of the reference
	 *                to use as the field selector.
	 * @param subrefsArray the list of field indices the searched fields were found at.
	 * @param typeArray the list of types found while traversing the fields.
	 * @return true in case the type of the referenced field could be evaluated, false otherwise.
	 */
	default boolean getSubrefsAsArray(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			List<Integer> subrefsArray, List<IType> typeArray) {
		if (reference.getSubreferences().size() <= actualSubReference) {
			return true;
		}
		TitanLogger.logFatal("Type " + getTypename() + " has no fields.");
		return false;
	}

	/** @return true if the type is the type of an optional field. */
	default boolean isOptionalField() {
		if (getOwnertype() == TypeOwner_type.OT_COMP_FIELD) {
			final CompField myOwner = (CompField) getOwner();
			return myOwner != null && myOwner.isOptional();
		}
		return false;
	}

	/**
	 * Returns whether the last field referenced by subReferences is an
	 * optional record/SEQUENCE or set/SET field.
	 * It can be used only after a successful
	 * semantic check (e.g. during code generation) or the behaviour will be unpredictable.
	 *
	 * @param subReferences the subreferences to walk
	 */
	default boolean fieldIsOptional(final List<ISubReference> subReferences) {
		if (subReferences == null || subReferences.isEmpty()) {
			return false;
		}

		final ISubReference lastSubReference = subReferences.get(subReferences.size() - 1);
		if (!(lastSubReference instanceof FieldSubReference)) {
			return false;
		}

		IType type = this;
		CompField compField = null;
		for ( int i = 1; i < subReferences.size(); i++) {
			type = type.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());

			final ISubReference subreference = subReferences.get(i);
			if (Subreference_type.fieldSubReference.equals(subreference.getReferenceType())) {
				final Identifier id = ((FieldSubReference) subreference).getId();
				switch(type.getTypetype()) {
				case TYPE_TTCN3_CHOICE:
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
					compField = ((TTCN3_Set_Seq_Choice_BaseType)type).getComponentByName(id.getName());
					break;
				case TYPE_ANYTYPE:
					compField = ((Anytype_Type)type).getComponentByName(id.getName());
					break;
				case TYPE_OPENTYPE:
					compField = ((Open_Type)type).getComponentByName(id);
					break;
				case TYPE_ASN1_SEQUENCE:
					compField = ((ASN1_Sequence_Type)type).getComponentByName(id);
					break;
				case TYPE_ASN1_SET:
					compField = ((ASN1_Set_Type)type).getComponentByName(id);
					break;
				case TYPE_ASN1_CHOICE:
					compField = ((ASN1_Choice_Type)type).getComponentByName(id);
					break;
				default:
					return false;
				}

				if (compField == null) {
					return false;
				}

				type = compField.getType();
			} else if (Subreference_type.arraySubReference.equals(subreference.getReferenceType())) {
				switch (type.getTypetype()) {
				case TYPE_SEQUENCE_OF:
				case TYPE_SET_OF:
					type = ((AbstractOfType) type).getOfType();
					break;
				case TYPE_ARRAY:
					type = ((Array_Type) type).getElementType();
					break;
				default:
					type = null;
					break;
				}
			}

			if (type == null) {
				return false;
			}
		}

		return compField != null && compField.isOptional();
	}

	/** @return the raw attribute of the type. */
	RawAST getRawAttribute();

	/**
	 * Returns the default field length of this type (in bits).
	 * integer: 8
	 * boolean: 0
	 * float: 64
	 * other: 0
	 *
	 * @return the default field length of the type.
	 */
	default int getDefaultRawFieldLength() {
		return 0;
	}

	/**
	 * TODO might need some optimization later.
	 * 
	 * @param timestamp the time stamp of the actual build cycle.
	 * @return the length of the raw field length of this type
	 */
	default int getRawLength(final BuildTimestamp timestamp) {
		return -1;
	}

	/** @return the json attribute of the type. */
	JsonAST getJsonAttribute();

	/** @return the ber attribute of the type. */
	BerAST getBerAttribute();

	/**
	 * Returns the default field length of this type (in bits).
	 * hexstring: 4
	 * octetstring, charstring, universalcharstring: 8
	 *
	 * other: 1
	 *
	 * @return the default field length of the type.
	 */
	default int getLengthMultiplier() {
		return 1;
	}

	/**
	 * Calculates the list of field types traversed. Does not check if the index values are valid.
	 * Does not give error messages, it just returns with false.
	 *
	 * @param reference the reference used to select the field.
	 * @param actualSubReference the index used to tell, which element of the reference
	 *                to use as the field selector.
	 * @param typeArray the list of types found while traversing the fields.
	 * @return true in case the type of the referenced field could be evaluated, false otherwise.
	 */
	default boolean getFieldTypesAsArray(final Reference reference, final int actualSubReference, List<IType> typeArray) {
		return reference.getSubreferences().size() <= actualSubReference;
	}

	boolean hasRawAttributes(final CompilationTimeStamp timestamp);

	/**
	 * Checks if there is a variant attribute among the ones reaching to this type.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @return true if there was a variant attribute found, false otherwise.
	 * */
	boolean hasVariantAttributes(final CompilationTimeStamp timestamp);

	/**
	 * Checks if a given type has the done extension attribute assigned to it, or not.
	 * */
	void checkDoneAttribute(final CompilationTimeStamp timestamp);

	/**
	 * Initializes the internal representation of coding attributes.
	 * This is also needed to clear information set during a previous check.
	 * Additionally, it also checks the existence of the done attribute.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 */
	void initAttributes(final CompilationTimeStamp timestamp);

	/**
	 * Fills the list parameter with the types that have an empty coding table.
	 * The types considered are the type itself and its field and element types. Recursive.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param typeList the list to fill with the found types
	 * @param only_own_table
	 *                if true, then only the type's own coding table is checked,
	 *                otherwise inherited coding tables are also checked
	 */
	void getTypesWithNoCodingTable(final CompilationTimeStamp timestamp, final List<IType> typeList, final boolean onlyOwnTable);

	/**
	 * Parses the specified variant attribute and checks its validity (when
	 * using the new codec handling).
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param singleWithAttribute the with attribute to parse.
	 * @param global is the attribute a global one?
	 */
	void checkThisVariant(final CompilationTimeStamp timestamp, final SingleWithAttribute singleWithAttribute, final boolean global);

	/**
	 * Checks the coding instructions set by the parsed variant attributes.
	 * The check is performed recursively on the type's fields and elements.
	 *
	 * Checks the raw and other coding attributes in one place.
	 * (the compiler has chk_raw separately)
	 * May also create such internal attributes based on restrictions.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param refChain a chain of references used to detect circular references.
	 */
	default void checkCodingAttributes(final CompilationTimeStamp timestamp, IReferenceChain refChain) {
		//FIXME implement default behaviour
	}

	/**
	 * Determines the method of encoding or decoding for values of this type
	 * based on its attributes and on encoder or decoder function
	 * definitions with this type as their input or output. An error is
	 * displayed if the coding method cannot be determined.
	 *
	 * @note Because this check depends on the checks of other AST elements
	 *       (external functions), it is sometimes delayed to the end of the
	 *       semantic analysis.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param encode {@code true} if used for encoding, {@code false} for decoding.
	 * @param usageModule the module where the type is used (unused kept for backward compatibility).
	 * @param delayed in some case it is needed to delay this check till the
	 *                end of the semantic checking.
	 * @param errorLocation the location to report error messages to in case of need.
	 */
	void checkCoding(final CompilationTimeStamp timestamp, final boolean encode,
			final Module usageModule, final boolean delayed, final Location errorLocation);

	/**
	 * If the type does not have its raw attribute, generate and check a default one.
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 */
	default void forceRaw(final CompilationTimeStamp timestamp) {
		// empty by default
	}

	/**
	 * Set the raw attribute of a type from outside.
	 * Should be used only when raw attribute checking requires it
	 *
	 * @param newAttributes the new attributes to set.
	 */
	void setRawAttributes(final RawAST newAttributes);

	/**
	 * Set the json attribute of a type from outside.
	 * Should be used only when json attribute checking requires it
	 *
	 * @param newAttributes the new attributes to set.
	 */
	void setJsonAttributes(final JsonAST newAttributes);

	/**
	 * Set the ber attribute of a type from outside.
	 * Should be used only when ber attribute checking requires it
	 *
	 * @param newAttributes the new attributes to set.
	 */
	void setBerAttributes(final BerAST newAttributes);

	/**
	 * If the type does not have its json attribute, generate and check a default one.
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 */
	default void forceJson(final CompilationTimeStamp timestamp) {
		// empty by default
	}

	/**
	 * Check the json coding attributes of the type.
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	void checkJson(final CompilationTimeStamp timestamp);

	/**
	 * Checks the json default attribute assigned to this type.
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 */
	void checkJsonDefault(final CompilationTimeStamp timestamp);

	/**
	 * Adds a coding to the type.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param name the name of the coding to add.
	 * @param modifier the modifier of the coding to add.
	 * @param silent stay silent if the coding can not be applied to a given type.
	 */
	void addCoding(final CompilationTimeStamp timestamp, final String name, final Attribute_Modifier_type modifier, final boolean silent);

	/**
	 * Sets the encoding function for a type.
	 *
	 * @param codingName the name of the coding to set it for
	 * @param functionDefinition the function definition to set
	 */
	default void setEncodingFunction(final String codingName, final Assignment functionDefinition) {
		final IType t = getTypeWithCodingTable(CompilationTimeStamp.getBaseTimestamp(), false);
		if (t == null) {
			return;
		}

		final List<Coding_Type> tempCodingTable = t.getCodingTable();
		for (final Coding_Type tempCoding : tempCodingTable) {
			if (!tempCoding.builtIn && tempCoding.customCoding.name.equals(codingName)) {
				final Map<IType, CoderFunction_Type> tempCoders = tempCoding.customCoding.encoders;
				if (tempCoders.containsKey(this) && tempCoders.get(this).functionDefinition != functionDefinition) {
					tempCoders.get(this).conflict = true;
				} else {
					final CoderFunction_Type newCoder = new CoderFunction_Type();
					newCoder.functionDefinition = functionDefinition;
					newCoder.conflict = false;
					tempCoders.put(this, newCoder);
					//phantom imports can be handled in code generator
				}
				return;
			}
		}
	}

	/**
	 * Sets the decoding function for a type.
	 *
	 * @param codingName the name of the coding to set it for
	 * @param functionDefinition the function definition to set
	 */
	default void setDecodingFunction(final String codingName, final Assignment functionDefinition) {
		final IType t = getTypeWithCodingTable(CompilationTimeStamp.getBaseTimestamp(), false);
		if (t == null) {
			return;
		}

		final List<Coding_Type> tempCodingTable = t.getCodingTable();
		for (final Coding_Type tempCoding : tempCodingTable) {
			if (!tempCoding.builtIn && tempCoding.customCoding.name.equals(codingName)) {
				final Map<IType, CoderFunction_Type> tempCoders = tempCoding.customCoding.decoders;
				if (tempCoders.containsKey(this) && tempCoders.get(this).functionDefinition != functionDefinition) {
					tempCoders.get(this).conflict = true;
				} else {
					final CoderFunction_Type newCoder = new CoderFunction_Type();
					newCoder.functionDefinition = functionDefinition;
					newCoder.conflict = false;
					tempCoders.put(this, newCoder);
					//phantom imports can be handled in code generator
				}
				return;
			}
		}
	}

	/**
	 * Checks for the type that has a coding table.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param ignoreLocal ignore the local coding table.
	 * @return the first type in the searched chain with a coding table.
	 */
	IType getTypeWithCodingTable(final CompilationTimeStamp timestamp, final boolean ignoreLocal);

	/**
	 * Checks if the type can have the given encoding.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param coding the coding to check for.
	 * @return {@code true} if the type has the given encoding.
	 * */
	default boolean canHaveCoding(final CompilationTimeStamp timestamp, final MessageEncoding_type coding) {
		//TODO base type behaviour
		// BER encoding is handled by 'hasEncoding'
		if (coding == MessageEncoding_type.BER) {
			return hasEncoding(timestamp, MessageEncoding_type.BER, null);
		}
		return false;
	}

	/**
	 * Checks if the type has the given encoding.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param coding the coding to check for.
	 * @return {@code true} if the type has the given encoding.
	 */
	boolean hasEncoding(final CompilationTimeStamp timestamp, final MessageEncoding_type coding, final String customEncoding);

	/**
	 * Helper function for hasEncodeAttribute. Checks this type's qualified
	 * encoding attributes that refer to the specified type (target_type).
	 * 
	 * Recursive function (calls the parent type's hasEncodeAttrForType
	 * function if no matching attributes are found).
	 *
	 * @param type the type to be checked.
	 * @param encodingName the name of the encoding to check for.
	 * @return {@code true} if any of the encoding attributes of the specified type
	 *         match the specified encoding (encoding_name)
	 */
	boolean hasEncodeAttributeForType(final Type type, final MessageEncoding_type encoding);

	/** @return the coding table of this type */
	List<Coding_Type> getCodingTable();

	/**
	 * Checks that the type can be used as a parameter for a map/unmap statement.
	 * It is not and does not contain component and default types.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param refChain a chain of references used to detect circular references.
	 * @param errorLocation the location the error message should be reported to.
	 */
	default void checkMapParameter(final CompilationTimeStamp timestamp, final IReferenceChain refChain, final Location errorLocation) {
		// the default implementation is empty as it is allowed
	}

	/**
	 * Checks if the complex type has a field whose name is exactly the same
	 * as the name of the definition defining the type.
	 *
	 * @param definitionName the name of the definition.
	 */
	default void checkConstructorName(final String definitionName) {
		// empty by default
	}

	/**
	 * The type of sub-type that belongs to this type or ST_NONE if this
	 * type cannot have sub-type, every type that can have a sub-type must
	 * override this function
	 */
	default SubType.SubType_type getSubtypeType() {
		return SubType.SubType_type.ST_NONE;
	}

	/**
	 * Checks for circular references within embedded types.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param referenceChain the ReferenceChain used to detect circular references,
	 *                must not be null.
	 */
	default void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		// empty by default
	}

	/**
	 * Checks if the values of this type can be used only in `self'. Some
	 * types (default, function reference with `runs on self') are invalid
	 * outside of the component they were created in, they should not be
	 * sent/received in ports or used in compref.start. All structured types
	 * that may contain such internal types are also internal.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @return {@code true} if component internal, {@code false} otherwise.
	 */
	default boolean isComponentInternal(final CompilationTimeStamp timestamp) {
		return false;
	}

	/**
	 * Checks the types which should be component internal, if they have
	 * left the component. Should be called only if is_component_internal()
	 * returned true.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param typeSet is used to escape infinite recursion, by maintaining
	 *                the set of types used to call this function.
	 * @param operation the name of the operation to be included in the error message.
	 */
	default void checkComponentInternal(final CompilationTimeStamp timestamp, final Set<IType> typeSet, final String operation) {
		// empty by default
	}

	/**
	 * Checks whether the type can be a component of another type definition
	 * (e.g. field of a structured type, parameter/return type/ exception of
	 * a signature). Ports and Signatures are not allowed, Default only
	 * within structured types.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param errorLocation the location to report the errors to.
	 * @param defaultAllowed whether default should be allowed or not.
	 * @param errorMessage the part of the error message to be reported.
	 */
	default void checkEmbedded(final CompilationTimeStamp timestamp, final Location errorLocation,
			final boolean defaultAllowed, final String errorMessage) {
		// empty by default
	}

	/**
	 * If the value is an undefined lowerid, then this member decides
	 * whether it is a reference or a lowerid value (e.g., enum, named number).
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param value the value to be checked
	 *
	 * @return the converted value so that it can replace the original, or
	 *         the original if no conversion was needed
	 */
	default IValue checkThisValueRef(final CompilationTimeStamp timestamp, final IValue value) {
		if (Value_type.UNDEFINED_LOWERIDENTIFIER_VALUE.equals(value.getValuetype())) {
			return value.setValuetype(timestamp, Value_type.REFERENCED_VALUE);
		}
		return value;
	}

	/**
	 * Checks if a given value is valid according to this type.
	 * <p>
	 * The default / base implementation checks referenced, expression and
	 * macro values, as they must be unfolded (if possible) before gaining
	 * access to the real / final value and that values kind.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param value the value to be checked
	 * @param lhs the assignment to check against
	 * @param valueCheckingOptions the options according to which the given value
	 *                should be evaluated
	 * @return {@code true} if the value contains a reference to lhs
	 */
	boolean checkThisValue(final CompilationTimeStamp timestamp, final IValue value, final Assignment lhs, final ValueCheckingOptions valueCheckingOptions);

	/**
	 * Checks whether the provided template is a specific value and the embedded value is a referenced one.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param template the template to check.
	 * @return the checked template, might be different from the one passed as parameter.
	 */
	default ITTCN3Template checkThisTemplateRef(final CompilationTimeStamp timestamp, final ITTCN3Template template) {
		return checkThisTemplateRef(timestamp, template, Expected_Value_type.EXPECTED_TEMPLATE, null);
	}

	/**
	 * Checks whether the provided template is a specific value and the embedded value is a referenced one.
	 * Additionally checks whether the template has the expected value type and avoids circular references.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param template the template to check.
	 * @return the checked template, might be different from the one passed as parameter.
	 */
	ITTCN3Template checkThisTemplateRef(final CompilationTimeStamp timestamp, final ITTCN3Template template,
			final Expected_Value_type expectedValue, final IReferenceChain referenceChain);

	/**
	 * Does the semantic checking of the provided template according to the a specific type.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param template the template to be checked by the type.
	 * @param isModified {@code true} if the template is a modified template
	 * @param implicitOmit {@code true} if the implicit omit optional attribute was set
	 *                for the template, {@code false} otherwise
	 * @param lhs the assignment to check against
	 * @return {@code true} if the value contains a reference to lhs
	 */
	boolean checkThisTemplate(final CompilationTimeStamp timestamp, final ITTCN3Template template, final boolean isModified,
			final boolean implicitOmit, final Assignment lhs);

	/**
	 * Does the semantic checking of the provided template according to the
	 * sub-type of the actual type.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param template the template to be checked by the sub-type.
	 */
	void checkThisTemplateSubtype(final CompilationTimeStamp timestamp, final ITTCN3Template template);

	/**
	 * Returns whether this type is compatible with type.
	 * <p>
	 * Note: The compatibility relation is asymmetric. The function returns {@code true}
	 * if the set of possible values in type is a subset of possible values in this.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param otherType the type to check against.
	 * @param info the type compatibility information.
	 * @param leftChain to detect type recursion on the left side.
	 * @param rightChain to detect type recursion on the right side.
	 * @return {@code true} if they are compatible, {@code false} otherwise.
	 */
	boolean isCompatible(final CompilationTimeStamp timestamp, final IType otherType, TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain);

	/**
	 * Returns whether this type is strongly compatible with type that is exactly
	 * has the same type and they are both base types
	 * <p>
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param otherType the type to check against.
	 * @param info the type compatibility information.
	 * @param leftChain to detect type recursion on the left side.
	 * @param rightChain to detect type recursion on the right side.
	 * @return {@code true} if they are compatible, {@code false} otherwise.
	 */
	boolean isStronglyCompatible(final CompilationTimeStamp timestamp, final IType otherType, TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain);

	/**
	 * @return whether this type and it's sub-type are compatible to the other type and it's sub-type.
	 */
	CompatibilityLevel getCompatibility(final CompilationTimeStamp timestamp, final IType type, final TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain);

	/**
	 * Check if the port definitions of the component types are the same
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param otherType the type to check against
	 * @return {@code true} if they are identical, {@code false} otherwise
	 */
	default boolean isCompatibleByPort(final CompilationTimeStamp timestamp, final IType otherType) {
		check(timestamp);
		otherType.check(timestamp);
		return false;
	}

	/**
	 * Returns whether this type is identical to the parameter type.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param type the type to check against
	 * @return {@code true} if they are identical, {@code false} otherwise
	 */
	default boolean isIdentical(final CompilationTimeStamp timestamp, final IType type) {
		check(timestamp);
		type.check(timestamp);
		final IType temp = type.getTypeRefdLast(timestamp);
		if (getIsErroneous(timestamp) || temp.getIsErroneous(timestamp)) {
			return true;
		}
		return getTypetypeTtcn3().equals(temp.getTypetypeTtcn3());
	}

	/**
	 * Returns the name of this type for the user to identify the type.
	 *
	 * For simple primitive types (like integer, float, bitstring) this is their name.
	 * For other types this is the full name of the definition of the type.
	 *
	 * @return the name of the type.
	 */
	String getTypename();

	/** @return the TTCN-3 equivalent of this type's type, or undefined if none */
	Type_type getTypetypeTtcn3();

	/** @return the assignment/definition that defines this type. */
	default Assignment getDefiningAssignment() {
		return getMyScope() == null ? null :
			getMyScope().getModuleScope().getEnclosingAssignment(getLocation().getStartPosition());
	}

//	// TODO declaration and proposal collecting should not belong here
//	/**
//	 * Searches and adds a declaration proposal to the provided collector if
//	 * a valid one is found.
//	 * <p>
//	 * Simple types can not be used as declarations.
//	 *
//	 * @param declarationCollector
//	 *                the declaration collector to add the declaration to,
//	 *                and used to get more information.
//	 * @param i
//	 *                index, used to identify which element of the reference
//	 *                (used by the declaration collector) should be checked.
//	 * */
//	void addDeclaration(final DeclarationCollector declarationCollector, final int i);
//
//	/**
//	 * Searches and adds a completion proposal to the provided collector if
//	 * a valid one is found.
//	 * <p>
//	 * If this type is a simple type, it can never complete any proposals.
//	 *
//	 * @param propCollector
//	 *                the proposal collector to add the proposal to, and
//	 *                used to get more information
//	 * @param i
//	 *                index, used to identify which element of the reference
//	 *                (used by the proposal collector) should be checked for
//	 *                completions.
//	 * */
//	void addProposal(final ProposalCollector propCollector, final int i);

	/**
	 * Creates the description part of this type to be displayed, in the code completion.
	 * @param the part of the description to be extended
	 * @return the description extended with this type's description.
	 */
	default StringBuilder getProposalDescription(final StringBuilder builder) {
		return builder.append(getTypetype().getName());
	}

	/**
	 * This function is used by user interface functions to determine which
	 * field of a type is enclosing the provided offset. For example this is
	 * used to find all references to a field.
	 *
	 * @param offset the offset of the location in the file to search for.
	 * @param rf the reference finder object that will store the type
	 *           and field informations when the offset is inside a field of this type.
	 */
	default void getEnclosingField(final Position offset, final ReferenceFinder rf) {
		// empty by default
	}

	/**
	 * Returns {@code true} if the type supports at least one built-in encoding.
	 * Only used with new codec handling.
	 *
	 * @return {@code true} if the type supports at least one built-in encoding.
	 */
	default boolean hasBuiltInEncoding() {
		final IType t = getTypeWithCodingTable(CompilationTimeStamp.getBaseTimestamp(), false);
		if (t == null) {
			return false;
		}

		final List<Coding_Type> cTable = t.getCodingTable();
		for (final Coding_Type ct : cTable) {
			if (ct.builtIn) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates and returns a string representation of this type, that can be
	 * used to calculate the alternative name of this type in an open type.
	 *
	 * create_stringRepr on the C side.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @return the base name to start calculating from.
	 */
	default String createStringRep_for_OpenType_AltName(final CompilationTimeStamp timestamp) {
		//return getGenNameOwn();
		return "FIXLSP";
	}

	/**
	 * Helper function used in generateCodeIspresentbound() for the
	 * ispresent() function in case of template parameter.
	 *
	 * @param expression the expression to generate code to (error messages if needed).
	 * @param subreferences the subreferences to check.
	 * @param beginIndex the index at which index the checking of subreferences should start.
	 * @return {@code true} if the referenced field which is embedded into a "?" is
	 *         always present, otherwise returns false.
	 */
	boolean isPresentAnyvalueEmbeddedField(final ExpressionStruct expression, final List<ISubReference> subreferences, final int beginIndex);

	/** Set the owner and its type type */
	void setOwnertype(final TypeOwner_type ownerType, final INamedNode owner);

	TypeOwner_type getOwnertype();

	INamedNode getOwner();

	/** Indicates that the type needs to have any from done support. */
	void set_needs_any_from_done();

	/** @return the JSON type of the type */
	int getJsonValueType();

	/** @return this type as {@link Class_Type} */
	default Class_Type getClassType() {
		if (getTypetype() != Type_type.TYPE_CLASS) {
			// fatal error
		}
		return (Class_Type)this;
	}

	/**
	 * Checks whether this is a type reference
	 * @return whether this is a type reference
	 */
	default boolean isReference() {
		switch(getTypetype()) {
		case TYPE_UNRESTRICTEDSTRING:
		case TYPE_OBJECTCLASSFIELDTYPE:
		case TYPE_EXTERNAL:
		case TYPE_EMBEDDED_PDV:
		case TYPE_REFERENCED:
		case TYPE_REFD_SPEC:
		case TYPE_SELECTION:
		case TYPE_ADDRESS:
			return true;
		default:
			return false;
		}
	}

	default String getExceptionName(CompilationTimeStamp timestamp) {
		switch(getOwnertype()) {
		case OT_TYPE_ASS:
		case OT_TYPE_DEF:
		case OT_ARRAY:
		case OT_COMP_FIELD:
		case OT_RECORD_OF:
			return "LSPFIX";
		default:
			if (isReference()) {
				return getTypeRefdLast(timestamp).getExceptionName(timestamp);
			} else {
				return getTypename();
			}
		}
	}

	default boolean containsClass(final CompilationTimeStamp timestamp) {
		// this helps avoiding infinite recursions in self-referencing types
		if (RecursionTracker.isHappening(this)) {
			return false;
		}
		new RecursionTracker(this);
		switch(getTypetype()) {
		case TYPE_CLASS:
			return true;
		case TYPE_REFERENCED:
			return getTypeRefdLast(timestamp).containsClass(timestamp);
		case TYPE_TTCN3_SEQUENCE:
		case TYPE_TTCN3_SET:
		case TYPE_TTCN3_CHOICE:
			if (this instanceof TTCN3_Set_Seq_Choice_BaseType) {
				final TTCN3_Set_Seq_Choice_BaseType sscType = (TTCN3_Set_Seq_Choice_BaseType)this; 
				for (int i = 0; i < sscType.getNofComponents(); i++) {
					if (sscType.getComponentByIndex(i).getType().containsClass(timestamp)) {
						return true;
					}
				}
			}
			return false;
		case TYPE_SEQUENCE_OF:
		case TYPE_SET_OF:
		case TYPE_ARRAY:
			if (this instanceof AbstractOfType) {
				return ((AbstractOfType)this).getOfType().containsClass(timestamp);
			}
			return false;
		default:
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 * The common fields provided by {@link IType#getSubtype()} and {@link IType#getAttributePath()}
	 * are checked here.
	 */
	@Override
	default void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);
		if (getSubtype() != null) {
			getSubtype().updateSyntax(reparser, false);
		}

		getAttributePath().updateSyntax(reparser, false);
		reparser.updateLocation(getAttributePath().getLocation());
	}
}
