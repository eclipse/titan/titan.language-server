/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.MatchExpression;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public final class Match_InternalLogArgument extends InternalLogArgument {
	private final MatchExpression matchExpression;

	public Match_InternalLogArgument(final MatchExpression matchExpression) {
		super(ArgumentType.Match);
		this.matchExpression = matchExpression;
	}

	public MatchExpression getMatchExpression() {
		return matchExpression;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (matchExpression == null) {
			return;
		}

		matchExpression.checkRecursions(timestamp, referenceChain);
	}
}
