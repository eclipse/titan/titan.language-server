/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import org.eclipse.titan.lsp.AST.FieldSubReference;

/**
 * Utility class for generating the value and template classes for union/choice
 * types.
 *
 * The code generated for union/choice types only differs in matching and
 * encoding.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * */
public final class UnionGenerator {

	/**
	 * Data structure to store sequence field variable and type names.
	 * Used for java code generation.
	 */
	public static class FieldInfo {

		/** Java type name of the field */
		private final String mJavaTypeName;

		private final String mJavaTemplateName;

		/** Field variable name in TTCN-3 and java */
		private final String mVarName;

		/** The user readable name of the field, typically used in error messages */
		private final String mDisplayName;

		/** Field variable name in java getter/setter function names and parameters */
		private final String mJavaVarName;

		private final String mTypeDescriptorName;

		private final String jsonAlias;

		private final int jsonValueType;

		/**
		 * @param fieldType
		 *                the string representing the value type of this
		 *                field in the generated code.
		 * @param fieldTemplate
		 *                the string representing the template type of
		 *                this field in the generated code.
		 * @param fieldName
		 *                the string representing the name of this field
		 *                in the generated code.
		 * @param displayName
		 *                the string representing the name of this field
		 *                in the error messages and logs in the
		 *                generated code.
		 * @param typeDescriptorName
		 *                the name of the type descriptor.
		 * */
		public FieldInfo(final String fieldType, final String fieldTemplate, final String fieldName, final String displayName,
						 final String typeDescriptorName, final String jsonAlias, final int jsonValueType) {
			mJavaTypeName = fieldType;
			mJavaTemplateName = fieldTemplate;
			mVarName = fieldName;
			mJavaVarName = FieldSubReference.getJavaGetterName( mVarName );
			mDisplayName = displayName;
			mTypeDescriptorName = typeDescriptorName;
			this.jsonAlias = jsonAlias;
			this.jsonValueType = jsonValueType;
		}
	}

	private static class TemporalVariable {
		public String type;
		public String typedescriptor;
		int start_pos;
		int use_counter;
		int decoded_for_element;
	}

	/*
	 * Should the union have more than 200 fields, we will generate helper functions.
	 * Each of which will handle 200 fields on its own.
	 * This is done as in the case of Diameter a union with 1666 fields
	 *  would generate too much code into a single function.
	 **/
	private static final int maxFieldsLength = 200;

	private UnionGenerator() {
		// private to disable instantiation
	}
}
