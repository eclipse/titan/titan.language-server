/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.core;

/**
 * Titan Language Server specific position class implementing
 * the {@link ILocationComparer} interface
 * 
 * @author Miklos Magyari
 * */
public final class Position extends org.eclipse.lsp4j.Position implements ILocationComparer {
	/**
	 * Constructs an instance based on an LSP Position
	 * @param position
	 */
	public Position(org.eclipse.lsp4j.Position position) {
		setLine(position.getLine());
		setCharacter(position.getCharacter());
	}

	public Position(int line, int column) {
		super(line, column);
	}

	@Override
	/** {@inheritDoc} */
	public final boolean before(final ILocationComparer other) {
		if (!(other instanceof Position) && !(other instanceof Range)) {
			throw new IllegalArgumentException();
		}

		final Position otherPosition = other instanceof Position ?
				new Position((Position)other) : new Position(((Range)other).getStart());
		if (getLine() < otherPosition.getLine())
			return true;
		if ((getLine() == otherPosition.getLine()) && (getCharacter() < otherPosition.getCharacter()))
			return true;

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean beforeOrEquals(final ILocationComparer other) {
		return before(other) || equals(other);
	}

	@Override
	/** {@inheritDoc} */
	public final boolean after(final ILocationComparer other) {
		if (!(other instanceof Position)) {
			throw new IllegalArgumentException();
		}

		final Position otherPosition = (Position)other;
		if (getLine() > otherPosition.getLine())
			return true;
		if ((getLine() == otherPosition.getLine()) && (getCharacter() > otherPosition.getCharacter()))
			return true;

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean afterOrEquals(final ILocationComparer other) {
		return after(other) || equals(other);
	}

	/**
	 * Checks if this position is inside of a given range, including the range's endpoints
	 * 
	 * @param range
	 * @return
	 */
	public boolean isInRangeClosed(final Range range) {
		return ((Position)range.getStart()).beforeOrEquals(this) && ((Position)range.getEnd()).afterOrEquals(this);
	}

	/**
	 * Checks if this position is inside of a given range, excluding the range's endpoints
	 * 
	 * @param range
	 * @return
	 */
	public boolean isInRangeOpen(final Range range) {
		return ((Position)range.getStart()).before(this) && ((Position)range.getEnd()).after(this);
	}

	/***
	 * Calculates the offset in a given string
	 * @param text
	 * @return
	 */
	public int toOffset(String text) {
		int index = 0;
        for (int i = 0; i < getLine(); i++) {
            index = text.indexOf("\n", index) + 1;
        }

        index += getCharacter();
        return index;
	}
}
