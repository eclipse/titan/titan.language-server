/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.IIdentifierContainer;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a list of qualifiers.
 * 
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class Qualifiers
	implements IIncrementallyUpdatable, IIdentifierContainer, IVisitableNode, ICollection<Qualifier> {
	private final List<Qualifier> qualifiersList = new ArrayList<Qualifier>(1);

	@Override
	public boolean add(final Qualifier qualifier) {
		if (qualifier != null) {
			return qualifiersList.add(qualifier);
		}
		return false;
	}

	@Override
	public int size() {
		return qualifiersList.size();
	}

	@Override
	public Qualifier get(final int index) {
		return qualifiersList.get(index);
	}

	/**
	 * Handles the incremental parsing of this list of qualifiers.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged {@code true} if the location contains the damaged area,
	 *                {@code false} if only its' location needs to be updated.
	 */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final Qualifier q : qualifiersList) {
			q.updateSyntax(reparser, false);
			reparser.updateLocation(q.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final Qualifier q : qualifiersList) {
			q.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final Qualifier q : qualifiersList) {
			if (!q.accept(v)) {
				return false;
			}
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}

	@Override
	public Iterator<Qualifier> iterator() {
		return qualifiersList.iterator();
	}
}
