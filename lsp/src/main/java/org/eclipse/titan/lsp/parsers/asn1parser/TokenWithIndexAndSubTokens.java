/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.asn1parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.misc.Pair;

/**
 * @author Laszlo Baji
 * @author Adam Knapp
 */
public class TokenWithIndexAndSubTokens extends CommonToken {
	private static final long serialVersionUID = 3906412166039744425L;
	private List<CommonToken> tokenList = new ArrayList<>();
	private File sourceFile;

	public TokenWithIndexAndSubTokens(final Pair<TokenSource, CharStream> source, final int type, final int channel, final int start, final int stop) {
		super(source, type, channel, start, stop);
	}

	public TokenWithIndexAndSubTokens(final Pair<TokenSource, CharStream> source, final int type, final int channel, final int start, final int stop, final List<Token> tokenList, final File sourceFile) {
		super(source, type, channel, start, stop);
		this.sourceFile = sourceFile;
		for (Token t : tokenList) {
			this.tokenList.add((CommonToken)t);
		}
	}

	public TokenWithIndexAndSubTokens(final int t) {
		super(t);
	}

	public TokenWithIndexAndSubTokens(final int t, final String text) {
		super(t, text);
	}

	public TokenWithIndexAndSubTokens(final Token tok) {
		super(tok);
		super.setStartIndex(tok.getStartIndex());
		super.setStopIndex(tok.getStopIndex());
	}

	public File getSourceFile() {
		return sourceFile;
	}

	/**
	 * Returns an unmodifiable view of the contained token list
	 * @return an unmodifiable view of the contained token list
	 */
	public List<Token> getSubTokens() {
		return Collections.unmodifiableList(tokenList);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("[:\"").append(getText()).append("\",<").append(type).append(">,line=").append(line).append(",col=")
		.append(charPositionInLine).append(",start=").append(start).append(",stop=").append(stop).append("]\n");
		return sb.toString();
	}

	/**
	 * Returns a new instance of this object with the same attributes
	 * @return a new instance of this object with the same attributes
	 */
	public TokenWithIndexAndSubTokens copy() {
		final TokenWithIndexAndSubTokens token = new TokenWithIndexAndSubTokens(source, type, channel, start, stop, new ArrayList<>(tokenList), sourceFile);
		token.line = line;
		token.charPositionInLine = charPositionInLine;
		token.setText(getText());

		return token;
	}

}

