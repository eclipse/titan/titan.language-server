/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import static java.util.Map.entry;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionKind;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.codeAction.data.MissingFunctionData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Miklos Magyari
 */
public class CodeActionsForMissingFunction extends CodeActionBase {
	public CodeActionsForMissingFunction(TextDocumentIdentifier doc, Diagnostic diagnostic) {
		super(doc, diagnostic);
	}

	public enum CodeActionType_MissingFunction {
		GENERATEFUNCTIONASDEFINITION,
		GENERATEFUNCTIONASMEMBER
	}

	private static final Map<CodeActionType_MissingFunction, BiFunction<TextDocumentIdentifier, Diagnostic, CodeAction>> codeActionList = Map.ofEntries(
		entry(CodeActionType_MissingFunction.GENERATEFUNCTIONASDEFINITION, CodeActionsForMissingFunction::addFunctionAsDefinition),
		entry(CodeActionType_MissingFunction.GENERATEFUNCTIONASMEMBER, CodeActionsForMissingFunction::addFunctionAsMember)
	);
	
	private static CodeAction addFunctionAsDefinition(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final CodeAction action = new CodeAction("Generate function template");
		action.setKind(CodeActionKind.QuickFix);
		
		final MissingFunctionData functionData = getFunctionData(diagnostic);
		final TextEditor editor = new TextEditor(doc);
		final CodeActionHelpers helper = new CodeActionHelpers();
		final String indentedCode = helper.indentCode(functionData.definitionLocation, functionData.code);
		final WorkspaceEdit edit = editor
			.insertText(new Position(functionData.definitionLocation.getEndLine(), functionData.definitionLocation.getEndColumn()), indentedCode)
			.getEdit();

		action.setEdit(edit);
		
		return action;
	}
	
	private static CodeAction addFunctionAsMember(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final CodeAction action = new CodeAction("Generate function template as a member");
		action.setKind(CodeActionKind.QuickFix);
		
		final MissingFunctionData functionData = getFunctionData(diagnostic);
		final TextEditor editor = new TextEditor(doc);
		final CodeActionHelpers helper = new CodeActionHelpers();
		final String indentedCode = helper.indentCode(functionData.memberLocation, functionData.code);
		final WorkspaceEdit edit = editor
			.insertText(new Position(functionData.memberLocation.getEndLine(), functionData.memberLocation.getEndColumn()), indentedCode)
			.getEdit();

		action.setEdit(edit);
		
		return action;
	}
	
	private static MissingFunctionData getFunctionData(final Diagnostic diagnostic) {
		final DiagnosticData diagnosticData = (DiagnosticData)diagnostic.getData();
		final Optional<Object> actionData = diagnosticData.getCodeActionData();
		GsonBuilder builder = new GsonBuilder();
		CodeActionHelpers.registerTypeAdapters(builder);
		Gson gson = builder.create();
		
		return gson.fromJson((String)actionData.orElse(""), MissingFunctionData.class);
	}
	
	@Override
	public List<Either<Command, CodeAction>> provideCodeActions() {
		return provideCodeActions(codeActionList, CodeActionType_MissingFunction.class);
	}
}
