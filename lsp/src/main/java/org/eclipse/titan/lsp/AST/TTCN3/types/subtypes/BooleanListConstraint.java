/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types.subtypes;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.INamedNode;

/**
 * @author Adam Delic
 * */
public final class BooleanListConstraint extends SubtypeConstraint {
	public enum ConstraintValue {
		FALSE(0x01), TRUE(0x02), ALL(0x03);
		private final int value;

		ConstraintValue(final int v) {
			value = v;
		}

		public int value() {
			return value;
		}
	}

	private final int constraint;

	public BooleanListConstraint(final ConstraintValue constraint) {
		this.constraint = constraint.value();
	}

	public BooleanListConstraint(final boolean b) {
		constraint = b ? ConstraintValue.TRUE.value() : ConstraintValue.FALSE.value();
	}

	private BooleanListConstraint(final int constraint) {
		this.constraint = constraint;
	}

	@Override
	/** {@inheritDoc} */
	public BooleanListConstraint complement() {
		return new BooleanListConstraint(constraint ^ ConstraintValue.ALL.value());
	}

	@Override
	/** {@inheritDoc} */
	public BooleanListConstraint intersection(final SubtypeConstraint other) {
		final BooleanListConstraint o = (BooleanListConstraint) other;
		return new BooleanListConstraint(constraint & o.constraint);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isElement(final Object o) {
		if (o == null) {
			return false;
		}
		final boolean b = ((Boolean) o).booleanValue();
		return b ? ((constraint & ConstraintValue.TRUE.value()) != 0) : ((constraint & ConstraintValue.FALSE.value()) != 0);
	}

	@Override
	/** {@inheritDoc} */
	public TernaryBool isEmpty() {
		return TernaryBool.fromBool(constraint == 0);
	}

	@Override
	/** {@inheritDoc} */
	public TernaryBool isEqual(final SubtypeConstraint other) {
		final BooleanListConstraint o = (BooleanListConstraint) other;
		return TernaryBool.fromBool(constraint == o.constraint);
	}

	@Override
	/** {@inheritDoc} */
	public TernaryBool isFull() {
		return TernaryBool.fromBool(constraint == ConstraintValue.ALL.value());
	}

	@Override
	/** {@inheritDoc} */
	public void toString(final StringBuilder sb) {
		sb.append(INamedNode.LEFTPARENTHESES);
		if ((constraint & ConstraintValue.FALSE.value()) != 0) {
			sb.append(GeneralConstants.FALSE);
		}
		if (constraint == ConstraintValue.ALL.value()) {
			sb.append(", ");
		}
		if ((constraint & ConstraintValue.TRUE.value()) != 0) {
			sb.append(GeneralConstants.TRUE);
		}
		sb.append(INamedNode.RIGHTPARENTHESES);
	}

	@Override
	/** {@inheritDoc} */
	public BooleanListConstraint union(final SubtypeConstraint other) {
		final BooleanListConstraint o = (BooleanListConstraint) other;
		return new BooleanListConstraint(constraint | o.constraint);
	}

}
