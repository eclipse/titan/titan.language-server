/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.titan.lsp.common.product.ProductIdentity;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.DocumentComment.CommentTag;
import org.eclipse.titan.lsp.AST.Assignments;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ModuleImportation;
import org.eclipse.titan.lsp.AST.ModuleImportationChain;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ModuleVersionAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstances;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.TitanVersionAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.VersionRequirementAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.types.Anytype_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Referenced_Value;
import org.eclipse.titan.lsp.codeAction.CodeActionHelpers;
import org.eclipse.titan.lsp.codeAction.CodeActionsForMissingFunction.CodeActionType_MissingFunction;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;
import org.eclipse.titan.lsp.codeAction.data.MissingFunctionData;
import org.eclipse.titan.lsp.core.CompilerVersionInformationCollector;
import org.eclipse.titan.lsp.core.LoadBalancingUtilities;
import org.eclipse.titan.lsp.core.LspMarker;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.ProductIdentityHelper;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.parsers.ProjectStructureDataCollector;
import org.eclipse.titan.lsp.parsers.extensionattributeparser.ExtensionAttributeAnalyzer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_reparser_optionalWithStatementContext;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;
import org.eclipse.titan.lsp.semantichighlighting.ISemanticInformation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Represents a Module.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Jeno Attila Balasko
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public final class TTCN3Module extends Module implements ISemanticInformation {
	private static final String FULLNAMEPART = ".control";

	private static final String MISSINGREFERENCE = "There is no visible definition with name `{0}'' in module `{1}''";
	private static final String IMPMODEXISTS = "Imported module with identifier `{0}'' already exists and refers to a different module";
	private static final String IMPMODPREV = "Previous imported module with identifier `{0}'' is here";

	private List<String> languageSpecifications;
	private final List<ImportModule> importedModules;
	private final List<FriendModule> friendModules;
	private final Definitions definitions;
	private ControlPart controlpart;

	private Location commentLocation = null;

	// for TTCNPP modules
	private Set<Path> includedFiles = null;
	// for TTCNPP modules
	private List<Location> inactiveCodeLocations = null;

	// The any type of the module
	private final Anytype_Type anytype;
	private final Def_Type anytypeDefinition;

	private WithAttributesPath withAttributesPath = null;

	// The "supplied" version
	private ProductIdentity versionNumber = null;

	//TODO detecting missing references should be done in Titanium.
	private final List<Reference> missingReferences;

	private boolean needsTobeBuilt = true;

	private static final int PROTOTYPE_INDEX = -1;
	
	public TTCN3Module(final Identifier identifier/*, final IProject project*/) {
		super(identifier/*, project*/);

		importedModules = new CopyOnWriteArrayList<ImportModule>();
		friendModules = new CopyOnWriteArrayList<FriendModule>();

		definitions = new Definitions();
		definitions.setParentScope(this);
		definitions.setFullNameParent(this);

		anytype = new Anytype_Type();
		anytypeDefinition = new Def_Type(new Identifier(Identifier_type.ID_TTCN, Type_type.TYPE_ANYTYPE.getName()), anytype);
		anytypeDefinition.setMyScope(this);
		anytypeDefinition.setFullNameParent(this);

		missingReferences = new ArrayList<Reference>();
	}

	@Override
	/** {@inheritDoc} */
	public module_type getModuletype() {
		return module_type.TTCN3_MODULE;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = new StringBuilder();
		builder.append(INamedNode.MODULENAMEPREFIX).append(getIdentifier().getDisplayName());

		if (controlpart != null && controlpart == child) {
			return builder.append(FULLNAMEPART);
		} else if (anytypeDefinition == child) {
			final Identifier identifier = anytypeDefinition.getIdentifier();

			return builder.append(INamedNode.DOT).append(identifier.getDisplayName());
		}
		
		return builder;
	}

	public void setLanguageSpecifications(final List<String> languageSpecifications) {
		this.languageSpecifications = languageSpecifications;
	}

	public List<String> getLanguageSpecifications() {
		return languageSpecifications;
	}

	/**
	 * Sets the location of this definition list.
	 *
	 * @param location
	 *                the location to set.
	 * */
	public void setDefinitionsLocation(final Location location) {
		definitions.setLocation(location);
	}

	/**
	 * Sets the module's version information
	 *
	 * @param versionNumber
	 *                the version number.
	 */
	private void setVersion(final ProductIdentity versionNumber) {
		this.versionNumber = versionNumber;
	}

	@Override
	public Location getCommentLocation() {
		return commentLocation;
	}

	@Override
	public void setCommentLocation(final Location commentLocation) {
		this.commentLocation = commentLocation;
	}

	public void addDefinitions(final List<Definition> definitionList) {
		definitions.addAll(definitionList);
	}

	public void addImportedModule(final ImportModule impmod) {
		if (impmod == null || impmod.getIdentifier() == null || impmod.getLocation() == null) {
			return;
		}

		importedModules.add(impmod);
		impmod.setMyModule(identifier);
		impmod.setMyModule(this);
		impmod.setProject(project);
	}

	public void addGroup(final Group group) {
		if (group != null && group.getIdentifier() != null) {
			definitions.addGroup(group);
		}
	}

	public void addFriendModule(final FriendModule friendModule) {
		if (friendModule != null) {
			friendModules.add(friendModule);
			friendModule.setProject(project);
		}
	}

	/**
	 * Adds a list of friend module declarations to the list of friend modules.
	 * <p>
	 * The project of the newly added declarations are set to this project
	 * here.
	 *
	 * @param friendModuleList
	 *                the friend modules to be added
	 * */
	public void addFriendModules(final List<FriendModule> friendModuleList) {
		if (friendModuleList != null) {
			final ArrayList<FriendModule> safeToAdd = new ArrayList<FriendModule>(friendModuleList.size());
			for (final FriendModule friendModule : friendModuleList) {
				if (friendModule != null) {
					safeToAdd.add(friendModule);
					friendModule.setProject(project);
				}
			}
			friendModules.addAll(safeToAdd);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Definitions getAssignmentsScope() {
		return definitions;
	}

	@Override
	/** {@inheritDoc} */
	public Assignments getAssignments() {
		return definitions;
	}

	public Definitions getDefinitions(){
		return definitions;
	}

	@Override
	/** {@inheritDoc} */
	public Def_Type getAnytype() {
		return anytypeDefinition;
	}

	/**
	 * Checks if there is an address type defined in the module.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle
	 *
	 * @return a pointer to the TTCN-3 special address type that is defined
	 *         in the TTCN-3 module. null is returned if the address type is
	 *         not defined in this module.
	 * */
	public IType getAddressType(final CompilationTimeStamp timestamp) {
		final Identifier addressIdentifier = new Identifier(Identifier_type.ID_TTCN, "address");

		if (!definitions.hasLocalAssignmentWithID(timestamp, addressIdentifier)) {
			return null;
		}

		final Definition definition = definitions.getLocalAssignmentByID(timestamp, addressIdentifier);

		if (!Assignment_type.A_TYPE.semanticallyEquals(definition.getAssignmentType())) {
			return null;
		}

		return definition.getType(timestamp);
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		if (!importedModules.isEmpty()) {
			return new Object[] { importedModules, definitions };
		}
		return new Object[] { definitions };
	}

	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		return SymbolKind.Module;
	}
	
	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return location.getRange();
	}
	
	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() {
		return identifier.getLocation().getRange();
	}
	
	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		if (identifier == null) {
			return null;
		}
		final DocumentSymbol symbol = new DocumentSymbol(identifier.getDisplayName(), getOutlineSymbolKind(),
			getOutlineRange(), getOutlineSelectionRange());
		symbol.setChildren(getOutlineSymbolChildren());
		
		return symbol;
	}
	
	@Override
	/** {@inheritDoc} */
	public List<DocumentSymbol> getOutlineSymbolChildren() {
		final List<DocumentSymbol> symbols = new ArrayList<>();
		for (final Assignment definition : definitions) {
			if (definition.getOutlineSymbolKind() != null) {				
				final DocumentSymbol child = new DocumentSymbol(definition.getFullName(), definition.getOutlineSymbolKind(),
					definition.getOutlineRange(), definition.getOutlineSelectionRange());
				child.setChildren(definition.getOutlineSymbolChildren());
				symbols.add(child);
			}
		}
		return symbols;
	}

	/**
	 * Adds the control part to this module.
	 *
	 * @param controlpart
	 *                the controlpart to be added.
	 * */
	public void addControlpart(final ControlPart controlpart) {
		if (controlpart != null) {
			this.controlpart = controlpart;
			controlpart.setMyScope(definitions);
			controlpart.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isValidModuleId(final Identifier identifier) {
		if (identifier == null) {
			return false;
		}

		final String originalName = identifier.getName();
		// The identifier represents the current module
		if (this.identifier != null && originalName.equals(this.identifier.getName())) {
			return true;
		}
		// The identifier represents a module imported in the current
		// module
		for (final ImportModule impMod : importedModules) {
			if (originalName.equals(impMod.getIdentifier(false).getName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void checkImports(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain, final List<Module> moduleStack) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastImportCheckTimeStamp != null && !lastImportCheckTimeStamp.isLess(timestamp)) {
			return;
		}

		for (final ImportModule impmod : importedModules) {
			impmod.setUsedForImportation(false);
		}

		for (final ImportModule impmod : importedModules) {
			referenceChain.markState();
			impmod.checkImports(timestamp, referenceChain, moduleStack);//This checks only existence, not "used or not used"
			//timestamp is set!!!, markers are refreshed
			referenceChain.previousState();
			LoadBalancingUtilities.astNodeChecked();
		}
		
		checkUniqueness(timestamp);
		
		lastImportCheckTimeStamp = timestamp; //timestamp can be set only here to handle circular import chains!
	}
	
	private void checkUniqueness(final CompilationTimeStamp timestamp) {
		final Map<String,ImportModule> importMap = new HashMap<>();
		for (final ImportModule impmod : importedModules) {
			final Identifier id = impmod.getIdentifier(false);
			final String name = id.getName();
			if (importMap.containsKey(name)) {
				final ImportModule impmod2 = importMap.get(name);
				if (!impmod2.getIdentifier(true).equals(impmod.getIdentifier(true))) {
					final LspMarker prevMarker = impmod2.getIdentifier().getLocation().reportError(
						MessageFormat.format(IMPMODPREV, id.getDisplayName()));
					final LspMarker modMarker = impmod.getIdentifier().getLocation().reportError(
						MessageFormat.format(IMPMODEXISTS, id.getDisplayName()));
					prevMarker.addRelatedInformation(impmod.getLocation(), "Conflicting import");
					modMarker.addRelatedInformation(impmod2.getLocation(), "Conflicting import");
				}
			} else {
				importMap.put(name, impmod);
			}
		}
	}

	/**
	 * Checks that each friend module of this module is provided only once.
	 * */
	private void checkFriendModuleUniqueness() {
		if (isBuildCancelled()) {
			return;
		}
		
		if (!friendModules.isEmpty()) {
			final Map<String, FriendModule> map = new HashMap<String, FriendModule>(friendModules.size());

			for (final FriendModule friendModule : friendModules) {
				final Identifier identifier = friendModule.getIdentifier();
				final String name = identifier.getName();
				if (map.containsKey(name)) {
					final Location otherLocation = map.get(name).getIdentifier().getLocation();
					otherLocation.reportSingularSemanticError(MessageFormat.format(
							"Duplicate friend module `{0}'' was first declared here", identifier.getDisplayName()));

					final Location friendLocation = friendModule.getIdentifier().getLocation();
					friendLocation.reportSingularSemanticError(MessageFormat.format(
							"Duplicate friend module `{0}'' was declared here again", identifier.getDisplayName()));
				} else {
					map.put(name, friendModule);
				}
			}
		}
	}

	/**
	 * Collects the module importations into a list. The list shall always
	 * exist even if being empty.
	 *
	 * @return the list of modules imported.
	 * */
	public List<ImportModule> getImports() {
		final List<ImportModule> result = new ArrayList<ImportModule>(importedModules.size());
		result.addAll(importedModules);

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public List<Module> getImportedModules() {
		final List<Module> result = new ArrayList<Module>();

		for (final ImportModule impmod : importedModules) {
			final Module module = impmod.getReferredModule();
			if (module != null) {
				result.add(module);
			}
		}

		return result;
	}

	/**
	 * Collects the fiend modules into a list. The list shall always
	 * exist even if being empty.
	 *
	 * @return the list of friend modules.
	 * */
	public List<FriendModule> getFriendModules() {
		final List<FriendModule> result = new ArrayList<FriendModule>(friendModules.size());
		result.addAll(friendModules);

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasUnhandledImportChanges() {
		for (final ImportModule impmod : importedModules) {
			if (impmod.hasUnhandledChange()) {
				return true;
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;
		needsTobeBuilt = true;

		if (getSkippedFromSemanticChecking()) {
			return;
		}

		NamingConventionHelper.checkConvention(NamingConventionElement.Ttcn3Module, identifier, "TTCN-3 module");

		// re-initialize at the beginning of the cycle.
		versionNumber = null;
		missingReferences.clear();

		definitions.setGenName();
		anytypeDefinition.setGenName(anytypeDefinition.getIdentifier().getName());

		if (withAttributesPath != null) {
			MarkerHandler.removeMarkers(withAttributesPath, true);
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

		for (final ImportModule impMod : importedModules) {
			impMod.check(timestamp);
		}

		checkFriendModuleUniqueness();
		for (final FriendModule friendModule : friendModules) {
			friendModule.check(timestamp);
		}

		analyzeExtensionAttributes(timestamp);

		anytypeDefinition.check(timestamp);
		definitions.check(timestamp);

		if (controlpart != null) {
			controlpart.check(timestamp);
		}
		
		checkDocumentComment();
		checkLanguageSpecification();
	}

	/**
	 * Experimental method for BrokenPartsViaInvertedImports.
	 */
	public void checkWithDefinitions(final CompilationTimeStamp timestamp, final List<Assignment> assignments) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;
		needsTobeBuilt = true;

		NamingConventionHelper.checkConvention(NamingConventionElement.Ttcn3Module, identifier, "TTCN-3 module");

		// re-initialize at the beginning of the cycle.
		versionNumber = null;
		missingReferences.clear();

		if (withAttributesPath != null) {
			MarkerHandler.removeMarkers(withAttributesPath, true);
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

		for (ImportModule impMod : importedModules) {
			impMod.check(timestamp);
		}

		checkFriendModuleUniqueness();
		for (final FriendModule friendModule : friendModules) {
			friendModule.check(timestamp);
		}

		analyzeExtensionAttributes(timestamp);

		anytypeDefinition.check(timestamp);
		definitions.check(timestamp);

		definitions.checkWithDefinitions(timestamp, assignments);

		if (controlpart != null) {
			controlpart.check(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (isBuildCancelled()) {
			return;
		}
		
		definitions.postCheck();

		if (controlpart != null) {
			controlpart.postCheck();
		}
	}

	/**
	 * Convert and check the version, requires and titan version extension attributes.
	 *
	 * @param timestamp
	 *                the timestamp of the actual build cycle.
	 * */
	private void analyzeExtensionAttributes(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (withAttributesPath == null) {
			return;
		}

		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);

		SingleWithAttribute attribute;
		List<AttributeSpecification> specifications = null;
		for (int i = 0; i < realAttributes.size(); i++) {
			attribute = realAttributes.get(i);
			if (Attribute_Type.Extension_Attribute.equals(attribute.getAttributeType())) {
				final Qualifiers qualifiers = attribute.getQualifiers();
				if (qualifiers == null || qualifiers.size() == 0) {
					if (specifications == null) {
						specifications = new ArrayList<AttributeSpecification>();
					}
					specifications.add(attribute.getAttributeSpecification());
				}
			}
		}

		if (specifications == null) {
			return;
		}

		final List<ExtensionAttribute> attributes = new ArrayList<ExtensionAttribute>();
		AttributeSpecification specification;
		for (int i = 0; i < specifications.size(); i++) {
			specification = specifications.get(i);
			final ExtensionAttributeAnalyzer analyzer = new ExtensionAttributeAnalyzer();
			analyzer.parse(specification);
			final List<ExtensionAttribute> temp = analyzer.getAttributes();
			if (temp != null) {
				attributes.addAll(temp);
			}
		}

		ExtensionAttribute extensionAttribute;
		for (int i = 0; i < attributes.size(); i++) {
			extensionAttribute = attributes.get(i);

			switch (extensionAttribute.getAttributeType()) {
			case VERSION: {
				final ModuleVersionAttribute moduleVersion = (ModuleVersionAttribute) extensionAttribute;
				moduleVersion.parse();
				if (versionNumber != null) {
					moduleVersion.getLocation().reportSemanticError("Duplicate version attribute");
				} else {
					setVersion(moduleVersion.getVersionNumber());
				}
				break;
			}
			case REQUIRES: {
				final VersionRequirementAttribute versionReq = (VersionRequirementAttribute) extensionAttribute;
				versionReq.parse();

				ImportModule theImport = null;
				final String requiredModuleName = versionReq.getRequiredModule().getName();
				for (final ImportModule impMod : importedModules) {
					if (requiredModuleName.equals(impMod.getIdentifier().getName())) {
						theImport = impMod;
						break;
					}
				}
				if (theImport == null) {
					final String message = MessageFormat.format(ImportModule.MISSINGMODULE, versionReq.getRequiredModule()
							.getDisplayName());
					versionReq.getRequiredModule().getLocation().reportSemanticError(message);
				} else {
					final TTCN3Module theImportedModule = (TTCN3Module) theImport.getReferredModule();
					if (theImportedModule == null) {
						break;
					}
					// make sure the version attribute is parsed (if any)
					theImportedModule.check(timestamp);
					final ProductIdentity requiredVersion = versionReq.getVersionNumber();
					if (requiredVersion != null && theImportedModule.versionNumber != null
							&& theImportedModule.versionNumber.compareTo(requiredVersion) < 0) {
						final String message = MessageFormat
								.format("Module `{0}'' requires version {1} of module `{2}'', but only version {3} is available",
										identifier.getDisplayName(), requiredVersion,
										theImportedModule.getIdentifier().getDisplayName(),
										theImportedModule.versionNumber);
						versionReq.getLocation().reportSemanticError(message);
					}
				}
				break;
			}
			case TITANVERSION: {
				final TitanVersionAttribute titanReq = (TitanVersionAttribute) extensionAttribute;
				titanReq.parse();
				final ProductIdentity requiredTITANVersion = titanReq.getVersionNumber();
				final String temp = CompilerVersionInformationCollector.getCompilerProductNumber();
				final ProductIdentity compilerVersion = ProductIdentityHelper.getProductIdentity(temp, null);
				if (requiredTITANVersion != null && compilerVersion != null && compilerVersion.compareTo(requiredTITANVersion) < 0) {
					final String message = MessageFormat.format(
							"Module `{0}'' requires TITAN version {1}, but version {2} is used right now",
							identifier.getDisplayName(), requiredTITANVersion, compilerVersion);
					titanReq.getLocation().reportSemanticError(message);
				}
				if (requiredTITANVersion != null && GeneralConstants.ON_THE_FLY_ANALYZER_VERSION != null
						&& GeneralConstants.ON_THE_FLY_ANALYZER_VERSION.compareTo(requiredTITANVersion) < 0) {
					final String message = MessageFormat.format(
							"Module `{0}'' requires TITAN version {1}, but the on-the-fly analyzer is of version {2}",
							identifier.getDisplayName(), requiredTITANVersion,
							GeneralConstants.ON_THE_FLY_ANALYZER_VERSION);
					titanReq.getLocation().reportSemanticError(message);
				}

				break;
			}
			default:
				// we don't care
				break;
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public Scope getSmallestEnclosingScope(final Position offset) {
		if (location == null || offset.before(location.getStartPosition()) || offset.after(location.getEndPosition())) {
			return null;
		}
		if (controlpart != null && controlpart.getLocation() != null && controlpart.getLocation().getStartPosition().before(offset)
				&& offset.before(controlpart.getLocation().getEndPosition())) {
			return controlpart.getSmallestEnclosingScope(offset);
		}
		if (!NULL_Location.INSTANCE.equals(definitions.getLocation()) && definitions.getLocation().getStartPosition().before(offset)
				&& offset.before(definitions.getLocation().getEndPosition())) {
			return definitions.getSmallestEnclosingScope(offset);
		}
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasImportedAssignmentWithID(final CompilationTimeStamp timestamp, final Identifier identifier) {
		for (final ImportModule impMod : importedModules) {
			if (impMod.hasImportedAssignmentWithID(timestamp, identifier)) {
				return true;
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public Definition importAssignment(final CompilationTimeStamp timestamp, final Identifier moduleId, final Reference reference) {
		final Definition result = definitions.getLocalAssignmentByID(timestamp, reference.getId());
		if (result == null) {
			return null;
		}

		final VisibilityModifier modifier = result.getVisibilityModifier();

		switch (modifier) {
		case Public:
			return result;
		case Friend:
			for (final FriendModule friend : friendModules) {
				if (friend.getIdentifier().equals(moduleId)) {
					return result;
				}
			}

			return null;
		case Private:
			return null;
		default:
			return result;
		}
	}

	/**
	 * Checks whether the module importation in this module is visible in
	 * the provided module or not.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param moduleId
	 *                the identifier of the module, against which the
	 *                visibility of the assignment is checked.
	 * @param impmod
	 *                the module importation to check.
	 *
	 * @return true if it is visible, false otherwise.
	 * */
	public boolean isVisible(final CompilationTimeStamp timestamp, final Identifier moduleId, final ImportModule impmod) {
		final VisibilityModifier modifier = impmod.getVisibilityModifier();
		switch (modifier) {
		case Public:
			return true;
		case Friend:
			for (final FriendModule friend : friendModules) {
				if (friend.getIdentifier().equals(moduleId)) {
					return true;
				}
			}
			return false;
		case Private:
			return false;
		default:
			return false;
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isVisible(final CompilationTimeStamp timestamp, final Identifier moduleId, final Assignment assignment) {
		if (!(assignment instanceof Definition)) {
			return false;
		}

		if (definitions.getLocalAssignmentByID(timestamp, assignment.getIdentifier()) != assignment) {
			return false;
		}

		final VisibilityModifier modifier = ((Definition) assignment).getVisibilityModifier();
		switch (modifier) {
		case Public:
			return true;
		case Friend:
			for (final FriendModule friend : friendModules) {
				if (friend.getIdentifier().equals(moduleId)) {
					return true;
				}
			}
			return false;
		case Private:
			return false;
		default:
			return false;
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		// if a moduleId is present, that import (or the actual module)
		// must be searched
		final Identifier moduleId = reference.getModuleIdentifier();
		final Location referenceLocation = reference.getLocation();
		final Identifier id = reference.getId();

		if (id == null) {
			return null;
		}

		Assignment temporalAssignment = null;

		if (moduleId == null) {
			// no module name is given in the reference
			if (Type_type.TYPE_ANYTYPE.getName().equals(id.getTtcnName())) {
				return anytypeDefinition;
			}

			Assignment tempResult = null;

			for (final ImportModule impMod : importedModules) {
				if (impMod.getReferredModule() != null) {
					final ModuleImportationChain referenceChain = new ModuleImportationChain(ModuleImportationChain.CIRCULARREFERENCE,
							false);
					tempResult = impMod.importAssignment(timestamp, referenceChain, identifier, reference,
							new ArrayList<ModuleImportation>());
					if (tempResult != null
							&& !tempResult.getMyScope().getModuleScope()
							.isVisible(timestamp, this.getIdentifier(), tempResult)) {
						tempResult = null;
					}
					if (tempResult != null) {
						if (temporalAssignment == null) {
							temporalAssignment = tempResult;
						} else if (temporalAssignment != tempResult) {
							reference.getLocation().reportSemanticError(
									"It is not possible to resolve this reference unambigously, as  it can be resolved to `"
											+ temporalAssignment.getFullName() + "' and to `"
											+ tempResult.getFullName() + "'");
							return null;
						}
					}
				}
			}

			if (temporalAssignment != null) {
				return temporalAssignment;
			}
			
			final List<ISubReference> subReferences = reference.getSubreferences();
			if (!subReferences.isEmpty()) {
				if (subReferences.get(0) instanceof ParameterisedSubReference) {
					final List<CodeActionType_MissingFunction> actionTypes = new ArrayList<>(Arrays.asList(
							CodeActionType_MissingFunction.GENERATEFUNCTIONASDEFINITION
						));
					final String data = getMissingFunctionData(actionTypes, timestamp, reference);
					referenceLocation.reportSemanticError(MessageFormat.format(MISSINGREFERENCE, id.getDisplayName(),
							identifier.getDisplayName()), actionTypes, CodeActionProviderType.MISSINGFUNCTION, data);
					missingReferences.add(reference);
					return temporalAssignment;
				}
			}

			referenceLocation
				.reportSemanticError(MessageFormat.format(MISSINGREFERENCE, id.getDisplayName(), identifier.getDisplayName()));

			missingReferences.add(reference);
		} else if (moduleId.getName().equals(name)) {
			// the reference points to the own module
			if (Type_type.TYPE_ANYTYPE.getName().equals(id.getTtcnName())) {
				return anytypeDefinition;
			}

			temporalAssignment = definitions.getLocalAssignmentByID(timestamp, id);
			if (temporalAssignment == null) {
				referenceLocation.reportSemanticError(MessageFormat.format(MISSINGREFERENCE, id.getDisplayName(),
						identifier.getDisplayName()));
			}
		} else {
			// the reference points to another module
			for (final ImportModule impMod : importedModules) {
				if (moduleId.getName().equals(impMod.getName())) {
					if (impMod.getReferredModule() == null) {
						referenceLocation.reportSemanticError(MessageFormat.format(MISSINGREFERENCE, id.getDisplayName(),
								impMod.getIdentifier().getDisplayName()));
						return null;
					}

					final ModuleImportationChain referenceChain = new ModuleImportationChain(ModuleImportationChain.CIRCULARREFERENCE,
							false);
					temporalAssignment = impMod.importAssignment(timestamp, referenceChain, identifier, reference,
							new ArrayList<ModuleImportation>());
					if (!impMod.getReferredModule().isVisible(timestamp, this.getIdentifier(), temporalAssignment)) {
						temporalAssignment = null;
					}
					if (temporalAssignment == null) {
						referenceLocation.reportSemanticError(MessageFormat.format(MISSINGREFERENCE, id.getDisplayName(),
								impMod.getIdentifier().getDisplayName()));
					}

					return temporalAssignment;
				}
			}

			missingReferences.add(reference);
		}

		return temporalAssignment;
	}
	
	/**
	 * Collects those references whose referred assignment could not be
	 * found.
	 *
	 * @return the list of missing references.
	 * */
	public List<Reference> getMissingReferences() {
		return missingReferences;
	}

	@Override
	/** {@inheritDoc} */
	public String toString() {
		final StringBuilder builder = new StringBuilder("module: ").append(name);
		return builder.toString();
	}


	/**
	 * Sets the with attributes for this module if it has any. Also creates
	 * the with attribute path, to store the attributes in.
	 *
	 * @param attributes
	 *                the attribute to be added.
	 * */
	public void setWithAttributes(final MultipleWithAttributes attributes) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}
		if (attributes != null) {
			withAttributesPath.setWithAttributes(attributes);
		}
	}

	/**
	 * @return the with attribute path element of this module. If it did not
	 *         exist it will be created.
	 * */
	public WithAttributesPath getAttributePath() {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		return withAttributesPath;
	}

	@Override
	/** {@inheritDoc} */
	public void extractStructuralInformation(final ProjectStructureDataCollector collector) {
		for (final ImportModule imported : importedModules) {
			collector.addImportation(identifier, imported.getIdentifier());
		}
	}

	private int reparseAfterModule(final TTCN3ReparseUpdater aReparser) {
		return aReparser.parse(parser -> {
			final Pr_reparser_optionalWithStatementContext root = parser.pr_reparser_optionalWithStatement();
			ParserUtilities.logParseTree( root, parser );
			final MultipleWithAttributes attributes = root.attributes;

			final ParseTree rootEof = parser.pr_EndOfFile();
			ParserUtilities.logParseTree( rootEof, parser );
			if ( parser.isErrorListEmpty() ) {
				withAttributesPath = new WithAttributesPath();
				withAttributesPath.setWithAttributes(attributes);
				if (attributes != null) {
					getLocation().setEndPosition(attributes.getLocation().getEndPosition());
				}
			}
		});
	}

	private int reparseInsideAttributelist(final TTCN3ReparseUpdater aReparser) {
		return aReparser.parse(parser -> {
			final Pr_reparser_optionalWithStatementContext root = parser.pr_reparser_optionalWithStatement();
			ParserUtilities.logParseTree( root, parser );
			final MultipleWithAttributes attributes = root.attributes;

			final ParseTree rootEof = parser.pr_EndOfFile();
			ParserUtilities.logParseTree( rootEof, parser );
			if ( parser.isErrorListEmpty() ) {
				withAttributesPath.setWithAttributes(attributes);
				getLocation().setEndPosition(attributes.getLocation().getEndPosition());
			}
		});
	}

	/**
	 * Handles the incremental parsing of this definition.
	 *
	 * @param reparser
	 *                the parser doing the incremental parsing.
	 * @param sourceParser
	 *                the general utility handling the parse of TTCN-3 and
	 *                ASN.1 files, to efficiently handle module renaming.
	 * */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final ProjectSourceParser sourceParser) throws ReParseException {
		needsTobeBuilt = true;

		if (reparser.getShift() < 0) {
			throw new ReParseException();
		}
		// edited outside the module
		if (reparser.getDamageEndPosition().before(location.getStartPosition())) {
			// before the module
			reparser.updateLocation(identifier.getLocation());
			if (definitions != null) {
				definitions.updateSyntax(reparser, importedModules, friendModules, controlpart);
			}
			if (controlpart != null) {
				controlpart.updateSyntax(reparser, reparser.isDamaged(controlpart.getLocation()));
				reparser.updateLocation(controlpart.getLocation());
			}
			if (withAttributesPath != null) {
				withAttributesPath.updateSyntax(reparser, false);
				reparser.updateLocation(withAttributesPath.getLocation());
			}
			return;
		} else if (reparser.getDamageStartPosition().after(location.getEndPosition())) {
			// after the module
			if (withAttributesPath == null || withAttributesPath.getAttributes() == null) {
				// new attribute might have appeared
				reparser.extendDamagedRegionTillFileEnd();
				final int result = reparseAfterModule( reparser );

				if (result != 0) {
					throw new ReParseException();
				}
			}
			return;
		}

		// edited the module identifier
		final Location temporalIdentifier = identifier.getLocation();
		if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
			reparser.extendDamagedRegion(temporalIdentifier);
			final IIdentifierReparser r = new IdentifierReparser(reparser);
			final int result = r.parse();
			identifier = r.getIdentifier();
			// damage handled
			if (result != 0) {
				throw new ReParseException(result);
			}

			if (definitions != null) {
				definitions.updateSyntax(reparser, importedModules, friendModules, controlpart);
			}
			if (controlpart != null) {
				controlpart.updateSyntax(reparser, reparser.isDamaged(controlpart.getLocation()));
				reparser.updateLocation(controlpart.getLocation());
			}
			if (withAttributesPath != null) {
				withAttributesPath.updateSyntax(reparser, false);
				reparser.updateLocation(withAttributesPath.getLocation());
			}
			return;
		} else if (reparser.isDamaged(temporalIdentifier)) {
			throw new ReParseException();
		}

		// the module has structurally changed
		boolean enveloped = false;
		if ((definitions != null && reparser.envelopsDamage(definitions.getLocation()))
				|| (controlpart != null && reparser.envelopsDamage(controlpart.getLocation()))) {
			if (definitions != null && reparser.isAffected(definitions.getLocation())) {
				definitions.updateSyntax(reparser, importedModules, friendModules, controlpart);
				reparser.updateLocation(definitions.getLocation());
			}
			if (controlpart != null && reparser.isAffected(controlpart.getLocation())) {
				controlpart.updateSyntax(reparser, reparser.isDamaged(controlpart.getLocation()));
				reparser.updateLocation(controlpart.getLocation());
			}

			enveloped = true;
		}

		if (withAttributesPath != null && reparser.isAffected(withAttributesPath.getLocation())) {
			// The modification happened inside the attribute list
			if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
				reparser.extendDamagedRegion(withAttributesPath.getLocation());
				final int result = reparseInsideAttributelist( reparser );

				if (result != 0) {
					throw new ReParseException();
				}

				return;
			} else if (enveloped) {
				// The modification happened inside the module
				withAttributesPath.updateSyntax(reparser, reparser.envelopsDamage(withAttributesPath.getLocation()));

				reparser.updateLocation(withAttributesPath.getLocation());
			} else {
				// Something happened that we can not handle,
				// for example the with attribute were commented out
				throw new ReParseException();
			}
		}

		if (!enveloped) {
			throw new ReParseException();
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		if (definitions == null) {
			return null;
		}
		return definitions.getEnclosingAssignment(offset);
	}

	@Override
	public List<Definition> getVisibleDefinitions() {
		final List<Definition> defs = new ArrayList<>();
		defs.addAll(definitions.getVisibleDefinitions());
		getImportedModules().forEach(mod -> {
			if (!(mod instanceof TTCN3Module)) {
				return;
			}
			final List<Definition> importedDefList = ((TTCN3Module)mod).getDefinitions().getDefinitions();
			for (Definition def : importedDefList) {
				final VisibilityModifier visibility = def.getVisibilityModifier();
				if (visibility == null || visibility == VisibilityModifier.Public) {
					defs.add(def);
				} else if (visibility == VisibilityModifier.Friend) {
					final List<FriendModule> fl = ((TTCN3Module)mod).getFriendModules();
					for (FriendModule fm : fl) {
						if (fm.getIdentifier().equals(this.getIdentifier())) {
							defs.add(def);
						}
					}
				}
			}
		});

		return defs;
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (definitions != null) {
			definitions.findReferences(referenceFinder, foundIdentifiers);
		}
		if (controlpart != null) {
			controlpart.findReferences(referenceFinder, foundIdentifiers);
		}
		if (anytypeDefinition != null) {
			anytypeDefinition.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (importedModules != null) {
			for (final ImportModule im : importedModules) {
				if (!im.accept(v)) {
					return false;
				}
			}
		}
		if (friendModules != null) {
			for (final FriendModule fm : friendModules) {
				if (!fm.accept(v)) {
					return false;
				}
			}
		}
		if (definitions != null) {
			if (!definitions.accept(v)) {
				return false;
			}
		}
		if (controlpart != null) {
			if (!controlpart.accept(v)) {
				return false;
			}
		}
		if (anytypeDefinition != null) {
			if (!anytypeDefinition.accept(v)) {
				return false;
			}
		}
		if (withAttributesPath != null) {
			if (!withAttributesPath.accept(v)) {
				return false;
			}
		}

		return true;
	}

	public void setIncludedFiles(final Set<Path> includedFiles) {
		this.includedFiles = includedFiles;
	}

	public Set<Path> getIncludedFiles() {
		return includedFiles;
	}

	public void setInactiveCodeLocations(final List<Location> inactiveCodeLocations) {
		this.inactiveCodeLocations = inactiveCodeLocations;
	}

	public List<Location> getInactiveCodeLocations() {
		return inactiveCodeLocations != null ? inactiveCodeLocations : new ArrayList<Location>();
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		hoverContent.addText(MODULE).addText(" ").addText(identifier.getDisplayName());

		if (hasDocumentComment()) {
			final DocumentComment dc = getDocumentComment();
			dc.addDescsContent(hoverContent);
			dc.addVerdictsContent(hoverContent);
			dc.addRequirementsContent(hoverContent);
			dc.addPurposeContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding)
			.append(ind).append(ICommentable.AUTHOR_TAG).append(lineEnding)
			.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	private String getMissingFunctionData(final List<CodeActionType_MissingFunction> actionTypes, final CompilationTimeStamp timestamp, final Reference reference) {
		final String lineEnding = ProjectItem.getLineEnding(location);
		boolean isReferenced = false;
		final CodeActionHelpers helper = new CodeActionHelpers();
		final StringBuilder code = new StringBuilder();
		code.append(lineEnding.repeat(2))
			.append("function ")
			.append(reference.getDisplayName().substring(0, reference.getDisplayName().length() - 1));			
		final ParameterisedSubReference subref = (ParameterisedSubReference)reference.getSubreferences().get(0);
		final ParsedActualParameters parsed = subref.getParsedParameters();
		final TemplateInstances instances =  parsed.getInstances();
		for (int i = 0; i < instances.size(); i++) {
			final TemplateInstance inst = instances.get(i);
			if (i > 0 ) {
				code.append(", ");
			}
			code.append(Type.typeToSrcType(inst.getExpressionReturntype(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE)))
				.append(" param" + (i + 1)); 
		}
		code.append(RIGHTPARENTHESES);
		Type_type valueType = null;
		if (reference.getNameParent() instanceof Referenced_Value) {
			Referenced_Value value = (Referenced_Value)reference.getNameParent();
			final IType governor = value.getMyGovernor();
			if (governor != null) {
				valueType = value.getMyGovernor().getTypetype();
				code.append(" return ")
					.append(Type.typeToSrcType(valueType))
					.append(' ');
				isReferenced = true;
			}
		}
		code.append(CURLYOPEN).append(lineEnding);
		helper.addExtraIndentedText(code, "// function body");
		code.append(lineEnding);
		if (isReferenced) {
			code.append(lineEnding);
			helper.addExtraIndentedText(code, "return ");
			code.append(Type.typeToSrcTypeDefault(valueType))
				.append(";")
				.append(lineEnding);
		}
		code.append(CURLYCLOSE);

		int nrOfDefinitions = getDefinitions().size();
		Assignment lastDefinition = getDefinitions().get(nrOfDefinitions - 1);
		Location definitionInsertLocation = NULL_Location.INSTANCE;
		if (lastDefinition != null) {
			definitionInsertLocation = lastDefinition.getLocation();
		}
		Location classDefinitionsInsertLocation = NULL_Location.INSTANCE;
		final Scope scope = reference.getMyScope();
		if(scope != null) {
			final Class_Type enclosingClass = reference.getMyScope().getEnclosingClass();

			Location modifierLocation = null;
			if (enclosingClass != null) {
				actionTypes.add(CodeActionType_MissingFunction.GENERATEFUNCTIONASMEMBER);
				nrOfDefinitions = enclosingClass.getDefinitionMap().size();
				lastDefinition = enclosingClass.getDefinitionMap().get(nrOfDefinitions - 1);
				classDefinitionsInsertLocation = lastDefinition.getLocation();
				modifierLocation = ((Definition)lastDefinition).getVisibilityModifierLocation();
				if (modifierLocation != null) {
					classDefinitionsInsertLocation.setStartPosition(modifierLocation.getStartPosition());
				}
			}
		}

		final MissingFunctionData functionData = 
			new MissingFunctionData(code.toString(), definitionInsertLocation, classDefinitionsInsertLocation);
		GsonBuilder builder = new GsonBuilder();
		CodeActionHelpers.registerTypeAdapters(builder);
		Gson gson = builder.create();
		return gson.toJson(functionData);
	}

	public List<Definition> getAllVisibleDefinitions(Position position) {
		final List<Definition> visibleDefinitions = new ArrayList<>();
		Scope scope = getSmallestEnclosingScope(position);
		if (scope == null) {
			return visibleDefinitions;
		}

		final List<Definition> defs = scope.getVisibleDefinitions();
		if (defs != null) {
			final List<Definition> visibleDefs = defs
					.stream()
					.filter(def -> def.getLocation().getEndPosition().before(position))
					.filter(def -> {
						final Path filePath = def.getLocation().getFile().toPath();
						if (filePath == null) {
							return false;
						}

						final ProjectItem projectItem = Project.INSTANCE.getProjectItem(filePath);
						if (projectItem != null && projectItem.isExcluded()) {
							return false;
						}

						return true;
					})
					.collect(Collectors.toList());
			if (!visibleDefs.isEmpty()) {
				visibleDefinitions.addAll(visibleDefs);
			}
		}

		scope = scope.getParentScope();
		while(scope != null) {
			final List<Definition> defList = scope.getVisibleDefinitions();
			if (defList != null) {
				visibleDefinitions.addAll(defList);
			}
			final Scope parentScope = scope.getParentScope();
			scope = scope == parentScope ? null : parentScope;
		}

		return visibleDefinitions;
	}

	/**
	 * Clears all existing references pointing to definitions in this module.
	 * It is needed to remove outdated definitions so references
	 * in other modules will be correctly updated.
	 */
	public void clearExistingReferences() {
		final Definitions moduleDefs = getDefinitions();
		if (moduleDefs != null) {
			final List<Definition> defs = moduleDefs.getDefinitions();
			if (defs != null) {
				for (final Definition def : defs) {
					def.clearExistingReferences();
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(identifier.getLocation(), SemanticType.Namespace);
		getInactiveCodeLocations().forEach(loc ->
			AstSemanticHighlighting.addSemanticToken(loc, SemanticType.Inactive)
		);
	}

	@Override
	public void checkDocumentComment() {
		if (!hasDocumentComment()) {
			return;
		}

		final EnumSet<CommentTag> naTags =
				EnumSet.of(CommentTag.Config, CommentTag.Exception, CommentTag.Member, CommentTag.Param, CommentTag.Priority);
		naTags.add(CommentTag.Return);

		getDocumentComment().checkNonApplicableTags(naTags, "modules");
		super.checkDocumentComment();
	}

	/**
	 * Checks if the language version of imported modules is newer than the importing module.
	 */
	private void checkLanguageSpecification() {
		final int myVersion = getVersionFromLanguageSpecification(languageSpecifications);
		if (myVersion == -1) {
			return;
		}

		for (final ImportModule impMod : importedModules) {
			if (impMod.getReferredModule() instanceof TTCN3Module) {
				final TTCN3Module imported = (TTCN3Module)impMod.getReferredModule();
				List<String> importedSpecs = imported.getLanguageSpecifications();
				int importedVersion = -1;

				if (importedSpecs != null) {
					importedVersion = getVersionFromLanguageSpecification(importedSpecs);
				}
				if (importedVersion == -1) {
					importedSpecs = impMod.getLanguageSpecifications();
					if (importedSpecs == null) {
						continue;
					}
					importedVersion = getVersionFromLanguageSpecification(importedSpecs);
				}
				if (importedVersion == -1) {
					continue;
				}
				if (importedVersion > myVersion) {
					impMod.getLocation().reportStandardWarning("The TTCN-3 language specification of the imported module is "
						+ "newer than the language specification of the importing module");
				}
			}
		}
	}

	/**
	 * Parses the language version from the language specification and returns it as an integer.
	 * The version must be in form of "TTCN-3:ABCD" where ABCD is a four digit number.
	 * Only the first version specification is matched (language specification is free text and can have
	 * multiple comma-separated entries).
	 * 
	 * @param specifications
	 * @return
	 */
	private int getVersionFromLanguageSpecification(final List<String> specifications) {
		if (specifications == null) {
			return -1;
		}

		final String versionRegex = "^TTCN-3:(\\d{4})$";
		final Pattern pattern = Pattern.compile(versionRegex);

		for (final String spec : specifications) {
			final Matcher matcher = pattern.matcher(spec);
			if (matcher.matches()) {
				return Integer.parseInt(matcher.group(1));
			}
		}
		return -1;
	}
}
