/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.product;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;
import org.eclipse.titan.lsp.titanium.markers.types.ProblemNameToPreferenceMapper;
import org.eclipse.titan.lsp.titanium.markers.types.TaskType;
import org.eclipse.titan.lsp.titanium.metrics.common.AltstepMetric;
import org.eclipse.titan.lsp.titanium.metrics.common.FunctionMetric;
import org.eclipse.titan.lsp.titanium.metrics.common.IMetricEnum;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricGroup;
import org.eclipse.titan.lsp.titanium.metrics.common.ModuleMetric;
import org.eclipse.titan.lsp.titanium.metrics.common.ProjectMetric;
import org.eclipse.titan.lsp.titanium.metrics.common.TestcaseMetric;
import org.eclipse.titan.lsp.titanium.preferences.PreferenceConstants;
import org.eclipse.titan.lsp.titanium.preferences.ProblemTypePreference;

public final class DefaultPreferences {

	private static boolean initiated = false;

	private DefaultPreferences() {
		// Hide constructor
	}

	public static void initialize() {
		// Import organization
		addPreference(PreferenceConstants.ORG_IMPORT_ADD, true);
		addPreference(PreferenceConstants.ORG_IMPORT_REMOVE, true);
		addPreference(PreferenceConstants.ORG_IMPORT_SORT, true);
		addPreference(PreferenceConstants.ORG_IMPORT_METHOD, "just_change");


		// Code smells
		addPreference(ProblemTypePreference.UNUSED_IMPORT.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.UNUSED_GLOBAL_DEFINITION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.UNUSED_LOCAL_DEFINITION.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.UNUSED_FUNTION_RETURN_VALUES.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.MISSING_IMPORT.getPreferenceName(), GeneralConstants.ERROR);
		addPreference(ProblemTypePreference.MISSING_FRIEND.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.INCORRECT_SHIFT_ROTATE_SIZE.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.UNNECESSARY_CONTROLS.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.UNNECESSARY_VALUEOF.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.INFINITE_LOOP.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.READONLY_VARIABLE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.GOTO.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.PRIVATE_COMPONENT_VARIABLE_ACCESS.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.INVOKING_FUNCTIONS_FROM_SPECIFIC_PLACES.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.CIRCULAR_IMPORTATION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.LANGUAGE_MIXING.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.NONPRIVATE_PRIVATE.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.VISIBILITY_IN_DEFINITION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.MODULENAME_IN_DEFINITION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.TYPENAME_IN_DEFINITION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.MAGIC_CONSTANTS.getPreferenceName(), GeneralConstants.IGNORE);
		// addPreference(ProblemTypePreference.DUPLICATE_NAME.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.TOO_MANY_PARAMETERS.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(PreferenceConstants.TOO_MANY_PARAMETERS_SIZE, 7);
		addPreference(ProblemTypePreference.TOO_COMPLEX_EXPRESSIONS.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(PreferenceConstants.TOO_COMPLEX_EXPRESSIONS_SIZE, 3);
		addPreference(ProblemTypePreference.EMPTY_STATEMENT_BLOCK.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.TOO_MANY_STATEMENTS.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(PreferenceConstants.TOO_MANY_STATEMENTS_SIZE, 150);
		addPreference(ProblemTypePreference.IF_WITHOUT_ELSE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.IF_NOT_WITHOUT_ELSE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.SWITCH_ON_BOOLEAN.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.SETVERDICT_WITHOUT_REASON.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.LAZY.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.PRIVATE_FIELD_VIA_PUBLIC.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.PRIVATE_VALUE_VIA_PUBLIC.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.LOGIC_INVERSION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.UNCOMMENTED_FUNCTION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.UNINITIALIZED_VARIABLE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.SIZECHECK_IN_LOOP.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.STOP_IN_FUNCTION.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.RECEIVE_ANY_TEMPLATE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.IF_INSTEAD_ALTGUARD.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.IF_INSTEAD_RECEIVE_TEMPLATE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.ALTSTEP_COVERAGE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.SHORTHAND.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.ISBOUND_WITHOUT_ELSE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.ISVALUE_WITH_VALUE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.ITERATE_ON_WRONG_ARRAY.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.CONSECUTIVE_ASSIGNMENTS.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.TOO_COMPLEX_OF_TYPE.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(PreferenceConstants.TOO_MANY_CONSECUTIVE_ASSIGNMENTS_SIZE, 4);
		addPreference(ProblemTypePreference.CONVERT_TO_ENUM.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.SELECT_COVERAGE.getPreferenceName(), GeneralConstants.IGNORE);
		addPreference(ProblemTypePreference.SELECT_UNION.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.SELECT_WITH_NUMBERS_SORTED.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(PreferenceConstants.ON_THE_FLY_SMELLS, false);
		addPreference(ProblemTypePreference.UNNECESSARY_ARRAYS.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.GROUPS_WITHOUT_ATTRIBUTES.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.CONNECTION_DETECTOR.getPreferenceName(), GeneralConstants.ERROR);
		addPreference(ProblemTypePreference.STARTED_FUNCTION_WITH_OUT_INOUT_PARAMETERS.getPreferenceName(), GeneralConstants.WARNING);
		addPreference(ProblemTypePreference.OVERRIDE_IN_ATTRIBUTES.getPreferenceName(), GeneralConstants.WARNING);


		// Initialize default values of metrics
		for (final IMetricEnum metric : MetricGroup.knownMetrics()) {
			final String name = metric.id();
			addPreference(PreferenceConstants.nameMetricEnabled(name), true);
			addPreference(PreferenceConstants.nameMetricRisk(name), 0);
			addPreference(PreferenceConstants.nameMetricLimits(name), "");
			if (!(metric instanceof ProjectMetric)) {
				addPreference(PreferenceConstants.nameMetricGraph(name), true);
			}
		}
		// Set those exceptional:
		addPreference(PreferenceConstants.nameMetricRisk(FunctionMetric.NUMBER_OF_PARAMETERS.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(FunctionMetric.NUMBER_OF_PARAMETERS.id()), "5;7");
		addPreference(PreferenceConstants.nameMetricRisk(FunctionMetric.LINES_OF_CODE.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(FunctionMetric.LINES_OF_CODE.id()), "100;150");
		addPreference(PreferenceConstants.nameMetricRisk(FunctionMetric.CYCLOMATIC_COMPLEXITY.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(FunctionMetric.CYCLOMATIC_COMPLEXITY.id()), "10;20");
		addPreference(PreferenceConstants.nameMetricRisk(FunctionMetric.NESTING.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(FunctionMetric.NESTING.id()), "4;6");

		addPreference(PreferenceConstants.nameMetricRisk(TestcaseMetric.LINES_OF_CODE.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(TestcaseMetric.LINES_OF_CODE.id()), "100;150");
		addPreference(PreferenceConstants.nameMetricRisk(TestcaseMetric.CYCLOMATIC_COMPLEXITY.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(TestcaseMetric.CYCLOMATIC_COMPLEXITY.id()), "10;20");
		addPreference(PreferenceConstants.nameMetricRisk(TestcaseMetric.NESTING.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(TestcaseMetric.NESTING.id()), "4;6");
		addPreference(PreferenceConstants.nameMetricRisk(TestcaseMetric.NUMBER_OF_PARAMETERS.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(TestcaseMetric.NUMBER_OF_PARAMETERS.id()), "5;7");

		addPreference(PreferenceConstants.nameMetricRisk(AltstepMetric.LINES_OF_CODE.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(AltstepMetric.LINES_OF_CODE.id()), "100;150");
		addPreference(PreferenceConstants.nameMetricRisk(AltstepMetric.CYCLOMATIC_COMPLEXITY.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(AltstepMetric.CYCLOMATIC_COMPLEXITY.id()), "10;20");
		addPreference(PreferenceConstants.nameMetricRisk(AltstepMetric.NESTING.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(AltstepMetric.NESTING.id()), "4;6");
		addPreference(PreferenceConstants.nameMetricRisk(AltstepMetric.NUMBER_OF_PARAMETERS.id()), 3);
		addPreference(PreferenceConstants.nameMetricLimits(AltstepMetric.NUMBER_OF_PARAMETERS.id()), "5;7");

		addPreference(PreferenceConstants.nameMetricRisk(ModuleMetric.NOF_FIXME.id()), 1);
		addPreference(PreferenceConstants.nameMetricLimits(ModuleMetric.NOF_FIXME.id()), "1");
		addPreference(PreferenceConstants.nameMetricRisk(ModuleMetric.INSTABILITY.id()), 1);
		addPreference(PreferenceConstants.nameMetricLimits(ModuleMetric.INSTABILITY.id()), "0.3");

		// Initialize default impact and baseline values for code smells:
		addPreference(PreferenceConstants.BASE_RISK_FACTOR, 43);

		// care about task markers:
		setTaskImpactAndBaseLine(TaskType.FIXME, 2, 28000);
		setTaskImpactAndBaseLine(TaskType.TODO, 1, 7000);

		// populate baselines:
		setSmellImpactAndBaseLine(CodeSmellType.CIRCULAR_IMPORTATION, 1, 50000);
		setSmellImpactAndBaseLine(CodeSmellType.LANGUAGE_MIXING, 1, 50000);
		setSmellImpactAndBaseLine(CodeSmellType.TOO_MANY_STATEMENTS, 2, 20000);
		setSmellImpactAndBaseLine(CodeSmellType.TOO_MANY_PARAMETERS, 2, 1500);
		// setSmellImpactAndBaseLine(preferenceStore, SemanticProblemType., 1,
		// 10); // TODO: divergent naming convention
		setSmellImpactAndBaseLine(CodeSmellType.UNCOMMENTED_FUNCTION, 2, 2000);
		setSmellImpactAndBaseLine(CodeSmellType.TYPENAME_IN_DEFINITION, 2, 200);
		setSmellImpactAndBaseLine(CodeSmellType.MODULENAME_IN_DEFINITION, 2, 4000);
		setSmellImpactAndBaseLine(CodeSmellType.VISIBILITY_IN_DEFINITION, 2, 15000);
		setSmellImpactAndBaseLine(CodeSmellType.UNINITIALIZED_VARIABLE, 2, 50);
		setSmellImpactAndBaseLine(CodeSmellType.GOTO, 3, 2000);
		setSmellImpactAndBaseLine(CodeSmellType.PRIVATE_COMPONENT_VARIABLE_ACCESS, 3, 2000); // TODO
		setSmellImpactAndBaseLine(CodeSmellType.INVOKING_FUNCTIONS_FROM_SPECIFIC_PLACES, 3, 2000); // TODOTODO 3
		setSmellImpactAndBaseLine(CodeSmellType.UNUSED_IMPORT, 1, 150);
		setSmellImpactAndBaseLine(CodeSmellType.UNUSED_GLOBAL_DEFINITION, 1, 150);
		setSmellImpactAndBaseLine(CodeSmellType.UNUSED_LOCAL_DEFINITION, 2, 300);
		setSmellImpactAndBaseLine(CodeSmellType.UNUSED_FUNTION_RETURN_VALUES, 2, 2000);
		setSmellImpactAndBaseLine(CodeSmellType.UNUSED_STARTED_FUNCTION_RETURN_VALUES, 3, 5000);
		setSmellImpactAndBaseLine(CodeSmellType.STARTED_FUNCTION_WITH_OUT_INOUT_FORMAL_PARAMETERS, 3, 5000);
		setSmellImpactAndBaseLine(CodeSmellType.INFINITE_LOOP, 3, 80000);
		// setSmellImpactAndBaseLine(preferenceStore, SemanticProblemType., 3,
		// 400000); // TODO: busy wait
		setSmellImpactAndBaseLine(CodeSmellType.NONPRIVATE_PRIVATE, 1, 100);
		// setSmellImpactAndBaseLine(preferenceStore,
		// SemanticProblemType.INCORRECT_SHIFT_ROTATE_SIZE, 2, ); // TODO:
		// missing label in ExRotSize in the xls
		setSmellImpactAndBaseLine(CodeSmellType.SIZECHECK_IN_LOOP, 2, 1000);
		setSmellImpactAndBaseLine(CodeSmellType.TOO_COMPLEX_OF_TYPE, 2, 2000);
		setSmellImpactAndBaseLine(CodeSmellType.TOO_COMPLEX_EXPRESSIONS, 2, 1000);
		setSmellImpactAndBaseLine(CodeSmellType.READONLY_INOUT_PARAM, 2, 4000);
		setSmellImpactAndBaseLine(CodeSmellType.READONLY_OUT_PARAM, 2, 50000);
		setSmellImpactAndBaseLine(CodeSmellType.READONLY_LOC_VARIABLE, 2, 100);
		setSmellImpactAndBaseLine(CodeSmellType.EMPTY_STATEMENT_BLOCK, 1, 1000);
		setSmellImpactAndBaseLine(CodeSmellType.SETVERDICT_WITHOUT_REASON, 2, 500);
		// setSmellImpactAndBaseLine(preferenceStore, SemanticProblemType., 1,
		// 8000); // TODO: cannot identify VariOutEn smell
		setSmellImpactAndBaseLine(CodeSmellType.STOP_IN_FUNCTION, 2, 30000);
		setSmellImpactAndBaseLine(CodeSmellType.UNNECESSARY_VALUEOF, 2, 80000);
		setSmellImpactAndBaseLine(CodeSmellType.MAGIC_NUMBERS, 2, 50);
		setSmellImpactAndBaseLine(CodeSmellType.MAGIC_STRINGS, 2, 15);
		setSmellImpactAndBaseLine(CodeSmellType.LOGIC_INVERSION, 1, 4000);
		setSmellImpactAndBaseLine(CodeSmellType.UNNECESSARY_CONTROLS, 3, 50000);

		// graph constants
//		addPreference(PreferenceConstants.NO_ITERATIONS, 500);
//		addPreference(PreferenceConstants.DAG_DISTANCE, TitaniumDAGLayout.SUM_DISTANCE_ALGORITHM);

		// graph clustering defaults
		addPreference(PreferenceConstants.CLUSTER_SPACE, true);
		addPreference(PreferenceConstants.CLUSTER_SMALL_LARGE, false);
		addPreference(PreferenceConstants.CLUSTER_DEPTH, 3);
		addPreference(PreferenceConstants.CLUSTER_ITERATION, 20);
		addPreference(PreferenceConstants.CLUSTER_SIZE_LIMIT, 7);
		addPreference(PreferenceConstants.CLUSTER_AUTO_FOLDER, true);
		addPreference(PreferenceConstants.CLUSTER_AUTO_REGEXP, true);
		addPreference(PreferenceConstants.CLUSTER_AUTO_NAME, true);

		initiated = true;
	}
	
	public static boolean isInitiated() {
		return initiated;
	}

	private static void addPreference(String preference, Object defaultValue) {
		Configuration.INSTANCE.addPreference(preference, defaultValue);
	}

	private static void setSmellImpactAndBaseLine(final CodeSmellType t, final int imp, final int bl) {
		addPreference(ProblemNameToPreferenceMapper.nameSmellImpact(t.name()), imp);
		addPreference(ProblemNameToPreferenceMapper.nameSmellBaseLine(t.name()), bl);
	}

	private static void setTaskImpactAndBaseLine(final TaskType t, final int imp, final int bl) {
		addPreference(ProblemNameToPreferenceMapper.nameSmellImpact(t.name()), imp);
		addPreference(ProblemNameToPreferenceMapper.nameSmellBaseLine(t.name()), bl);
	}
}
