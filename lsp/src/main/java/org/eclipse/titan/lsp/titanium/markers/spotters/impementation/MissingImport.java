/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ImportModule;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class MissingImport extends BaseModuleCodeSmellSpotter {
	private static final String MISSING_MODULE = "There is no module with name `{0}''";

	public MissingImport() {
		super(CodeSmellType.MISSING_IMPORT);
		addStartNode(ImportModule.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof ImportModule) {
			final ImportModule s = (ImportModule) node;
			if (s.getReferredModule() == null) {
				final String msg = MessageFormat.format(MISSING_MODULE, s.getIdentifier().getDisplayName());
				problems.report(s.getIdentifier().getLocation(), msg);
			}
		}
	}
}
