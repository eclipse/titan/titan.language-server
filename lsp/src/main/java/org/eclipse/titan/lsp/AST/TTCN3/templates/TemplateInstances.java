/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a list of template instances.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class TemplateInstances extends ASTNode
	implements ILocateableNode, IIncrementallyUpdatable, ICollection<TemplateInstance> {

	private final List<TemplateInstance> instances;

	/** The location of the template instances. */
	private Location location = Location.getNullLocation();

	public TemplateInstances() {
		super();
		instances = new ArrayList<TemplateInstance>();
	}

	public TemplateInstances(final TemplateInstances other) {
		super();
		instances = new ArrayList<TemplateInstance>(other.instances.size());
		for (final TemplateInstance otherInstance : other.instances) {
			instances.add(otherInstance);
		}
		location = other.location;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		for (final TemplateInstance instance : instances) {
			instance.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = instances.size(); i < size; i++) {
			if (instances.get(i) == child) {
				return builder.append(INamedNode.SQUAREOPEN).append(String.valueOf(i + 1)).append(INamedNode.SQUARECLOSE);
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	public boolean add(final TemplateInstance instance) {
		if (instance != null) {
			instance.setFullNameParent(this);
			return instances.add(instance);
		}
		return false;
	}

	@Override
	public int size() {
		return instances.size();
	}

	@Override
	public TemplateInstance get(final int index) {
		return instances.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final TemplateInstance instance : instances) {
			instance.updateSyntax(reparser, false);
			reparser.updateLocation(instance.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (instances == null) {
			return;
		}

		for (final TemplateInstance ti : instances) {
			ti.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final TemplateInstance ti : instances) {
			if (!ti.accept(v)) {
				return false;
			}
		}
		return true;
	}

	public String createStringRepresentation() {
		final StringBuilder sb = new StringBuilder();
		for (final TemplateInstance ti : instances) {
			sb.append(ti.createStringRepresentation()).append(", ");
		}
		if (!instances.isEmpty()) {
			sb.setLength(sb.length() - 2);
		}
		return sb.toString();
	}

	@Override
	public Iterator<TemplateInstance> iterator() {
		return instances.iterator();
	}
}
