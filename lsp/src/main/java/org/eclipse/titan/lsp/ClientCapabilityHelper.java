/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import org.eclipse.lsp4j.ClientCapabilities;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.SemanticTokensWorkspaceCapabilities;
import org.eclipse.lsp4j.WindowClientCapabilities;
import org.eclipse.lsp4j.WorkspaceClientCapabilities;

/** 
 * Static helper class for querying and storing client capabilities.
 * 
 * Some client side lsp implementations are not complete and we have to make
 * sure not to send requests/notifications for unsupported features. 
 * 
 * @author Miklos Magyari
 *
 */
public final class ClientCapabilityHelper {
	private static boolean semanticTokensSupported = false;
	private static boolean progressSupported = false;
		
	private ClientCapabilityHelper() {
		// Do nothing
	}
	
	public static void getClientCapabilities(final InitializeParams params) {
		final ClientCapabilities clientCaps = params.getCapabilities();
        final WorkspaceClientCapabilities clientWsCaps = clientCaps.getWorkspace();
        if (clientWsCaps != null) {
        	final SemanticTokensWorkspaceCapabilities stwc = clientWsCaps.getSemanticTokens();
        	if (stwc != null) {
        		semanticTokensSupported = stwc.getRefreshSupport();
        	}
        }
        final WindowClientCapabilities clientWindowCaps = clientCaps.getWindow();
        if (clientWindowCaps != null) {
        	progressSupported = clientWindowCaps.getWorkDoneProgress();
        }
	}

	public static boolean isSemanticTokensSupported() {
		return semanticTokensSupported;
	}

	public static boolean isProgressSupported() {
		return progressSupported;
	}
}
