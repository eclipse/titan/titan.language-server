/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Error_Setting;
import org.eclipse.titan.lsp.AST.ISetting;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public final class Erroneous_FieldSpecification extends FieldSpecification {

	private ISetting settingError;
	private final boolean hasDefaultFlag;

	public Erroneous_FieldSpecification(final Identifier identifier, final boolean isOptional, final boolean hasDefault) {
		super(identifier, isOptional);
		hasDefaultFlag = hasDefault;
	}

	@Override
	/** {@inheritDoc} */
	public Fieldspecification_types getFieldSpecificationType() {
		return Fieldspecification_types.FS_ERROR;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasDefault() {
		return hasDefaultFlag;
	}

	@Override
	/** {@inheritDoc} */
	public ISetting getDefault() {
		if (null == settingError && hasDefaultFlag) {
			settingError = new Error_Setting();
		}
		return settingError;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		// Do nothing
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (identifier != null && !identifier.accept(v)) {
			return false;
		}
		return true;
	}
}
