/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.core.Position;

public class ContextStringLocator {
	private Scope scope;
	private Module module;
	private String sourceText;
	private Position cursorPosition;
	private Statement actualStatement;
	private Definition actualDefinition;

	public ContextStringLocator(final Scope scope, final Module module, final String sourceText, Position cursorPosition) {
		this.scope = scope;
		this.module = module;
		this.sourceText = sourceText;
		this.cursorPosition = cursorPosition;
	}

	private String getContextStringFromTTCN() {
		IContextLocator contextLocator = null;
		if (scope instanceof Definitions) {
			final DefinitionBlockContextLocator locator = new DefinitionBlockContextLocator((Definitions)scope, (TTCN3Module)module, sourceText, cursorPosition);
			actualDefinition = locator.getActualDefinition();
			contextLocator = locator;
		}

		if (scope instanceof StatementBlock) {
			final StatementBlockContextLocator locator = new StatementBlockContextLocator((StatementBlock)scope, (TTCN3Module)module, sourceText, cursorPosition);
			actualStatement = locator.getActualStatement();
			contextLocator = locator;
		}

		if (scope instanceof NamedBridgeScope) {
			contextLocator = new NamedBridgeContextLocator((NamedBridgeScope)scope, (TTCN3Module)module, sourceText, cursorPosition);
		}

		if (contextLocator == null) {
			return null;
		}

		return contextLocator.getContextString();
	}

	public String getContextString() {
		if (module instanceof TTCN3Module) {
			return getContextStringFromTTCN();
		}
		return null;
	}

	public Statement getActualStatement() {
		return actualStatement;
	}

	public Definition getActualDefinition() {
		return actualDefinition;
	}
}
