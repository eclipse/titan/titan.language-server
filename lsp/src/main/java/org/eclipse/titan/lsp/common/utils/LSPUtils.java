/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.utils;

import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;

public final class LSPUtils {

	private LSPUtils() {
		// Hide constructor
	}

	private static String getLineEnding(final ProjectItem projectItem) {
		return projectItem == null ? StringUtils.DEFAULT_LINE_SEPARATOR : projectItem.getLineEnding();
	}

	public static String getLineEnding(TextDocumentIdentifier document) {
		return getLineEnding(Project.INSTANCE.getProjectItem(document.getUri()));
	}

	public static String getLineEnding(Location location) {
		return getLineEnding(Project.INSTANCE.getProjectItem(location.getFile().toPath()));
	}

	public static DiagnosticSeverity getDiagnosticSeverity(final String severityLevel) {
		return getDiagnosticSeverity(severityLevel, DiagnosticSeverity.Error);
	}
		
	public static DiagnosticSeverity getDiagnosticSeverity(final String severityLevel, final DiagnosticSeverity defaultSeverity) {
		if (severityLevel == null) {
			TitanLogger.logDebug(new UnsupportedOperationException("null is used, this should not be a coincidence"));
			return defaultSeverity;
		}
		switch (severityLevel.toLowerCase()) {
		case "error":
			return DiagnosticSeverity.Error;
		case "warning":
			return DiagnosticSeverity.Warning;
		case "info":
			return DiagnosticSeverity.Information;
		case "hint":
			return DiagnosticSeverity.Hint;
		default:
			return defaultSeverity;
		}
	}
}
