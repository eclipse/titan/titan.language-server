/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.util.List;
import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstances;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The SelectCase class is helper class for the SelectCase_Statement class.
 * Represents a select case branch parsed from the source code.
 *
 * @see SelectCase_Statement
 * @see SelectCases
 *
 * @author Kristof Szabados
 * */
public final class SelectCase extends ASTNode implements ILocateableNode, IIncrementallyUpdatable {
	private static final String FULLNAMEPART1 = ".templateinstances";
	private static final String FULLNAMEPART2 = ".block";

	private final TemplateInstances templateInstances;
	private final StatementBlock statementBlock;

	private Location location = NULL_Location.INSTANCE;

	public SelectCase(final TemplateInstances templateInstances, final StatementBlock statementblock) {
		this.templateInstances = templateInstances;
		this.statementBlock = statementblock;

		if (templateInstances != null) {
			templateInstances.setFullNameParent(this);
		}
		if (statementblock != null) {
			statementblock.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (templateInstances == child) {
			return builder.append(FULLNAMEPART1);
		} else if (statementBlock == child) {
			return builder.append(FULLNAMEPART2);
		}

		return builder;
	}

	public StatementBlock getStatementBlock() {
		return statementBlock;
	}

	/**
	 * Sets the scope of the select case branch.
	 *
	 * @param scope
	 *                the scope to be set.
	 * */
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (templateInstances != null) {
			templateInstances.setMyScope(scope);
		}
		if (statementBlock != null) {
			statementBlock.setMyScope(scope);
		}
	}

	public void setMyStatementBlock(final StatementBlock parStatementBlock, final int index) {
		if (statementBlock != null) {
			statementBlock.setMyStatementBlock(parStatementBlock, index);
		}
	}

	public void setMyDefinition(final Definition definition) {
		if (statementBlock != null) {
			statementBlock.setMyDefinition(definition);
		}
	}

	public void setMyAltguards(final AltGuards altGuards) {
		if (statementBlock != null) {
			statementBlock.setMyAltguards(altGuards);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	/** @return true if the select case is the else case, false otherwise. */
	public boolean hasElse() {
		return templateInstances == null;
	}

	/**
	 * Checks whether the select case has a return statement, either
	 * directly or embedded.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return the return status of the select case.
	 * */
	public StatementBlock.ReturnStatus_type hasReturn(final CompilationTimeStamp timestamp) {
		if (statementBlock != null) {
			return statementBlock.hasReturn(timestamp);
		}

		return StatementBlock.ReturnStatus_type.RS_NO;
	}

	/**
	 * Used when generating code for interleaved statement.
	 * If the block has no receiving statements, then the general code generation can be used
	 *  (which may use blocks).
	 * */
	public boolean hasReceivingStatement() {
		if (statementBlock != null) {
			return statementBlock.hasReceivingStatement(0);
		}

		return false;
	}

	/**
	 * Does the semantic checking of this select case.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param governor
	 *                the governor of the select expression, to check the
	 *                cases against.
	 * @param unreachable
	 *                tells if this case branch is still reachable or not.
	 *
	 * @return true if this case branch was found to be unreachable, false
	 *         otherwise.
	 * */
	public boolean check(final CompilationTimeStamp timestamp, final IType governor, final boolean unreachable) {
		if (isBuildCancelled()) {
			return false;
		}
		
		boolean unreachable2 = unreachable;
		if (templateInstances != null) {
			for (TemplateInstance ti : templateInstances) {
				ti.check(timestamp, governor);
			}
		} else {
			unreachable2 = true;
		}

		statementBlock.check(timestamp);

		return unreachable2;
	}

	/**
	 * Checks if some statements are allowed in an interleave or not
	 * */
	public void checkAllowedInterleave() {
		if (isBuildCancelled()) {
			return;
		}
		
		if (statementBlock != null) {
			statementBlock.checkAllowedInterleave();
		}
	}

	/**
	 * Checks the properties of the statement, that can only be checked
	 * after the semantic check was completely run.
	 */
	public void postCheck() {
		if (isBuildCancelled()) {
			return;
		}
		
		if (statementBlock != null) {
			statementBlock.postCheck();
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		if (templateInstances != null) {
			templateInstances.updateSyntax(reparser, false);
			reparser.updateLocation(templateInstances.getLocation());
		}

		if (statementBlock != null) {
			statementBlock.updateSyntax(reparser, false);
			reparser.updateLocation(statementBlock.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (templateInstances != null) {
			templateInstances.findReferences(referenceFinder, foundIdentifiers);
		}
		if (statementBlock != null) {
			statementBlock.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (templateInstances != null && !templateInstances.accept(v)) {
			return false;
		}
		if (statementBlock != null && !statementBlock.accept(v)) {
			return false;
		}
		return true;
	}
}
