/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ControlPart;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Testcase;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 *
 * @author Farkas Izabella Ingrid
 */
public class UnusedLocalDefinition extends BaseModuleCodeSmellSpotter {
	public UnusedLocalDefinition() {
		super(CodeSmellType.UNUSED_LOCAL_DEFINITION);
		addStartNodes(Arrays.asList(Def_Altstep.class, Def_Function.class, Def_Testcase.class, ControlPart.class));
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		final Set<Assignment> unused = new HashSet<Assignment>();

		final LocalDefinitionCheck chek = new LocalDefinitionCheck();
		node.accept(chek);
		unused.addAll(chek.getDefinitions());

		final LocalUsedDefinitionCheck chekUsed = new LocalUsedDefinitionCheck();
		node.accept(chekUsed);
		unused.removeAll(chekUsed.getDefinitions());

		for (final Assignment ass : unused) {
			final String name = ass.getIdentifier().getDisplayName();
			final String msg = MessageFormat.format("The {0} `{1}'' seems to be never used locally", ass.getAssignmentName(), name);
			problems.report(ass.getIdentifier().getLocation(), msg);
		}
	}

	class LocalDefinitionCheck extends ASTVisitor {

		private final Set<Assignment> setOfDefinition = new HashSet<Assignment>();

		public LocalDefinitionCheck() {
			setOfDefinition.clear();
		}

		public Set<Assignment> getDefinitions() {
			return setOfDefinition;
		}

		@Override
		public int visit(final IVisitableNode node) {
			if (node instanceof Assignment) {
				final Assignment assignment = (Assignment) node;
				if (assignment.isLocal()) {
					setOfDefinition.add(assignment);
				}
			}
			return V_CONTINUE;
		}
	}

	class LocalUsedDefinitionCheck extends ASTVisitor {

		private final Set<Assignment> setOfDefinition = new HashSet<Assignment>();

		public LocalUsedDefinitionCheck() {
			setOfDefinition.clear();
		}

		public Set<Assignment> getDefinitions() {
			return setOfDefinition;
		}

		@Override
		public int visit(final IVisitableNode node) {
			if (node instanceof Reference) {
				if (((Reference) node).getIsErroneous(CompilationTimeStamp.getBaseTimestamp())) {
					return V_CONTINUE;
				}

				final Assignment assignment = ((Reference) node).getRefdAssignment(CompilationTimeStamp.getBaseTimestamp(), false, null);
				if (assignment != null && assignment.isLocal()) {
					setOfDefinition.add(assignment);
				}
			}
			return V_CONTINUE;
		}
	}
}
