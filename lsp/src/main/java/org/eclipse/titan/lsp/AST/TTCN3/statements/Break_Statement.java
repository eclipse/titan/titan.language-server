/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * @author Farkas Izabella Ingrid
 * */
public final class Break_Statement extends Statement {
	private static final String INCORRECTUSAGE = "Break statement cannot be used outside loops,"
			+ " alt or interleave statements, altsteps or response and exception handling part of call operations";
	private static final String STATEMENT_NAME = "break";

	private Statement loop_stmt;
	private AltGuards altGuards;

	@Override
	/** {@inheritDoc} */
	public Statement_type getType() {
		return Statement_type.S_BREAK;
	}

	@Override
	/** {@inheritDoc} */
	public String getStatementName() {
		return STATEMENT_NAME;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isTerminating(final CompilationTimeStamp timestamp) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	protected void setMyLaicStmt(final AltGuards pAltGuards, final Statement pLoopStmt) {
		if (pLoopStmt != null) {
			loop_stmt = pLoopStmt;
		}
		altGuards = pAltGuards;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		if (myStatementBlock == null || !myStatementBlock.hasEnclosingLoopOrAltguard()) {
			location.reportSemanticError(INCORRECTUSAGE);
		}

		if(loop_stmt == null && altGuards == null) {
			location.reportSemanticError(INCORRECTUSAGE);
		}

		if (loop_stmt != null) {
			//FIXME: brk_cnt.loop_stmt->loop.has_brk=true;
		}
		lastTimeChecked = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		// no members
		return true;
	}
}
