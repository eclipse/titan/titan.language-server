/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.generator.JsonRpcData;

/**
 * @author Miklos Magyari
 **/
@JsonRpcData
public class GenerateDocumentCommentParams {
	private TextDocumentIdentifier identifier;
	private Position position;
	private String indentation;
	
	public GenerateDocumentCommentParams(final TextDocumentIdentifier identifier, final Position position, final String indentation) {
		this.identifier = identifier;
		this.position = position;
		this.indentation = indentation;
	}

	public TextDocumentIdentifier getIdentifier() {
		return identifier;
	}

	public Position getPosition() {
		return position;
	}

	public String getIndentation() {
		return indentation;
	}
}
