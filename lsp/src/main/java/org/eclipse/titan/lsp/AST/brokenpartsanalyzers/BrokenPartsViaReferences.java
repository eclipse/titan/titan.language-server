/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.brokenpartsanalyzers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignments;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ASN1.ASN1Assignment;
import org.eclipse.titan.lsp.AST.ASN1.Undefined_Assignment;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Peter Olah
 * @author Jeno Attila Balasko
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public final class BrokenPartsViaReferences {
	// when the definition based search for parts to be analyzed exceeds this limit we switch back to the import based method.
	// 1 second in nanoseconds
	private static final long TIMELIMIT = 10 * (long)1e+9;

	/**
	 *  When the percentage of broken modules is bigger than this limit
	 *   the module level selection shall be used.
	 *
	 * Otherwise the assignment level detection would take too long.
	 */
	private static final double BROKEN_MODULE_LIMIT = 10.0;

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss.SSS", Locale.US);
	private static final String FORMAT = "%s %s";
	private static final String HEADER = "**Selection with Broken parts via references is started at:";
	private static final String FOOTER = "**Selection with Broken parts via references is ended at:  ";
	private static final String INFO = "**Selection time is:     {0} seconds ({1} nanoseconds)." + System.lineSeparator()
									 + "**Nr. of broken modules: {2}" + System.lineSeparator()
									 + "**Broken modules name:   {3}" + System.lineSeparator();

	private List<Module> allModules;
	private List<Module> modulesToCheck;
	private List<Module> modulesToSkip;
	private List<String> semanticallyChecked;
	private final Map<Module, List<Assignment>> moduleAndBrokenAssignments;
	private long start;
	private long end;
	private boolean analyzeOnlyAssignments;
	private final CompilationTimeStamp timestamp;

	public BrokenPartsViaReferences(final CompilationTimeStamp timestamp) {
		moduleAndBrokenAssignments = new HashMap<Module, List<Assignment>>();
		analyzeOnlyAssignments = false;
		this.timestamp = timestamp;
	}

	public List<Module> getModulesToCheck() {
		return modulesToCheck;
	}

	public List<Module> getModulesToSkip() {
		return modulesToSkip;
	}

	public void setModules(final List<Module> allModules, final List<String> semanticallyChecked){
		modulesToCheck = new ArrayList<Module>();
		modulesToSkip = new ArrayList<Module>();
		this.allModules = new ArrayList<Module>(allModules);
		this.semanticallyChecked = semanticallyChecked;
	}

	public Map<Module, List<Assignment>> getModuleAndBrokenDefs() {
		return moduleAndBrokenAssignments;
	}

	public boolean getAnalyzeOnlyDefinitions() {
		return analyzeOnlyAssignments;
	}

	private void afterExecute() {
		for (final Module module : allModules) {
			if (!modulesToCheck.contains(module) && !modulesToSkip.contains(module)) {
				modulesToSkip.add(module);
			}
		}
	}

	public void execute() {
		TitanLogger.logDebug(String.format(FORMAT, HEADER, simpleDateFormat.format(new Date())));
		start = System.nanoTime();

		final List<Module> startModules = collectStartModules(allModules);
		final Map<Module, List<Module>> invertedImports = buildInvertedImportStructure(allModules, startModules);

		computeAnalyzeOnlyDefinitionsFlag(allModules, startModules);

		if (analyzeOnlyAssignments) {
			final boolean useIncrementalParsing = Configuration.INSTANCE.getBoolean(
					Configuration.USE_INCREMENTAL_PARSING, false);
			final Map<Module, List<AssignmentHandler>> result = collectBrokenParts(startModules, invertedImports,useIncrementalParsing);

//			final Map<Module, List<AssignmentHandler>> result = collectBrokenParts(startModules, invertedImports);
			if (!isTooSlow()) {
				writeDebugInfo(result);
			}
			collectRealBrokenParts(result, useIncrementalParsing);
//			collectRealBrokenParts(result);
		}

		if (isTooSlow()) {
			TitanLogger.logDebug("  Switching back to old selection format");
		}
		// if we need to use the old selection or the new selection method took too long
		if (!analyzeOnlyAssignments || isTooSlow()) {
			analyzeOnlyAssignments = false;
			modulesToCheck.clear();
			moduleAndBrokenAssignments.clear();
			final List<Module> modules = collectBrokenModulesViaInvertedImports(startModules, invertedImports);
			modulesToCheck.addAll(modules);
		}

		afterExecute();

		end = System.nanoTime() - start;
		TitanLogger.logDebug(String.format(FORMAT, FOOTER, simpleDateFormat.format(new Date())));
		//infoAfterExecute();
	}

	/**
	 * Sets the flag "analyzeOnlyAssignments" to true, if the size of the broken modules is not too high
	 *
	 * @param allModules
	 * @param startModules - the broken modules
	 */
	public void computeAnalyzeOnlyDefinitionsFlag(final List<Module> allModules, final List<Module> startModules) {
		final double brokenModulesRatio = (startModules.size() * 100.0) / allModules.size();
		if (brokenModulesRatio < BROKEN_MODULE_LIMIT) {
			analyzeOnlyAssignments = true;
			return;
		}
		analyzeOnlyAssignments = false;
	}

	/**
	 * Adds modules from allModules to a list  which
	 * - have null CompilationTimestamp or
	 * - canbeCheckRoot or
	 * - have not been checked semantically
	 *
	 * @param allModules
	 * @param startModules
	 */
	private List<Module> collectStartModules(final List<Module> allModules) {
		final List<Module> startModules = new ArrayList<Module>();
		for (final Module actualModule : allModules) {
			// Collect injured modules directly into startModules, we will start the checking from these modules.
			// Collect modules which have not been checked semantically.
			if ((actualModule.getLastCompilationTimeStamp() == null || actualModule.isCheckRoot() || !semanticallyChecked.contains(actualModule.getName())) && !startModules.contains(actualModule) ) {
				startModules.add(actualModule);
			}
		}
		return startModules;
	}

	/**
	 * It is build an inverted import structure and identify startmodules whose CompilationTimeStamp is null.
	 *
	 * @param allModules
	 *            the list of modules to be check. Initially all modules.
	 * @param startModules
	 *            the list of modules to be check. Initially all modules, but the function will remove those that can be skipped.
	 *
	 * @return invertedImports contains the following:<br>
	 *         - key: a module.<br>
	 *         - values: in these modules the key module is used, so all values imported this module, it is an inverted "imported" connection.<br>
	 *         If module A import B, C, D and module B import D, E then the next structure will be built:<br>
	 *         A = [],<br>
	 *         B = [A],<br>
	 *         C = [A],<br>
	 *         D = [A, B],<br>
	 *         E = [B]<br>
	 */
	private Map<Module, List<Module>> buildInvertedImportStructure(final List<Module> allModules, final List<Module> startModules) {

		final Map<Module, List<Module>> invertedImports = new HashMap<Module, List<Module>>();

		for (final Module actualModule : allModules) {
			// We have to add all module to get a correct inverted import structure.
			// It covers the case when a module is a top-level module, so it has't got any import.
			invertedImports.computeIfAbsent(actualModule, k -> new ArrayList<Module>());

			for (final Module actualImportedModule : actualModule.getImportedModules()) {
				if (invertedImports.containsKey(actualImportedModule)) {
					final List<Module> dependentModules = invertedImports.get(actualImportedModule);

					if (!dependentModules.contains(actualModule)) {
						dependentModules.add(actualModule);
					}
				} else {
					final List<Module> temp = new ArrayList<Module>();
					temp.add(actualModule);
					invertedImports.put(actualImportedModule, temp);
				}
			}
		}

		return invertedImports;
	}

	private List<Module> collectBrokenModulesViaInvertedImports(final List<Module> startModules, final Map<Module, List<Module>> invertedImports) {

		final List<Module> startModulesCopy = new ArrayList<Module>(startModules);

		final List<Module> result = new ArrayList<Module>();
		for (final Module startModule: startModules) {
			TitanLogger.logDebug("  ** Module " + startModule.getName() + " can not be skipped as it was not yet analyzed.");
		}

		for (int s = 0; s < startModulesCopy.size(); ++s) {
			final Module startModule = startModulesCopy.get(s);
			if (!result.contains(startModule)) {
				result.add(startModule);
			}

			final List<Module> whereStartModuleUsed = invertedImports.get(startModule);
			for (int d = 0; d < whereStartModuleUsed.size(); ++d) {
				final Module dependentModule = whereStartModuleUsed.get(d);
				if (!startModulesCopy.contains(dependentModule)) {
					startModulesCopy.add(dependentModule);
					TitanLogger.logDebug("  ** Module " + dependentModule.getName() + " cannot be skipped as it depends on "
							+ startModule.getName() + " which needs to be checked.");
				}
			}

			startModule.notCheckRoot();
			final Assignments assignments = startModule.getAssignments();
			for (final Assignment assignment : assignments) {
				assignment.notCheckRoot();
			}
		}
		return result;
	}

	private Map<Module, List<AssignmentHandler>> collectBrokenParts(
			final List<Module> startModules,
			final Map<Module, List<Module>> invertedImports,
			final boolean useIncrementalParsing) {

		final List<Module> startModulesCopy = new ArrayList<Module>(startModules);

		final Map<Module, List<AssignmentHandler>> localModuleAndBrokenAssignments = new HashMap<Module, List<AssignmentHandler>>();

		processStartModules(startModulesCopy, localModuleAndBrokenAssignments);

		for (int i = 0; i < startModulesCopy.size() && !isTooSlow(); ++i) {

			final Module startModule = startModulesCopy.get(i);
			List<AssignmentHandler> startAssignments;
			if (localModuleAndBrokenAssignments.containsKey(startModule)) {
				startAssignments = localModuleAndBrokenAssignments.get(startModule);
			} else {
				startAssignments = getAssignmentsFrom(startModule); //<<<<<< getAssignments() used in collectBrokenModulesViaInvertedImports, too
				localModuleAndBrokenAssignments.put(startModule, startAssignments);
			}

			if (startAssignments.isEmpty()) {
				continue;
			}

			final List<Module> whereStartModuleUsed = invertedImports.get(startModule);

			// Incremental parsing: If lastCompilationTimestamp of definitions of startModule is null (=name changed)
			// then all imported module shall be fully analyzed
			// If not incremental parsing is used, also all importing modules shall be fully reanalyze
			if (!useIncrementalParsing ||
					(startModule instanceof TTCN3Module &&
						((TTCN3Module) startModule).getDefinitions().getLastCompilationTimeStamp() == null)) {
				for (int j = 0; j < whereStartModuleUsed.size(); ++j) {
					final Module dependentModule = whereStartModuleUsed.get(j);
					//overwrites the dependentAssignments with the full list of assignments
					final List<AssignmentHandler> dependentAssignments = getAssignmentsFrom(dependentModule);
					localModuleAndBrokenAssignments.put(dependentModule, dependentAssignments);
				}
			} else { //incremental parsing + no name change
				for (int j = 0; j < whereStartModuleUsed.size(); ++j) {
					final Module dependentModule = whereStartModuleUsed.get(j);
					List<AssignmentHandler> dependentAssignments;
					if (localModuleAndBrokenAssignments.containsKey(dependentModule)) {
						dependentAssignments = localModuleAndBrokenAssignments.get(dependentModule);
					} else {
						dependentAssignments = getAssignmentsFrom(dependentModule);
						localModuleAndBrokenAssignments.put(dependentModule, dependentAssignments);
					}
					// We have to separate broken and not broken definition, because of postcheck.
					final List<AssignmentHandler> brokens = new ArrayList<AssignmentHandler>();
					final List<AssignmentHandler> notBrokens = new ArrayList<AssignmentHandler>();
					for (int s = 0; s < startAssignments.size(); ++s) {
						final AssignmentHandler startAssignment = startAssignments.get(s);
						if (startAssignment.getIsContagious()) {
							for (int d = 0; d < dependentAssignments.size(); ++d) {
								final AssignmentHandler dependentAssignment = dependentAssignments.get(d);
								dependentAssignment.check(startAssignment); //only infection and contagion are checked
								if (dependentAssignment.getIsInfected()) {
									if (!startModulesCopy.contains(dependentModule)) {
										startModulesCopy.add(dependentModule);
									}
									if( !brokens.contains(dependentAssignment) ) {
										brokens.add(dependentAssignment);
									}
								}
							}
						}
					}

					for (int d = 0; d < dependentAssignments.size(); ++d) {
						final AssignmentHandler dependentAssignment = dependentAssignments.get(d);
						if (!dependentAssignment.getIsInfected()) {
							notBrokens.add(dependentAssignment);
						}
					}

					// Have to post check of local definition of modules.
					// A definition can reference an other definition too.
					checkLocalAssignments(brokens, notBrokens);

					// If dependent module not added to startModules,
					// it means it has not got broken definition,
					// so we have to delete it from moduleAndBrokenDefs.
					if (!startModulesCopy.contains(dependentModule)) {
						localModuleAndBrokenAssignments.remove(dependentModule);
					}


				}
			}

		}//for

		return localModuleAndBrokenAssignments;
	}

	private void collectRealBrokenParts(
			final Map<Module, List<AssignmentHandler>> moduleAndAssignments,
			final boolean useIncrementalParsing) {
		for (final Map.Entry<Module, List<AssignmentHandler>> entry : moduleAndAssignments.entrySet()) {

			final List<Assignment> assignments = new ArrayList<Assignment>();
			for (final AssignmentHandler assignmentHandler : entry.getValue()) {
				if (!useIncrementalParsing || assignmentHandler.getIsInfected()) {
					assignments.add(assignmentHandler.getAssignment());
				}
				assignmentHandler.assignment.notCheckRoot();
			}
			final Module module = entry.getKey();
			if (!assignments.isEmpty() || !module.getSkippedFromSemanticChecking()) {
				moduleAndBrokenAssignments.put(module, assignments);
				modulesToCheck.add(module);
			}
		}
	}

	public void processStartModules(final List<Module> startModules, final Map<Module, List<AssignmentHandler>> moduleAndBrokenAssignments) {
		for (final Module startModule : startModules) {
			if(isTooSlow()) {
				return;
			}
			if (startModule instanceof TTCN3Module && startModule.getLastCompilationTimeStamp() != null && !startModule.isCheckRoot()) {
				//definition name has not changed but module semantically has not been checked:
				final Assignments startAssignments = startModule.getAssignments();
				final List<AssignmentHandler> brokens = new ArrayList<AssignmentHandler>();
				final List<AssignmentHandler> notBrokens = new ArrayList<AssignmentHandler>();
				for (final Assignment assignment: startAssignments) {
					Project.INSTANCE.removeMarkers(assignment.getLocation().getFile());
				}
				for (final Assignment startAssignment : startAssignments) {
					final AssignmentHandler assignmentHandler = AssignmentHandlerFactory.getDefinitionHandler(startAssignment);

					startAssignment.check(timestamp);
					startAssignment.accept(assignmentHandler);

					if (startAssignment.isCheckRoot()) {
						assignmentHandler.setIsInfected(true);
						startAssignment.notCheckRoot();
						assignmentHandler.addReason("Definition's infected, because of incremental parsing.");
						brokens.add(assignmentHandler);
					} else if (assignmentHandler.getIsInfected()) {
						assignmentHandler.addReason("Definition contains an infected reference.");
						brokens.add(assignmentHandler);
					} else {
						notBrokens.add(assignmentHandler);
					}
				}

				if (!brokens.isEmpty()) {
					checkLocalAssignments(brokens, notBrokens);
					if (moduleAndBrokenAssignments.containsKey(startModule)) {
						moduleAndBrokenAssignments.get(startModule).addAll(brokens);
					} else {
						moduleAndBrokenAssignments.put(startModule, brokens);
					}
				}
			} else {
				if (startModule.getLastCompilationTimeStamp() == null) {
					//The markers have been marked for removal only for ASN1 modules
					startModule.check(timestamp);
				}

				final List<AssignmentHandler> startAssignments = getAssignmentsFrom(startModule); //puts additional markers!
				for (final AssignmentHandler assignmentHandler : startAssignments) {
					assignmentHandler.initStartParts();
					assignmentHandler.assignment.notCheckRoot();
					assignmentHandler.addReason("Parent module's CompilationTimeStamp is null.");
				}

				if (moduleAndBrokenAssignments.containsKey(startModule)) {
					moduleAndBrokenAssignments.get(startModule).addAll(startAssignments);
				} else {
					moduleAndBrokenAssignments.put(startModule, startAssignments);
				}
			}

			startModule.notCheckRoot();
		}
	}

	public List<AssignmentHandler> getAssignmentsFrom(final Module module) {
		final List<AssignmentHandler> assignmentHandlers = new ArrayList<AssignmentHandler>();
		final Assignments assignments = module.getAssignments();
		for (final Assignment assignment : assignments) {
			final AssignmentHandler assignmentHandler = AssignmentHandlerFactory.getDefinitionHandler(assignment);
			if(assignment instanceof Undefined_Assignment){
				final ASN1Assignment realAssignment = ((Undefined_Assignment)assignment).getRealAssignment(CompilationTimeStamp.getBaseTimestamp());
				if(realAssignment != null) {
					realAssignment.accept(assignmentHandler);
				} else {
					assignment.accept(assignmentHandler);//TODO: re-fine this branch
				}
			} else {
				assignment.accept(assignmentHandler);
			}
			assignmentHandlers.add(assignmentHandler);
		}

		return assignmentHandlers;
	}

	private void checkLocalAssignments(final List<AssignmentHandler> brokens, final List<AssignmentHandler> notBrokens) {

		if (brokens.isEmpty() || notBrokens.isEmpty()) {
			return;
		}

		final HashMap<String, AssignmentHandler> brokenMap = new HashMap<String, AssignmentHandler>(brokens.size() + notBrokens.size());
		for(final AssignmentHandler handler: brokens) {
			brokenMap.put(handler.getAssignment().getIdentifier().getDisplayName(), handler);
		}

		boolean proceed = true;
		while (proceed) {
			proceed = false;


			for (int i = notBrokens.size() -1; i >=0; --i) {
				final AssignmentHandler notBroken = notBrokens.get(i);
				boolean found = false;
				for (final String name : notBroken.getContagiousReferences()) {
					if(brokenMap.containsKey(name)) {
						notBroken.check(brokenMap.get(name));
						found = true;
						break;
					}
				}
				if(!found) {
					for (final String name : notBroken.getNonContagiousReferences()) {
						if(brokenMap.containsKey(name)) {
							notBroken.check(brokenMap.get(name));
							found = true;
							break;
						}
					}
				}

				if(found) {
					proceed = true;
					notBrokens.remove(i);
					brokens.add(notBroken);
					brokenMap.put(notBroken.getAssignment().getIdentifier().getDisplayName(), notBroken);
				}
			}
		}
	}

	// Returns true, if the time spent is over a time limit
	// It is used to check if this kind of algorithm is too slow
	// For debugging purposes you can set it for "false"
	private boolean isTooSlow() {
		return ((System.nanoTime()-start) > TIMELIMIT);
	}

	private void writeDebugInfo(final Map<Module, List<AssignmentHandler>> moduleAndAssignments) {
		CompletableFuture.runAsync(() -> {
			StringBuilder sb = new StringBuilder();
			sb.append("  Detailed info:\n");

			for (final Map.Entry<Module, List<AssignmentHandler>> entry : moduleAndAssignments.entrySet()) {
				final List<AssignmentHandler> values = entry.getValue();

				sb.append("    module: ").append(entry.getKey().getIdentifier().getDisplayName()).append("\n");

				for (final AssignmentHandler assignmentHandler : values) {
					sb.append("         ").append(assignmentHandler).append(" | ")
							.append(assignmentHandler.getReasons()).append("\n");
					if (assignmentHandler.getIsInfected() || assignmentHandler.getIsContagious()) {
						sb.append("            ").append(assignmentHandler.getIsInfected() ? "[+]" : "[-]").append(" : infected\n");
						sb.append("            ").append(assignmentHandler.getIsContagious() ? "[+]" : "[-]").append(" : contagious\n");
						sb.append("            nonContagious references: ").append(assignmentHandler.getNonContagiousReferences()).append("\n");
						sb.append("            contagious references: ").append(assignmentHandler.getContagiousReferences()).append("\n");
						sb.append("            infected references: ").append(assignmentHandler.getInfectedReferences()).append("\n");
					}
				}
			}

			sb.append("  in dot format:\n  digraph {\n    rankdir=LR;\n");

			final ArrayList<Module> modules = new ArrayList<Module>(moduleAndAssignments.keySet());
			Collections.sort(modules, (o1, o2) -> o1.getName().compareTo(o2.getName()));
			
			for (final Module module : modules) {
				final String moduleName = module.getName();
				sb.append("    subgraph cluster_").append(moduleName).append(" {\n");
				sb.append("    label=\" ").append(module.getIdentifier().getDisplayName()).append("\";\n");

				final List<AssignmentHandler> values = moduleAndAssignments.get(module);

				for (final AssignmentHandler assignmentHandler : values) {
					for(final String reference: assignmentHandler.getInfectedReferences()) {
						sb.append("      ").append(assignmentHandler.getAssignment().getIdentifier().getDisplayName())
							.append("->").append(reference).append(";\n");
					}
					for(final String reference: assignmentHandler.getContagiousReferences()) {
						sb.append("      ").append(assignmentHandler.getAssignment().getIdentifier().getDisplayName())
							.append("->").append(reference).append(" [color=red];\n");
					}
				}
				sb.append("    }\n");
			}
			sb.append("  }\n");
			TitanLogger.logTrace(sb.toString());
		});
	}
}
