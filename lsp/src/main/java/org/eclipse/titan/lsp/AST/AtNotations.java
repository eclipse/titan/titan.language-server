/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.ArrayList;

/**
 * @author Kristof Szabados
 * */
public final class AtNotations extends ASTNode {

	private final ArrayList<AtNotation> atnotationsList;

	public AtNotations() {
		atnotationsList = new ArrayList<AtNotation>();
	}

	public void addAtNotation(final AtNotation notation) {
		if (null != notation) {
			atnotationsList.add(notation);
		}
	}

	public int getNofAtNotations() {
		return atnotationsList.size();
	}

	public AtNotation getAtNotationByIndex(final int i) {
		return atnotationsList.get(i);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (atnotationsList != null) {
			for (final AtNotation an : atnotationsList) {
				if (!an.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}
}
