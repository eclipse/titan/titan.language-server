/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.implementation;

import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.titanium.metrics.common.AltstepMetric;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricData;

public class AMBranches extends BaseAltstepMetric {
	public AMBranches() {
		super(AltstepMetric.BRANCHES);
	}

	@Override
	public Number measure(final MetricData data, final Def_Altstep altstep) {
		return altstep.nofBranches();
	}
}