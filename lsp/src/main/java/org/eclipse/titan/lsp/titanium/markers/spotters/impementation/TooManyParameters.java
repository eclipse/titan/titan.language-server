/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class TooManyParameters extends BaseModuleCodeSmellSpotter {
	private final int reportTooManyParametersSize;
	private static final String TOOMANYPARAMETERSWARNING = "More than {0} parameters: {1}";

	public TooManyParameters() {
		super(CodeSmellType.TOO_MANY_PARAMETERS);
		reportTooManyParametersSize = 7;
		
		addStartNode(FormalParameterList.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof FormalParameterList) {
			final FormalParameterList s = (FormalParameterList) node;
			if (s.size() > reportTooManyParametersSize) {
				final String msg = MessageFormat.format(TOOMANYPARAMETERSWARNING, reportTooManyParametersSize, s.size());
				problems.report(s.getLocation(), msg);
			}
		}
	}
}
