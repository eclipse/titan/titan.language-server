/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import org.eclipse.titan.lsp.common.product.ProductIdentity;

/**
 * Constants that did not fit into any other group.
 * 
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public final class GeneralConstants {
	public static final ProductIdentity ON_THE_FLY_ANALYZER_VERSION =
			ProductIdentity.getProductIdentity(ProductIdentity.TITAN_PRODUCT_NUMBER, 
					ProductIdentity.TITAN_VERSION_MAJOR, ProductIdentity.TITAN_VERSION_MINOR, 
					ProductIdentity.TITAN_VERSION_PATCHLEVEL, 0);
	public static final boolean DEBUG = true;
	public static final boolean ETSI_BUILD = false;
	
	public static final String FOLDER_CONFIG = ".TITAN_config.json";

	// General strings
	public static final String AND_SIGN = "&";
	public static final char   AND_SIGN_CHAR = '&';
	public static final String ASTERIX = "*";
	public static final String AT = "@";
	public static final char   AT_CHAR = '@';
	public static final String CALL = "call";
	public static final String COMMA = ",";
	public static final String DONE = "done";
	public static final String DOT = ".";
	public static final char   DOT_CHAR = '.';
	public static final String DOUBLE_UNDERSCORE = "__";
	public static final String EMPTY_STRING = "";
	public static final String EQUALSFUNCTION = "equals";
	public static final String FALSE = "false";
	public static final String FEW = "few";
	public static final String FIRST = "first";
	public static final String IN = "in";
	public static final String INTERNAL_ERROR = "INTERNAL ERROR";
	public static final String MANY = "many";
	public static final String MINUS_SIGN = "-";
	public static final char   MINUS_SIGN_CHAR = '-';
	public static final String NULL = "null";
	public static final String OUT = "out";
	public static final String QUESTION_MARK = "?";
	public static final String SECOND = "second";
	public static final String THIRD = "third";
	public static final String TRUE = "true";
	public static final String TEMPLATE = "template";
	public static final String TITAN = "titan";
	public static final String TITANIUM = "titanium";
	public static final String TOSTRINGFUNCITON = "toString";
	public static final String UNDERSCORE = "_";
	public static final char   UNDERSCORE_CHAR = '_';
	public static final String UNKNOWN = "unknown";
	public static final String VALUE = "value";
	public static final String VALUEORTEMPLATE = "value or template";

	// for the Combo settings of the syntactic and semantic check options.
	public static final String IGNORE = "ignore";
	public static final String WARNING = "warning";
	public static final String ERROR = "error";

	public static final String COMPILER_ERRORMARKER =  ERROR;
	public static final String COMPILER_WARNINGMARKER = WARNING;
	public static final String COMPILER_INFOMARKER = "info";
	public static final String ONTHEFLY_SYNTACTIC_MARKER = "syntax";
	public static final String ONTHEFLY_SEMANTIC_MARKER = "semantic";
	public static final String ONTHEFLY_TASK_MARKER = ".ontheflyTaskMarker";
	// Places syntactic markers in semantic time...
	public static final String ONTHEFLY_MIXED_MARKER = ".ontheflyMixedMarker"; 

	// naming rules
	public static final String OLD = "old";
	public static final String NEW = "new";
	public static final String UNSPECIFIED = "unspecified";

	// code splitting
	public static final String NONE = "none";
	public static final String TYPE = "type";
	public static final String NUMBER = "number";
	public static final String NUMBER_DEFAULT = "1";

	public static final String VERSION_STRING = ON_THE_FLY_ANALYZER_VERSION.toString();
	
	public static final String JAVA_BUILD_DIR = "java_bin";
	public static final String JAVA_SOURCE_DIR = "java_src";
	public static final String JAVA_TEMP_DIR = "temp";
	public static final String JAVA_USER_PROVIDED = "user_provided";
	
	/*
	 * Naming conventions
	 */
	public static final String NAMINGCONV_ALTSTEP = "as_.*";
	public static final String NAMINGCONV_LOCAL_CONST= "cl.*";
	public static final String NAMINGCONV_GLOBAL_CONST = "cg_.*";
	public static final String NAMINGCONV_COMPONENT_CONST = "c_.*";
	public static final String NAMINGCONV_EXTFUNCTION = "ef_.*"; 
	public static final String NAMINGCONV_FUNCTION = "f_.*";
	public static final String NAMINGCONV_FORMALPAR = "pl_.*";
	public static final String NAMINGCONV_COMPONENT_VARIABLE = "v_.*";
	public static final String NAMINGCONV_LOCAL_VARIABLE = "vl_.*";
	public static final String NAMINGCONV_COMPONENT_TIMER = "T_.*";
	public static final String NAMINGCONV_LOCAL_TIMER = "TL_.*";
	public static final String NAMINGCONV_GLOBAL_TIMER = "TG_.*";
	public static final String NAMINGCONV_LOCAL_VARTEMPLATE = "vt.*";
	public static final String NAMINGCONV_TESTCASE = "tc_.*";
	public static final String NAMINGCONV_PORT = ".*_PT";
	public static final String NAMINGCONV_VARIABLE = "v_.*";

	/** private constructor to disable instantiation */
	private GeneralConstants() {
	}
}
