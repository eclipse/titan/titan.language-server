/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the exception types of a signature.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class SignatureExceptions extends ASTNode
	implements IIncrementallyUpdatable, ILocateableNode, ICollection<Type> {
	private static final String FULLNAMEPART = ".<type";
	private static final String EMBEDDED_ERROR = "on the exception list of a signature";
	private static final String DUPLICATE_TYPE = "Duplicate type in exception list";
	private static final String DUPLICATE_TYPE_ORIG = "Type `{0}'' is already given here";

	private final List<Type> exceptionTypes;
	private final Map<String, Type> exceptionMap;

	private Location location = Location.getNullLocation();

	private CompilationTimeStamp lastTimeChecked = null;

	public SignatureExceptions(final List<Type> exceptionTypes) {
		if (exceptionTypes == null) {
			this.exceptionTypes = Collections.emptyList();
			exceptionMap = Collections.emptyMap();
		} else {
			this.exceptionTypes = exceptionTypes;
			exceptionMap = new HashMap<String, Type>();
		}

		for (final Type type : this.exceptionTypes) {
			type.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0; i < exceptionTypes.size(); i++) {
			if (exceptionTypes.get(i) == child) {
				return builder.append(FULLNAMEPART).append(Integer.toString(i)).append(INamedNode.MORETHAN);
			}
		}

		return builder;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (final Type exception : exceptionTypes) {
			exception.setMyScope(scope);
		}
	}

	@Override
	public int size() {
		return exceptionTypes.size();
	}

	@Override
	public Type get(final int index) {
		return exceptionTypes.get(index);
	}

	/**
	 * Checks if an exception is in this list.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * @param type the exception to look for
	 * @return {@code true} if there is an exception with the same name, {@code false} otherwise.
	 */
	public boolean contains(final CompilationTimeStamp timestamp, final Type type) {
		if (type == null) {
			return false;
		}

		if (type.getIsErroneous(timestamp)) {
			return true;
		}

		return exceptionMap.containsKey(type.getTypename());
	}

	/**
	 * Calculates the number of exceptions that are compatible with the provided type.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * @param type the type to check against
	 * @return the number of compatible exceptions
	 */
	public int getNofCompatibleExceptions(final CompilationTimeStamp timestamp, final IType type) {
		if (type.getTypeRefdLast(timestamp).getIsErroneous(timestamp)) {
			return 1;
		}

		int result = 0;
		for (Type t : exceptionTypes) {
			if (t.isCompatible(timestamp, type, null, null, null)) {
				result++;
			}
		}

		return result;
	}

	/**
	 * Checks the type of the exception list.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * @param signature the signature type the exceptions belong to.
	 */
	public void check(final CompilationTimeStamp timestamp, final Signature_Type signature) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		exceptionMap.clear();

		for (final Type type : exceptionTypes) {
			type.setParentType(signature);
			type.check(timestamp);
			if (!type.getIsErroneous(timestamp)) {
				type.checkEmbedded(timestamp, type.getLocation(), false, EMBEDDED_ERROR);

				final String name = type.getTypename();
				if (exceptionMap.containsKey(name)) {
					type.getLocation().reportSemanticError(DUPLICATE_TYPE);
					exceptionMap.get(name).getLocation().reportSingularSemanticError(
							MessageFormat.format(DUPLICATE_TYPE_ORIG, name));
				} else {
					exceptionMap.put(name, type);
				}
			}
		}

		lastTimeChecked = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final Type exception : exceptionTypes) {
			exception.updateSyntax(reparser, isDamaged);
			reparser.updateLocation(exception.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final Type t : exceptionTypes) {
			t.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final Type t : exceptionTypes) {
			if (!t.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean add(final Type e) {
		throw new UnsupportedOperationException("Adding type is not allowed: " + e.toString());
	}

	@Override
	public Iterator<Type> iterator() {
		return exceptionTypes.iterator();
	}
}
