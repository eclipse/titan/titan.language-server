/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types.subtypes;

import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a ranged sub-type restriction as it was parsed.
 *
 * @author Adam Delic
 * */
public final class Range_ParsedSubType implements IParsedSubType {
	private final IValue min;
	private final boolean minExclusive;
	private final IValue max;
	private final boolean maxExclusive;

	public Range_ParsedSubType(final IValue min, final boolean minExclusive, final IValue max, final boolean maxExclusive) {
		this.min = min;
		this.minExclusive = minExclusive;
		this.max = max;
		this.maxExclusive = maxExclusive;
	}

	@Override
	/** {@inheritDoc} */
	public ParsedSubType_type getSubTypetype() {
		return ParsedSubType_type.RANGE_PARSEDSUBTYPE;
	}

	public IValue getMin() {
		return min;
	}

	public boolean getMinExclusive() {
		return minExclusive;
	}

	public IValue getMax() {
		return max;
	}

	public boolean getMaxExclusive() {
		return maxExclusive;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IParsedSubType.super.updateSyntax(reparser, isDamaged);
		min.updateSyntax(reparser, false);
		reparser.updateLocation(min.getLocation());
		max.updateSyntax(reparser, false);
		reparser.updateLocation(max.getLocation());
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		// FIXME: this object should always know it's own location, but
		// currently infinity is null
		return Location.interval(min.getLocation(), max.getLocation());
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		min.findReferences(referenceFinder, foundIdentifiers);
		max.findReferences(referenceFinder, foundIdentifiers);
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		if (!min.accept(v)) {
			return false;
		}
		if (!max.accept(v)) {
			return false;
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}
}
