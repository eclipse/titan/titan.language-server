/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.antlr.v4.runtime.Token;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.codeAction.DiagnosticData;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.utils.LSPUtils;
import org.eclipse.titan.lsp.core.LspMarker;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.Range;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
 * The Location class represents a location in the source code.
 * <p>
 * This class is mainly used to: locate language elements, build structures based on their textual positions.
 * This class is also used to report some kind of warning or error to a given location.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public class Location {
	/**
	 * Type adapter for GSON encoding/decoding of a Location instance.
	 */
	public static class LocationTypeAdapter extends TypeAdapter<Location> {
		@Override
		public void write(JsonWriter out, Location value) throws IOException {
			out.beginObject();

			if (value != null && value != NULL_Location.INSTANCE) {
				out.name("file");
				out.value(value.getFile().toString());
				out.name("startLine");
				out.value(value.getStartLine());
				out.name("startColumn");
				out.value(value.getStartColumn());
				out.name("endLine");
				out.value(value.getEndLine());
				out.name("endColumn");
				out.value(value.getEndColumn());
			}

			out.endObject();
		}

		@Override
		public Location read(JsonReader in) throws IOException {
			final Location location = new Location();
			String fieldname = null;
			in.beginObject();

			while(in.hasNext()) {
				JsonToken token = in.peek();
				if (token.equals(JsonToken.NAME)) {
					fieldname = in.nextName();
				}
				switch (fieldname) {
				case "file":
					location.setFile(new File(in.nextString()));
					break;
				case "startLine":
					location.setStartLine(in.nextInt());
					break;
				case "startColumn":
					location.setStartColumn(in.nextInt());
					break;
				case "endLine":
					location.setEndLine(in.nextInt());
					break;
				case "endColumn":
					location.setEndColumn(in.nextInt());
					break;
				default:
					break;
				}
			}

			in.endObject();
			return location;
		}
	}

	/**
	 * File of the source code.
	 */
	private File file;

	// LSP positions
	private int startLine;
	private int startColumn;
	private int endLine;
	private int endColumn;

	private static final Location ZERO_Location = new Location(null, 0, 0, 0, 0);

	/**
	 * Copy constructor
	 * @param location original location object to copy
	 */
	public Location(final Location location) {
		this.file = location.getFile();
		this.startLine = location.getStartLine();
		this.endLine = location.getEndLine();
		this.startColumn = location.getStartColumn();
		this.endColumn = location.getEndColumn();
	}

	public Location(final File file, final int startline, final int endline) {
		this( file, startline, -1, endline, -1 );
	}

	public Location(final File file) {
		this( file, -1, -1 );
	}

	public Location(final File file, final int startLine, final int startColumn, final int endLine, final int endColumn) {
		this.file = file;
		this.startLine = startLine;
		this.startColumn = startColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
	}

	public Location(final File file, final Position startPosition, final Position endPosition) {
		this(file, startPosition.getLine(), startPosition.getCharacter(), endPosition.getLine(), endPosition.getCharacter());
	}

	public Location(final File file, final Range range) {
		this(file, range.getStart().getLine(), range.getStart().getCharacter(), range.getEnd().getLine(), range.getEnd().getCharacter());
	}

	public Location(final File file, final org.eclipse.lsp4j.Range range) {
		this(file, range.getStart().getLine(), range.getStart().getCharacter(), range.getEnd().getLine(), range.getEnd().getCharacter());
	}

	public Location(final File aFile, final Token aStartToken, final Token aEndToken) {
		this(aFile, aStartToken.getLine(), aStartToken.getCharPositionInLine(), aEndToken.getLine(), aEndToken.getCharPositionInLine());
	}

	/**
	 * Constructor.
	 * Default implementation for NULL_Location
	 */
	protected Location() {
		this( ( File )null );
	}

	public File getFile() {
		return file;
	}

	public void setFile(final File file) {
		this.file = file;
	}

	public void setStartLine(final int line) {
		startLine = line;
	}

	public int getStartLine() {
		return startLine;
	}

	public void setEndLine(final int line) {
		endLine = line;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setStartColumn(final int column) {
		startColumn = column;
	}

	public int getStartColumn() {
		return startColumn;
	}

	public int getEndColumn() {
		return endColumn;
	}

	public void setEndColumn(final int column) {
		endColumn = column;
	}

	public Position getStartPosition() {
		return new Position(startLine, startColumn);
	}

	public void setStartPosition(final Position position) {
		startLine = position.getLine();
		startColumn = position.getCharacter();
	}

	public Position getEndPosition() {
		return new Position(endLine, endColumn);
	}

	public void setEndPosition(final Position position) {
		endLine = position.getLine();
		endColumn = position.getCharacter();
	}

	public Range getRange() {
		return new Range(
			new Position(startLine, startColumn),
			new Position(endLine, endColumn)
		);
	}

	@Override
	/** {@inheritDoc} */
	public boolean equals(final Object object) {
		if (this == object) {
			return true;
		}

		if (object instanceof Location) {
			final Location other = (Location) object;

			// this order of checking is more likely to fail fast
			return startLine == other.startLine && startColumn == other.startColumn && 
				endLine == other.endLine && endColumn == other.endColumn &&
				((file != null && file.equals(other.file)) || (file == null && other.file == null));
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public int hashCode() {
		return Objects.hash(endColumn, endLine, file, startColumn, startLine);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(file == null ? GeneralConstants.UNKNOWN : file.toString())
		.append(":").append(startLine).append(":").append(startColumn);
		return sb.toString();
	}

	/**
	 * Checks whether the given offset is inside this location.
	 * @param offset the offset
	 * @return true if the offset is inside this location
	 */
	public boolean containsPosition(final Position position) {
		return getStartPosition().beforeOrEquals(position) && getEndPosition().afterOrEquals(position);
	}

	public LspMarker reportProblem(final String reason, final DiagnosticSeverity severity) {
		return reportProblem(reason, severity, false);
	}

	public LspMarker reportProblem(final String reason, final DiagnosticSeverity severity, final boolean isSyntactic) {
		return reportProblem(reason, severity, isSyntactic, GeneralConstants.TITAN);
	}

	/**
	 * Creates a problem marker for the given location
	 * @param reason marker text
	 * @param severity severity of the problem
	 * @param isSyntactic true if the problem is syntactic (detected by the lexer/parser), false if semantic
	 * @param source problem source (typically 'titan' or 'titanium')
	 * @return
	 */
	public LspMarker reportProblem(final String reason, final DiagnosticSeverity severity, final boolean isSyntactic, final String source) {
		final Project project = Project.INSTANCE;
		final LspMarker marker = new LspMarker(
				new Diagnostic(new Range(new Position(startLine, startColumn), new Position(endLine, endColumn)),
						reason, severity, source), isSyntactic);
		return project.addMarker(file, marker);
	}

	public LspMarker reportError(final String reason) {
		return reportProblem(reason, DiagnosticSeverity.Error);
	}

	public LspMarker reportWarning(final String reason) {
		return reportProblem(reason, DiagnosticSeverity.Warning);
	}

	public void reportSemanticError(final String reason) {
		reportError(reason);
	}

	public void reportSemanticWarning(final String reason) {
		reportWarning(reason);
	}

	public void reportStandardWarning(final String reason) {
		reportWarning(reason);
	}

	/**
	 * Kept only for legacy code. All marker additions are checked for duplicates by default. 
	 * @param reason
	 */
	public void reportSingularSemanticError(final String reason) {
		reportSemanticError(reason);
	}

	public void reportSyntacticError(final String reason) {
		reportProblem(reason, DiagnosticSeverity.Error, true);
	}

	public void reportSyntacticWarning(final String reason) {
		reportProblem(reason, DiagnosticSeverity.Warning, true);
	}

	public void reportConfigurableSemanticProblem(final String option, final String reason) {
		reportProblem(reason, LSPUtils.getDiagnosticSeverity(option, DiagnosticSeverity.Information));
	}

	public void reportMixedWarning(final String reason) {
		reportWarning(reason);
	}

	public void reportExternalProblem(final String reason, final DiagnosticSeverity severity) {
		reportProblem(reason, severity, false, GeneralConstants.TITANIUM);
	}

	public <E extends Enum<E>> void reportSemanticError(final String reason, List<E> actionTypes, CodeActionProviderType providerType) {
		reportSemanticError(reason, actionTypes, providerType, null);
	}

	public <E extends Enum<E>> void reportSemanticError(final String reason, final List<E> actionTypes, final CodeActionProviderType providerType, final String data) {
		reportSemanticProblem(DiagnosticSeverity.Error, reason, actionTypes, providerType, data);
	}

	public <E extends Enum<E>> void reportSemanticWarning(final String reason, final List<E> actionTypes, final CodeActionProviderType providerType, final String data) {
		reportSemanticProblem(DiagnosticSeverity.Warning, reason, actionTypes, providerType, data);
	}

	public <E extends Enum<E>> void reportSemanticProblem(
		final DiagnosticSeverity severity, final String reason, final List<E> actionTypes, final CodeActionProviderType providerType, final String data) {

		final Project project = Project.INSTANCE;
		final Diagnostic diagnostic = new Diagnostic(new Range(new Position(startLine, startColumn), new Position(endLine, endColumn)),
				reason, severity, GeneralConstants.TITAN);
		final DiagnosticData diagnosticData = new DiagnosticData(providerType, actionTypes, data);
		diagnostic.setData(diagnosticData);
		project.addMarker(file, new LspMarker(diagnostic));
	}

	public org.eclipse.lsp4j.Location getLspLocation() {
		final org.eclipse.lsp4j.Range range = new Range(getStartPosition(), getEndPosition());
		return new org.eclipse.lsp4j.Location(getFile().toURI().toString(), range);
	}

	public static Comparator<Location> getComparator() {
		return (o1, o2) -> {
			if (o1.getStartLine() < o2.getStartLine()) return -1;
			if (o2.getStartLine() < o1.getStartLine()) return 1;
			
			if (o1.getStartColumn() < o2.getStartColumn()) return -1;
			if (o2.getStartColumn() < o1.getStartColumn()) return 1;
			
			return 0;
		};
	}

	public static final Location getNullLocation() {
		return NULL_Location.getInstance();
	}

	/**
	 * Returns a special location of {@code (null,0,0,0,0)}. Used for internally generated AST elements.
	 * @return a special location of {@code (null,0,0,0,0)}
	 */
	public static final Location getZeroLocation() {
		return ZERO_Location;
	}

	public static final Location interval(final Location startLoc, final Location endLoc) {
		return new Location(
			startLoc.getFile(),
			startLoc.getStartLine(), startLoc.getStartColumn(),
			endLoc.getEndLine(), endLoc.getEndLine());
	}

	/**
	 * Returns whether the location equals to {@code null} or to {@link NULL_Location}
	 * @param location the location to check
	 * @return whether the location is {@code null} or equals to {@link NULL_Location}
	 */
	public static final boolean isNullLocation(final Location location) {
		return location == null || getNullLocation().equals(location);
	}

	/**
	 * Adds an offset to the specified base location
	 * @param baseLocation
	 * @param offset
	 * @return a new {@code Location} object that have been shifted
	 */
	public static final Location shift(final Location baseLocation, final Location offset) {
		if (isNullLocation(baseLocation)) {
			return getNullLocation();
		}
		if (isNullLocation(offset)) {
			return baseLocation;
		}
		if (!baseLocation.getFile().equals(offset.getFile())) {
			TitanLogger.logFatal(MessageFormat.format("Shifting location using different files: `{0}'' `{1}''",
					baseLocation, offset));
			return baseLocation;
		}
		return new Location(baseLocation.getFile(),
				baseLocation.startLine + offset.startLine,
				baseLocation.startColumn + offset.startColumn,
				baseLocation.endLine + offset.startLine,
				baseLocation.endColumn + offset.startColumn);
	}
}
