/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the actual parameters from the parser in "raw form".
 *
 * Contains both positional and named parameters. There is not enough
 * information during parsing to construct a "full" ActualParameters object
 * (information about formal parameters is needed). This object holds the
 * available information until the ActualParameters object can be constructed
 * during semantic analysis.
 *
 * @author Kristof Szabados
 */
public final class ParsedActualParameters extends ASTNode implements ILocateableNode, IIncrementallyUpdatable {
	/** the unnamed parameters. */
	private TemplateInstances unnamedPart;

	/** the named parameters. */
	private NamedParameters namedPart;

	/** the formal parameter list that this was checked against */
	public FormalParameterList formalParList = null;

	private Location location = Location.getNullLocation();

	public ParsedActualParameters() {
		super();
	}

	public void setFormalParList(final FormalParameterList formalParList) {
		this.formalParList = formalParList;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (unnamedPart != null) {
			unnamedPart.setMyScope(scope);
		}
		if (namedPart != null) {
			namedPart.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
		if (unnamedPart != null) {
			unnamedPart.setLocation(location);
		}
		if (namedPart != null) {
			namedPart.setLocation(location);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	public void addUnnamedParameter(final TemplateInstance parameter) {
		if (unnamedPart == null) {
			unnamedPart = new TemplateInstances();
			unnamedPart.setFullNameParent(this);
		}

		unnamedPart.add(parameter);
	}

	public TemplateInstances getInstances() {
		if (unnamedPart == null) {
			return new TemplateInstances();
		}

		return unnamedPart;
	}

	public void addNamedParameter(final NamedParameter parameter) {
		if (namedPart == null) {
			namedPart = new NamedParameters();
			namedPart.setFullNameParent(this);
		}

		namedPart.add(parameter);
	}

	public NamedParameters getNamedParameters() {
		if (namedPart == null) {
			return new NamedParameters();
		}

		return namedPart;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		// The parts have the same location object, so it must not be updated here
		if (unnamedPart != null) {
			unnamedPart.updateSyntax(reparser, false);
		}

		if (namedPart != null) {
			namedPart.updateSyntax(reparser, false);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (unnamedPart != null) {
			unnamedPart.findReferences(referenceFinder, foundIdentifiers);
		}
		if (namedPart != null) {
			if (formalParList != null) {
				for (final NamedParameter np : namedPart) {
					final Identifier parName = np.getName();
					final FormalParameter fp = formalParList.get(parName);
					if (fp == referenceFinder.assignment) {
						foundIdentifiers.add(new Hit(parName));
					}
				}
			}
			namedPart.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (unnamedPart != null && !unnamedPart.accept(v)) {
			return false;
		}
		if (namedPart != null && !namedPart.accept(v)) {
			return false;
		}
		return true;
	}

	public String createStringRepresentation() {
		final StringBuilder sb = new StringBuilder();
		if (unnamedPart != null) {
			sb.append(unnamedPart.createStringRepresentation());
		}

		if (namedPart != null) {
			if (!namedPart.isEmpty()) {
				sb.append(", ");
			}
			sb.append(namedPart.createStringRepresentation());
		}
		return sb.toString();
	}
}
