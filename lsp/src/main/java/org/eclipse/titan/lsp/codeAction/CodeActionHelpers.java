/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.io.BufferedReader;
import java.io.StringReader;

import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Location.LocationTypeAdapter;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.utils.LSPUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Static helper functions for implementing code actions.
 * 
 * @author Miklos Magyari
 *
 */
public class CodeActionHelpers {
	private String indent;
	private String extraIndent;
	
	public CodeActionHelpers() {
		setExtraIndent();
	}
	/**
	 * Gets the indentation of the given location as string. The indentation depends on whether 
	 * tabs or spaces are used for indentation in the editor and the tab size.<br><br>
	 * This string can be used to insert code action code snippets with correct indentation.<br><br>
	 * For example the following indentation:<br><br>
	 * --INDENT--function myFunction(integer param) {<br>
	 * --INDENT-- --EXTRAINDENT--var integer myVar := 0;<br>
	 * --INDENT--}<br><br>
	 * Produces this snippet:<br><br>
	 * <pre>
	 *     function myFunction(integer param) {
	 *         var integer myVar := 0;
	 *     }
	 * </pre>
	 * 
	 * @param location
	 * @return
	 */
	public final void setIndent(final Location location) {
		if (location instanceof NULL_Location) {
			return;
		}
		final boolean insertSpaces = Configuration.INSTANCE.getBoolean(Configuration.INSERTSPACES, false);
		if (insertSpaces) {
			indent = " ".repeat(location.getStartColumn());
		} else {
			final int tabSize = Configuration.INSTANCE.getInt(Configuration.TABSIZE, 4);
			final int column = location.getStartColumn();
			final int tabs = column / tabSize;
			indent = "\t".repeat(tabs) + " ".repeat(column - tabs);
		}
	}
	
	/**
	 * Gets the indentation string that is inserted into the code if the TAB key is pressed in the editor.
	 * The indentation depends on whether tabs or spaces are used for indentation in the editor and
	 * the tab size.<br>
	 * This string can be used to indent code lines inside the code action code snippets.<br><br>
	 * 
	 * See <code>getIndent()</code> for an example.
	 *  
	 * @return
	 */
	private void setExtraIndent() {
		final boolean insertSpaces = Configuration.INSTANCE.getBoolean(Configuration.INSERTSPACES, false);
		if (insertSpaces) {
			final int tabSize = Configuration.INSTANCE.getInt(Configuration.TABSIZE, 4);
			extraIndent = " ".repeat(tabSize);
		} else {
			extraIndent = "\t";
		}
	}
	
	public final String getIndent() {
		return indent;
	}
	
	public void addIndentedText(final StringBuilder sb, final String text) {
		sb.append(indent).append(text);
	}
	
	public void addExtraIndentedText(final StringBuilder sb, final String text) {
		addExtraIndentedText(sb, text, 1);
	}
	
	public void addExtraIndentedText(final StringBuilder sb, final String text, int extraIndentCount) {
		sb.append(extraIndent.repeat(extraIndentCount)).append(text);
	}
	
	public final String indentCode(final Location location, final String code) {
		final CodeActionHelpers helper = new CodeActionHelpers();
		helper.setIndent(location);
		final StringBuilder indentedCode = new StringBuilder();
		final BufferedReader reader = new BufferedReader(new StringReader(code));
		reader.lines().forEach(str ->
			indentedCode.append(helper.getIndent()).append(str).append(LSPUtils.getLineEnding(location))
		);
		final String indented = indentedCode.toString();
		return indented.substring(0, indented.length() - 1);
	}
	
	public static void registerTypeAdapters(final GsonBuilder builder) {
		final LocationTypeAdapter adapter = new LocationTypeAdapter();
		builder.registerTypeAdapter(Location.class, adapter);
		builder.registerTypeAdapter(NULL_Location.class, adapter);
	}
	
	public static <T> String getJSON(final T data) {
		GsonBuilder builder = new GsonBuilder();
		CodeActionHelpers.registerTypeAdapters(builder); 
		Gson gson = builder.create();
		return gson.toJson(data);
	}
}
