/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Goto_statement;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class Goto extends BaseModuleCodeSmellSpotter {
	private static final String ERROR_MESSAGE = "Usage of goto and label statements is not recommended "
			+ "as they usually break the structure of the code";

	public Goto() {
		super(CodeSmellType.GOTO);
		addStartNode(Goto_statement.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof Goto_statement) {
			final Goto_statement s = (Goto_statement) node;
			problems.report(s.getLocation(), ERROR_MESSAGE);
		}
	}
}
