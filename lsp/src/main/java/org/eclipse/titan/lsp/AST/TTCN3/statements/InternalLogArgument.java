/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public abstract class InternalLogArgument implements ICancelBuild {
	public enum ArgumentType {
		TemplateInstance, Value, Match, Macro, Reference, String
	}

	private final ArgumentType argumentType;

	protected InternalLogArgument(final ArgumentType argumentType) {
		this.argumentType = argumentType;
	}

	public final ArgumentType getArgumentType() {
		return argumentType;
	}

	/**
	 * Checks whether this log argument is defining itself in a recursive
	 * way. This can happen for example if a constant is using itself to
	 * determine its initial value.
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 * @param referenceChain
	 *                the ReferenceChain used to detect circular references.
	 * */
	public abstract void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain);
}