/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import org.eclipse.lsp4j.generator.JsonRpcData;

/**
 * This class represents the configuration data for a workspace folder
 * specified in the <code>.TITAN_config.json</code> file.
 * 
 * @author Miklos Magyari
 */
@JsonRpcData
public class FolderConfigParams {
	public class CompilerFlags {
		private boolean forceXER;
		private boolean disableBER;
		private boolean defaultOmit;
		
		CompilerFlags(boolean forceXER, boolean disableBER, boolean defaultOmit) {
			this.forceXER = forceXER;
			this.disableBER = disableBER;
			this.defaultOmit = defaultOmit;
		}

		public boolean isForceXER() {
			return forceXER;
		}

		public boolean isDisableBER() {
			return disableBER;
		}

		public boolean isDefaultOmit() {
			return defaultOmit;
		}
	}
	
	public static class NamingConventions {
		public String altstep;
		public String componentConstant;
		public String componentTimer;
		public String componentVariable;
		public String externalFunction;
		public String formalParameter;
		public String function;
		public String globalConstant;
		public String globalTimer;
		public String localConstant;
		public String localTimer;
		public String localVariable;
		public String localVarTemplate;
		public String port;
		public String testcase;
		public String variable;
	}
	
	public static class CodeSmells {
		public String identifierNameTooLongOrShort;
	}
	
	public class FolderConfig {
		private String[] definedSymbols;
		private String[] undefinedSymbols;
		private CompilerFlags compilerFlags;
		private NamingConventions namingConventions;
		private CodeSmells codeSmells;
		
		public FolderConfig(String[] definedSymbols, String[] undefinedSymbols, CompilerFlags compilerFlags,
			NamingConventions namingConventions, CodeSmells codeSmells) {
			this.definedSymbols = definedSymbols;
			this.undefinedSymbols = undefinedSymbols;
			this.compilerFlags = compilerFlags;
			this.codeSmells = codeSmells;
		}

		public CompilerFlags getCompilerFlags() {
			return compilerFlags;
		}

		public String[] getUndefinedSymbols() {
			return undefinedSymbols;
		}

		public String[] getDefinedSymbols() {
			return definedSymbols;
		}
		
		public NamingConventions getNamingConventions() {
			return namingConventions;
		}

		public CodeSmells getCodeSmells() {
			return codeSmells;
		}
	}
	
	private String uri;
	private FolderConfig config;
	
	public FolderConfigParams(String uri, FolderConfig config) {
		this.uri = uri;
		this.config = config;
	}

	public FolderConfig getConfig() {
		return config;
	}

	public String getUri() {
		return uri;
	}
}
