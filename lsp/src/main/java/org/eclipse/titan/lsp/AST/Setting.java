/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public abstract class Setting extends ASTNode implements ISetting {
	/** indicates if this setting has already been found erroneous in the actual checking cycle. */
	protected boolean isErroneous;

	/** the time when this setting was check the last time. */
	protected CompilationTimeStamp lastTimeChecked;

	/**
	 * The location of the whole setting.
	 * This location encloses the setting fully, as it is used to report errors to.
	 **/
	protected Location location = NULL_Location.INSTANCE;

	@Override
	public final boolean getIsErroneous() {
		return getIsErroneous(null);
	}

	@Override
	/** {@inheritDoc} */
	public final boolean getIsErroneous(final CompilationTimeStamp timestamp) {
		return isErroneous;
	}

	@Override
	/** {@inheritDoc} */
	public final void setIsErroneous(final boolean isErroneous) {
		this.isErroneous = isErroneous;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}
}
