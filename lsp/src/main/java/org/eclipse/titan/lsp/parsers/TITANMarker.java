/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import org.eclipse.lsp4j.DiagnosticSeverity;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
@Deprecated
public class TITANMarker {
	/**
	 * High priority constant (value 2).
	 */
	public static final int PRIORITY_HIGH = 2;

	/**
	 * Normal priority constant (value 1).
	 */
	public static final int PRIORITY_NORMAL = 1;

	/**
	 * Low priority constant (value 0).
	 */
	public static final int PRIORITY_LOW = 0;

	/**
	 * Error severity constant (value 2) indicating an error state.
	 */
	public static final int SEVERITY_ERROR = 2;

	/**
	 * Warning severity constant (value 1) indicating a warning.
	 */
	public static final int SEVERITY_WARNING = 1;

	/**
	 * Info severity constant (value 0) indicating information only.
	 */
	public static final int SEVERITY_INFO = 0;

	private final String message;
	private final int line;
	private int offset = -1;
	private int endOffset = -1;
	private final int priority;
	
	private DiagnosticSeverity severity;
	// LSP positions
	private int startLine;
	private int startColumn;
	private int endLine;
	private int endColumn;

	public TITANMarker(final String message, final int startLine, final int startColumn, final int endLine, final int endColumn,
		final DiagnosticSeverity severity, final int priority) {
		this.message = message;
		this.startLine = startLine;
		this.startColumn = startColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
		this.severity = severity;
		this.priority = priority;
		
		this.line = -1;
	}

	public TITANMarker(final String message, final int line, final int offset, final int endOffset, final int severity, final int priority) {
		this.message = message;
		this.line = line;
		this.offset = offset;
		this.endOffset = endOffset;
		//this.severity = severity;
		this.priority = priority;
	}

	public String getMessage() {
		return message;
	}

	public int getLine() {
		return line;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(final int offset) {
		this.offset = offset;
	}

	public int getEndOffset() {
		return endOffset;
	}

	public void setEndOffset(final int endOffset) {
		this.endOffset = endOffset;
	}

	public DiagnosticSeverity getSeverity() {
		return severity;
	}

	public void setSeverity(final DiagnosticSeverity severity) {
		this.severity = severity;
	}

	public int getPriority() {
		return priority;
	}

	public int getStartLine() {
		return startLine;
	}

	public int getStartColumn() {
		return startColumn;
	}

	public int getEndLine() {
		return endLine;
	}

	public int getEndColumn() {
		return endColumn;
	}
}
