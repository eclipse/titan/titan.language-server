/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import static java.util.Map.entry;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionKind;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.codeAction.data.MissingReturnData;
import org.eclipse.titan.lsp.common.utils.LSPUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CodeActionForMissingReturn extends CodeActionBase {
	public enum CodeActionType_MissingReturn{
		ADDMISSINGRETURN
	}

	private static final Map<CodeActionType_MissingReturn, BiFunction<TextDocumentIdentifier, Diagnostic, CodeAction>> codeActionList = Map.ofEntries(
			entry(CodeActionType_MissingReturn.ADDMISSINGRETURN, CodeActionForMissingReturn::addMissingReturn)
		);
	
	public CodeActionForMissingReturn(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		super(doc, diagnostic);
	}
	
	private static CodeAction addMissingReturn(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final CodeAction action = new CodeAction("Add missing return statement");
		action.setKind(CodeActionKind.QuickFix);
		
		final DiagnosticData diagnosticData = (DiagnosticData)diagnostic.getData();
		final Optional<Object> actionData = diagnosticData.getCodeActionData();
		GsonBuilder builder = new GsonBuilder();
		CodeActionHelpers.registerTypeAdapters(builder);
		Gson gson = builder.create();
		final MissingReturnData returnData = gson.fromJson((String)actionData.orElse(""), MissingReturnData.class);
		final CodeActionHelpers helper = new CodeActionHelpers();
		helper.setIndent(returnData.functionLocation);
		final TextEditor editor = new TextEditor(doc);
		final String newLine = LSPUtils.getLineEnding(doc);
		final WorkspaceEdit edit = editor
			.insertText(new Position(returnData.location.getEndLine(), returnData.location.getEndColumn()),
				helper.indentCode(returnData.location, returnData.code) + newLine + helper.getIndent())
			.getEdit();

		action.setEdit(edit);

		return action;
	}
	
	@Override
	public List<Either<Command, CodeAction>> provideCodeActions() {
		return provideCodeActions(codeActionList, CodeActionType_MissingReturn.class);
	}

}
