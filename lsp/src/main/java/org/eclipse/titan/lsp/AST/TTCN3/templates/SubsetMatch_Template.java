/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a template for the subset matching mechanism.
 *
 * <p>
 * Example 1:
 * <p>
 * type set of integer SoI;
 * <p>
 * template SoI t_1 := subset ( 1,2,? );
 *
 * <p>
 * Example 2:
 * <p>
 * type set of integer SoI;
 * <p>
 * template SoI t_SoI1 := {1, 2, (6..9)};
 * <p>
 * template subset(all from t_SOI1) length(2);
 *
 * @author Kristof Szabados
 * */
public final class SubsetMatch_Template extends CompositeTemplate {

	public SubsetMatch_Template(final ListOfTemplates templates) {
		super(templates);
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.SUBSET_MATCH;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (getIsErroneous(timestamp)) {
			return Type_type.TYPE_UNDEFINED;
		}

		return Type_type.TYPE_SET_OF;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		for (final TTCN3Template t : templates) {
			if (t.checkExpressionSelfReferenceTemplate(timestamp, lhs)) {
				return true;
			}
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	protected void checkTemplateSpecificLengthRestriction(final CompilationTimeStamp timestamp, final Type_type typeType) {
		if (Type_type.TYPE_SET_OF.equals(typeType)) {
			final boolean hasAnyOrNone = templateContainsAnyornone();
			lengthRestriction.checkNofElements(timestamp, getNofTemplatesNotAnyornone(timestamp), hasAnyOrNone, true, hasAnyOrNone, this);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected String getNameForStringRep() {
		return SUBSET;
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		return true;
	}
}
