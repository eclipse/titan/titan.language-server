/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.util.List;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawASTStruct;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawASTStruct.rawAST_coding_taglist;

/**
 * Utility class for generating the value and template classes for record and
 * set types.
 *
 * The code generated for record and set types only differs in matching and
 * encoding.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public final class RecordSetCodeGenerator {

	/**
	 * Data structure to store sequence field variable and type names.
	 * Used for java code generation.
	 */
	public static class FieldInfo {

		/** Java type name of the field */
		private final String mJavaTypeName;

		/** Java template type name of the field */
		private final String mJavaTemplateTypeName;

		/** Field variable name in TTCN-3 and java */
		private final String mVarName;

		/** The user readable name of the field, typically used in error messages */
		private final String mDisplayName;

		/** {@code true} if the field is optional */
		private final boolean isOptional;

		/** {@code true} if the field is recordof/setof */
		private final boolean ofType;

		/** Field variable name in java getter/setter function names and parameters */
		private final String mJavaVarName;

		/** Java AST type name (for debug purposes) */
		private final String mTTCN3TypeName;

		private final String mTypeDescriptorName;

		public boolean hasRaw;
		public RawASTStruct raw;

		/**
		 * If set, encodes unbound fields of records and sets as null and inserts a
		 * meta info field into the JSON object specifying that the field is unbound.
		 * The decoder sets the field to unbound if the meta info field is present and
		 * the field's value in the JSON code is either null or a valid value for that
		 * field.
		 * Example: { "field1" : null, "metainfo field1" : "unbound" }
		 *
		 * Also usable on record of/set of/array types to indicate that an element is
		 * unbound. Unbound elements are encoded as a JSON object containing one
		 * metainfo member. The decoder sets the element to unbound if the object
		 * with the meta information is found.
		 * Example: [ value1, value2, { "metainfo []" : "unbound" }, value3 ]
		 */
		public final boolean jsonMetainfoUnbound;

		/**
		 * Decoding only.
		 * Fields that don't appear in the JSON code will decode this value instead.
		 */
		public final String jsonDefaultValue;

		/** chosen fields for JSON encoding */
		public final List<rawAST_coding_taglist> jsonChosen;

		/**
		 * An alias for the name of the field (in a record, set or union).
		 * Encoding: this alias will appear instead of the name of the field
		 * Decoding: the decoder will look for this alias instead of the field's real name
		 */
		public final String jsonAlias;

		/**
		 * Encoding only.
		 * true  : use the null literal to encode omitted fields in records or sets
		 *         example: { "field1" : value1, "field2" : null, "field3" : value3 }
		 * false : skip both the field name and the value if a field is omitted
		 *         example: { "field1" : value1, "field3" : value3 }
		 * The decoder will always accept both variants.
		 */
		public final boolean jsonOmitAsNull;

		/**
		 * @param fieldType
		 *                the string representing the type of this field
		 *                in the generated code.
		 * @param fieldTemplateType
		 *                the string representing the template type of
		 *                this field in the generated code.
		 * @param fieldName
		 *                the string representing the name of this field
		 *                in the generated code.
		 * @param displayName
		 *                The user readable name of the field, typically
		 *                used in error messages
		 * @param isOptional
		 *                {@code true} if the field is optional.
		 * @param ofType
		 *                {@code true} if the field is recordof/setof
		 * @param debugName
		 *                additional text printed out in a comment after
		 *                the generated local variables.
		 * @param typeDescriptorName
		 *                the name of the type descriptor.
		 * @param jsonMetainfoUnbound
		 *                If set, encodes unbound fields of records and sets as null and inserts a
		 *                meta info field into the JSON object specifying that the field is unbound.
		 * @param jsonDefaultValue
		 *                Fields that don't appear in the JSON code will decode this value instead
		 * @param jsonChosen
		 *                chosen fields for JSON encoding
		 * @param jsonAlias
		 *                An alias for the name of the field
		 * @param jsonOmitAsNull
		 *                {@code true} to use the null literal to encode omitted fields in records or sets
		 * */
		public FieldInfo( final String fieldType, final String fieldTemplateType, final String fieldName,
						  final String displayName, final boolean isOptional, final boolean ofType,
						  final String debugName, final String typeDescriptorName,
						  final boolean jsonMetainfoUnbound, final String jsonDefaultValue,
						  final List<rawAST_coding_taglist> jsonChosen, final String jsonAlias, final boolean jsonOmitAsNull) {
			mJavaTypeName = fieldType;
			mJavaTemplateTypeName = fieldTemplateType;
			mVarName = fieldName;
			mDisplayName = displayName;
			mJavaVarName  = FieldSubReference.getJavaGetterName( mVarName );
			this.isOptional = isOptional;
			this.ofType = ofType;
			mTTCN3TypeName = debugName;
			mTypeDescriptorName = typeDescriptorName;
			this.jsonMetainfoUnbound = jsonMetainfoUnbound;
			this.jsonDefaultValue = jsonDefaultValue;
			this.jsonChosen = jsonChosen;
			this.jsonAlias = jsonAlias;
			this.jsonOmitAsNull = jsonOmitAsNull;
		}
	}

	private static class raw_option_struct {
		public boolean lengthto;
		public int lengthof;
		public List<Integer> lengthofField;
		public boolean pointerto;
		public int pointerof;
		public boolean ptrbase;
		public int extbitgroup;
		public int tag_type;
		public boolean delayedDecode;
		public List<Integer> dependentFields;
	}

	/*
	 * Should the union have more than 200 crosstag attributes, we will generate helper functions.
	 * Each of which will handle 200 crosstags on its own.
	 * This happened in Diamater (1.667 crosstag attributes on one type)
	 **/
	private static final int maxCrosstagLength = 200;

	private RecordSetCodeGenerator() {
		// private to disable instantiation
	}
}
