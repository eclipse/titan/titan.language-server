/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ASN1.ASN1Object;
import org.eclipse.titan.lsp.AST.ASN1.ObjectSetElement_Visitor;

/**
 * ObjectSetElement Visitor, code generator.
 *
 * @author Kristof Szabados
 */
public class ObjectSetElementVisitor_codeGen extends ObjectSetElement_Visitor {
//	private final JavaGenData aData;

//	public ObjectSetElementVisitor_codeGen(final ObjectSet parent, final JavaGenData aData) {
//		super(parent.getLocation());
//		this.aData = aData;
//	}

	public ObjectSetElementVisitor_codeGen(Location location) {
		super(location);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void visitObject(final ASN1Object p) {
//		p.generateCode(aData);
	}

	@Override
	public void visitObjectSetReferenced(final Referenced_ObjectSet p) {
//		p.generateCode(aData);
	}

	@Override
	protected boolean memberAccept(final ASTVisitor v) {
		return true;
	}

}
