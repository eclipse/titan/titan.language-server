/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class NamingConventionHelper {
	private static final String DISABLED_PATTERN = "off";
	
	private static final String INCORRECTWORKSPACELEVEL = "At least one of the naming convention checking regular expressions"
			+ " set on workspace level is incorrect, please correct it.";
	private static final String NAMINGCONVENTION = "The {0} with name {1} breaks the naming convention  `{2}''";
	private static final String MODULENAMECLASH = "The name {1} of the {0} contains the module name {2} it is located in";
	private static final String VISIBILITYINNAMECLASH = "The name {1} of the {0} contains visibility attributes";
	private static final Pattern VISIBILITY_PATTERN = Pattern.compile(".*(?:public|private|friend).*");
	
	public enum NamingConventionElement {
		Altstep,
		Asn1Module,
		ComponentConstant,
		ComponentTimer,
		ComponentVariable,
		ExternalConstant,
		ExternalFunction,
		FormalParameter,
		Function,
		GlobalConstant,
		GlobalTemplate,
		GlobalTimer,
		Group,
		LocalConstant,
		LocalTemplate,
		LocalTimer,
		LocalVariable,
		LocalVarTemplate,
		ModuleParameter,
		Port,
		Testcase,
		Ttcn3Module,
		Type,
		Variable
	}

	// The actual severity to be used for reporting naming convention related problems.
	private static String namingSeverity = GeneralConstants.WARNING;
	private static String containsModuleNameSeverity;
	private static String containsVisibilitySeverity;

	// true if naming convention related problems should not be reported.
	private static boolean ignoreNamingProblems;

	static {
		ignoreNamingProblems = !Configuration.INSTANCE.getBoolean(Configuration.REPORT_NAMING_CONVENTION_PROBLEMS, false);
		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.REPORT_NAMING_CONVENTION_PROBLEMS, pref ->
			ignoreNamingProblems = Configuration.INSTANCE.getBoolean(Configuration.REPORT_NAMING_CONVENTION_PROBLEMS, false) 
		);

		namingSeverity = Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, GeneralConstants.WARNING);
		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.NAMING_CONVENTION_SEVERITY, pref ->
			namingSeverity = Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, GeneralConstants.WARNING) 
		);

		// TODO: are other, specific preferences needed for this two?  
		containsModuleNameSeverity = Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, namingSeverity);
		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.NAMING_CONVENTION_SEVERITY, pref ->
			containsModuleNameSeverity = Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, namingSeverity) 
		);

		containsVisibilitySeverity = Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, namingSeverity);
		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.NAMING_CONVENTION_SEVERITY, pref ->
			containsVisibilitySeverity = Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, namingSeverity) 
		);
	}

	private NamingConventionHelper() {
	}
	
	private static String getPatternText(final NamingConventionElement element) {
		switch(element) {
		case Altstep:
			return TitanLanguageServer.getFolderConfiguration().getNamingAltstep();
		case ComponentConstant:
			return TitanLanguageServer.getFolderConfiguration().getNamingComponentConstant();
		case ComponentTimer:
			return TitanLanguageServer.getFolderConfiguration().getNamingComponentTimer();
		case ComponentVariable:
			return TitanLanguageServer.getFolderConfiguration().getNamingComponentVariable();
		case ExternalFunction:
			return TitanLanguageServer.getFolderConfiguration().getNamingExternalFunction();
		case FormalParameter:
			return TitanLanguageServer.getFolderConfiguration().getNamingFormalParameter();
		case Function:
			return TitanLanguageServer.getFolderConfiguration().getNamingFunction();
		case GlobalConstant:
			return TitanLanguageServer.getFolderConfiguration().getNamingGlobalConstant();
		case GlobalTimer:
			return TitanLanguageServer.getFolderConfiguration().getNamingGlobalTimer();
		case LocalConstant:
			return TitanLanguageServer.getFolderConfiguration().getNamingLocalConstant();
		case LocalTimer:
			return TitanLanguageServer.getFolderConfiguration().getNamingLocalTimer();
		case LocalVariable:
			return TitanLanguageServer.getFolderConfiguration().getNamingLocalVariable();
		case LocalVarTemplate:
			return TitanLanguageServer.getFolderConfiguration().getNamingLocalVarTemplate();
		case Port:
			return TitanLanguageServer.getFolderConfiguration().getNamingPort();
		case Testcase:
			return TitanLanguageServer.getFolderConfiguration().getNamingTestcase();
		case Variable:
			return TitanLanguageServer.getFolderConfiguration().getNamingVariable();
		default:
			break;
		}
		return null;
	}

	/**
	 * Checks whether an identifier is breaking a naming convention rule.
	 *
	 * @param element the element to be checked
	 * @param identifier the identifier check.
	 * @param checkedItem the item being checked, used in error messages.
	 */
	public static void checkConvention(NamingConventionElement element, final Identifier identifier, final Assignment checkedItem) {
		if (ignoreNamingProblems) {
			return;
		}

		final String patternText = getPatternText(element);
		if (patternText == null || patternText.equals(DISABLED_PATTERN)) {
			return;
		}

		if (internalCheckConvention(identifier, patternText)) {
			identifier.getLocation().reportConfigurableSemanticProblem(namingSeverity,
				MessageFormat.format(NAMINGCONVENTION, checkedItem.getDescription(), 
					identifier.getDisplayName(), patternText));
		}
	}
	
	/**
	 * Checks whether an identifier is breaking a naming convention rule.
	 *
	 * @param element the element to be checked
	 * @param identifier the identifier check.
	 * @param description the description of the source of the identifier, used in error messages.
	 */
	public static void checkConvention(final NamingConventionElement element, final Identifier identifier, final String description) {
		if (ignoreNamingProblems) {
			return;
		}

		final String patternText = getPatternText(element);
		if (patternText == null) {
			return;
		}

		if (internalCheckConvention(identifier, patternText)) {
			identifier.getLocation().reportConfigurableSemanticProblem(namingSeverity,
					MessageFormat.format(NAMINGCONVENTION, description, identifier.getDisplayName(), patternText));
		}
	}

	/**
	 * Checks whether an identifier is breaking a naming convention rule.
	 *
	 * @param identifier the identifier check.
	 * @param patternText the text to be used as the pattern.
	 * @return {@code true} if mismatch was found, {@code false} otherwise.
	 */
	private static boolean internalCheckConvention(final Identifier identifier, final String patternText) {
		if (patternText == null) {
			return false;
		}

		final Pattern pattern;
		try {
			pattern = Pattern.compile(patternText);
		} catch (PatternSyntaxException e) {
			TitanLogger.logError(e);
			return false;
		}

		final Matcher matcher = pattern.matcher(identifier.getDisplayName());
		return !matcher.matches();
	}

	/**
	 * Checks whether an identifier contains either the moduleID or a visibility attribute.
	 *
	 * @param identifier the identifier check.
	 * @param moduleID the module identifier to check against
	 * @param description the description of the source of the identifier, used in error messages.
	 */
	public static void checkNameContents(final Identifier identifier, final Identifier moduleID, final String description) {
		if (ignoreNamingProblems) {
			return;
		}
		final String displayName = identifier.getDisplayName();
		if (displayName.contains(moduleID.getDisplayName())) {
			identifier.getLocation().reportConfigurableSemanticProblem(containsModuleNameSeverity, MessageFormat.format(
					MODULENAMECLASH, description, displayName,
					moduleID.getDisplayName()));
		}
		if (VISIBILITY_PATTERN.matcher(displayName).matches()) {
			identifier.getLocation().reportConfigurableSemanticProblem(containsVisibilitySeverity,
					MessageFormat.format(VISIBILITYINNAMECLASH, description, displayName));
		}
	}
}
