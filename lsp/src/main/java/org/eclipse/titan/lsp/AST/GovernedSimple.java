/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.compiler.BuildTimestamp;

/**
 * A governed thing that will be mapped to a Java entity.
 * (e.g. Value, Template)
 *
 * @author Kristof Szabados
 */
public abstract class GovernedSimple extends Setting implements IGovernedSimple {

	/** the time when this governed simple was built the last time. */
	protected BuildTimestamp lastTimeGenerated = null;

	/** whether the value/template needs type compatibility conversion during code generation. */
	protected boolean needs_conversion = false;

	@Override
	/** {@inheritDoc} */
	public BuildTimestamp getLastTimeBuilt() {
		return lastTimeGenerated;
	}

	@Override
	/** {@inheritDoc} */
	public boolean get_needs_conversion() {
		return needs_conversion;
	}

	@Override
	/** {@inheritDoc} */
	public void set_needs_conversion() {
		needs_conversion = true;
	}

	@Override
	public boolean isTopLevel() {
//	 FIXLSP	//TODO find a more semantic way to do this.
//		final String name = getGenNameOwn();
//		for (int i = 0; i < name.length(); i++) {
//			final char c = name.charAt(i);
//			if ((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '_') {
//				return false;
//			}
//		}

		return true;
	}
}
