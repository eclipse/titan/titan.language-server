/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.declarationsearch;

import org.eclipse.titan.lsp.AST.ASTLocationChainVisitor;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IReferencingElement;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ASN1.ASN1Assignment;
import org.eclipse.titan.lsp.core.Position;

/**
 * This class is similar to {@link ASTLocationChainVisitor} except it aborts as
 * soon as an identifier is found.
 * 
 * @author Szabolcs Beres
 * @author Miklos Magyari
 */
//FIXME why identifierFinderVisitor if it searches for references ?
public class IdentifierFinderVisitor extends ASTVisitor {
	private IReferencingElement reference = null;
	private ISubReference subReference = null;

	private Position position;
	
	public IdentifierFinderVisitor(final Position position) {
		this.position = position;
	}
	
	public IdentifierFinderVisitor(final org.eclipse.lsp4j.Position position) {
		this.position = new Position(position);
	}

	@Override
	public int visit(final IVisitableNode node) {
		if (node instanceof ASN1Assignment) {
			final ASN1Assignment assignment = (ASN1Assignment) node;
			if(assignment.getAssPard() != null) {
				return V_SKIP;
			}
		}

		if (node instanceof ILocateableNode) {
			final Location loc = ((ILocateableNode) node).getLocation();
			Location docloc = NULL_Location.INSTANCE;
			if (node instanceof ICommentable) {
				final DocumentComment comment = ((ICommentable) node).getDocumentComment();
				if (comment != null) {
					docloc = comment.getCommentLocation();
				}
			}
			if (loc == null) {
				return V_ABORT;
			}
			if (!loc.containsPosition(position) && !docloc.containsPosition(position)) {
				// skip the children, the offset is not inside this node
				return V_SKIP;
			}

			if (node instanceof IReferencingElement) {
				reference = (IReferencingElement) node;
			} else if (node instanceof ISubReference) {
				subReference = (ISubReference) node;
			}

			if (node instanceof Identifier) {
				return V_ABORT;
			}

		}
		return V_CONTINUE;
	}

	/**
	 * Identifies the referenced declaration and returns it.
	 * 
	 * @return the referenced declaration or null.
	 */
	public IDeclaration getReferencedDeclaration() {
		if (reference == null) {
			// nothing found during AST visit
			return null;
		}

		if (subReference == null || !(reference instanceof Reference)) {
			// we have a reference, but nothing more specific
			return reference.getDeclaration();
		}

		return ((Reference) reference).getReferencedDeclaration(subReference);
	}

}
