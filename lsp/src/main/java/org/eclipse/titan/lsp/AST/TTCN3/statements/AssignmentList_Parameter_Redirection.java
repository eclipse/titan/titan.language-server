/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.types.SignatureFormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.types.SignatureFormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.types.Signature_Type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the parameter redirection of a getcall/getreply operation.
 * <p>
 * Provided with assignment list notation.
 *
 * @author Kristof Szabados
 * */
public final class AssignmentList_Parameter_Redirection extends Parameter_Redirection {
	private static final String FULLNAMEPART = ".parameterassignments";
	private static final String DUPLICATE_REDIRECT_ERROR = "Duplicate redirect for parameter `{0}''";
	private static final String ALREADY_GIVEN = "A variable entry for parameter `{0}'' is already given here";
	private static final String PARAMETER_DIRECTION = "Parameter `{0}'' of signature `{1}'' has `{2}'' direction";
	private static final String DECODED_MODIFIER_UNIV =
			"The encoding format parameter for the `@decoded' modifier is only available to parameter redirects of universal charstrings";
	private static final String DECODED_MODIFIER =
			"The '@decoded' modifier is only available to parameter redirects of string types.";
	private static final String PARAMETER_MISMATCH = "Signature `{0}'' does not have parameter named `{1}''";

	private final Parameter_Assignments assignments;

	// calculated field
	private Variable_Entries entries;

	/**
	 * Constructs the assignment list style parameter redirection with the parameter assignments.
	 * @param assignments the assignments to manage.
	 */
	public AssignmentList_Parameter_Redirection(final Parameter_Assignments assignments) {
		this.assignments = assignments;

		if (assignments != null) {
			assignments.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (assignments == child) {
			return builder.append(FULLNAMEPART);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (assignments != null) {
			assignments.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasDecodedModifier() {
		if (entries != null) {
			for (final Variable_Entry entry : entries) {
				if (entry.isDecoded()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void checkErroneous(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		final HashMap<String, Parameter_Assignment> parameterMap = new HashMap<String, Parameter_Assignment>();
		for (final Parameter_Assignment assignment : assignments) {
			final String name = assignment.getIdentifier().getName();
			if (parameterMap.containsKey(name)) {
				assignment.getLocation().reportSemanticError(
						MessageFormat.format(DUPLICATE_REDIRECT_ERROR, assignment.getIdentifier().getDisplayName()));
				final Location otherLocation = parameterMap.get(name).getLocation();
				otherLocation.reportSemanticWarning(MessageFormat.format(ALREADY_GIVEN, assignment.getIdentifier().getDisplayName()));
			} else {
				parameterMap.put(name, assignment);
			}

			checkVariableReference(timestamp, assignment.getReference(), null);
			final Value stringEncoding = assignment.getStringEncoding();
			if (stringEncoding != null) {
				stringEncoding.checkStringEncoding(timestamp, null);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final Signature_Type signature, final boolean isOut) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		final SignatureFormalParameterList parameterList = signature.getParameterList();
		if (parameterList.isEmpty()) {
			getLocation().reportSemanticError(MessageFormat.format(SIGNATUREWITHOUTPARAMETERS, signature.getTypename()));
			checkErroneous(timestamp);
			return;
		}

		boolean errorFlag = false;
		final HashMap<String, Parameter_Assignment> parameterMap = new HashMap<String, Parameter_Assignment>();
		for (final Parameter_Assignment assignment : assignments) {
			final String name = assignment.getIdentifier().getName();
			if (parameterMap.containsKey(name)) {
				assignment.getLocation().reportSemanticError(
						MessageFormat.format(DUPLICATE_REDIRECT_ERROR, assignment.getIdentifier().getDisplayName()));
				final Location otherLocation = parameterMap.get(name).getLocation();
				otherLocation.reportSemanticWarning(MessageFormat.format(ALREADY_GIVEN, assignment.getIdentifier().getDisplayName()));
				errorFlag = true;
			} else {
				parameterMap.put(name, assignment);
			}

			if (parameterList.contains(name)) {
				final SignatureFormalParameter parameterTemplate = parameterList.get(name);
				if (isOut) {
					if (SignatureFormalParameter.ParamaterDirection.PARAM_IN.equals(parameterTemplate.getDirection())) {
						final String message = MessageFormat.format(
								PARAMETER_DIRECTION, assignment.getIdentifier().getDisplayName(), signature.getTypename(), IN);
						assignment.getLocation().reportSemanticError(message);
						errorFlag = true;
					}
				} else {
					if (SignatureFormalParameter.ParamaterDirection.PARAM_OUT.equals(parameterTemplate.getDirection())) {
						final String message = MessageFormat.format(
								PARAMETER_DIRECTION, assignment.getIdentifier().getDisplayName(), signature.getTypename(), OUT);
						assignment.getLocation().reportSemanticError(message);
						errorFlag = true;
					}
				}

				if (assignment.isDecoded()) {
					final Value stringEncoding = assignment.getStringEncoding();
					final Type parType = parameterTemplate.getType();
					//boolean isErroneous = false;
					final IType refdLast = parType.getTypeRefdLast(timestamp);
					switch (refdLast.getTypetypeTtcn3()) {
					case TYPE_BITSTRING:
					case TYPE_HEXSTRING:
					case TYPE_OCTETSTRING:
					case TYPE_CHARSTRING:
						if (stringEncoding != null) {
							stringEncoding.getLocation().reportSemanticError(DECODED_MODIFIER_UNIV);
							errorFlag = true;
						}
						break;
					case TYPE_UCHARSTRING:
						if (stringEncoding != null) {
							stringEncoding.checkStringEncoding(timestamp, null);
						}
						break;
					default:
						assignment.getLocation().reportSemanticError(DECODED_MODIFIER);
						errorFlag = true;
						break;
					}

					final Reference variableReference = assignment.getReference();
					final IType varType = variableReference.checkVariableReference(timestamp);
					if (!errorFlag && varType != null) {
						// store the variable type in case it's decoded (since this cannot
						// be extracted from the value type with the sub-references)
						final IType declarationType = varType.getTypeRefdLast(timestamp);
						assignment.setDeclarationType(declarationType);
						varType.checkCoding(timestamp, false, variableReference.getMyScope().getModuleScope(), false, variableReference.getLocation());
					}
				} else {
					checkVariableReference(timestamp, assignment.getReference(), parameterTemplate.getType());
				}
			} else {
				assignment.getLocation().reportSemanticError(
						MessageFormat.format(PARAMETER_MISMATCH, signature.getTypename(), assignment.getIdentifier().getDisplayName()));
				errorFlag = true;
				checkVariableReference(timestamp, assignment.getReference(), null);
				final Value stringEncoding = assignment.getStringEncoding();
				if (stringEncoding != null) {
					stringEncoding.checkStringEncoding(timestamp, null);
				}
			}
		}

		if (!errorFlag) {
			// converting the AssignmentList to VariableList
			entries = new Variable_Entries();
			final int upperLimit = isOut ? parameterList.getNofOutParameters() : parameterList.getNofInParameters();
			for (int i = 0; i < upperLimit; i++) {
				final SignatureFormalParameter parameter = isOut ? parameterList.getOutParameterByIndex(i) : parameterList
						.getInParameterByIndex(i);
				final String name = parameter.getIdentifier().getName();
				if (parameterMap.containsKey(name)) {
					final Parameter_Assignment parAssignment = parameterMap.get(name);
					entries.add(new Variable_Entry(parAssignment.getReference(), parAssignment.isDecoded(), parAssignment.getStringEncoding(), parAssignment.getDeclarationType()));
				} else {
					entries.add(new Variable_Entry());
				}
			}
		}

		lastTimeChecked = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);
		assignments.updateSyntax(reparser, isDamaged);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (assignments == null) {
			return;
		}
		assignments.findReferences(referenceFinder, foundIdentifiers);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (assignments != null && !assignments.accept(v)) {
			return false;
		}
		return true;
	}
}
