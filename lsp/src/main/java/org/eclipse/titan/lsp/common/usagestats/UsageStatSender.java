/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.usagestats;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.utils.MapJoiner;

/**
 * This class can send usage stats through HTTP.
 */
public final class UsageStatSender {
	/**
	 * Global configuration flag for usage statistics sending, true if usage statistics is sent
	 */
	private static final boolean USAGE_STAT_SENDING = false;
	private static final String HOST_URI = "http://ttcn.ericsson.se/download/usage_stats/usage_stats.php";

	private UsageStatSender() {
		// Do nothing
	}

	public static void send() {
		if (USAGE_STAT_SENDING) {
			post(UsageStatCollector.getUsageData());
		}
	}

	private static void post(final Map<String, String> data) {
		final String urlParameters = new MapJoiner(GeneralConstants.AND_SIGN, "=").join(data).toString();
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder(URI.create(HOST_URI))
				.header("Content-type", "application/x-www-form-urlencoded")
				.POST(HttpRequest.BodyPublishers.ofString(urlParameters))
				.build();

		try {
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			if (response.statusCode() == 200) {
				TitanLogger.logInfo("Usage data sent");
			} else {
				final StringBuilder sb = new StringBuilder("Usage data not sent: \nStatus code: ");
				sb.append(response.statusCode()).append('\n').append(response.body());
				TitanLogger.logInfo(sb.toString());
			}
		} catch (IOException e) {
			TitanLogger.logInfo("Usage data not sent");
		} catch (InterruptedException e) {
			TitanLogger.logInfo("Usage data sending interrupted");
			Thread.currentThread().interrupt();
		}
	}
}
