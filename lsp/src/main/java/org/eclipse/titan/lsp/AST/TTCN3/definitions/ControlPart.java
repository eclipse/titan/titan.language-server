/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.util.EnumSet;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferencingElement;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.DocumentComment.CommentTag;
import org.eclipse.titan.lsp.AST.ILocatableComment;
import org.eclipse.titan.lsp.AST.TTCN3.IAppendableSyntax;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_reparser_optionalWithStatementContext;

/**
 * The ControlPart class represents the control parts of TTCN3 modules.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public final class ControlPart extends Scope
	implements ILocateableNode, IAppendableSyntax, IReferencingElement, IIncrementallyUpdatable, ILocatableComment {

	private static final String KIND = "controlpart";

	private StatementBlock statementblock;

	private WithAttributesPath withAttributesPath = null;

	private Location location = Location.getNullLocation();

	private Location commentLocation = null;

	/** the time when this control part was checked the last time. */
	private CompilationTimeStamp lastTimeChecked;

	public static String getKind() {
		return KIND;
	}

	@Override
	public Location getCommentLocation() {
		return commentLocation;
	}

	@Override
	public void setCommentLocation(final Location commentLocation) {
		this.commentLocation = commentLocation;
	}

	public ControlPart(final StatementBlock statementblock) {
		setScopeMacroName("control");
		if (statementblock == null) {
			this.statementblock = new StatementBlock();
		} else {
			this.statementblock = statementblock;
			setLocation(statementblock.getLocation());
			addSubScope(statementblock.getLocation(), statementblock);
		}
		this.statementblock.setFullNameParent(this);
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	/**
	 * Sets the scope of the control part.
	 *
	 * @param scope
	 *                the scope to be set
	 * */
	public void setMyScope(final Scope scope) {
		parentScope = scope;
		statementblock.setMyScope(this);
	}

	/**
	 * @return the scope of this control part
	 * */
	public Scope getMyScope() {
		return statementblock;
	}

	/**
	 * Sets the with attributes for this control part if it has any. Also
	 * creates the with attribute path, to store the attributes in.
	 *
	 * @param attributes
	 *                the attribute to be added.
	 * */
	public void setWithAttributes(final MultipleWithAttributes attributes) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}
		if (attributes != null) {
			withAttributesPath.setWithAttributes(attributes);
		}
	}

	/**
	 * @return the with attribute path element of this control part. If it
	 *         did not exist it will be created.
	 * */
	public WithAttributesPath getAttributePath() {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		return withAttributesPath;
	}

	/**
	 * Sets the parent path for the with attribute path element of this
	 * control part. Also, creates the with attribute path node if it did
	 * not exist before.
	 *
	 * @param parent
	 *                the parent to be set.
	 * */
	public void setAttributeParentPath(final WithAttributesPath parent) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		withAttributesPath.setAttributeParent(parent);
	}

	/**
	 * Does the semantic checking of the control part.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * */
	public void check(final CompilationTimeStamp timestamp) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		MarkerHandler.removeMarkers(this, true);
		lastTimeChecked = timestamp;
		
		statementblock.check(timestamp);

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

		checkDocumentComment();
	}

	/**
	 * Checks the properties of the control part, that can only be checked
	 * after the semantic check was completely run.
	 * */
	public void postCheck() {
		statementblock.postCheck();
	}

	@Override
	/** {@inheritDoc} */
	public ControlPart getControlPart() {
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		return getParentScope().getAssBySRef(timestamp, reference);
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		if (withAttributesPath == null || withAttributesPath.getAttributes() == null) {
			return List.of(Ttcn3Lexer.WITH);
		}
		return IAppendableSyntax.super.getPossibleExtensionStarterTokens();
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;

			if (reparser.envelopsDamage(statementblock.getLocation())) {
				statementblock.updateSyntax(reparser, true);
				enveloped = true;
				reparser.updateLocation(statementblock.getLocation());
			} else if (reparser.isDamaged(statementblock.getLocation())) {
				throw new ReParseException();
			} else {
				statementblock.updateSyntax(reparser, false);
				reparser.updateLocation(statementblock.getLocation());
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					reparser.extendDamagedRegion(withAttributesPath.getLocation());
					final int result = reparse( reparser );
					if (result == 0) {
						enveloped = true;
					} else {
						throw new ReParseException();
					}
				} else if (reparser.isDamaged(withAttributesPath.getLocation())) {
					throw new ReParseException();
				} else {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				}
			}

			if (!enveloped) {
				throw new ReParseException();
			}

			return;
		}

		statementblock.updateSyntax(reparser, false);
		reparser.updateLocation(statementblock.getLocation());

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	private int reparse( final TTCN3ReparseUpdater aReparser ) {
		return aReparser.parse(parser -> {
			final Pr_reparser_optionalWithStatementContext root = parser.pr_reparser_optionalWithStatement();
			ParserUtilities.logParseTree( root, parser );
			final MultipleWithAttributes attributes = root.attributes;

			final ParseTree rootEof = parser.pr_EndOfFile();
			ParserUtilities.logParseTree( rootEof, parser );
			if ( parser.isErrorListEmpty() ) {
				withAttributesPath.setWithAttributes(attributes);
				if (attributes != null) {
					getLocation().setEndPosition(attributes.getLocation().getEndPosition());
				}
			}
		});
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		if (statementblock == null) {
			return null;
		}
		return statementblock.getEnclosingAssignment(offset);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (statementblock != null) {
			statementblock.findReferences(referenceFinder, foundIdentifiers);
		}
		if (withAttributesPath != null) {
			withAttributesPath.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		if (statementblock != null) {
			if (!statementblock.accept(v)) {
				return false;
			}
		}
		if (withAttributesPath != null) {
			if (!withAttributesPath.accept(v)) {
				return false;
			}
		}
		if (v.leave(this) == ASTVisitor.V_ABORT) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		hoverContent = new Ttcn3HoverContent();
		hoverContent.addStyledText(KIND);

		if (hasDocumentComment()) {
			final DocumentComment dc = getDocumentComment();
			dc.addDescsContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding)
			.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	@Override
	public IDeclaration getDeclaration() {
		return IDeclaration.createInstance(this);
	}
	
	@Override
	public void checkDocumentComment() {
		if (!hasDocumentComment()) {
			return;
		}
		
		final EnumSet<CommentTag> naTags =
				EnumSet.of(CommentTag.Config, CommentTag.Exception, CommentTag.Member, CommentTag.Param, CommentTag.Priority);
		naTags.addAll(EnumSet.of(CommentTag.Purpose, CommentTag.Return, CommentTag.Requirement, CommentTag.Verdict));
			
		getDocumentComment().checkNonApplicableTags(naTags, "modules");
		super.checkDocumentComment();
	}

}
