/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.brokenpartsanalyzers;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.TitanLanguageServer.ServerMode;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.SemanticInformationVisitor;

/**
 * Helper class to check broken parts.
 *
 * @author Peter Olah
 * @author Kristof Szabados
 * @author Miklos Magyari
 */
public final class BrokenPartsChecker {
	private static final String DEBUG_MESSAGE_TIME_INTERVAL = 
			"  **It took {0} seconds so far for Titan to check the modules in parallel mode";
	private static final String DEBUG_MESSAGE_TIME_INTERVAL2 = 
			"  **It took ({0}, {1}) {2} seconds for Titan to check {3}";

	private final CompilationTimeStamp compilationCounter;
	private final BrokenPartsViaReferences selectionMethod;
	private static final Object lockObject = new Object();

	public BrokenPartsChecker(final CompilationTimeStamp compilationCounter, final BrokenPartsViaReferences selectionMethod) {
		this.compilationCounter = compilationCounter;
		this.selectionMethod = selectionMethod;
	}

	public void doChecking() {
		if (selectionMethod.getAnalyzeOnlyDefinitions()) {
			final Map<Module, List<Assignment>> moduleAndBrokenDefinitions = selectionMethod.getModuleAndBrokenDefs();
			definitionsChecker(moduleAndBrokenDefinitions);
		} else {
			generalChecker();
		}

		for (final Module module : selectionMethod.getModulesToCheck()) {
			module.postCheck();
		}

		final List<Path> filesToCheck = new ArrayList<>();
		for (Module moduleToCheck : selectionMethod.getModulesToCheck()) {
			filesToCheck.add(moduleToCheck.getLocation().getFile().toPath());
		}
		
		/** 
		 * Trigger rendering for open editors to update semantic highlighting
		 * for all the editors containing a module that has been reanalyzed
		 *
		 **/
		// FIXME : check if it is needed
	}

	private void generalChecker() {
		final List<Module> modulesToCheck = selectionMethod.getModulesToCheck();
		if (modulesToCheck.isEmpty()) {
			return;
		}

		for (final Module module : selectionMethod.getModulesToSkip()) {
			module.setSkippedFromSemanticChecking(true);
		}
		for (final Module module : modulesToCheck) {
			module.setSkippedFromSemanticChecking(false);
		}

		// process the modules one-by-one
		final long absoluteStart = System.nanoTime();
		final int availableProcessors = Runtime.getRuntime().availableProcessors();

		// When enabled do a quick parallel checking on the modules, where it is possible.
		// 2 modules can be checked in parallel if the codes to be checked do not overlap.
		// Please note, that this will not let all modules be processed in parallel,
		// modules in import loops (and all modules depending on them) have to be checked in the single threaded way.
		ExecutorService executor = Executors.newFixedThreadPool(availableProcessors);

		final AtomicInteger activeExecutorCount = new AtomicInteger(0);
		final List<Module> modulesToCheckCopy = new ArrayList<Module>(modulesToCheck);
		Collections.sort(modulesToCheckCopy, (o1, o2) ->
			o2.getAssignments().size() - o1.getAssignments().size()
		);

		final ArrayList<Module> modulesToCheckParallely = new ArrayList<Module>();
		//initial filling
		for (final Module module : modulesToCheckCopy) {
			final List<Module> importedModules = module.getImportedModules();
			boolean ok = true;
			for (final Module importedModule : importedModules) {
				if (!importedModule.getSkippedFromSemanticChecking() && (importedModule.getLastCompilationTimeStamp() == null || importedModule.getLastCompilationTimeStamp() != compilationCounter)) {
					ok = false;
					break;
				}
			}
			if (ok) {
				modulesToCheckParallely.add(module);
			}
		}

		final CountDownLatch latch = new CountDownLatch(modulesToCheck.size());

		if (modulesToCheckParallely.isEmpty()) {
			// When we can not start in parallel mode, just start somewhere
			modulesToCheckParallely.add(modulesToCheckCopy.remove(0));
		} else {
			modulesToCheckCopy.removeAll(modulesToCheckParallely);
		}

		final LinkedBlockingDeque<Module> modulesBeingChecked = new LinkedBlockingDeque<Module>(modulesToCheckParallely);

		synchronized(modulesToCheckCopy) {
			for (final Module module : modulesToCheckParallely) {
				addToExecutor(module, executor, latch, compilationCounter, absoluteStart, modulesToCheckCopy, modulesBeingChecked, activeExecutorCount);
			}
		}

		try {
			latch.await();
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		TitanLanguageServer.requestSemanticTokensRefresh();
		TitanLogger.logDebug(MessageFormat.format(DEBUG_MESSAGE_TIME_INTERVAL, (System.nanoTime() - absoluteStart) * (1e-9)));

		executor.shutdown();
		try {
			executor.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdownNow();

		for (final Module module : selectionMethod.getModulesToSkip()) {
			module.setSkippedFromSemanticChecking(false);
		}
	}

	private static void addToExecutor(final Module moduleIn, final ExecutorService executor, final CountDownLatch latch, final CompilationTimeStamp compilationCounter, final long absoluteStart,
			final List<Module> modulesToCheckCopy, final LinkedBlockingDeque<Module> modulesBeingChecked, final AtomicInteger activeExecutorCount) {
		try {
			executor.execute(() -> {
				try {
					activeExecutorCount.incrementAndGet();
					final long absoluteStart2 = System.nanoTime();
					ProjectItem item = Project.INSTANCE.getProjectItem(moduleIn.getLocation().getFile().toPath());
					if (item != null) {
						AstSemanticHighlighting.clearSemanticTokensAndModifiers(item.getPath());
						if (moduleIn instanceof TTCN3Module) {
							final TTCN3Module ttcn3module = (TTCN3Module)moduleIn;
							ttcn3module.clearExistingReferences();
						}
						moduleIn.check(compilationCounter);
						final SemanticInformationVisitor visitor = new SemanticInformationVisitor();
						moduleIn.accept(visitor);
						final List<Diagnostic> markerList = item.getMarkerList();
						if (markerList != null) {
							PublishDiagnosticsParams diagpar;
							if (item.isUntitled()) {
								final String fname = "untitled:" + item.getPath().getFileName().toString();
								diagpar = new PublishDiagnosticsParams(fname, item.getMarkerList());
							} else {
								diagpar = new PublishDiagnosticsParams(item.getPath().toUri().toString(), item.getMarkerList());
							}
							if (diagpar != null) {
								if (TitanLanguageServer.getMode() == ServerMode.LanguageServer) {
									TitanLanguageServer.getClient().publishDiagnostics(diagpar);
								}
							}
						}
					}
					
					final long now = System.nanoTime();
					TitanLogger.logDebug(MessageFormat.format(DEBUG_MESSAGE_TIME_INTERVAL2,
							absoluteStart2 - absoluteStart, now - absoluteStart, (now - absoluteStart2) * (1e-9), moduleIn.getName()));
				} catch (Exception e) { 
					TitanLogger.logError(e);
					Thread.currentThread().interrupt();
				} finally {
					modulesBeingChecked.remove(moduleIn);
					latch.countDown();
	
					final ArrayList<Module> modulesToCheckParallely = new ArrayList<Module>();
					synchronized(lockObject) {
						for (final Module module : modulesToCheckCopy) {
							final List<Module> importedModules = module.getImportedModules();
							boolean ok = true;
							for (final Module importedModule : importedModules) {
								if (modulesBeingChecked.contains(importedModule) || (!importedModule.getSkippedFromSemanticChecking() && (importedModule.getLastCompilationTimeStamp() == null || importedModule.getLastCompilationTimeStamp() != compilationCounter))) {
									ok = false;
									break;
								}
							}
							if (ok) {
								modulesToCheckParallely.add(module);
							}
						}

						int remainingExecutors = activeExecutorCount.decrementAndGet();
						if (modulesToCheckParallely.isEmpty()) {
							//did not enable the processing of more modules
							if (remainingExecutors == 0 && modulesBeingChecked.isEmpty() && !modulesToCheckCopy.isEmpty()) {
								// there are more modules to check, but none can be checked in the normal way
								// and this is the last executor running.
								// current heuristic: just select one to keep checking ... and hope this breaks the loop stopping parallelism.
								final Module module = modulesToCheckCopy.remove(0);
								modulesToCheckCopy.remove(module);
								modulesBeingChecked.add(module);
								addToExecutor(module, executor, latch, compilationCounter, absoluteStart, modulesToCheckCopy, modulesBeingChecked, activeExecutorCount);
							}
	
							return;
						}

						//Enabled the processing of more modules
						modulesToCheckCopy.removeAll(modulesToCheckParallely);
						modulesBeingChecked.addAll(modulesToCheckParallely);
						for (final Module module : modulesToCheckParallely) {
							addToExecutor(module, executor, latch, compilationCounter, absoluteStart, modulesToCheckCopy, modulesBeingChecked, activeExecutorCount);
						}
					}
				}
			});
		} catch (RejectedExecutionException e) {
			TitanLogger.logError(e);
		}
	}

	private void definitionsChecker(final Map<Module, List<Assignment>> moduleAndBrokenDefs) {
		for (final Map.Entry<Module, List<Assignment>> entry : moduleAndBrokenDefs.entrySet()) {
			final Module module = entry.getKey();

			if (module instanceof TTCN3Module) {
				((TTCN3Module) module).checkWithDefinitions(compilationCounter, entry.getValue());
			} else {
				module.check(compilationCounter);
			}
		}
	}
}

