/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import org.eclipse.lsp4j.generator.JsonRpcData;

/**
 * This class represents a Titan project descriptor (TPD) file to be activated.
 * 
 * @author Miklos Magyari
 *
 */
@JsonRpcData
public class ActivateTpdParams {
	private String tpdPath;
	
	public ActivateTpdParams(final String tpdPath) {
		this.tpdPath = tpdPath;
	}

	public String getTpdPath() {
		return tpdPath;
	}
}
