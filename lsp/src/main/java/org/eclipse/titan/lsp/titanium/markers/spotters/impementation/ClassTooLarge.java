/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

import java.text.MessageFormat;

public class ClassTooLarge extends BaseModuleCodeSmellSpotter {
	private final int definitionsCountMax;
	
	public ClassTooLarge() {
		super(CodeSmellType.CLASS_TOO_LARGE);
		definitionsCountMax = 20;
		
		addStartNode(ClassTypeBody.class);
	}

	@Override
	protected void process(IVisitableNode node, Problems problems) {
		if (node instanceof ClassTypeBody) {
			final ClassTypeBody body = (ClassTypeBody)node;
			final Class_Type parent = body.getParentClass();
			if (parent == null) {
				return;
			}
			int count = 0;
			for (final Definition def : parent.getDefinitions()) {
				if (!def.isInherited()) {
					count++;
				}
			}
			if (count > definitionsCountMax) {
				final Location classNameLocation = parent.getIdentifier().getLocation();
				if (classNameLocation instanceof NULL_Location) {
					return;
				}
				problems.report(classNameLocation, MessageFormat.format("Class is too large; more than {0} definitions: {1}",
					definitionsCountMax, count));
			}
		}
	}
}
