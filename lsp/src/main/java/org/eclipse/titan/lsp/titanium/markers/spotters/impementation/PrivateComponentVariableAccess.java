/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * This class marks the following code smell:
 * This code smell report a problem, when someone try access to
 * a module private variable from an another module.
 *
 * @author Sandor Balazs
 */
public class PrivateComponentVariableAccess extends BaseModuleCodeSmellSpotter {
	/**
	 * The error message contain the private components name and the module name too.
	 */
	private static final String ERROR_MESSAGE = "The \"{0}\" is a Private component in the \"{1}\" module. Access is not recommended from \"{2}\" module.";

	public PrivateComponentVariableAccess() {
		super(CodeSmellType.PRIVATE_COMPONENT_VARIABLE_ACCESS);
		addStartNode(Reference.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (!(node instanceof Reference)) {
			return;
		}

		final Reference reference = (Reference) node;
		if(reference.getIsErroneous(CompilationTimeStamp.getBaseTimestamp())){
			return;
		}

		final Assignment referedAssignment = reference.getRefdAssignment(CompilationTimeStamp.getBaseTimestamp(), false);
		if(referedAssignment == null) {
			return;
		}
		if(!(referedAssignment instanceof Definition)) {
			return;
		}

		final Definition definition = (Definition) referedAssignment;
		if(definition.getVisibilityModifier().equals(VisibilityModifier.Private)) {
			final Module referingModule = reference.getMyScope().getModuleScope();
			final Module referedModule = definition.getMyScope().getModuleScope();
			if(!referingModule.equals(referedModule)) {
				problems.report(reference.getLocation(), MessageFormat.format(ERROR_MESSAGE, reference.getDisplayName(),
						referedModule.getName(), referingModule.getName()));
			}
		}
	}
}