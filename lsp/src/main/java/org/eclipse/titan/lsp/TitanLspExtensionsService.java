/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.product.FolderConfiguration;
import org.eclipse.titan.lsp.common.product.IPreferenceChangedListener;
import org.eclipse.titan.lsp.common.product.TpdImporter;
import org.eclipse.titan.lsp.common.utils.FileExtensionUtils;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.lspextensions.DocumentCommentParams;
import org.eclipse.titan.lsp.lspextensions.EnvironmentParams;
import org.eclipse.titan.lsp.lspextensions.ExcludedStatesChangedParams;
import org.eclipse.titan.lsp.lspextensions.FolderConfigParams;
import org.eclipse.titan.lsp.lspextensions.GenerateDocumentCommentParams;
import org.eclipse.titan.lsp.lspextensions.MetricDataParams;
import org.eclipse.titan.lsp.lspextensions.ActivateTpdParams;
import org.eclipse.titan.lsp.lspextensions.TpdProjectFiles;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricDataProvider;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricDataProvider.MetricsResponse;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricDataProvider.MetricsStatsResponse;

public final class TitanLspExtensionsService implements LspExtensionsService {

	private final TitanLanguageServer languageServer;

	private static final IPreferenceChangedListener excludedFileListListener = new IPreferenceChangedListener() {
		@Override
		public void changed(String preferenceName) {
			// Do nothing
		}

		@Override
		public void changed(String preferenceName, Object oldValue) {
			final Project project = Project.INSTANCE;
			final List<Object> excludeList = Configuration.INSTANCE.getList(preferenceName, new ArrayList<>());
			try {
				if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 0) {
					TitanLanguageServer.BUILD_LOCK.lock();
				}
				if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 0 && excludeList.isEmpty()) {
					project.getAllProjectItems().forEach((path, item) -> item.setExcluded(false));
					project.resetProject();
				} else {
					if (oldValue != null) {
						@SuppressWarnings("unchecked")
						final List<String> oldList = (List<String>)oldValue;
						for (String s : oldList) {
							final ProjectItem projectItem = project.getProjectItem(s);
							if (projectItem == null) {
								/* Probably trying to exclude a file outside of the project */
								continue;
							}
							projectItem.setExcluded(false);
							if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 0) {
								projectItem.reset();
							}
						}
					}
					for (final Object excludedParam : excludeList) {
						String filename = (String)excludedParam;
						if (!FileExtensionUtils.isSupportedExtension(filename)) {
							continue;
						}
						final ProjectItem projectItem = project.getProjectItem(filename);
						if (projectItem == null) {
							/* Probably trying to exclude a file outside of the project */
							continue;
						}
						projectItem.setExcluded(true);
						if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 0) {
							projectItem.reset();
						}
					}
				}
				if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 0) {
					project.outdateAllItems();
				}
			} catch (Exception e) {
				TitanLogger.logError(e);
			} finally {
				TitanLanguageServer.BUILD_LOCK.unlock();
				if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 1) {
					return;
				}
				try {
					TitanLanguageServer.analyzeProjectUnconditionally(project).get();
				} catch (InterruptedException e) {
					TitanLogger.logError(e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					TitanLogger.logError(e);
				}
			}
		}
	};

	static {
		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.EXCLUDED_FROM_BUILD, excludedFileListListener);
	}

	public TitanLspExtensionsService(TitanLanguageServer languageServer) {
		this.languageServer = languageServer;
	}

	@Override
	public CompletableFuture<String> documentComment(DocumentCommentParams params) {
		return CompletableFuture.supplyAsync(() -> {
			String result = null;
			try {
				final ProjectItem projectItem = Project.INSTANCE.getProjectItem(
						IOUtils.getPath(params.getTextDocument().getUri()));
				if (projectItem == null) {
					return null;
				}

				final Module module = projectItem.getModule();
				if (module instanceof TTCN3Module) {
					final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(params.getPosition());
					module.accept(visitor);
					final IDeclaration declaration = visitor.getReferencedDeclaration();
					if (declaration == null) {
						result = GeneralConstants.EMPTY_STRING;
					} else {
						final ICommentable commentable = declaration.getCommentable();
						if (commentable == null) {
							result = "Null comment";
						} else {
							result = commentable.getHoverContent().getText(HoverContentType.INFO);
						}
					}
				}
			} catch (Exception e) {
				// Do nothing
			}
			return result;
		});
	}

	@Override
	public void setExcludedState(ExcludedStatesChangedParams params) {
		Configuration.INSTANCE.updateExcludedFromBuild(
				params.getExcludedStates().stream()
				.map(param -> param.getTextDocument().getUri())
				.collect(Collectors.toList()));
	}

	@Override
	public CompletableFuture<TpdProjectFiles> activateTpd(ActivateTpdParams params) {
		return CompletableFuture.supplyAsync(() -> {
			final Project project = Project.INSTANCE;
			final TpdProjectFiles tpdFiles = new TpdProjectFiles();
			if (params.getTpdPath() == null) {
				TitanLanguageServer.BUILD_LOCK.lock();
				try {
					if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 0) {
						project.resetProject();
					}
				} finally {
					TitanLanguageServer.BUILD_LOCK.unlock();
				}
				TitanLanguageServer.analyzeProjectUnconditionally(project);
				tpdFiles.success = true;
				return tpdFiles;
			}

			final Path path = IOUtils.getPath(params.getTpdPath());
			if (path == null) {
				tpdFiles.success = false;
				return tpdFiles;
			}
			TpdImporter importer = new TpdImporter(path.toFile());
			importer.processTpd(false, false, false, null);

			project.setTpdFiles(importer.getFileNames());
			project.outdateAllItems();

			final List<String> fileList = new ArrayList<>();
			importer.getFileNames().stream()
				.filter(f -> project.getProjectItem(f) != null)
				.forEach(filepath ->
					fileList.add(filepath.toString())
				);
			tpdFiles.success = !fileList.isEmpty();
			tpdFiles.result = Either.forLeft(fileList);

			if (tpdFiles.success) {
				TitanLanguageServer.BUILD_LOCK.lock();
				try {
					if (TitanLanguageServer.FIRST_ANALYSIS_LATCH.getCount() == 0) {
						project.resetProject();
					}
				} finally {
					TitanLanguageServer.BUILD_LOCK.unlock();
				}
//				Project reset possibly produces a substantial number of notifications.
//				VSCode client needs some time to process them.
//				It must be ensured, that the client is ready to receive requests
//				before sending the next one (e.g. createProgress).
//				workspaceFolders req doesn't affect anything
				try {
					TitanLanguageServer.getClient().workspaceFolders().get();
				} catch (InterruptedException e) {
					TitanLogger.logError(e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					TitanLogger.logError(e);
				}
				TitanLanguageServer.analyzeProject(project, true, path.toString());
			}
			return tpdFiles;
		});
	}

	@Override
	public CompletableFuture<String> deactivateTpd() {
		return CompletableFuture.supplyAsync(() -> {
			final boolean analyzeTpdOnly = Configuration.INSTANCE.getBoolean(Configuration.ANALYZE_ONLY_WHEN_TPD_IS_ACTIVE, false);
			final String message = "Tpd is deactivated";
			final Project project = Project.INSTANCE;
			TitanLanguageServer.BUILD_LOCK.lock();
			try {
				final Map<Path, ProjectItem> projectItems = project.getAllProjectItems();
				projectItems.entrySet().stream().forEach(item ->
					item.getValue().setExcluded(false)
				);
				project.resetProject();
			} finally {
				TitanLanguageServer.BUILD_LOCK.unlock();
			}
			if (!analyzeTpdOnly) {
				TitanLanguageServer.analyzeProject(project, false, GeneralConstants.EMPTY_STRING);
			}
			return message;
		});
	}

	@Override
	public CompletableFuture<String> generateDocumentComment(GenerateDocumentCommentParams params) {
		return CompletableFuture.supplyAsync(() -> {
			final Path path = IOUtils.getPath(params.getIdentifier().getUri());
			final Project project = Project.INSTANCE;
			if (project.getProjectItem(path) == null) {
				return null;
			}

			final Module module = project.getModuleByPath(path);
			final Position position = new Position(params.getPosition());
			final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(position);
			module.accept(visitor);
			final IDeclaration declaration = visitor.getReferencedDeclaration();
			if (declaration != null) {
				ICommentable commentable = declaration.getCommentable();
				final Location location = declaration.getLocation();
				if (commentable == null || commentable.hasDocumentComment() ||
						location == null) {
					return null;
				}

				final String lineEnding = project.getProjectItem(path).getLineEnding();
				return commentable.generateDocComment(params.getIndentation(), lineEnding);
			}

			return null;
		});
	}

	@Override
	public CompletableFuture<Either<MetricsResponse, MetricsStatsResponse>> getMetricData(MetricDataParams params) {
//		if(!Configuration.INSTANCE.getBoolean(Configuration.ENABLE_TITANIUM, false)) {
//			return null;
//		}
		return CompletableFuture.supplyAsync(() -> {
			final MetricDataProvider provider = new MetricDataProvider(params);
			return provider.getData();
		});
	}

	@Override
	public void environmentVariables(EnvironmentParams params) {
		params.getVariables().forEach((name, value) ->
			Configuration.INSTANCE.addPreference("env." + name, value)
		);
	}

	@Override
	public void folderConfig(FolderConfigParams params) {
		final FolderConfiguration config = TitanLanguageServer.getFolderConfiguration();
		config.setFolderNamingConventions(params.getConfig().getNamingConventions());
		config.setCodeSmellSeverities(params.getConfig().getCodeSmells());
	}
}
