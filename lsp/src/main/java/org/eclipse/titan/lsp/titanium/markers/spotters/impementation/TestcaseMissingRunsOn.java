/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import org.eclipse.titan.lsp.AST.ICompilerInfo.Ttcn_version;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Testcase;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * 
 * This spotter detects if a testcase has no 'runs on' clause.
 * This smell should not be reported for TTCN 2011 because 'runs on' is mandatory for that version. 
 * 
 * @author Miklos Magyari
 */
public class TestcaseMissingRunsOn extends BaseModuleCodeSmellSpotter {
	private static final String MISSING_RUNSON = "Missing `runs on' clause; MTC created by the testcase has no ports, component constants and variables";
	
	public TestcaseMissingRunsOn() {
		super(CodeSmellType.TESTCASE_MISSING_RUNS_ON);
		addStartNode(Def_Testcase.class);
	}

	@Override
	protected void process(IVisitableNode node, Problems problems) {
		if (node instanceof Def_Testcase) {
			final CompilationTimeStamp timestamp = CompilationTimeStamp.getBaseTimestamp();
			final Def_Testcase testcase = (Def_Testcase)node;
			if (testcase.getRunsOnReference(timestamp) == null && testcase.getTtcnVersion().equals(Ttcn_version.Y2023)) {
				problems.report(testcase.getIdentifier().getLocation(), MISSING_RUNSON);
			}
		}
	}
}
