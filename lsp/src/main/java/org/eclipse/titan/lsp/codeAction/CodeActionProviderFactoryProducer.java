/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.util.Optional;

public class CodeActionProviderFactoryProducer {
	public enum CodeActionProviderFactoryType {
		DATA,
		TAG
	}

	public static Optional<ICodeActionProviderFactory> getFactory(final CodeActionProviderFactoryType factoryType) {
		if (factoryType == CodeActionProviderFactoryType.DATA) {
			return Optional.of(new FactoryForDiagnosticData());
		}
		
		if (factoryType == CodeActionProviderFactoryType.TAG) {
			return Optional.of(new FactoryForDiagnosticTags());
		}

		return Optional.empty();
	}
}
