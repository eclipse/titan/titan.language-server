/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo.Chain;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Miklos Magyari
 */
public class Map_Type extends Type {
	private static final String MAP_UNSUPPORTED = "the `map'' type is not supported for TTCN3 version 2011";
	private static final String FULLNAMEPART1 = ".<from_type>";
	private static final String FULLNAMEPART2 = ".<to_type>";
	private static final String FULLNAMEPART3 = ".<set_of_keys>";
	private static final String FULLNAMEPART4 = ".<set_of_values>";
	
	private final Type fromType;
	private final Type toType;
	private SetOf_Type setOfKeysType;
	private SetOf_Type setOfValuesType;

	public Map_Type(final Type fromType, final Type toType) {
		this.fromType = fromType;
		this.toType = toType;
	}

	@Override
	public Type_type getTypetype() {
		return Type_type.TYPE_MAP;
	}
	
	@Override
    /** {@inheritDoc} */
    public Type_type getTypetypeTtcn3() {
    	if (isErroneous) {
    		return Type_type.TYPE_UNDEFINED;
    	}
    	return getTypetype();
    }

	@Override
	public IType getFieldType(CompilationTimeStamp timestamp, Reference reference, int actualSubReference,
			Expected_Value_type expectedIndex, IReferenceChain refChain, boolean interruptIfOptional) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		// TODO : checks
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		
		if (getTtcnVersion().equals(Ttcn_version.Y2011)) {
			location.reportWarning(MAP_UNSUPPORTED);
		}
		
		setOfKeysType = new SetOf_Type(fromType);
		setOfKeysType.check(timestamp);
		setOfValuesType = new SetOf_Type(toType);
		setOfValuesType.check(timestamp);
		
		fromType.check(timestamp);
		toType.check(timestamp);
	}
	
	@Override
	public boolean checkThisTemplate(CompilationTimeStamp timestamp, ITTCN3Template template, boolean isModified,
			boolean implicitOmit, Assignment lhs) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCompatible(CompilationTimeStamp timestamp, IType otherType, TypeCompatibilityInfo info,
			Chain leftChain, Chain rightChain) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getTypename() {
		return getFullName();
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		
		if (fromType != null) {
			fromType.setMyScope(myScope);
		}
		if (toType != null) {
			toType.setMyScope(scope);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);
		
		if (fromType == child) {
			return builder.append(FULLNAMEPART1);
		}
		if (toType == child) {
			return builder.append(FULLNAMEPART2);
		}
		if (setOfKeysType == child) {
			return builder.append(FULLNAMEPART3);
		}
		if (setOfValuesType == child) {
			return builder.append(FULLNAMEPART4);
		}
		
		return builder;
	}
}
