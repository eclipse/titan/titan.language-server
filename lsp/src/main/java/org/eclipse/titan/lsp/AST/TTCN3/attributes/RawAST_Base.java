/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

/**
 * Represents the common RAW encoding related setting extracted from variant attributes.
 *
 * @author Adam Knapp
 * */
public abstract class RawAST_Base {

	// Logic kept to be in-line with the compiler side
	public static final int XDEFUNSIGNED = 1;
	public static final	int XDEFCOMPL = 2;
	public static final int XDEFSIGNBIT = 3;
	public static final int XDEFYES = 2;
	public static final int XDEFNO = 1;
	public static final int XDEFREVERSE = 3;
	public static final int XDEFMSB = 1;
	public static final int XDEFLSB = 2;
	public static final int XDEFBITS = 1;
	public static final int XDEFOCTETS = 2;
	public static final int XDEFLEFT = 1;
	public static final int XDEFRIGHT = 2;
	public static final int XDEFFIRST = 1;
	public static final int XDEFLAST = 2;
	public static final int XDEFLOW = 1;
	public static final int XDEFHIGH = 2;
	public static final int XDEFDEFAULT = -1;

	/** Contains {@linkplain rawAST_toplevel#bitorder} */
	public static class rawAST_toplevel {
		/** Invert bitorder of the encoded data */
		public int bitorder;
	}

	/** Nr of bits per character, hexstring : 4, octetstring and charstring : 8, etc. */
	public int fieldlength;
	/** Handling of sign: no, 2scomp, signbit */
	public int comp;
	/** {@linkplain RawAST_Base#XDEFMSB}, {@linkplain RawAST_Base#XDEFLSB} */
	public int byteorder;
	/** {@linkplain RawAST_Base#XDEFLEFT}, {@linkplain RawAST_Base#XDEFRIGHT} */
	public int align;
	/** {@linkplain RawAST_Base#XDEFMSB}, {@linkplain RawAST_Base#XDEFLSB} */
	public int bitorderinfield;
	/** {@linkplain RawAST_Base#XDEFMSB}, {@linkplain RawAST_Base#XDEFLSB} */
	public int bitorderinoctet;
	/** {@linkplain RawAST_Base#XDEFYES}, {@linkplain RawAST_Base#XDEFNO} */
	public int extension_bit;
	/** {@linkplain RawAST_Base#XDEFLOW}, {@linkplain RawAST_Base#XDEFHIGH} */
	public int hexorder;
	/** {@linkplain RawAST_Base#XDEFYES}: next field starts at next octet */
	public int padding;  // XDEFYES: next field starts at next octet
	/** {@linkplain RawAST_Base#XDEFMSB}, {@linkplain RawAST_Base#XDEFLSB} */
	public int fieldorder;
	public int lengthto_offset;
	public int ptroffset;
	/** {@linkplain RawAST_Base#XDEFOCTETS}, {@linkplain RawAST_Base#XDEFBITS} */
	public int unit;
	public rawAST_toplevel toplevel = new rawAST_toplevel();
	public int toplevelind;
	public int repeatable;

}
