/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.ttcn3parser;

import java.nio.file.Path;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.parsers.AntlrErrorStrategy;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;

/**
 * Reparser for getting pr_Identifier
 * 
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public class Ttcn3FileReparser implements ITtcn3FileReparser {

	private final TTCN3ReparseUpdater mReparser;
	private final Path mFile;
	private final ProjectSourceParser mSourceParser;
	private final Map<Path, String> mFileMap;
	private final Map<Path, String> mUptodateFiles;
	private final Set<Path> mHighlySyntaxErroneousFiles;

	private boolean mSyntacticallyOutdated = false;

	public Ttcn3FileReparser( final TTCN3ReparseUpdater aReparser,
							  final Path aFile,
							  final ProjectSourceParser aSourceParser,
							  final Map<Path, String> aFileMap,
							  final Map<Path, String> aUptodateFiles,
							  final Set<Path> aHighlySyntaxErroneousFiles ) {
		mReparser = aReparser;
		mFile = aFile;
		mSourceParser = aSourceParser;
		mFileMap = aFileMap;
		mUptodateFiles = aUptodateFiles;
		mHighlySyntaxErroneousFiles = aHighlySyntaxErroneousFiles;
	}

	@Override
	public boolean parse() {
		mReparser.parse(parser -> {
			final AntlrErrorStrategy strategy = new AntlrErrorStrategy();
			parser.setErrorHandler(strategy);
			final ParseTree root = parser.pr_TTCN3File();
			ParserUtilities.logParseTree( root, parser );
			final TTCN3Module actualTtcn3Module = parser.getModule();
			if (actualTtcn3Module != null && actualTtcn3Module.getIdentifier() != null) {
				mSourceParser.getSemanticAnalyzer().addModule(actualTtcn3Module);
				mFileMap.put(mFile, actualTtcn3Module.getName());
				mUptodateFiles.put(mFile, actualTtcn3Module.getName());

				if (actualTtcn3Module.getLocation().getEndLine() == -1 && !parser.isErrorListEmpty()) {
					// FIXME
					//actualTtcn3Module.getLocation().setEndOffset((int) new Path(mFile.getLocationURI()).length());
				}
			} else {
				mSyntacticallyOutdated = true;
				mHighlySyntaxErroneousFiles.add(mFile);
			}
		});
		
		return mSyntacticallyOutdated;
	}
}
