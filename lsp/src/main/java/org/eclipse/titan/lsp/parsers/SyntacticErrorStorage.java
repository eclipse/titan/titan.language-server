/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.File;

import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.AST.Location;

/**
 * @author Kristof Szabados
 *
 */
public final class SyntacticErrorStorage {
	private static final String SYNTAX_ERROR = "Syntax error";
	private final Location location;
	private final String message;

	public SyntacticErrorStorage(final File file, final int lineNumber, final int charStart, final int charEnd, final String message) {
		location = new Location(file, lineNumber, charStart, lineNumber, charEnd);
		this.message = message;
	}

	public Location getLocation() {
		return location;
	}

	public String getMessage() {
		return message;
	}

	public void reportProblem(final DiagnosticSeverity severity) {
		location.reportProblem(SYNTAX_ERROR, severity);
	}

	public void reportSyntacticError() {
		reportSyntacticError(null);
	}

	public void reportSyntacticError(final Location offset) {
		Location.shift(location, offset).reportSyntacticError(SYNTAX_ERROR);
	}
}
