/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.titanium.markers.handlers.Marker;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * This spotter detects definitions with names only differing from other definitions by capitalization.
 * It is a more generic solution to replace the DuplicateName spotter. 
 * 
 * @author Miklos Magyari
 */
public class DefinitionDiffersInCaseOnly extends BaseModuleCodeSmellSpotter {
	private static final String WARNING_MESSAGE1 = "The name of this definition only differs from `{0}'' by capitalization.";
	private static final String WARNING_MESSAGE2 = "Later definition `{0}'' only differs by capitalization";
	
	public DefinitionDiffersInCaseOnly() {
		super(CodeSmellType.DEFINITION_NAME_DIFFERS_IN_CASE_ONLY);
		addStartNode(Definition.class);
	}

	@Override
	protected void process(IVisitableNode node, Problems problems) {
		List<Definition> visibleDefinitions = new ArrayList<>();
		if (node instanceof Definition) {
			final Definition def = (Definition)node;
			final String defName = def.getIdentifier().getDisplayName();

			Scope defScope = def.getMyScope();	
			if (defScope == null) {
				return;
			}

			final List<Definition> scopeVisibleDefinitions = defScope.getVisibleDefinitions();
			if (scopeVisibleDefinitions.isEmpty()) {
				return;
			}
			visibleDefinitions.addAll(scopeVisibleDefinitions);

			Scope scope = defScope.getParentScope();
			while(scope != null) {
				final List<Definition> parentDefs = scope.getVisibleDefinitions();
				if (!parentDefs.isEmpty()) {
					for (final Definition toBeAdded : parentDefs) {
						if (!visibleDefinitions.contains(toBeAdded)) {
							visibleDefinitions.add(toBeAdded);
						}
					}
				}
				final Scope parentScope = scope.getParentScope();
				scope = scope == parentScope ? null : parentScope;
			}

			for (final Definition visibleDefinition : visibleDefinitions) {
				final String availableDefName = visibleDefinition.getIdentifier().getDisplayName();
				if (!availableDefName.equals(defName) && availableDefName.equalsIgnoreCase(defName)) {
					final Marker marker = problems.report(def.getIdentifier().getLocation(), 
						MessageFormat.format(WARNING_MESSAGE1, visibleDefinition.getIdentifier().getDisplayName()));
					final Marker origMarker = problems.report(visibleDefinition.getIdentifier().getLocation(), 
							MessageFormat.format(WARNING_MESSAGE2, def.getIdentifier().getDisplayName()));
					marker.addRelatedInformation(visibleDefinition.getIdentifier().getLocation(), "Similar declaration was here");
					origMarker.addRelatedInformation(def.getIdentifier().getLocation(), "Later definition is here");
				}
			}
			visibleDefinitions.clear();
		}
	}
}
