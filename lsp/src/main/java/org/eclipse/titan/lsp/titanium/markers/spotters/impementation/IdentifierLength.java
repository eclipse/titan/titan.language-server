/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * This spotter checks if an identifier's name is shorter or longer than the specified limit.
 * Both too short and long names can make the code harder to read.
 * 
 *  @author Miklos Magyari
 */
public class IdentifierLength extends BaseModuleCodeSmellSpotter {
	// FIXME : make these configurable
	private int NAME_MIN_LENGTH = 2;
	
	/** value is imported from the removed 'definition too long' smell */
	private int NAME_MAX_LENGTH = 42;

	private static final String TOO_SHORT_ERROR = "Identifier `{0}'' is too short, it can decrease code readability";
	private static final String TOO_LONG_ERROR = "Identifier `{0}'' is too long, it can decrease code readability";
	
	public IdentifierLength() {
		super(CodeSmellType.IDENTIFIER_LENGTH);
		addStartNode(Identifier.class);
	}
	
	@Override
	protected void process(IVisitableNode node, Problems problems) {
		if (node instanceof Identifier) {
			final Identifier id = (Identifier)node;
			final String name = id.getDisplayName();
			if (name instanceof String) {
				int len = name.length();
				if (len < NAME_MIN_LENGTH) {
					problems.report(id.getLocation(), 
						MessageFormat.format(TOO_SHORT_ERROR, name));
				}
				if (len > NAME_MAX_LENGTH) {
					problems.report(id.getLocation(), 
						MessageFormat.format(TOO_LONG_ERROR, name));
				}
			}
		}
	}
}
