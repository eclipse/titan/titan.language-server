/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.declarationsearch;

import java.util.Collections;
import java.util.List;

import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;

/**
 * This class represents such a declaration, which can be referenced
 * that could have documentation comments on it.
 * 
 * @author Adam Knapp
 *
 */
public class CommentableDeclaration implements IDeclaration {
	private final ICommentable commentable; 

	public CommentableDeclaration(final ICommentable commentable) {
		this.commentable = commentable;
	}

	@Override
	public List<Hit> getReferences(Module module) { 
		return Collections.<Hit>emptyList();
	}

	@Override
	public Identifier getIdentifier() {
		return null;
	}

	@Override
	public Location getLocation() {
		if (commentable instanceof ILocateableNode) {
			return ((ILocateableNode) commentable).getLocation();
		}
		return NULL_Location.getInstance();
	}

	@Override
	public ICommentable getCommentable() {
		return commentable;
	}

}
