/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.TTCN3.values.Notused_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a template that matches for not used elements.
 *
 * @author Kristof Szabados
 * */
public final class NotUsed_Template extends TTCN3Template {

	// cache storing the value form of this if already created, or null
	private Notused_Value asValue = null;

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.TEMPLATE_NOTUSED;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder(MINUS_SIGN);
		return addToStringRepresentation(builder).toString();
	}

	@Override
	/** {@inheritDoc} */
	public boolean isValue(final CompilationTimeStamp timestamp) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IValue getValue() {
		if (asValue != null) {
			return asValue;
		}

		asValue = new Notused_Value();
		asValue.setLocation(getLocation());
		asValue.setMyScope(getMyScope());
		asValue.setFullNameParent(getNameParent());
		asValue.setMyGovernor(getMyGovernor());

		return asValue;
	}

	@Override
	/** {@inheritDoc} */
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		if (isBuildCancelled()) {
			return;
		}
		// unfoldable at this point
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, omitAllowed, usageLocation);
		if (baseTemplate != null) {
			return baseTemplate.checkValueomitRestriction(timestamp, definitionName, omitAllowed, usageLocation);
		}
		return true;
	}
}
