/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * This class handles a list of invalid AST nodes.
 * 
 * @author Miklos Magyari
 *
 */
public class InvalidASTNodes implements IVisitableNode {
	private List<InvalidASTNode> invalidNodes;
	private Scope parent;

	public InvalidASTNodes() {
		invalidNodes = new CopyOnWriteArrayList<>();
	}
	
	public InvalidASTNodes(final List<InvalidASTNode> nodes) {
		invalidNodes = nodes;
	}
	
	public List<InvalidASTNode> getInvalidNodes() {
		return invalidNodes;
	}

	public void setInvalidNodes(List<InvalidASTNode> invalidNodes) {
		this.invalidNodes = invalidNodes;
	}
	
	public void check(final CompilationTimeStamp timestamp) {
		for (final InvalidASTNode node : invalidNodes) {
			node.check(timestamp);
		}
	}
	
	public void setMyScope(final Scope scope) {
		parent = scope;
	}
	
	public Scope getMyScope() {
		return parent;
	}

	@Override
	public boolean accept(ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final InvalidASTNode node : invalidNodes) {
			if (!node.accept(v)) {
				return false;
			}
		}
		if (v.leave(this) == ASTVisitor.V_ABORT) {
			return false;
		}
		return true;
	}
}
