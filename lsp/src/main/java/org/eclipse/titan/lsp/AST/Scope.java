/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.lang.ref.WeakReference;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ControlPart;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Testcase;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Getter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.PortScope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.RunsOnScope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Setter;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.types.Altstep_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Function_Type;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * The Scope class represents general visibility scopes.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public abstract class Scope implements INamedNode, IIdentifierContainer, IVisitableNode, ICommentable, ILocatableComment {
	private static final String RUNSONREQUIRED = "A definition without `runs on'' clause cannot {0} {1}, which runs on component type `{2}''";
	private static final String RUNSONREQUIRED2 = "A definition without `runs on'' clause cannot {0} a value of {1} type `{2}'',"
			+ " which runs on component type `{3}''";
	private static final String RUNSONMISSMATCH = "Runs on clause mismatch: A definition that runs on component type `{0}'' cannot {1} {2},"
			+ " which runs on `{3}''";
	private static final String RUNSONMISSMATCH2 = "Runs on clause mismatch: A definition that runs on component type `{0}'' cannot {1}"
			+ " a value of {2} type `{3}'', which runs on `{4}''";

	protected Scope parentScope;
	protected List<Location> subScopeLocations;
	protected List<Scope> subScopes;

	/** Some special kind of parent. Used only in conjunction with ASN.1 parameterized references. */
	protected Scope parentScopeGen;

	protected String scopeName = GeneralConstants.EMPTY_STRING;

	/** The name of the scope as returned by the __SCOPE__ macro */
	private String scopeMacroName = GeneralConstants.EMPTY_STRING;

	/** the naming parent of the scope. */
	private WeakReference<INamedNode> nameParent;

	/** Optional documentation comment according to ETSI ES 201 873-10 */
	private DocumentComment documentComment = null;

	/** content of hover window for this definition */
	protected Ttcn3HoverContent hoverContent = null;

	public void setScopeName(final String name) {
		scopeName = name;
	}

	public String getScopeName() {
		final StringBuilder builder = new StringBuilder();
		if (parentScope != null) {
			builder.append(parentScope.getScopeName());
		}

		if (scopeName != null && !scopeName.isEmpty()) {
			if (builder.length() == 0) {
				builder.append(scopeName);
			} else {
				builder.append(INamedNode.DOT);
				builder.append(scopeName);
			}
		}

		return builder.toString();
	}

	/**
	 * Sets the name for this scope it should be referred to by the __SCOPE__ macro.
	 * @param name the name to be set
	 */
	public final void setScopeMacroName(final String name) {
		scopeMacroName = name;
	}

	/**
	 * Calculates the name for this scope as required by the __SCOPE__ macro.
	 * @return the __SCOPE__ macro name for this scope.
	 */
	public final String getScopeMacroName() {
		if (scopeMacroName != null && !scopeMacroName.isEmpty()) {
			return scopeMacroName;
		}

		if (parentScope != null) {
			return parentScope.getScopeMacroName();
		}

		return "unknown scope";
	}

	@Override
	/** {@inheritDoc} */
	public String getFullName() {
		return getFullName(null).toString();
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		if (nameParent == null) {
			return new StringBuilder();
		}

		final INamedNode tempParent = nameParent.get();
		// //to avoid infinite loop
		if (tempParent == null || tempParent == this) {
			return new StringBuilder();
		}

		return tempParent.getFullName(this);
	}

	@Override
	/** {@inheritDoc} */
	public final void setFullNameParent(final INamedNode nameParent) {
		this.nameParent = new WeakReference<INamedNode>(nameParent);
	}

	@Override
	/** {@inheritDoc} */
	public INamedNode getNameParent() {
		if (nameParent == null) {
			return null;
		}

		return nameParent.get();
	}

	public final void setParentScope(final Scope parentScope) {
		this.parentScope = parentScope;
	}

	public final Scope getParentScope() {
		return parentScope;
	}

	public final void setParentScopeGen(final Scope parentScope) {
		this.parentScopeGen = parentScope;
	}

	/**
	 * Search in the scope hierarchy for the innermost enclosing statementblock scope unit.
	 * @return the innermost enclosing statement block, or null if none.
	 */
	public StatementBlock getStatementBlockScope() {
		if (parentScope == null) {
			return null;
		}

		return parentScope.getStatementBlockScope();
	}

	/**
	 * Return the module this scope is contained in. Can return
	 * <code>null</code>, it indicates a FATAL ERROR.
	 *
	 * @return the module of the scope, or null in case of error.
	 */
	public Module getModuleScope() {
		if (parentScope == null) {
			return null;
		}

		return parentScope.getModuleScope();
	}

	/**
	 * Return the module this scope is contained in for code generation. Can return
	 * <code>null</code>, it indicates a FATAL ERROR.
	 *
	 * @return the module of the scope, or null in case of error.
	 */
	public Module getModuleScopeGen() {
		if (parentScopeGen != null) {
			return parentScopeGen.getModuleScopeGen();
		} else if (parentScope != null) {
			return parentScope.getModuleScopeGen();
		}

		return null;
	}

	/**
	 * Return the control part this scope is contained in. Can return
	 * <code>null</code>, it indicates that the scope is not inside a
	 * control part..
	 *
	 * @return the control part of the scope, or null if the scope is not in
	 *         a control part.
	 */
	public ControlPart getControlPart() {
		if (parentScope == null) {
			return null;
		}

		return parentScope.getControlPart();
	}

	/**
	 * @return the scope unit of the hierarchy that belongs to a `runs on'
	 *         clause, or {@code null}.
	 */
	public RunsOnScope getScopeRunsOn() {
		if (parentScope == null) {
			return null;
		}

		return parentScope.getScopeRunsOn();
	}

	/**
	 * @return the scope unit of the hierarchy that belongs to a runs on `port'
	 *         clause, or {@code null}.
	 */
	public PortScope getScopePort() {
		if (parentScope == null) {
			return null;
		}

		return parentScope.getScopePort();
	}

	/**
	 * Collects the TTCN-3 component type associated with the mtc or system
	 * component.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param isSystem {@code true} if searching for the system component, {@code false} otherwise.
	 * @return the system or mtc component type, or null if it cannot be
	 *         determined (outside testcase definitions - in functions and in altsteps )
	 */
	public Component_Type getMtcSystemComponentType(final CompilationTimeStamp timestamp, final boolean isSystem) {
		if (parentScope == null) {
			return null;
		}

		return parentScope.getMtcSystemComponentType(timestamp, isSystem);
	}

	/**
	 * @return the assignments/module definitions scope, or {@code null}.
	 */
	public Assignments getAssignmentsScope() {
		if (parentScope == null) {
			return null;
		}

		return parentScope.getAssignmentsScope();
	}

	/**
	 * Checks that operations that can only be executed in definitions that
	 * have a runs on scope, are really executed in such a definition. And
	 * that the required runs on component is compatible with the runs on
	 * component of the definition.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param assignment the assignment to check (used or referred to by the statement).
	 * @param errorLocation the location to report the error to.
	 * @param operation the name of the operation.
	 */
	public void checkRunsOnScope(final CompilationTimeStamp timestamp, final Assignment assignment, final ILocateableNode errorLocation,
			final String operation) {
		Component_Type referencedComponent;
		switch (assignment.getAssignmentType()) {
		case A_ALTSTEP:
			referencedComponent = ((Def_Altstep) assignment).getRunsOnType(timestamp);
			break;
		case A_FUNCTION:
		case A_FUNCTION_RVAL:
		case A_FUNCTION_RTEMP:
			referencedComponent = ((Def_Function) assignment).getRunsOnType(timestamp);
			break;
		case A_TESTCASE:
			referencedComponent = ((Def_Testcase) assignment).getRunsOnType(timestamp);
			break;
		default:
			return;
		}

		if (referencedComponent == null) {
			return;
		}

		final RunsOnScope runsOnScope = getScopeRunsOn();
		if (runsOnScope == null) {
			errorLocation.getLocation().reportSemanticError(
					MessageFormat.format(RUNSONREQUIRED, operation, assignment.getDescription(),
							referencedComponent.getTypename()));
		} else {
			final Component_Type localType = runsOnScope.getComponentType();
			if (!referencedComponent.isCompatible(timestamp, localType, null, null, null)) {
				errorLocation.getLocation().reportSemanticError(
						MessageFormat.format(RUNSONMISSMATCH, localType.getTypename(), operation,
								assignment.getDescription(), referencedComponent.getTypename()));
			}
		}
	}

	/**
	 * Checks that operations that can only be executed in definitions that
	 * have a runs on scope, are really executed in such a definition. And
	 * that the required runs on component is compatible with the runs on
	 * component of the definition.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param type the fat type to check (used or referred to by the location).
	 * @param errorLocation the location to report the error to.
	 * @param operation the name of the operation.
	 */
	public void checkRunsOnScope(final CompilationTimeStamp timestamp, final IType type, final ILocateableNode errorLocation,
			final String operation) {
		if (type == null) {
			return;
		}

		Component_Type referencedComponent;
		switch (type.getTypetype()) {
		case TYPE_FUNCTION:
			referencedComponent = ((Function_Type) type).getRunsOnType(timestamp);
			break;
		case TYPE_ALTSTEP:
			referencedComponent = ((Altstep_Type) type).getRunsOnType(timestamp);
			break;
		default:
			return;
		}

		if (referencedComponent == null) {
			return;
		}

		final String typename = type.getTypetype().getName();
		final RunsOnScope runsOnScope = getScopeRunsOn();
		if (runsOnScope == null) {
			errorLocation.getLocation().reportSemanticError(
					MessageFormat.format(RUNSONREQUIRED2, operation, typename, type.getTypename(),
							referencedComponent.getTypename()));
		} else {
			final Component_Type localType = runsOnScope.getComponentType();
			if (!referencedComponent.isCompatible(timestamp, localType, null, null, null)) {
				errorLocation.getLocation().reportSemanticError(
						MessageFormat.format(RUNSONMISSMATCH2, localType.getTypename(), operation, typename,
								type.getTypename(), referencedComponent.getTypename()));
			}
		}
	}

	/**
	 * Checks the 'mtc' clause of assignment if it can be called from this scope unit
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param mtcComponentType the system component type of the assignment.
	 * @param errorLocation the location to report the error to.
	 * @param description the name of the operation and description of assignment.
	 * @param inControlPart is the function called from a control part?
	 */
	public void checkMTCScope(final CompilationTimeStamp timestamp, final Component_Type mtcComponentType, final ILocateableNode errorLocation,
			final String description, final boolean inControlPart) {
		if (mtcComponentType == null) {
			// definitions without 'mtc' can be called from anywhere
			return;
		}

		if (inControlPart) {
			errorLocation.getLocation().reportSemanticError("Function with mtc or system clause is not allowed in control part.");
			return;
		}

		final Component_Type componentType = getMtcSystemComponentType(timestamp, false);
		if (componentType != null) {
			if (!mtcComponentType.isCompatibleByPort(timestamp, componentType)) {
				// the 'mtc' clause of the referred definition is not compatible
				// with that of the current scope (i.e. the referring definition)
				errorLocation.getLocation().reportSemanticError(MessageFormat.format("Mtc clause mismatch: A definition that runs on component type `{0}'' cannot {1}, which mtc clause is `{2}''", componentType.getTypename(), description, mtcComponentType.getTypename()));
			}
		}
	}

	/**
	 * Checks the 'system' clause of assignment if it can be called from this scope unit
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param systemComponentType the system component type of the assignment.
	 * @param errorLocation the location to report the error to.
	 * @param description the name of the operation and description of assignment.
	 * @param inControlPart is the function called from a control part?
	 */
	public void checkSystemScope(final CompilationTimeStamp timestamp, final Component_Type systemComponentType, final ILocateableNode errorLocation,
			final String description, final boolean inControlPart) {
		if (systemComponentType == null) {
			// definitions without 'mtc' can be called from anywhere
			return;
		}

		if (inControlPart) {
			errorLocation.getLocation().reportSemanticError("Function with mtc or system clause is not allowed in control part.");
			return;
		}

		final Component_Type componentType = getMtcSystemComponentType(timestamp, false);
		if (componentType != null) {
			if (!systemComponentType.isCompatibleByPort(timestamp, componentType)) {
				// the 'mtc' clause of the referred definition is not compatible
				// with that of the current scope (i.e. the referring definition)
				errorLocation.getLocation().reportSemanticError(MessageFormat.format("Mtc clause mismatch: A definition that runs on component type `{0}'' cannot {1}, which mtc clause is `{2}''", componentType.getTypename(), description, systemComponentType.getTypename()));
			}
		}
	}

	/**
	 * Finds and returns the smallest sub-scope of the current scope, which
	 * still physically encloses the provided position.
	 *
	 * @param position to position of which the smallest enclosing scope we wish to find.
	 * @return the smallest enclosing sub-scope of the actual scope, or
	 *         itself, if no sub-scope was found.
	 */
	public Scope getSmallestEnclosingScope(final Position position) {
		if (subScopes == null) {
			return this;
		}

		Location location;
		final int size = subScopeLocations.size();
		for (int i = 0; i < size; i++) {
			location = subScopeLocations.get(i);
			if (location.getStartPosition().before(position) && position.before(location.getEndPosition())) {
				return subScopes.get(i).getSmallestEnclosingScope(position);
			}
		}
		return this;
	}

	/**
	 * Adds a subscope to the actual scope, which is physically inside the
	 * actual scope (might not be a child of the scope though).
	 * <p>
	 * This is used to create a scope hierarchy, which can be used to
	 * quickly locate the smallest enclosing scope of a region described
	 * with positions inside a file.
	 *
	 * @param location the location of the scope to be added
	 * @param scope the scope to be added.
	 */
	public void addSubScope(final Location location, final Scope scope) {
		if (NULL_Location.INSTANCE.equals(location)) {
			return;
		}

		if (subScopes == null) {
			subScopes = new ArrayList<Scope>(1);
			subScopes.add(scope);
			subScopeLocations = new ArrayList<Location>(1);
			subScopeLocations.add(location);
			return;
		}

		int index = subScopeLocations.size() - 1;
		if (index == -1) {
			subScopeLocations.add(location);
			subScopes.add(scope);
			return;
		}

		// don't add the same subscope more than once
		for (final Scope ss : subScopes) {
			if (scope == ss) {
				return;
			}
		}

		Location subScope = subScopeLocations.get(index);
		while (index > 0 && (subScope.getEndPosition().after(location.getStartPosition()) || subScope.getEndPosition().getLine() == -1)) {
			index--;
			subScope = subScopeLocations.get(index);
		}
		subScopeLocations.add(index, location);
		subScopes.add(index, scope);
	}

	/**
	 * Removes the scope from its parent's lists of sub-scopes.
	 */
	public void remove() {
		if (subScopeLocations != null) {
			subScopeLocations.clear();
			subScopeLocations = null;
		}
		if (subScopes != null) {
			subScopes.clear();
			subScopes = null;
		}

		if (parentScope != null) {
			final int index = parentScope.subScopes.indexOf(this);
			parentScope.subScopes.remove(index);
			parentScope.subScopeLocations.remove(index);
		}
	}

	/**
	 * Detects if an assignment can be reached from the actual scope, using
	 * the provided identifier.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param identifier the identifier to be use to search for the assignment.
	 * @return {@code true} if an assignment with the same identifier exists, or {@code false} otherwise.
	 */
	public boolean hasAssignmentWithId(final CompilationTimeStamp timestamp, final Identifier identifier) {
		if (parentScope != null) {
			return parentScope.hasAssignmentWithId(timestamp, identifier);
		}
		return false;
	}

	/**
	 * Tells if the provided identifier can be a valid module identifier.
	 * <p>
	 * Used to detect, if a definition could hide other definitions coming
	 * from an imported, or the actual module.
	 *
	 * @param identifier the identifier to check.
	 *
	 * @return {@code true} if the identifier is a valid module identifier.
	 */
	public boolean isValidModuleId(final Identifier identifier) {
		if (parentScope != null) {
			return parentScope.isValidModuleId(identifier);
		}
		return false;
	}

	/**
	 * Tries to find the assignment that a given reference refers to.
	 * <p>
	 * This function walks the scope hierarchy in a bottom-up order, for
	 * this reason it must be started in the smallest enclosing scope, or
	 * might miss some assignments.
	 *
	 * @param timestamp the timestamp of the actual semantic checking cycle.
	 * @param reference the reference used to describe the assignment to be found.
	 * @return the first assignment that can be referred to be the provided reference, or {@code null}.
	 */
	public abstract Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference);

	/**
	 * Tries to find the assignment that a given reference refers to.
	 * <p>
	 * This function walks the scope hierarchy in a bottom-up order, for
	 * this reason it must be started in the smallest enclosing scope, or
	 * might miss some assignments.
	 *
	 * @param timestamp the timestamp of the actual semantic checking cycle.
	 * @param reference the reference used to describe the assignment to be found.
	 * @return the first assignment that can be referred to be the provided reference, or {@code null}.
	 */
	public abstract Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain referenceChain);

	public abstract Assignment getEnclosingAssignment(final Position offset);

	/**
	 * Returns an unmodifiable view of definitions
	 * @return an unmodifiable view of definitions 
	 */
	public List<Definition> getVisibleDefinitions() {
		return Collections.emptyList();
	}

	public String getSubScopeAndLocInfo() {
		if (subScopeLocations == null) {
			return GeneralConstants.EMPTY_STRING;
		}

		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < subScopeLocations.size(); i++) {
			if (i != 0) {
				sb.append(", ");
			}
			sb.append(subScopes.get(i).getClass().getName()).append(": LOC=[").append(subScopeLocations.get(i).getStartPosition()).append(MINUS_SIGN)
			.append(subScopeLocations.get(i).getEndPosition()).append(SQUARECLOSE);
		}
		return sb.toString();
	}

	public String getInfo() {
		final StringBuilder sb = new StringBuilder();
		Scope s = this;
		boolean first = true;
		while (s != null) {
			if (first) {
				sb.append("scope: ");
			} else {
				sb.append("\n    -> ");
			}
			sb.append('[').append(s.scopeName).append("]*[").append(s.getFullName()).append("]*[").append(s.getClass().getName())
			.append("] SUBSCOPES: ").append(s.getSubScopeAndLocInfo());
			s = s.parentScope;
			first = false;
		}
		return sb.toString();
	}

	public String getParentInfo() {
		final StringBuilder sb = new StringBuilder();
		Scope s = this;
		boolean first = true;
		while (s != null) {
			if (!first) {
				sb.append(" -> ");
			}
			sb.append(SQUAREOPEN).append(s.getClass().getSimpleName()).append("] ").append(s.getFullName());
			s = s.parentScope;
			first = false;
		}
		return sb.toString();
	}

	public boolean isChildOf(final Scope s) {
		Scope tempScope = getParentScope();
		while (tempScope != null) {
			if (tempScope == s) {
				return true;
			}

			tempScope = tempScope.getParentScope();
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasDocumentComment() {
		return documentComment != null;
	}

	@Override
	/** {@inheritDoc} */
	public DocumentComment getDocumentComment() {
		return documentComment;
	}

	@Override
	/** {@inheritDoc} */
	public void setDocumentComment(DocumentComment docComment) {
		documentComment = docComment;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		// Do nothing, relevant subclasses overrides this method
		return hoverContent;
	}

	@Override
	public String generateDocComment(String indentation, String lineEnding) {
		// Do nothing
		return null;
	}
	
	public Class_Type getScopeClass() {
		if (parentScope != null) {
			return parentScope.getScopeClass();
		}
		return null;
	}
	
	/**
	 * Checks whether the scope is inside a class
	 * @return
	 */
	public boolean isClassScope() {
		return false;
	}

	/**
	 * Checks whether the current scope is inside a class property setter
	 * @return
	 */
	public boolean isInSetterScope() {
		if (parentScope != null) {
			return parentScope.isInSetterScope();
		}
		return false;
	}
	
	/**
	 * Checks whether the current scope is inside a class property getter
	 * @return
	 */
	public boolean isInGetterScope() {
		if (parentScope != null) {
			return parentScope.isInGetterScope();
		}
		return false;
	}
	
	/**
	 * Gets the class property setter for this scope
	 * @return
	 */
	public Setter getScopeSetter() {
		if (parentScope != null) {
			return parentScope.getScopeSetter();
		}
		return null;
	}
	
	/**
	 * Gets the class property setter for this scope
	 * @return
	 */
	public Getter getScopeGetter() {
		if (parentScope != null) {
			return parentScope.getScopeGetter();
		}
		return null;
	}

	public Class_Type getEnclosingClass() {
		Scope scope = this;
		do {
			if (scope instanceof ClassTypeBody) {
				return ((ClassTypeBody)scope).getParentClass();
			}
			scope = scope.getParentScope();
		} while (scope != null);
		
		return null;
	}
	
	@Override
	public void checkDocumentComment() {
		// No generic check for scopes right now
	}
}
