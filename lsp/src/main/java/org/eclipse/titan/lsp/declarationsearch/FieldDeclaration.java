/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.declarationsearch;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;

/**
 * @author Szabolcs Beres
 * */
class FieldDeclaration implements IDeclaration {
	private final Assignment ass;
	private final Identifier fieldId;

	public FieldDeclaration(final Assignment ass, final Identifier id) {
		this.ass = ass;
		this.fieldId = id;
	}

	@Override
	public List<Hit> getReferences(final Module module) {
		try {
			final ReferenceFinder referenceFinder = new ReferenceFinder(ass);
			referenceFinder.fieldId = fieldId;
			final List<Hit> result = referenceFinder.findReferencesInModule(module);
			if (ass.getMyScope().getModuleScope() == module) {
				result.add(new Hit(fieldId));
			}
			return result;
		} catch (final IllegalArgumentException e) {
			return new ArrayList<ReferenceFinder.Hit>();
		}
	}

	@Override
	public Identifier getIdentifier() {
		return fieldId;
	}

	@Override
	public ReferenceFinder getReferenceFinder(final Module module) {
		try {
			return new ReferenceFinder(ass,fieldId);
		} catch (final IllegalArgumentException e) {
			return null;
		}
	}

	@Override
	public Assignment getAssignment() {
		return ass;
	}

	@Override
	public Location getLocation() {
		return fieldId.getLocation();
	}

	@Override
	public ICommentable getCommentable() {
		if (ass instanceof ICommentable) {
			return (ICommentable) ass;
		}
		return null;
	}
}
