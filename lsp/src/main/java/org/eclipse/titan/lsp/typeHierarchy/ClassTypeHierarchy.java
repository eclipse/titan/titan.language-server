/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.typeHierarchy;

import java.util.List;
import java.util.Objects;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.TypeHierarchyItem;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;

public class ClassTypeHierarchy {
	private static final SymbolKind kind = SymbolKind.Function;
	
	private Class_Type assType;

	public ClassTypeHierarchy(Class_Type assType) {
		this.assType = Objects.requireNonNull(assType);
	}
	
	private TypeHierarchyItem createTypeHierarchyItem(Class_Type ass) {
		final String name = ass.getIdentifier().getDisplayName();
		final String uri = ass.getLocation().getFile().toPath().toUri().toString();
		final Range range = ass.getLocation().getRange();
		return new TypeHierarchyItem(name, kind, uri, range, range);
	}
	
	public List<TypeHierarchyItem> getSuperTypes() {
		final Class_Type parentClass = assType.getBaseClass();
		if (parentClass != null) {
			return List.of(createTypeHierarchyItem(parentClass));
		}
		return null;
	}

	public List<TypeHierarchyItem> getSubTypes() {
		return null;
	}
}
