/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.CachedReferenceChain;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferencingType;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Choice_Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.JsonAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST.rawAST_single_tag;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.IMemberInfo;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template.Completeness_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template.Template_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.NamedTemplate;
import org.eclipse.titan.lsp.AST.TTCN3.templates.Named_Template_List;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.SubType;
import org.eclipse.titan.lsp.AST.TTCN3.values.Choice_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ExpressionStruct;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 * */
public final class TTCN3_Choice_Type extends TTCN3_Set_Seq_Choice_BaseType implements IMemberInfo {
	private static final String UNSUPPERTED_FIELDNAME =
			"Sorry, but it is not supported for sequence types to have a field with a name (`{0}'') which exactly matches the name of the type definition.";
	private static final String TEMPLATENOTALLOWED = "{0} cannot be used for union type `{1}''";
	private static final String LENGTHRESTRICTIONNOTALLOWED = "Length restriction is not allowed for union type `{0}''";
	private static final String ONEFIELDEXPECTED = "A template for union type must contain exactly one selected field";
	private static final String REFERENCETONONEXISTENTFIELD = "Reference to non-existent field `{0}'' in union template for type `{1}''";
	private static final String CHOICEEXPECTED = "CHOICE value was expected for type `{0}''";
	private static final String UNIONEXPECTED = "Union value was expected for type `{0}''";
	private static final String NONEXISTENTCHOICE = "Reference to a non-existent alternative `{0}'' in CHOICE value for type `{1}''";
	private static final String NONEXISTENTUNION = "Reference to a non-existent field `{0}'' in union value for type `{1}''";

	private static final String NOCOMPATIBLEFIELD = "union/CHOICE type `{0}'' doesn''t have any field compatible with `{1}''";
	private static final String NOTCOMPATIBLEUNION = "union/CHOICE types are compatible only with other union/CHOICE types";

	public TTCN3_Choice_Type(final CompFieldMap compFieldMap) {
		super(compFieldMap);
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetype() {
		return Type_type.TYPE_TTCN3_CHOICE;
	}

	@Override
	/** {@inheritDoc} */
	public SubType.SubType_type getSubtypeType() {
		return SubType.SubType_type.ST_UNION;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isCompatible(final CompilationTimeStamp timestamp, final IType otherType, final TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain) {
		check(timestamp);
		otherType.check(timestamp);
		final IType temp = otherType.getTypeRefdLast(timestamp);

		if (getIsErroneous(timestamp) || temp.getIsErroneous(timestamp) || this == temp) {
			return true;
		}

		if (info == null || noStructuredTypeCompatibility) {
			return this == temp;
		}

		switch (temp.getTypetype()) {
		case TYPE_ASN1_CHOICE: {
			final ASN1_Choice_Type tempType = (ASN1_Choice_Type) temp;
			TypeCompatibilityInfo.Chain lChain = leftChain;
			TypeCompatibilityInfo.Chain rChain = rightChain;
			if (lChain == null) {
				lChain = info.getChain();
				lChain.add(this);
			}
			if (rChain == null) {
				rChain = info.getChain();
				rChain.add(tempType);
			}
			for (int i = 0, size = getNofComponents(); i < size; i++) {
				final CompField cf = getComponentByIndex(i);
				final IType cfType = cf.getType().getTypeRefdLast(timestamp);
				for (int j = 0, size2 = tempType.getNofComponents(); j < size2; j++) {
					final CompField tempComponentField = tempType.getComponentByIndex(j);
					final IType tempTypeCompFieldType = tempComponentField.getType().getTypeRefdLast(timestamp);
					if (!cf.getIdentifier().getDisplayName().equals(tempComponentField.getIdentifier().getDisplayName())) {
						continue;
					}
					lChain.markState();
					rChain.markState();
					lChain.add(cfType);
					rChain.add(tempTypeCompFieldType);
					if (cfType.equals(tempTypeCompFieldType)
							|| (lChain.hasRecursion() && rChain.hasRecursion())
							|| cfType.isCompatible(timestamp, tempTypeCompFieldType, info, lChain, rChain)) {
						info.setNeedsConversion(true);
						lChain.previousState();
						rChain.previousState();
						return true;
					}
					lChain.previousState();
					rChain.previousState();
				}
			}
			info.setErrorStr(MessageFormat.format(NOCOMPATIBLEFIELD, temp.getTypename(), getTypename()));
			return false;
		}
		case TYPE_TTCN3_CHOICE: {
			final TTCN3_Choice_Type tempType = (TTCN3_Choice_Type) temp;
			if (this == tempType) {
				return true;
			}
			TypeCompatibilityInfo.Chain lChain = leftChain;
			TypeCompatibilityInfo.Chain rChain = rightChain;
			if (lChain == null) {
				lChain = info.getChain();
				lChain.add(this);
			}
			if (rChain == null) {
				rChain = info.getChain();
				rChain.add(tempType);
			}
			for (int i = 0, size = getNofComponents(); i < size; i++) {
				final CompField cf = getComponentByIndex(i);
				final IType cfType = cf.getType().getTypeRefdLast(timestamp);
				for (int j = 0, size2 = tempType.getNofComponents(); j < size2; j++) {
					final CompField tempComponentField = tempType.getComponentByIndex(j);
					final IType tempTypeCompFieldType = tempComponentField.getType().getTypeRefdLast(timestamp);
					if (!cf.getIdentifier().getDisplayName().equals(tempComponentField.getIdentifier().getDisplayName())) {
						continue;
					}
					lChain.markState();
					rChain.markState();
					lChain.add(cfType);
					rChain.add(tempTypeCompFieldType);
					if (cfType.equals(tempTypeCompFieldType)
							|| (lChain.hasRecursion() && rChain.hasRecursion())
							|| cfType.isCompatible(timestamp, tempTypeCompFieldType, info, lChain, rChain)) {
						info.setNeedsConversion(true);
						lChain.previousState();
						rChain.previousState();
						return true;
					}
					lChain.previousState();
					rChain.previousState();
				}
			}
			info.setErrorStr(MessageFormat.format(NOCOMPATIBLEFIELD, temp.getTypename(), getTypename()));
			return false;
		}
		case TYPE_ASN1_SEQUENCE:
		case TYPE_TTCN3_SEQUENCE:
		case TYPE_SEQUENCE_OF:
		case TYPE_ARRAY:
		case TYPE_ASN1_SET:
		case TYPE_TTCN3_SET:
		case TYPE_SET_OF:
		case TYPE_ANYTYPE:
			info.setErrorStr(NOTCOMPATIBLEUNION);
			return false;
		default:
			return false;
		}
	}


	@Override
	/** {@inheritDoc} */
	public boolean isIdentical(final CompilationTimeStamp timestamp, final IType type) {
		check(timestamp);
		type.check(timestamp);
		final IType temp = type.getTypeRefdLast(timestamp);

		if (getIsErroneous(timestamp) || temp.getIsErroneous(timestamp)) {
			return true;
		}

		return this == temp;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetypeTtcn3() {
		if (isErroneous) {
			return Type_type.TYPE_UNDEFINED;
		}

		return getTypetype();
	}

	@Override
	/** {@inheritDoc} */
	public void checkConstructorName(final String definitionName) {
		if (hasComponentWithName(definitionName)) {
			final CompField field = getComponentByName(definitionName);
			field.getIdentifier().getLocation().reportSemanticError(MessageFormat.format(UNSUPPERTED_FIELDNAME, field.getIdentifier().getDisplayName()));
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (!referenceChain.add(this) || compFieldMap.isEmpty()) {
			return;
		}

		// FIXME there should be a better way than checking for all possible IReferenceChain implementation
		CachedReferenceChain cachedChain;
		if (referenceChain instanceof CachedReferenceChain) {
			cachedChain = (CachedReferenceChain) referenceChain;
		} else {
			if (!(referenceChain instanceof ReferenceChain)) {
				return;
			}
			cachedChain = ((ReferenceChain) referenceChain).toCachedReferenceChain();
		}

		final Map<String, CompField> map = compFieldMap.getComponentFieldMap(timestamp);
		cachedChain.markErrorState();
		int i = 1;
		for (final CompField compField : map.values()) {
			final IType type = compField.getType();
			if (type != null) {
				cachedChain.markState();
				type.checkRecursions(timestamp, cachedChain);
				cachedChain.previousState();
			}

			if (cachedChain.getNofErrors() < i) {
				break;
			}
			++i;
		}

		if (cachedChain.getNofErrors() == map.size()) {
			cachedChain.reportAllTheErrors();
		}
		cachedChain.prevErrorState();
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisValue(final CompilationTimeStamp timestamp, final IValue value, final Assignment lhs, final ValueCheckingOptions valueCheckingOptions) {
		if (getIsErroneous(timestamp)) {
			return false;
		}

		boolean selfReference = super.checkThisValue(timestamp, value, lhs, valueCheckingOptions);

		IValue last = value.getValueRefdLast(timestamp, valueCheckingOptions.expected_value, null);
		if (last == null || last.getIsErroneous(timestamp)) {
			return selfReference;
		}

		// already handled ones
		switch (value.getValuetype()) {
		case OMIT_VALUE:
		case REFERENCED_VALUE:
			return selfReference;
		case UNDEFINED_LOWERIDENTIFIER_VALUE:
			if (Value_type.REFERENCED_VALUE.equals(last.getValuetype())) {
				return selfReference;
			}
			break;
		default:
			break;
		}

		switch (last.getValuetype()) {
		case SEQUENCE_VALUE:
			if (value.isAsn()) {
				value.getLocation().reportSemanticError(MessageFormat.format(CHOICEEXPECTED, getFullName()));
				value.setIsErroneous(true);
			} else {
				last = last.setValuetype(timestamp, Value_type.CHOICE_VALUE);
				if (!last.getIsErroneous(timestamp)) {
					selfReference = checkThisValueChoice(timestamp, (Choice_Value) last, lhs, valueCheckingOptions.expected_value,
							valueCheckingOptions.incomplete_allowed, valueCheckingOptions.implicit_omit, valueCheckingOptions.str_elem);
				}
			}
			break;
		case CHOICE_VALUE:
			selfReference = checkThisValueChoice(timestamp, (Choice_Value) last, lhs, valueCheckingOptions.expected_value,
					valueCheckingOptions.incomplete_allowed, valueCheckingOptions.implicit_omit, valueCheckingOptions.str_elem);
			break;
		case EXPRESSION_VALUE:
		case MACRO_VALUE:
			// already checked
			break;
		default:
			if (value.isAsn()) {
				value.getLocation().reportSemanticError(MessageFormat.format(CHOICEEXPECTED, getFullName()));
			} else {
				value.getLocation().reportSemanticError(MessageFormat.format(UNIONEXPECTED, getFullName()));
			}
			value.setIsErroneous(true);
			break;
		}

		if (valueCheckingOptions.sub_check) {
			//there is no parent type to check
			if (subType != null) {
				subType.checkThisValue(timestamp, last);
			}
		}

		value.setLastTimeChecked(timestamp);

		return selfReference;
	}

	private boolean checkThisValueChoice(final CompilationTimeStamp timestamp, final Choice_Value value, final Assignment lhs, final Expected_Value_type expectedValue,
			final boolean incompleteAllowed, final boolean implicit_omit, final boolean strElem) {
		boolean selfReference = false;
		final Identifier name = value.getName();
		if (!hasComponentWithName(name.getName())) {
			if (value.isAsn()) {
				value.getLocation().reportSemanticError(MessageFormat.format(NONEXISTENTCHOICE, name.getDisplayName(), getFullName()));
				value.setIsErroneous(true);
			} else {
				value.getLocation().reportSemanticError(MessageFormat.format(NONEXISTENTUNION, name.getDisplayName(), getFullName()));
				value.setIsErroneous(true);
			}
			return selfReference;
		}

		IValue alternativeValue = value.getValue();
		if (alternativeValue == null) {
			return selfReference;
		}

		final Type alternativeType = getComponentByName(name.getName()).getType();
		alternativeValue.setMyGovernor(alternativeType);
		alternativeValue = alternativeType.checkThisValueRef(timestamp, alternativeValue);
		selfReference = alternativeType.checkThisValue(timestamp, alternativeValue, lhs, new ValueCheckingOptions(expectedValue,
				incompleteAllowed, false, true, implicit_omit, strElem));

		value.setLastTimeChecked(timestamp);

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplate(final CompilationTimeStamp timestamp, final ITTCN3Template template,
			final boolean isModified, final boolean implicitOmit, final Assignment lhs) {
		registerUsage(template);
		template.setMyGovernor(this);

		if (getIsErroneous(timestamp)) {
			return false;
		}

		boolean selfReference = false;
		if (Template_type.NAMED_TEMPLATE_LIST.equals(template.getTemplatetype())) {
			final Named_Template_List namedTemplateList = (Named_Template_List) template;
			final int nofTemplates = namedTemplateList.size();
			if (nofTemplates != 1) {
				template.getLocation().reportSemanticError(ONEFIELDEXPECTED);
			}

			for (final NamedTemplate namedTemplate : namedTemplateList) {
				final Identifier name = namedTemplate.getName();

				final CompField field = compFieldMap.getCompWithName(name);
				if (field == null) {
					namedTemplate.getLocation().reportSemanticError(MessageFormat.format(REFERENCETONONEXISTENTFIELD, name.getDisplayName(), getFullName()));
				} else {
					final Type fieldType = field.getType();
					ITTCN3Template namedTemplateTemplate = namedTemplate.getTemplate();

					namedTemplateTemplate.setMyGovernor(fieldType);
					namedTemplateTemplate = fieldType.checkThisTemplateRef(timestamp, namedTemplateTemplate);
					final Completeness_type completeness = namedTemplateList.getCompletenessConditionChoice(timestamp, isModified, name);
					selfReference |= namedTemplateTemplate.checkThisTemplateGeneric(
							timestamp, fieldType, Completeness_type.MAY_INCOMPLETE.equals(completeness), false, false, true, implicitOmit, lhs);
				}
			}
		} else {
			template.getLocation().reportSemanticError(MessageFormat.format(TEMPLATENOTALLOWED, template.getTemplateTypeName(), getTypename()));
		}

		if (template.getLengthRestriction() != null) {
			template.getLocation().reportSemanticError(MessageFormat.format(LENGTHRESTRICTIONNOTALLOWED, getTypename()));
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public void checkCodingAttributes(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		//check raw attributes
		//TODO can unions have length restriction?
		if (rawAttribute != null) {
			//TODO force_raw()
			if (rawAttribute.taglist != null) {
				for (int c = 0; c < rawAttribute.taglist.size(); c++) {
					final rawAST_single_tag singleTag = rawAttribute.taglist.get(c);
					final Identifier fieldname = singleTag.fieldName;
					if (!hasComponentWithName(fieldname.getName())) {
						fieldname.getLocation().reportSemanticError(MessageFormat.format("Invalid field name `{0}'' in RAW parameter TAG for type `{1}''", fieldname.getDisplayName(), getTypename()));
						continue;
					}

					if (singleTag.keyList != null) {
						for (int a = 0; a < singleTag.keyList.size(); a++) {
							final Reference reference = new Reference(null);
							reference.addSubReference(new FieldSubReference(fieldname));
							for (int b = 0; b < singleTag.keyList.get(a).keyField.names.size(); b++) {
								reference.addSubReference(new FieldSubReference(singleTag.keyList.get(a).keyField.names.get(b)));
							}

							final IType t = getFieldType(timestamp, reference, 0, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
							if (t != null) {
								final Value v = singleTag.keyList.get(a).v_value;
								if (v != null) {
									v.setMyScope(getMyScope());
									v.setMyGovernor(t);
									final IValue tempValue = t.checkThisValueRef(timestamp, v);
									t.checkThisValue(timestamp, tempValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, false, false, false, false, false));
								}
							}
						}
					}
				}
			}
			if (rawAttribute.csn1lh) {
				for (int i = 0; i < getNofComponents(); i++) {
					final CompField cField = getComponentByIndex(i);
					final Type fieldType = cField.getType();
					RawAST fieldRawAttribute = fieldType.rawAttribute;
					if (fieldRawAttribute == null) {
						Type t = fieldType;
						while ( t.rawAttribute == null && t instanceof IReferencingType) {
							final IReferenceChain tempRefChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
							t = (Type)((IReferencingType)t).getTypeRefd(timestamp, tempRefChain);
							tempRefChain.release();
						}
						fieldRawAttribute = new RawAST(t.rawAttribute, fieldType.getDefaultRawFieldLength());
						fieldType.setRawAttributes(fieldRawAttribute);
					}

					fieldRawAttribute.csn1lh = true;
				}
			}
		}

		checkJson(timestamp);
		//TODO add checks for other encodings.

		if (refChain.contains(this)) {
			return;
		}

		refChain.add(this);
		refChain.markState();
		for (int i = 0; i < getNofComponents(); i++) {
			final Type cftype = getComponentByIndex(i).getType();
			if (cftype != null) {
				cftype.checkCodingAttributes(timestamp, refChain);
			}
		}
		refChain.previousState();
	}

	@Override
	/** {@inheritDoc} */
	public void forceRaw(final CompilationTimeStamp timestamp) {
		if (rawAttribute == null) {
			rawAttribute = new RawAST(getDefaultRawFieldLength());
		}
	}

	@Override
	/** {@inheritDoc} */
	public int getRawLength(final BuildTimestamp timestamp) {
		if (rawLengthCalculated != null && !rawLengthCalculated.isLess(timestamp)) {
			return rawLength;
		}

		rawLengthCalculated = timestamp;
		rawLength = 0;
		for (int i = 0; i < getNofComponents(); i++) {
			final CompField cf = getComponentByIndex(i);

			final Type t = cf.getType();
			final int l = t.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp()).getRawLength(timestamp);
			if (l == -1) {
				rawLength = -1;
				return rawLength;
			}

			if (i == 0) {
				rawLength = l;
			} else {
				if (rawLength != l) {
					rawLength = -1;
					return rawLength;
				}
			}
		}

		return rawLength;
	}

	@Override
	/** {@inheritDoc} */
	public void forceJson(final CompilationTimeStamp timestamp) {
		if (jsonAttribute == null) {
			jsonAttribute = new JsonAST();
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkJson(final CompilationTimeStamp timestamp) {
		if (jsonAttribute == null && !hasEncodeAttribute(MessageEncoding_type.JSON)) {
			return;
		}

		for (int i = 0; i < getNofComponents(); i++) {
			final Type fieldType = getComponentByIndex(i).getType();
			if (fieldType != null) {
				fieldType.forceJson(timestamp);
			}
		}

		if (jsonAttribute == null) {
			return;
		}

		if (jsonAttribute.omit_as_null && !isOptionalField()) {
			getLocation().reportSemanticError("Invalid attribute, 'omit as null' requires optional field of a record or set.");
		}

		if (jsonAttribute.alias != null) {
			final IType parent = getParentType();
			if (parent == null) {
				// only report this error when using the new codec handling, otherwise
				// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
				getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
			}

			if (parent != null && parent.getJsonAttribute() != null && parent.getJsonAttribute().as_value) {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					// parent_type_name remains null if the 'as value' attribute is set for an invalid type
					getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					break;
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
					if (((TTCN3_Set_Seq_Choice_BaseType)parent).getNofComponents() == 1) {
						// parent_type_name remains null if the 'as value' attribute is set for an invalid type
						getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					}
					break;
				default:
					break;
				}
			}
		}

		if (jsonAttribute.parsed_default_value != null) {
			checkJsonDefault(timestamp);
		}

		//TODO: check schema extensions 

		if (jsonAttribute.metainfo_unbound) {
			if (getParentType() == null || (getParentType().getTypetype() != Type_type.TYPE_TTCN3_SEQUENCE &&
					getParentType().getTypetype() != Type_type.TYPE_TTCN3_SET)) {
				// only allowed if it's an array type or a field of a record/set
				getLocation().reportSemanticError("Invalid attribute 'metainfo for unbound', requires record, set, record of, set of, array or field of a record or set");
			}
		}

		if (jsonAttribute.as_number) {
			getLocation().reportSemanticError("Invalid attribute, 'as number' is only allowed for enumerated types");
		}

		//FIXME: check tag_list

		if (jsonAttribute.as_map) {
			getLocation().reportSemanticError("Invalid attribute, 'as map' requires record of or set of");
		}

		if (!jsonAttribute.enum_texts.isEmpty()) {
			getLocation().reportSemanticError("Invalid attribute, 'text ... as ...' requires an enumerated type");
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isPresentAnyvalueEmbeddedField(final ExpressionStruct expression, final List<ISubReference> subreferences, final int beginIndex) {
		if (subreferences == null || getIsErroneous(CompilationTimeStamp.getBaseTimestamp())) {
			return true;
		}

		if (beginIndex >= subreferences.size()) {
			return true;
		}

		return false;
	}

	@Override
	public void addMembersContent(Ttcn3HoverContent content) {
		if (getNofComponents() > 0) {
			content.addTag("Members").addText("\n\n");
			for (int i = 0; i < getNofComponents(); i++) {
				final CompField field = getComponentByIndex(i);
				final IType memberType = field.getType();
				final StringBuilder sb = new StringBuilder(field.getIdentifier().getDisplayName());

				if (memberType != null) {
					String memberTypeName = memberType.getTypename();
					if (memberType instanceof Referenced_Type) {
						memberTypeName = ((Referenced_Type)memberType).getReference().getDisplayName();
					}
					sb.append(" (").append(memberTypeName).append(")");
				}

				content.addText("- ").addIndentedText(sb.toString(), "\n");
			}
		}
	}

	@Override
	public String getDefaultSnippet(String lineEnding, int indentation, AtomicInteger placeholderIdx) {
		final StringBuilder builder = new StringBuilder("{");
		final int nofComponents = getNofComponents();
		if (nofComponents == 0) {
			builder.append("}");
			return builder.toString();
		}

		indentation++;

		final CompField field = getComponentByIndex(0);
		final String fieldName = field.getIdentifier().getDisplayName();
		// TODO: default value
		String fieldValue = field.getDefault() != null ? field.getDefault().getFullName() : field.getType().getDefaultSnippet(lineEnding, indentation, placeholderIdx);

		builder
		.append(lineEnding)
		.append("\t".repeat(indentation))
		.append(fieldName + " := ")
		.append(fieldValue)
		.append(lineEnding)
		.append("\t".repeat(Math.max(0, indentation - 1)))
		.append("}");

		return builder.toString();
	}
}
