/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Farkas Izabella Ingrid
 * @author Arpad Lovassy
 * */
public final class EnumeratedGenerator {

	private static final String UNKNOWN_VALUE = "UNKNOWN_VALUE";
	private static final String UNBOUND_VALUE ="UNBOUND_VALUE";

	public static class Enum_field {
		private final String name;
		private final String displayName;

		private final long value;

		public Enum_field(final String aName, final String aDisplayName, final long aValue) {
			name = aName;
			displayName = aDisplayName;
			value = aValue;
		}
	}

	public static class Enum_Defs {
		private final List<Enum_field> items;
		private final String name;
		private final String displayName;
		private final String templateName;
		private final boolean hasRaw;
		private final boolean hasJson;
		private long firstUnused = -1;  //first unused value for this enum type
		private long secondUnused = -1; //second unused value for this enum type

		public Enum_Defs(final List<Enum_field> aItems, final String aName, final String aDisplayName, final String aTemplateName, final boolean aHasRaw, final boolean aHasJson){
			items = aItems;
			name = aName;
			displayName = aDisplayName;
			templateName = aTemplateName;
			hasRaw = aHasRaw;
			hasJson = aHasJson;
			calculateFirstAndSecondUnusedValues();
		}

		//This function supposes that the enum class is already checked and error free
		private void calculateFirstAndSecondUnusedValues() {
			if( firstUnused != -1 ) {
				return; //function already have been called
			}
			final Map<Long, Enum_field> valueMap = new HashMap<Long, Enum_field>(items.size());

			for(final Enum_field item : items) {
				valueMap.put(item.value, item);
			}

			long localFirstUnused = 0L;
			while (valueMap.containsKey(localFirstUnused)) {
				localFirstUnused++;
			}

			firstUnused = localFirstUnused;
			localFirstUnused++;
			while (valueMap.containsKey(localFirstUnused)) {
				localFirstUnused++;
			}
			secondUnused = localFirstUnused;
			valueMap.clear();
		}
	}

	private EnumeratedGenerator() {
		// private to disable instantiation
	}
}
