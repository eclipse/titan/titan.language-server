/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3DocCommentLexer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3DocCommentParser;

/**
 * A helper class for documentation comment handling
 * 
 * ETSI ES 201 873-10 V4.5.1 (2013-04)
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public class DocumentComment implements ILocatableComment {
	public enum CommentTag {
		Author, Config, Desc, Exception, Member, Param, Priority,
		Purpose, Remark, Reference, Requirement, Return, See,
		Since, Status, Url, Verdict, Version
	}

	private static final String SINGLE_TAG_DUPLICATED_ERROR = "`{0}'' tag is specified more than once";
	private static final String SAME_TAG_VALUE_DUPLICATED_ERROR = "The {0} `{1}'' is specified more than once, using the latest";
	private static final String TAG_NOT_APPLICABLE = "The `{0}'' tag is not applicable for {1}";
	private static final String IDPATTERN = "^[A-Za-z][A-Za-z0-9_]*$";

	/** Raw text of the comment */
	private String comment;

	/** First and last tokens of the comment */
	private Token first;
	private Token last;

	/** Location of the whole document comment. */
	private Location location;

	private final List<String> authors = new ArrayList<String>();
	private final List<Location> authorLocations = new ArrayList<>();
	private String config = null;
	private Location configLocation;
	private final List<String> descs = new ArrayList<String>();
	private final List<Location> descLocations = new ArrayList<>();
	private final Map<String,String> exceptions = new LinkedHashMap<>();
	private final Map<String,Location> exceptionLocations = new LinkedHashMap<>();
	private final Map<String,String> members = new LinkedHashMap<>();
	private final Map<String,Location> memberLocations = new LinkedHashMap<>();
	private final Map<String,Location> memberIdLocations = new LinkedHashMap<>();
	private final Map<String,String> params = new LinkedHashMap<>();
	private final Map<String,Location> paramLocations = new LinkedHashMap<>();
	private final Map<String,Location> paramIdLocations = new LinkedHashMap<>();
	private String priority = null;
	private Location priorityLocation;
	private String purpose = null;
	private Location purposeLocation;
	private String reference = null;
	private final List<String> remarks = new ArrayList<String>();
	private final List<Location> remarkLocations = new ArrayList<>();
	private final List<String> requirements = new ArrayList<String>();
	private final List<Location> requirementLocations = new ArrayList<>();
	private String return_ = null;
	private Location returnLocation;
	private final List<String> sees = new ArrayList<String>();
	private String since = null;
	private String status = null;
	private final List<String> urls = new ArrayList<String>();
	private final Map<String,String> verdicts = new LinkedHashMap<String, String>();
	private final List<Location> verdictLocations = new ArrayList<>();
	private String version = null;
	private boolean isDeprecated = false;
	private ICommentable owner;
	private String category = null;

	public DocumentComment(final String comment, final Location location, final Token first, final Token last) {
		this.comment = comment;
		this.location = location;
		this.first = first;
		this.last = last;
		parseComment();
	}

	/**
	 * Returns whether the status tag contains the "deprecated" word 
	 * @return whether the status tag contains the "deprecated" word
	 */
	public boolean isDeprecated() {
		return isDeprecated;
	}

	public Token getFirstToken() {
		return first;
	}

	public Token getLastToken() {
		return last;
	}

	@Override
	public void setCommentLocation(final Location location) {
		this.location = location; 
	}

	@Override
	public Location getCommentLocation() {
		return location;
	}

	/**
	 * Adds author(s)
	 * @param text Author(s)
	 * @param location Location of the document comment tag
	 */
	public void addAuthor(final String text, Location location) {
		if (text != null) {
			authors.add(text.trim());
		}
	}

	/**
	 * Adds authors' content to hover info
	 * @param content Content of hover info
	 */
	public void addAuthorsContent(Ttcn3HoverContent content) {
		if (hasAuthors()) {
			content.addTag("\n#### Authors: ");
			for (String author : authors) {
				content.addText("\n - " + author);
			}
			content.addText("\n");
		}
	}

	/**
	 * Returns the list of authors
	 * @return List of authors
	 */
	public List<String> getAuthors() {
		return authors;
	}
	
	/**
	 * Returns the location of the @author tag
	 * @return
	 */
	public List<Location> getAuthorLocations() {
		return authorLocations;
	}

	/**
	 * Returns whether {@code @author} tag is specified
	 * @return {@code true} if {@code @author} tag is specified, {@code false} otherwise
	 */
	public boolean hasAuthors() {
		return !authors.isEmpty();
	}

	/**
	 * Adds the test configuration's description 
	 * @param text Description of the test configuration
	 * @param location Location of the document comment tag
	 */
	public void addConfig(final String text, Location location) {
		if (config == null) {
			if (text != null) {
				config = text.trim();
				configLocation = location;
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.CONFIG_TAG), location);
		}
	}

	/**
	 * Adds the configuration's content to hover info
	 * @param content Content of hover info
	 */
	public void addConfigContent(Ttcn3HoverContent content) {
		if (config == null || config.isEmpty()) {
			return;
		}
		content.addTag("Configuration:");
		content.addIndentedText(config);
	}

	/**
	 * Returns the configuration's description. If it is not specified, empty string is returned.
	 * @return Description of the configuration
	 */
	public String getConfig() {
		return config != null ? config : "";
	}
	
	/**
	 * Returns the location of the @config tag
	 * @return
	 */
	public Location getConfigLocation() {
		return configLocation;
	}

	/**
	 * Returns whether {@code @config} tag is specified
	 * @return {@code true} if {@code @config} tag is specified, {@code false} otherwise
	 */
	public boolean hasConfig() {
		return config != null;
	}

	/**
	 * Adds description(s)
	 * @param text Description(s)
	 * @param location Location of the document comment tag
	 */
	public void addDesc(final String text, Location location) {
		if (text != null) {
			descs.add(text.trim());
			descLocations.add(location);
		}
	}

	/**
	 * Adds descriptions' content to hover info
	 * @param content Content of hover info
	 */
	public void addDescsContent(Ttcn3HoverContent content) {
		if (hasDescs()) {
			content.addText("\n\n#### ");
			for (String desc : descs) {
				content.addText(desc).addText(" ");
			}
		}
	}

	/**
	 * Returns the list of descriptions
	 * @return List of descriptions
	 */
	public List<String> getDescs() {
		return descs;
	}
	
	/**
	 * Returns the locations of the <code>@desc</code> tags.
	 * @return
	 */
	public List<Location> getDescLocations() {
		return descLocations;
	}

	/**
	 * Returns whether {@code @desc} tag is specified
	 * @return {@code true} if {@code @desc} tag is specified, {@code false} otherwise
	 */
	public boolean hasDescs() {
		return !descs.isEmpty();
	}

	/**
	 * Adds description to the specified exception. The text can be {@code null}.
	 * @param id Exception to describe
	 * @param text Description of the exception
	 * @param location Location of the document comment tag
	 */
	public void addException(final String id, final String text, Location location) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim();
		final String prev = exceptions.put(trimmedId, text != null ? text.trim() : "");
		// This key is already present in the map, i.e. this exception is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format(SAME_TAG_VALUE_DUPLICATED_ERROR, ICommentable.EXCEPTION_TAG, trimmedId), location);
		}
		exceptionLocations.put(trimmedId, location);
	}

	/**
	 * Adds exceptions' content to hover info
	 * @param content Content of hover info
	 */
	public void addExceptionsContent(Ttcn3HoverContent content) {
		if (hasExceptions()) {
			content.addTag("Exceptions");
			for (Map.Entry<String, String> exception : exceptions.entrySet()) {
				content.addIndentedText(exception.getKey(), exception.getValue());
			}
		}
	}

	/**
	 * Returns the map of exceptions' descriptions
	 * @return Map of exceptions' descriptions
	 */
	public Map<String, String> getExceptions() {
		return exceptions;
	}

	/**
	 * Returns the locations of the <code>@exception</code> tags.
	 * @return
	 */
	public Map<String, Location> getExceptionLocations() {
		return exceptionLocations;
	}
	
	/**
	 * Returns whether {@code @exception} tag is specified
	 * @return {@code true} if {@code @exception} tag is specified, {@code false} otherwise
	 */
	public boolean hasExceptions() {
		return !exceptions.isEmpty();
	}

	/**
	 * Adds description to the specified member. The text can be {@code null}.
	 * @param id Member to describe
	 * @param text Description of the member
	 * @param location Location of the document comment tag
	 */
	public void addMember(final String id, final String text, Location location, Location idLocation) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim();
		final String prev = members.put(trimmedId, text != null ? text.trim() : "");
		memberLocations.put(trimmedId, location);
		memberIdLocations.put(trimmedId, idLocation);
		// This key is already present in the map, i.e. this member is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format(SAME_TAG_VALUE_DUPLICATED_ERROR, ICommentable.MEMBER_TAG, trimmedId), location);
		}
	}

	/**
	 * Adds members' content to hover info
	 * @param content Content of hover info
	 */
	public void addMembersContent(Ttcn3HoverContent content) {
		if (hasMembers()) {
			content.addTag("#### Members\n\n");
			for (Map.Entry<String, String> member : members.entrySet()) {
				content.addText(" - " + member.getKey() + ": " + member.getValue() + "\n");
			}
		}
	}
	
	/**
	 * Returns the map of object members
	 * @return Map of object members
	 */
	public Map<String, String> getMembers() {
		return members;
	}
	
	/**
	 * Returns the locations of member identifiers of the <code>@member</code> tags.
	 * @return
	 */
	public Map<String, Location> getMemberIdLocations() {
		return memberIdLocations;
	}
	
	/**
	 * Returns the locations of the <code>@member</code> tags.
	 * @return
	 */
	public Map<String, Location> getMemberLocations() {
		return memberLocations;
	}
	
	/**
	 * Returns the comment for the given member
	 * @param memberName
	 * @return
	 */
	public String getMember(String memberName) {
		return members.get(memberName);
	}

	/**
	 * Returns whether {@code @member} tag is specified
	 * @return {@code true} if {@code @member} tag is specified, {@code false} otherwise
	 */
	public boolean hasMembers() {
		return !members.isEmpty();
	}

	/**
	 * Adds description to the specified parameter. The text can be {@code null}.
	 * @param id Parameter to describe
	 * @param text Description of the parameter
	 * @param location Location of the document comment tag
	 */
	public void addParam(final String id, final String text, Location location, Location idLocation) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim();
		paramLocations.put(trimmedId, location);
		paramIdLocations.put(trimmedId, idLocation);
		final String prev = params.put(trimmedId, text != null ? text.trim() : "");
		// This key is already present in the map, i.e. this parameter is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format(SAME_TAG_VALUE_DUPLICATED_ERROR, ICommentable.PARAM_TAG, trimmedId), location);
		}
	}

	/**
	 * Adds parameters' content to hover info
	 * @param content Content of hover info
	 */
	public void addParamsContent(Ttcn3HoverContent content, final FormalParameterList fpl) {
		if (hasParams()) {
			content.addTag("\n\n#### Parameters: \n\n");
			String paramType = null;
			String argTypeName = null;
			for (Map.Entry<String, String> param : params.entrySet()) {
				final Location loc = paramLocations.get(param.getKey());
				FormalParameter fp = null;
				if (fpl != null) {
					fp = fpl.get(new Identifier(Identifier_type.ID_TTCN, param.getKey()));
				}
				if (fp != null) {
					paramType = fp.getFormalParamType();
					final Type argType = fp.getType(CompilationTimeStamp.getBaseTimestamp());
					argTypeName = argType != null ? argType.getTypename() : "<?>";
					if (argType instanceof Referenced_Type) {
						argTypeName = ((Referenced_Type) argType).getReference().getDisplayName();
					}
				} else {
					reportProblem(MessageFormat.format(
							"The formal parameter `{0}'' in the comment is not part of the definition", param.getKey()), loc);
				}
				final StringBuilder sb = new StringBuilder();
				sb
				.append(param.getKey())
				.append(" (")
				.append(paramType != null ? paramType : "<?>")
				.append(") ")
				.append(argTypeName)
				.append("\n\t")
				.append(param.getValue());
				content.addText("- ").addIndentedText(sb.toString(), "\n");
			}
			if (fpl.size() != params.size()) {
				reportProblem("The number of formal parameters in the comment is not the same as of the definition", location);
			}
			content.addText("\n\n");
		}
	}

	/**
	 * Returns the map of parameters
	 * @return Map of parameters
	 */
	public Map<String, String> getParams() {
		return params;
	}
	
	/**
	 * Returns the locations of parameter identifiers of the <code>@param</code> tags.
	 * @return
	 */
	public Map<String, Location> getParamIdLocations() {
		return paramIdLocations;
	}
	
	/**
	 * Returns the locations of the <code>@param</code> tags.
	 * @return
	 */
	public Map<String, Location> getParamLocations() {
		return paramLocations;
	}

	/**
	 * Returns whether {@code @param} tag is specified
	 * @return {@code true} if {@code @param} tag is specified, {@code false} otherwise
	 */
	public boolean hasParams() {
		return !params.isEmpty();
	}

	/**
	 * Adds the priority category of the test case
	 * @param text Priority category of the test case
	 * @param location Location of the document comment tag
	 */
	public void addPriority(final String text, Location location) {
		if (priority == null) {
			if (text != null) {
				priority = text.trim();
				priorityLocation = location;
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.PRIORITY_TAG), location);
		}
	}

	/**
	 * Adds the priority content to hover info
	 * @param content Content of hover info
	 */
	public void addPriorityContent(Ttcn3HoverContent content) {
		if (priority == null || priority.isEmpty()) {
			return;
		}
		content.addTagWithText("Priority:", priority);
	}

	/**
	 * Returns the priority category of the test case. If it is not specified, empty string is returned.
	 * @return Priority category of the test case
	 */
	public String getPriority() {
		return priority != null ? priority : "";
	}

	/**
	 * Returns the location of the @priority tag
	 * @return
	 */
	public Location getPriorityLocation() {
		return priorityLocation;
	}
	
	/**
	 * Returns whether {@code @priority} tag is specified
	 * @return {@code true} if {@code @priority} tag is specified, {@code false} otherwise
	 */
	public boolean hasPriority() {
		return priority != null;
	}

	/**
	 * Adds description to the {@code @purpose} tag
	 * @param text Description of the {@code @purpose} tag
	 * @param location Location of the document comment tag
	 */
	public void addPurpose(final String text, Location location) {
		if (purpose == null) {
			if (text != null) {
				purpose = text.trim();
				purposeLocation = location;
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.PURPOSE_TAG), location);
		}
	}

	/**
	 * Adds the purpose content to hover info
	 * @param content Content of hover info
	 */
	public void addPurposeContent(Ttcn3HoverContent content) {
		if (purpose == null || purpose.isEmpty()) {
			return;
		}
		content.addTagWithText("Purpose:", purpose);
	}

	/**
	 * Returns the purpose. If it is not specified, empty string is returned.
	 * @return Purpose
	 */
	public String getPurpose() {
		return purpose != null ? purpose : "";
	}
	
	/**
	 * Returns the location of the @purpose tag
	 * @return
	 */
	public Location getPurposeLocation() {
		return purposeLocation;
	}

	/**
	 * Returns whether {@code @purpose} tag is specified
	 * @return {@code true} if {@code @purpose} tag is specified, {@code false} otherwise
	 */
	public boolean hasPurpose() {
		return purpose != null;
	}

	/**
	 * Adds reference
	 * @param text Reference
	 * @param location Location of the document comment tag
	 */
	public void addReference(final String text, Location location) {
		if (reference == null) {
			if (text != null) {
				reference = text.trim();
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.REFERENCE_TAG), location);
		}
	}

	/**
	 * Adds the reference content to hover info
	 * @param content Content of hover info
	 */
	public void addReferenceContent(Ttcn3HoverContent content) {
		if (reference == null || reference.isEmpty()) {
			return;
		}
		content.addTagWithText("Reference:", reference);
	}

	/**
	 * Returns the reference. If it is not specified, empty string is returned.
	 * @return Reference
	 */
	public String getReference() {
		return reference != null ? reference : "";
	}

	/**
	 * Returns whether {@code @reference} tag is specified
	 * @return {@code true} if {@code @reference} tag is specified, {@code false} otherwise
	 */
	public boolean hasReference() {
		return reference != null;
	}

	/**
	 * Adds description to the {@code @remark} tag
	 * @param text Description of the {@code @remark} tag
	 * @param location Location of the document comment tag
	 */
	public void addRemark(final String text, Location location) {
		if (text != null) {
			remarks.add(text.trim());
		}
	}

	/**
	 * Adds remarks' content to hover info
	 * @param content Content of hover info
	 */
	public void addRemarksContent(Ttcn3HoverContent content) {
		if (hasRemarks()) {
			content.addTag("Remarks");
			for (String remark : remarks) {
				content.addText(remark);
			}
		}
	}

	/**
	 * Returns the list of remarks
	 * @return List of remarks
	 */
	public List<String> getRemarks() {
		return remarks;
	}
	
	/**
	 * Returns the locations of the <code>@remark</code> tags.
	 * @return
	 */
	public List<Location> getRemarkLocations() {
		return remarkLocations;
	}

	/**
	 * Returns whether {@code @remark} tag is specified
	 * @return {@code true} if {@code @remark} tag is specified, {@code false} otherwise
	 */
	public boolean hasRemarks() {
		return !remarks.isEmpty();
	}

	/**
	 * Adds requirement to the test case
	 * @param text Requirement of the test case
	 * @param location Location of the document comment tag
	 */
	public void addRequirement(final String text, Location location) {
		if (text != null) {
			requirements.add(text.trim());
			requirementLocations.add(location);
		}
	}

	/**
	 * Adds requirements' content to hover info
	 * @param content Content of hover info
	 */
	public void addRequirementsContent(Ttcn3HoverContent content) {
		if (hasRequirements()) {
			content.addTag("Requirements");
			for (String requirement : requirements) {
				content.addIndentedText(requirement);
			}
		}
	}

	/**
	 * Returns the list of requirements
	 * @return List of requirements
	 */
	public List<String> getRequirements() {
		return requirements;
	}
	
	/**
	 * Returns the location of the @requirement tags
	 * @return
	 */
	public List<Location> getRequirementLocations() {
		return requirementLocations;
	}

	/**
	 * Returns whether {@code @requirement} tag is specified
	 * @return {@code true} if {@code @requirement} tag is specified, {@code false} otherwise
	 */
	public boolean hasRequirements() {
		return !requirements.isEmpty();
	}

	/**
	 * Adds description to the return statement
	 * @param text Description of the return statement
	 * @param location Location of the document comment tag
	 */
	public void addReturn(final String text, Location location) {
		if (return_ == null) {
			if (text != null) {
				return_ = text.trim();
				returnLocation = location;
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.RETURN_TAG), location);
		}
	}

	/**
	 * Adds return statement's content to hover info
	 * @param content Content of hover info
	 */
	public void addReturnContent(Ttcn3HoverContent content) {
		if (return_ == null || return_.isEmpty()) {
			return;
		}
		content.addTag("**Returns** ");
		content.addIndentedText(return_);
	}

	/**
	 * Returns the returns statement's description. If it is not specified, empty string is returned.
	 * @return Description of the return statement
	 */
	public String getReturn() {
		return return_ != null ? return_ : "";
	}
	
	/**
	 * Returns the location of the @return tag
	 * @return
	 */
	public Location getReturnLocation() {
		return returnLocation;
	}

	/**
	 * Returns whether {@code @return} tag is specified
	 * @return {@code true} if {@code @return} tag is specified, {@code false} otherwise
	 */
	public boolean hasReturn() {
		return return_ != null;
	}

	/**
	 * Adds the identifier of {@code @see} tag
	 * @param text Identifier
	 * @param location Location of the document comment tag
	 */
	public void addSee(final String text, Location location) {
		if (text != null) {
			sees.add(text.trim());
		}
	}

	/**
	 * Adds content of {@code @see} tags' identifiers to hover info
	 * @param content Content of hover info
	 */
	public void addSeesContent(Ttcn3HoverContent content) {
		if (hasSees()) {
			content.addTag("See: ");
			for (String see : sees) {
				content.addIndentedText(see);
			}
		}
	}

	/**
	 * Returns the list of {@code @see} tags' identifiers
	 * @return List of {@code @see} tags' identifiers
	 */
	public List<String> getSees() {
		return sees;
	}

	/**
	 * Returns whether {@code @see} tag is specified
	 * @return {@code true} if {@code @see} tag is specified, {@code false} otherwise
	 */
	public boolean hasSees() {
		return !sees.isEmpty();
	}

	/**
	 * Adds description to the {@code @since} tag
	 * @param text Description of the {@code @since} tag
	 * @param location Location of the document comment tag
	 */
	public void addSince(final String text, Location location) {
		if (since == null) {
			if (text != null) {
				since = text.trim();
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.SINCE_TAG), location);
		}
	}

	/**
	 * Adds {@code @since} tag's content to hover info
	 * @param content Content of hover info
	 */
	public void addSinceContent(Ttcn3HoverContent content) {
		if (since == null || since.isEmpty()) {
			return;
		}
		content.addTagWithText("Since: ", since);
	}

	/**
	 * Returns {@code @since} tag's description. If it is not specified, empty string is returned.
	 * @return Description of the {@code @since} tag
	 */
	public String getSince() {
		return since != null ? since : "";
	}

	/**
	 * Returns whether {@code @since} tag is specified
	 * @return {@code true} if {@code @since} tag is specified, {@code false} otherwise
	 */
	public boolean hasSince() {
		return since != null;
	}

	/**
	 * Adds description to the {@code @status} tag
	 * @param text Description of the {@code @status} tag
	 * @param location Location of the document comment tag
	 */
	public void addStatus(final String text, Location location) {
		if (status == null) {
			if (text != null) {
				status = text.trim();
				isDeprecated = status.toLowerCase().contains("deprecated");
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.STATUS_TAG), location);
		}
	}

	/**
	 * Adds {@code @status} tag's content to hover info
	 * @param content Content of hover info
	 */
	public void addStatusContent(Ttcn3HoverContent content) {
		if (status == null || status.isEmpty()) {
			return;
		}
		content.addTagWithText("Status: ", status);
	}

	/**
	 * Returns {@code @status} tag's description. If it is not specified, empty string is returned.
	 * @return Description of the {@code @status} tag
	 */
	public String getStatus() {
		return status != null ? status : "";
	}

	/**
	 * Returns whether {@code @status} tag is specified
	 * @return {@code true} if {@code @status} tag is specified, {@code false} otherwise
	 */
	public boolean hasStatus() {
		return status != null;
	}

	/**
	 * Adds the text of {@code @url} tag
	 * @param text URL
	 * @param location Location of the document comment tag
	 */
	public void addUrl(final String text, Location location) {
		if (text != null) {
			urls.add(text.trim());
		}
	}

	/**
	 * Adds content of URLs to hover info
	 * @param content Content of hover info
	 */
	public void addUrlsContent(Ttcn3HoverContent content) {
		if (hasUrls()) {
			content.addTag("#### URL: ");
			for (String url : urls) {
				content.addUrlAsLink(url);
			}
		}
	}

	/**
	 * Returns the list of URLs
	 * @return List of URLs
	 */
	public List<String> getUrls() {
		return urls;
	}

	/**
	 * Returns whether {@code @url} tag is specified
	 * @return {@code true} if {@code @url} tag is specified, {@code false} otherwise
	 */
	public boolean hasUrls() {
		return !urls.isEmpty();
	}

	/**
	 * Adds description to the specified verdict type. The text can be {@code null}.
	 * @param id Verdict type to describe
	 * @param text Description of the verdict type
	 * @param location Location of the document comment tag
	 */
	public void addVerdict(final String id, final String text, Location location) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim().toLowerCase();
		// Check the verdict type for 'pass', 'fail', 'inconc'
		if (!trimmedId.equals("pass") && !trimmedId.equals("fail") && !trimmedId.equals("inconc")) {
			reportProblem(MessageFormat.format("The verdict `{0}'' is not allowed.\n"
					+ "Allowed verdicts are: `pass', `fail', `inconc'", trimmedId), location);
			return;
		}
		final String prev = verdicts.put(trimmedId, text != null ? text.trim() : "");
		verdictLocations.add(location);
		// This key is already present in the map, i.e. this exception is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format(SAME_TAG_VALUE_DUPLICATED_ERROR, ICommentable.VERDICT_TAG, trimmedId), location);
		}
	}

	/**
	 * Adds verdicts' content to hover info
	 * @param content Content of hover info
	 */
	public void addVerdictsContent(Ttcn3HoverContent content) {
		if (hasVerdicts()) {
			content.addTag("#### Verdicts:\n");
			for (Map.Entry<String, String> verdict : verdicts.entrySet()) {
				content.addText("- ")
				.addIndentedText(verdict.getKey(), verdict.getValue())
				.addText("\n");
			}
		}
	}

	/**
	 * Returns the map of verdicts' descriptions
	 * @return Map of verdicts' descriptions
	 */
	public Map<String, String> getVerdicts() {
		return verdicts;
	}
	
	/**
	 * Returns the location of the @verdict tags
	 * @return
	 */
	public List<Location> getVerdictLocations() {
		return verdictLocations;
	}

	/**
	 * Returns whether {@code @verdict} tag is specified
	 * @return {@code true} if {@code @verdict} tag is specified, {@code false} otherwise
	 */
	public boolean hasVerdicts() {
		return !verdicts.isEmpty();
	}

	/**
	 * Adds the version
	 * @param text Version
	 * @param location Location of the document comment tag
	 */
	public void addVersion(final String text, Location location) {
		if (version == null) {
			if (text != null) {
				version = text.trim();
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.VERSION_TAG), location);
		}
	}

	/**
	 * Adds the version's content to hover info
	 * @param content Content of hover info
	 */
	public void addVersionContent(Ttcn3HoverContent content) {
		if (version == null || version.isEmpty()) {
			return;
		}
		content.addTagWithText("Version:", version);
	}

	/**
	 * Returns the version. If it is not specified, empty string is returned.
	 * @return Version
	 */
	public String getVersion() {
		return version != null ? version : "";
	}

	/**
	 * Returns whether {@code @version} tag is specified
	 * @return {@code true} if {@code @version} tag is specified, {@code false} otherwise
	 */
	public boolean hasVersion() {
		return version != null;
	}

	// non-standard tags

	/**
	 * Adds the category
	 * @param text Category of the given element (arbitrary text)
	 * @param location Location of the document comment tag
	 */
	public void addCategory(final String text, Location location) {
		if (category == null) {
			if (text != null) {
				category = text.trim();
			}
		} else {
			reportProblem(MessageFormat.format(SINGLE_TAG_DUPLICATED_ERROR, ICommentable.CATEGORY_TAG), location);
		}
	}
	
	/**
	 * Returns whether {@code @category} tag is specified
	 * @return {@code true} if {@code @category} tag is specified, {@code false} otherwise
	 */
	public boolean hasCategory() {
		return category != null;
	}
	
	/**
	 * Returns the category. If it is not specified, empty string is returned.
	 * @return Category text
	 */
	public String getCategory() {
		return category != null ? category : "";
	}

	/**
	 * Parses the comment section
	 */
	public void parseComment() {
		final Reader reader = new StringReader(comment);
		final CharStream charStream = new UnbufferedCharStream(reader);
		final Ttcn3DocCommentLexer lexer = new Ttcn3DocCommentLexer(charStream);
		lexer.setTokenFactory(new CommonTokenFactory(true));
		lexer.removeErrorListeners();
		lexer.addErrorListener(new ErrorListenerWithFileName());
		final File file = getCommentLocation().getFile();
		if (file instanceof File) {
			lexer.setActualFile(file);
		}
		
		// Pls. do not remove this comment, the below line is kept for later debugging
		//List<? extends Token> tokens = lexer.getAllTokens();
		
		final CommonTokenStream cts = new CommonTokenStream(lexer);
 		final Ttcn3DocCommentParser parser = new Ttcn3DocCommentParser(cts);
 		parser.setFile(file);
 		parser.setCommentLocation(location);
 		parser.removeErrorListeners();
 		parser.addErrorListener(new ErrorListenerWithFileName());
		parser.pr_DocComment(this);
	}
	
	/**
	 * Helper function for reporting semantic errors/warning according to the related preference 
	 * @param reason the reason of the problem
	 * @param location location of the document comment tag
	 * @see PreferenceConstants#REPORT_DOC_COMMENT_INCONSISTENCY
	 */
	private void reportProblem(final String reason, Location location) {
		final String option = Configuration.INSTANCE.getString(Configuration.REPORT_DOC_COMMENT_INCONSISTENCY, GeneralConstants.WARNING);
		location.reportConfigurableSemanticProblem(option, reason);
	}

	public ICommentable getOwner() {
		return owner;
	}

	public void setOwner(ICommentable owner) {
		this.owner = owner;
	}
	
	public void checkNonApplicableTags(final Set<CommentTag> tags, final String nodeName) {
		if (tags.contains(CommentTag.Author) && hasAuthors()) {
			getAuthorLocations().forEach(loc ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.AUTHOR_TAG, nodeName), loc)
			);
		}
		if (tags.contains(CommentTag.Config) && hasConfig()) {
			reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.CONFIG_TAG, nodeName), getConfigLocation());
		}
		if (tags.contains(CommentTag.Desc) && hasDescs()) {
			getDescLocations().forEach(loc ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.DESC_TAG, nodeName), loc)
			);
		}
		if (tags.contains(CommentTag.Exception) && hasExceptions()) {
			getExceptionLocations().forEach((e, loc) ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.EXCEPTION_TAG, nodeName), loc)
			);
		}
		if (tags.contains(CommentTag.Member) && hasMembers()) {
			getMemberLocations().forEach((m, loc) ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.MEMBER_TAG, nodeName), loc)
			);
		}
		if (tags.contains(CommentTag.Param) && hasParams()) {
			getParamLocations().forEach((p, loc) ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.PARAM_TAG, nodeName), loc)
			);
		}
		if (tags.contains(CommentTag.Priority) && hasPriority()) {
			reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.PRIORITY_TAG, nodeName), getPriorityLocation());
		}
		if (tags.contains(CommentTag.Purpose) && hasPurpose()) {
			reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.PURPOSE_TAG, nodeName), getPurposeLocation());
		}
		if (tags.contains(CommentTag.Remark) && hasRemarks()) {
			getRemarkLocations().forEach(loc ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.REMARK_TAG, nodeName), loc)
			);
		}
		if (tags.contains(CommentTag.Requirement) && hasRequirements()) {
			getRequirementLocations().forEach(loc ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.REQUIREMENT_TAG, nodeName), loc)
			);
		}
		if (tags.contains(CommentTag.Return) && hasReturn()) {
			reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.RETURN_TAG, nodeName), getReturnLocation());
		}
		if (tags.contains(CommentTag.Verdict) && hasVerdicts()) {
			getVerdictLocations().forEach(loc ->
				reportProblem(MessageFormat.format(TAG_NOT_APPLICABLE, ICommentable.VERDICT_TAG, nodeName), loc)
			);
		}
	}

	/**
	 * Specific error listener to provide the exact location
	 * (including the file name, the line and the char. position)
	 * of a syntax error raised during the doc comment parsing.
	 * */
	public class ErrorListenerWithFileName extends BaseErrorListener {
		@Override
		public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
				String msg, RecognitionException e) {
			final Location loc = new Location(getCommentLocation());
			loc.setStartLine(getCommentLocation().getStartLine() + line - 1);
			loc.setStartColumn(charPositionInLine - 1);
			loc.setEndLine(getCommentLocation().getEndLine() + line - 1);
			loc.setEndColumn(charPositionInLine - 1);
			if (msg.contains("token recognition error")) {
				loc.reportProblem("unexpected character", DiagnosticSeverity.Error, true);
			} else {
				loc.reportProblem(msg, DiagnosticSeverity.Error, true);
			}
		}
	}
}
