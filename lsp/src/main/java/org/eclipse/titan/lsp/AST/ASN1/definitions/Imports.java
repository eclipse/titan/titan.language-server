/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IOutlineElement;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ModuleImportationChain;
import org.eclipse.titan.lsp.core.LoadBalancingUtilities;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.parsers.ProjectStructureDataCollector;

/**
 * Imported modules.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 */
public final class Imports extends ASTNode implements IOutlineElement, ILocateableNode {
	private static final String DUPLICATEIMPORTFIRST = "Duplicate import from module `{0}'' was first declared here";
	private static final String DUPLICATEIMPORTREPEATED = "Duplicate import from module `{0}'' was declared here again";
	private static final String TTCN3IMPORT = "An ASN.1 module cannot import from a TTCN-3 module";
	private static final String SELFIMPORT = "A module cannot import from itself";
	private static final String MISSING_IMPORT = "Missing IMPORTS clause is interpreted as `IMPORTS ;' "
			+ "(import nothing) instead of importing all symbols from all modules.";

	/** my module. */
	private ASN1Module module;

	/** The list of imported modules contained here. */
	private final List<ImportModule> importedModules_v;

	/** A hashmap of imported modules, used to to speed up searches. */
	private final Map<String, ImportModule> importedModules_map;

	/**
	 * A hashmap of imported symbol names which were only imported from one
	 * location and their source module.
	 */
	protected final Map<String, Module> singularImportedSymbols_map;
	/**
	 * A hashset of imported symbol names which were imported from multiple locations.
	 */
	protected final Set<String> pluralImportedSymbols;

	private CompilationTimeStamp lastImportCheckTimeStamp;

	/**
	 * The location of the whole import list. This location encloses the
	 * import list fully, as it is used to report errors to.
	 **/
	private Location location = Location.getNullLocation();

	private final boolean isMissing;

	public Imports() {
		this(false);
	}

	public Imports(final boolean isMissing) {
		this.isMissing = isMissing;
		if (isMissing) {
			importedModules_v = Collections.emptyList();
			importedModules_map = Collections.emptyMap();
			singularImportedSymbols_map = Collections.emptyMap();
			pluralImportedSymbols = Collections.emptySet();
			return;
		}
		importedModules_v = new ArrayList<ImportModule>();
		importedModules_map = new HashMap<String, ImportModule>();
		singularImportedSymbols_map = new HashMap<String, Module>();
		pluralImportedSymbols = new HashSet<String>();
	}

	public void addImportModule(final ImportModule importedModule) {
		if (importedModule != null && importedModule.getIdentifier() != null &&
				importedModule.getIdentifier().getName() != null) {
			importedModules_v.add(importedModule);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	/**
	 * Sets the module of this importation list to be the provided module.
	 * @param module the module of this importations.
	 */
	public void setMyModule(final ASN1Module module) {
		this.module = module;
	}

	@Override
	/** {@inheritDoc} */
	public Identifier getIdentifier() {
		return new Identifier(Identifier.Identifier_type.ID_ASN, "imports", location, true);
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		return EMPTY_STRING;
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		return importedModules_v.toArray();
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		return 0;
	}

	/**
	 * Checks the import hierarchies of this importation (and the ones in
	 * the imported module recursively).
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param referenceChain a chain of references used to find circularly imported modules.
	 * @param moduleStack the stack of modules visited so far, from the starting point.
	 */
	public void checkImports(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain, final List<Module> moduleStack) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastImportCheckTimeStamp != null && !lastImportCheckTimeStamp.isLess(timestamp)) {
			return;
		}

		lastImportCheckTimeStamp = timestamp;

		if (isMissing && !SpecialASN1Module.isSpecAsss(module)) {
			// TODO: is a dedicated config preference needed for this?
			module.getIdentifier().getLocation().reportConfigurableSemanticProblem(GeneralConstants.COMPILER_INFOMARKER, MISSING_IMPORT);
			return;
		}

		importedModules_map.clear();
		singularImportedSymbols_map.clear();
		pluralImportedSymbols.clear();

		final ProjectSourceParser parser = GlobalParser.getProjectSourceParser(Project.INSTANCE);
		if (parser == null) {
			return;
		}

		for (final ImportModule importModule : importedModules_v) {
			final Identifier identifier = importModule.getIdentifier();

			if (identifier == null || identifier.getLocation() == null) {
				continue;
			}

			final Module referredModule = parser.getModuleByName(identifier.getName());

			if (referredModule == null) {
				identifier.getLocation().reportSemanticError(
						MessageFormat.format(ImportModule.MISSINGMODULE, identifier.getDisplayName()));
				continue;
			} else if (!(referredModule instanceof ASN1Module)) {
				identifier.getLocation().reportSemanticError(TTCN3IMPORT);
				continue;
			} else if (referredModule == module) {
				identifier.getLocation().reportSemanticError(SELFIMPORT);
				continue;
			}

			String name = identifier.getName();
			if (importedModules_map.containsKey(name)) {
				final Location importedLocation = importedModules_map.get(name).getIdentifier().getLocation();
				importedLocation.reportSingularSemanticError(MessageFormat.format(DUPLICATEIMPORTFIRST,
						identifier.getDisplayName()));
				identifier.getLocation().reportSemanticError(
						MessageFormat.format(DUPLICATEIMPORTREPEATED, identifier.getDisplayName()));
			} else {
				importedModules_map.put(name, importModule);
			}

			final Symbols symbols = importModule.getSymbols();
			if (symbols == null) {
				continue;
			}

			for (Identifier symbol : symbols.getSymbols()) {
				name = symbol.getName();
				if (singularImportedSymbols_map.containsKey(name)) {
					if (!referredModule.equals(singularImportedSymbols_map.get(name))) {
						singularImportedSymbols_map.remove(name);
						pluralImportedSymbols.add(name);
					}
				} else if (!pluralImportedSymbols.contains(name)) {
					singularImportedSymbols_map.put(name, referredModule);
				}
			}
			importModule.setUnhandledChange(false);
			LoadBalancingUtilities.astNodeChecked();
		}

		for (final ImportModule importModule : importedModules_v) {
			// check the imports recursively
			referenceChain.markState();
			importModule.checkImports(timestamp, referenceChain, moduleStack);
			referenceChain.previousState();

		}
	}

	/**
	 * Checks the import statement itself.
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		for (final ImportModule importModule : importedModules_v) {
			importModule.check(timestamp);
		}
	}

	/**
	 * Collects and returns the list of imported modules.
	 * <p>
	 * Module importations that does not point to an existing module are
	 * reported as null objects.
	 *
	 * @return the list of modules imported by the actual module.
	 */
	public List<Module> getImportedModules() {
		return importedModules_v.stream()
		.filter(impmod -> impmod.getReferredModule() != null)
		.map(impmod -> impmod.getReferredModule()).collect(Collectors.toList());
	}

	/**
	 * @return Whether any of the importations has changed to an other
	 *         module one since the last importation check.
	 */
	public boolean hasUnhandledImportChanges() {
		for (final ImportModule impmod : importedModules_v) {
			if (impmod.hasUnhandledChange()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks if a module with the provided id is imported in this module.
	 *
	 * @param id the identifier to use.
	 * @return {@code true} if a module with the provided name is imported,
	 *         {@code false} otherwise.
	 */
	public boolean hasImportedModuleWithId(final Identifier id) {
		final String name = id.getName();
		return importedModules_map.containsKey(name);
	}

	/**
	 * Returns the module with the provided id that imports this module.
	 *
	 * @param id the identifier to use.
	 * @return the module with the provided name that imports this module,
	 *         {@code null} otherwise.
	 */
	public ImportModule getImportedModuleById(final Identifier id) {
		final String name = id.getName();
		if (importedModules_map.containsKey(name)) {
			return importedModules_map.get(name);
		}

		return null;
	}

	public void extractStructuralInformation(final Identifier from, final ProjectStructureDataCollector collector) {
		for (final ImportModule imported : importedModules_v) {
			collector.addImportation(from, imported.getIdentifier());
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final ImportModule im : importedModules_v) {
			if (!im.accept(v)) {
				return false;
			}
		}
		return true;
	}
}
