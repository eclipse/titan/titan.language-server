/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.titanium.markers.handlers.Marker;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * Abstract base class of code smell spotters working on some part the AST.
 * <p>
 * They have a specific type of AST node, on which they can 'work', i.e. process
 * the node for a given code smell. If the spotter is registered in the
 * {@link Analyzer}, the <code>Analyzer</code> will call the
 * {@link #checkNode(IVisitableNode)} method every time it visits a node of this
 * type.
 *
 * @author poroszd
 *
 */
public abstract class BaseModuleCodeSmellSpotter extends BaseCodeSmellSpotter {
	final List<Class<? extends IVisitableNode>> nodeTypes = new ArrayList<Class<? extends IVisitableNode>>();
	final CodeSmellType type;

	protected BaseModuleCodeSmellSpotter(final CodeSmellType type) {
		super(type);
		this.type = type;
	}

	/**
	 * Processing the node.
	 * <p>
	 * The spotter checks the node, searching for the kind of problem that it
	 * can spot.
	 *
	 * @param node
	 *            the node to process.
	 * @return the list of found problems
	 */
	public final List<Marker> checkNode(final IVisitableNode node) {
		final String preference = type.getPreferenceName();
		final String severity = TitanLanguageServer.getFolderConfiguration().getCodeSmellSeverity(preference);
		
		if (severity.equals("ignore")) {
			return new ArrayList<Marker>();
		}
		final Problems problems = new Problems(severity);
		process(node, problems);
		
		return problems.getMarkers();
	}

	/**
	 * Internal processing the node.
	 * <p>
	 * The actual work for matching the code smell is done here. If the spotter
	 * was registered for analysis, this method will be called when the AST
	 * traversal hits a node of type {@link #getStartNodes()}. When the smell
	 * matches the node, <code>problems</code> should be notified about the new
	 * problem. In this case {@link Problems#report(Location, String)} should be
	 * called (usually with the location of the processed node), or in the
	 * absence of such location
	 * {@link Problems#report(org.eclipse.core.resources.IResource, String)}
	 * should be called with the analyzed module's associated resource.
	 *
	 * @param node
	 *            the node to process
	 * @param problems
	 *            the handler class where problems should be reported
	 */
	protected abstract void process(IVisitableNode node, Problems problems);

	/**
	 * Returns the list of node types this spotter will work on while the visitor traverses the AST graph. 
	 * 
	 * @return List of node types
	 */
	public List<Class<? extends IVisitableNode>> getStartNodes() {
		return nodeTypes;
	}

	/**
	 * Adds a node type for which this spotter will work.
	 * 
	 * @param node
	 */
	public void addStartNode(final Class<? extends IVisitableNode> node) {
		nodeTypes.add(node);
	}

	/**
	 * Adds a list of node types for which this spotter will work.
	 * 
	 * @param nodes
	 */
	public void addStartNodes(final List<Class<? extends IVisitableNode>> nodes) {
		nodeTypes.addAll(nodes);
	}
}

