/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.values.expressions;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.PortScope;
import org.eclipse.titan.lsp.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public class GetPortReferenceExpression extends Expression_Value {

	private Port_Type type;

	@Override
	/** {@inheritDoc} */
	public Operation_type getOperationType() {
		return Operation_type.GETPORTREFERENCE_OPERATION;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return Type_type.TYPE_PORT;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReference(final CompilationTimeStamp timestamp, final Assignment lhs) {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		return "getref()";
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnfoldable(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		return true;
	}

	/**
	 * Checks the parameters of the expression and if they are valid in
	 * their position in the expression or not.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * */
	private void checkExpressionOperands(final CompilationTimeStamp timestamp) {
		final PortScope portScope = myScope.getScopePort();
		if (portScope == null) {
			getLocation().reportSemanticError("Operation `port.getref' can only be used in a function with a port clause.");
			setIsErroneous(true);
			return;
		}

		type = portScope.getPortType();
		if (myGovernor != null && type != null && !type.isIdentical(timestamp, myGovernor)) {
			getLocation().reportSemanticError(MessageFormat.format("Type mismatch: A value of type `{0}'' was expected instead of `{1}''", myGovernor.getTypename(), type.getTypename()));
			setIsErroneous(true);
		}
	}

	@Override
	/** {@inheritDoc} */
	public IValue evaluateValue(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}

		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;
		type = null;

		checkExpressionOperands(timestamp);

		return lastValue;
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		// no members
		return true;
	}
}
