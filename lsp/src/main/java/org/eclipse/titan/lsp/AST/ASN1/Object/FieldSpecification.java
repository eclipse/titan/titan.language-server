/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.IIdentifiable;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.ISetting;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Class to represent a FieldSpec of an ObjectClass.
 *
 * @author Kristof Szabados
 */
public abstract class FieldSpecification extends ASTNode implements ILocateableNode, ICancelBuild, IIdentifiable {

	public enum Fieldspecification_types {
		/** undefined. */
		FS_UNDEFINED,
		/** erroneous field specification. */
		FS_ERROR,
		/** type field specification. */
		FS_T,
		/** fixed type value field specification. */
		FS_V_FT,
		/** variable type value field specification. */
		FS_V_VT,
		/** fixed type valueset field specification. */
		FS_VS_FT,
		/** variable type valueset field specification. */
		FS_VS_VT,
		/** object field specification. */
		FS_O,
		/** objectset field specification. */
		FS_OS
	}

	/**
	 * The location of the whole field specification. This location encloses
	 * the field specification fully, as it is used to report errors to.
	 */
	protected Location location = Location.getNullLocation();

	protected final Identifier identifier;
	protected boolean isOptional;
	protected ObjectClass_Definition myObjectClass;

	/** the time when this field specification was check the last time. */
	protected CompilationTimeStamp lastTimeChecked;

	protected FieldSpecification(final Identifier identifier, final boolean isOptional) {
		this.identifier = identifier;
		this.isOptional = isOptional;
	}

	@Override
	/** {@inheritDoc} */
	public final void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getLocation() {
		return location;
	}

	@Override
	public final Identifier getIdentifier() {
		return identifier;
	}

	public abstract Fieldspecification_types getFieldSpecificationType();

	public void setMyObjectClass(final ObjectClass_Definition objectClass) {
		myObjectClass = objectClass;
	}

	public final boolean isOptional() {
		return isOptional;
	}

	public abstract boolean hasDefault();

	/**
	 * Does the semantic checking of the type.
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 */
	public abstract void check(final CompilationTimeStamp timestamp);

	public abstract ISetting getDefault();

	/**
	 * Only the fixed type value field specification has this attribute
	 * @return whether the fixed type value field specification is unique
	 */
	public boolean isUnique() {
		return false;
	}

	public FieldSpecification getLast() {
		return this;
	}
}
