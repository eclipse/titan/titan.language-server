/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;

/**
 * ObjectSet elements (flat container).
 * Warning: the objects are not owned by
 * Objects, it stores only the pointers...
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 */
public final class ASN1Objects extends ASTNode {

	private final CopyOnWriteArrayList<Object_Definition> objects = new CopyOnWriteArrayList<Object_Definition>();

	/**
	 * Add a new object to the object list.
	 * @param object the object to add.
	 * */
	public void add(final Object_Definition object) {
		objects.add(object);
	}

	/**
	 * Returns an unmodifiable view of the underlying {@linkplain Object_Definition} list
	 * @return an unmodifiable view of the underlying Object_Definition list
	 */
	public List<Object_Definition> getAll() {
		return Collections.unmodifiableList(objects);
	}

	/**
	 * Returns the number of objects.
	 * @return the number of objects.
	 */
	public int size() {
		return objects.size();
	}

	/**
	 * Returns the object at a given index.
	 *
	 * @param index the index of the element to return.
	 * @return the object at a given index.
	 */
	public Object_Definition get(final int index) {
		return objects.get(index);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final Object_Definition object : objects) {
			if (!object.accept(v)) {
				return false;
			}
		}
		return true;
	}
}
