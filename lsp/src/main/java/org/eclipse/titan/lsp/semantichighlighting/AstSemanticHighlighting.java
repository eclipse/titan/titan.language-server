/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.semantichighlighting;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.core.Position;

/**
 * This class supports semantic highlighting, handling both semantic types and modifiers.
 *  
 * @author Miklos Magyari
 *
 */
public final class AstSemanticHighlighting {
	private static List<String> tokenTypes = new ArrayList<>();
	private static List<String> tokenModifiers = new ArrayList<>();

	/**
	 * List of supported token types. Names should match with token types
	 * defined in the LSP standard. Enum items are converted to lowercase
	 * and sent as token legend.
	 * Token 'None' is a specific value used for cases when a location
	 * does not have a token type but it has one or more modifiers.
	 */
	public enum SemanticType {
		// IMPORTANT: For proper functionality and compatibility with VSCode,
		// numeric values must be incremented by 1

		// General token types
		Namespace(0), 	Type(1), 		Class(2),
		Enum(3), 		Interface(4), 	TypeParameter(5),
		Parameter(6), 	Variable(7), 	Property(8),
		EnumMember(9), 	Event(10), 		Function(11),
		Method(12), 	Macro(13), 		Keyword(14),
		Modifier(15), 	Comment(16), 	String(17),
		Number(18), 	Regexp(19), 	Operator(20),
		Decorator(21),

		// TTCN3 specific token types
		Module(22), 	Import(23), 	Group(24),
		Port(25), 		Component(26), 	Signature(27),
		External(28), 	Template(29), 	Altstep(30),
		Testcase(31), 	Timer(32),		TemplateVariable(33),
		Inactive(34),	None(35);

		private final int value;

        SemanticType(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }
	}

	/**
	 * List of supported token modifiers. Names should match with token modifiers
	 * defined in the LSP standard. Enum items are converted to lowercase
	 * and sent as token legend.
	 * 
	 * Numeric values should be powers of 2 as multiple modifiers for the same location
	 * will be combined using bitwise OR. 
	 */
	public enum SemanticModifier {
		Readonly(1), Deprecated(2), Definition(4);

		private final int value;

        SemanticModifier(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }
	}
	
	static {
		for (SemanticType type : SemanticType.values()) {
			tokenTypes.add(type.name().toLowerCase());
		}
		for (SemanticModifier modifier : SemanticModifier.values()) {
			tokenModifiers.add(modifier.name().toLowerCase());
		}
	}

	/** map of all semantic tokens for the current project */
	private static Map<Path,Map<Location,SemanticType>> tokens = new ConcurrentHashMap<>();

	/** map of all semantic modifiers for the current project */
	private static Map<Path,Map<Location,List<SemanticModifier>>> modifiers = new ConcurrentHashMap<>();

	private AstSemanticHighlighting() {
		// Do nothing
	}

	/**
	 * Sets the semantic token type for a given location. If a previous value is present, it will
	 * be overwritten.
	 *  
	 * @param location
	 * @param tokenType
	 */
	public static void addSemanticToken(final Location location, final SemanticType tokenType) {
		if (location == NULL_Location.INSTANCE) {
			return;
		}
		final Path path = location.getFile().toPath();
		if (! tokens.containsKey(path)) {
			final Map<Location,SemanticType> newmap = new ConcurrentHashMap<>();
			newmap.put(location, tokenType);
			tokens.put(path, newmap);
		} else {
			final Map<Location,SemanticType> pathMap = tokens.get(path);
			pathMap.put(location, tokenType);
			tokens.put(path, pathMap);
		}
	}

	/**
	 * Adds a semantic modifier for the given location (multiple modifiers can be added to the same 
	 * location).
	 * 
	 * Make sure to also add a semantic type when setting modifiers otherwise they have no effect (either
	 * by calling <code>addSemanticModifier()</code> or using the combined
	 * <code>addSemanticTokenAndModifier()</code> instead.
	 * 
	 * @param location
	 * @param tokenModifier
	 */
	public static void addSemanticModifier(final Location location, final SemanticModifier tokenModifier) {
		if (location == NULL_Location.INSTANCE) {
			return;
		}
		final Path path = location.getFile().toPath();
		final Map<Location,List<SemanticModifier>> pathMap = modifiers.get(path);
		if (pathMap == null) {
			final Map<Location,List<SemanticModifier>> newmap = new ConcurrentHashMap<>();
			final List<SemanticModifier> newlist = new CopyOnWriteArrayList<>(Arrays.asList(tokenModifier));
			newmap.put(location, newlist);
			modifiers.put(path, newmap);
		} else {
			List<SemanticModifier> oldlist = pathMap.get(location);
			if (oldlist == null) {
				oldlist = new CopyOnWriteArrayList<>();
			}
			if (oldlist.contains(tokenModifier)) {
				return;
			}
			oldlist.add(tokenModifier);
			pathMap.put(location, oldlist);
			modifiers.put(path, pathMap);
		}

	}
	
	public static void addSemanticTokenAndModifier(final Location location, final SemanticType semanticType, final SemanticModifier tokenModifier) {
		addSemanticToken(location, semanticType);
		addSemanticModifier(location, tokenModifier);
	}

	public static SemanticType getSemanticToken(final Location location) {
		if (location == NULL_Location.INSTANCE) {
			return null;
		}

		final Path path = location.getFile().toPath();
		Map<Location,SemanticType> pathMap = tokens.get(path);
		if (pathMap == null) {
			return null;
		}
		return pathMap.get(location);
	}

	public static boolean hasSemanticToken(final Location location) {
		if (location == NULL_Location.INSTANCE) {
			return false;
		}
		final Path path = location.getFile().toPath();
		Map<Location,SemanticType> pathMap = tokens.get(path);
		if (pathMap == null) {
			return false;
		}
		return pathMap.containsKey(location);
	}

	public static List<SemanticModifier> getSemanticModifier(final Location location) {
		final Path path = location.getFile().toPath();
		Map<Location,List<SemanticModifier>> pathMap = modifiers.get(path);
		if (pathMap == null) {
			return Collections.emptyList();
		}
		return pathMap.get(location);
	}

	/** 
	 * Removes all semantic tokens and modifiers for the given file.
	 * @param path
	 */
	public static void clearSemanticTokensAndModifiers(final Path path) {
		if (tokens.containsKey(path)) {
			tokens.get(path).clear();
		}

		if (modifiers.containsKey(path)) {
			modifiers.get(path).clear();
		}
	}

	/** 
	 * Removes all semantic tokens and modifiers between a start and end position for the given file. 
	 * Used by incremental parsing.
	 * 
	 * @param path
	 */
	public static void clearSemanticTokensAndModifiers(final Path path, final Position from, final Position to) {
		if (tokens.containsKey(path)) {
			final List<Location> toRemove = new CopyOnWriteArrayList<>();
			tokens.get(path).forEach((location, type) -> {
				if (location.getStartPosition().afterOrEquals(from) && location.getEndPosition().beforeOrEquals(to)) {
					toRemove.add(location);
				}
			});
			final Map<Location,SemanticType> existing = tokens.get(path);
			toRemove.forEach(location ->
				existing.remove(location)
			);
		}

		if (modifiers.containsKey(path)) {
			final List<Location> toRemove = new CopyOnWriteArrayList<>();
			modifiers.get(path).forEach((location, modifier) -> {
				if (location.getStartPosition().afterOrEquals(from) && location.getEndPosition().beforeOrEquals(to)) {
					toRemove.add(location);
				}
			});
			final Map<Location,List<SemanticModifier>> existing = modifiers.get(path);
			toRemove.forEach(location ->
				existing.remove(location)
			);
		}
	}

	public static Map<Location,SemanticType> getSemanticTokens(final Path path) {
		return tokens.get(path);
	}

	public static void setSemanticTokens(final Path path, final Map<Location,SemanticType> newTokens) {
		tokens.put(path, newTokens);
	}

	public static Map<Location,List<SemanticModifier>> getSemanticModifiers(final Path path) {
		Map<Location,List<SemanticModifier>> existingModifiers = modifiers.get(path);
		return existingModifiers != null ? existingModifiers : Collections.emptyMap();
	}

	public static void setSemanticModifiers(final Path path, final Map<Location,List<SemanticModifier>> newModifiers) {
		modifiers.put(path, newModifiers);
	}

	public static List<String> getSemanticTokensLegend() {
		return tokenTypes;
	}

	public static List<String> getSemanticModifiersLegend() {
		return tokenModifiers;
	}
}
