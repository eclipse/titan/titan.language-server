/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.util.List;

/**
 * Utility class for generating the value and template classes for
 * function/altstep/testcase types.
 *
 * starting/activating/executing is not yet supported
 *
 * @author Kristof Szabados
 * */
public final class FunctionReferenceGenerator {

	enum fatType {FUNCTION, ALTSTEP, TESTCASE}

	public static class FunctionReferenceDefinition {
		private final String genName;
		private final String displayName;
		public String returnType;
		public fatType type;
		public boolean runsOnSelf;
		public boolean isStartable;
		public String formalParList;
		public String actualParList;
		public List<String> parameterTypeNames;
		public List<String> parameterNames;

		public FunctionReferenceDefinition(final String genName, final String displayName) {
			this.genName = genName;
			this.displayName = displayName;
		}
	}

	private FunctionReferenceGenerator() {
		// private to disable instantiation
	}
}
