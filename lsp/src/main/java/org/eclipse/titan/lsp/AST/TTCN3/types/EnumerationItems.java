/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * */
public final class EnumerationItems extends ASTNode implements IIncrementallyUpdatable {
	private static final String FULLNAMEPART = ".<unknown_enumeration_item>";

	private final List<EnumItem> items;

	private Location location = NULL_Location.INSTANCE;

	public EnumerationItems() {
		items = new ArrayList<EnumItem>();
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final EnumItem item : items) {
			if (item == child) {
				final Identifier identifier = item.getIdentifier();
				if (identifier != null) {
					return builder.append(INamedNode.DOT).append(identifier.getDisplayName());
				}

				return builder.append(FULLNAMEPART);
			}
		}

		return builder;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(final Location location) {
		this.location = location;
	}

	public void addEnumItem(final EnumItem enumItem) {
		if (enumItem != null && enumItem.getIdentifier() != null) {
			items.add(enumItem);
			enumItem.setFullNameParent(this);
		}
	}

	public List<EnumItem> getItems() {
		return items;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		for (final EnumItem item : items) {
			item.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final EnumItem item : items) {
			item.updateSyntax(reparser, false);
			reparser.updateLocation(item.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (items != null) {
			for (final EnumItem ei : items) {
				ei.findReferences(referenceFinder, foundIdentifiers);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (items != null) {
			for (final EnumItem ei : items) {
				if (!ei.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}
}
