/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.LoadBalancingUtilities;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.titanium.markers.handlers.Marker;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseProjectCodeSmellSpotter;

/**
 * The core controller class of the code smell module
 * <p>
 * The <code>Analyzer</code> is responsible for conducting the code smell
 * analysis of a ttcn3 project or a single module. This includes handling and
 * executing the code smell spotters, locking the project during the analysis to
 * prevent modification.
 * <p>
 * Analyzer instances are immutable, but (slightly) expensive to construct.
 * Instances are obtained via the builder facility (see {@link #builder()}).
 * <p>
 * For performance reasons the {@link #withAll()} and {@link #withPreference()}
 * methods can also be used to obtain <code>Analyzer</code> instances. These are
 * cached instances, updated on preference setting changes.
 *
 * @author poroszd
 * @author Miklos Magyari
 *
 */
public class Analyzer {
	private final Map<Class<? extends IVisitableNode>, Set<BaseModuleCodeSmellSpotter>> actions;
	private final Set<BaseProjectCodeSmellSpotter> projectActions;
	private final Object lockObject = new Object();

	Analyzer(final Map<Class<? extends IVisitableNode>, Set<BaseModuleCodeSmellSpotter>> actions, final Set<BaseProjectCodeSmellSpotter> projectActions) {
		this.actions = actions;
		this.projectActions = projectActions;
	}

	// TODO: Run spotters parallel in a thread-pool
	private class CodeSmellVisitor extends ASTVisitor {
		private final List<Marker> markers;

		public CodeSmellVisitor() {
			markers = new ArrayList<Marker>();
		}

		@Override
		public int visit(final IVisitableNode node) {
			final Set<BaseModuleCodeSmellSpotter> actionsOnNode = actions.get(node.getClass());
			if (actionsOnNode != null) {
				for (final BaseModuleCodeSmellSpotter spotter : actionsOnNode) {
					markers.addAll(spotter.checkNode(node));
				}
			}
			return V_CONTINUE;
		}
	}

	private List<Marker> internalAnalyzeModule(final Module module) {
		final CodeSmellVisitor v = new CodeSmellVisitor();
		module.accept(v);
		return v.markers;
	}

	private List<Marker> internalAnalyzeProject(final Project project) {
		final List<Marker> markers = new ArrayList<Marker>();
		for (final BaseProjectCodeSmellSpotter spotter : projectActions) {
			List<Marker> ms;
			synchronized (lockObject) {
				ms = spotter.checkProject(project);
			}
			markers.addAll(ms);
		}
		return markers;
	}

	/**
	 * Analyze a whole project.
	 * <p>
	 * Executes the configured code smell spotters on the given project. Locking
	 * the project to prevent modification of the AST is handled internally.
	 *
	 * @param monitor
	 *            shows progress and makes it interruptable
	 * @param module
	 *            the ttcn3 project to analyze
	 *
	 * @return the code smells found in the given project
	 */
	public List<Marker> analyzeProject(final Project project) {
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(project);
		final Set<String> knownModuleNames = projectSourceParser.getKnownModuleNames();
		final List<Marker> markers = new CopyOnWriteArrayList<>();
		markers.addAll(internalAnalyzeProject(project));

		final List<Module> knownModules = new ArrayList<Module>(knownModuleNames.size());
		for (final String moduleName : knownModuleNames) {
			final Module mod = projectSourceParser.getModuleByName(moduleName);
			knownModules.add(mod);
		}
		Collections.sort(knownModules, (o1, o2) ->
			o2.getAssignments().size() - o1.getAssignments().size()
		);

		final ExecutorService executor = Executors.newCachedThreadPool(r -> {
			final Thread t = new Thread(r);
			t.setPriority(LoadBalancingUtilities.getThreadPriority());
			return t;
		});
		final CountDownLatch latch = new CountDownLatch(knownModuleNames.size());
		for (final Module module : knownModules) {
			executor.execute(() -> {
//				if (progress.isCanceled()) {
//					latch.countDown();
//					throw new OperationCanceledException();
//				}

				try {
					markers.addAll(internalAnalyzeModule(module));
				} finally {
					latch.countDown();
				}
			});
		}

		try {
			latch.await();
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdown();
		try {
			executor.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdownNow();

		return markers;
	}

	/**
	 * The factory method of {@link AnalyzerBuilder}s.
	 * <p>
	 * To obtain an <code>Analyzer</code> instance, one have to call this
	 * method, configure the builder appropriately and let the builder construct
	 * the <code>Analyzer</code> itself. This is cruical to ensure the
	 * immutability, thus thread-safety of this class.
	 *
	 * @return a new {@link AnalyzerBuilder} instance
	 */
	public static AnalyzerBuilder builder() {
		return new AnalyzerBuilder();
	}


}
