/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ActualParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.types.Function_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value.Operation_type;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ApplyExpression;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents an invoke template. <br>
 * This kind of template is not parsed, but transformed from a single value template
 *
 * @author Kristof Szabados
 * */
public final class Invoke_Template extends TTCN3Template {
	private static final String FUNCTIONEXPECTED = "A value of type function expected instead of `{0}''";
	private static final String TYPEMISSMATCHERROR = "Type mismatch: a value or template of type `{0}'' was expected instead of `{1}''";
	private static final String VALUEXPECTED1 = "A value of type function was expected";
	private static final String VALUEXPECTED2 = "Reference to a value was expected, but functions of type `{0}'' return a template of type `{1}''";
	private static final String FUNCTIONEXPECTEDINARG = "A value of type function was expected in the argument";
	private static final String FUNCTIONEXPECTEDINARG2 = "A value of type function was expected in the argument instead of `{0}''";

	// the first part of the expression, the function reference value to be invoked
	private final Value value;

	private final ParsedActualParameters actualParameterList;

	private ActualParameterList actualParameter_list;

	Invoke_Template(final CompilationTimeStamp timestamp, final SpecificValue_Template original) {
		copyGeneralProperties(original);
		final IValue v = original.getValue();

		if (v == null || !Value_type.EXPRESSION_VALUE.equals(v.getValuetype())) {
			value = null;
			actualParameterList = null;

			return;
		}

		final Expression_Value expressionValue = (Expression_Value) v;
		if (!Operation_type.APPLY_OPERATION.equals(expressionValue.getOperationType())) {
			value = null;
			actualParameterList = null;

			return;
		}

		final ApplyExpression expression = (ApplyExpression) expressionValue;
		value = expression.getValue();
		actualParameterList = expression.getParameters();
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.TEMPLATE_INVOKE;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		builder.append(value.createStringRepresentation());
		builder.append(".invoke(");
		if (actualParameterList != null) {
			// TODO implement more precise create_StringRepresentation
			builder.append(ELLIPSIS);
		}
		builder.append(PARENTHESESOPENCLOSE);

		return addToStringRepresentation(builder).toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (value != null) {
			value.setMyScope(scope);
		}
		if (actualParameterList != null) {
			actualParameterList.setMyScope(scope);
		}
		/*
		 * if(actualParameter_list != null){
		 * actualParameter_list.set_my_scope(scope); }
		 */
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (myGovernor != null) {
			return myGovernor;
		}

		if (value == null) {
			setIsErroneous(true);
			return null;
		}

		IType type = value.getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
		if (type == null) {
			if (!value.getIsErroneous(timestamp)) {
				value.getLocation().reportSemanticError(VALUEXPECTED1);
			}
			setIsErroneous(true);
			return null;
		}

		type = type.getTypeRefdLast(timestamp);
		switch (type.getTypetype()) {
		case TYPE_FUNCTION:
			final Type result = ((Function_Type) type).getReturnType();
			if (!Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue) && ((Function_Type) type).returnsTemplate()) {
				location.reportSemanticError(MessageFormat.format(VALUEXPECTED2, type.getTypename(), result.getTypename()));
			}
			return result;
		case TYPE_ALTSTEP:
			setIsErroneous(true);
			return null;
		default:
			value.getLocation().reportSemanticError(MessageFormat.format(FUNCTIONEXPECTED, type.getTypename()));
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (getIsErroneous(timestamp) || value == null) {
			return Type_type.TYPE_UNDEFINED;
		}

		final IType type = value.getExpressionGovernor(timestamp, expectedValue);
		if (type == null) {
			return Type_type.TYPE_UNDEFINED;
		}

		return type.getTypeRefdLast(timestamp).getTypetypeTtcn3();
	}

	@Override
	/** {@inheritDoc} */
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		if (isBuildCancelled()) {
			return;
		}
		
		checkInvoke(timestamp);
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {
		checkInvoke(timestamp);
		final IType governor = getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
		if (governor == null) {
			setIsErroneous(true);
		} else if (!type.isCompatible(timestamp, governor, null, null, null)) {
			location.reportSemanticError(MessageFormat.format(TYPEMISSMATCHERROR, type.getTypename(), governor.getTypename()));
			setIsErroneous(true);
		}

		checkThisTemplateGenericPostChecks(timestamp, type, allowOmit, subCheck);
		return false;
	}

	public void checkInvoke(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (getIsErroneous(timestamp) || actualParameterList == null || value == null) {
			return;
		}

		value.setLoweridToReference(timestamp);
		IType type = value.getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
		if (type != null) {
			type = type.getTypeRefdLast(timestamp);
		}

		if (type == null) {
			if (!value.getIsErroneous(timestamp)) {
				value.getLocation().reportSemanticError(FUNCTIONEXPECTEDINARG);
			}
			setIsErroneous(true);
			return;
		}

		if (!Type_type.TYPE_FUNCTION.equals(type.getTypetype())) {
			value.getLocation().reportSemanticError(
					MessageFormat.format(FUNCTIONEXPECTEDINARG2, type.getTypename()));
			setIsErroneous(true);
			return;
		}

		if (myScope == null) {
			return;
		}

		myScope.checkRunsOnScope(timestamp, type, this, GeneralConstants.CALL);
		final FormalParameterList formalParameterList = ((Function_Type) type).getFormalParameters();
		actualParameter_list = new ActualParameterList();
		if (!formalParameterList.checkActualParameterList(timestamp, actualParameterList, actualParameter_list)) {
			actualParameter_list.setFullNameParent(this);
			actualParameter_list.setMyScope(getMyScope());
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, omitAllowed, usageLocation);
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (value != null) {
			value.updateSyntax(reparser, false);
			reparser.updateLocation(value.getLocation());
		}

		if (actualParameterList != null) {
			actualParameterList.updateSyntax(reparser, false);
			reparser.updateLocation(actualParameterList.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (value != null) {
			value.findReferences(referenceFinder, foundIdentifiers);
		}
		if (actualParameterList != null) {
			actualParameterList.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (value != null && !value.accept(v)) {
			return false;
		}
		if (actualParameterList != null && !actualParameterList.accept(v)) {
			return false;
		}
		return true;
	}
}
