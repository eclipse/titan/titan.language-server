/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.types;

import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.types.CompField;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * ExtensionAndException.
 * <p>
 * originally ExtAndExc in TITAN
 *
 * @author Kristof Szabados
 */
public final class ExtensionAndException extends ASTNode {
	private static final String FULLNAMEPART = ".<exception>";

	/** optional exception specification. */
	private ExceptionSpecification exceptionSpecification;
	private ExtensionAdditions extensionAdditions;

	public ExtensionAndException(final ExceptionSpecification exceptionSpecification, final ExtensionAdditions extensionAdditions) {
		this.exceptionSpecification = exceptionSpecification;
		this.extensionAdditions = (null != extensionAdditions) ? extensionAdditions : new ExtensionAdditions();

		this.extensionAdditions.setFullNameParent(this);
	}

	public ExceptionSpecification getExceptionSpecification(){
		return exceptionSpecification;
	}

	public void setExceptionSpecification(final ExceptionSpecification exceptionSpecification) {
		this.exceptionSpecification = exceptionSpecification;

		if (null != exceptionSpecification) {
			exceptionSpecification.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (exceptionSpecification == child) {
			return builder.append(FULLNAMEPART);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		extensionAdditions.setMyScope(scope);
		if (null != exceptionSpecification) {
			exceptionSpecification.setMyScope(scope);
		}
	}

	public int size() {
		return extensionAdditions.size();
	}

	public CompField getCompByIndex(final int index) {
		return extensionAdditions.getCompByIndex(index);
	}

	public void trCompsof(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain, final boolean isSet) {
		extensionAdditions.trCompsof(timestamp, referenceChain, isSet);
	}

	public void setExtensionAdditions(final ExtensionAdditions extensionAdditions) {
		this.extensionAdditions = extensionAdditions;
		this.extensionAdditions.setFullNameParent(this);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (exceptionSpecification != null) {
			exceptionSpecification.findReferences(referenceFinder, foundIdentifiers);
		}
		if (extensionAdditions != null) {
			extensionAdditions.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (exceptionSpecification != null && !exceptionSpecification.accept(v)) {
			return false;
		}
		if (extensionAdditions != null && !extensionAdditions.accept(v)) {
			return false;
		}
		return true;
	}
}
