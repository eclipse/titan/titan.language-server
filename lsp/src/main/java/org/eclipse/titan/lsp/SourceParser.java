/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ModuleImportationChain;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Parser;

public class SourceParser {
	public static final String CIRCULARIMPORTCHAIN = "Circular import chain is not recommended: {0}";
	
	public List<Diagnostic> items;
	private TTCN3Module module;
	
	/**
	 * The list of markers (ERROR and WARNING) created during parsing
	 * NOTE: used from ANTLR v4
	 * TODO: what is its purpose?
	 */
	private List<SyntacticErrorStorage> mErrorsStored = null;

	public List<Diagnostic> parse( final Reader aReader, final File file, final long aFileLength) {
		final boolean realtimeEnabled = false;
		final boolean oopEnabled = true;
		
		items = new ArrayList<Diagnostic>();
		
		final CharStream charStream = new UnbufferedCharStream( aReader );
		final Ttcn3Lexer lexer = new Ttcn3Lexer( charStream );
		lexer.setActualFile(file);
		lexer.enableOop(oopEnabled);
		lexer.setCommentTodo( true );
		lexer.setTokenFactory( new CommonTokenFactory( true ) );
		lexer.initRootInterval( (int)aFileLength );
		if (realtimeEnabled) {
			lexer.enableRealtime();
		}

		try {
			final TitanErrorListener lexerListener = new TitanErrorListener(file);
			lexer.removeErrorListeners();
			lexer.addErrorListener(lexerListener);
		
			final CommonTokenStream tokenStream = new CommonTokenStream( lexer );
	
			Ttcn3Parser parser = new Ttcn3Parser(tokenStream);
			parser.setActualFile(file);
			parser.enableOop(oopEnabled);
			ParserUtilities.setBuildParseTree( parser );
			parser.removeErrorListeners();
			final TitanErrorListener parserListener = new TitanErrorListener(file);
			parser.addErrorListener( parserListener );
	
			// This is added because of the following ANTLR 4 bug:
			// Memory Leak in PredictionContextCache #499
			// https://github.com/antlr/antlr4/issues/499
			final DFA[] decisionToDFA = parser.getInterpreter().decisionToDFA;
			parser.setInterpreter(new ParserATNSimulator(parser, parser.getATN(), decisionToDFA, new PredictionContextCache()));
	
			//try SLL mode
			try {
				parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
				final ParseTree root = parser.pr_TTCN3File();
				module = parser.getModule();
				final ModuleImportationChain referenceChain = new ModuleImportationChain(CIRCULARIMPORTCHAIN, false);
				final List<Module> modlist = new ArrayList<Module>();
				if (module != null) {
					module.checkImports(module.getLastCompilationTimeStamp(), referenceChain, modlist);
				}
				referenceChain.clear();
				if (module != null) {
					module.check(CompilationTimeStamp.getNewCompilationCounter());
					module.postCheck();
				}
				mErrorsStored = lexerListener.getErrorsStored();
				mErrorsStored.addAll( parserListener.getErrorsStored() );
			} catch (RecognitionException e) {
				TitanLogger.logError(e);
			} 
			//tokenStream.fill();
		} catch (Exception e) {
			TitanLogger.logError(e);
		}
			
		try {
			aReader.close();
		} catch (IOException e) {
		}

		return items;
	}
	
	public TTCN3Module getTtcn3Module() {
		return module;
	}
}
