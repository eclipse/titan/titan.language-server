/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.SignatureExceptions;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * This class represents abstract functions used in Ttcn3 classes
 * 
 * @author Miklos Magyari
 */
public class Def_AbsFunction extends Def_FunctionBase {
	private static final String CONCRETEABSTRACT = "Concrete class type `{0}'' cannot have abstract methods";
	private static final String KIND = "abstract function";
	
	private final SignatureExceptions exceptions;
	
	public Def_AbsFunction(final Identifier identifier, final FormalParameterList formalParameterList, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final Reference portReference,
			final Type returnType, final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction,
			final SignatureExceptions exceptions, final StatementBlock block, final StatementBlock finallyBlock,
			final boolean isClassFunction, final boolean isAbstract, final boolean isFinal, final Location classModifierLocation,
			final boolean isDeterministic, final boolean isControl, final Location funcModifierLocation) {
		super(identifier, block, isAbstract, isFinal, isDeterministic, isControl, formalParameterList, returnType);
		this.exceptions = exceptions;
		assignmentType = (returnType == null) ? Assignment_type.A_FUNCTION : (returnsTemplate ? Assignment_type.A_FUNCTION_RTEMP
			: Assignment_type.A_FUNCTION_RVAL);
	}

	@Override
	public String getProposalKind() {
		return KIND;
	}

	@Override
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	public void check(CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	public void check(CompilationTimeStamp timestamp, IReferenceChain refChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;
		
//		removeSyntaxDecoration();
		/** 
		 * Code minings are disabled for now
		 */
		//removeMining();
		
		final Class_Type myClass = myScope.getScopeClass();
		if (myClass == null) {
			// FIXME : fatal error
		}
		if (!myClass.isAbstract() && !myClass.isTrait()) {
			getSignatureLocation().reportSemanticError(
				MessageFormat.format(CONCRETEABSTRACT, myClass.getDefiningAssignment().getType(timestamp).getTypename()));
		}
		formalParameterList.check(timestamp, assignmentType);
		if (returnType != null) {
			returnType.check(timestamp);
			returnType.checkAsReturnType(timestamp, assignmentType == Assignment_type.A_FUNCTION_RVAL, "n abstract function");
		}
		if (exceptions != null) {
			// TODO : check
		}
		
		checkDocumentComment();	
	}

	@Override
	public IsIdenticalResult isIdentical(CompilationTimeStamp timestamp, Def_FunctionBase other) {
		// TODO Auto-generated method stub
		return null;
	}
}
