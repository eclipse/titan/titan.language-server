/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignments;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IReferencingElement;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ModuleImportation;
import org.eclipse.titan.lsp.AST.ModuleImportationChain;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.TTCN3.IAppendableSyntax;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.IModifiableVisibility;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_reparser_optionalWithStatementContext;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.ISemanticInformation;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * The ImportModule class represents a TTCN3 import statement. This class is
 * used to create a link between the actual mode containing the import statement
 * and the imported module.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public final class ImportModule extends ModuleImportation
	implements ILocateableNode, IAppendableSyntax, IIncrementallyUpdatable, IReferencingElement, ISemanticInformation, IModifiableVisibility {
	public static final String MISSINGMODULE = "There is no module with name `{0}''";

	private WithAttributesPath withAttributesPath = null;

	private Identifier myModuleIdentifier;
	private Identifier localIdentifier = null;
	private TTCN3Module myModule;

	private Group parentGroup = null;
	private Location location = Location.getNullLocation();

	/** The visibility modifier of this import module statement.
	 *  Should be set by the parser.
	 * */
	private VisibilityModifier visibilityModifier;
	/** The location of visibility modifier of this definition */
	private Location visibilityModifierLocation = NULL_Location.INSTANCE;

	private boolean hasNormalImport = false;
	private boolean hasImportOfImport = false;
	
	private List<String> languageSpecifications;

	public ImportModule(final Identifier identifier) {
		super(identifier);
		visibilityModifier = VisibilityModifier.Private;
	}

	/**
	 * Sets the current import statement to be containing normal (non-import
	 * of imports) imports.
	 * */
	public void setHasNormalImports() {
		hasNormalImport = true;
	}

	/**
	 * Sets the current import statement to be containing import of imports
	 * imports.
	 * */
	public void setHasImportOfImports() {
		hasImportOfImport = true;
	}

	@Override
	/** {@inheritDoc} */
	public final void setVisibilityModifier(final VisibilityModifier modifier, final Location location) {
		visibilityModifier = modifier;
		visibilityModifierLocation = location;
	}

	@Override
	/** {@inheritDoc} */
	public final VisibilityModifier getVisibilityModifier() {
		if (visibilityModifier == null) {
			throw new UnsupportedOperationException("Visibility modifier cannot be null");
		}

		return visibilityModifier;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getVisibilityModifierLocation() {
		return visibilityModifierLocation;
	}

	/**
	 * Returns the name of the module.
	 *
	 * @return the name of the module
	 * */
	public String getName() {
		return (identifier != null) ? identifier.getName() : null;
	}

	@Override
	/** {@inheritDoc} */
	public String chainedDescription() {
		if (myModuleIdentifier != null) {
			return myModuleIdentifier.getDisplayName();
		}

		return (identifier != null) ? identifier.getDisplayName() : null;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	/**
	 * Sets the module in which this importation can be found.
	 *
	 * @param module
	 *                the module containing this importation
	 * */
	public void setMyModule(final Identifier module) {
		myModuleIdentifier = module;
	}

	/**
	 * Sets the module of the importation.
	 *
	 * @param myModule
	 *                the module of the importation.
	 * */
	public void setMyModule(final TTCN3Module myModule) {
		this.myModule = myModule;
	}

	/**
	 * returns the module the importation is located in
	 *
	 * @return the module of the importation.
	 * */
	public TTCN3Module getMyModule() {
		return myModule;
	}

	/**
	 * Sets the parent group of the importation.
	 *
	 * @param parentGroup
	 *                the parent group to be set.
	 * */
	public void setParentGroup(final Group parentGroup) {
		this.parentGroup = parentGroup;
	}

	/** @return the parent group of the importation */
	public Group getParentGroup() {
		return parentGroup;
	}

	/**
	 * Returns the anytype definition contained in the module pointed to by
	 * this importation.
	 *
	 * @return the anytype definition of the imported module
	 * */
	public Def_Type getAnytype() {
		return (referredModule == null) ? null : referredModule.getAnytype();
	}

	/**
	 * Sets the with attributes for this importation if it has any. Also
	 * creates the with attribute path, to store the attributes in.
	 *
	 * @param attributes
	 *                the attribute to be added.
	 * */
	public void setWithAttributes(final MultipleWithAttributes attributes) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}
		if (attributes != null) {
			withAttributesPath.setWithAttributes(attributes);
		}
	}

	/**
	 * @return the with attribute path element of this importation. If it
	 *         did not exist it will be created.
	 * */
	public WithAttributesPath getAttributePath() {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		return withAttributesPath;
	}

	/**
	 * Sets the parent path for the with attribute path element of this
	 * importation. Also, creates the with attribute path node if it did not
	 * exist before.
	 *
	 * @param parent
	 *                the parent to be set.
	 * */
	public void setAttributeParentPath(final WithAttributesPath parent) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		withAttributesPath.setAttributeParent(parent);
	}

	@Override
	/** {@inheritDoc} */
	public void checkImports(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain, final List<Module> moduleStack) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastImportCheckTimeStamp != null && !lastImportCheckTimeStamp.isLess(timestamp)) {
			return;
		}
 
		final ProjectSourceParser parser = GlobalParser.getProjectSourceParser(project);
		if (parser == null || identifier == null) {
			lastImportCheckTimeStamp = timestamp;
			referredModule = null;
			return;
		}

		final Module temp = referredModule;
		referredModule = parser.getModuleByName(identifier.getName());
		if (temp != referredModule) {
			setUnhandledChange(true);
		}

		if (referredModule == null) {
			identifier.getLocation().reportSemanticError(MessageFormat.format(MISSINGMODULE, identifier.getDisplayName()));
			lastImportCheckTimeStamp = timestamp;
			return;
		}

		moduleStack.add(referredModule);
		if (referenceChain.add(this)) {
			if (hasImportOfImport) {
				if (referredModule instanceof TTCN3Module) {
					final TTCN3Module ttcnmodule = (TTCN3Module) referredModule;
					final List<ImportModule> imports = ttcnmodule.getImports();
					for (final ImportModule importation : imports) {
						referenceChain.markState();
						importation.checkImports(timestamp, referenceChain, moduleStack);
						referenceChain.previousState();
					}
				} else {
					// FIXME There was no test for this case
					location.reportSemanticError("import of imports can only be used on TTCN-3 modules");
				}
			}
			if (hasNormalImport) {
				referredModule.checkImports(timestamp, referenceChain, moduleStack);
			}
		}
		moduleStack.remove(moduleStack.size() - 1);

		lastImportCheckTimeStamp = timestamp; //timestamp can be set only here to handle circular import chains!
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		setUnhandledChange(false);

		if (lastImportCheckTimeStamp != null && !lastImportCheckTimeStamp.isLess(timestamp)) {
			// the flag indicating whether this import is used
			// should be reset only once per semantic check cycle.
			return;
		}

		lastImportCheckTimeStamp = timestamp;

		if (referredModule != null) {
			referredModule.check(timestamp);
		}

		if (withAttributesPath != null) {
			MarkerHandler.removeMarkers(withAttributesPath, true);
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

		usedForImportation = false;
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		return withAttributesPath == null || withAttributesPath.getAttributes() == null
				? List.of(Ttcn3Lexer.WITH)
				: IAppendableSyntax.super.getPossiblePrefixTokens();
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossiblePrefixTokens() {
		return withAttributesPath == null || withAttributesPath.getAttributes() == null
				? List.of(Ttcn3Lexer.PUBLIC, Ttcn3Lexer.PRIVATE)
				: IAppendableSyntax.super.getPossiblePrefixTokens();
	}

	/**
	 * Handles the incremental parsing of this module importation.
	 *
	 * @param reparser
	 *                the parser doing the incremental parsing.
	 * @param isDamaged
	 *                true if the location contains the damaged area, false
	 *                if only its' location needs to be updated.
	 * */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parse();
				identifier = r.getIdentifier();

				// damage handled
				if (result == 0) {
					enveloped = true;
				} else {
					throw new ReParseException(result);
				}
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					reparser.extendDamagedRegion(withAttributesPath.getLocation());
					final int result = reparse( reparser );
					if (result != 0) {
						throw new ReParseException();
					}
					enveloped = true;
				}
			}

			if (!enveloped) {
				throw new ReParseException(1);
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	private int reparse(final TTCN3ReparseUpdater aReparser) {
		return aReparser.parse(parser -> {
			final Pr_reparser_optionalWithStatementContext root = parser.pr_reparser_optionalWithStatement();
			ParserUtilities.logParseTree( root, parser );
			final MultipleWithAttributes attributes = root.attributes;

			final ParseTree rootEof = parser.pr_EndOfFile();
			ParserUtilities.logParseTree( rootEof, parser );
			if ( parser.isErrorListEmpty() ) {
				withAttributesPath.setWithAttributes(attributes);
				if (attributes != null) {
					getLocation().setEndPosition(attributes.getLocation().getEndPosition());
				}
			}
		});
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasImportedAssignmentWithID(final CompilationTimeStamp timestamp, final Identifier identifier) {
		if (referredModule == null) {
			return false;
		}

		if (hasImportOfImport) {
			if (referredModule instanceof TTCN3Module) {
				final TTCN3Module ttcnmodule = (TTCN3Module) referredModule;
				final List<ImportModule> imports = ttcnmodule.getImports();
				for (final ImportModule importation : imports) {
					if (importation.hasImportedAssignmentWithID(timestamp, identifier)) {
						return true;
					}
				}
			}
		}

		if (hasNormalImport) {
			final Assignments assignments = referredModule.getAssignments();
			if (assignments != null && assignments.hasLocalAssignmentWithID(timestamp, identifier)) {
				return true;
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment importAssignment(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain,
			final Identifier moduleId, final Reference reference, final List<ModuleImportation> usedImports) {
		if (referenceChain.contains(this)) {
			return null;
		}

		if (referredModule == null) {
			return null;
		}

		Assignment result = null;
		if (hasImportOfImport) {
			if (referredModule instanceof TTCN3Module) {
				final TTCN3Module ttcnmodule = (TTCN3Module) referredModule;
				Assignment tempResult = null;
				final List<ImportModule> imports = ttcnmodule.getImports();
				for (final ImportModule importation : imports) {
					final List<ModuleImportation> tempUsedImports = new ArrayList<ModuleImportation>();
					// check if it could be reached if
					// visibility is out of question
					referenceChain.markState();
					if (importation.getVisibilityModifier() == VisibilityModifier.Public) {
						tempResult = importation.importAssignment(timestamp, referenceChain, moduleId, reference,
								tempUsedImports);
					} else if (importation.getVisibilityModifier() == VisibilityModifier.Friend) {
						tempResult = importation.importAssignment(timestamp, referenceChain, moduleId, reference,
								tempUsedImports);
						if (tempResult != null) {
							tempUsedImports.add(importation);
						}
					}
					referenceChain.previousState();

					if (tempResult != null) {
						// if found something check if
						// all imports used to find it
						// are visible in the actual
						// module
						boolean visible = true;
						for (final ModuleImportation usedImportation : tempUsedImports) {
							if (usedImportation instanceof ImportModule) {
								final ImportModule ttcnImport = (ImportModule) usedImportation;
								if (!ttcnImport.getMyModule().isVisible(timestamp, myModuleIdentifier, ttcnImport)) {
									visible = false;
								}
							}
						}
						if (visible) {
							usedImports.addAll(tempUsedImports);
							if (result == null) {
								result = tempResult;
							} else if (result != tempResult) {
								// the reference could point to two locations
								reference.getLocation().reportSemanticError(
										"It is not possible to resolve this reference unambigously, as  it can be resolved to `"
												+ result.getFullName() + "' and to `"
												+ tempResult.getFullName() + "'");
								return null;
							}
						}
						tempResult = null;
					}
				}
			}
		}
		if (hasNormalImport) {
			result = referredModule.importAssignment(timestamp, moduleId, reference);
		}

		if (result != null) {
			usedImports.add(this);
			setUsedForImportation();
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		if (identifier != null && !identifier.accept(v)) {
			return false;
		}
		if (withAttributesPath != null && !withAttributesPath.accept(v)) {
			return false;
		}
		if (v.leave(this) == ASTVisitor.V_ABORT) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IDeclaration getDeclaration() {
		return IDeclaration.createInstance(getReferredModule());
	}
	
	@Override
	/** {@inheritDoc} */
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(identifier.getLocation(), SemanticType.Namespace);
	}

	public List<String> getLanguageSpecifications() {
		return languageSpecifications;
	}

	public void setLanguageSpecifications(List<String> languageSpecifications) {
		this.languageSpecifications = languageSpecifications;
	}
	
	/**
	 * Sets the local module name.
	 * @param localIdentifier
	 */
	public void setLocalIdentifier(final Identifier localIdentifier) {
		this.localIdentifier = localIdentifier;
	}
	
	@Override
	public Identifier getIdentifier() {
		return getIdentifier(true);
	}
	
	public Identifier getIdentifier(boolean isLocalIgnored) {
		if (isLocalIgnored || localIdentifier == null) {
			return identifier;
		}
		return localIdentifier;
	}
}
