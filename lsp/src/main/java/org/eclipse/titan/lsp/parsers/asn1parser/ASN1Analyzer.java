/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.asn1parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.Interval;
import org.eclipse.titan.lsp.AST.ASN1.definitions.ASN1Module;
import org.eclipse.titan.lsp.parsers.ISourceAnalyzer;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.TITANMarker;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;

/**
 * ASN1 Analyzer
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Adam Knapp
 */
public class ASN1Analyzer implements ISourceAnalyzer {

	private List<TITANMarker> warnings;
	private List<TITANMarker> unsupportedConstructs;
	private Interval rootInterval;
	private ASN1Module actualAsn1Module = null;
	private TitanErrorListener lexerListener = null;
	private TitanErrorListener parserListener = null;

	@Override
	public List<TITANMarker> getWarnings() {
		return warnings == null ? Collections.emptyList() : warnings;
	}

	@Override
	public List<TITANMarker> getUnsupportedConstructs() {
		return unsupportedConstructs == null ? Collections.emptyList() : unsupportedConstructs;
	}

	@Override
	public ASN1Module getModule() {
		return actualAsn1Module;
	}

	@Override
	public Interval getRootInterval() {
		return rootInterval;
	}

	@Override
	public List<SyntacticErrorStorage> getErrorStorage() {
		if (lexerListener == null || parserListener == null) {
			// the parser was not yet run for some reason.
			return Collections.emptyList();
		}

		if (!lexerListener.getErrorsStored().isEmpty() && parserListener.getErrorsStored().isEmpty()) {
			return lexerListener.getErrorsStored();
		} else if (lexerListener.getErrorsStored().isEmpty() && !parserListener.getErrorsStored().isEmpty()) {
			return parserListener.getErrorsStored();
		} else if (!lexerListener.getErrorsStored().isEmpty() && !parserListener.getErrorsStored().isEmpty()) {
			if (lexerListener.addAll(parserListener.getErrorsStored())) {
				return lexerListener.getErrorsStored();
			}
		}
		return Collections.emptyList();
	}

	@Override
	public void parse(final Path infile, final String code) throws FileNotFoundException {
		final File file = infile.toFile();
		Reader reader = null;

		if (code != null) {
			reader = new StringReader(code);
		} else if (file != null) {
			try {
//				reader = new BufferedReader(new InputStreamReader(getContents(), file.getCharset()));
				
				// LSPFIX : encoding? read all lines?
				reader = new BufferedReader(new FileReader(file));
				((BufferedReader)reader).lines().collect(Collectors.joining(System.lineSeparator()));
				
			} catch (Exception e) {
				TitanLogger.logError("Could not get the contents of `" + file.getName() + "'", e);
				return;
			} finally {
				IOUtils.closeQuietly(reader);
			}
		} else {
			return;
		}

		CharStream charStream;
		try {
			charStream = new ANTLRInputStream(reader);
		} catch (IOException e) {
			IOUtils.closeQuietly(reader);
			return;
		}
		final Asn1Lexer lexer = new Asn1Lexer(charStream);
		lexer.setTokenFactory(new TokenWithIndexAndSubTokensFactory(true));
		lexer.setActualFile(file);
		lexerListener = new TitanErrorListener(file);
		lexer.removeErrorListeners(); // remove ConsoleErrorListener
		lexer.addErrorListener(lexerListener);

		final ModuleLevelTokenStreamTracker tracker = new ModuleLevelTokenStreamTracker(lexer);
		tracker.discard(Asn1Lexer.WS);
		tracker.discard(Asn1Lexer.MULTILINECOMMENT);
		tracker.discard(Asn1Lexer.SINGLELINECOMMENT);
		tracker.setActualFile(file);
		final Asn1Parser parser = new Asn1Parser(tracker);
//		parser.setProject(file.getProject());
		parser.setActualFile(file);
		parser.setBuildParseTree(false);
		parserListener = new TitanErrorListener(file, parser.getErrorStorage());
		parser.removeErrorListeners(); // remove ConsoleErrorListener
		parser.addErrorListener(parserListener);
		parser.pr_ASN1ModuleDefinition();
		actualAsn1Module = parser.getModule();
		warnings = parser.getWarnings();
		unsupportedConstructs = parser.getUnsupportedConstructs();

		IOUtils.closeQuietly(reader);
	}
}
