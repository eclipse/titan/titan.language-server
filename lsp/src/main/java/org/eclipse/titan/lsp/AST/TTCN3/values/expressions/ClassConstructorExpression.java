/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.values.expressions;

import java.text.MessageFormat;
import java.util.ArrayList;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Miklos Magyari
 **/
public class ClassConstructorExpression extends Expression_Value {
	private static final String CLASS_EXPECTED = "Operation `create'' should refer to a class type instead of {0}";
	private static final String TYPEMISMATCH1 = "Type mismatch: reference to a class type was expected in operation `create'' instead of `{0}''";
	private static final String TYPEMISMATCH2 = "Incompatible class type: operation `create'' should refer to `{0}'' instead of `{1}''";
	private static final String TRAITINSTANTIATED = "A trait class cannot be instantiated";
	private static final String ABSTRACTINSTANTIATED = "An abstract class cannot be instantiated";

	private final Reference classReference;
	private final ParsedActualParameters parameters;

	private CompilationTimeStamp checkCreateTimestamp;
	private Class_Type checkCreateCache;
	
	public ClassConstructorExpression(Reference classReference, ParsedActualParameters parameters) {
		this.classReference = classReference;
		this.parameters = parameters;
		
		if (this.parameters.formalParList == null) {
			this.parameters.formalParList = new FormalParameterList(new ArrayList<>());
		}
	}
	
	@Override
	public Operation_type getOperationType() {
		return Operation_type.CLASS_CONSTRUCTOR_OPERATION;
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReference(final CompilationTimeStamp timestamp, final Assignment lhs) {
		// assume no self-ref
		return false;
	}

	@Override
	public IValue evaluateValue(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}

		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;

		if (classReference == null) {
			return lastValue;
		}

		checkExpressionOperands(timestamp, expectedValue, referenceChain);

		return lastValue;
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		myScope = scope;
	}

	@Override
	public Type_type getExpressionReturntype(CompilationTimeStamp timestamp, Expected_Value_type expectedValue) {
		return Type_type.TYPE_CLASS;
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isUnfoldable(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String createStringRepresentation() {
		if (classReference == null) {
			return ERRONEOUS_VALUE;
		}

		final StringBuilder builder = new StringBuilder();
		builder.append(classReference.getDisplayName());
		builder.append(".create");
		if (parameters != null) {
			builder.append(LEFTPARENTHESES);
			builder.append(parameters.createStringRepresentation());
			builder.append(RIGHTPARENTHESES);
		}

		return builder.toString();
	}

	@Override
	protected boolean memberAccept(ASTVisitor v) {
		if (classReference != null && !classReference.accept(v)) {
			return false;
		}
		if (parameters != null && !parameters.accept(v)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the class reference
	 */
	public Reference getClassReference() {
		return classReference;
	}

	/**
	 * @return the parameters
	 */
	public ParsedActualParameters getParameters() {
		return parameters;
	}

	@Override
	public IType getExpressionGovernor(CompilationTimeStamp timestamp, Expected_Value_type expectedValue) {
		return checkCreate(timestamp);
	}

	/**
	 * Checks the parameters of the expression and if they are valid in
	 * their position in the expression or not.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param expectedValue the kind of value expected.
	 * @param referenceChain a reference chain to detect cyclic references.
	 */
	private void checkExpressionOperands(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		checkCreate(timestamp);
		parameters.formalParList.check(timestamp, Assignment_type.A_CONSTRUCTOR);
		checkExpressionDynamicPart(expectedValue, UndefCreateExpression.OPERATIONNAME, false, true, false);
	}

	private Class_Type checkCreate(final CompilationTimeStamp timestamp) {
		if (checkCreateTimestamp != null && !checkCreateTimestamp.isLess(timestamp)) {
			return checkCreateCache;
		}

		checkCreateTimestamp = timestamp;
		checkCreateCache = null;

		if (classReference == null) {
			return null;
		}

		final Assignment assignment = classReference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			setIsErroneous(true);
			return null;
		}

		if (!Assignment_type.A_TYPE.semanticallyEquals(assignment.getAssignmentType())) {
			classReference.getLocation().reportSemanticError(MessageFormat.format(CLASS_EXPECTED, assignment.getDescription()));
			setIsErroneous(true);
			return null;
		}

		final IType type = ((Def_Type) assignment).getType(timestamp).getFieldType(timestamp, classReference, 1,
				Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
		if (type == null) {
			setIsErroneous(true);
			return null;
		}

		if (!Type_type.TYPE_CLASS.equals(type.getTypetype())) {
			classReference.getLocation().reportSemanticError(MessageFormat.format(TYPEMISMATCH1, type.getTypename()));
			setIsErroneous(true);
			return null;
		} else {
			final Class_Type classInstance = (Class_Type)type;
			if (classInstance.isTrait()) {
				getLocation().reportSemanticError(TRAITINSTANTIATED);
				setIsErroneous(true);
			}
			if (classInstance.isAbstract()) {
				getLocation().reportSemanticError(ABSTRACTINSTANTIATED);
				setIsErroneous(true);
			}
		}

		if (myGovernor != null) {
			final IType last = myGovernor.getTypeRefdLast(timestamp);

			if (Type_type.TYPE_CLASS.equals(last.getTypetype()) && !last.isCompatible(timestamp, type, null, null, null)) {
				classReference.getLocation().reportSemanticError(
						MessageFormat.format(TYPEMISMATCH2, last.getTypename(), type.getTypename()));
				setIsErroneous(true);
				return null;
			}
		}

		checkCreateCache = (Class_Type) type;
		return checkCreateCache;
	}
}
