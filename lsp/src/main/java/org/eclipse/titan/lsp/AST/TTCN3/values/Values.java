/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.values;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * A list of values provided either by value notation, or by indexed notation.
 *
 * @author Kristof Szabados
 * */
public final class Values extends ASTNode implements IIncrementallyUpdatable {
	private final boolean isIndexed;
	private ArrayList<IValue> valuesList;
	private ArrayList<IndexedValue> indexedValuesList;

	public Values(final boolean indexed) {
		super();
		isIndexed = indexed;
		if (indexed) {
			indexedValuesList = new ArrayList<IndexedValue>();
		} else {
			valuesList = new ArrayList<IValue>();
		}
	}

	public boolean isIndexed() {
		return isIndexed;
	}

	public void addValue(final IValue value) {
		if (value != null) {
			valuesList.add(value);
			value.setFullNameParent(this);
		}
	}

	public void addIndexedValue(final IndexedValue indexedValue) {
		if (indexedValue != null) {
			indexedValuesList.add(indexedValue);
			indexedValue.setFullNameParent(this);
		}
	}

	public int getNofValues() {
		if (isIndexed) {
			return 0;
		}

		return valuesList.size();
	}

	public int getNofIndexedValues() {
		if (!isIndexed) {
			return 0;
		}

		return indexedValuesList.size();
	}

	public IValue getValueByIndex(final int index) {
		if (isIndexed) {
			return null;
		}

		return valuesList.get(index);
	}

	public IndexedValue getIndexedValueByIndex(final int index) {
		if (!isIndexed) {
			return null;
		}

		return indexedValuesList.get(index);
	}

	public IValue getIndexedValueByRealIndex(final int index) {
		if (!isIndexed) {
			return null;
		}

		for (int i = 0; i < indexedValuesList.size(); i++) {
			final IndexedValue temp = indexedValuesList.get(i);
			final IValue value = temp.getIndex().getValue();
			if (Value_type.INTEGER_VALUE.equals(value.getValuetype())) {
				final Integer_Value integerValue = (Integer_Value) value;
				if (index == integerValue.intValue()) {
					return temp.getValue();
				}
			}
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (isIndexed) {
			indexedValuesList.trimToSize();
			for (int i = 0; i < indexedValuesList.size(); i++) {
				indexedValuesList.get(i).setMyScope(scope);
			}
		} else {
			valuesList.trimToSize();
			for (int i = 0; i < valuesList.size(); i++) {
				valuesList.get(i).setMyScope(scope);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (isIndexed) {
			for (int i = 0; i < indexedValuesList.size(); i++) {
				if (indexedValuesList.get(i) == child) {
					return builder.append(INamedNode.SQUAREOPEN).append(String.valueOf(i)).append(INamedNode.SQUARECLOSE);
				}
			}
		} else {
			for (int i = 0; i < valuesList.size(); i++) {
				if (valuesList.get(i) == child) {
					return builder.append(INamedNode.SQUAREOPEN).append(String.valueOf(i)).append(INamedNode.SQUARECLOSE);
				}
			}
		}

		return builder;
	}

	/**
	 * Handles the incremental parsing of this value list.
	 *
	 * @param reparser
	 *                the parser doing the incremental parsing.
	 * @param isDamaged
	 *                true if the location contains the damaged area, false
	 *                if only its' location needs to be updated.
	 * */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		if (isIndexed) {
			for (final IndexedValue indexedValue : indexedValuesList) {
				indexedValue.updateSyntax(reparser, false);
				reparser.updateLocation(indexedValue.getLocation());
			}
		} else {
			for (final IValue value : valuesList) {
				if (value != null) {
					value.updateSyntax(reparser, false);
					reparser.updateLocation(value.getLocation());
				} else {
					throw new ReParseException();
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (isIndexed) {
			for (final IndexedValue indexedValue : indexedValuesList) {
				indexedValue.findReferences(referenceFinder, foundIdentifiers);
			}
		} else {
			for (final IValue value : valuesList) {
				value.findReferences(referenceFinder, foundIdentifiers);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (isIndexed) {
			for (final IndexedValue indexedValue : indexedValuesList) {
				if (!indexedValue.accept(v)) {
					return false;
				}
			}
		} else {
			for (final IValue value : valuesList) {
				if (!value.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}
}
