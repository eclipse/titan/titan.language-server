/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import static java.util.Map.entry;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionKind;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.codeAction.data.MissingParamTagData;
import org.eclipse.titan.lsp.codeAction.data.MissingParamTagData.MissingParamTagEntry;
import org.eclipse.titan.lsp.common.utils.LSPUtils;

/**
 * Code action provider for document comment actions
 * 
 * @author Miklos Magyari
 */
public class CodeActionsForDocumentComments extends CodeActionBase {
	public enum CodeActionType_DocumentComment {
		ADDMISSINGPARAMETERTAG,
		ADDALLMISSINGPARAMETERTAGS
	}
	
	public CodeActionsForDocumentComments(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		super(doc, diagnostic);
	}
	
	private static final Map<CodeActionType_DocumentComment, BiFunction<TextDocumentIdentifier, Diagnostic, CodeAction>> codeActionList = Map.ofEntries(
		entry(CodeActionType_DocumentComment.ADDMISSINGPARAMETERTAG, CodeActionsForDocumentComments::fixMissingParameterTag),
		entry(CodeActionType_DocumentComment.ADDALLMISSINGPARAMETERTAGS, CodeActionsForDocumentComments::fixAllMissingParameterTags)
	);
	
	private static CodeAction fixMissingParameterTag(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final CodeAction action = new CodeAction("Add missing @param tag");
		action.setKind(CodeActionKind.QuickFix);
		
		final MissingParamTagData paramTagData = getCodeActionData(diagnostic, MissingParamTagData.class);
		final Location firstTag = paramTagData.currentEntry.location;
		final TextEditor editor = new TextEditor(doc);
		final String newLine = LSPUtils.getLineEnding(doc);
		String toInsert = newLine + " * @param " + paramTagData.currentEntry.paramName;
		final WorkspaceEdit edit = editor
			.insertText(new Position(firstTag.getEndLine(), firstTag.getEndColumn()), toInsert)
			.getEdit();
		action.setEdit(edit);
		
		return action;
	}
	
	private static CodeAction fixAllMissingParameterTags(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final CodeAction action = new CodeAction("Add all missing @param tags");
		action.setKind(CodeActionKind.QuickFix);
		
		final MissingParamTagData paramTagData = getCodeActionData(diagnostic, MissingParamTagData.class);
		final Location firstTag = paramTagData.currentEntry.location;
		final TextEditor editor = new TextEditor(doc);
		final String newLine = LSPUtils.getLineEnding(doc);

		StringBuilder sb = new StringBuilder();
		for (MissingParamTagEntry entry : paramTagData.missingParams) {
			sb.append(newLine).append(" * @param ").append(entry.paramName);
		}
		
		final WorkspaceEdit edit = editor
			.insertText(new Position(firstTag.getEndLine(), firstTag.getEndColumn()), sb.toString())	
			.getEdit();
		action.setEdit(edit);
		
		return action;
	}

	@Override
	public List<Either<Command, CodeAction>> provideCodeActions() {
		return provideCodeActions(codeActionList, CodeActionType_DocumentComment.class);
	}
}
