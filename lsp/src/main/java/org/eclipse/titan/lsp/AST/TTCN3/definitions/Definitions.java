/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignments;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IOutlineElement;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.IAppendableSyntax;
import org.eclipse.titan.lsp.common.utils.IteratorFactory;
import org.eclipse.titan.lsp.core.LoadBalancingUtilities;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_reparse_ModuleDefinitionsListContext;

/**
 * The Definitions class represents the scope of module level definitions inside
 * Modules.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Jeno Attila Balasko
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public final class Definitions extends Assignments {
	/** The list of definitions contained in this scope. */
	private final List<Definition> definitionsList;

	/**
	 * A hashmap of definitions, used to find multiple declarations, and to speed up searches.
	 */
	private HashMap<String, Definition> definitionMap;

	/**
	 * Stores those definitions which were identified to be duplicates of
	 * others. This is used to provide much faster operation.
	 * */
	private List<Definition> doubleDefinitions;

	/**
	 * Holds the last time when these definitions were checked, or {@code null} if never.
	 */
	private CompilationTimeStamp lastCompilationTimeStamp;

	/**
	 * Holds the last time when the uniqueness of these definitions were
	 * checked, or {@code null} if never.
	 */
	private CompilationTimeStamp lastUniquenessCheckTimeStamp;

	/** The list of the groups contained in this scope. */
	private final List<Group> groups;

	public Definitions() {
		definitionsList = new CopyOnWriteArrayList<Definition>();
		groups = new ArrayList<Group>();
		scopeName = DEFINITIONS;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final Definition definition : definitionsList) {
			if (definition == child) {
				final Identifier identifier = definition.getIdentifier();

				return builder.append(INamedNode.DOT).append(identifier.getDisplayName());
			}
		}

		for (final Group group : groups) {
			if (group == child) {
				final Identifier identifier = group.getIdentifier();
				return builder.append(INamedNode.DOT).append(identifier.getDisplayName());
			}
		}

		return builder;
	}

	@Override
	public int size() {
		return definitionsList.size();
	}

	@Override
	public Definition get(final int index) {
		return definitionsList.get(index);
	}

	public CompilationTimeStamp getLastCompilationTimeStamp(){
		return lastCompilationTimeStamp;
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		final List<IOutlineElement> outlineDefinitions = new ArrayList<IOutlineElement>();
		// Take care of ordering.
		outlineDefinitions.addAll(definitionsList);
		outlineDefinitions.addAll(groups);
		Collections.sort(outlineDefinitions, IOutlineElement.getComparator());
		return outlineDefinitions.toArray();
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		if (NULL_Location.getInstance().equals(location) && !definitionsList.isEmpty()) {
			List<Location> locList = definitionsList.stream()
				.filter(def -> !NULL_Location.getInstance().equals(def.getLocation()))
				.map(def -> def.getLocation())
				.sorted(Location.getComparator())
				.collect(Collectors.toList());
			final Location startLoc = locList.get(0);
			final Location endLoc = locList.get(locList.size()-1);
			location = new Location(startLoc.getFile(), startLoc.getStartPosition(), endLoc.getEndPosition());
		}
		return location;
	}

	@Override
	public boolean add(final Assignment def) {
		return addAll(Arrays.asList(def));
	}

	/**
	 * Adds a list of definitions to the list of definitions.
	 * <p>
	 * The scope of the newly added definitions are set to this scope scope here.
	 *
	 * @param definitionList the definitions to be added
	 */
	@Override
	public synchronized boolean addAll(final Collection<? extends Assignment> definitionList) {
		lastUniquenessCheckTimeStamp = null;
		if (definitionList == null) {
			return false;
		}
		boolean changed = false;
		for (final Assignment definition : definitionList) {
			if (definition != null && definition.getIdentifier() != null
					&& definition.getIdentifier().getLocation() != null
					&& definition instanceof Definition) {
				definition.setMyScope(this);
				definition.setFullNameParent(this);
				changed |= definitionsList.add((Definition)definition);
			}
		}
		return changed;
	}
	
	public synchronized void clear() {
		definitionsList.clear();
	}

	/**
	 * Adds a group to the list of groups.
	 * <p>
	 * The scope of the newly added group is set to this scope here.
	 *
	 * @param group the group to be added
	 */
	public void addGroup(final Group group) {
		if (group != null && group.getIdentifier() != null && group.getIdentifier().getLocation() != null) {
			group.setMyScope(this);
			groups.add(group);
			group.setFullNameParent(this);
		}
	}

	/**
	 * Checks the uniqueness of the definitions, and also builds a hashmap
	 * of them to speed up further searches.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	//checkUniquiness has been splitted into to parts because
	//the check should be done at the beginning of the check but the reporting shall be done finally
	protected void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastUniquenessCheckTimeStamp != null && !lastUniquenessCheckTimeStamp.isLess(timestamp)) {
			return;
		}

		createDefinitionMap(timestamp); //creates a hash map
		reportDoubleDefinitions();
	}

	// creates or refreshes DefinitionMap and doubleDefinitions but not reports the found double definitons
	private void createDefinitionMap(final CompilationTimeStamp timestamp) {
		if (lastUniquenessCheckTimeStamp != null && !lastUniquenessCheckTimeStamp.isLess(timestamp)) {
			return;
		}
		lastUniquenessCheckTimeStamp = timestamp;

		if (doubleDefinitions != null) {
			doubleDefinitions.clear();
		}

		//(rebuild) definitionMap and doubleDefinitions from the updated field "definitions"
		definitionMap = new HashMap<String, Definition>(definitionsList.size());
		for (final Definition definition : definitionsList) {
			final String definitionName = definition.getIdentifier().getName();
			if (definitionMap.containsKey(definitionName)) {
				if (doubleDefinitions == null) {
					doubleDefinitions = new ArrayList<Definition>();
				}
				doubleDefinitions.add(definition);
			} else {
				definitionMap.put(definitionName, definition);
			}
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public List<Definition> getVisibleDefinitions() {
		return Collections.unmodifiableList(definitionsList);
	}

	//reports the found double definitions. It is supposed doubleDefinition to be created already
	private void reportDoubleDefinitions() {
		if (doubleDefinitions == null) {
			return;
		}
		for (final Definition definition : doubleDefinitions) {
			final Identifier identifier = definition.getIdentifier();
			final String definitionName = identifier.getName();
			final Location otherLocation = definitionMap.get(definitionName).getIdentifier().getLocation();
			otherLocation.reportSingularSemanticError(MessageFormat.format(DUPLICATEDEFINITIONFIRST,
					identifier.getDisplayName()));
			identifier.getLocation().reportSemanticError(
					MessageFormat.format(DUPLICATEDEFINITIONREPEATED, identifier.getDisplayName()));
		}
	}

	/**
	 * Checks the uniqueness of the groups, and after that the groups themselves..
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	private void checkGroups(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (groups.isEmpty()) {
			return;
		}

		final HashMap<String, Group> groupMap = new HashMap<String, Group>(groups.size());
		final HashMap<String, Definition> defs = new HashMap<String, Definition>(definitionsList.size());

		//This defs is necessary because the definitionMaps is not ready yet
		for (final Definition definition : definitionsList) {
			if (definition.getParentGroup() == null) {
				final String defName = definition.getIdentifier().getName();
				if (!defs.containsKey(defName)) {
					defs.put(defName, definition);
				} //duplication reported by checkUniqueness
			}
		}

		for (final Group group : groups) {
			if (group == null) {
				continue; //paranoia
			}

			final String groupName = group.getIdentifier().getName();
			if (defs.containsKey(groupName)) {
				group.getIdentifier().getLocation().reportSemanticError(MessageFormat.format(Group.GROUPCLASHGROUP, groupName));
				defs.get(groupName).getIdentifier().getLocation()
				.reportSingularSemanticError(MessageFormat.format(Group.GROUPCLASHDEFINITION, groupName));
			}
			if (groupMap.containsKey(groupName)) {
				groupMap.get(groupName).getIdentifier().getLocation()
				.reportSingularSemanticError(MessageFormat.format(Group.DUPLICATEGROUPFIRST, groupName));
				group.getIdentifier().getLocation()
				.reportSemanticError(MessageFormat.format(Group.DUPLICATEGROUPREPEATED, groupName));
			} else {
				groupMap.put(groupName, group);
			}
		}

		for (final Group temp : groups) {
			temp.check(timestamp);
		}
	}

	/**
	 * Refresh all markers on groups, definitions
	 */
	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;

		final Module module = getModuleScope();
		if (module != null) {
			if (module.getSkippedFromSemanticChecking()) {
				return;
			}
		}

		//markers on imports cannot be removed, they are already refreshed

		//These positions will be used to remove last comments within Definitions
		Position maxEndPosition = getLocation().getStartPosition();
		Position lastEndPosition;

		for (final Definition definition : definitionsList) {
			if (definition.getLastTimeChecked() == null || definition.getLastTimeChecked().isLess(timestamp) ){
				MarkerHandler.removeMarkers(definition, true); //doubleDefinition report will be deleted!
			}
			lastEndPosition = definition.getLocation().getEndPosition();
			if (lastEndPosition.after(maxEndPosition)) {
				maxEndPosition = lastEndPosition;
			}
		}

		for (final Group group: groups) {
			group.markMarkersForRemoval(timestamp);
		}

		//remove markers on the final commented lines (for incremental parsing)
		final Position defsEndPosition = getLocation().getEndPosition();
		if (maxEndPosition.before(defsEndPosition)) {
			final Location loc = new Location(getLocation().getFile(), maxEndPosition, defsEndPosition);
			MarkerHandler.removeMarkers(loc, true);
		}

		checkUniqueness(timestamp);
		checkGroups(timestamp);
		for( final Definition definition: definitionsList){
			definition.check(timestamp); //it calls definition.checkUniqueness!
		}

		reportDoubleDefinitions(); //perhaps checkUniqueness() was executed earlier and their markers have been removed
	}

	/**
	 * Experimental method for BrokenPartsViaInvertedImports.
	 * The only difference to check() that not all assignments will be rechecked, just the listed in "assignments"
	 */
	public void checkWithDefinitions(final CompilationTimeStamp timestamp, final List<Assignment> assignments) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}
		if (lastCompilationTimeStamp == null){
			//fall back to checking each definition:
			check(timestamp);
			return;
		}

		lastCompilationTimeStamp = timestamp;

		//markers on imports cannot be removed, the are already fresh

		for (final Assignment assignment : assignments) {
			if (assignment.getLastTimeChecked() == null || assignment.getLastTimeChecked().isLess(timestamp)){
				MarkerHandler.removeMarkers(assignment, true);
			}
		}

		Position maxEndPosition = getLocation().getEndPosition();
		Position lastEndPosition = new Position(0, 0);

		for (final Definition definition : definitionsList){
			lastEndPosition = definition.getLocation().getEndPosition();
			if (lastEndPosition.after(maxEndPosition)) {
				maxEndPosition = lastEndPosition;
			}
		}

		for (final Group group: groups){
			group.markMarkersForRemoval(timestamp);
		}

		//remove markers on the final commented lines (for incremental parsing)
		final Position defsEndPosition = getLocation().getEndPosition();
		if (maxEndPosition.before(defsEndPosition)) {
			final Location loc = new Location(getLocation().getFile(), maxEndPosition, defsEndPosition);
			MarkerHandler.removeMarkers(loc, true);
		}

		checkUniqueness(timestamp);
		checkGroups(timestamp);
		for (final Assignment assignmentFrom : assignments) {
			assignmentFrom.check(timestamp);
			LoadBalancingUtilities.astNodeChecked();
		}

		reportDoubleDefinitions();//perhaps checkUniqueness() was executed earlier and their markers have been removed
	}


	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (isBuildCancelled()) {
			return;
		}

		final Module module = getModuleScope();
		if (module != null && module.getSkippedFromSemanticChecking()) {
			return;
		}

		for (final Definition temp : definitionsList) {
			temp.postCheck();
		}
		for (final Group group : groups) {
			group.postCheck();
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		if (reference.getModuleIdentifier() != null) {
			return getModuleScope().getAssBySRef(timestamp, reference);
		}

		final Identifier identifier = reference.getId();
		if (identifier == null) {
			return getModuleScope().getAssBySRef(timestamp, reference);
		}

		if (lastUniquenessCheckTimeStamp == null) {
			createDefinitionMap(timestamp);
		} //uniqueness shall be reported only after checking all the definitions

		final Definition result = definitionMap.get(identifier.getName());
		if (result != null) {
			return result;
		}

		return getParentScope().getAssBySRef(timestamp, reference);
	}

	/**
	 * Searches the definitions for one with a given Identifier.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param id the identifier used to find the definition
	 * @return the definition if found, or null otherwise
	 */
	@Override
	public Definition getLocalAssignmentByID(final CompilationTimeStamp timestamp, final Identifier id) {
		if (lastUniquenessCheckTimeStamp == null) {
			checkUniqueness(timestamp);
		}
		return definitionMap.get(id.getName());
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasLocalAssignmentWithID(final CompilationTimeStamp timestamp, final Identifier identifier) {
		if (lastUniquenessCheckTimeStamp == null) {
			checkUniqueness(timestamp);
		}
		return definitionMap.containsKey(identifier.getName());
	}

	/**
	 * Handles the incremental parsing of this list of definitions.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param importedModules the list of module importations found in the same module.
	 * @param friendModules the list of friend module declaration in the same module.
	 * @param controlpart the control part found in the same module.
	 * @throws ReParseException if there was an error while refreshing the location
	 *                 information and it could not be solved internally.
	 */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final List<ImportModule> importedModules,
			final List<FriendModule> friendModules, final ControlPart controlpart) throws ReParseException {
		// calculate damaged region
		int result = 0;
		boolean enveloped = false;
		int nofDamaged = 0;
		Position leftBoundary = location.getStartPosition();
		Position rightBoundary = location.getEndPosition();
		final Position damageOffset = reparser.getDamageStartPosition();
		final Position damageEndOffset = reparser.getDamageEndPosition();
		IAppendableSyntax lastAppendableBeforeChange = null;
		IAppendableSyntax lastPrependableBeforeChange = null;

		if (controlpart != null) {
			final Location tempLocation = new Location(controlpart.getLocation());

			if (controlpart.hasDocumentComment()) {
				final Location docStart = controlpart.getDocumentComment().getCommentLocation();
				if (docStart != null && !docStart.equals(NULL_Location.INSTANCE)) {
					if (tempLocation.getStartPosition().after(docStart.getStartPosition())) {
						tempLocation.setStartPosition(docStart.getStartPosition());
					}
				}
			}

			if (reparser.envelopsDamage(tempLocation)) {
				enveloped = true;
			} else if (!reparser.isDamaged(tempLocation)) {
				if (tempLocation.getEndPosition().before(damageOffset) && tempLocation.getEndPosition().after(leftBoundary)) {
					leftBoundary = tempLocation.getEndPosition();
					lastAppendableBeforeChange = controlpart;
				}
				if (tempLocation.getStartPosition().after(damageEndOffset) && tempLocation.getStartPosition().before(rightBoundary)) {
					rightBoundary = tempLocation.getStartPosition();
					lastPrependableBeforeChange = controlpart;
				}
			}
		}

		Position lastOffset = new Position(0, 0);
		for (int i = 0, size = groups.size(); i < size && !enveloped; i++) {
			final Group tempGroup = groups.get(i);
			final Location tempLocation = tempGroup.getLocation();
			if (reparser.envelopsDamage(tempLocation)) {
				enveloped = true;
				leftBoundary = tempLocation.getStartPosition();
				rightBoundary = tempLocation.getEndPosition();
			} else if (reparser.isDamaged(tempLocation)) {
				nofDamaged++;
			} else {
				if (tempLocation.getEndPosition().before(damageOffset) && tempLocation.getEndPosition().after(leftBoundary)) {
					leftBoundary = tempLocation.getEndPosition();
					lastAppendableBeforeChange = tempGroup;
				}
				if (tempLocation.getStartPosition().after(damageEndOffset) && tempLocation.getStartPosition().before(rightBoundary)) {
					rightBoundary = tempLocation.getStartPosition();
					lastPrependableBeforeChange = tempGroup;
				}
				if (lastOffset.before(tempLocation.getEndPosition())) {
					lastOffset = tempLocation.getEndPosition();
				}
			}
		}

		for (int i = 0, size = importedModules.size(); i < size && !enveloped; i++) {
			final ImportModule tempImport = importedModules.get(i);
			if (tempImport.getParentGroup() == null) {
				final Location tempLocation = tempImport.getLocation();
				if (reparser.envelopsDamage(tempLocation)) {
					enveloped = true;
					leftBoundary = tempLocation.getStartPosition();
					rightBoundary = tempLocation.getEndPosition();
				} else if (reparser.isDamaged(tempLocation)) {
					nofDamaged++;
				} else {
					if (tempLocation.getEndPosition().before(damageOffset) && tempLocation.getEndPosition().after(leftBoundary)) {
						leftBoundary = tempLocation.getEndPosition();
						lastAppendableBeforeChange = tempImport;
					}
					if (tempLocation.getStartPosition().after(damageEndOffset) && tempLocation.getStartPosition().before(rightBoundary)) {
						rightBoundary = tempLocation.getStartPosition();
						lastPrependableBeforeChange = tempImport;
					}
					if (lastOffset.before(tempLocation.getEndPosition())) {
						lastOffset = tempLocation.getEndPosition();
					}
				}
			}
		}

		for (int i = 0, size = friendModules.size(); i < size && !enveloped; i++) {
			final FriendModule tempFriend = friendModules.get(i);
			if (tempFriend.getParentGroup() == null) {
				final Location tempLocation = tempFriend.getLocation();
				if (reparser.envelopsDamage(tempLocation)) {
					enveloped = true;
					leftBoundary = tempLocation.getStartPosition();
					rightBoundary = tempLocation.getEndPosition();
				} else if (reparser.isDamaged(tempLocation)) {
					nofDamaged++;
				} else {
					if (tempLocation.getEndPosition().before(damageOffset) && tempLocation.getEndPosition().after(leftBoundary)) {
						leftBoundary = tempLocation.getEndPosition();
						lastAppendableBeforeChange = tempFriend;
					}
					if (tempLocation.getStartPosition().after(damageEndOffset) && tempLocation.getStartPosition().before(rightBoundary)) {
						rightBoundary = tempLocation.getStartPosition();
						lastPrependableBeforeChange = tempFriend;
					}
					if (lastOffset.before(tempLocation.getEndPosition())) {
						lastOffset = tempLocation.getEndPosition();
					}
				}
			}
		}

		for (int i = 0, size = definitionsList.size(); i < size && !enveloped; i++) {
			final Definition temp = definitionsList.get(i);
			if (temp.getParentGroup() == null) {
				final Location tempLocation = new Location(temp.getLocation());
				final Location cumulativeLocation = new Location(temp.getCumulativeDefinitionLocation());

 				if (temp.hasDocumentComment()) {
					final Location docStart = temp.getDocumentComment().getCommentLocation();
					if (docStart != null && !docStart.equals(NULL_Location.INSTANCE)) {
						if (tempLocation.getStartPosition().after(docStart.getStartPosition())) {
							tempLocation.setStartPosition(docStart.getStartPosition());
						}
						if (cumulativeLocation.getStartPosition().after(docStart.getStartPosition())) {
							cumulativeLocation.setStartPosition(docStart.getStartPosition());
						}
					}
				} else if (reparser.documentCommentInserted()) {
					if (tempLocation.getStartLine() - 1 == reparser.getOriginalModificationStartPosition().getLine()) {
						tempLocation.setStartPosition(reparser.getOriginalModificationStartPosition());
						cumulativeLocation.setStartPosition(reparser.getOriginalModificationStartPosition());
					}
				}
 
				if (tempLocation.equals(cumulativeLocation) && reparser.envelopsDamage(cumulativeLocation)) {
					enveloped = true;
					leftBoundary = cumulativeLocation.getStartPosition();
					rightBoundary = cumulativeLocation.getEndPosition();
				} else if (reparser.isDamaged(cumulativeLocation)) {
					nofDamaged++;
					if (reparser.getDamageStartPosition().equals(cumulativeLocation.getEndPosition())) {
						lastAppendableBeforeChange = temp;
					} else if (reparser.getDamageEndPosition().equals(cumulativeLocation.getStartPosition())) {
						lastPrependableBeforeChange = temp;
					}
				} else {
					if (cumulativeLocation.getEndPosition().before(damageOffset) && cumulativeLocation.getEndPosition().after(leftBoundary)) {
						leftBoundary = cumulativeLocation.getEndPosition();
						lastAppendableBeforeChange = temp;
					}
					if (cumulativeLocation.getStartPosition().after(damageEndOffset) && cumulativeLocation.getStartPosition().before(rightBoundary)) {
						rightBoundary = cumulativeLocation.getStartPosition();
						lastPrependableBeforeChange = temp;
					}
					if (lastOffset.before(tempLocation.getEndPosition())) {
						lastOffset = tempLocation.getEndPosition();
					}
				}
			}
		}

		// extend the reparser to the calculated values if the damage
		// was not enveloped
		if (!enveloped && reparser.isDamaged(location)) {
			// if there is an element that is right now being
			// extended we should add it to the damaged domain as
			// the extension might be correct
			if (lastAppendableBeforeChange != null) {
				final boolean isBeingExtended = reparser.startsWithFollow(lastAppendableBeforeChange.getPossibleExtensionStarterTokens());
				if (isBeingExtended) {
					leftBoundary = lastAppendableBeforeChange.getLocation().getStartPosition();
					nofDamaged++;
					enveloped = false;
					reparser.extendDamagedRegion(leftBoundary, rightBoundary);
				}
			}

			if (lastPrependableBeforeChange != null) {
				final List<Integer> temp = lastPrependableBeforeChange.getPossiblePrefixTokens();

				if (reparser.endsWithToken(temp)) {
					rightBoundary = lastPrependableBeforeChange.getLocation().getEndPosition();
					nofDamaged++;
					enveloped = false;
					reparser.extendDamagedRegion(leftBoundary, rightBoundary);
				}
			}

			if (nofDamaged != 0) {
				// remove damaged elements
				removeElementsInRange(reparser, importedModules, friendModules);

				if (doubleDefinitions != null) {
					doubleDefinitions.clear();
				}
				lastUniquenessCheckTimeStamp = null;
				lastCompilationTimeStamp = null;
			}

			//extend damaged region till the neighbor definitions just here to avoid calculating something damaged or extended:
			//Perhaps it should be moved even farther:
			reparser.extendDamagedRegion(leftBoundary, rightBoundary);
		}

		// update what is left
		for (int i = 0; i < groups.size(); i++) {
			final Group temp = groups.get(i);
			final Location tempLocation = temp.getLocation();
			if (reparser.isAffected(tempLocation)) {
				try {
					temp.updateSyntax(reparser, importedModules, definitionsList, friendModules);
				} catch (ReParseException e) {
					if (e.getDepth() == 1) {
						enveloped = false;
						groups.remove(i);
						i--;
						reparser.extendDamagedRegion(tempLocation);
						result = 1;
					} else {
						if (doubleDefinitions != null) {
							doubleDefinitions.clear();
						}
						lastUniquenessCheckTimeStamp = null;
						throw new ReParseException(e);
					}
				}
			}
		}

		for (int i = 0; i < importedModules.size(); i++) {
			final ImportModule temp = importedModules.get(i);
			if (temp.getParentGroup() == null) {
				final Location tempLocation = temp.getLocation();
				if (reparser.isAffected(tempLocation)) {
					try {
						final boolean isDamaged = enveloped && reparser.envelopsDamage(tempLocation);
						temp.updateSyntax(reparser, enveloped && reparser.envelopsDamage(tempLocation));
						if(isDamaged) {
							((TTCN3Module) parentScope).checkRoot();
						}
					} catch (ReParseException e) {
						if (e.getDepth() == 1) {
							enveloped = false;
							importedModules.remove(i);
							i--;
							reparser.extendDamagedRegion(tempLocation);
							result = 1;
						} else {
							if (doubleDefinitions != null) {
								doubleDefinitions.clear();
							}
							lastUniquenessCheckTimeStamp = null;
							throw new ReParseException(e);
						}
					}
				}
			}
		}

		for (int i = 0; i < friendModules.size(); i++) {
			final FriendModule temp = friendModules.get(i);
			if (temp.getParentGroup() == null) {
				final Location tempLocation = temp.getLocation();
				if (reparser.isAffected(tempLocation)) {
					try {
						final boolean isDamaged = enveloped && reparser.envelopsDamage(tempLocation);
						temp.updateSyntax(reparser, enveloped && reparser.envelopsDamage(tempLocation));
						if(isDamaged) {
							((TTCN3Module) parentScope).checkRoot();
						}
					} catch (ReParseException e) {
						if (e.getDepth() == 1) {
							enveloped = false;
							friendModules.remove(i);
							i--;
							reparser.extendDamagedRegion(tempLocation);
							result = 1;
						} else {
							if (doubleDefinitions != null) {
								doubleDefinitions.clear();
							}
							lastUniquenessCheckTimeStamp = null;
							throw new ReParseException(e);
						}
					}
				}
			}
		}

		for (final Definition temp : definitionsList) {
			if (temp.getParentGroup() == null) {
				final Location tempLocation = new Location(temp.getLocation());
				final Location cumulativeLocation = new Location(temp.getCumulativeDefinitionLocation());

				if (temp.hasDocumentComment()) {
					final Location docStart = temp.getDocumentComment().getCommentLocation();
					if (docStart != null && !docStart.equals(NULL_Location.INSTANCE)) {
						if (tempLocation.getStartPosition().after(docStart.getStartPosition())) {
							tempLocation.setStartPosition(docStart.getStartPosition());
						}
						if (cumulativeLocation.getStartPosition().after(docStart.getStartPosition())) {
							cumulativeLocation.setStartPosition(docStart.getStartPosition());
						}
					}
				}

				if (reparser.isAffected(cumulativeLocation)) {
					try {
						final boolean isDamaged = enveloped && reparser.envelopsDamage(tempLocation);
						temp.updateSyntax(reparser, isDamaged);
						if (reparser.getNameChanged()) {
							if (doubleDefinitions != null) {
								doubleDefinitions.clear();
							}
							lastUniquenessCheckTimeStamp = null;
							lastCompilationTimeStamp = null;//to recheck the whole module
							reparser.setNameChanged(false);
							// This could also spread
						}
						if(isDamaged) {
							temp.checkRoot();//TODO lets move this into the definitions
						}
					} catch (ReParseException e) {
						if (e.getDepth() == 1) {
							enveloped = false;
							/*
							 * Existing references to this definition must be cleared, otherwise
							 * the definition will stay alive and getRefdAssignment() will
							 * still return it, instead of the new definition.
							 */
							temp.clearExistingReferences();
							definitionsList.remove(temp);
							reparser.extendDamagedRegion(cumulativeLocation);
							result = 1;
						} else {
							if (doubleDefinitions != null) {
								doubleDefinitions.clear();
							}
							lastUniquenessCheckTimeStamp = null;
							throw new ReParseException(e);
						}
					}
				}
			}
		}

		if (result == 1) {
			removeElementsInRange(reparser, importedModules, friendModules);
			if (doubleDefinitions != null) {
				doubleDefinitions.clear();
			}
			lastUniquenessCheckTimeStamp = null;
			lastCompilationTimeStamp = null;
		}

		for (final Group temp : groups) {
			final Location tempLocation = temp.getLocation();
			if (reparser.isAffected(tempLocation)) {
				reparser.updateLocation(tempLocation);
			}
		}

		for (final ImportModule temp : importedModules) {
			if (temp.getParentGroup() == null) {
				final Location tempLocation = temp.getLocation();
				if (reparser.isAffected(tempLocation)) {
					reparser.updateLocation(tempLocation);
				}
			}
		}

		for (final FriendModule temp : friendModules) {
			if (temp.getParentGroup() == null) {
				final Location tempLocation = temp.getLocation();
				if (reparser.isAffected(tempLocation)) {
					reparser.updateLocation(tempLocation);
				}
			}
		}

		for (final Definition temp : definitionsList) {
			if (temp.getParentGroup() == null) {
				final Location tempLocation = temp.getLocation();
				final Location cumulativeLocation = temp.getCumulativeDefinitionLocation();
				if (reparser.isAffected(tempLocation)) {
					if(tempLocation != cumulativeLocation) {
						reparser.updateLocation(cumulativeLocation);
					}
					reparser.updateLocation(tempLocation);
				}
			}
			if (temp.hasDocumentComment()) {
				final Location docLocation = temp.getDocumentComment().getCommentLocation();
				if (reparser.isAffected(docLocation)) {
					reparser.updateLocation(docLocation);
				}
			}
		}

		// there is no control part and the modification happened after everything else
		final boolean tempIsControlPossible = controlpart == null && lastOffset.before(leftBoundary);
		if (!enveloped) {
			if(reparser.envelopsDamage(location)){
				reparser.extendDamagedRegion(leftBoundary, rightBoundary);
				result = reparse( reparser, tempIsControlPossible );
				result = Math.max(result - 1, 0);
				lastCompilationTimeStamp = null;
			} else {
				result = Math.max(result, 1);
			}
		}

		if (result == 0) {
			lastUniquenessCheckTimeStamp = null;
		} else {
			if (doubleDefinitions != null) {
				doubleDefinitions.clear();
			}
			lastUniquenessCheckTimeStamp = null;
			throw new ReParseException(result);
		}
	}

	private int reparse( final TTCN3ReparseUpdater aReparser, final boolean aTempIsControlPossible ) {
		return aReparser.parse(parser -> {
			final List<Definition> allDefinitions = new ArrayList<Definition>();
			final List<Definition> localDefinitions = new ArrayList<Definition>();
			final List<Group> localGroups = new ArrayList<Group>();
			final List<ImportModule> allImports = new ArrayList<ImportModule>();
			final List<ImportModule> localImports = new ArrayList<ImportModule>();
			final List<FriendModule> allFriends = new ArrayList<FriendModule>();
			final List<FriendModule> localFriends = new ArrayList<FriendModule>();
			List<ControlPart> controlParts = null;
			if (aTempIsControlPossible) {
				controlParts = new ArrayList<ControlPart>();
			}

			final TTCN3Module module = (TTCN3Module) parentScope;
			parser.setModule((TTCN3Module) parentScope);
			final Pr_reparse_ModuleDefinitionsListContext root =
					parser.pr_reparse_ModuleDefinitionsList( null, allDefinitions, localDefinitions, localGroups, allImports,
							localImports, allFriends, localFriends, controlParts );
			ParserUtilities.logParseTree( root, parser );

			if ( parser.isErrorListEmpty() ) {
				addAll(allDefinitions);
				if (doubleDefinitions != null) {
					doubleDefinitions.clear();
				}
				lastUniquenessCheckTimeStamp = null;

				for (final ImportModule impmod : allImports) {
					module.addImportedModule(impmod);
				}

				for (final Group group : localGroups) {
					addGroup(group);
				}

				module.addFriendModules(allFriends);
				if (controlParts != null && controlParts.size() == 1) {
					((TTCN3Module) parentScope).addControlpart(controlParts.get(0));
				}
			}
		});
	}

	/**
	 * Destroy every element trapped inside the damage radius.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param importedModules the list of module importations found in the same module.
	 * @param friendModules the list of friend module declaration found in the same module.
	 */
	private void removeElementsInRange(final TTCN3ReparseUpdater reparser, final List<ImportModule> importedModules,
			final List<FriendModule> friendModules) {
		for (int i = groups.size() - 1; i >= 0; i--) {
			final Group temp = groups.get(i);
			if (reparser.isDamaged(temp.getLocation())) {
				reparser.extendDamagedRegion(temp.getLocation());
				groups.remove(i);
			}
		}

		final ArrayList<ImportModule> importsToBeRemoved = new ArrayList<ImportModule>();
		for (final ImportModule temp : importedModules) {
			if (reparser.isDamaged(temp.getLocation())) {
				reparser.extendDamagedRegion(temp.getLocation());
				importsToBeRemoved.add(temp);
			}
		}
		importedModules.removeAll(importsToBeRemoved);

		final ArrayList<FriendModule> friendsToBeRemoved = new ArrayList<FriendModule>();
		for (final FriendModule temp : friendModules) {
			if (reparser.isDamaged(temp.getLocation())) {
				reparser.extendDamagedRegion(temp.getLocation());
				friendsToBeRemoved.add(temp);
			}
		}
		friendModules.removeAll(friendsToBeRemoved);

		final ArrayList<Definition> toBeRemoved = new ArrayList<Definition>();
		for (final Definition temp : definitionsList) {
			if (reparser.isDamaged(temp.getCumulativeDefinitionLocation())) {
				reparser.extendDamagedRegion(temp.getCumulativeDefinitionLocation());
				toBeRemoved.add(temp);
			}
		}
		definitionsList.removeAll(toBeRemoved);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		if (definitionsList == null) {
			return null;
		}
		for (final Definition definition : definitionsList) {
			if (definition.getLocation().containsPosition(offset)) {
				return definition;
			}
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		final List<Definition> tempList = new ArrayList<Definition>(definitionsList);
		for (final Definition definition : tempList) {
			definition.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final Definition definition : definitionsList) {
			if (!definition.accept(v)) {
				return false;
			}
		}
		for (final Group g : groups) {
			if (!g.accept(v)) {
				return false;
			}
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}

	@Override
	/** {@inheritDoc} */
	public Iterator<Assignment> iterator() {
		return IteratorFactory.getIterator((List<? extends Assignment>)definitionsList);
	}

	public void setGenName() {
		for (final Definition definition: definitionsList) {
			definition.setGenName(definition.getIdentifier().getName());
		}
	}

	public Map<String, Definition> getDefinitionMap() {
		return definitionMap;
	}

	public List<Definition> getDefinitions() {
		return definitionsList;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isClassScope() {
		return parentScope.isClassScope();
	}
}
