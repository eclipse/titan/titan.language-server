/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the list of type mapping targets provided in a single type mapping.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class TypeMappingTargets extends ASTNode
	implements IIncrementallyUpdatable, ICollection<TypeMappingTarget> {

	private static final String FULLNAMEPART = ".<target ";

	private final List<TypeMappingTarget> targets = new ArrayList<TypeMappingTarget>();

	@Override
	public boolean add(final TypeMappingTarget mappingTarget) {
		mappingTarget.setFullNameParent(this);
		return targets.add(mappingTarget);
	}

	@Override
	public int size() {
		return targets.size();
	}

	@Override
	public TypeMappingTarget get(final int index) {
		return targets.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = targets.size(); i < size; i++) {
			final TypeMappingTarget target = targets.get(i);
			if (target == child) {
				return builder.append(FULLNAMEPART).append(i + 1).append(MORETHAN);
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (TypeMappingTarget tmt : targets) {
			tmt.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final TypeMappingTarget tmt : targets) {
			tmt.updateSyntax(reparser, false);
			reparser.updateLocation(tmt.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final TypeMappingTarget tmt : targets) {
			tmt.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final TypeMappingTarget tmt : targets) {
			if (!tmt.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<TypeMappingTarget> iterator() {
		return targets.iterator();
	}
}
