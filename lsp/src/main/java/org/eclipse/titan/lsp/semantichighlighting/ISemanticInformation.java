/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.semantichighlighting;

/**
 * This interface represents an AST object that can provide semantic information about itself
 * (tokens and/or modifiers).
 * 
 * @author Miklos Magyari
 * */
public interface ISemanticInformation {
	/**
	 * Sets semantic tokens and/or modifiers for this node. If overridden, it is preferred to
	 * call <code>super.setSemanticInformation()</code> as well if the superclass also implements
	 * this interface.
	 */
	default void setSemanticInformation() {
		// Do nothing by default
	}
}
