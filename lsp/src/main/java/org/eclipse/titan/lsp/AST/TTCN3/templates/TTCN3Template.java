/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import java.util.List;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.GovernedSimple;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferenceChainElement;
import org.eclipse.titan.lsp.AST.IReferencingType;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Choice_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Sequence_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Set_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.Open_Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.types.Anytype_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Array_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Choice_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Sequence_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Set_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimension;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the templates of the TTCN-3 language.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public abstract class TTCN3Template extends GovernedSimple
	implements IReferenceChainElement, ITTCN3Template, ICancelBuild {
	
	protected static final String RESTRICTIONERROR = "Restriction ''value'' or ''omit'' on {0} does not allow usage of `{1}''";
	protected static final String OMITRESTRICTIONERROR = "Restriction ''omit'' on {0} does not allow usage of `{1}''";
	protected static final String VALUERESTRICTIONERROR = "Restriction ''value'' on {0} does not allow usage of {1}";
	protected static final String PRESENTRESTRICTIONERROR = "Restriction ''present'' on {0} does not allow usage of `{1}''";
	private static final String LENGTHRESTRICTIONERROR = "Restriction on {0} does not allow usage of length restriction";
	protected static final String REFERENCETONONEXISTENTFIELD = "Reference to non-existent {0} field `{1}'' in type `{2}''";
	protected static final String SPECIFIC_VALUE_EXPECTED = "A specific value expected instead of a(n) `{0}''";
	protected static final String SPECIFIC_VALUE_EXPECTED_OMIT = "A specific value expected instead of a(n) `omit value'";
	protected static final String LENGTHRESTRICTIONERROR_OMIT = "Length restriction cannot be used with omit value";
	private static final String LENGTHRESTRICTIONERROR_TEMPLATE = "Length restriction cannot be used in a template of type `{0}''";
	protected static final String IFPRESENT_NOT_ALLOWED = "`ifpresent' is not allowed here";
	private static final String INACTIVE_FIELD_REF_ERROR =
			"Reference to inactive field `{0}'' in a template of union type `{1}''. The active field is `{2}''";
	private static final String REF_ERROR = "Reference to field `{0}'' of {1} `{2}''";
	private static final String INVALID_FIELD_REF = "Invalid field reference `{0}'': type `{1}'' does not have fields";

	/** The type of the template, which also happens to be its governor. */
	protected IType myGovernor;

	/** Length restriction. */
	protected LengthRestriction lengthRestriction;

	/** ifpresent flag. */
	protected boolean isIfpresent;

	/**
	 * Indicates whether it has been verified that the template is free of
	 * matching symbols.
	 */
	protected boolean specificValueChecked;

	/**
	 * Pointer to the base template (or template field) that this template
	 * is derived from. It is set in modified templates (including in-line
	 * modified templates) and only if the compiler is able to determine the
	 * base template. It is null otherwise.
	 */
	protected ITTCN3Template baseTemplate;

	protected BuildTimestamp lastTimeBuilt;

	@Override
	/** {@inheritDoc} */
	public Setting_type getSettingtype() {
		return Setting_type.S_TEMPLATE;
	}

	@Override
	/** {@inheritDoc} */
	public final void copyGeneralProperties(final ITTCN3Template original) {
		location = original.getLocation();
		super.setFullNameParent(original.getNameParent());
		myGovernor = original.getMyGovernor();
		myScope = original.getMyScope();
		lengthRestriction = original.getLengthRestriction();
		isIfpresent = original.getIfPresent();
	}

	@Override
	/** {@inheritDoc} */
	public final IType getMyGovernor() {
		return myGovernor;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyGovernor(final IType governor) {
		myGovernor = governor;
	}

	@Override
	/** {@inheritDoc} */
	public String chainedDescription() {
		return "template reference: " + getFullName();
	}

	@Override
	/** {@inheritDoc} */
	public final Location getChainLocation() {
		return getLocation();
	}

	@Override
	/** {@inheritDoc} */
	public final void setLengthRestriction(final LengthRestriction lengthRestriction) {
		if (lengthRestriction != null) {
			this.lengthRestriction = lengthRestriction;
		}
	}

	@Override
	/** {@inheritDoc} */
	public final LengthRestriction getLengthRestriction() {
		return lengthRestriction;
	}

	@Override
	public final boolean hasLengthRestriction() {
		return lengthRestriction != null;
	}

	@Override
	/** {@inheritDoc} */
	public final void setIfpresent() {
		isIfpresent = true;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean getIfPresent() {
		return isIfpresent;
	}

	@Override
	/** {@inheritDoc} */
	public final ITTCN3Template getBaseTemplate() {
		return baseTemplate;
	}

	@Override
	/** {@inheritDoc} */
	public final void setBaseTemplate(final ITTCN3Template baseTemplate) {
		this.baseTemplate = baseTemplate;
	}

	@Override
	/** {@inheritDoc} */
	public final Completeness_type getCompletenessConditionSeof(final CompilationTimeStamp timestamp, final boolean incompleteAllowed) {
		if (!incompleteAllowed) {
			return Completeness_type.MUST_COMPLETE;
		}

		if (baseTemplate == null) {
			return Completeness_type.MAY_INCOMPLETE;
		}

		final ITTCN3Template temp = baseTemplate.getTemplateReferencedLast(timestamp);
		if (temp.getIsErroneous(timestamp)) {
			return Completeness_type.MAY_INCOMPLETE;
		}

		switch (temp.getTemplatetype()) {
		case TEMPLATE_NOTUSED:
		case ANY_VALUE:
		case ANY_OR_OMIT:
		case TEMPLATE_REFD:
		case TEMPLATE_INVOKE:
		case NAMED_TEMPLATE_LIST:
		case INDEXED_TEMPLATE_LIST:
			return Completeness_type.MAY_INCOMPLETE;
		case TEMPLATE_LIST:
			if (myGovernor == null) {
				return Completeness_type.MAY_INCOMPLETE;
			}

			final IType type = myGovernor.getTypeRefdLast(timestamp);
			if (type == null) {
				return Completeness_type.MAY_INCOMPLETE;
			}

			switch (type.getTypetype()) {
			case TYPE_SEQUENCE_OF:
			case TYPE_SET_OF:
				return Completeness_type.PARTIAL;
			default:
				return Completeness_type.MAY_INCOMPLETE;
			}
		default:
			return Completeness_type.MUST_COMPLETE;
		}
	}

	@Override
	/** {@inheritDoc} */
	public final Completeness_type getCompletenessConditionChoice(final CompilationTimeStamp timestamp, final boolean incompleteAllowed,
			final Identifier fieldName) {
		if (!incompleteAllowed) {
			return Completeness_type.MUST_COMPLETE;
		}

		if (baseTemplate == null) {
			return Completeness_type.MAY_INCOMPLETE;
		}

		final ITTCN3Template temp = baseTemplate.getTemplateReferencedLast(timestamp);
		if (temp.getIsErroneous(timestamp)) {
			return Completeness_type.MAY_INCOMPLETE;
		}

		switch (temp.getTemplatetype()) {
		case TEMPLATE_NOTUSED:
		case ANY_VALUE:
		case ANY_OR_OMIT:
		case TEMPLATE_REFD:
		case TEMPLATE_INVOKE:
		case TEMPLATE_LIST:
			return Completeness_type.MAY_INCOMPLETE;
		case NAMED_TEMPLATE_LIST:
			if (((Named_Template_List) temp).contains(fieldName)) {
				return Completeness_type.MAY_INCOMPLETE;
			}

			return Completeness_type.MUST_COMPLETE;
		default:
			return Completeness_type.MUST_COMPLETE;
		}
	}

	@Override
	/** {@inheritDoc} */
	public final ITTCN3Template getTemplateReferencedLast(final CompilationTimeStamp timestamp) {
		final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final ITTCN3Template result = getTemplateReferencedLast(timestamp, referenceChain);
		referenceChain.release();

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public TTCN3Template getTemplateReferencedLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		return this;
	}

	@Override
	public void checkSpecificValue(CompilationTimeStamp timestamp, boolean allowOmit) {
		if (isBuildCancelled()) {
			return;
		}
		getLocation().reportSemanticError(MessageFormat.format(SPECIFIC_VALUE_EXPECTED, getTemplatetype().getName()));
	}

	/**
	 * Helper function, for checking union typed templates whether field
	 * access is allowed for a given template sub reference or not.
	 *
	 * @param fieldIdentifier the identifier of the field to search for.
	 * @param reference the reference to report the error to.
	 * @param type the type of the template.
	 */
	private ITTCN3Template getRefdUnionFieldTemplate(final Identifier fieldIdentifier, final Reference reference, final IType type) {
		if (!Template_type.NAMED_TEMPLATE_LIST.equals(getTemplatetype())) {
			return null;
		}

		final Named_Template_List namedList = (Named_Template_List) this;
		if (namedList.size() != 1) {
			// invalid template, the error is already reported
			return null;
		}

		final NamedTemplate namedTemplate = namedList.get(0);
		if (namedTemplate.getName().equals(fieldIdentifier)) {
			return namedTemplate.getTemplate();
		}

		if (!reference.getUsedInIsbound()) {
			reference.getLocation().reportSemanticError(MessageFormat.format(INACTIVE_FIELD_REF_ERROR,
					fieldIdentifier.getDisplayName(), type.getTypename(), namedTemplate.getName().getDisplayName()));
		}

		return null;
	}

	/**
	 * Helper function, for checking set and sequence typed templates whether
	 * field access is allowed for a given template sub reference or not.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param fieldIdentifier the identifier of the field to search for.
	 * @param reference the reference to report the error to.
	 * @param referenceChain the reference chain use to detect circular references.
	 * @param silent {@code true} if errors are not to be reported.
	 */
	public ITTCN3Template getReferencedSetSequenceFieldTemplate(final CompilationTimeStamp timestamp, final Identifier fieldIdentifier,
			final Reference reference, final IReferenceChain referenceChain, final boolean silent) {
		return null;
	}

	/**
	 * Helper function, checks whether field access is allowed for a given
	 * template sub reference or not.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param fieldIdentifier the identifier of the field to search for.
	 * @param reference the reference to report the error to.
	 * @param referenceChain the reference chain use to detect circular references.
	 * @param silent {@code true} if errors are not to be reported.
	 */
	protected ITTCN3Template getReferencedFieldTemplate(final CompilationTimeStamp timestamp, final Identifier fieldIdentifier,
			final Reference reference, final IReferenceChain referenceChain, final boolean silent) {
		switch (getTemplatetype()) {
		case OMIT_VALUE:
		case ANY_VALUE:
		case ANY_OR_OMIT:
		case VALUE_LIST:
		case COMPLEMENTED_LIST:
		case SUBSET_MATCH:
		case SUPERSET_MATCH:
		case PERMUTATION_MATCH:
		case BSTR_PATTERN:
		case HSTR_PATTERN:
		case OSTR_PATTERN:
		case CSTR_PATTERN:
		case USTR_PATTERN:
			// TODO compiler problem: check if the list is complete
			if (!silent) {
				reference.getLocation().reportSemanticError(
					MessageFormat.format(REF_ERROR, fieldIdentifier.getDisplayName(), getTemplateTypeName(), getFullName()));
			}
			break;
		default:
			break;
		}

		final IType tempType = myGovernor.getTypeRefdLast(timestamp);
		if (tempType.getIsErroneous(timestamp)) {
			return null;
		}

		switch (tempType.getTypetype()) {
		case TYPE_ASN1_CHOICE:
			if (!((ASN1_Choice_Type) tempType).hasComponentWithName(fieldIdentifier)) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getRefdUnionFieldTemplate(fieldIdentifier, reference, tempType);
		case TYPE_TTCN3_CHOICE:
			if (!((TTCN3_Choice_Type) tempType).hasComponentWithName(fieldIdentifier.getName())) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getRefdUnionFieldTemplate(fieldIdentifier, reference, tempType);
		case TYPE_OPENTYPE:
			if (!((Open_Type) tempType).hasComponentWithName(fieldIdentifier)) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getRefdUnionFieldTemplate(fieldIdentifier, reference, tempType);
		case TYPE_ANYTYPE:
			if (!((Anytype_Type) tempType).hasComponentWithName(fieldIdentifier.getName())) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getRefdUnionFieldTemplate(fieldIdentifier, reference, tempType);
		case TYPE_ASN1_SEQUENCE:
			if (!((ASN1_Sequence_Type) tempType).hasComponentWithName(fieldIdentifier)) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getReferencedSetSequenceFieldTemplate(timestamp, fieldIdentifier, reference, referenceChain, silent);
		case TYPE_TTCN3_SEQUENCE:
			if (!((TTCN3_Sequence_Type) tempType).hasComponentWithName(fieldIdentifier.getName())) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getReferencedSetSequenceFieldTemplate(timestamp, fieldIdentifier, reference, referenceChain, silent);
		case TYPE_ASN1_SET:
			if (!((ASN1_Set_Type) tempType).hasComponentWithName(fieldIdentifier)) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getReferencedSetSequenceFieldTemplate(timestamp, fieldIdentifier, reference, referenceChain, silent);
		case TYPE_TTCN3_SET:
			if (!((TTCN3_Set_Type) tempType).hasComponentWithName(fieldIdentifier.getName())) {
				if (!silent) {
					reference.getLocation().reportSemanticError(
						MessageFormat.format(REFERENCETONONEXISTENTFIELD, tempType.getTypetype().getName(),
								fieldIdentifier.getDisplayName(), tempType.getTypename()));
				}
				return null;
			}

			return getReferencedSetSequenceFieldTemplate(timestamp, fieldIdentifier, reference, referenceChain, silent);
		default:
			if (!silent) {
				reference.getLocation().reportSemanticError(
					MessageFormat.format(INVALID_FIELD_REF, fieldIdentifier.getDisplayName(), tempType.getTypename()));
			}
			return null;
		}
	}

	/**
	 * Checks whether array indexing is allowed for a given template sub
	 * reference or not.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param arrayIndex the index to check.
	 * @param referenceChain the reference chain use to detect circular references.
	 * @param silent {@code true} if errors are not to be reported.
	 */
	protected ITTCN3Template getReferencedArrayTemplate(final CompilationTimeStamp timestamp, final IValue arrayIndex,
			final IReferenceChain referenceChain, final boolean silent) {
		switch (getTemplatetype()) {
		case OMIT_VALUE:
		case ANY_VALUE:
		case ANY_OR_OMIT:
		case VALUE_LIST:
		case COMPLEMENTED_LIST:
		case SUPERSET_MATCH:
		case SUBSET_MATCH:
			if (!silent) {
				arrayIndex.getLocation().reportSemanticError(
					MessageFormat.format("Reference with index to an element of {0} `{1}''",
							getTemplateTypeName(), getFullName()));
			}
			break;
		default:
			break;
		}

		IValue indexValue = arrayIndex.setLoweridToReference(timestamp);
		indexValue = indexValue.getValueRefdLast(timestamp, referenceChain);
		if (indexValue.getIsErroneous(timestamp)) {
			return null;
		}

		long index = 0;
		if (!indexValue.isUnfoldable(timestamp)) {
			if (Value_type.INTEGER_VALUE.equals(indexValue.getValuetype())) {
				index = ((Integer_Value) indexValue).getValue();
			} else if (!silent) {
				arrayIndex.getLocation().reportSemanticError("An integer value was expected as index");
				return null;
			}
		} else {
			return null;
		}

		final IType tempType = myGovernor.getTypeRefdLast(timestamp);
		if (tempType.getIsErroneous(timestamp)) {
			return null;
		}

		switch (tempType.getTypetype()) {
		case TYPE_SEQUENCE_OF:
			if (index < 0) {
				if (!silent) {
					final String message = MessageFormat
						.format("A non-negative integer value was expected instead of {0} for indexing a template of `sequence of'' type `{1}''",
								index, tempType.getTypename());
					arrayIndex.getLocation().reportSemanticError(message);
				}
				return null;
			} else if (!Template_type.TEMPLATE_LIST.equals(getTemplatetype())) {
				return null;
			} else {
				final int nofElements = ((Template_List) this).size();
				if (index > nofElements) {
					if (!silent) {
						final String message = MessageFormat
							.format("Index overflow in a template of `sequence of'' type `{0}'': the index is {1}, but the template has only {2} elements",
									tempType.getTypename(), index, nofElements);
						arrayIndex.getLocation().reportSemanticError(message);
					}
					return null;
				}
			}
			break;
		case TYPE_SET_OF:
			if (index < 0) {
				if (!silent) {
					final String message = MessageFormat
						.format("A non-negative integer value was expected instead of {0} for indexing a template of `set of'' type `{1}''",
								index, tempType.getTypename());
					arrayIndex.getLocation().reportSemanticError(message);
				}
				return null;
			} else if (!Template_type.TEMPLATE_LIST.equals(getTemplatetype())) {
				return null;
			} else {
				final int nofElements = ((Template_List) this).size();
				if (index > nofElements) {
					if (!silent) {
						final String message = MessageFormat
							.format("Index overflow in a template of `set of'' type `{0}'': the index is {1}, but the template has only {2} elements",
									tempType.getTypename(), index, nofElements);
						arrayIndex.getLocation().reportSemanticError(message);
					}
					return null;
				}
			}
			break;
		case TYPE_ARRAY: {
			final ArrayDimension dimension = ((Array_Type) tempType).getDimension();
			dimension.checkIndex(timestamp, indexValue, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
			if (Template_type.TEMPLATE_LIST.equals(getTemplatetype()) && !dimension.getIsErroneous(timestamp)) {
				// re-base the index
				index -= dimension.getOffset();
				if (index < 0 || index > ((Template_List) this).size()) {
					if (!silent) {
						arrayIndex.getLocation().reportSemanticError(
							MessageFormat.format("The index value {0} is outside the array indexable range", index
									+ dimension.getOffset()));
					}
					return null;
				}
			} else {
				return null;
			}
			break;
		}
		default: {
			if (!silent) {
				final String message = MessageFormat.format("Invalid array element reference: type `{0}'' cannot be indexed",
					tempType.getTypename());
				arrayIndex.getLocation().reportSemanticError(message);
			}
			return null;
		}
		}
		if (this instanceof Template_List) {
			final ITTCN3Template returnValue = ((Template_List) this).get((int) index);
			if (Template_type.TEMPLATE_NOTUSED.equals(returnValue.getTemplatetype())) {
				return baseTemplate == null ? null :
					 baseTemplate.getTemplateReferencedLast(timestamp, referenceChain)
							.getReferencedArrayTemplate(timestamp, indexValue, referenceChain, silent);
			}

			return returnValue;
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public final ITTCN3Template getReferencedSubTemplate(final CompilationTimeStamp timestamp, final Reference reference,
			final IReferenceChain referenceChain, final boolean silent) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		ITTCN3Template template = this;
		for (int i = 1; i < subreferences.size(); i++) {
			if (template == null) {
				return template;
			}

			template = template.getTemplateReferencedLast(timestamp, referenceChain);
			if (template.getIsErroneous(timestamp)) {
				return template;
			}
			template = template.setLoweridToReference(timestamp);

			if (Template_type.TEMPLATE_REFD.equals(template.getTemplatetype())) {
				// unfoldable
				return null;
			} else if (Template_type.SPECIFIC_VALUE.equals(template.getTemplatetype())) {
				((SpecificValue_Template) template).getValue().getReferencedSubValue(timestamp, reference, i, referenceChain);
			}

			final ISubReference ref = subreferences.get(i);
			if (Subreference_type.fieldSubReference.equals(ref.getReferenceType())) {
				template = ((TTCN3Template) template).getReferencedFieldTemplate(timestamp, ref.getId(), reference, referenceChain, silent);
			} else if (Subreference_type.arraySubReference.equals(ref.getReferenceType())) {
				template = ((TTCN3Template) template).getReferencedArrayTemplate(timestamp, ((ArraySubReference) ref).getValue(),
						referenceChain, silent);
			} else {
				// error found
				return this;
			}
		}

		return template;
	}

	/**
	 * Performs post checks for {@code checkThisTemplateGeneric}.
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param type the type this template should be checked against
	 * @param allowOmit enables the acceptance of omit templates.
	 * @param subCheck enables the checking of sub types.
	 */
	protected final void checkThisTemplateGenericPostChecks(final CompilationTimeStamp timestamp,
			final IType type, final boolean allowOmit, final boolean subCheck) {
		checkLengthRestriction(timestamp, type);
		if (!allowOmit && isIfpresent) {
			location.reportSemanticError(IFPRESENT_NOT_ALLOWED);
		}
		if (subCheck) {
			type.checkThisTemplateSubtype(timestamp, this);
		}
	}

	/**
	 * Checks that if there is a length restriction applied to this
	 * template, it is semantically correct.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param type the type the template is being checked against.
	 */
	protected final void checkLengthRestriction(final CompilationTimeStamp timestamp, final IType type) {
		if (!hasLengthRestriction()) {
			return;
		}
		lengthRestriction.setMyScope(myScope);
		lengthRestriction.check(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
		if (type instanceof IReferencingType) {
			final IReferenceChain refChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			final IType last = ((IReferencingType) type).getTypeRefd(timestamp, refChain);
			refChain.release();
			if (!last.getIsErroneous(timestamp)) {
				checkLengthRestriction(timestamp, last);
			}
			return;
		}

		if (type.getIsErroneous(timestamp)) {
			return;
		}

		final Type_type typeType = type.getTypetypeTtcn3();
		switch (typeType) {
		case TYPE_PORT:
			// the error was already reported.
			return;
		case TYPE_ARRAY:
			lengthRestriction.checkArraySize(timestamp, ((Array_Type) type).getDimension());
			break;
		case TYPE_BITSTRING:
		case TYPE_HEXSTRING:
		case TYPE_OCTETSTRING:
		case TYPE_CHARSTRING:
		case TYPE_UCHARSTRING:
		case TYPE_SEQUENCE_OF:
		case TYPE_SET_OF:
			break;
		default:
			lengthRestriction.getLocation().reportSemanticError(MessageFormat.format(LENGTHRESTRICTIONERROR_TEMPLATE, type.getTypename()));
			return;
		}

		checkTemplateSpecificLengthRestriction(timestamp, typeType);
	}

	/**
	 * Does the template specific part of checking the length restriction.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param typeType the TTCN-3 type-type to describe the type.
	 */
	protected void checkTemplateSpecificLengthRestriction(final CompilationTimeStamp timestamp, final Type_type typeType) {
		// In the general implementation we cannot verify anything on
		// the matching mechanisms
		// they are either correct or not applicable to the type
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {

		if (type == null) {
			return false;
		}

		boolean selfReference = false;
		if (!getIsErroneous(timestamp)) {

			if ( !(Template_type.TEMPLATE_NOTUSED.equals(getTemplatetype())) ) {
				selfReference = type.checkThisTemplate(timestamp, this, isModified, implicitOmit, lhs);
			}

			checkThisTemplateGenericPostChecks(timestamp, type, allowOmit, subCheck);
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRestrictionCommon(final CompilationTimeStamp timestamp, final String definitionName, final TemplateRestriction.Restriction_type templateRestriction, final Location usageLocation) {
		switch (templateRestriction) {
		case TR_VALUE:
		case TR_OMIT:
			if (hasLengthRestriction()) {
				usageLocation.reportSemanticError(MessageFormat.format(LENGTHRESTRICTIONERROR, definitionName));
			}
			if (isIfpresent) {
				usageLocation.reportSemanticError(MessageFormat.format(RESTRICTIONERROR, definitionName, IFPRESENT));
			}
			break;
		case TR_PRESENT:
			if (isIfpresent || getTemplateReferencedLast(timestamp).getIfPresent()) {
				usageLocation.reportSemanticError(MessageFormat.format(PRESENTRESTRICTIONERROR, definitionName, IFPRESENT));
			}
			//if TR_NONE -> warning
			break;
		default:
			return;
		}
	}

	/**
	 * Checks template restriction using common data members of this class.
	 * 
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param definitionName name for the error/warning message
	 * @param omitAllowed {@code true} in case of TR_OMIT, {@code false} in case of TR_VALUE
	 * @param usageLocation the location to be used for reporting errors
	 */
	protected final void checkRestrictionCommon(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName,
				omitAllowed ? TemplateRestriction.Restriction_type.TR_OMIT : TemplateRestriction.Restriction_type.TR_VALUE, usageLocation);
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, omitAllowed, usageLocation);
		usageLocation.reportSemanticError(MessageFormat.format(RESTRICTIONERROR, definitionName, getTemplateTypeName()));
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkPresentRestriction(final CompilationTimeStamp timestamp, final String definitionName, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, TemplateRestriction.Restriction_type.TR_PRESENT, usageLocation);
		return false;
	}

	/** Test if omit is allowed in a value list
	 * <p>
	 *  Uses TITANFlagsOptionsData.ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY:
	 *  (It is the same as -M flag of makefilegen) <p>
	 *  If it is true the old syntax allowed.
	 *  If it is false then only the new syntax is allowed.<p>
	 *	For example:<p/>
	 *	 ( 1 ifpresent, 2 ifpresent, omit ) //=> allowed in old solution,
	 *                                           not allowed in new solution (3 error markers)<p>
	 *	 ( 1, 2 ) ifpresent //= only this allowed in new solution when this function returns false<p>
	 *
	 * @param allowOmit true if the field is optional field,
	 *                  false if the field is mandatory.<p>
	 *                  Of course in this case omit value and the ifpresent clause is prohibitied=> returns false<p>
	 * @return
	 *   If allowOmit == false it returns false
	 *   ( quick exit for mandatory fields).
	 *	 If allowOmit == true it returns according to the
	 *	 project property setting
	 *   TITANFlagsOptionsData.ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY
	 */
	public static final boolean allowOmitInValueList(final Location location, final boolean allowOmit) {
		if ( !allowOmit ) {
			return false;
		}

		if (location == null || NULL_Location.getInstance().equals(location)) {
			return true;
		}

//	FIXLSP	final IResource f = location.getFile();
//		if( f == null) {
//			return true;
//		}
//
//		final IProject project = f.getProject();
//		if(project == null) {
//			return true;
//		}
//
//		final QualifiedName qn = new QualifiedName(ProjectBuildPropertyData.QUALIFIER,TITANFlagsOptionsData.ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY);
//		try {
//			final String s= project.getPersistentProperty(qn);
//			return ( "true".equals(s));
//		} catch (CoreException e) {
//			TitanLogger.logError(e);
//			return true;
//		}
		return true;
	}

	/**
	 * Handles the incremental parsing of this template.
	 * The common {@link TTCN3Template#lengthRestriction} and {@link TTCN3Template#baseTemplate}
	 * fields are checked here.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged <li>{@code true} if the location contains the damaged area,</li>
	 * 				    <li>{@code false} if only its' location needs to be updated. </li>
	 * @throws ReParseException when the mechanism was not able to handle the re-parsing need at a node.
	 */
	@Override
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		ITTCN3Template.super.updateSyntax(reparser, isDamaged);

		if (lengthRestriction != null) {
			lengthRestriction.updateSyntax(reparser, false);
			reparser.updateLocation(lengthRestriction.getLocation());
		}

		if (baseTemplate != null) {
			baseTemplate.updateSyntax(reparser, false);
			reparser.updateLocation(baseTemplate.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (!hasLengthRestriction()) {
			return;
		}
		lengthRestriction.findReferences(referenceFinder, foundIdentifiers);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (hasLengthRestriction() && !lengthRestriction.accept(v)) {
			return false;
		}
		return true;
	}

	/**
	 * Returns whether the Java initialization sequence requires a
	 * temporary variable reference to be introduced for efficiency reasons.
	 */
	public boolean needsTemporaryReference() {
		return hasLengthRestriction() || isIfpresent;
	}

	/**
	 * Adds the {@code length restriction} and {@code ifpresent} attributes
	 * to the end of the specified StringBuilder
	 * @param sb the StringBuilder to collect the additional content
	 * @return the same StringBuilder containing the additional content
	 */
	protected final StringBuilder addToStringRepresentation(final StringBuilder sb) {
		if (hasLengthRestriction()) {
			sb.append(lengthRestriction.createStringRepresentation());
		}
		return isIfpresent ? sb.append(IFPRESENT) : sb;
	}
}
