/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.util.Map;
import java.util.Optional;

import static java.util.Map.entry;

import java.util.function.BiFunction;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;

public class FactoryForDiagnosticData implements ICodeActionProviderFactory {
	private static Map<CodeActionProviderType, BiFunction<TextDocumentIdentifier, Diagnostic, ICodeActionProvider>> codeActionProviders = Map.ofEntries(
				entry(CodeActionProviderType.CLASS, (doc, diagnostic) -> new CodeActionsForClassType(doc, diagnostic)),
				entry(CodeActionProviderType.MISSINGFUNCTION, (doc, diagnostic) -> new CodeActionsForMissingFunction(doc, diagnostic)),
				entry(CodeActionProviderType.MISSINGRETURN, (doc, diagnostic) -> new CodeActionForMissingReturn(doc, diagnostic)),
				entry(CodeActionProviderType.MISSINGPARAMTAG, (doc, diagnostic) -> new CodeActionsForDocumentComments(doc, diagnostic))
			);

	@Override
	public ICodeActionProvider createProvider(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final Optional<CodeActionProviderType> providerType = ((DiagnosticData) diagnostic.getData()).getCodeActionProviderType();
		if (providerType.isPresent()) {
			return codeActionProviders.get(providerType.get()).apply(doc, diagnostic);
		}

		return null;
	}
}
