/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.compiler.BuildTimestamp;

/**
 * @author Kristof Szabados
 * */
public interface IGovernedSimple extends IGoverned {

	enum CodeSectionType {
		/** Unknown (i.e. not specified) */
		CS_UNKNOWN,
		/**
		 * Initialized before processing the configuration file.
		 * Example: constants, default value for module parameters.
		 */
		CS_PRE_INIT,
		/**
		 * Initialized after processing the configuration file.
		 * Example: non-parameterized templates.
		 */
		CS_POST_INIT,
		/**
		 * Initialized with the component entities.
		 * Example: initial value for component variables, default
		 * duration for timers.
		 */
		CS_INIT_COMP,
		/**
		 * Initialized immediately at the place of definition.
		 * Applicable to local definitions only.
		 * Example: initial value for a local variable.
		 */
		CS_INLINE
	}

	/**
	 * Returns the build timestamp of the last time this governedsimple was built.
	 * @return the build timestamp of the last time this governedsimple was built
	 *  */
	BuildTimestamp getLastTimeBuilt();

	/**
	 * Return whether the value/template needs type compatibility conversion
	 * during code generation.
	 * 
	 * @return {@code true} if the code generator will need to generate type
	 *         conversion, {@code false} otherwise.
	 * */
	boolean get_needs_conversion();

	/**
	 * Indicates that this value/template will need type conversion code generated.
	 * */
	void set_needs_conversion();

	/**
	 * Returns whether the entity is a top-level one (i.e. it is not
	 * embedded into another entity). The function examines whether the
	 * genname is a single identifier or not.
	 * */
	boolean isTopLevel();
}
