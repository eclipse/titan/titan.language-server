/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.implementation;

import org.eclipse.titan.lsp.titanium.metrics.common.IMetric;
import org.eclipse.titan.lsp.titanium.metrics.common.IMetricEnum;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricData;

/**
 * A base implementation of {@link IMetric}.
 *
 * @author poroszd
 *
 */
abstract class BaseMetric<E, M extends IMetricEnum> implements IMetric<E, M> {
	private final M metric;

	BaseMetric(final M metric) {
		this.metric = metric;
	}

	@Override
	public final M getMetric() {
		return metric;
	}

	@Override
	public void init(final MetricData data) {
		//intentionally left empty
	}
}
