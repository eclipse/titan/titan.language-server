/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.TTCN3Scope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter.parameterEvaluationType;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template.Template_type;
import org.eclipse.titan.lsp.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.utils.StringUtils;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.AST.TTCN3.templates.NamedParameter;
import org.eclipse.titan.lsp.AST.TTCN3.templates.NamedParameters;
import org.eclipse.titan.lsp.AST.TTCN3.templates.NotUsed_Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstances;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The FormalParameterList class represents the formal parameter lists found in TTCN3.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class FormalParameterList extends TTCN3Scope
	implements ILocateableNode, IIncrementallyUpdatable, ICollection<FormalParameter> {

	public enum IsIdenticalResult {
		RES_DIFFERS,
		RES_NAME_DIFFERS,
		RES_IDENTICAL
	}

	public static final String FULLNAMEPART = ".<unknown_parameter>";
	public static final String DUPLICATEPARAMETERFIRST = "Duplicate parameter with name `{0}'' was first declared here";
	public static final String DUPLICATEPARAMETERREPEATED = "Duplicate parameter with name `{0}'' was declared here again";
	private static final String HIDINGSCOPEELEMENT = "Parameter with identifier `{0}'' is not unique in the scope hierarchy";
	private static final String HIDINGMODULEIDENTIFIER = "Parameter with name `{0}'' hides a module identifier";
	private static final String MISSINGPARAMETER = "There is no value specified for formal parameter `{0}''";
	private static final String ILLEGALACTIVATEPARAMETER = "Parameter {0} of {1} refers to {2},"
			+ " which is a local definition within a statement block and may have shorter lifespan than the activated default."
			+ " Only references to variables and timers defined in the component type can be passed to activated defaults";
	private static final String CANNOTBESTARTED = "a parameter or embedded in a parameter of a function used in a start operation."
			+ " {0} `{1}'' cannot be start on a parallel test component";
	private static final String WILLREMAINUNCHANGED =
		"{0} `{1}'' started on parallel test components. Its `out'' and `inout'' parameters will remain unchanged at the end of the operation.";
	private static final String CONSTRUCTORPARAMIN = "A constructor cannot have {0}";
	private static final String LAZY_PARAMETER_ERROR = "Formal parameter `{0}'' cannot be @{1}, not supported in this case.";
	private static final String OUT_OF_ORDER_ERROR = "Named parameter `{0}'' is out of order";
	private static final String MULTIPLE_ASSIGNMENT_ERROR = "Named parameter `{0}'' is assigned more than once";
	private static final String PARAMETER_MISSING = "{0} `{1}'' has no formal parameter with name `{2}''";
	private static final String NONMATCHING_PARAMETER_NUMBER = "Too {0} parameters: {1} was expected instead of {2}";
	private static final String TOO_FEW_MANY_PARAMETER = "Too {0} parameters: at {1} {2} was expected instead of {3}";
	private static final String NOT_USED_SYMBOL_ERROR = "Not used symbol (`-'') cannot be used for parameter that does not have a default value";
	private static final String PTC_ERROR = "{0} `{1}'' cannot be started on a parallel test component because it has {2}";
	private static final String PARAMETER_KIND_TYPE_ERROR = "The {0} of the {1} parameter is not the same: `{2}'' was expected instead of `{3}''";
	private static final String SUBTYPE_MISMATCH = "Subtype mismatch: subtype `{0}'' has no common value with subtype `{1}''";
	private static final String TEMPLATE_RESTRICTION_ERROR = "The template restriction of the {0} parameter is not the same: `{1}'' was expected instead of `{2}''";
	private static final String EVALUATION_TYPE_ERROR = "{0} parameter evaluation type (normal, @lazy or @fuzzy) mismatch";
	private static final String NAME_MISMATCH = "The name of the {0} parameter is not the same: `{1}'' was expected instead of `{2}''";
	private static final String ACTIVATING_LAZY_FUZZY_NOT_ALLOWED = "Activating a default altstep with @lazy or @fuzzy parameters is not supported";
	private static final String BACKWARD = "An altstep activated as default cannot have {0} according to the TTCN-3 standard. It is allowed in TITAN for backward compatibility";

	private final List<FormalParameter> parameters = new ArrayList<>();
	private Location location = Location.getNullLocation();

	/** the definition containing this formal parameter list. */
	private Definition myDefinition;

	/** the time when this formal parameter list was check the last time. */
	private CompilationTimeStamp lastTimeChecked;

	/**
	 * the time when this formal parameter list was checked for uniqueness the last time.
	 */
	private CompilationTimeStamp lastTimeUniquenessChecked;

	/** A map of the parameters. */
	private Map<String, FormalParameter> parameterMap;

	/**
	 * The minimum number of actual parameters that might be considered a
	 * valid list for this list of formal parameters.
	 */
	private int minimumNofParameters = 0;

	/** stores whether functions with this formal parameter list can be started or not */
	private boolean isStartable;

	public FormalParameterList(final List<FormalParameter> parameters) {
		for (final FormalParameter parameter : parameters) {
			if (parameter != null && parameter.getIdentifier() != null) {
				this.parameters.add(parameter);
				parameter.setFullNameParent(this);
				parameter.setFormalParamaterList(this);
			}
		}
	}

	@Override
	public boolean add(final FormalParameter fp) {
		return !isExistingFormalParameter(fp) && parameters.add(fp);
	}

	private boolean isExistingFormalParameter(final FormalParameter fp) {
		for (final FormalParameter param : parameters) {
			if (param.getFullName().equals(fp.getFullName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final FormalParameter parameter : parameters) {
			if (parameter == child) {
				final Identifier identifier = parameter.getIdentifier();
				return builder.append(INamedNode.DOT).append((identifier != null) ? identifier.getDisplayName() : FULLNAMEPART);
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	public Definition getMyDefinition() {
		return myDefinition;
	}

	public void setMyDefinition(final Definition definition) {
		myDefinition = definition;
	}

	public boolean getStartability() {
		return isStartable;
	}

	@Override
	public final int size() {
		return parameters.size();
	}

	@Override
	public FormalParameter get(final int index) {
		return parameters.get(index);
	}

	public FormalParameter get(final Identifier id) {
		return (parameterMap == null || id == null) ? null : parameterMap.get(id.getName());
	}

	/**
	 * Checks if a "-" was specified in the formal parameter list.
	 * @return {@code true} if one of the parameters has notused as default value.
	 */
	public boolean hasNotusedDefaultValue() {
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters.get(i).hasNotusedDefaultValue()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks whether the formal parameter list has only formal parameters that
	 * have default values. In case of template, they can be called without parenthesis.
	 *
	 * @return {@code true} if each formal parameter has a default value, {@code false} otherwise.
	 */
	public boolean hasOnlyDefaultValues() {
		for (int i = 0; i < parameters.size(); i++) {
			if (null == parameters.get(i).getDefaultParameter()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Sets the scope of the formal parameter list.
	 * @param scope the scope to be set
	 */
	public void setMyScope(final Scope scope) {
		setParentScope(scope);
		for (final FormalParameter parameter : parameters) {
			parameter.setMyScope(this);
		}
	}

	@Override
	public List<Definition> getVisibleDefinitions() {
		return Collections.unmodifiableList(parameters);
	}

	/** reset the properties tracking the use of the formal parameters */
	public void reset () {
		for (final FormalParameter parameter : parameters) {
			parameter.reset();
		}
	}

	/**
	 * Checks the uniqueness of the parameters, and also builds a hashmap of
	 * them to speed up further searches.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	private void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (lastTimeUniquenessChecked != null && !lastTimeUniquenessChecked.isLess(timestamp)) {
			return;
		}

		if (parameterMap != null) {
			parameterMap.clear();
		}

		for (final FormalParameter parameter: parameters) {
			if (parameter != null) {
				final String parameterName = parameter.getIdentifier().getName();

				if (parameterMap == null) {
					parameterMap = new HashMap<String, FormalParameter>(parameters.size());
				}

				if (parameterMap.containsKey(parameterName)) {
					final Location otherLocation = parameterMap.get(parameterName).getIdentifier().getLocation();
					otherLocation.reportSemanticError(MessageFormat.format(DUPLICATEPARAMETERFIRST, parameter
							.getIdentifier().getDisplayName()));
					final Location paramLocation = parameter.getIdentifier().getLocation();
					paramLocation.reportSemanticError(MessageFormat.format(DUPLICATEPARAMETERREPEATED, parameter.getIdentifier()
							.getDisplayName()));
				} else {
					parameterMap.put(parameterName, parameter);
				}
			}
		}

		lastTimeUniquenessChecked = timestamp;
	}

	public void check(final CompilationTimeStamp timestamp, final Assignment_type definitionType) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		minimumNofParameters = 0;
		isStartable = true;

		for (int i = 0, size = parameters.size(); i < size; i++) {
			FormalParameter parameter = parameters.get(i);
			final Identifier identifier = parameter.getIdentifier();
			if (getMyDefinition() instanceof Def_Constructor) {
				switch (parameter.getAssignmentType()) {
				case A_PAR_VAL:
				case A_PAR_VAL_IN:
				case A_PAR_TEMP_IN:
					break;
				default:
					parameter.getLocation().reportSemanticError(
							MessageFormat.format(CONSTRUCTORPARAMIN, parameter.getAssignmentName()));
				}
			}

			if (parentScope != null && !isInClassBody()) {
				if (parentScope.hasAssignmentWithId(timestamp, identifier)) {
					parameter.getLocation().reportSemanticError(
							MessageFormat.format(HIDINGSCOPEELEMENT, identifier.getDisplayName()));
					parentScope.hasAssignmentWithId(timestamp, identifier);
					final List<ISubReference> subReferences = new ArrayList<ISubReference>();
					subReferences.add(new FieldSubReference(identifier));
					final Reference reference = new Reference(null, subReferences);
					final Assignment assignment = parentScope.getAssBySRef(timestamp, reference);
					if (assignment != null && assignment.getLocation() != null) {
						assignment.getLocation().reportSemanticWarning(
								MessageFormat.format(StatementBlock.HIDDENSCOPEELEMENT, identifier.getDisplayName()));
					}
				} else if (parentScope.isValidModuleId(identifier)) {
					final String severity = Configuration.INSTANCE.getString(Configuration.REPORT_MODULE_NAME_REUSE, GeneralConstants.ERROR);
					parameter.getLocation().reportConfigurableSemanticProblem(severity,
						MessageFormat.format(HIDINGMODULEIDENTIFIER, identifier.getDisplayName()));
				}
			}

			parameter.check(timestamp);
			if (parameter.getAssignmentType() != parameter.getRealAssignmentType()) {
				parameter = parameter.setParameterType(parameter.getRealAssignmentType());
				parameters.set(i, parameter);
			}

			if (Assignment_type.A_TEMPLATE.semanticallyEquals(definitionType)) {
				if (!Assignment_type.A_PAR_VAL_IN.semanticallyEquals(parameter.getAssignmentType())
						&& !Assignment_type.A_PAR_TEMP_IN.semanticallyEquals(parameter.getAssignmentType())) {
					parameter.getLocation().reportSemanticError("A template cannot have " + parameter.getAssignmentName());
				}
			} else if (Assignment_type.A_TESTCASE.semanticallyEquals(definitionType)) {
				if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getAssignmentType())
						&& Assignment_type.A_PAR_PORT.semanticallyEquals(parameter.getAssignmentType())) {
					parameter.getLocation().reportSemanticError("A testcase cannot have " + parameter.getAssignmentName());
				}
			} else if (Assignment_type.A_PORT.semanticallyEquals(definitionType)) {
				switch (parameter.getAssignmentType()) {
				case A_PAR_VAL:
				case A_PAR_VAL_IN:
				case A_PAR_VAL_OUT:
				case A_PAR_VAL_INOUT: {
					final IReferenceChain refChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
					parameter.getType(CompilationTimeStamp.getBaseTimestamp()).checkMapParameter(timestamp, refChain, parameter.getLocation());
					refChain.release();
					break;
				}
				default:
					parameter.getLocation().reportSemanticError(MessageFormat.format("The `map'/`unmap' parameters of a port type cannot have {0}", parameter.getAssignmentName()));
					break;
				}
			} else {
				// everything is allowed for functions and altsteps
			}

			// startability check
			switch (parameter.getAssignmentType()) {
			case A_PAR_VAL:
			case A_PAR_VAL_IN:
			case A_PAR_VAL_OUT:
			case A_PAR_VAL_INOUT:
			case A_PAR_TEMP_IN:
			case A_PAR_TEMP_OUT:
			case A_PAR_TEMP_INOUT: {
				final IType tempType = parameter.getType(timestamp);
				if (isStartable && tempType != null && tempType.isComponentInternal(timestamp)) {
					isStartable = false;
				}
				break;
			}
			default:
				isStartable = false;
				break;
			}

			if (!parameter.hasDefaultValue()) {
				minimumNofParameters = i + 1;
			}
		}

		checkUniqueness(timestamp);

		lastTimeChecked = timestamp;
	}

	private boolean isInClassBody() {
		Scope scope = parentScope;
		while (scope instanceof StatementBlock || scope instanceof FormalParameterList
				|| scope instanceof NamedBridgeScope || scope instanceof Definitions) {
			scope = scope.getParentScope();
		}
		return scope instanceof ClassTypeBody;
	}

	/**
	 * Checks the properties of the parameter list, that can only be checked
	 * after the semantic check was completely run.
	 */
	public void postCheck() {
		for (final FormalParameter parameter: parameters) {
			parameter.postCheck();
		}
	}

	/**
	 * check that @lazy parametrisation not used in cases currently unsupported
	 */
	public void checkNoLazyParams() {
		for (final FormalParameter formalParameter: parameters) {
			if (!parameterEvaluationType.NORMAL_EVAL.equals(formalParameter.getEvaluationType())) {
				formalParameter.getLocation().reportSemanticError(MessageFormat.format(
						LAZY_PARAMETER_ERROR, formalParameter.getAssignmentName(), formalParameter.getEvaluationType() == parameterEvaluationType.LAZY_EVAL? "lazy" : "fuzzy"));
			}
		}
	}

	/**
	 * Read the parsed actual parameters, and collate the lazy and non-lazy actual parameters
	 * according to their associated formal parameters.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param parsedParameters the parsed actual parameters (may contain named, and unnamed parts too).
	 * @param actualLazyParameters the list of actual lazy parameters returned for later usage.
	 * @param actualNonLazyParameters the list of actual non lazy parameters returned for later usage.
	 */
	public void collateLazyAndNonLazyActualParameters(final CompilationTimeStamp timestamp, final ParsedActualParameters parsedParameters,
			final ActualParameterList actualLazyParameters,final ActualParameterList actualNonLazyParameters) {

		final TemplateInstances unnamed = parsedParameters.getInstances();
		final NamedParameters named = parsedParameters.getNamedParameters();
		int nofLocated = unnamed.size();

		final Map<FormalParameter, Integer> formalParameterMap = new HashMap<FormalParameter, Integer>();
		for (int i = 0, size = parameters.size(); i < size; i++) {
			formalParameterMap.put(parameters.get(i), Integer.valueOf(i));
		}

		final TemplateInstances finalUnnamed = new TemplateInstances(unnamed);

		for (final NamedParameter namedParameter : named) {
			final FormalParameter formalParameter = parameterMap.get(namedParameter.getName().getName());
			final int isAt = formalParameterMap.get(formalParameter);
			for (; nofLocated < isAt; nofLocated++) {
				final NotUsed_Template temp = new NotUsed_Template();
				if (!parameters.get(nofLocated).hasDefaultValue()) {
					temp.setIsErroneous(true);
				}

				final TemplateInstance instance = new TemplateInstance(null, null, temp);
				instance.setLocation(parsedParameters.getLocation());
				finalUnnamed.add(instance);
			}
			finalUnnamed.add(namedParameter.getInstance());
			nofLocated++;
		}

		finalUnnamed.setLocation(parsedParameters.getLocation());

		final int upperLimit = (finalUnnamed.size() < parameters.size()) ? finalUnnamed.size() : parameters.size();

		for (int i = 0; i < upperLimit; i++) {
			final TemplateInstance instance = finalUnnamed.get(i);
			final FormalParameter formalParameter = parameters.get(i);
			if (instance.getType() == null && instance.getDerivedReference() == null
					&& Template_type.TEMPLATE_NOTUSED.equals(instance.getTemplateBody().getTemplatetype())) {

				final ActualParameter defaultValue = formalParameter.getDefaultValue();
				final Default_ActualParameter temp = new Default_ActualParameter(defaultValue);
				if (defaultValue != null && !defaultValue.getIsErroneous()) {
					temp.setLocation(defaultValue.getLocation());
				}

				if(formalParameter.getEvaluationType() == parameterEvaluationType.LAZY_EVAL) {
					actualLazyParameters.add(temp);
				} else if(formalParameter.getEvaluationType() == parameterEvaluationType.NORMAL_EVAL) {
					actualNonLazyParameters.add(temp);
				}
			} else {
				final ActualParameter actualParameter = formalParameter.checkActualParameter(timestamp, instance, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
				actualParameter.setLocation(instance.getLocation());
				if(formalParameter.getEvaluationType() == parameterEvaluationType.LAZY_EVAL) {
					actualLazyParameters.add(actualParameter);
				} else if(formalParameter.getEvaluationType() == parameterEvaluationType.NORMAL_EVAL){
					actualNonLazyParameters.add(actualParameter);
				}
			}
		}

		for (int i = upperLimit; i < parameters.size(); i++) {
			final FormalParameter formalParameter = parameters.get(i);
			final ActualParameter defaultValue = formalParameter.getDefaultValue();
			final Default_ActualParameter temp = new Default_ActualParameter(defaultValue);
			if (defaultValue != null && !defaultValue.getIsErroneous()) {
				temp.setLocation(defaultValue.getLocation());
			}

			if(formalParameter.getEvaluationType() == parameterEvaluationType.LAZY_EVAL) {
				actualLazyParameters.add(temp);
			} else if(formalParameter.getEvaluationType() == parameterEvaluationType.NORMAL_EVAL) {
				actualNonLazyParameters.add(temp);
			}
		}
	}

	/**
	 * Check if a list of parsed actual parameters is semantically correct
	 * according to a list of formal parameters (the called entity).
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param parsedParameters the parsed actual parameters (may contain named,
	 *                and unnamed parts too).
	 * @param actualParameters the list of actual parameters returned for later usage.
	 * @return {@code true} if a semantic error was found, {@code false} otherwise
	 */
	public boolean checkActualParameterList(final CompilationTimeStamp timestamp, final ParsedActualParameters parsedParameters,
			final ActualParameterList actualParameters) {
		parsedParameters.setFormalParList(this);

		checkUniqueness(timestamp);

		boolean isErroneous = false;

		final TemplateInstances unnamed = parsedParameters.getInstances();
		final NamedParameters named = parsedParameters.getNamedParameters();
		int nofLocated = unnamed.size();

		final Map<FormalParameter, Integer> formalParameterMap = new HashMap<FormalParameter, Integer>();
		for (int i = 0, size = parameters.size(); i < size; i++) {
			formalParameterMap.put(parameters.get(i), Integer.valueOf(i));
		}

		final TemplateInstances finalUnnamed = new TemplateInstances(unnamed);

		for (final NamedParameter namedParameter : named) {
			if (parameterMap != null && parameterMap.containsKey(namedParameter.getName().getName())) {
				final FormalParameter formalParameter = parameterMap.get(namedParameter.getName().getName());
				final int isAt = formalParameterMap.get(formalParameter);

				if (isAt >= nofLocated) {
					for (; nofLocated < isAt; nofLocated++) {
						final NotUsed_Template temp = new NotUsed_Template();
						if (!parameters.get(nofLocated).hasDefaultValue()) {
							temp.setIsErroneous(true);
						}
						final TemplateInstance instance = new TemplateInstance(null, null, temp);
						instance.setLocation(parsedParameters.getLocation());
						finalUnnamed.add(instance);
					}
					finalUnnamed.add(namedParameter.getInstance());
					nofLocated++;
				} else {
					isErroneous = true;

					if (isAt >= unnamed.size()) {
						namedParameter.getLocation().reportSemanticError(
								MessageFormat.format(OUT_OF_ORDER_ERROR, namedParameter.getName().getDisplayName()));
					} else {
						namedParameter.getLocation().reportSemanticError(
								MessageFormat.format(MULTIPLE_ASSIGNMENT_ERROR, namedParameter.getName().getDisplayName()));
					}
				}
			} else {
				String name;
				switch (myDefinition.getAssignmentType()) {
				case A_TYPE:
					switch (((Def_Type) myDefinition).getType(timestamp).getTypetype()) {
					case TYPE_FUNCTION:
						name = "Function reference";
						break;
					case TYPE_ALTSTEP:
						name = "Altstep reference";
						break;
					case TYPE_TESTCASE:
						name = "Testcase reference";
						break;
					default:
						name = EMPTY_STRING;
						break;
					}
					break;
				default:
					name = myDefinition.getAssignmentName();
					break;
				}

				isErroneous = true;
				namedParameter.getLocation().reportSemanticError(
						MessageFormat.format(PARAMETER_MISSING, name, myDefinition.getFullName(), namedParameter.getName().getDisplayName()));
			}
		}

		if (isErroneous) {
			return false;
		}

		finalUnnamed.setLocation(parsedParameters.getLocation());

		return checkActualParameterList(timestamp, finalUnnamed, actualParameters);
	}

	/**
	 * Check if a list of parsed actual parameters is semantically correct
	 * according to a list of formal parameters (the called entity).
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param instances the list of actual parameters containing both the named
	 *                and the unnamed part converted into an unnamed list,
	 *                that is full and in correct order.
	 * @param actualParameters the list of actual parameters returned for later usage.
	 * @return {@code true} if a semantic error was found, {@code false} otherwise
	 */
	private boolean checkActualParameterList(final CompilationTimeStamp timestamp, final TemplateInstances instances,
			final ActualParameterList actualParameters) {
		boolean isErroneous = false;

		if (minimumNofParameters == parameters.size()) {
			if (instances.size() != parameters.size()) {
				instances.getLocation().reportSemanticError(
						MessageFormat.format(NONMATCHING_PARAMETER_NUMBER,
								(instances.size() < parameters.size()) ? FEW : MANY, parameters.size(), instances.size()));
				isErroneous = true;
			}
		} else {
			if (instances.size() < minimumNofParameters) {
				instances.getLocation().reportSemanticError(
						MessageFormat.format(TOO_FEW_MANY_PARAMETER, FEW, "least", minimumNofParameters, instances.size()));
				isErroneous = true;
			} else if (instances.size() > parameters.size()) {
				instances.getLocation().reportSemanticError(
						MessageFormat.format(TOO_FEW_MANY_PARAMETER, MANY, "most", parameters.size(), instances.size()));
				isErroneous = true;
			}
		}

		final int upperLimit = (instances.size() < parameters.size()) ? instances.size() : parameters.size();
		for (int i = 0; i < upperLimit; i++) {
			final TemplateInstance instance = instances.get(i);
			final FormalParameter formalParameter = parameters.get(i);

			if (instance.getType() == null && instance.getDerivedReference() == null
					&& Template_type.TEMPLATE_NOTUSED.equals(instance.getTemplateBody().getTemplatetype())) {
				if (formalParameter.hasDefaultValue()) {
					final ActualParameter defaultValue = formalParameter.getDefaultValue();
					final Default_ActualParameter temp = new Default_ActualParameter(defaultValue);
					actualParameters.add(temp);
					if (defaultValue == null || defaultValue.getIsErroneous()) {
						isErroneous = true;
					} else {
						temp.setLocation(defaultValue.getLocation());
					}
				} else if (instance.getTemplateBody().getIsErroneous(timestamp)) {
					instances.getLocation().reportSemanticError(
							MessageFormat.format(MISSINGPARAMETER, formalParameter.getIdentifier().getDisplayName()));
					isErroneous = true;
				} else {
					instance.getLocation().reportSemanticError(NOT_USED_SYMBOL_ERROR);
					final ActualParameter temp = new Value_ActualParameter(null);
					temp.setLocation(instances.getLocation());
					temp.setIsErroneous();
					actualParameters.add(temp);
					isErroneous = true;
				}
			} else {
				final ActualParameter actualParameter = formalParameter.checkActualParameter(timestamp, instance,
						Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
				actualParameter.setLocation(instance.getLocation());
				actualParameters.add(actualParameter);
				if (actualParameter.getIsErroneous()) {
					isErroneous = true;
				}
			}
		}

		for (int i = upperLimit; i < parameters.size(); i++) {
			final FormalParameter formalParameter = parameters.get(i);
			if (formalParameter.hasDefaultValue()) {
				final ActualParameter defaultValue = formalParameter.getDefaultValue();
				final Default_ActualParameter temp = new Default_ActualParameter(defaultValue);
				actualParameters.add(temp);
				if (defaultValue == null || defaultValue.getIsErroneous()) {
					isErroneous = true;
				} else {
					temp.setLocation(defaultValue.getLocation());
				}
			} else {
				final ActualParameter temp = new Value_ActualParameter(null);
				temp.setLocation(instances.getLocation());
				temp.setIsErroneous();
				actualParameters.add(temp);
				isErroneous = true;
			}
		}

		return isErroneous;
	}

	/**
	 * Check if a list of parsed actual parameters is semantically correct
	 * according to a list of formal parameters in an activate statement/operation.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param actualParameters the list of actual parameters containing both the named
	 *                and the unnamed part converted into an unnamed list,
	 *                that is full and in correct order.
	 * @param description the description of the assignment to be used for reporting errors
	 * @return {@code true} if a semantic error was not found, {@code false} otherwise
	 */
	public boolean checkActivateArgument(final CompilationTimeStamp timestamp, final ActualParameterList actualParameters,
			final String description) {
		if (actualParameters == null) {
			return false;
		}

		boolean returnValue = true;
		for (int i = 0; i < actualParameters.size(); i++) {
			final ActualParameter actualParameter = actualParameters.get(i);
			final FormalParameter formalParameter = parameters.get(i);

			if (formalParameter.getEvaluationType() != parameterEvaluationType.NORMAL_EVAL) {
				actualParameter.getLocation().reportSemanticError(ACTIVATING_LAZY_FUZZY_NOT_ALLOWED);
			}
			
			if (!(actualParameter instanceof Referenced_ActualParameter)) {
				continue;
			}

			final Reference reference = ((Referenced_ActualParameter) actualParameter).getReference();
			final Assignment assignment = reference.getRefdAssignment(timestamp, true);
			if (assignment == null) {
				return false;
			}

			boolean displayWarning = true;
			switch (assignment.getAssignmentType()) {
			case A_VAR:
			case A_VAR_TEMPLATE:
			case A_TIMER:
				// it is not allowed to pass references of local variables or timers
				if (assignment.isLocal()) {
					reference.getLocation().reportSemanticError(
							MessageFormat.format(ILLEGALACTIVATEPARAMETER, i + 1, description,
									assignment.getDescription()));
					displayWarning = false;
					returnValue = false;
				}
				break;
			case A_PAR_VAL:
			case A_PAR_VAL_IN:
			case A_PAR_VAL_OUT:
			case A_PAR_VAL_INOUT:
			case A_PAR_TEMP_IN:
			case A_PAR_TEMP_OUT:
			case A_PAR_TEMP_INOUT:
			case A_PAR_TIMER: {
				// it is not allowed to pass references pointing to formal parameters
				// except for activate() statements within testcases
				final FormalParameter referencedFormalParameter = (FormalParameter) assignment;
				final FormalParameterList formalParameterList = referencedFormalParameter.getMyParameterList();
				if (formalParameterList != null) {
					final Definition definition = formalParameterList.getMyDefinition();
					if (definition != null && !Assignment_type.A_TESTCASE.semanticallyEquals(definition.getAssignmentType())) {
						reference.getLocation().reportSemanticError(
								MessageFormat.format(ILLEGALACTIVATEPARAMETER, i + 1, description,
										assignment.getDescription()));
						returnValue = false;
					}
				}
				displayWarning = false;
				break;
			}
			default:
				break;
			}
			if (displayWarning) {
				reference.getLocation().reportSemanticWarning(
					MessageFormat.format(BACKWARD, formalParameter.getAssignmentName()));
			}
		}

		return returnValue;
	}

	/**
	 * Checks the parameter list for startability: reports error if the owner function
	 * cannot be started on a PTC. Used by functions and function types.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param what shall contain a short description of the caller.
	 * @param namedNode shall contain the namedNode of the function of function type.
	 * @param errorLocation the location to report the error to, if needed.
	 */
	public void checkStartability(final CompilationTimeStamp timestamp, final String what, final INamedNode namedNode,
			final Location errorLocation) {

		for (final FormalParameter parameter : parameters) {
			switch (parameter.getAssignmentType()) {
			case A_PAR_VAL_OUT:
			case A_PAR_TEMP_OUT:
			case A_PAR_VAL_INOUT:
			case A_PAR_TEMP_INOUT:
				errorLocation.reportSemanticWarning(MessageFormat.format(WILLREMAINUNCHANGED,what, namedNode.getFullName()));
				// fallthrough
			case A_PAR_VAL:
			case A_PAR_VAL_IN:
			case A_PAR_TEMP_IN:
			 {
				final IType tempType = parameter.getType(timestamp);
				if (tempType != null && tempType.isComponentInternal(timestamp)) {
					final Set<IType> typeSet = new HashSet<IType>();
					final String errorString = MessageFormat.format(CANNOTBESTARTED, what, namedNode.getFullName(),
							parameter.getDescription());
					tempType.checkComponentInternal(timestamp, typeSet, errorString);
				}
				break;
			}
			default:
				errorLocation.reportSemanticError(MessageFormat.format(PTC_ERROR, what, namedNode.getFullName(), parameter.getDescription()));
				break;
			}
		}
	}

	/**
	 * Checks the compatibility of two formal parameter lists.
	 * They are compatible if every parameter is compatible,
	 * has the same attribute, type, restriction and name.
	 *
	 * Please note that all errors will be reported to the location provided as the last parameter.
	 * In themselves both formal parameter lists might be OK,
	 * so the error needs to be reported to the location where they are compared.
	 *
	 * @param timestamp the compilation timestamp
	 * @param fpList the formal parameter list to be compared to the actual one.
	 * @param callSite the location where errors should be reported to.
	 */
	public void checkCompatibility(final CompilationTimeStamp timestamp, final FormalParameterList fpList, final Location callSite) {
		if (parameters.size() != fpList.parameters.size()) {
			callSite.reportSemanticError(MessageFormat.format(NONMATCHING_PARAMETER_NUMBER,
					(parameters.size() < fpList.parameters.size()) ? MANY : FEW, parameters.size(), fpList.parameters.size()));
		}

		final int upperLimit = Math.min(parameters.size(), fpList.parameters.size());
		for (int i = 0; i < upperLimit; i++) {
			final FormalParameter typeParameter = parameters.get(i);
			final FormalParameter functionParameter = fpList.get(i);

			if (typeParameter.getIsErroneous() || functionParameter.getIsErroneous()) {
				continue;
			}

			if (!typeParameter.getAssignmentType().semanticallyEquals(functionParameter.getAssignmentType())) {
				callSite.reportSemanticError(MessageFormat.format(PARAMETER_KIND_TYPE_ERROR,
						"kind", StringUtils.getOrdinalIndicator(i), typeParameter.getAssignmentName(), functionParameter.getAssignmentName()));
			}

			if (typeParameter.getAssignmentType() != Assignment_type.A_PAR_TIMER &&
					functionParameter.getAssignmentType() != Assignment_type.A_PAR_TIMER) {
				final Type typeParameterType = typeParameter.getType(timestamp);
				final Type functionParameterType = functionParameter.getType(timestamp);
				if (!typeParameterType.isIdentical(timestamp, functionParameterType)) {
					callSite.reportSemanticError(MessageFormat.format(PARAMETER_KIND_TYPE_ERROR,
							"type", StringUtils.getOrdinalIndicator(i), typeParameterType.getTypename(), functionParameterType.getTypename()));
				} else if (typeParameterType.getSubtype() != null && functionParameterType.getSubtype() != null &&
						typeParameterType.getSubtypeType().equals(functionParameterType.getSubtypeType()) &&
						!typeParameterType.getSubtype().isCompatible(timestamp, functionParameterType.getSubtype())) {
					// TODO: maybe equivalence should be checked, or maybe that is too strict
					callSite.reportSemanticError(MessageFormat.format(SUBTYPE_MISMATCH, typeParameterType.getSubtype().toString(), functionParameterType.getSubtype().toString()));
				}
			}

			if (typeParameter.getTemplateRestriction() != functionParameter.getTemplateRestriction()) {
				callSite.reportSemanticError(MessageFormat.format(TEMPLATE_RESTRICTION_ERROR,
						StringUtils.getOrdinalIndicator(i), typeParameter.getTemplateRestriction().getDisplayName(), functionParameter.getTemplateRestriction().getDisplayName()));
			}

			if (typeParameter.getEvaluationType() != functionParameter.getEvaluationType()) {
				callSite.reportSemanticError(MessageFormat.format(EVALUATION_TYPE_ERROR, StringUtils.getOrdinalIndicator(i)));
			}

			if (!typeParameter.getIdentifier().equals(functionParameter.getIdentifier())) {
				callSite.reportSemanticWarning(MessageFormat.format(NAME_MISMATCH,
						StringUtils.getOrdinalIndicator(i), typeParameter.getIdentifier().getDisplayName(), functionParameter.getIdentifier().getDisplayName()));
			}
		}
	}
	
	/**
	 * Checks if two formal parameters are the same.
	 * They are considered to be the same if they contain the same formal parameters
	 * in the same order.
	 *  
	 * @param timestamp the compilation timestamp
	 * @param fpList the formal parameter list to be compared to the actual one.
	 * @return if the two formal parameter lists are the same
	 */
	public IsIdenticalResult isIdentical(final CompilationTimeStamp timestamp, final FormalParameterList fpList) {
		if (fpList == null) {
			return isEmpty() ? IsIdenticalResult.RES_IDENTICAL : IsIdenticalResult.RES_DIFFERS; 
		}
		if (parameters.size() != fpList.parameters.size()) {
			return IsIdenticalResult.RES_DIFFERS;
		}
		
		boolean isNameDiffers = false;
		for (int i = 0; i < parameters.size(); i++) {
			final FormalParameter prevParam = parameters.get(i);
			final FormalParameter localParam = fpList.get(i);
			if (! localParam.getIdentifier().getName().equals(prevParam.getIdentifier().getName())) {
				isNameDiffers = true;
			}
			if (localParam.getType(timestamp).getTypetype() != prevParam.getType(timestamp).getTypetype()) {
				return IsIdenticalResult.RES_DIFFERS;
			}
		}
		return isNameDiffers ? IsIdenticalResult.RES_NAME_DIFFERS : IsIdenticalResult.RES_IDENTICAL;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasAssignmentWithId(final CompilationTimeStamp timestamp, final Identifier identifier) {
		if (parameterMap != null && parameterMap.containsKey(identifier.getName())) {
			return true;
		}
		if (parentScope != null) {
			return parentScope.hasAssignmentWithId(timestamp, identifier);
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		if (reference.getModuleIdentifier() != null || parameterMap == null) {
			return getParentScope().getAssBySRef(timestamp, reference);
		}

		final Identifier identifier = reference.getId();
		if (identifier == null) {
			return getParentScope().getAssBySRef(timestamp, reference);
		}

		final Assignment assignment = parameterMap.get(identifier.getName());
		if (assignment != null) {
			return assignment;
		}

		return getParentScope().getAssBySRef(timestamp, reference);

	}

	/**
	 * Creates a representation of this formal parameter list for use as the
	 * part of the description of a proposal.
	 *
	 * @param builder the StringBuilder to append the representation to.
	 * @return the StringBuilder after appending the representation.
	 * */
	public StringBuilder getAsProposalDesriptionPart(final StringBuilder builder) {
		for (int i = 0, size = parameters.size(); i < size; i++) {
			if (i != 0) {
				builder.append(", ");
			}
			parameters.get(i).getAsProposalDesriptionPart(builder);
		}
		return builder;
	}

	/**
	 * Handles the incremental parsing of this list of formal parameters.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged {@code true} if the location contains the damaged area,
	 *                {@code false} if only its' location needs to be updated.
	 * */
	@Override
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final FormalParameter parameter : parameters) {
			parameter.updateSyntax(reparser, isDamaged);
			reparser.updateLocation(parameter.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		for (final FormalParameter parameter : parameters) {
			if (parameter.getLocation().containsPosition(offset)) {
				return parameter;
			}
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final FormalParameter formalPar : parameters) {
			formalPar.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final FormalParameter formalPar : parameters) {
			if (!formalPar.accept(v)) {
				return false;
			}
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}

	@Override
	public Iterator<FormalParameter> iterator() {
		return parameters.iterator();
	}
}
