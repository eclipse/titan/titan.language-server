/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Interface for such AST elements that contains a {@link Collection},
 * and offers some methods to manipulate/access the contained Collection.
 * Due to the {@link Iterable}, enhanced for (for-each) loop is available to use.
 * 
 * @param <E> the type of elements in this collection
 * 
 * @author Adam Knapp
 * */
public interface ICollection<E> extends Iterable<E> {
	/**
	 * Adds the specified element to this collection.
	 * @param e element to be added to this collection
	 * @see Collection#add(Object)
	 */
	boolean add(E e);

	/**
	 * Adds all of the elements in the specified collection to this collection.
	 * @param c collection containing elements to be added to this collection
	 */
	default boolean addAll(Collection<? extends E> c) {
		boolean changed = false;
		for (E e : c) {
			changed |= add(e);
		}
		return changed;
	}

	/**
	 * Returns {@code true} if this collection contains the specified element.
	 * @param o element whose presence in this collection is to be tested
	 * @return {@code true} if this collection contains the specified element
	 * @see Collection#contains(Object)
	 */
	default boolean contains(Object o) {
		// Do nothing by default
		return false;
	}

	/**
	 * Returns the element at the specified position in this collection.
	 * @param index index of the element to return
	 * @return the element at the specified position in this collection
	 * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
	 * @see List#get(int)
	 */
	E get(int index);

	/**
     * Returns {@code true} if this collection contains no elements.
     * @return {@code true} if this collection contains no elements
     */
	default boolean isEmpty() {
		return size() == 0;
	}

	/**
     * Returns an iterator over the elements in this collection.
     * @return an {@code Iterator} over the elements in this collection
     * @see Collection#iterator()
     * @see Iterable#iterator()
     */
	Iterator<E> iterator();

	/**
	 * @return the number of elements in this collection
	 */
	int size();

	/**
	 * Returns a sequential {@code Stream} with this collection as its source.
	 * @return a sequential {@code Stream} over the elements in this collection
	 * @see Collection#stream()
	 */
	default Stream<E> stream() {
        return StreamSupport.stream(spliterator(), false);
    }

	/**
	 * Returns a possibly parallel {@code Stream} with this collection as its
     * source. It is allowable for this method to return a sequential stream.
	 * @return a possibly parallel {@code Stream} over the elements in this collection
	 * @see Collection#parallelStream()
	 */
	default Stream<E> parallelStream() {
        return StreamSupport.stream(spliterator(), true);
    }
}
