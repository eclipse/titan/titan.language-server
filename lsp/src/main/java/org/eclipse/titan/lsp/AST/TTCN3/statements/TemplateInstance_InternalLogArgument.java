/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
/**
 * @author Kristof Szabados
 * */
public final class TemplateInstance_InternalLogArgument extends InternalLogArgument {
	private final TemplateInstance templateInstance;

	public TemplateInstance_InternalLogArgument(final TemplateInstance templateInstance) {
		super(ArgumentType.TemplateInstance);
		this.templateInstance = templateInstance;
	}

	public TemplateInstance getTemplate() {
		return templateInstance;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (templateInstance == null) {
			return;
		}

		templateInstance.checkRecursions(timestamp, referenceChain);
	}
}
