/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.semantichighlighting;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IVisitableNode;

/**
 * A visitor to set semantic information on AST nodes
 * 
 * @author Miklos Magyari
 *
 */
public class SemanticInformationVisitor extends ASTVisitor {
	public SemanticInformationVisitor() {
		// Do nothing
	}
	
	@Override
	public int visit(final IVisitableNode node) {
		if (node instanceof ISemanticInformation) {
			((ISemanticInformation) node).setSemanticInformation();
		}

		return V_CONTINUE;
	}
}