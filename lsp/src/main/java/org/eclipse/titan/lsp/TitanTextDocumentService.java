/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionParams;
import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.CodeLensParams;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.CompletionContext;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.CompletionParams;
import org.eclipse.lsp4j.CompletionTriggerKind;
import org.eclipse.lsp4j.DefinitionParams;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.DidSaveTextDocumentParams;
import org.eclipse.lsp4j.DocumentDiagnosticParams;
import org.eclipse.lsp4j.DocumentDiagnosticReport;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.DocumentSymbolParams;
import org.eclipse.lsp4j.jsonrpc.messages.ResponseError;
import org.eclipse.lsp4j.jsonrpc.messages.ResponseErrorCode;
import org.eclipse.lsp4j.Hover;
import org.eclipse.lsp4j.HoverParams;
import org.eclipse.lsp4j.InlayHint;
import org.eclipse.lsp4j.InlayHintParams;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.LocationLink;
import org.eclipse.lsp4j.MarkupContent;
import org.eclipse.lsp4j.MarkupKind;
import org.eclipse.lsp4j.PrepareRenameDefaultBehavior;
import org.eclipse.lsp4j.PrepareRenameParams;
import org.eclipse.lsp4j.PrepareRenameResult;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.ReferenceParams;
import org.eclipse.lsp4j.RelatedFullDocumentDiagnosticReport;
import org.eclipse.lsp4j.RenameParams;
import org.eclipse.lsp4j.SemanticTokens;
import org.eclipse.lsp4j.SemanticTokensParams;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SignatureHelpParams;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.TextDocumentContentChangeEvent;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.TypeDefinitionParams;
import org.eclipse.lsp4j.TypeHierarchyItem;
import org.eclipse.lsp4j.TypeHierarchyPrepareParams;
import org.eclipse.lsp4j.TypeHierarchySubtypesParams;
import org.eclipse.lsp4j.TypeHierarchySupertypesParams;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.ResponseErrorException;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.jsonrpc.messages.Either3;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticModifier;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;
import org.eclipse.titan.lsp.signatureHelp.SignatureHelpProvider;
import org.eclipse.titan.lsp.typeHierarchy.TypeHierarchyProvider;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.NodeVisitor;
import org.eclipse.titan.lsp.AST.NodeVisitor.VisitedNodeType;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.ASN1.definitions.ASN1Module;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.codeAction.CodeActionCollector;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.completion.CompletionProvider;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.core.Range;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.inlayhint.InlayHintProvider;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceSyntacticAnalyzer;
import org.eclipse.titan.lsp.parsers.Status.FutureStatus;

public final class TitanTextDocumentService implements TextDocumentService {
	private static final String NO_INFO_MSG = "No {0} info is available because this file is excluded from the build.";
	private static final String CANNOT_FIND_MSG = "Cannot find {0} because this file is excluded.";
	/** Time to wait after the last change in the editor before running code analysis (in milliseconds) */ 
	public static final long TYPING_DELAY = 500L;

	private CompletableFuture<FutureStatus> lastIncrementalSyntaxCheck;

	private static Timer delayTimer = new Timer();
	private static Range delayedChangedRange;
	private static int delayedShift;
	private static int delayedMaxDiffCount;
	private static boolean delayedLineEndingChanged;
	private static StringBuilder delayedDiff = new StringBuilder();

	/** 
	 * Keeping track of changed documents to perform a full analysis if more than one document is changed.<br>
	 * An example is a rename operation where a separate <code>textDocument/didChange</code> notification is
	 * sent for each file that has changed. 
	 */
	private static Set<Path> changedDocuments = new ConcurrentSkipListSet<>();

	public TitanTextDocumentService(TitanLanguageServer languageServer) {
		// Do nothing
	}

	@Override
	public CompletableFuture<Hover> hover(HoverParams params) {
		return CompletableFuture.supplyAsync(() -> {
			final Hover hover = new Hover();

			final ProjectItem item = getProjectItem(params.getTextDocument());
			if (item != null && item.isExcluded()) {
				hover.setContents(Either.forRight(
					new MarkupContent(MarkupKind.MARKDOWN, MessageFormat.format(NO_INFO_MSG, "hover"))));
				return hover;
			}

			final Module module = Project.INSTANCE.getModuleByPath(IOUtils.getTextDocumentPath(params.getTextDocument()));
			String hoverText = null;
			if (module instanceof TTCN3Module) {
				final NodeVisitor visitor = new NodeVisitor(params.getPosition());
				module.accept(visitor);
				if (visitor.getNodeType() == VisitedNodeType.Identifier) {
					final IDeclaration declaration = visitor.getReferencedDeclaration();
					if (declaration != null) {
						final ICommentable commentable = declaration.getCommentable();
						if (commentable != null) {
							hoverText = commentable.getHoverContent().getText(HoverContentType.INFO);
						}
					}
				}
			}
			hover.setContents(
				Either.forRight(hoverText != null ? 
					new MarkupContent(MarkupKind.MARKDOWN, hoverText) 
					: new MarkupContent(MarkupKind.MARKDOWN, "")));
			return hover;
		});
	}

	@Override
	public CompletableFuture<List<Either<SymbolInformation, DocumentSymbol>>> documentSymbol(DocumentSymbolParams params) {
		return CompletableFuture.supplyAsync(() -> {
			List<Either<SymbolInformation, DocumentSymbol>> symbols = new ArrayList<>();

			final ProjectItem item = getProjectItem(params.getTextDocument());
			if (item != null && item.isExcluded()) {
				final DocumentSymbol symbol = new DocumentSymbol();
				symbol.setName(MessageFormat.format(NO_INFO_MSG, "outline"));
				final Range range = new Range(new Position(0, 0), new Position(0, 0));
				symbol.setRange(range);
				symbol.setSelectionRange(range);
				symbol.setKind(SymbolKind.Null);
				symbols.add(Either.forRight(symbol));
			} else {
				final Project project = Project.INSTANCE;
				final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
				if (path != null) {
					Module module = project.getModuleByPath(path);
					if (module == null) {
						TitanLanguageServer.waitFirstAnalyzer();
						module = project.getModuleByPath(path);
					}
					if (module != null) {
						symbols.add(Either.forRight(module.getOutlineSymbol()));
					}
				}
			}
			return symbols;
		});
	}

	@Override
	public CompletableFuture<Either<List<? extends Location>, List<? extends LocationLink>>> definition(DefinitionParams params) {
		final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
		ProjectItem item = Project.INSTANCE.getProjectItem(path);
		if (item == null) {
			return null;
		}

		if (item.isExcluded()) {
			final CompletableFuture<Either<List<? extends Location>, List<? extends LocationLink>>> errorFuture = new CompletableFuture<>();
				final ResponseError error = new ResponseError(ResponseErrorCode.InvalidRequest,
						MessageFormat.format(CANNOT_FIND_MSG, "definition"), null);
				errorFuture.completeExceptionally(new  ResponseErrorException(error));
			return errorFuture;
		}
		return CompletableFuture.supplyAsync(() -> {
			final Project project = Project.INSTANCE;
			final Position position = new Position(params.getPosition());
			Either<List<? extends Location>, List<? extends LocationLink>> result = null;			

			final Module module = project.getModuleByPath(path);
			if (module instanceof TTCN3Module || module instanceof ASN1Module) {
				final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(position);
				module.accept(visitor);
				final IDeclaration declaration = visitor.getReferencedDeclaration();
				if (declaration == null) {
					return null;
				}
				final org.eclipse.titan.lsp.AST.Location astLocation = declaration.getLocation();
				if (astLocation != null && !astLocation.equals(NULL_Location.INSTANCE)) {
					final LocationLink lspLocationLink =
						new LocationLink(astLocation.getFile().toURI().toString(), astLocation.getRange(), declaration.getIdentifier().getLocation().getRange());

					final List<LocationLink> locationLinkList = new ArrayList<LocationLink>();
					locationLinkList.add(lspLocationLink);
					result = Either.forRight(locationLinkList);
				}
			}

			return result;
		});
	}

	@Override
	public CompletableFuture<SignatureHelp> signatureHelp(SignatureHelpParams params) {
		return CompletableFuture.supplyAsync(() -> {
			final SignatureHelpProvider provider = new SignatureHelpProvider(params);
			return provider.collectSignatureHelp();
		});
	}

	@Override
	public CompletableFuture<Either<List<CompletionItem>, CompletionList>> completion(CompletionParams params) {
		return CompletableFuture.supplyAsync(() -> {
			// Turn off automatic completion
			final CompletionContext context = params.getContext();
			if (context != null && context.getTriggerKind() != CompletionTriggerKind.Invoked) {
				return null;
			}

			final CompletionProvider provider = new CompletionProvider(params);
			return provider.getComplitions();
		});
	}

	@Override
	public CompletableFuture<CompletionItem> resolveCompletionItem(CompletionItem unresolved) {
		return CompletableFuture.supplyAsync(() ->
			unresolved
		);
	}

	@Override
	public CompletableFuture<DocumentDiagnosticReport> diagnostic(DocumentDiagnosticParams params) {
		// FIXME:
		// Sending publishDiagnostics notifications and response for diagnostic pull requests
		// causes duplicated diagnostic report on the client.
		// The diagnostic provider has been temporarily commented out in the server's capabilities.
		// This endpoint won't be reached.
		return CompletableFuture.supplyAsync(() -> {
			final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
			final ProjectItem item = Project.INSTANCE.getProjectItem(path);
			if (item != null) {
				final List<Diagnostic> diags = item.getMarkerList();
				RelatedFullDocumentDiagnosticReport docReport = new RelatedFullDocumentDiagnosticReport(diags);
//				String previousResultId = params.getPreviousResultId();
//				Integer resultId = previousResultId == null ? 1 : Integer.parseInt(previousResultId) + 1;
//				
//				docReport.setResultId(resultId.toString());
				return new DocumentDiagnosticReport(docReport);
			}
			return null;
		});
	}

	@Override
	public CompletableFuture<List<? extends CodeLens>> codeLens(CodeLensParams params) {
		return CompletableFuture.supplyAsync(() -> {

			// FIXME :: disabled until fixed, see issue #54

//			final Project project = Project.INSTANCE;
//			final Path path = LSPUtils.getTextDocumentPath(params.getTextDocument());
//			List<CodeLens> lens = null;
//
//			final ProjectItem projectItem = project.getProjectItem(path);
//			if (projectItem != null) {
//				lens = projectItem.getCodeLenses();
//			}
//			return lens;

			return null;
		});
	}

	@Override
	public CompletableFuture<CodeLens> resolveCodeLens(CodeLens unresolved) {
		// TODO: reference collection should be here?
		return null;
	}

	@Override
	public CompletableFuture<List<? extends Location>> references(ReferenceParams params) {
		ProjectItem item = Project.INSTANCE.getProjectItem(IOUtils.getTextDocumentPath(params.getTextDocument()));
		if (item == null) {
			return null;
		}

		if (item.isExcluded()) {
			final CompletableFuture<List<? extends Location>> errorFuture = new CompletableFuture<>();
			final ResponseError error = new ResponseError(ResponseErrorCode.InvalidRequest,
					MessageFormat.format(CANNOT_FIND_MSG, "reference"), null);
			errorFuture.completeExceptionally(new  ResponseErrorException(error));
			
			return errorFuture;
		}
		return CompletableFuture.supplyAsync(() -> {
			final IDeclaration declaration = findReferencedDeclaration(params.getTextDocument(), new Position(params.getPosition()));
			if (declaration != null) {
				final Assignment ass = declaration.getAssignment();
				if (ass != null) {
					final List<org.eclipse.titan.lsp.AST.Location> referenceList = ass.getReferenceLocationList();
					if (referenceList != null) {
						return referenceList.stream()
							.map(m -> m.getLspLocation())
							.collect(Collectors.toList());
					}
				}
			}

			return new ArrayList<>();
		});
	}

	@Override
	public CompletableFuture<SemanticTokens> semanticTokensFull(SemanticTokensParams params) {
		return CompletableFuture.supplyAsync(() -> {
			final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
			try {
				Map<org.eclipse.titan.lsp.AST.Location,SemanticType> tokens = AstSemanticHighlighting.getSemanticTokens(path);
				if (tokens == null) {
					TitanLanguageServer.waitFirstAnalyzer();
					tokens = AstSemanticHighlighting.getSemanticTokens(path);
				}

				if (tokens == null) {
					return null;
				}

				final Map<org.eclipse.titan.lsp.AST.Location,List<SemanticModifier>> modifiers = AstSemanticHighlighting.getSemanticModifiers(path);

				Comparator<org.eclipse.titan.lsp.AST.Location> locationComparator = org.eclipse.titan.lsp.AST.Location.getComparator();
				final Stream<org.eclipse.titan.lsp.AST.Location> tokenLocations = tokens.entrySet().stream().map(m -> m.getKey());
				final Stream<org.eclipse.titan.lsp.AST.Location> modifierLocations = modifiers.entrySet().stream().map(m -> m.getKey());
				final List<org.eclipse.titan.lsp.AST.Location> locationsOrdered = Stream.concat(tokenLocations, modifierLocations)
						.sorted(locationComparator)
						.collect(Collectors.toList());

				if (tokens != null || modifiers != null) {
					List<Integer> tokenList = new ArrayList<>();
					int prevLine = 0;
					int prevColumn = 0;
					for (org.eclipse.titan.lsp.AST.Location location : locationsOrdered) {
						final SemanticType token = tokens.get(location);
						final List<SemanticModifier> tokenModifiers = modifiers != null ? modifiers.get(location) : null; 
						if (token == null && (tokenModifiers == null || tokenModifiers.isEmpty())) {
							continue;
						}
						int lineDiff = location.getStartLine() - prevLine;
						int columnDiff =
							location.getStartLine() == prevLine ? location.getStartColumn() - prevColumn : location.getStartColumn();
						int tokenLength = location.getEndColumn() - location.getStartColumn();
						int tokenType = token != null ? token.getValue() : SemanticType.Decorator.getValue();
						int tokenModifier = 0;
						if (tokenModifiers != null) {
							for (SemanticModifier modifier : tokenModifiers) {
								tokenModifier |= modifier.getValue();
							}
						}

						tokenList.add(lineDiff);
						tokenList.add(columnDiff);
						tokenList.add(tokenLength);
						tokenList.add(tokenType);
						tokenList.add(tokenModifier);
						prevLine = location.getStartLine();
						prevColumn = location.getStartColumn();
					}
					return new SemanticTokens(tokenList);
				}
			} catch (Exception e) {
				TitanLogger.logError(e);
			}
			return null;
		});
	}

	@Override
	public CompletableFuture<List<TypeHierarchyItem>> prepareTypeHierarchy(TypeHierarchyPrepareParams params) {
		return CompletableFuture.supplyAsync(() ->
			TypeHierarchyProvider.prepareTypeHierarchy(params)
		);
	}

	@Override
	public CompletableFuture<List<TypeHierarchyItem>> typeHierarchySupertypes(TypeHierarchySupertypesParams params) {
		return CompletableFuture.supplyAsync(() ->
			TypeHierarchyProvider.getSuperTypes(params)
		);
	}

	@Override
	public CompletableFuture<List<TypeHierarchyItem>> typeHierarchySubtypes(TypeHierarchySubtypesParams params) {
		return CompletableFuture.supplyAsync(() ->
			TypeHierarchyProvider.getSubTypes(params)
		);
	}

	@Override
	public CompletableFuture<List<Either<Command, CodeAction>>> codeAction(CodeActionParams params) {
		return CompletableFuture.supplyAsync(() ->
			new CodeActionCollector(params).getCodeActions()
		);
	}

	@Override
	public CompletableFuture<List<InlayHint>> inlayHint(InlayHintParams params) {
		return CompletableFuture.supplyAsync(() -> {
			if(!Configuration.INSTANCE.getBoolean(Configuration.ENABLE_INLAY_HINTS, false)) {
				return null;
			}
			final InlayHintProvider provider = new InlayHintProvider(params);
			List<InlayHint> hints = provider.provideInlayHints();
			return !hints.isEmpty() ? hints : null;
		});
	}

	@Override
	public void didOpen(DidOpenTextDocumentParams params) {
		final String filename = params.getTextDocument().getUri();
		try {
			final Project project = Project.INSTANCE;
			ProjectItem projectItem = null;
			if (params.getTextDocument().getVersion() == 0) {
				projectItem = project.addProjectItem(IOUtils.getPath(filename));
				projectItem.setUntitled(true);
			} else {				
				final Path path = IOUtils.getPath(filename);
				/** skip files outside of workspace folders */
				boolean inWsFolder = false;
				for (final Path wsPath : project.getWorkspaceFolders().keySet()) {
					if (path.startsWith(wsPath)) {
						inWsFolder = true;
						break;
					}
				}
				if (!inWsFolder) {
					return;
				}

				projectItem = project.getProjectItem(path);
				if (projectItem == null) {
					projectItem = project.addProjectItem(path);
				}
			}
			projectItem.openFile(params.getTextDocument());
			final List<Diagnostic> markers = projectItem.getMarkerList();
			if (markers != null) {
				PublishDiagnosticsParams diagpar = new PublishDiagnosticsParams(params.getTextDocument().getUri(), markers);
				TitanLanguageServer.getClient().publishDiagnostics(diagpar);
			}
		} catch(Exception e) {
			TitanLogger.logError(e);
		}
	}

	@Override
	public synchronized void didChange(DidChangeTextDocumentParams params) {
		final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
		if (path == null) {
			return;
		}
		changedDocuments.add(path);
		try {
			final Project project = Project.INSTANCE;
			ProjectItem projectItem = project.getProjectItem(path);
			if (projectItem == null) {
				return;
			}
			projectItem.setSyntacticallyUpToDate(false);
			final List<TextDocumentContentChangeEvent> changeEvents = params.getContentChanges();

			if (projectItem.updateLineEnding(changeEvents)) {
				delayedLineEndingChanged = true;
			}
			int shift = projectItem.applyChange(changeEvents, params.getTextDocument().getVersion());
			delayedShift += shift;

			final boolean isIncremental = Configuration.INSTANCE.getBoolean(Configuration.USE_INCREMENTAL_PARSING, false);
			final TextDocumentContentChangeEvent event = params.getContentChanges().get(0);

			if (shift > 0) {
				delayedDiff.append(event.getText());
			} else {
				if (delayedDiff.length() > 0 && delayedDiff.length() > shift) {
					delayedDiff.delete(delayedDiff.length() + shift, delayedDiff.length());
				}
			}
			final Range changedRange = new Range(event.getRange());
			delayedMaxDiffCount = Math.max(delayedMaxDiffCount, params.getContentChanges().size());

			if (delayTimer != null) {
				delayTimer.cancel();
			}
			delayTimer = new Timer();
			delayTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					if (delayedChangedRange == null) {
						return;
					}

					/**
					 * For now, only perform incremental parsing if all the change sets contain one
					 * change only. It is unclear how to calculate correct line and column
					 * shifts for multiple changes in the same document.
					 * 
					 * We also perform a full reanalysis if the line endings are changed as this affects
					 * text offsets. 
					 */
					if (isIncremental && !delayedLineEndingChanged && delayedMaxDiffCount == 1 
						&& changedDocuments.size() == 1 && delayedShift >= 0) {
						final TTCN3ReparseUpdater reparser = new TTCN3ReparseUpdater(path, projectItem.getSource(),
								new org.eclipse.titan.lsp.AST.Location(path.toFile(), delayedChangedRange),
								delayedDiff.toString(), delayedShift);
						if (lastIncrementalSyntaxCheck != null) {
							try {
								lastIncrementalSyntaxCheck.join();
							} catch (Exception e) {
								TitanLogger.logError(e);
							}
						}
						if (projectItem.getModule() instanceof TTCN3Module) {
							final ProjectSourceParser sourceParser = GlobalParser.getProjectSourceParser(project);
							lastIncrementalSyntaxCheck = sourceParser.updateSyntax(path, reparser);
							lastIncrementalSyntaxCheck.join();
						}
						TitanLanguageServer.analyzeProject(project);
					} else {
						/** Do a full analysis of the file. This probably can be improved */
						final ProjectSourceSyntacticAnalyzer syntacticAnalyzer = GlobalParser.getProjectSourceParser(project).getSyntacticAnalyzer();
						if (syntacticAnalyzer != null) {
							changedDocuments.stream().forEach(changedPath -> {
								final ProjectItem changedItem = Project.INSTANCE.getProjectItem(changedPath);
								if (changedItem == null) {
									return;
								}
								if (changedItem.getModule() instanceof TTCN3Module) {
									final TTCN3Module module = (TTCN3Module)changedItem.getModule();
									module.clearExistingReferences();
								}
								syntacticAnalyzer.reportOutdating(changedPath);
							});
						}
						TitanLanguageServer.analyzeProject(project);
					}
					TitanLanguageServer.requestSemanticTokensRefresh();

					delayedShift = 0;
					delayedChangedRange = null;
					delayedLineEndingChanged = false;
					delayedMaxDiffCount = 0;
					delayedDiff.setLength(0);
					delayTimer.cancel();
					changedDocuments.clear();
				}
			}, TYPING_DELAY);

			if (delayedChangedRange == null) {
				delayedChangedRange = changedRange;
			} else {
				final Position changeStart = new Position(changedRange.getStart());
				if (changeStart.before(delayedChangedRange)) {
					delayedChangedRange.setStart(changeStart);
				}
				final Position changeEnd = new Position(changedRange.getEnd());
				if (changeEnd.after(new Position(delayedChangedRange.getEnd()))) {
					delayedChangedRange.setEnd(changeEnd);
				}
			}
		} catch (Exception e) {
			TitanLogger.logError(e);
		}
	}

	@Override
	public void didClose(DidCloseTextDocumentParams params) {
		ProjectItem projectItem = getProjectItem(params.getTextDocument());
		if (projectItem != null) {
			projectItem.closeFile();
		}
	}

	@Override
	public void didSave(DidSaveTextDocumentParams params) {
		// Do nothing for now
	}

	@Override
	public CompletableFuture<Either<List<? extends Location>, List<? extends LocationLink>>> typeDefinition(TypeDefinitionParams params) {
		return CompletableFuture.supplyAsync(() -> {
			final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
			if (path == null) {
				return null;
			}
			final Module module = Project.INSTANCE.getModuleByPath(path);
			if (module == null) {
				return null;
			}
			final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(params.getPosition());
			module.accept(visitor);
			final IDeclaration declaration = visitor.getReferencedDeclaration();
			if (declaration == null) {
				return null;
			}
			final CompilationTimeStamp timestamp = module.getLastCompilationTimeStamp();
			Location location = null;
			if (declaration.getAssignment() instanceof Def_Type) {
				final Def_Type type = (Def_Type)declaration.getAssignment();
				location = type.getLocation().getLspLocation();
			} else if (declaration.getAssignment().getType(timestamp) instanceof Referenced_Type) {
				final IType refd = declaration.getAssignment().getType(timestamp).getTypeRefdLast(timestamp);
				location = refd.getLocation().getLspLocation();
			}
			if (location != null) {
				return Either.forLeft(List.of(location));
			}
			return null;
		});
	}

	@Override
	public CompletableFuture<Either3<org.eclipse.lsp4j.Range, PrepareRenameResult, PrepareRenameDefaultBehavior>> prepareRename(PrepareRenameParams params) {
		ProjectItem item = Project.INSTANCE.getProjectItem(IOUtils.getTextDocumentPath(params.getTextDocument()));
		if (item == null) {
			return null;
		}

		if (item.isExcluded()) {
			final CompletableFuture<Either3<org.eclipse.lsp4j.Range, PrepareRenameResult, PrepareRenameDefaultBehavior>> errorFuture = new CompletableFuture<>();
			final ResponseError error = new ResponseError(ResponseErrorCode.InvalidRequest, "Cannot rename symbol because this file is excluded.", null);
			errorFuture.completeExceptionally(new ResponseErrorException(error));

			return errorFuture;
		}
		return CompletableFuture.supplyAsync(() -> {
			final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
			if (path == null) {
				return null;
			}
			final Module module = Project.INSTANCE.getModuleByPath(path);
			if (!(module instanceof TTCN3Module)) {
				return null;
			}
			final NodeVisitor visitor = new NodeVisitor(params.getPosition());
			module.accept(visitor);
			if (visitor.getIdentifier() != null) {
				return Either3.forFirst(new Range(visitor.getIdentifier().getLocation()));
			}

			return null;
		});
	}

	@Override
	public CompletableFuture<WorkspaceEdit> rename(RenameParams params) {
		boolean canRename = true;
		final Path path = IOUtils.getTextDocumentPath(params.getTextDocument());
		final Module module = Project.INSTANCE.getModuleByPath(path);
		if (!(module instanceof TTCN3Module)) {
			return null;
		}
		if (!Identifier.isValidInTtcn(params.getNewName())) {
			return renameError("'" + params.getNewName() + "' is not a valid TTCN3 identifier.");
		}
		final Identifier newIdentifier = new Identifier(Identifier_type.ID_TTCN, params.getNewName());
		final NodeVisitor visitor = new NodeVisitor(params.getPosition());
		module.accept(visitor);
		if (visitor.getNodeType() != VisitedNodeType.Identifier) {
			return null;
		}
		final IDeclaration declaration = visitor.getReferencedDeclaration();
		if (declaration == null) {
			return null;
		}
		final Assignment assignment = declaration.getAssignment();
		if (!(assignment instanceof Definition)) {
			return null;
		}
		final Definition definition = (Definition)assignment;
		final String oldName = definition.getIdentifier().getDisplayName();
		final CompilationTimeStamp timestamp = module.getLastCompilationTimeStamp();
		if (definition.getMyScope().hasAssignmentWithId(timestamp, newIdentifier)) {
			canRename = false;
		}
		if (canRename) {
			final List<Reference> references = definition.getReferences();
			for (final Reference ref : references) {
				final Scope scope = ref. getMyScope();
				if (scope.hasAssignmentWithId(timestamp, newIdentifier)) {
					canRename = false;
				}
			}
		}
		if (!canRename) {
			return renameError("Cannot rename: identifier name conflict.");
		}
		final ProjectItem item = Project.INSTANCE.getProjectItem(path);
		if (item == null) {
			return renameError("Cannot rename: invalid path");
		}
		item.removeCodeLens(definition.getIdentifier().getLocation().getRange());
		return CompletableFuture.supplyAsync(() -> {
			Map<String, List<TextEdit>> allEdits = new HashMap<>();
			final String replace = newIdentifier.getDisplayName();
			Range renameRange = new Range(definition.getIdentifier().getLocation());
			// FIXME :: null locations ?
			final String definitionName = definition.getLocation().getFile().toPath().toUri().toString();
			final TextEdit edit = new TextEdit(renameRange, replace);
			final List<TextEdit> originalDefinitionEdits = new ArrayList<>();
			originalDefinitionEdits.add(edit);
			if (definition.hasDocumentComment()) {
				final DocumentComment comment = definition.getDocumentComment();
				renameDocCommentId(comment.getParamIdLocations(), originalDefinitionEdits, oldName, newIdentifier.getDisplayName());
				renameDocCommentId(comment.getMemberIdLocations(), originalDefinitionEdits, oldName, newIdentifier.getDisplayName());
			}
			allEdits.put(definitionName, originalDefinitionEdits);
			
			final List<Reference> references = definition.getReferences();
			for (final Reference ref : references) {
				final org.eclipse.titan.lsp.AST.Location refloc = ref.getId().getLocation();
				final Range refRange = new Range(refloc);
				final String refName = refloc.getFile().toPath().toUri().toString();
				if (!allEdits.containsKey(refName)) {
					allEdits.put(refName, new ArrayList<>());
				}
				final List<TextEdit> existingEdits = allEdits.get(refName);
				existingEdits.add(new TextEdit(refRange, replace));
				allEdits.put(refName, existingEdits);
			}
			
			final WorkspaceEdit edits = new WorkspaceEdit();
			edits.setChanges(allEdits);
			return edits;
		});
	}

	private void renameDocCommentId(final Map<String, org.eclipse.titan.lsp.AST.Location> ids, final List<TextEdit> edit,
		final String oldName, final String newName) {
		ids.entrySet().stream().forEach(param -> {
			if (param.getKey().equals(oldName)) {
				final Range paramIdRange = new Range(param.getValue());
				final TextEdit paramEdit = new TextEdit(paramIdRange, newName);
				edit.add(paramEdit);
			}
		});
	}

	private CompletableFuture<WorkspaceEdit> renameError(final String errorCode) {
		return renameError(errorCode, null);
	}

	private CompletableFuture<WorkspaceEdit> renameError(final String errorCode, final Object data) {
		final CompletableFuture<WorkspaceEdit> errorFuture = new CompletableFuture<>();
		final ResponseError error = new ResponseError(ResponseErrorCode.InvalidRequest, errorCode, data);
		errorFuture.completeExceptionally(new ResponseErrorException(error));
		return errorFuture;
	}

	private final ProjectItem getProjectItem(TextDocumentIdentifier document) {
		final Path path = IOUtils.getTextDocumentPath(document);
		if (path == null) {
			return null;
		}

		return Project.INSTANCE.getProjectItem(path); 
	}

	private IDeclaration findReferencedDeclaration(final TextDocumentIdentifier textDocument, final Position position) {
		final Project project = Project.INSTANCE;
		final Path path = IOUtils.getTextDocumentPath(textDocument);

		final Module module = project.getModuleByPath(path);
		if (module instanceof TTCN3Module || module instanceof ASN1Module) {
			final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(position);
			module.accept(visitor);
			return visitor.getReferencedDeclaration();
		}
		return null;
	}
}
