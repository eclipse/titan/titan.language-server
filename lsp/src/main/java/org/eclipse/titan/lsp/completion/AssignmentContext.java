/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.InsertReplaceEdit;
import org.eclipse.lsp4j.InsertTextFormat;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Const;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Assignment_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Definition_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Statement;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.types.CompField;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Choice_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Enumerated_Type;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.core.Range;

public class AssignmentContext extends CompletionContext {
	private AssignmentMatch aMatch;

	public AssignmentContext(CompletionContextInfo contextInfo) {
		super(contextInfo);

		aMatch = new AssignmentMatch(contextInfo);
	}

	private void addCompletionItemsForUnionAlternatives(Set<CompletionItem> completions, TTCN3_Choice_Type union) {
		final Path path = IOUtils.getTextDocumentPath(contextInfo.document);
		final ProjectItem projectItem = contextInfo.module.getProject().getProjectItem(path);
		if (projectItem == null) {
			return;
		}

		final int nofOptions = union.getNofComponents();
		for (int i = 0; i < nofOptions; i++) {
			final CompField alt = union.getComponentByIndex(i);
			final String altName = alt.getIdentifier().getDisplayName();
			final Type altType = alt.getType();
			final StringBuilder sb = new StringBuilder();
			sb.append("{")
			.append(projectItem.getLineEnding())
			.append(altName)
			.append(" := ");

			if (altType != null) {
				final String altSnippet = altType.getDefaultSnippet(projectItem.getLineEnding(), 1, new AtomicInteger(1));
				sb.append(altSnippet);
			}

			sb.append(projectItem.getLineEnding())
			.append("}");
			final CompletionItem compItem = new CompletionItem();
			compItem.setLabel(altName);
			compItem.setInsertText(sb.toString());
			compItem.setKind(CompletionItemKind.Snippet);
			compItem.setInsertTextFormat(InsertTextFormat.Snippet);
			completions.add(compItem);
		}
	}

	private void addCompletionItemsForEnumItems(Set<CompletionItem> completions, TTCN3_Enumerated_Type enumerated) {
		enumerated.getEnumItemsOrdered().forEach(enumItem ->
			completions.add(enumItem.getCompletionItem())
		);
	}

	private void addDefaultTypeSnippetCompletionItem(Set<CompletionItem> completions, Def_Type definition) {
		final Path path = IOUtils.getTextDocumentPath(contextInfo.document);
		final ProjectItem projectItem = contextInfo.module.getProject().getProjectItem(path);
		if (projectItem == null) {
			return;
		}

		final Type defType = definition.getType(contextInfo.module.getLastCompilationTimeStamp());
		if (defType == null) {
			return;
		}

		final String typeSnippet = defType.getDefaultSnippet(projectItem.getLineEnding(), 1, new AtomicInteger(1));
		final CompletionItem defaultTypeSnippetCompletion = definition.getCompletionItem();
		defaultTypeSnippetCompletion.setKind(CompletionItemKind.Snippet);
		defaultTypeSnippetCompletion.setInsertTextFormat(InsertTextFormat.Snippet);
		defaultTypeSnippetCompletion.setInsertText(typeSnippet);
		completions.add(defaultTypeSnippetCompletion);
	}

	private void collectCompletionItemsOfTypeDefinition(Set<CompletionItem> completions, Def_Type definition) {
		final Type defType = definition.getType(contextInfo.module.getLastCompilationTimeStamp());
		if (defType == null) {
			return;
		}

		addDefaultTypeSnippetCompletionItem(completions, definition);
		
		if (defType instanceof TTCN3_Enumerated_Type) {
			addCompletionItemsForEnumItems(completions, (TTCN3_Enumerated_Type)defType);
			return;
		}

		if (defType instanceof TTCN3_Choice_Type) {
			addCompletionItemsForUnionAlternatives(completions, (TTCN3_Choice_Type)defType);
		}
	}

	private void addTextEditToCompletionItems(List<CompletionItem> completions) {
		final Statement actualStatement = contextInfo.actualStatement;
		final Definition actualDefinition = contextInfo.actualDefinition;
		Range replacementRange = null;
		// by default the new text is inserted at the cursor position
		if (actualStatement != null) {
			if (actualStatement instanceof Assignment_Statement) {
				final TTCN3Template template = ((Assignment_Statement)actualStatement).getTemplate();
				if (template != null) {
					replacementRange = template.getLocation().getRange();
				}
			}

			if (actualStatement instanceof Definition_Statement) {
				final Definition definition = ((Definition_Statement)actualStatement).getDefinition();
				if (definition instanceof Def_Var) {
					final Value initialValue = ((Def_Var)definition).getInitialValue();
					if (initialValue != null) {
						replacementRange = initialValue.getLocation().getRange();
					}
				} else if (definition instanceof Def_Const) {
					final IValue value = ((Def_Const)definition).getValue();
					if (value != null) {
						replacementRange = ((Value)value).getLocation().getRange();
					}
				}
			}
		}

		if (actualDefinition instanceof Def_Const) {
			final IValue value = ((Def_Const)actualDefinition).getValue();
			if (value != null) {
				replacementRange = ((Value)value).getLocation().getRange();
			}
		}

		if (replacementRange == null) {
			return;
		}

		for (CompletionItem completionItem : completions) {
			final InsertReplaceEdit insRepEdit = new InsertReplaceEdit();
			insRepEdit.setInsert(replacementRange);
			insRepEdit.setReplace(replacementRange);
			if (completionItem.getInsertText() == null) {
				continue;
			}
			insRepEdit.setNewText(completionItem.getInsertText());
			completionItem.setTextEdit(Either.forRight(insRepEdit));
		}
	}

	private List<CompletionItem> collectCompletions() {
		final CompilationTimeStamp timestamp = contextInfo.module.getLastCompilationTimeStamp();
		final Set<CompletionItem> completions = new HashSet<>();

		if (contextInfo.module instanceof TTCN3Module) {
			final TTCN3Module module = (TTCN3Module)contextInfo.module;
			final List<Definition> visibleDefinitions = module.getAllVisibleDefinitions(contextInfo.cursorPosition);

			visibleDefinitions.forEach(def -> {
				if (aMatch.isTypeDefinitionMatch(def)) {
					collectCompletionItemsOfTypeDefinition(completions, ((Def_Type)def));
					return;
				}

				if (aMatch.isCompletionMatch(timestamp, def)) {
					completions.add(def.getCompletionItem());
				}
			});
		}

		final List<CompletionItem> completionItemList = new ArrayList<>(completions);

		addTextEditToCompletionItems(completionItemList);

		return completionItemList;
	}

	@Override
	public Either<List<CompletionItem>, CompletionList> getCompletions() {
		final List<CompletionItem> completions = collectCompletions();
		return Either.forLeft(completions);
	}

}
