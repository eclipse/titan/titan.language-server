/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionParams;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticTag;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.codeAction.CodeActionProviderFactoryProducer.CodeActionProviderFactoryType;
import com.google.gson.JsonObject;

public class CodeActionCollector {
	private CodeActionParams codeActionParams;
	private List<Either<Command, CodeAction>> codeActions = new ArrayList<>();

	public CodeActionCollector(final CodeActionParams codeActionParams) {
		this.codeActionParams = Objects.requireNonNull(codeActionParams);
		collectCodeActionsFromDiagnostics();
	}

	private void collectCodeActions(final CodeActionProviderFactoryType factoryType, final Diagnostic diagnostic) {
		CodeActionProviderFactoryProducer.getFactory(factoryType).ifPresent(factory -> {
			final ICodeActionProvider provider = factory.createProvider(codeActionParams.getTextDocument(), diagnostic);
			if (provider != null) {
				codeActions.addAll(provider.provideCodeActions());
			}
		});
	}

	private void collectCodeActionsFromDiagnostics() {
		for (Diagnostic diagnostic : codeActionParams.getContext().getDiagnostics()) {
			final Object jsonData = diagnostic.getData();
			if (jsonData != null) {
				final DiagnosticData diagnosticData = DiagnosticData.fromJson((JsonObject) jsonData);
				diagnostic.setData(diagnosticData);

				collectCodeActions(CodeActionProviderFactoryType.DATA, diagnostic);
			}

			final List<DiagnosticTag> tags = diagnostic.getTags();
			if (tags != null && !tags.isEmpty()) {
				collectCodeActions(CodeActionProviderFactoryType.TAG, diagnostic);
			}
		}
	}

	public List<Either<Command, CodeAction>> getCodeActions() {
		return codeActions;
	}
}
