/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.values;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Bitstring_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public final class Named_Bits extends Value implements ICollection<Identifier> {
	private static final String DUPLICATED_NAMED_BIT = "Duplicate named bit `{0}''";

	private final List<Identifier> identifierList = new ArrayList<Identifier>();
	private final Map<String, Identifier> identifierMap = new HashMap<String, Identifier>();

	private Bitstring_Value realValue;

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder(CURLYOPEN);
		boolean first = true;
		for (final Entry<String, Identifier> entry : identifierMap.entrySet()) {
			if (!first) {
				builder.append(' ');
			}
			builder.append(entry.getValue().getDisplayName());
			first = false;
		}
		builder.append(CURLYCLOSE);

		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return (lastTimeChecked == null || lastTimeChecked.isLess(timestamp) ||
				realValue == null || realValue.getIsErroneous(timestamp)) ? Type_type.TYPE_UNDEFINED :
					realValue.getExpressionReturntype(timestamp, expectedValue);
	}

	@Override
	public int size() {
		return identifierMap.size();
	}

	@Override
	public Identifier get(final int i) {
		return identifierList.get(i);
	}

	@Override
	public boolean add(final Identifier id) {
		if (identifierMap.containsKey(id.getName())) {
			id.getLocation().reportSyntacticError(MessageFormat.format(DUPLICATED_NAMED_BIT, id.getDisplayName()));
			return false;
		}
		identifierMap.put(id.getName(), id);
		return identifierList.add(id);
	}

	public void setRealValue(final Bitstring_Value realValue) {
		this.realValue = realValue;
	}

	@Override
	/** {@inheritDoc} */
	public IValue getReferencedSubValue(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final IReferenceChain refChain) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (getIsErroneous(timestamp) || subreferences.size() <= actualSubReference) {
			return this;
		}

		final IValue result = getValueRefdLast(timestamp, refChain);
		if (null != result && result != this) {
			return result.getReferencedSubValue(timestamp, reference, actualSubReference, refChain);
		}

		final IType type = myGovernor.getTypeRefdLast(timestamp);
		if (type.getIsErroneous(timestamp)) {
			return null;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);
		switch (subreference.getReferenceType()) {
		case arraySubReference:
			subreference.getLocation().reportSemanticError(
					MessageFormat.format(ArraySubReference.INVALIDVALUESUBREFERENCE, type.getTypename()));
			return null;
		case fieldSubReference:
			location.reportSemanticError(MessageFormat.format(FieldSubReference.INVALIDSUBREFERENCE, ((FieldSubReference) subreference)
					.getId().getDisplayName(), type.getTypename()));
			return null;
		case parameterisedSubReference:
			subreference.getLocation().reportSemanticError(ParameterisedSubReference.INVALIDVALUESUBREFERENCE);
			return null;
		default:
			subreference.getLocation().reportSemanticError(ISubReference.INVALIDSUBREFERENCE);
			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public Value_type getValuetype() {
		return Value_type.NAMED_BITS;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnfoldable(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		return realValue == null || realValue.getIsErroneous(timestamp)
				|| realValue.isUnfoldable(timestamp, expectedValue, referenceChain);
	}

	@Override
	/** {@inheritDoc} */
	public IValue getValueRefdLast(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		return (realValue == null || realValue.getIsErroneous(timestamp)) ? this :
			realValue.getValueRefdLast(timestamp, expectedValue, referenceChain);
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkEquality(final CompilationTimeStamp timestamp, final IValue other) {
		return realValue != null && realValue.checkEquality(timestamp, other);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final Identifier id : identifierList) {
			if (!id.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<Identifier> iterator() {
		return identifierList.iterator();
	}
}
