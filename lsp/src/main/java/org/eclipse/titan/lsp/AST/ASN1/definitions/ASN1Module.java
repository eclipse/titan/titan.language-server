/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignments;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ModuleImportationChain;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.ASN1.ASN1Assignments;
import org.eclipse.titan.lsp.AST.ASN1.Defined_Reference;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.parsers.ProjectStructureDataCollector;

/**
 * Class to represent ASN-modules.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 */
public final class ASN1Module extends Module {
	private static final String FULLNAMEPART1 = ".<exports>";
	private static final String FULLNAMEPART2 = ".<imports>";

	private static final String NOASSIGNMENT = "There is no assignment with name `{0}'' in module `{1}''";
	public static final String NOASSIGNMENTORSYMBOL = "There is no assignment or imported symbol with name `{0}'' in module `{1}''";
	private static final String MORESYMBOLS = "There are more imported symbols with name `{0}'' in module `{1}''";
	private static final String NOIMPORTEDMODULE = "There is no imported module with name `{0}''";
	private static final String NOSYMBOLSIMPORTED = "There is no symbol with name `{0}'' imported from module `{1}''";

	/** default tagging. */
	private final Tag_types tagdef;

	/**
	 * Extensibility implied means in ASN.1 that all assignments in that
	 * module should be treated as extendable.
	 * <p>
	 * The information is only stored, but not supported for now (documented limitation).
	 * */
	private final boolean extensibilityImplied;

	/** exported stuff. */
	private Exports exports;
	/** imported stuff. */
	private Imports imports;

	private ASN1Assignments assignments;

	private boolean needsTobeBuilt = true;

	public ASN1Module(final Identifier identifier, final Tag_types tagdef, final boolean extensibilityImplied) {
		super(identifier);
		this.tagdef = tagdef;
		this.extensibilityImplied = extensibilityImplied;
		exports = new Exports(false);
		exports.setMyModule(this);
		exports.setFullNameParent(this);
		imports = new Imports();
		imports.setMyModule(this);
		imports.setFullNameParent(this);
		assignments = new ASN1Assignments();
		assignments.setParentScope(this);
		assignments.setFullNameParent(this);
	}

	@Override
	/** {@inheritDoc} */
	public module_type getModuletype() {
		return module_type.ASN_MODULE;
	}

	/**
	 * Sets the export list of the module.
	 * @param exports the exports to be set.
	 */
	public void setExports(final Exports exports) {
		this.exports = exports;
		exports.setMyModule(this);
		exports.setFullNameParent(this);
	}

	/**
	 * Sets the import list of the module.
	 * @param imports the imports to be set.
	 */
	public void setImports(final Imports imports) {
		this.imports = imports;
		imports.setMyModule(this);
		imports.setFullNameParent(this);
	}

	/**
	 * Sets the assignment list of the module.
	 * @param assignments the assignments to be set.
	 */
	public void setAssignments(final ASN1Assignments assignments) {
		this.assignments = assignments;
		assignments.setParentScope(this);
		addSubScope(assignments.getLocation(), assignments);
		assignments.setFullNameParent(this);
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = new StringBuilder();
		builder.append(INamedNode.MODULENAMEPREFIX).append(getIdentifier().getDisplayName());

		if (exports == child) {
			return builder.append(FULLNAMEPART1);
		} else if (imports == child) {
			return builder.append(FULLNAMEPART2);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public Assignments getAssignmentsScope() {
		return assignments;
	}

	@Override
	/** {@inheritDoc} */
	public Def_Type getAnytype() {
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		return new Object[] { imports, assignments };
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		return EMPTY_STRING;
	}

	@Override
	/** {@inheritDoc} */
	public void checkImports(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain, final List<Module> moduleStack) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastImportCheckTimeStamp != null && !lastImportCheckTimeStamp.isLess(timestamp)) {
			return;
		}

		assignments.checkUniqueness(timestamp);
		imports.checkImports(timestamp, referenceChain, moduleStack);
		exports.check(timestamp);

		lastImportCheckTimeStamp = timestamp;
		// TODO compiler: method named collect_visible_mods should be here
	}

	@Override
	/** {@inheritDoc} */
	public Assignment importAssignment(final CompilationTimeStamp timestamp, final Identifier moduleId, final Reference reference) {
		final Identifier id = reference.getId();
		return assignments.getLocalAssignmentByID(timestamp, id);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isVisible(final CompilationTimeStamp timestamp, final Identifier moduleId, final Assignment assignment) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public List<Module> getImportedModules() {
		return imports.getImportedModules();
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasUnhandledImportChanges() {
		return imports.hasUnhandledImportChanges();
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (isBuildCancelled()) {
			return;
		}

		assignments.postCheck();
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;
		needsTobeBuilt = true;

		if (!SpecialASN1Module.isSpecAsss(this)) {
			NamingConventionHelper.checkConvention(NamingConventionElement.Asn1Module, identifier, "ASN.1 module");
		}

		imports.check(timestamp);
		assignments.check(timestamp);

	}

	@Override
	/** {@inheritDoc} */
	public Assignments getAssignments() {
		return assignments;
	}

	/**
	 * Checks if there is a symbol exported with the provided identifier.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param identifier the identifier used to search for a symbol.
	 * @return {@code true} if a symbol with the provided name is exported,
	 *         {@code false} otherwise.
	 */
	public boolean exportsSymbol(final CompilationTimeStamp timestamp, final Identifier identifier) {
		return exports.exportsSymbol(timestamp, identifier);
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasImportedAssignmentWithID(final CompilationTimeStamp timestamp, final Identifier identifier) {
		return imports.singularImportedSymbols_map.containsKey(identifier.getName());
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		Identifier moduleId = reference.getModuleIdentifier();
		final Identifier id = reference.getId();

		if (id == null) {
			return null;
		}

		Module module = null;

		if (moduleId == null || moduleId.getName().equals(identifier.getName())) {
			if (assignments.hasLocalAssignmentWithID(timestamp, id)) {
				return assignments.getLocalAssignmentByID(timestamp, id);
			}
			if (moduleId != null) {
				id.getLocation().reportSemanticError(
						MessageFormat.format(NOASSIGNMENT, id.getDisplayName(), identifier.getDisplayName()));
				return null;
			}
			if (imports.singularImportedSymbols_map.containsKey(id.getName())) {
				module = imports.singularImportedSymbols_map.get(id.getName());
				moduleId = module.getIdentifier();
				imports.getImportedModuleById(moduleId).setUsedForImportation();
			} else if (imports.pluralImportedSymbols.contains(id.getName())) {
				id.getLocation().reportSemanticError(
						MessageFormat.format(MORESYMBOLS, id.getDisplayName(), identifier.getDisplayName()));
				return null;
			} else {
				id.getLocation().reportSemanticError(
						MessageFormat.format(NOASSIGNMENTORSYMBOL, id.getDisplayName(), identifier.getDisplayName()));
				return null;
			}
		}

		if (module == null) {
			if (!imports.hasImportedModuleWithId(moduleId)) {
				moduleId.getLocation().reportSemanticError(MessageFormat.format(NOIMPORTEDMODULE, moduleId.getDisplayName()));
				return null;
			}
			if (!imports.getImportedModuleById(moduleId).hasSymbol(id)) {
				id.getLocation().reportSemanticError(
						MessageFormat.format(NOSYMBOLSIMPORTED, id.getDisplayName(), moduleId.getDisplayName()));
				return null;
			}

			imports.getImportedModuleById(moduleId).setUsedForImportation();
			ProjectSourceParser parser = GlobalParser.getProjectSourceParser(project);
			if (parser == null) {
				return null;
			}

			module = parser.getModuleByName(moduleId.getName());
		}

		if (this == module || module == null) {
			return null;
		}

		final List<ISubReference> newSubreferences = new ArrayList<ISubReference>();
		newSubreferences.add(new FieldSubReference(id));
		final Defined_Reference finalReference = new Defined_Reference(null, newSubreferences);

		// FIXME add semantic check guard on project level.
		return module.getAssBySRef(timestamp, finalReference);
	}

	@Override
	/** {@inheritDoc} */
	public String toString() {
		final StringBuilder builder = new StringBuilder("module: ").append(name);
		return builder.toString();
	}
	
	@Override
	/** {@inheritDoc} */
	public void extractStructuralInformation(final ProjectStructureDataCollector collector) {
		imports.extractStructuralInformation(identifier, collector);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		if (assignments == null) {
			return null;
		}
		return assignments.getEnclosingAssignment(offset);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (assignments != null) {
			assignments.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (exports != null) {
			if (!exports.accept(v)) {
				return false;
			}
		}
		if (imports != null) {
			if (!imports.accept(v)) {
				return false;
			}
		}
		if (assignments != null) {
			if (!assignments.accept(v)) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		return SymbolKind.Module;
	}
	
	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		if (identifier == null) {
			return null;
		}
		final DocumentSymbol symbols = new DocumentSymbol(identifier.getDisplayName(), getOutlineSymbolKind(),
			getOutlineRange(), getOutlineSelectionRange());
		symbols.setChildren(new ArrayList<>());
		
		return symbols;
	}
	
	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return location.getRange();
	}
	
	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() {
		return identifier.getLocation().getRange();
	}
}
