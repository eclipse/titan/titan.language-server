/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.values.expressions;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.values.Charstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public final class TestcasenameExpression extends Expression_Value {
	@Override
	/** {@inheritDoc} */
	public Operation_type getOperationType() {
		return Operation_type.TESTCASENAME_OPERATION;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return Type_type.TYPE_CHARSTRING;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReference(final CompilationTimeStamp timestamp, final Assignment lhs) {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		return "testcasename()";
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnfoldable(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IValue evaluateValue(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}

		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;

		// Evaluate only if it's sure that we're in a test case or we're
		// directly called from the control part. Always return the
		// unqualified name or "".
		if (myScope != null) {
			if (myScope instanceof StatementBlock) {
				final StatementBlock block = (StatementBlock) myScope;
				final Definition definition = block.getMyDefinition();
				if (definition == null) {
					// An error would be better here.
					lastValue = new Charstring_Value("");
					lastValue.copyGeneralProperties(this);
				} else {
					if (Assignment_type.A_TESTCASE.semanticallyEquals(definition.getAssignmentType())) {
						lastValue = new Charstring_Value(definition.getIdentifier().getDisplayName());
						lastValue.copyGeneralProperties(this);
					}
				}
			}
		}

		// Run-time evaluation.
		return lastValue;
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		// no members
		return true;
	}
}
