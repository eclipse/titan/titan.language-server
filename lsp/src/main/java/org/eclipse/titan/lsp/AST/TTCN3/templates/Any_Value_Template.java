/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import java.util.Set;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents the any template.
 *
 * @author Kristof Szabados
 * */
public final class Any_Value_Template extends TTCN3Template {
	private static final String SIGNATUREERROR = "Generic wildcard `?'' cannot be used for signature `{0}''";

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.ANY_VALUE;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder(QUESTION_MARK);
		return addToStringRepresentation(builder).toString();
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {
		final IType last = type.getTypeRefdLast(timestamp);
		if (Type_type.TYPE_SIGNATURE.equals(last.getTypetype())) {
			location.reportSemanticError(MessageFormat.format(SIGNATUREERROR, last.getFullName()));
			setIsErroneous(true);
		}
		checkThisTemplateGenericPostChecks(timestamp, type, allowOmit, subCheck);
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean chkRestrictionNamedListBaseTemplate(final CompilationTimeStamp timestamp, final String definitionName,
			final boolean omitAllowed, final Set<String> checkedNames, final int neededCheckedCnt, final Location usageLocation) {
		usageLocation.reportSemanticError(MessageFormat.format(RESTRICTIONERROR, definitionName, getTemplateTypeName()));
		return false;
	}
}
