/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticRelatedInformation;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.common.product.Configuration;

/**
 * This class represents a diagnostic marker.
 * It supports related information and can point to another diagnostic. This can be used
 * to report paired problems like duplicate definitions where we can relate the original
 * definition with the conflicting one. This way the paired diagnostic can be easily
 * located.
 * 
 * @author Miklos Magyari
 *
 */
public final class LspMarker implements Comparable<LspMarker> {
	private static boolean isTestMode;

	static {
		isTestMode = Configuration.INSTANCE.getBoolean(Configuration.TEST_MODE, false);
		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.TEST_MODE,
			preferenceName ->
				isTestMode = Configuration.INSTANCE.getBoolean(Configuration.TEST_MODE, false)
		);
	}

	private Diagnostic diagnostic;
	private List<DiagnosticRelatedInformation> relatedInformations = new ArrayList<>();
	private boolean isSyntactic = false;

	public LspMarker(final Diagnostic diagnostic) {
		this(diagnostic, false);
	}

	public LspMarker(final Diagnostic diagnostic, final boolean isSyntactic) {
		this.diagnostic = diagnostic;
		this.diagnostic.setRelatedInformation(relatedInformations);
		this.setSyntactic(isSyntactic);
	}

	public Diagnostic getDiagnostic() {
		return diagnostic;
	}

	public void setDiagnostic(final Diagnostic diagnostic) {
		this.diagnostic = diagnostic;
	}

	public List<DiagnosticRelatedInformation> getRelatedInformations() {
		return relatedInformations;
	}

	public void addRelatedInformation(final Location relatedLocation, final String message) {
		DiagnosticRelatedInformation related = new DiagnosticRelatedInformation(relatedLocation.getLspLocation(), message);
		relatedInformations.add(related);
	}

	public void addRelatedInformations(final List<DiagnosticRelatedInformation> relatedInformations) {
		this.relatedInformations.addAll(relatedInformations);
	}

	public void setRelatedInformations(final List<DiagnosticRelatedInformation> relatedInformations) {
		this.relatedInformations = relatedInformations;
	}

	public boolean hasRelatedInformation() {
		return !relatedInformations.isEmpty();
	}

	public boolean isSyntactic() {
		return isSyntactic;
	}

	public boolean isSemantic() {
		return !isSyntactic;
	}

	private void setSyntactic(final boolean isSyntactic) {
		this.isSyntactic = isSyntactic;
		if (isTestMode) {
			diagnostic.setSource(isSyntactic ? GeneralConstants.ONTHEFLY_SYNTACTIC_MARKER : GeneralConstants.ONTHEFLY_SEMANTIC_MARKER);
		}
	}

	@Override
	/** {@inheritDoc} */
	public int compareTo(LspMarker o) {
		final Range oldRange = new Range(o.getDiagnostic().getRange());
		final Range range = new Range(diagnostic.getRange());
		return range.beforeOrEquals(oldRange) ? -1 : 1;
	}

	@Override
	/** {@inheritDoc} */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof LspMarker)) {
			return false;
		}
		LspMarker m = (LspMarker)obj;
		return isSyntactic == m.isSyntactic() &&
				relatedInformations.equals(m.getRelatedInformations()) &&
				diagnostic.equals(m.getDiagnostic());
	}

	@Override
	/** {@inheritDoc} */
	public int hashCode() {
		return Objects.hash(diagnostic, relatedInformations, isSyntactic);
	}
}
