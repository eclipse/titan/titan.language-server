/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsp4j.ParameterInformation;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SignatureInformation;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.IIdentifiable;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * This interface represents AST nodes that support signature help functionality of
 * the language server protocol.
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public interface ISignatureHelp extends IParameterisedAssignment, ICommentable, IIdentifiable {
	/**
	 * Returns the {@link SignatureHelp} for this parametrised assignment.
	 * It contains the textual signature of the node and the list of its parameters
	 * with additional document information extracted from document comments.
	 * 
	 * @return
	 */
	default SignatureHelp getSignatureHelp() {
		final SignatureHelp help = new SignatureHelp();
		final FormalParameterList fpl = getFormalParameterList();
		final List<ParameterInformation> paramList = new ArrayList<>(fpl.size()); 
		final DocumentComment docComment = getDocumentComment();
		fpl.forEach(param -> {
			final String paramName = param.getIdentifier().getDisplayName();
			StringBuilder commentSb = new StringBuilder();
			if (docComment != null) {
				docComment.getParams().entrySet().stream()
					.filter(e -> e.getKey().equals(paramName))
					.forEach(e -> commentSb.append(e.getValue()));
			}
			paramList.add(new ParameterInformation(paramName, commentSb.toString()));
		});
		final List<SignatureInformation> signatureList = new ArrayList<>();
		signatureList.add(new SignatureInformation(getSignature(), GeneralConstants.EMPTY_STRING, paramList));
		help.setSignatures(signatureList);
		return help;
	}

	/**
	 * Gets the signature in form of:<br>
	 * <br>
	 * <code>identifier(param_type1 param_name1, param_type2 param_name2, ...)</code>
	 * <br><br>
	 * Used by LSP signature help.
	 *  
	 * @return the signature in the specified form
	 */
	private String getSignature() {
		final StringBuilder text = new StringBuilder(getIdentifier().getDisplayName());
		final FormalParameterList fpl = getFormalParameterList();
		final CompilationTimeStamp ts = getLastTimeChecked();
		if (fpl == null || ts == null) {
			return text.toString();
		}

		text.append('(');
		for (int i = 0; i < fpl.size(); i++) {
			if (i != 0) {
				text.append(", ");
			}

			final FormalParameter parameter = fpl.get(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				text.append("timer");
			} else {
				final IType type = parameter.getType(ts);
				final String formalParamType = parameter.getFormalParamType();
				if (!formalParamType.isEmpty()) {
					text.append("[").append(formalParamType).append("] ");
				}
				if (type == null) {
					text.append("Unknown type");
				} else {
					text.append(type.getTypename());
				}
				text.append(' ')
					.append(parameter.getIdentifier().getDisplayName());
			}
		}
		text.append(')');
		return text.toString();
	}

	CompilationTimeStamp getLastTimeChecked();
}
