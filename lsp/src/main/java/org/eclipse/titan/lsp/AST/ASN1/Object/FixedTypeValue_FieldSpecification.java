/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ISetting;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.IType.ValueCheckingOptions;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.ASN1.IASN1Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Class to represent a FixedTypeValueFieldSpec.
 *
 * @author Kristof Szabados
 */
public final class FixedTypeValue_FieldSpecification extends FieldSpecification {
	public static final String OPTIONAL_DEFAULT_COLLISION = "OPTIONAL and DEFAULT are mutual exclusive";
	private static final String UNIQUE_DEFAULT_COLLISION = "UNIQUE and DEFAULT are mutual exclusive";
	/** Fixed type. */
	private final IASN1Type fixedType;
	private final boolean isUnique;
	// FIXME only temporal solution, should be corrected when values become
	// fully supported for this usage
	private final boolean hasDefault;
	private final IValue defaultValue;

	public FixedTypeValue_FieldSpecification(final Identifier identifier, final IASN1Type fixedType, final boolean isUnique,
			final boolean isOptional, final boolean hasDefault, final IValue defaultValue) {
		super(identifier, isOptional);
		this.fixedType = fixedType;
		this.isUnique = isUnique;
		this.hasDefault = hasDefault;
		this.defaultValue = defaultValue;

		if (null != fixedType) {
			fixedType.setOwnertype(TypeOwner_type.OT_FT_V_FLD, this);
			fixedType.setFullNameParent(this);
		}
		if (null != defaultValue) {
			defaultValue.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Fieldspecification_types getFieldSpecificationType() {
		return Fieldspecification_types.FS_V_FT;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyObjectClass(final ObjectClass_Definition objectClass) {
		super.setMyObjectClass(objectClass);
		if (null != fixedType) {
			fixedType.setMyScope(myObjectClass.getMyScope());
		}
		if (null != defaultValue) {
			defaultValue.setMyScope(myObjectClass.getMyScope());
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasDefault() {
		return hasDefault;
	}

	@Override
	public ISetting getDefault() {
		return defaultValue;
	}

	public IASN1Type getType() {
		return fixedType;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnique() {
		return isUnique;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (null != lastTimeChecked && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		if (null != fixedType) {
			fixedType.check(timestamp);

			if (null != defaultValue) {
				if (isOptional) {
					getLocation().reportSemanticError(OPTIONAL_DEFAULT_COLLISION);
					isOptional = false;
				} else if (isUnique) {
					getLocation().reportSemanticError(UNIQUE_DEFAULT_COLLISION);
				}

				defaultValue.setMyGovernor(fixedType);
				final IValue tempValue = fixedType.checkThisValueRef(timestamp, defaultValue);
				fixedType.checkThisValue(timestamp, tempValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT,
						false, false, true, false, false));
			}
		}

		lastTimeChecked = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (identifier != null && !identifier.accept(v)) {
			return false;
		}
		if (fixedType != null && !fixedType.accept(v)) {
			return false;
		}
		if (defaultValue != null && !defaultValue.accept(v)) {
			return false;
		}
		return true;
	}
}
