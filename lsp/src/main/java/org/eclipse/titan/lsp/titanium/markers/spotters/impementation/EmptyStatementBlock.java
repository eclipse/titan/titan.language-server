/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class EmptyStatementBlock extends BaseModuleCodeSmellSpotter {
	private static final String ERROR_MESSAGE = "Empty statement block";

	public EmptyStatementBlock() {
		super(CodeSmellType.EMPTY_STATEMENT_BLOCK);
		addStartNode(StatementBlock.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof StatementBlock) {
			final StatementBlock s = (StatementBlock) node;
			
			/** Altstep statement block */ 
			if (s.getMyDefinition() instanceof Def_Altstep) {
				final Def_Altstep altstep = (Def_Altstep)s.getMyDefinition();
				if (altstep.getAltGuards().getNofAltguards() > 0) {
					return;
				}
			}
			
			if (s.isEmpty()) {
				problems.report(s.getLocation(), ERROR_MESSAGE);
			}
		}
	}
}
