/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.values.expressions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * */
public final class ComponentCreateExpression extends Expression_Value {
	private static final String FIRSTOPERANDERROR = "The first operand of operation `create()' should be a charstring value";
	private static final String SECONDOPERANDERROR = "The second operand of operation `create()' should be a charstring value";
	public static final String COMPONENTEXPECTED = "Operation `create'' should refer to a component type instead of {0}";
	public static final String TYPEMISMATCH1 = "Type mismatch: reference to a component type was expected in operation `create'' instead of `{0}''";
	public static final String TYPEMISMATCH2 = "Incompatible component type: operation `create'' should refer to `{0}'' instead of `{1}''";

	private final Reference componentReference;
	private final Value name;
	private final Value componentLocation;
	private final boolean isAlive;

	private CompilationTimeStamp checkCreateTimestamp;
	private Component_Type checkCreateCache;

	public ComponentCreateExpression(final Reference componentReference, final Value name, final Value location, final boolean isAlive) {
		this.componentReference = componentReference;
		this.name = name;
		this.componentLocation = location;
		this.isAlive = isAlive;

		if (componentReference != null) {
			componentReference.setFullNameParent(this);
		}
		if (name != null) {
			name.setFullNameParent(this);
		}
		if (location != null) {
			location.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Operation_type getOperationType() {
		return Operation_type.COMPONENT_CREATE_OPERATION;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReference(final CompilationTimeStamp timestamp, final Assignment lhs) {
		// assume no self-ref
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		if (componentReference == null) {
			return ERRONEOUS_VALUE;
		}

		final StringBuilder builder = new StringBuilder();
		builder.append(componentReference.getDisplayName());
		builder.append(".create");
		if (name != null || componentLocation != null) {
			builder.append(LEFTPARENTHESES);
			if (name != null) {
				builder.append(name.createStringRepresentation());
			} else {
				builder.append(MINUS_SIGN_CHAR);
			}
			if (componentLocation != null) {
				builder.append(", ");
				builder.append(componentLocation.createStringRepresentation());
			}
			builder.append(RIGHTPARENTHESES);
		}
		if (isAlive) {
			builder.append(" alive");
		}

		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (componentReference != null) {
			componentReference.setMyScope(scope);
		}
		if (name != null) {
			name.setMyScope(scope);
		}
		if (componentLocation != null) {
			componentLocation.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (componentReference == child) {
			return builder.append(OPERAND1);
		} else if (name == child) {
			return builder.append(OPERAND2);
		} else if (componentLocation == child) {
			return builder.append(OPERAND3);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return Type_type.TYPE_COMPONENT;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnfoldable(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return checkCreate(timestamp);
	}

	/**
	 * Checks the parameters of the expression and if they are valid in
	 * their position in the expression or not.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param expectedValue the kind of value expected.
	 * @param referenceChain a reference chain to detect cyclic references.
	 */
	private void checkExpressionOperands(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		checkCreate(timestamp);
		if (name != null) {
			final IValue last = name.setLoweridToReference(timestamp);
			final Type_type typeType = last.getExpressionReturntype(timestamp, expectedValue);
			if (!last.getIsErroneous(timestamp)) {
				switch (typeType) {
				case TYPE_CHARSTRING:
					last.getValueRefdLast(timestamp, referenceChain);
					break;
				case TYPE_UNDEFINED:
					break;
				default:
					name.getLocation().reportSemanticError(FIRSTOPERANDERROR);
					setIsErroneous(true);
					break;
				}
			}
		}
		if (componentLocation != null) {
			final IValue last = componentLocation.setLoweridToReference(timestamp);
			final Type_type typeType = last.getExpressionReturntype(timestamp, expectedValue);
			if (!last.getIsErroneous(timestamp)) {
				switch (typeType) {
				case TYPE_CHARSTRING:
					last.getValueRefdLast(timestamp, referenceChain);
					break;
				case TYPE_UNDEFINED:
					break;
				default:
					componentLocation.getLocation().reportSemanticError(SECONDOPERANDERROR);
					setIsErroneous(true);
					break;
				}
			}
		}
		checkExpressionDynamicPart(expectedValue, UndefCreateExpression.OPERATIONNAME, false, true, false);
	}

	@Override
	/** {@inheritDoc} */
	public IValue evaluateValue(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}

		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;

		if (componentReference == null) {
			return lastValue;
		}

		checkExpressionOperands(timestamp, expectedValue, referenceChain);

		return lastValue;
	}

	private Component_Type checkCreate(final CompilationTimeStamp timestamp) {
		if (checkCreateTimestamp != null && !checkCreateTimestamp.isLess(timestamp)) {
			return checkCreateCache;
		}

		checkCreateTimestamp = timestamp;
		checkCreateCache = null;

		if (componentReference == null) {
			return null;
		}

		final Assignment assignment = componentReference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			setIsErroneous(true);
			return null;
		}

		if (!Assignment_type.A_TYPE.semanticallyEquals(assignment.getAssignmentType())) {
			componentReference.getLocation().reportSemanticError(MessageFormat.format(COMPONENTEXPECTED, assignment.getDescription()));
			setIsErroneous(true);
			return null;
		}

		final IType type = ((Def_Type) assignment).getType(timestamp).getFieldType(timestamp, componentReference, 1,
				Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
		if (type == null) {
			setIsErroneous(true);
			return null;
		}

		if (!Type_type.TYPE_COMPONENT.equals(type.getTypetype())) {
			componentReference.getLocation().reportSemanticError(MessageFormat.format(TYPEMISMATCH1, type.getTypename()));
			setIsErroneous(true);
			return null;
		}

		if (myGovernor != null) {
			final IType last = myGovernor.getTypeRefdLast(timestamp);

			if (Type_type.TYPE_COMPONENT.equals(last.getTypetype()) && !last.isCompatible(timestamp, type, null, null, null)) {
				componentReference.getLocation().reportSemanticError(
						MessageFormat.format(TYPEMISMATCH2, last.getTypename(), type.getTypename()));
				setIsErroneous(true);
				return null;
			}
		}

		checkCreateCache = (Component_Type) type;
		return checkCreateCache;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (componentReference != null) {
			componentReference.updateSyntax(reparser, false);
			reparser.updateLocation(componentReference.getLocation());
		}

		if (name != null) {
			name.updateSyntax(reparser, false);
			reparser.updateLocation(name.getLocation());
		}

		if (componentLocation != null) {
			componentLocation.updateSyntax(reparser, false);
			reparser.updateLocation(componentLocation.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (componentReference != null) {
			componentReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (name != null) {
			name.findReferences(referenceFinder, foundIdentifiers);
		}
		if (componentLocation != null) {
			componentLocation.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (componentReference != null && !componentReference.accept(v)) {
			return false;
		}
		if (name != null && !name.accept(v)) {
			return false;
		}
		if (componentLocation != null && !componentLocation.accept(v)) {
			return false;
		}
		return true;
	}
}
