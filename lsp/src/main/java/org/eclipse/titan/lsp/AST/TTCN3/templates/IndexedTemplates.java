/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Class to represent a list of indexed templates.
 *
 * @author Kristof Szabados
 */
public final class IndexedTemplates extends ASTNode
	implements IIncrementallyUpdatable, ICollection<IndexedTemplate> {

	private final ArrayList<IndexedTemplate> indexed_templates;

	public IndexedTemplates() {
		super();
		indexed_templates = new ArrayList<IndexedTemplate>();
	}

	@Override
	public boolean add(final IndexedTemplate template) {
		template.setFullNameParent(this);
		return indexed_templates.add(template);
	}

	@Override
	public int size() {
		return indexed_templates.size();
	}

	@Override
	public IndexedTemplate get(final int index) {
		return indexed_templates.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		indexed_templates.trimToSize();
		for (final IndexedTemplate template : indexed_templates) {
			template.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final IndexedTemplate template : indexed_templates) {
			if (template == child) {
				final IValue index = template.getIndex().getValue();
				return builder.append(INamedNode.SQUAREOPEN).append(index.createStringRepresentation())
						.append(INamedNode.SQUARECLOSE);
			}
		}

		return builder;
	}

	/**
	 * Handles the incremental parsing of this list of indexed templates.
	 *
	 * @param reparser
	 *                the parser doing the incremental parsing.
	 * @param isDamaged
	 *                true if the location contains the damaged area, false
	 *                if only its' location needs to be updated.
	 * */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final IndexedTemplate template : indexed_templates) {
			template.updateSyntax(reparser, false);
			reparser.updateLocation(template.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final IndexedTemplate indexedTemp : indexed_templates) {
			indexedTemp.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final IndexedTemplate it : indexed_templates) {
			if (!it.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<IndexedTemplate> iterator() {
		return indexed_templates.iterator();
	}
}
