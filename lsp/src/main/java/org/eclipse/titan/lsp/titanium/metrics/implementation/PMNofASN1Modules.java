/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.implementation;

import org.eclipse.titan.lsp.titanium.metrics.common.MetricData;
import org.eclipse.titan.lsp.titanium.metrics.common.ProjectMetric;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ASN1.definitions.ASN1Module;
import org.eclipse.titan.lsp.core.Project;

public class PMNofASN1Modules extends BaseProjectMetric {
	public PMNofASN1Modules() {
		super(ProjectMetric.NOF_ASN1_MODULES);
	}

	@Override
	public Number measure(final MetricData data, final Project p) {
		int count = 0;
		for (final Module m : data.getModules()) {
			if (m instanceof ASN1Module) {
				++count;
			}
		}
		return count;
	}
}

