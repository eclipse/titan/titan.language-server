/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.GeneralConstants;

/**
 * @author Kristof Szabados
 * */
public final class Identifier_Internal_Data {
	public static final String INVALID_STRING = "<invalid>";

	private enum AsnKind {
		/** undefined. */
		ASN_UNDEF,
		/** lower identifier. */
		ASN_LOWER,
		/** upper identifier. */
		ASN_UPPER,
		/** all upper identifier. */
		ASN_ALLUPPER,
		/** word. */
		ASN_WORD,
		/** ampersand + upper identifier. */
		ASN_AMPUPPER,
		/** ampersand + lower identifier.  */
		ASN_AMPLOWER;
	}

	private final String name;
	private String asnName;
	private String ttcnName;

	// ASN kind of identifier
	private AsnKind asnKind;

	public Identifier_Internal_Data(final String name, final String asnName, final String ttcnName) {
		this.name = name;

		if (name.equals(asnName)) {
			this.asnName = name;
		} else {
			this.asnName = asnName;
		}

		if (name.equals(ttcnName)) {
			this.ttcnName = name;
		} else {
			this.ttcnName = ttcnName;
		}

		asnKind = AsnKind.ASN_UNDEF;
	}

	public static String asnToName(final String from) {
		final StringBuilder builder = new StringBuilder(from);
		/* "@aaa" -> "_root_aaa" */
		if (builder.length() > 0 && GeneralConstants.AT_CHAR == builder.charAt(0)) {
			builder.replace(0, 1, "_root_");
		}
		/* "aa.<xxxx>.bb" -> "aa.bb" */
		int index = 0;
		int index2 = 0;
		while (index2 != -1  && index != -1) {
			index = builder.indexOf(".<", index);
			if (index != -1) {
				index2 = builder.indexOf(">.", index);
				if (index2 != -1) {
					builder.replace(index, index2 + 1, GeneralConstants.EMPTY_STRING);
				}
			}
		}
		/* "-" -> "__" */
		index = builder.indexOf(GeneralConstants.MINUS_SIGN, 0);
		while (index != -1) {
			builder.replace(index, index + 1, GeneralConstants.DOUBLE_UNDERSCORE);
			index += 2;
			index = builder.indexOf(GeneralConstants.MINUS_SIGN, index);
		}
		/* "." -> "_" */
		index = builder.indexOf(GeneralConstants.DOT, 0);
		while (index != -1) {
			builder.replace(index, index + 1, GeneralConstants.UNDERSCORE);
			index++;
			index = builder.indexOf(GeneralConstants.DOT, index);
		}
		/* "&" -> "" */
		index = builder.indexOf(GeneralConstants.AND_SIGN, 0);
		while (index != -1) {
			builder.replace(index, index + 1, GeneralConstants.EMPTY_STRING);
			index = builder.indexOf(GeneralConstants.AND_SIGN, index);
		}

		final String result = builder.toString();
		if (result.equals(from)) {
			return from;
		}

		return builder.toString();
	}

	public static String nameToAsn(final String from) {
		final StringBuilder builder = new StringBuilder(from);
		int index = 0;
		/* remove leading '_'s */
		while (index < builder.length() && GeneralConstants.UNDERSCORE_CHAR == builder.charAt(index)) {
			index++;
		}
		if (index > 0) {
			builder.delete(0, index);
		}
		/* remove trailing '_'s */
		index = builder.length();
		while (index > 0 && GeneralConstants.UNDERSCORE_CHAR == builder.charAt(index - 1)) {
			index--;
		}
		if (index != builder.length()) {
			builder.delete(index, builder.length());
		}
		/* "__" -> "-" */
		index = builder.indexOf(GeneralConstants.DOUBLE_UNDERSCORE, 0);
		while (index != -1) {
			builder.replace(index, index + 2, GeneralConstants.MINUS_SIGN);
			index++;
			index = builder.indexOf(GeneralConstants.DOUBLE_UNDERSCORE, index);
		}
		/* "_" -> "-" */
		index = builder.indexOf(GeneralConstants.UNDERSCORE, 0);
		while (index != -1) {
			builder.replace(index, index + 1, GeneralConstants.MINUS_SIGN);
			index++;
			index = builder.indexOf(GeneralConstants.UNDERSCORE, index);
		}

		final String result = builder.toString();
		if (result.equals(from)) {
			return from;
		}

		return builder.toString();
	}

	public static String ttcnToName(final String from) {
		final StringBuilder builder = new StringBuilder(from);
		/* "_" -> "__" */
		int index = builder.indexOf(GeneralConstants.UNDERSCORE, 0);
		while (index != -1) {
			builder.replace(index, index + 1, GeneralConstants.DOUBLE_UNDERSCORE);
			index += 2;
			index = builder.indexOf(GeneralConstants.UNDERSCORE, index);
		}

		final String result = builder.toString();
		if (result.equals(from)) {
			return from;
		}

		return builder.toString();
	}

	public static String nameToTtcn(final String from) {
		final StringBuilder builder = new StringBuilder(from);
		int index = 0;
		/* remove leading '_'s */
		while (index < builder.length() && GeneralConstants.UNDERSCORE_CHAR == builder.charAt(index)) {
			index++;
		}
		if (index > 0) {
			builder.delete(0, index);
		}
		/* remove trailing '_'s */
		index = builder.length();
		while (index > 0 && GeneralConstants.UNDERSCORE_CHAR == builder.charAt(index - 1)) {
			index--;
		}
		if (index != builder.length()) {
			builder.delete(index, builder.length());
		}
		/* "__" -> "_" */
		index = builder.indexOf(GeneralConstants.DOUBLE_UNDERSCORE, 0);
		while (index != -1) {
			builder.deleteCharAt(index);
			index = builder.indexOf(GeneralConstants.DOUBLE_UNDERSCORE, index);
		}

		final String result = builder.toString();
		if (result.equals(from)) {
			return from;
		}

		return builder.toString();
	}

	public String getName() {
		return name;
	}

	public String getAsnName() {
		if (asnName == null) {
			final String temp = nameToAsn(name);
			if (name.equals(temp)) {
				asnName = name;
			} else {
				asnName = temp;
			}
		}
		return asnName;
	}

	public String getTtcnName() {
		if (ttcnName == null) {
			final String temp = nameToTtcn(name);
			if (name.equals(temp)) {
				ttcnName = name;
			} else {
				ttcnName = temp;
			}
		}
		return ttcnName;
	}

	private void decideAsnKind() {
		if (!AsnKind.ASN_UNDEF.equals(asnKind)) {
			return;
		}

		if (INVALID_STRING.equals(asnName)) {
			return;
		}

		if (asnName.charAt(0) == GeneralConstants.AND_SIGN_CHAR) {
			if (asnName.length() > 2) {
				if (Character.isUpperCase(asnName.charAt(1))) {
					asnKind = AsnKind.ASN_AMPUPPER;
				} else if (Character.isLowerCase(asnName.charAt(1))) {
					asnKind = AsnKind.ASN_AMPLOWER;
				}
			}
		} else if (Character.isLowerCase(asnName.charAt(0))) {
			asnKind = AsnKind.ASN_LOWER;
		} else if (Character.isUpperCase(asnName.charAt(0))) {
			asnKind = AsnKind.ASN_UPPER;
			boolean hasLower = false;
			boolean hasDigit = false;
			for (int i = 0; i < asnName.length(); i++) {
				if (Character.isLowerCase(asnName.charAt(i))) {
					hasLower = true;
				} else if (Character.isDigit(asnName.charAt(i))) {
					hasDigit = true;
				}
			}

			if (!hasLower) {
				asnKind = AsnKind.ASN_ALLUPPER;
				if (!hasDigit) {
					asnKind = AsnKind.ASN_WORD;
				}
			}
		}
	}

	public boolean isvalidAsnTyperef() {
		decideAsnKind();

		return (asnKind == AsnKind.ASN_UPPER) || (asnKind == AsnKind.ASN_ALLUPPER) || (asnKind == AsnKind.ASN_WORD);
	}

	public boolean isvalidAsnValueReference() {
		decideAsnKind();

		return asnKind == AsnKind.ASN_LOWER;
	}

	public boolean isvalidAsnObjectClassReference() {
		decideAsnKind();

		return (asnKind == AsnKind.ASN_ALLUPPER) || (asnKind == AsnKind.ASN_WORD);
	}

	public boolean isvalidAsnObjectReference() {
		decideAsnKind();

		return asnKind == AsnKind.ASN_LOWER;
	}

	public boolean isvalidAsnValueFieldReference() {
		decideAsnKind();

		return asnKind == AsnKind.ASN_AMPLOWER;
	}

	public boolean isvalidAsnObjectFieldReference() {
		decideAsnKind();

		return asnKind == AsnKind.ASN_AMPLOWER;
	}

	public boolean isvalidAsnObjectSetFieldReference() {
		decideAsnKind();

		return asnKind == AsnKind.ASN_AMPUPPER;
	}

	public boolean isvalidAsnWord() {
		decideAsnKind();

		return asnKind == AsnKind.ASN_WORD;
	}
}
