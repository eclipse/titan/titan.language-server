/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.hover;

import java.text.MessageFormat;
import java.util.EnumMap;
import java.util.Map;

import org.eclipse.titan.lsp.GeneralConstants;

/**
 * This class represents a categorized collection of strings (containing html)
 * 
 * Used for ttcn3 editor hover support returned by getHoverInfo2 as an 'object'
 *
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public final class Ttcn3HoverContent {
	private static final String LINK_PATTERN = "<a href=\"{0}\">{0}</a>";
	private static final String ICON_PATTERN = "<div class=\"title-with-icon\"><img src=\"{0}\"><span style=\"margin-left: 5px\">";
	private static final String BOLD_STYLE = "**";
	private static final String ITALIC_STYLE = GeneralConstants.UNDERSCORE;
	private static final String SPACE = GeneralConstants.EMPTY_STRING;
	private static final String INDENTAION = "    ";
	private static final String DEPRECATED_TEXT = "This object is deprecated.";

	public static final int ITALIC = 1;
	public static final int BOLD = 2;
	public static final int INDENTED = 4;
	
	private StringBuilder sb = new StringBuilder();

	private Map<HoverContentType, String> map = new EnumMap<>(HoverContentType.class);

	private boolean hasIcon = false;

	/**
	 * Default constructor
	 */
	public Ttcn3HoverContent() {
		// Do nothing
	}

	/**
	 * Adds the specified text as {@link HoverContentType#INFO} to the content
	 * @param text text to add as {@link HoverContentType#INFO}
	 */
	public Ttcn3HoverContent(final String text) {
		this(HoverContentType.INFO, text);
	}

	/**
	 * Adds the specified text to the specified content type
	 * @param type content type
	 * @param text text to add
	 */
	public Ttcn3HoverContent(final HoverContentType type, final String text) {
		addContent(type, text);
	}

	/**
	 * Adds the specified text to the specified content type
	 * @param type content type
	 * @param text text to add
	 */
	public void addContent(final HoverContentType type, final String text) {
		if (text == null || text.isEmpty()) {
			map.put(type, text);
			return;
		}
		sb = new StringBuilder(text);
		addContent(type);
	}

	public void addContent(final HoverContentType type) {
		map.put(type, sb.toString());
		sb.setLength(0);
	}

	public boolean hasText(final HoverContentType type) {
		return map.get(type) != null;
	}

	public String getText(final HoverContentType type) {
		return getText(type, false);
	}
	
	public String getText(final HoverContentType type, boolean isDocView) {
//		String url = null;
//		switch (ColorManager.getColorTheme()) {
//		case Dark:
//			url = getStyleSheetUrl("HoverInfoDark.css");
//			break;
//		case Light:
//		default:
//			url = getStyleSheetUrl("HoverInfo.css");
//			break;
//		}
//		StringBuilder styled = new StringBuilder();
//		if (url != null) {
//			styled.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
//			styled.append(url);
//			styled.append("\">");
//			
//		} else {
//			styled.append(sb);
//		}
//		Color background = null;
//		if (isDocView) {
//			background = PreferenceHandler.getEditorBackgroundColor();
//		} else {
//			background = ColorManager.getInformationBackgroundColor();
//		}
//		
//		styled.append("<style>body { background-color: rgb(");
//		styled.append(background.getRed()).append(", ").append(background.getGreen()).append(", ")
//			.append(background.getBlue()).append("); }</style>");
		
//		final String text = styled.append(map.get(type)).toString();
		final String text = map.get(type);
		if (text != null) {
			return text.replace("@@", GeneralConstants.AT);
		}
		return null;
	}

	public Ttcn3HoverContent addText(final String text) {
		sb.append(text);
		return this;
	}
	
	public Ttcn3HoverContent addTag(final String text) {
		sb.append(text);
		return this;
	}

	public Ttcn3HoverContent addTagWithText(final String tag, final String text) {
		sb.append("\n").append(INDENTAION)
			.append(tag).append(SPACE)
			.append(text);
		return this;
	}

	// FIXME :: add styles
	public Ttcn3HoverContent addStyledText(final String text) {
		sb.append(text);
		return this;
	}

	public Ttcn3HoverContent addStyledText(final String text, final int style) {
		final boolean isItalic = (style & ITALIC) > 0;
		final boolean isBold = (style & BOLD) > 0;
		final boolean isIndented = (style & INDENTED) > 0;
		
		if (isIndented) {
			sb.append(INDENTAION);
		}
		if (isItalic) {
			sb.append(ITALIC_STYLE);
		}
		if (isBold) {
			sb.append(BOLD_STYLE);
		}
		sb.append(text);
		if (isBold) {
			sb.append(BOLD_STYLE);
		}
		if (isItalic) {
			sb.append(ITALIC_STYLE);
		}
		return this;
	}
	
	public Ttcn3HoverContent addIndentedText(final String text) {
		return addIndentedText(text, 0);
	}

	public Ttcn3HoverContent addIndentedText(final String text, final int style) {
		return addStyledText(text, style & INDENTED);
	}

	public Ttcn3HoverContent addIndentedText(final String indentTo, final String textToIndent) {
		if (indentTo == null || textToIndent == null) {
			return this;
		}
		sb.append(INDENTAION)
			.append(indentTo).append(SPACE)
			.append(textToIndent);
		
		return this;
	}

	public Ttcn3HoverContent addUrlAsLink(final String url) {
		return addUrlAsLink(url, 0);
	}

	public Ttcn3HoverContent addUrlAsLink(final String url, final int style) {
		if (url == null || url.isEmpty()) {
			return this;
		}
		return addIndentedText(MessageFormat.format(LINK_PATTERN, url), style);
	}

	/**
	 * Adds the specified icon from the icons directory to header. If it is used, the header <b>MUST</b> be closed.
	 * @param iconUri the name of the gif file from the icons directory
	 * @return a reference to this object
	 */
	public Ttcn3HoverContent addIcon(final String iconUri) {
		if (iconUri == null || iconUri.isEmpty()) {
			return this;
		}
		hasIcon = true;
		return addText(MessageFormat.format(ICON_PATTERN, getIconUrl(iconUri)));
	}
	
	public Ttcn3HoverContent addDeprecated() {
		return addStyledText(DEPRECATED_TEXT, ITALIC)
			.addText("\n\n");
	}

	public boolean hasIcon() {
		return hasIcon;
	}

	/**
	 * If icon is added to the header, this will close the header section.
	 * @return a reference to this object
	 */
	public Ttcn3HoverContent closeHeader() {
		if (hasIcon) {
			//sb.append(SPAN_END).append(DIV_END);
		}
		return this;
	}
	private String getIconUrl(final String icon) {
//		return getUrl("icons", icon);
		return GeneralConstants.EMPTY_STRING;
	}
}
