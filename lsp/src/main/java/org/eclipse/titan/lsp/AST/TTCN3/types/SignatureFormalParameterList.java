/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Signature parameter-list.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 */
public final class SignatureFormalParameterList extends ASTNode
	implements IIncrementallyUpdatable, ILocateableNode, ICollection<SignatureFormalParameter> {
	private static final String FULLNAMEPART = FormalParameterList.FULLNAMEPART;
	private static final String NONBLOCKING_ERROR = "A non-blocking signature cannot have `out' parameter";
	private static final String EMBEDDED_ERROR = "the type of a signature parameter";

	private final List<SignatureFormalParameter> parameters;
	/** A map of the parameters. */
	private final Map<String, SignatureFormalParameter> parameterMap;

	private final List<SignatureFormalParameter> inParameters;
	private final List<SignatureFormalParameter> outParameters;

	/** the time when this formal parameter list was check the last time. */
	private CompilationTimeStamp lastTimeChecked;

	private Location location = Location.getNullLocation();

	public SignatureFormalParameterList(final List<SignatureFormalParameter> parameters) {
		if (parameters == null || parameters.isEmpty()) {
			this.parameters = Collections.emptyList();
			parameterMap = Collections.emptyMap();
			inParameters = Collections.emptyList();
			outParameters = Collections.emptyList();
		} else {
			this.parameters = new ArrayList<SignatureFormalParameter>(parameters.size());
			parameterMap = new HashMap<String, SignatureFormalParameter>(parameters.size());
			inParameters = new ArrayList<SignatureFormalParameter>();
			outParameters = new ArrayList<SignatureFormalParameter>();

			for (final SignatureFormalParameter parameter : parameters) {
				if (parameter != null && parameter.getIdentifier() != null) {
					this.parameters.add(parameter);
					parameter.setFullNameParent(this);
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);
		for (final SignatureFormalParameter parameter : parameters) {
			if (parameter == child) {
				final Identifier identifier = parameter.getIdentifier();
				return builder.append(INamedNode.DOT).append(identifier != null ? identifier.getDisplayName() : FULLNAMEPART);
			}
		}
		return builder;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public void setLocation(final Location location) {
		this.location = location;
	}

	/**
	 * Sets the scope of the formal parameter list.
	 * @param scope the scope to be set
	 */
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		for (final SignatureFormalParameter parameter : parameters) {
			parameter.setMyScope(scope);
		}
	}

	@Override
	public int size() {
		return parameters.size();
	}

	@Override
	public SignatureFormalParameter get(final int index) {
		return parameters.get(index);
	}

	@Override
	public boolean contains(final Object name) {
		return parameterMap.containsKey(name);
	}

	/**
	 * Returns the parameter with the specified name.
	 *
	 * @param name the name of the parameter to return
	 * @return the element with the specified name in this list
	 */
	public SignatureFormalParameter get(final String name) {
		return parameterMap.get(name);
	}

	/** @return the number of in parameters */
	public int getNofInParameters() {
		return inParameters.size();
	}

	/**
	 * Returns the in parameter at the specified position.
	 *
	 * @param index index of the element to return
	 * @return the formal parameter at the specified position in this list of in parameters
	 */
	public SignatureFormalParameter getInParameterByIndex(final int index) {
		return inParameters.get(index);
	}

	/** @return the number of ou parameters */
	public int getNofOutParameters() {
		return outParameters.size();
	}

	/**
	 * Returns the out parameter at the specified position.
	 *
	 * @param index index of the element to return
	 * @return the formal parameter at the specified position in this list of out parameters
	 */
	public SignatureFormalParameter getOutParameterByIndex(final int index) {
		return outParameters.get(index);
	}

	/**
	 * Checks the uniqueness of the parameters, and also builds a hashmap of
	 * them to speed up further searches.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle,
	 *            or {@code -1} in silent check mode.
	 */
	private void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		for (final SignatureFormalParameter parameter : parameters) {
			final String parameterName = parameter.getIdentifier().getName();
			if (parameterMap.containsKey(parameterName)) {
				parameterMap.get(parameterName).getIdentifier().getLocation().reportSingularSemanticError(
						MessageFormat.format(FormalParameterList.DUPLICATEPARAMETERFIRST, parameter.getIdentifier().getDisplayName()));
				parameter.getIdentifier().getLocation().reportSemanticError(
						MessageFormat.format(FormalParameterList.DUPLICATEPARAMETERREPEATED, parameter.getIdentifier().getDisplayName()));
			} else {
				parameterMap.put(parameterName, parameter);
			}
		}
	}

	public void check(final CompilationTimeStamp timestamp, final Signature_Type signature) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;

		if (parameters.isEmpty()) {
			return;
		}

		parameterMap.clear();
		inParameters.clear();
		outParameters.clear();

		checkUniqueness(timestamp);

		final boolean isNonblock = signature.isNonblocking();
		for (final SignatureFormalParameter parameter : parameters) {
			switch (parameter.getDirection()) {
			case PARAM_IN:
				inParameters.add(parameter);
				break;
			case PARAM_INOUT:
				if (isNonblock) {
					parameter.getLocation().reportSemanticError(NONBLOCKING_ERROR);
				}
				inParameters.add(parameter);
				outParameters.add(parameter);
				break;
			case PARAM_OUT:
			default:
				if (isNonblock) {
					parameter.getLocation().reportSemanticError(NONBLOCKING_ERROR);
				}
				outParameters.add(parameter);
				break;
			}

			final Type type = parameter.getType();
			type.setParentType(signature);
			type.check(timestamp);
			type.checkEmbedded(timestamp, type.getLocation(), false, EMBEDDED_ERROR);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final SignatureFormalParameter parameter : parameters) {
			parameter.updateSyntax(reparser, isDamaged);
			reparser.updateLocation(parameter.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final SignatureFormalParameter sfp : parameters) {
			sfp.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final SignatureFormalParameter sfp : parameters) {
			if (!sfp.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean add(SignatureFormalParameter e) {
		throw new UnsupportedOperationException("Adding parameter is not allowed: " + e.toString());
	}

	@Override
	public Iterator<SignatureFormalParameter> iterator() {
		return parameters.iterator();
	}
}
