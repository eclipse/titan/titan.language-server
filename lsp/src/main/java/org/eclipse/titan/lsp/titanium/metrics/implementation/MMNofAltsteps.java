/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.implementation;

import org.eclipse.titan.lsp.titanium.metrics.common.MetricData;
import org.eclipse.titan.lsp.titanium.metrics.common.ModuleMetric;
import org.eclipse.titan.lsp.AST.Module;

public class MMNofAltsteps extends BaseModuleMetric {
	public MMNofAltsteps() {
		super(ModuleMetric.NOF_ALTSTEPS);
	}

	@Override
	public Number measure(final MetricData data, final Module module) {
		return data.getAltsteps().get(module).size();
	}
}
