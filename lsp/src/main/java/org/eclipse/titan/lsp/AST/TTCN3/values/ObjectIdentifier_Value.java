/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.values;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.values.ObjectIdentifierComponent.oidState_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * */
public final class ObjectIdentifier_Value extends Value {
	private final List<ObjectIdentifierComponent> objectIdComponents = new ArrayList<ObjectIdentifierComponent>();

	@Override
	/** {@inheritDoc} */
	public Value_type getValuetype() {
		return Value_type.OBJECTID_VALUE;
	}

	public void add(final ObjectIdentifierComponent component) {
		if (component == null) {
			return;
		}

		objectIdComponents.add(component);
		component.setFullNameParent(this);
		component.setMyScope(myScope);
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		ObjectIdentifierComponent component;
		for (int i = 0, size = objectIdComponents.size(); i < size; i++) {
			component = objectIdComponents.get(i);
			if (component == child) {
				return builder.append(INamedNode.DOT).append(i + 1);
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return Type_type.TYPE_OBJECTID;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		if (!isAsn()) {
			builder.append("objid ");
		}

		builder.append('{');
		for (int i = 0, size = objectIdComponents.size(); i < size; i++) {
			if (i > 0) {
				builder.append(' ');
			}
			builder.append(objectIdComponents.get(i).createStringRepresentation());
		}
		builder.append('}');
		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (final ObjectIdentifierComponent component : objectIdComponents) {
			component.setMyScope(scope);
		}
	}

	public int size() {
		return objectIdComponents.size();
	}

	@Override
	/** {@inheritDoc} */
	public IValue getReferencedSubValue(final CompilationTimeStamp timestamp, final Reference reference,
			final int actualSubReference, final IReferenceChain refChain) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (getIsErroneous(timestamp) || subreferences.size() <= actualSubReference) {
			return this;
		}

		final IType type = myGovernor.getTypeRefdLast(timestamp);
		final ISubReference subreference = subreferences.get(actualSubReference);
		switch (subreference.getReferenceType()) {
		case arraySubReference:
			subreference.getLocation().reportSemanticError(MessageFormat.format(ArraySubReference.INVALIDVALUESUBREFERENCE, type.getTypename()));
			return null;
		case fieldSubReference:
			subreference.getLocation().reportSemanticError(MessageFormat.format(
					FieldSubReference.INVALIDSUBREFERENCE, ((FieldSubReference) subreference).getId().getDisplayName(), type.getTypename()));
			return null;
		case parameterisedSubReference:
			subreference.getLocation().reportSemanticError(ParameterisedSubReference.INVALIDVALUESUBREFERENCE);
			return null;
		default:
			subreference.getLocation().reportSemanticError(ISubReference.INVALIDSUBREFERENCE);
			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnfoldable(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		checkOID(timestamp, referenceChain);

		for (final ObjectIdentifierComponent component : objectIdComponents) {
			if (component.isVariable()) {
				return true;
			}
		}

		return false;
	}

	public void checkOID(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (objectIdComponents.isEmpty()) {
			return;
		}

		if (!referenceChain.add(this)) {
			return;
		}

		oidState_type state = oidState_type.START;
		for (final ObjectIdentifierComponent component : objectIdComponents) {
			referenceChain.markState();
			state = component.checkOID(timestamp, referenceChain, this, state);
			referenceChain.previousState();
		}

		if (!oidState_type.LATER.equals(state) && !oidState_type.ITU_REC.equals(state)) {
			// X.680 (07/2002) 31.10
			getLocation().reportSemanticError("An OBJECT IDENTIFIER value must have at least two components");
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean evaluateIsvalue(final boolean fromSequence) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkEquality(final CompilationTimeStamp timestamp, final IValue other) {
		final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final IValue last = other.getValueRefdLast(timestamp, referenceChain);
		referenceChain.release();

		if (!Value_type.OBJECTID_VALUE.equals(last.getValuetype())) {
			return false;
		}

		final ObjectIdentifier_Value otherObjid = (ObjectIdentifier_Value) last;
		if (objectIdComponents.size() != otherObjid.objectIdComponents.size()) {
			return false;
		}

		for (int i = 0, size = objectIdComponents.size(); i < size; i++) {
			if (objectIdComponents.get(i) != otherObjid.objectIdComponents.get(i)) {
				return false;
			}
		}

		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		for (final ObjectIdentifierComponent component : objectIdComponents) {
			component.updateSyntax(reparser, false);
			reparser.updateLocation(component.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final ObjectIdentifierComponent c : objectIdComponents) {
			if (!c.accept(v)) {
				return false;
			}
		}
		return true;
	}
}
