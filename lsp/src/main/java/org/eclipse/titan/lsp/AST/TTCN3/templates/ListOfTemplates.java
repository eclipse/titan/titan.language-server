/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents  the BNF element "ListOfTemplates"
 * Ref: ttcn3 standard "ETSI ES 201 873-1 V4.6.1 (2014-06)"
 * A.1.6.1.3 Template definitions/125.
 *
 * Replaces the obsolete class Templates which had a field templates of a different type "TTCN3Template"
 *
 * @author Jeno Balasko
 * @author Adam Knapp
 */
public class ListOfTemplates extends ASTNode implements IIncrementallyUpdatable, ICollection<TTCN3Template> {

	private final List<TTCN3Template> templates;

	public ListOfTemplates() {
		templates = new ArrayList<TTCN3Template>();
	}

	@Override
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		for (final TTCN3Template template : templates) {
			template.setMyScope(scope);
		}
	}

	@Override
	public boolean add(final TTCN3Template template) {
		return templates.add(template);
	}

	@Override
	public int size() {
		return templates.size();
	}

	@Override
	public TTCN3Template get(final int index) {
		return templates.get(index);
	}

	/**
	 * Handles the incremental parsing of this list of templates.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged {@code true} if the location contains the damaged area,
	 *               {@code false} if only its' location needs to be updated.
	 */
	@Override
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final TTCN3Template template : templates) {
			template.updateSyntax(reparser, false);
			reparser.updateLocation(template.getLocation());
		}
	}

	@Override
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (templates == null) {
			return;
		}

		for (final TTCN3Template  template : templates) {
			template.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	protected boolean memberAccept(final ASTVisitor v) {
		if (templates != null) {
			for (final TTCN3Template t : templates) {
				if (!t.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public Iterator<TTCN3Template> iterator() {
		return templates.iterator();
	}
}
