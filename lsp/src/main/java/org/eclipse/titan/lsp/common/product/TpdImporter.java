/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.product;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.LoadBalancingUtilities;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSParser;
import org.xml.sax.SAXException;

/**
 * TPD file importer 
 * 
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public class TpdImporter {
	private static final String IMPORT_ERROR = "Error while importing from file ";

	private static final String TPD_XSD = "/TPD.xsd";

	private DOMImplementationLS domImplLS;
	private LSParser parser;

	private final Map<String, String> finalProjectNames = new HashMap<String, String>();
	private final Map<URI, Document> projectsToImport = new HashMap<URI, Document>();
	private final List<URI> importChain = new ArrayList<URI>();
	private List<String> searchPaths;
	private final Map<String, String> tpdNameAttrMap = new HashMap<String, String>();
	private final Map<String, String> tpdURIMap = new HashMap<String, String>();
	
	private boolean islimitImportProcesses = false;
	private int processingUnitsToUse;

	/** The TPD file that is activated on the client */
	private File processedTpdFile;

	/** List of source file names collected from all TPDs */
	private final List<Path> collectedFileNames = new CopyOnWriteArrayList<>();
	
	public TpdImporter(final File tpdFile) {
		processedTpdFile = tpdFile;
	}

	public void validateTpd() throws IOException, SAXException {
		if (processedTpdFile == null) {
			return;
		}
		final Schema tpdXsd = getTPDSchema();
		final Validator validator = tpdXsd.newValidator();
		validator.validate(new StreamSource(processedTpdFile));
	}

	public Schema getTPDSchema() throws IOException, SAXException {
		InputStream xsdInputStream = this.getClass().getResourceAsStream(TPD_XSD);
		final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		// completely disable DOCTYPE declaration
		factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		// prohibit the use of all protocols by external entities:
		factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		final Schema result = factory.newSchema(new StreamSource(xsdInputStream));
		xsdInputStream.close();
		return result;
	}
	
	/**
	 * Extracts an XML document from the provided file.
	 *
	 * @param file
	 *            the file to read from.
	 * @return the extracted XML document, or null if there were some error.
	 * */
	public Document getDocumentFromFile(final String file) {
		final LSInput lsInput = domImplLS.createLSInput();
		Document document = null;
		try {
			final FileInputStream istream = new FileInputStream(file);
			lsInput.setByteStream(istream);
			document = parser.parse(lsInput);
			istream.close();
		} catch (Exception e) {
			TitanLogger.logError("While getting the document from `" + file + "'", e);
		}

		return document;
	}
	
	public boolean processTpd(final boolean isSkipExistingProjects,
			final boolean isOpenPropertiesForAllImports, final boolean islimitImportProcesses,
			final List<String> searchPaths) {
		if (processedTpdFile == null) {
			return false;
		}
		final String projectFile = processedTpdFile.getAbsolutePath(); 
		if (projectFile == null || projectFile.trim().isEmpty()) {
			return false;
		}
		this.islimitImportProcesses = islimitImportProcesses;
		if(searchPaths != null) {
			this.searchPaths = new ArrayList<String>(searchPaths);
		}
		System.setProperty(DOMImplementationRegistry.PROPERTY, ProjectFormatConstants.DOM_IMPLEMENTATION_SOURCE);
		DOMImplementationRegistry registry = null;
		try {
			registry = DOMImplementationRegistry.newInstance();
		} catch (Exception e) {
			TitanLogger.logError("While importing from `" + projectFile + "'", e);
			//activatePreviousSettings();
			return false;
		}

		// Specifying "LS 3.0" in the features list ensures that the
		// DOMImplementation
		// object implements the load and save features of the DOM 3.0
		// specification.
		final DOMImplementation domImpl = registry.getDOMImplementation(ProjectFormatConstants.LOAD_SAVE_VERSION);
		domImplLS = (DOMImplementationLS) domImpl;
		// If the mode is MODE_SYNCHRONOUS, the parse and parseURI
		// methods of the LSParser
		// object return the org.w3c.dom.Document object. If the mode is
		// MODE_ASYNCHRONOUS, the parse and parseURI methods return null.
		parser = domImplLS.createLSParser(DOMImplementationLS.MODE_SYNCHRONOUS, ProjectFormatConstants.XML_SCHEMA);

		final DOMConfiguration config = parser.getDomConfig();
		final DOMErrorHandlerImpl errorHandler = new DOMErrorHandlerImpl();
		config.setParameter("error-handler", errorHandler);
		config.setParameter("validate", Boolean.TRUE);
		config.setParameter("schema-type", ProjectFormatConstants.XML_SCHEMA);
		config.setParameter("well-formed", Boolean.TRUE);
		config.setParameter("validate-if-schema", Boolean.TRUE);

		Validator tpdValidator = null;
		try {
			final Schema tpdXsd = getTPDSchema();
			tpdValidator = tpdXsd.newValidator();
		} catch (Exception e) {
			TitanLogger.logFatal(e);
			// Hint: cp $TTCN3_DIR/etc/xsd/TPD.xsd designer/schema/
		}

		URI resolvedProjectFileURI = new File(projectFile).toURI();

		//====================================
		// Loading all URI Documents (tpds) 
		// and collect projects to be imported
		//====================================
		if (!loadURIDocuments(resolvedProjectFileURI, tpdValidator)) {
			return false;
		}
		
		//========================
		// Create projects and 
		// store load location 
		// (where they are loaded from)
		//========================
		final Map<URI, String> projectMap = new ConcurrentHashMap<URI, String>();
		final ThreadFactory threadFactory = r -> {
			final Thread t = new Thread(r);
			t.setPriority(LoadBalancingUtilities.getThreadPriority());
			return t;
		};
		ExecutorService executor;
		if (islimitImportProcesses) {
			executor = Executors.newFixedThreadPool(processingUnitsToUse, threadFactory);
		} else {
			executor = Executors.newCachedThreadPool(threadFactory);
		}

		final AtomicBoolean isErroneous = new AtomicBoolean(false);
		final LinkedBlockingDeque<String> projectsCreatedTemp = new LinkedBlockingDeque<String>();
		final CountDownLatch latch = new CountDownLatch(projectsToImport.size());
		for (final Map.Entry<URI, Document> entry : projectsToImport.entrySet()) {
			final URI file = entry.getKey();
			final Document actualDocument = entry.getValue();

			executor.execute(() -> {
				final String project = actualDocument.getDocumentElement().getAttribute(projectFile);
				if (project == null) {
					if (file.equals(resolvedProjectFileURI)) {
						isErroneous.set(true);
						latch.countDown();
						return;
					} else {
						latch.countDown();
						return;
					}
				}
				projectsCreatedTemp.add(project);
				projectMap.put(file, project);
//				try {
//					project.setPersistentProperty(
//							new QualifiedName(ProjectBuildPropertyData.QUALIFIER, ProjectBuildPropertyData.LOAD_LOCATION), file.getPath()
//									.toString());
//				} catch (CoreException e) {
//					TitanLogger.logError("While loading referenced project from `" + file.getPath() + "'", e);
//				} finally {
//					latch.countDown();
//				}
				latch.countDown();
			});
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdown();
		try {
			executor.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}

		executor.shutdownNow();
		if (isErroneous.get()) {
			return false;
		}
		
		//====================================
		//Load Project Data from all projects:
		//====================================
		ExecutorService executor2;
		if (islimitImportProcesses) {
			executor2 = Executors.newFixedThreadPool(processingUnitsToUse, threadFactory);
		} else  {
			executor2 = Executors.newCachedThreadPool(threadFactory);
		}

		final CountDownLatch latch2 = new CountDownLatch(projectsToImport.size());
		for (final Map.Entry<URI, Document> entry : projectsToImport.entrySet()) {
			final URI file = entry.getKey();

			executor2.execute(() -> {
				if (!projectMap.containsKey(file)) {
					latch2.countDown();
					return;
				}
	
				final String project = projectMap.get(file);
				final Path projectFileFolderPath = Path.of(file).getParent();
				final URI projectFileFolderURI = projectFileFolderPath.toUri();
				final Document actualDocument = entry.getValue();
	
				if (searchPaths != null && !searchPaths.isEmpty()) {
					final String tpdNameAttrVal = tpdNameAttrMap.get(project);
					final String tpdURIVal = tpdURIMap.get(project);
//							if (tpdNameAttrVal != null) {
//								try {
//									project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, ProjectBuildPropertyData.USE_TPD_NAME),
//											tpdNameAttrVal);
//								} catch (CoreException e) {
//									TitanLogger.logError("While setting `useTpdName' for project `" + project.getName() + "'", e);
//								}
//							}
//							if (tpdURIVal != null) {
//								try {
//									project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, ProjectBuildPropertyData.ORIG_TPD_URI),
//											tpdURIVal);
//								} catch (CoreException e) {
//									TitanLogger.logError("While setting `origTpdURI' for project `" + project.getName() + "'", e);
//								}
//							}
				}
	
				final Element mainElement = actualDocument.getDocumentElement();
				//=== Get the copyright text ===
				final Node node = mainElement.getFirstChild();
	
				String commentStr = ""; //default value. This will be changed for PreferenceConstants.COPYRIGHT_DEFAULT_STRING at export
				if (node != null && node.getNodeType() == Node.COMMENT_NODE) {
					//process comment node
					final Comment comment = (Comment) node;
					commentStr = comment.getData();
				}
//						try {
//							project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, ProjectBuildPropertyData.PROJECT_COPYRIGHT_STRING_ID),
//									commentStr);
//						} catch (CoreException e) {
//							TitanLogger.logError("While setting `copyright string' for project `" + project.getName() + "'", e);
//						}
	
//						if (!loadProjectDataFromNode(mainElement, project, projectFileFolderURI)) {
//							latch2.countDown();
//							isErroneous.set(true);
//							return;
//						}
	
				latch2.countDown();
			});
		}
		try {
			latch2.await();
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor2.shutdown();
		try {
			executor2.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor2.shutdownNow();
		if (isErroneous.get()) {
			return false;
		}
		
		//=====================================
		//Load information from packed projects
		//=====================================
		final Path mainProjectFileFolderPath = Path.of(resolvedProjectFileURI).getParent();
		final URI mainProjectFileFolderURI = mainProjectFileFolderPath.toUri();

		final List<String> projectsCreated = new ArrayList<>();

		final File a = new File(resolvedProjectFileURI);
		File absolute = null;
		try {
			absolute = a.getCanonicalFile();
		} catch (IOException e) {
			TitanLogger.logError(e);
			return false;
		}
		if (projectsToImport.get(absolute.toURI()) != null) {
			final List<Node> packedProjects = loadPackedProjects(projectsToImport.get(absolute.toURI()));
			for (final Node node : packedProjects) {
	//			final IProject project = createProject(node, false);
	//			if (project == null) {
	//				packedInformationLoadingMonitor.worked(1);
	//				continue;
	//			}
				projectsCreated.add(node.getNodeName());
			}
		}
		
		projectsToImport.entrySet().stream().forEach(x -> {
			final NodeList referencedProjectsList = x.getValue().getDocumentElement().getChildNodes();
			
			final Node folderNode = getNodebyName(referencedProjectsList, ProjectFormatConstants.FOLDERS_NODE);
			if (folderNode != null) {
				loadFoldersData(folderNode, mainProjectFileFolderURI);
				loadFileList(folderNode, projectFile, x.getKey());
			}
			
			final Node node = getNodebyName(referencedProjectsList, ProjectFormatConstants.FILES_NODE);
			if (node != null) {
				loadFilesData(node, projectFile, mainProjectFileFolderURI);
				loadFileList(node, projectFile, x.getKey());
			}
		});
		
		return islimitImportProcesses;
	}
	
	/**
	 * Searches for the node with the given name, and if found returns it.
	 * 
	 * @param nodeList
	 *                the list of node to search.
	 * @param name
	 *                the name to search for.
	 * 
	 * @return the node with the provided name, or null if not found.
	 * */
	private static Node getNodebyName(final NodeList nodeList, final String name) {
		for (int i = 0, size = nodeList.getLength(); i < size; i++) {
			final Node temp = nodeList.item(i);
			if (temp.getNodeName().equals(name)) {
				return temp;
			}
		}

		return null;
	}
	
	/**
	 * Load the project information document from the provided file and
	 * recursively for all project files mentioned in the referenced projects
	 * section.
	 *
	 * @param file
	 *            the file to load the data from.
	 * @param validator
	 *            the xml validator. can be <code>null</code>
	 *
	 * @return true if there were no errors, false otherwise.
	 * */
	private boolean loadURIDocuments(final URI file, final Validator validator) {
		final File a = new File(file);
		final Document document = getDocumentFromFile(file.getPath());
		File absolute = null;
		try {
			absolute = a.getCanonicalFile();
		} catch (IOException e) {
			TitanLogger.logError(e);
			return false;
		}
		if (projectsToImport.containsKey(absolute.toURI())) {
			return true;
		}

		if (!"file".equals(file.getScheme()) && !file.getScheme().isEmpty()) {
			TitanLogger.logError("Loading of project information is only supported for local files right now. "
					+ file.toString() + " could not be loaded");
			return false;
		}

		if (document == null) {
			final StringBuilder builder = new StringBuilder();
			builder.append("It was not possible to load the imported project file: '")
				.append(file.toString()).append("'\n");
			for (int i = importChain.size() - 1; i >= 0; --i) {
				builder.append("imported by: '");
				builder.append(importChain.get(i).toString());
				builder.append("'\n");
			}
			TitanLogger.logError(builder.toString());
			return false;
		}
		if (validator != null) {
			try {
				validator.validate(new StreamSource(new File(file)));
			} catch (final Exception e) {
				TitanLogger.logError(IMPORT_ERROR + file.toString() + ": ", e);
				return false;
			}
		}

//		ProjectFileHandler.clearNode(document);

		projectsToImport.put(absolute.toURI(), document);

		final Element mainElement = document.getDocumentElement();
		final NodeList mainNodes = mainElement.getChildNodes();
		final Node referencedProjectsNode = getNodebyName(mainNodes, ProjectFormatConstants.REFERENCED_PROJECTS_NODE);
		if (referencedProjectsNode == null) {
			return true;
		}
//
//		// === Get referenced projects ===
		final Path projectFileFolderPath = Path.of(file).getParent();
		final NodeList referencedProjectsList = referencedProjectsNode.getChildNodes();
		boolean result = true;
		for (int i = 0, size = referencedProjectsList.getLength(); i < size; i++) {
			final Node referencedProjectNode = referencedProjectsList.item(i);
			if (referencedProjectNode.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			final NamedNodeMap attributeMap = referencedProjectNode.getAttributes();
			if (attributeMap == null) {
				continue;
			}
			final Node nameNode = attributeMap.getNamedItem(ProjectFormatConstants.REFERENCED_PROJECT_NAME_ATTRIBUTE);
			if (nameNode == null) {
				TitanLogger.logError(IMPORT_ERROR + file.toString()
						+ ": the name attribute of a referenced project is missing");
				return false;
			}

			final String projectName = nameNode.getTextContent();
			final Node locationNode = attributeMap.getNamedItem(ProjectFormatConstants.REFERENCED_PROJECT_LOCATION_ATTRIBUTE);
			if (locationNode == null) {
				if (i > 0) {
					TitanLogger.logError(IMPORT_ERROR + file
							+ ": the location attribute of the referenced project " + projectName + " is not given");
					
				} else {
					TitanLogger.logError("Import failed while importing from file " + file
							+ ": the location attribute of the referenced project " 
							+ projectName + " is not given.\nPerhaps it is under PackedReferencedProjects");
				}
				break; // project handling continues in processing PackedReferencedProjects
			}

			final String unresolvedProjectLocationURI = locationNode.getTextContent();

			//URI absoluteURI = TITANPathUtilities.resolvePath(unresolvedProjectLocationURI, URIUtil.toURI(projectFileFolderPath));
			final Path unresolved = Path.of(projectFileFolderPath.toString(), unresolvedProjectLocationURI);
			URI absoluteURI = unresolved.toUri();

			String fileName;
			// Determine tpdname
			final Node tpdNameNode = attributeMap.getNamedItem(ProjectFormatConstants.REFERENCED_PROJECT_TPD_NAME_ATTRIBUTE);
			if(tpdNameNode != null) {
				fileName = tpdNameNode.getTextContent();
			}else {
				fileName = projectName + ".tpd";
			}

			tpdNameAttrMap.put(projectName, fileName);
			if (searchPaths != null && !searchPaths.isEmpty()) {
				File f = new File(absoluteURI);
				final Path unresolvedProjectLocationURIPath = Path.of(unresolvedProjectLocationURI);
				if(!unresolvedProjectLocationURIPath.isAbsolute() && (!f.exists() || f.isDirectory())) {
					// Try search paths
					for (final String path : searchPaths) {
						String filePath = path;
						if(path.charAt(path.length() - 1) != '/') {
							filePath += "/";
						}
						filePath += fileName;
						final String systemPath = Path.of(filePath).toString();
						f = new File(systemPath);
						// tpd found
						if (f.exists() && !f.isDirectory()) {
							//absoluteURI = URIUtil.toURI(systemPath);
							tpdURIMap.put(projectName, unresolvedProjectLocationURI);
							break;
						}
					}
				}
			}

			if (absoluteURI!=null && !"file".equals(absoluteURI.getScheme())) {
				final StringBuilder builder = new StringBuilder();
				builder.append("Loading of project information is only supported for local files right now. ")
					.append(absoluteURI.toString()).append(" could not be loaded\n");
				for (int j = importChain.size() - 1; j >= 0; --j) {
					builder.append("imported by: '");
					builder.append(importChain.get(j).toString());
					builder.append("'\n");
				}
				TitanLogger.logError(builder.toString());
				continue;
			}

			importChain.add(file);
			result &= loadURIDocuments(absoluteURI, validator);
			importChain.remove(importChain.size() - 1);
		}

		return result;
	}
	
	/**
	 * Collects the list of packed projects from the provided document.
	 *
	 * @param document
	 *            the document to check.
	 *
	 * @return the list of found packed projects, an empty list if none.
	 * */
	private List<Node> loadPackedProjects(final Document document) {
		final NodeList referencedProjectsList = document.getDocumentElement().getChildNodes();
		final Node packed = getNodebyName(referencedProjectsList, ProjectFormatConstants.PACKED_REFERENCED_PROJECTS_NODE);
		if (packed == null) {
			return new ArrayList<Node>();
		}

		final List<Node> result = new ArrayList<Node>();
		final NodeList projects = packed.getChildNodes();
		for (int i = 0, size = projects.getLength(); i < size; i++) {
			final Node referencedProjectNode = projects.item(i);
			if (ProjectFormatConstants.PACKED_REFERENCED_PROJECT_NODE.equals(referencedProjectNode.getNodeName())) {
				result.add(referencedProjectNode);
			}
		}

		return result;
	}
	
	/**
	 * Load the information describing folders.
	 *
	 * @param foldersNode
	 *            the node to load from.
	 * @param project
	 *            the project to set this information on.
	 * @param projectFileFolderURI
	 *            the location of the project file's folder.
	 *
	 * @return true if the import was successful, false otherwise.
	 * */
	private boolean loadFoldersData(final Node foldersNode, final URI projectFileFolderURI) {
		final NodeList folderNodeList = foldersNode.getChildNodes();

		for (int i = 0, size = folderNodeList.getLength(); i < size; i++) {
			final Node folderItem = folderNodeList.item(i);
			if (folderItem.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			final NamedNodeMap attributeMap = folderItem.getAttributes();
			if (attributeMap == null) {
				continue;
			}

			final Node projectRelativePathNode = attributeMap.getNamedItem(ProjectFormatConstants.FOLDER_ECLIPSE_LOCATION_NODE);
			if (projectRelativePathNode == null) {
//				TitanLogger.logError("Import failed", "Error while importing project " + project.getName()
//						+ " the project relative path attribute of the " + i + "th folder is missing");
				return false;
			}

			final String projectRelativePath = projectRelativePathNode.getTextContent();

			final Node relativeURINode = attributeMap.getNamedItem(ProjectFormatConstants.FOLDER_RELATIVE_LOCATION);
			final Node rawURINode = attributeMap.getNamedItem(ProjectFormatConstants.FOLDER_RAW_LOCATION);

//			final IFolder folder = project.getFolder(projectRelativePath);
//			if (!folder.exists()) {
//				try {
//					if (relativeURINode != null) {
//						final String relativeLocation = relativeURINode.getTextContent();
//						//if relativeLocation == "virtual:/virtual" then
//						//create a workaround according to the rawURI branch
//						if( "virtual:/virtual".equals(relativeLocation) ) {
//							folder.createLink(URI.create(relativeLocation), IResource.ALLOW_MISSING_LOCAL, null);
//							continue;
//						}
//
//						final URI absoluteURI = TITANPathUtilities.resolvePathURI(relativeLocation, URIUtil.toPath(projectFileFolderURI).toOSString());
//						if (absoluteURI == null) {
//							// The URI cannot be resolved - for example it
//							// contains not existing environment variables
//							continue; 
//						}
//						if (TitanURIUtil.isPrefix(projectLocationURI, absoluteURI)) {
//							folder.create(false, true, null);
//						} else {
//							final File tmpFolder = new File(absoluteURI);
//							if (tmpFolder.exists()) {
//								folder.createLink(absoluteURI, IResource.ALLOW_MISSING_LOCAL, null);
//							} else {
//								TitanLogger.logError("Error while importing folders into project `" + project.getName() + "'. Folder `"
//										+ absoluteURI + "' does not exist");
//								continue;
//							}
//						}
//					} else if (rawURINode != null) {
//						final String rawLocation = rawURINode.getTextContent();
//						folder.createLink(URI.create(rawLocation), IResource.ALLOW_MISSING_LOCAL, null);
//					} else {
//						TitanLogger.logError("Cannot create the resource " + folder.getFullPath().toString()
//								+ " the location information is missing or corrupted");
//					}
//				} catch (CoreException e) {
//					TitanLogger.logError("Error while importing folders into project: " + e );
//					// be silent, it can happen normally
//				}
//			} else {
//				TitanLogger.logWarning("Folder to be imported `" + folder.getName()
//						+ "' already exists in project `" + project.getName() + "'");
//			}
		}
		return true;
	}
	
	/**
	 * Load the information describing files.
	 *
	 * @param filesNode
	 *            the node to load from.
	 * @param project
	 *            the project to set this information on.
	 * @param projectFileFolderURI
	 *            the location of the project file's folder.
	 *
	 * @return true if the import was successful, false otherwise.
	 * */
	private boolean loadFilesData(final Node filesNode, final String project, final URI projectFileFolderURI) {
		final NodeList fileNodeList = filesNode.getChildNodes();
		final ThreadFactory threadFactory = r -> {
			final Thread t = new Thread(r);
			t.setPriority(LoadBalancingUtilities.getThreadPriority());
			return t;
		};
		ExecutorService executor;
		if (islimitImportProcesses) {
			executor = Executors.newFixedThreadPool(processingUnitsToUse, threadFactory);
		} else {
			executor = Executors.newCachedThreadPool(threadFactory);
		}

		final AtomicBoolean isErroneous = new AtomicBoolean(false);
		final CountDownLatch latch = new CountDownLatch(fileNodeList.getLength());
		for (int i = 0, size = fileNodeList.getLength(); i < size; i++) {
			final Node fileItem = fileNodeList.item(i);
			final int index = i;

			executor.execute(() -> {
				if (fileItem.getNodeType() != Node.ELEMENT_NODE) {
					latch.countDown();
					return;
				}

				final NamedNodeMap attributeMap = fileItem.getAttributes();
				if (attributeMap == null) {
					// there is no attribute, check next node
					latch.countDown();
					return;
				}

				final Node projectRelativePathNode = attributeMap.getNamedItem(ProjectFormatConstants.FILE_ECLIPSE_LOCATION_NODE);
				if (projectRelativePathNode == null) {
					TitanLogger.logError("Error while importing project " + project
							+ ": some attributes of the " + index + "th file are missing");
					latch.countDown();
					return;
				}

				final String projectRelativePath = projectRelativePathNode.getTextContent();

				final Node relativeURINode = attributeMap.getNamedItem(ProjectFormatConstants.FILE_RELATIVE_LOCATION);
				final Node rawURINode = attributeMap.getNamedItem(ProjectFormatConstants.FILE_RAW_LOCATION);

				final File targetFile = new File(projectRelativePath);
				if (!targetFile.exists()) {
					if (relativeURINode != null) {
						final String relativeLocation = relativeURINode.getTextContent();
						//perhaps the next few lines should be implemented as in the function loadFoldersData()
						final URI absoluteURI = projectFileFolderURI;
						if (absoluteURI == null) {
							TitanLogger.logError("Error while importing files into project `" + project
									+ "'. File `" + absoluteURI + "' does not exist");
							latch.countDown();
							return;
						}

						final File file = new File(absoluteURI);
						if (file.exists()) {
							//targetFile.createLink(absoluteURI, IResource.ALLOW_MISSING_LOCAL, null);
						} else {
							TitanLogger.logError("Error while importing files into project `" + project
									+ "'. File `" + absoluteURI + "' does not exist");
							latch.countDown();
							return;
						}
					} else if (rawURINode != null) {
						final String rawURI = rawURINode.getTextContent();
						//targetFile.createLink(URI.create(rawURI), IResource.ALLOW_MISSING_LOCAL, null);
					} else {
//						TitanLogger.logError("Can not create the resource " + targetFile.getFullPath().toString()
//								+ " the location information is missing or corrupted");
						latch.countDown();
						return;
					}
				}

				latch.countDown();
			});
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdown();
		try {
			executor.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdownNow();
		if (isErroneous.get()) {
			return false;
		}

		return true;
	}
	
	private boolean loadFileList(final Node filesNode, String project, URI projectUri) {
		final NodeList fileNodeList = filesNode.getChildNodes();
		final ThreadFactory threadFactory = r -> {
			final Thread t = new Thread(r);
			t.setPriority(LoadBalancingUtilities.getThreadPriority());
			return t;
		};
		ExecutorService executor;
		if (islimitImportProcesses) {
			executor = Executors.newFixedThreadPool(processingUnitsToUse, threadFactory);
		} else {
			executor = Executors.newCachedThreadPool(threadFactory);
		}

		final AtomicBoolean isErroneous = new AtomicBoolean(false);
		final CountDownLatch latch = new CountDownLatch(fileNodeList.getLength());
		for (int i = 0, size = fileNodeList.getLength(); i < size; i++) {
			final Node fileItem = fileNodeList.item(i);

			executor.execute(() -> {
				if (fileItem.getNodeType() != Node.ELEMENT_NODE) {
					latch.countDown();
					return;
				}

				final NamedNodeMap attributeMap = fileItem.getAttributes();
				if (attributeMap == null) {
					// there is no attribute, check next node
					latch.countDown();
					return;
				}

				final Node relativeUriNode = attributeMap.getNamedItem(ProjectFormatConstants.FOLDER_RELATIVE_LOCATION);
				final File projectDir = new File(projectUri).getParentFile();

				try {
					final File collectedFile = Path.of(projectDir.getAbsolutePath(), relativeUriNode.getNodeValue()).toFile();
					Path correctedPath;
					correctedPath = Path.of(collectedFile.getCanonicalPath());
					if (correctedPath.toFile().exists()) {
						collectedFileNames.add(correctedPath);
					}
				} catch (IOException e) {
					TitanLogger.logError(e);
				}
				latch.countDown();
			});
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdown();
		try {
			executor.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
		executor.shutdownNow();
		if (isErroneous.get()) {
			return false;
		}
		return true;
	}

	public List<Path> getFileNames() {
		return collectedFileNames;
	}
}