/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.jsonrpc.messages.Either;

/**
 * Base class for different proposal contexts
 * 
 * @author Miklos Magyari
 * @author Arpad Lovassy
 *
 */
public abstract class CompletionContext {
	protected CompletionContextInfo contextInfo;

	protected CompletionContext(final CompletionContextInfo contextInfo) {
		this.contextInfo = contextInfo;
	}

	public abstract Either<List<CompletionItem>, CompletionList> getCompletions();
}
