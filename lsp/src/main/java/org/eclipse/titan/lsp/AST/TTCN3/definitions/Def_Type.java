/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.ITypeWithComponents;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifier;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.types.Address_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Altstep_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.CompField;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.EnumItem;
import org.eclipse.titan.lsp.AST.TTCN3.types.Function_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Enumerated_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Set_Seq_Choice_BaseType;
import org.eclipse.titan.lsp.AST.TTCN3.types.Testcase_Type;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.extensionattributeparser.ExtensionAttributeAnalyzer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * The Def_Type class represents TTCN3 type definitions.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * 
 **/
public final class Def_Type extends Definition implements ICommentable {
	private final Type type;

	private NamedBridgeScope bridgeScope = null;

	public Def_Type(final Identifier identifier, final Type type) {
		this(identifier, type, null);
	}
	
	public Def_Type(final Identifier identifier, final Type type, DocumentComment documentComment) {
		super(identifier);
		this.type = type;

		if (type != null) {
			type.setFullNameParent(this);
			type.setOwnertype(TypeOwner_type.OT_TYPE_DEF, this);
		}
		
		this.documentComment = documentComment;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_TYPE;
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return "type";
	}
	
	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		return type != null ? type.getOutlineSymbolKind() : null;
	}
	
	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return type != null ? type.getOutlineRange() : null;
	}
	
	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() { 
		return type != null ? type.getOutlineSelectionRange() : null;
	}
	
	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		return type != null ? type.getOutlineSymbol() : null;
	}
	
	@Override
	/** {@inheritDoc} */
	public List<DocumentSymbol> getOutlineSymbolChildren() {
		return type != null ? type.getOutlineSymbolChildren() : super.getOutlineSymbolChildren();
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		int result = super.category();
		if (type != null) {
			result += type.category();
		}
		return result;
	}

	/**
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return the type defined by this type definition
	 * */
	@Override
	public Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return type;
	}

	@Override
	/** {@inheritDoc} */
	public Type getSetting(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return type;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		if (bridgeScope != null && bridgeScope.getParentScope() == scope) {
			return;
		}

		bridgeScope = new NamedBridgeScope();
		bridgeScope.setParentScope(scope);
		scope.addSubScope(getLocation(), bridgeScope);
		bridgeScope.setScopeMacroName(identifier.getDisplayName());

		super.setMyScope(bridgeScope);
		if (type != null) {
			type.setMyScope(bridgeScope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setWithAttributes(final MultipleWithAttributes attributes) {
		if (type == null) {
			return;
		}

		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
			type.setAttributeParentPath(withAttributesPath);
		}

		type.setWithAttributes(attributes);
	}

	@Override
	/** {@inheritDoc} */
	public void setAttributeParentPath(final WithAttributesPath parent) {
		super.setAttributeParentPath(parent);
		if (type == null) {
			return;
		}

		type.setAttributeParentPath(getAttributePath());
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;

		if (!Type_type.TYPE_ADDRESS.getName().equals(identifier.getTtcnName()) && !Type_type.TYPE_ANYTYPE.getName().equals(identifier.getTtcnName())) {
			NamingConventionHelper.checkConvention(NamingConventionElement.Type, identifier, this);
			NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());
		}

		if (type == null) {
			return;
		}

		if (Type_type.TYPE_COMPONENT.equals(type.getTypetype())) {
			((Component_Type)type).getComponentBody().setGenName(getGenName() + "_component_");
		}
		type.check(timestamp);
		type.checkConstructorName(identifier.getName());
		if (Type_type.TYPE_ADDRESS.getName().equals(identifier.getTtcnName())) {
			Address_Type.checkAddress(timestamp, type);
		}

		final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		type.checkRecursions(timestamp, chain);
		chain.release();

		if (withAttributesPath != null) {
			// type definitions don't have their own attributes,
			// but this is still valid as a fallback.
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp, type.getTypetype());
			hasImplicitOmitAttribute(timestamp);
			analyzeExtensionAttributes(timestamp, withAttributesPath);
		}

		switch (type.getTypetype()) {
		case TYPE_FUNCTION:
			((Function_Type) type).getFormalParameters().setMyDefinition(this);
			break;
		case TYPE_ALTSTEP:
			((Altstep_Type) type).getFormalParameters().setMyDefinition(this);
			break;
		case TYPE_TESTCASE:
			((Testcase_Type) type).getFormalParameters().setMyDefinition(this);
			break;
		default:
			break;
		} 
		checkDocumentComment();
	}

	/**
	 * Convert and check the encoding attributes applied to this function.
	 *
	 * @param timestamp
	 *                the timestamp of the actual build cycle.
	 * */
	public void analyzeExtensionAttributes(final CompilationTimeStamp timestamp, final WithAttributesPath withAttributesPath) {
		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);

		List<AttributeSpecification> specifications = null;
		for (final SingleWithAttribute attribute : realAttributes) {
			if (Attribute_Type.Extension_Attribute.equals(attribute.getAttributeType())) {
				final Qualifiers qualifiers = attribute.getQualifiers();
				if (qualifiers == null || qualifiers.size() == 0) {
					if (specifications == null) {
						specifications = new ArrayList<AttributeSpecification>();
					}
					specifications.add(attribute.getAttributeSpecification());
				} else {
					for (final Qualifier tempQualifier : qualifiers) {
						final ISubReference tempSubReference = tempQualifier.get(0);
						if (tempSubReference.getReferenceType() == Subreference_type.arraySubReference) {
							tempQualifier.getLocation().reportSemanticError(Qualifier.INVALID_INDEX_QUALIFIER);
						} else {
							tempQualifier.getLocation().reportSemanticError(
									MessageFormat.format(Qualifier.INVALID_FIELD_QUALIFIER, tempSubReference
											.getId().getDisplayName()));
						}
					}
				}
			}
		}

		if (specifications == null) {
			return;
		}

		final List<ExtensionAttribute> attributes = new ArrayList<ExtensionAttribute>();
		for (int i = 0; i < specifications.size(); i++) {
			final AttributeSpecification specification = specifications.get(i);
			final ExtensionAttributeAnalyzer analyzer = new ExtensionAttributeAnalyzer();
			analyzer.parse(specification);

			final List<ExtensionAttribute> temp = analyzer.getAttributes();
			if (temp != null) {
				attributes.addAll(temp);
			}
		}

		for (int i = 0; i < attributes.size(); i++) {
			final ExtensionAttribute extensionAttribute = attributes.get(i);
			switch (extensionAttribute.getAttributeType()) {
			case ANYTYPE:
			case VERSION:
			case REQUIRES:
			case TITANVERSION:
				break;
			default:
				// only extension attributes are allowed ... and
				// only because they can not be stopped earlier.
				extensionAttribute.getLocation().reportSemanticError("Extension attributes are not supported for types");
				break;
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		if (type != null) {
			return type.getProposalDescription(new StringBuilder()).toString();
		}
		return Type_type.TYPE_UNDEFINED.getName();
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		if (type == null) {
			return super.getOutlineChildren();
		}

		return type.getOutlineChildren();
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		final List<Integer> result = new ArrayList<Integer>();
		// might be extended with a subtype
		result.add(Ttcn3Lexer.LPAREN);
		// length restriction
		result.add(Ttcn3Lexer.LENGTH);
		// dimension
		result.add(Ttcn3Lexer.SQUAREOPEN);

		if (withAttributesPath == null || withAttributesPath.getAttributes() == null) {
			// might be extended with a with attribute
			result.add(Ttcn3Lexer.WITH);
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();

				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					removeBridge();
					throw new ReParseException(result);
				}
			}

			if (type != null) {
				if (enveloped) {
					type.updateSyntax(reparser, false);
					reparser.updateLocation(type.getLocation());
				} else if (reparser.envelopsDamage(type.getLocation())) {
					try {
						type.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(type.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					try {
						withAttributesPath.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(withAttributesPath.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (!enveloped) {
				removeBridge();
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());
		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	/**
	 * Removes the name bridging scope.
	 * */
	private void removeBridge() {
		if (bridgeScope != null) {
			bridgeScope.remove();
			bridgeScope = null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (type != null) {
			type.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (type != null && !type.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}

		final CompilationTimeStamp timestamp = lastTimeChecked != null ? lastTimeChecked : CompilationTimeStamp.getBaseTimestamp();
		if (timestamp == null) {
			return hoverContent;
		}

		final IType assType = getType(timestamp);
		if (assType != null) {
			final String typeRestrictions = ((Type)assType).getRestrictionDesc();
			final String typeDesc = ((Type)assType).getProposalDescription(new StringBuilder()).toString();

			hoverContent
			.addStyledText("type " + typeDesc, Ttcn3HoverContent.BOLD)
			.addText("\n\n")
			// VSCode doesn't display html content in hover tooltip
			// "<span style='color: blue;'>" + assType.getTypename() + "</span>"
			.addText(assType.getTypename());

			if (typeRestrictions != null) {
				hoverContent.addText("\n\n")
				.addStyledText("Restrictions:", Ttcn3HoverContent.ITALIC)
				.addText("\n\n")
				.addText(typeRestrictions);
			}

			hoverContent.addText("\n\n");
		}

		boolean isClass = assType.getTypetype() == Type_type.TYPE_CLASS;
		boolean isFunction = assType.getTypetype() == Type_type.TYPE_FUNCTION;

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			// TODO: maybe change to switch-case based on type.getTypetype()
		}
		if (isClass) {
			final Class_Type ct = (Class_Type)assType;
			if (ct != null) {
				ct.addClassMembers(this, hoverContent, dc);
			}
		} else if (isFunction) {
			final Function_Type ft = (Function_Type)assType;
			final FormalParameterList params = ft.getFormalParameters();
			if (params != null && params.size() > 0) {
				hoverContent.addTag("Parameters").addText("\n\n");
				for (final FormalParameter param : params) {
					String paramType = param.getFormalParamType();

					final StringBuilder sb = new StringBuilder(param.getIdentifier().getDisplayName());
					sb.append(" (").append(paramType != null ? paramType : "<?>").append(")");

					final Type argType = param.getType(timestamp);
					String argTypeName = argType != null ? argType.getTypename() : "<?>";
					if (argType instanceof Referenced_Type) {
						argTypeName = ((Referenced_Type) argType).getReference().getDisplayName();
					}
					sb.append(" ").append(argTypeName);

					hoverContent.addText("- ").addIndentedText(sb.toString(), "\n");
				}
			}
		} else if (dc != null) {
			dc.addMembersContent(hoverContent);
		} else {
			if (assType instanceof IMemberInfo) {
				final IMemberInfo mInfo = (IMemberInfo)assType;
				mInfo.addMembersContent(hoverContent);
			}
		}
		if (dc != null) {
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		} else {
			// TODO: can we provide some information?
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}
//
	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final CompilationTimeStamp timestamp = lastTimeChecked != null ? lastTimeChecked : CompilationTimeStamp.getBaseTimestamp();
		if (timestamp == null) {
			return "";
		}
		final IType assType = getType(timestamp);

		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding);
		
		switch (assType.getTypetype()) {
		case TYPE_CLASS:
			final Class_Type ct = (Class_Type)assType;
			for (final Definition def : ct.getDefinitions()) {
				boolean functionOverride = false;
				if (def instanceof Def_Function) {
					if (((Def_Function)def).isOverride()) {
						functionOverride = true;
					}
				}
				if (! def.isInherited() || functionOverride) {
					sb.append(ind).append(ICommentable.MEMBER_TAG).append(' ')
						.append(def.getIdentifier().getDisplayName())
						.append('\n');
				}
			}
			break;
		case TYPE_TTCN3_CHOICE:
		case TYPE_TTCN3_SEQUENCE:
			final TTCN3_Set_Seq_Choice_BaseType choice = (TTCN3_Set_Seq_Choice_BaseType)assType;
			for (int i = 0; i < choice.getNofComponents(); i++) {
				sb.append(ind).append(ICommentable.MEMBER_TAG).append(' ')
					.append(choice.getComponentByIndex(i).getIdentifier().getDisplayName())
					.append('\n');
			}
			break;
		case TYPE_COMPONENT:
			final Component_Type component = (Component_Type)assType;
			for (final Definition def : component.getComponentBody().getDefinitions()) {
				sb.append(ind).append(ICommentable.MEMBER_TAG).append(' ')
					.append(def.getIdentifier().getDisplayName())
					.append('\n');
			}
			break;
		case TYPE_TTCN3_ENUMERATED:
			final TTCN3_Enumerated_Type enumerated = (TTCN3_Enumerated_Type)assType;
			for (final EnumItem item : enumerated.getEnumItemsOrdered()) {
				sb.append(ind).append(ICommentable.MEMBER_TAG).append(' ')
					.append(item.getIdentifier().getDisplayName())
					.append('\n');
			}
			break;
		default:
			break;
		}
		
		sb.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	@Override
	public void setSemanticInformation() {
		final Location idLocation = identifier.getLocation();
		if (!AstSemanticHighlighting.hasSemanticToken(idLocation)) {
			AstSemanticHighlighting.addSemanticToken(idLocation, SemanticType.Type);
		}
		super.setSemanticInformation();
	}
	
	@Override
	public void checkDocumentComment() {
		if (!hasDocumentComment()) {
			return;
		}
		
		final CompilationTimeStamp timestamp = lastTimeChecked != null ? lastTimeChecked : CompilationTimeStamp.getBaseTimestamp();
		if (timestamp == null) {
			return;
		}
		final Type nodeType = getType(timestamp); 
		final Map<String,String> members = documentComment.getMembers();
		if (members != null && !members.isEmpty()) {
			if (nodeType instanceof ITypeWithComponents) {
				final ITypeWithComponents typeWithComps = (ITypeWithComponents)getType(timestamp);
				final Map<String,Location> memberLocations = documentComment.getMemberLocations();
				members.entrySet().stream().forEach(member -> {
					final String memberName = member.getKey();
					if (typeWithComps.getComponentIdentifierByName(new Identifier(Identifier_type.ID_TTCN, memberName)) == null) {
						memberLocations.get(memberName).reportSemanticWarning(
							MessageFormat.format("This type has no member named `{0}''", memberName));
					}
				});
			}
			if (nodeType instanceof TTCN3_Set_Seq_Choice_BaseType) {
				/**
				 * Report undocumented members.
				 * On purpose, this is only checked if at least one @member tag exists already. This is to avoid
				 * warnings if there is no intention to document the members. 
				 */
				final TTCN3_Set_Seq_Choice_BaseType base = (TTCN3_Set_Seq_Choice_BaseType)nodeType;
				for (int i = 0; i < base.getNofComponents(); i++) {
					final CompField field = base.getComponentByIndex(i);
					if (!members.containsKey(field.getIdentifier().getDisplayName())) {
						field.getIdentifier().getLocation().reportSemanticWarning("Member has no related `@member` comment tag");
					}
				}
			}
		}
		
		super.checkDocumentComment();
	}
}
