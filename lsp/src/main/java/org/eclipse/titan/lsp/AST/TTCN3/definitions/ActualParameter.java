/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * class to represent an actual parameter.
 *
 * @author Kristof Szabados
 * */
public abstract class ActualParameter extends ASTNode implements ILocateableNode, IIncrementallyUpdatable, ICancelBuild {
	private boolean isErroneous = false;

	/**
	 * The location of the whole actual parameter. This location encloses
	 * the parameter fully, as it is used to report errors to.
	 **/
	private Location location = NULL_Location.INSTANCE;

	public final void setIsErroneous() {
		isErroneous = true;
	}

	public final boolean getIsErroneous() {
		return isErroneous;
	}

	@Override
	/** {@inheritDoc} */
	public final void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getLocation() {
		return location;
	}

	/**
	 * Checks for circular references within the actual parameter.
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 * @param referenceChain
	 *                the ReferenceChain used to detect circular references,
	 *                must not be null.
	 **/
	public abstract void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain);
}
