/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class CompletionContextFactory {

	// Regex fragments (used by other regexes)
	private static final String IDENTIFIER = "([A-Za-z][A-Za-z0-9_]*)";
	private static final String IDENTIFIERWITHDOT = "([A-Za-z][A-Za-z0-9_.]*)";
	private static final String ASSIGNOP = ":=";
	private static final String ATTRIBUTES = "([^;]+;)*";
	private static final String ATTRIBUTE_KEYWORD = "(encode|display|extension|variant|optional)";
	private static final String ATTRIBUTE_MODIFIER = "(override|\\@local)";
	private static final String ENCODE_DECODE_TYPE = "(BER|PER|XER|RAW|TEXT|JSON|OER)";
	private static final String DEFINITION = "\\s+" + IDENTIFIER + "\\s+" + IDENTIFIER + "\\s*" + ASSIGNOP;
	private static final String OPT_PARAMETER = "\\s*(\\([^)]*\\))?";
	// 1 group
	private static final String WITH_ATTRIBUTE_START = "with\\s*\\{\\s*" + ATTRIBUTES + "\\s*";
	// matches only to simple cases, because syntax of extension attribute can be very different
	// 3 groups
	private static final String EXTENSION_ATTRIBUTE =
									"((prototype|printing)\\s*\\(\\s*[a-z]+\\s*\\)" +
									"|transparent|internal|address|provider|done" +
									"|(encode|decode)\\s*\\(\\s*[A-Z]+\\s*(\\:\\s*[A-Z_]+\\s*)?\\)" +
									")";
	// 4 groups
	private static final String EXTENSION_ATTRIBUTES = "(" + EXTENSION_ATTRIBUTE + "\\s+)*";
	// 6 groups
	private static final String WITH_ATTRIBUTE_EXTENSION_START = WITH_ATTRIBUTE_START + "extension\\s*" +
									ATTRIBUTE_MODIFIER + "?\\s*\\\"\\s*" + EXTENSION_ATTRIBUTES;
	// 2 groups
	private static final String WITH_ATTRIBUTE_VARIANT_START = WITH_ATTRIBUTE_START + "variant\\s*" +
									ATTRIBUTE_MODIFIER + "?\\s*\\\"\\s*";

	// Regexes of the different proposal context cases
	private static final String ASSIGNMENT = "(var|const)" + DEFINITION + "\\s*(" + IDENTIFIER + ")?$";
	private static final String SINGLEASSIGNMENT = "()()\\s*" + IDENTIFIERWITHDOT + "\\s*" + ASSIGNOP + "\\s*(" + IDENTIFIERWITHDOT + ")?$";
	private static final String FRIEND = "friend[\\s]+module[\\s]*(.*)";
	private static final String IMPORT = "import[\\s]+from([\\s]*$|[\\s+]+(" + IDENTIFIER + "))";
	private static final String COMPONENT = "(system|mtc|runs[\\s]+on)((\\s+" + IDENTIFIER + "$)|\\s*)";
	private static final String PARAMETER = "\\s*\\(([^)]*$)";
	private static final String SETVERDICT = "setverdict" + PARAMETER;
	private static final String EXECUTE = "execute" + PARAMETER;
	private static final String FUNCTION_MODIFIER = "function\\s+\\@" + IDENTIFIER + "?$";
	private static final String FUNCTION_PARAM_MODIFIER = "function\\s+(\\@([a-z]+)\\s+)?" + IDENTIFIER +
														"\\s*\\(\\s*([^,]*,\\s*)*\\@" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_PROTOTYPE = WITH_ATTRIBUTE_EXTENSION_START +
									"prototype\\s*\\(\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_PRINTING = WITH_ATTRIBUTE_EXTENSION_START +
									"printing\\s*\\(\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_ENCODE_DECODE = WITH_ATTRIBUTE_EXTENSION_START +
									"(encode|decode)\\s*\\(\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_ENCODE_DECODE_OPTION = WITH_ATTRIBUTE_EXTENSION_START +
									"(encode|decode)\\s*\\(\\s*" + ENCODE_DECODE_TYPE + "\\s*\\:\\s*" + IDENTIFIER + "?$";
	// 4 groups
	private static final String WITH_ATTRIBUTE_VARIANT_OPTION = WITH_ATTRIBUTE_VARIANT_START + IDENTIFIER +
									"\\s*\\(\\s*" + IDENTIFIER +"?$";
	// 4 groups
	private static final String WITH_ATTRIBUTE_VARIANT_COLON = WITH_ATTRIBUTE_VARIANT_START + IDENTIFIER +
									"\\s*\\:\\s*" + IDENTIFIER +"?$";
	private static final String FUNCTION_PARAM = "(" + IDENTIFIER + OPT_PARAMETER + "\\s*\\.)+\\s*"
												+ IDENTIFIER + "\\s*\\(\\s*([^,]*,\\s*)*" + IDENTIFIER + "?$";
	private static final String PARAMETERIZED = IDENTIFIER + PARAMETER;
	private static final String CLASS_MODIFIER = "type\\s+class\\s+\\@" + IDENTIFIER + "?$";
	private static final String CLASS_EXTENDS = "type\\s+class\\s+(\\@([a-z]+)\\s+)?" + IDENTIFIER + "\\s+extends\\s+(" + IDENTIFIER + "(\\s*,\\s*" + IDENTIFIER + ")*)?";
	private static final String ALL_ANY = "(all|any)\\s*(component|port|timer)\\s*\\.\\s*" + IDENTIFIER + "?$";
	private static final String DOT_NOTATION_CHAIN = "((" + IDENTIFIER + OPT_PARAMETER + "\\s*\\.)*\\s*"
												+ IDENTIFIER + OPT_PARAMETER + ")\\s*\\.\\s*"
												+ IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_ENCODE_TYPE = WITH_ATTRIBUTE_START + "encode\\s*" + ATTRIBUTE_MODIFIER +
									"?\\s*\\\"" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION = WITH_ATTRIBUTE_EXTENSION_START + IDENTIFIER + "?$";
	// 3 groups
	//TODO: variant attribute van start with number, so change IDENTIFIER to something more general
	private static final String WITH_ATTRIBUTE_VARIANT = WITH_ATTRIBUTE_VARIANT_START + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_KEYWORD = "with\\s*\\{\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_MODIFIER = WITH_ATTRIBUTE_START + ATTRIBUTE_KEYWORD + "\\s*\\@?" +
									IDENTIFIER + "?$";

	private static LinkedHashMap<Pattern, Function<CompletionContextInfo, CompletionContext>> contexts = new LinkedHashMap<>();
	static {
	    contexts.put(Pattern.compile(ASSIGNMENT), contextInfo -> new AssignmentContext(contextInfo));
	    contexts.put(Pattern.compile(SINGLEASSIGNMENT), contextInfo -> new AssignmentContext(contextInfo));
	    contexts.put(Pattern.compile(IMPORT), contextInfo -> new ImportContext(contextInfo));
	    contexts.put(Pattern.compile(COMPONENT), contextInfo -> new ComponentContext(contextInfo));
	    contexts.put(Pattern.compile(FRIEND), contextInfo -> new FriendModuleContext(contextInfo));
	}

	private CompletionContextFactory() {
		// Hide constructor
	}

	public static CompletionContext getContext(CompletionContextInfo contextInfo) {
		if (contextInfo.cleanedContext == null) {
			return null;
		}

		for (Map.Entry<Pattern, Function<CompletionContextInfo, CompletionContext>> entry : contexts.entrySet()) {
			final Matcher matcher = entry.getKey().matcher(contextInfo.cleanedContext);
			if (matcher.find()) {
				contextInfo.matcher = matcher;
				return entry.getValue().apply(contextInfo);
			}
		}

		return null;
	}
}
