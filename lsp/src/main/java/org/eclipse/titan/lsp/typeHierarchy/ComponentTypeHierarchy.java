/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.typeHierarchy;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.TypeHierarchyItem;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.TTCN3.types.ComponentTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.ComponentTypeReferenceList;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;

public class ComponentTypeHierarchy {
	private static final SymbolKind kind = SymbolKind.Function;

	private Component_Type component;
	private List<Reference> referenceList;

	public ComponentTypeHierarchy(final Component_Type component) {
		this.component = Objects.requireNonNull(component);
	}

	public ComponentTypeHierarchy(final List<Reference> referenceList) {
		this.referenceList = referenceList;
	}

	private TypeHierarchyItem createTypeHierarchyItem(ComponentTypeBody componentBody) {
		final String name = componentBody.getIdentifier().getDisplayName();
		final String uri = componentBody.getLocation().getFile().toPath().toUri().toString();
		final Range range = componentBody.getLocation().getRange();
		return new TypeHierarchyItem(name, kind, uri, range, range);
	}

	public List<TypeHierarchyItem> getSuperTypes() {
		final List<ComponentTypeBody> parentComponents = component.getComponentBody().getExtensions().getComponentBodies();
		if (parentComponents == null) {
			return null;
		}
		return parentComponents
				.stream()
				.map(parent -> createTypeHierarchyItem(parent))
				.collect(Collectors.toList());
	}

	public List<TypeHierarchyItem> getSubTypes() {
		if (referenceList == null || referenceList.isEmpty()) {
			return null;
		}
		return referenceList
				.stream()
				.filter(ref -> ref.getNameParent() instanceof ComponentTypeReferenceList)
				.map(ref -> createTypeHierarchyItem((ComponentTypeBody) ref.getNameParent().getNameParent()))
				.collect(Collectors.toList());
	}
}
