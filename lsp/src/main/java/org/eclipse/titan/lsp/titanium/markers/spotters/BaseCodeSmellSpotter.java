/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.utils.LSPUtils;
import org.eclipse.titan.lsp.titanium.markers.handlers.Marker;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * Abstract base class of code smell spotters.
 * <p>
 * Code smell spotters are used to analyze a ttcn3 module and project. They
 * extend either {@link BaseModuleCodeSmellSpotter} or
 * {@link BaseProjectCodeSmellSpotter}, both of which are descendants of this
 * class.
 * <p>
 * Their general purpose is to analyze a module or a project searching for
 * patterns of a specific problem, and when found, report it, that is, create a
 * {@link Marker}, which later can be used for example to show up in eclipse.
 * <p>
 * This class extracts what is common in every spotter:
 * <ul>
 * <li>can report a single code smell type</li>
 * <li>during the analysis collects multiple reports</li>
 * <li>can be initialized with some spotter-specific data</li>
 * </ul>
 * Code smell spotters should be immutable; any required user preference setting
 * should be read at construction time.
 *
 * @author poroszd
 *
 */
abstract class BaseCodeSmellSpotter {
	/** The code smell that this spotter can spot */
	private final CodeSmellType type;
	/** The severity of the code smell to spot */
	

	protected BaseCodeSmellSpotter(final CodeSmellType type) {
		this.type = type;
	}

	/**
	 * A utility class to make code smell reporting in the spotter subclasses
	 * more straightforward.
	 *
	 * @author poroszd
	 * @author Miklos Magyari
	 *
	 */
	protected class Problems {
		private String severity;
		private final List<Marker> reports = new LinkedList<Marker>();

		/**
		 * Create a problem marker at a specific location.
		 *
		 * @param loc
		 *            The location to marker
		 * @param message
		 *            the message of the marker
		 */
		public Problems(final String severity) {
			this.severity = severity;
		}
		
		public Problems() {
			this(Configuration.INSTANCE.getString(Configuration.NAMING_CONVENTION_SEVERITY, "warning"));
		}
		
		/**
		 * Adds a simple marker containing the message only.
		 * @param loc
		 * @param message
		 * @return
		 */
		public Marker report(final Location loc, final String message) {
			return report(loc, message, null, null, null);
		}
		
		/**
		 * Adds a marker containing a quick fix. 
		 * @param loc
		 * @param message
		 * @param actionTypes
		 * @param providerType
		 * @param data
		 * @return
		 */
		public <E extends Enum<E>> Marker report(final Location loc, final String message,
			List<E> actionTypes, CodeActionProviderType providerType, String data) {
			for (final Marker report : reports) {
				if (report.getLocation().equals(loc) && report.getMessage().equals(message)) {
					return report;
				}
			}
			final DiagnosticSeverity diagSeverity = LSPUtils.getDiagnosticSeverity(severity, DiagnosticSeverity.Warning);
			Marker marker;
			if (actionTypes == null) {
				marker = new Marker(loc, message, diagSeverity, type);
			} else {
				marker = new Marker(loc, message, diagSeverity, type, actionTypes, providerType, data);
			}
			reports.add(marker);
			
			return marker; 
		}

		public List<Marker> getMarkers() {
			return reports;
		}
	}
}

