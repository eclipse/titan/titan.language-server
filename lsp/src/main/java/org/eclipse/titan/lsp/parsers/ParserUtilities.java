/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.File;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.titan.lsp.common.logging.ParserLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.parsers.asn1parser.Asn1Parser;
import org.eclipse.titan.lsp.parsers.cfg.CfgParser;
import org.eclipse.titan.lsp.parsers.extensionattributeparser.ExtensionAttributeParser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.PreprocessorDirectiveParser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Parser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser;

/**
 * Utility functions for Parser classes.<br>
 * Features:
 * <ul>
 * <li> logging
 * <li> calculating hidden tokens (comments)
 * </ul>
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public final class ParserUtilities {
	private ParserUtilities() {
		// Hide constructor
	}

	/**
	 * Logs a parse tree.
	 * @param aRoot parse tree
	 * @param aParser parser to get rule names
	 */
	public static void logParseTree( final ParseTree aRoot,
									 final Parser aParser ) {
		if (! isParseTreeLogged()) {
			return;
		}
		final String description = getDescription( aParser );
		ParserLogger.log( aRoot, aParser, description );
	}

	public static final boolean isParseTreeLogged() {
		return Configuration.INSTANCE.getBoolean(Configuration.LOG_PARSE_TREE, false);
	}

	/**
	 * Sets Parser._buildParseTree according to TITAN Preferences: true, if parse tree logging is switched on
	 * @param aParser parser
	 */
	public static final void setBuildParseTree( final Parser aParser ) {
		aParser.setBuildParseTree( isParseTreeLogged() );
	}

	/**
	 * @param aParser parser instance
	 * @return gets the description for the parse tree logging header
	 */
	private static String getDescription( final Parser aParser ) {
		if ( aParser instanceof Ttcn3Parser ) {
			return "TTCN-3";
		} else if ( aParser instanceof Ttcn3Reparser ) {
			return "TTCN-3 incremental parsing";
		} else if ( aParser instanceof Asn1Parser ) {
			return "ASN.1";
		} else if ( aParser instanceof ExtensionAttributeParser ) {
			return "Extension Attribute";
		} else if ( aParser instanceof PreprocessorDirectiveParser ) {
			return "Preprocessor Directive";
		} else if ( aParser instanceof CfgParser ) {
			return "CFG";
		}
		return null;
	}

	public static int calculateOffset(final File file, final Position start) {
		final ProjectItem item = Project.INSTANCE.getProjectItem(file.toPath());
		if (item == null) {
			return -1;
		}
		final String code = item.getSource();
		final String lineSeparator = code.contains("\r\n") ? "\r\n" : "\n";
		final String[] lines = code.split(lineSeparator);
		int offset = 0;
		for (int i = 0; i < lines.length && i < start.getLine() - 1; i++) {
			offset += lines[i].length() + System.lineSeparator().length();
		}
		offset += start.getCharacter();
		return offset;
	}
}
