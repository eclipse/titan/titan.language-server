/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.List;

import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.Interval;

/**
 * An interface for source code analyzers.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public interface ISourceAnalyzer {

	/** @return the errors from ANTLR 4 lexer and parser, never returns {@code null} */
	List<SyntacticErrorStorage> getErrorStorage();

	/**
	 * @return the list of markers created for the parse time found
	 *         unsupported features and bad practices, never returns {@code null}
	 */
	List<TITANMarker> getWarnings();

	/**
	 * @return the list of markers created for the parse time found
	 *         unsupported features, never returns {@code null}
	 */
	List<TITANMarker> getUnsupportedConstructs();

	/** @return the module that was created from the parsed source code */
	Module getModule();

	/** @return the root interval of the interval created for the source code */
	Interval getRootInterval();

	/**
	 * Parses the provided elements. If the contents of an editor are to be
	 * parsed, than the file parameter is only used to report the errors to.
	 *
	 * @param file the file to parse, and report the errors to.
	 * @param code the contents of an editor, or {@code null}.
	 * @exception FileNotFoundException if this method fails, the file was not found.
	 * */
	void parse(Path file, String code) throws FileNotFoundException;
}
