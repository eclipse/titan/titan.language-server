/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.BridgingNamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferencingType;
import org.eclipse.titan.lsp.AST.ISetting;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo;
import org.eclipse.titan.lsp.AST.ASN1.IASN1Type;
import org.eclipse.titan.lsp.AST.ASN1.Type_Assignment;
import org.eclipse.titan.lsp.AST.ASN1.Undefined_Assignment;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.JsonAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ExpressionStruct;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class Referenced_Type extends Type implements IASN1Type, IReferencingType {

	private final Reference reference;
	private IType referenced;
	private IType referencedLast;

	private boolean componentInternal;

	public Referenced_Type(final Reference reference) {
		this.reference = reference;
		componentInternal = false;

		if (reference != null) {
			reference.setFullNameParent(this);
			setLocation(reference.getLocation());
			setMyScope(reference.getMyScope());
		}
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetype() {
		return Type_type.TYPE_REFERENCED;
	}

	public Reference getReference() {
		return reference;
	}

	@Override
	// Location is optimized not to store an object that it is not needed
	public Location getLocation() {
		if (reference != null && reference.getLocation() != null) {
			return new Location(reference.getLocation());
		}

		return NULL_Location.INSTANCE;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		//Do nothing
	}

	@Override
	/** {@inheritDoc} */
	public IASN1Type newInstance() {
		return new Referenced_Type(reference);
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (reference != null) {
			reference.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public String chainedDescription() {
		return "type reference: " + reference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isCompatible(final CompilationTimeStamp timestamp, final IType otherType, final TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain) {
		check(timestamp);
		otherType.check(timestamp);
		final IType t1 = getTypeRefdLast(timestamp);
		final IType t2 = otherType.getTypeRefdLast(timestamp);

		if (t1.getIsErroneous(timestamp) || t2.getIsErroneous(timestamp)) {
			return true;
		}

		return t1.isCompatible(timestamp, t2, info, null, null);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isCompatibleByPort(final CompilationTimeStamp timestamp, final IType otherType) {
		super.isCompatibleByPort(timestamp, otherType);

		final IType t1 = getTypeRefdLast(timestamp);
		final IType t2 = otherType.getTypeRefdLast(timestamp);

		if (t1.getIsErroneous(timestamp) || t2.getIsErroneous(timestamp)) {
			return true;
		}

		return t1.isCompatibleByPort(timestamp, otherType);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isIdentical(final CompilationTimeStamp timestamp, final IType type) {
		check(timestamp);
		type.check(timestamp);
		final IType t1 = getTypeRefdLast(timestamp);
		final IType t2 = type.getTypeRefdLast(timestamp);

		if (t1.getIsErroneous(timestamp) || t2.getIsErroneous(timestamp)) {
			return true;
		}

		return t1.isIdentical(timestamp, t2);
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetypeTtcn3() {
		if (isErroneous) {
			return Type_type.TYPE_UNDEFINED;
		}

		return getTypetype();
	}

	@Override
	/** {@inheritDoc} */
	public String getTypename() {
		if (isErroneous || referencedLast == null || referencedLast == this) {
			return getTypetype().getName();
		}

		return referencedLast.getTypename();//TODO maybe this should be the name of the current type.
	}

	@Override
	/** {@inheritDoc} */
	public IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final IReferenceChain refChain, final boolean interruptIfOptional) {

		check(timestamp);
		if (reference.getSubreferences().size() <= actualSubReference) {
			return this;
		}

		if (referencedLast != null && this != referencedLast) {
			final Expected_Value_type internalExpectation =
					expectedIndex == Expected_Value_type.EXPECTED_TEMPLATE ? Expected_Value_type.EXPECTED_DYNAMIC_VALUE : expectedIndex;
			final IType temp = referencedLast.getFieldType(timestamp, reference, actualSubReference, internalExpectation, refChain, interruptIfOptional);
			if (reference.getIsErroneous(timestamp)) {
				setIsErroneous(true);
			}
			return temp;
		}

		return this;
	}

	@Override
	/** {@inheritDoc} */
	public boolean getSubrefsAsArray(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final List<Integer> subrefsArray, final List<IType> typeArray) {
		if (reference.getSubreferences().size() <= actualSubReference) {
			return true;
		}

		if (this == referencedLast) {
			return false;
		}

		return referencedLast.getSubrefsAsArray(timestamp, reference, actualSubReference, subrefsArray, typeArray);
	}

	@Override
	/** {@inheritDoc} */
	public boolean getFieldTypesAsArray(final Reference reference, final int actualSubReference, final List<IType> typeArray) {
		if (reference.getSubreferences().size() <= actualSubReference) {
			return true;
		}

		if (this == referencedLast || referencedLast == null) {
			return false;
		}

		return referencedLast.getFieldTypesAsArray(reference, actualSubReference, typeArray);
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getProposalDescription(final StringBuilder builder) {
		Assignment ass;
		if (lastTimeChecked == null) {
			ass = reference.getRefdAssignment(CompilationTimeStamp.getBaseTimestamp(), true);
		} else {
			ass = reference.getRefdAssignment(lastTimeChecked, true);
		}

		if (ass != null && Assignment_type.A_TYPE.semanticallyEquals(ass.getAssignmentType())) {
			if (ass instanceof Def_Type) {
				final Def_Type defType = (Def_Type) ass;
				return builder.append(defType.getIdentifier().getDisplayName());
			} else if (ass instanceof Type_Assignment) {
				return builder.append(((Type_Assignment) ass).getIdentifier().getDisplayName());
			}
		}

		return builder.append(UNKNOWN_REFERRED_TYPE);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isComponentInternal(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return componentInternal;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;
		componentInternal = false;
		isErroneous = false;
		referenced = null;
		referencedLast = null; //Do not remove! Intentionally set for null to avoid checking circle.

		initAttributes(timestamp);

		referencedLast = getTypeRefdLast(timestamp);

		if (referencedLast != null && !referencedLast.getIsErroneous(timestamp)) {
			referencedLast.check(timestamp);
			componentInternal = referencedLast.isComponentInternal(timestamp);
		}

		if (constraints != null) {
			constraints.check(timestamp);
		}

		final IReferenceChain tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final IType typeParent = getTypeRefd(timestamp, tempReferenceChain);
		tempReferenceChain.release();
		if (!referencedLast.getIsErroneous(timestamp) && !typeParent.getIsErroneous(timestamp)) {
			checkSubtypeRestrictions(timestamp, referencedLast.getSubtypeType(), typeParent.getSubtype());
		}

		if (myScope != null) {
			checkEncode(timestamp);
			checkVariants(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkComponentInternal(final CompilationTimeStamp timestamp, final Set<IType> typeSet, final String operation) {
		if (isBuildCancelled()) {
			return;
		}

		final IType last = getTypeRefdLast(timestamp);

		if (last != null && !last.getIsErroneous(timestamp) && last != this) {
			last.checkComponentInternal(timestamp, typeSet, operation);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkEmbedded(final CompilationTimeStamp timestamp, final Location errorLocation, final boolean defaultAllowed,
			final String errorMessage) {
		if (isBuildCancelled()) {
			return;
		}

		final IType last = getTypeRefdLast(timestamp);

		if (last != null && !last.getIsErroneous(timestamp) && last != this) {
			last.checkEmbedded(timestamp, errorLocation, defaultAllowed, errorMessage);
		}
	}

	@Override
	/** {@inheritDoc} */
	public IValue checkThisValueRef(final CompilationTimeStamp timestamp, final IValue value) {
		if (Value_type.UNDEFINED_LOWERIDENTIFIER_VALUE.equals(value.getValuetype())) {
			final IReferenceChain tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			final IType refd = getTypeRefd(timestamp, tempReferenceChain);
			tempReferenceChain.release();

			if (this.equals(refd)) {
				return value;
			}

			return refd.checkThisValueRef(timestamp, value);
		}

		return value;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisValue(final CompilationTimeStamp timestamp, final IValue value, final Assignment lhs, final ValueCheckingOptions valueCheckingOptions) {
		if (getIsErroneous(timestamp)) {
			return false;
		}

		boolean selfReference = false;
		final IType tempType = getTypeRefdLast(timestamp);
		if (tempType != this) {
			selfReference = tempType.checkThisValue(timestamp, value, lhs, new ValueCheckingOptions(valueCheckingOptions.expected_value,
					valueCheckingOptions.incomplete_allowed, valueCheckingOptions.omit_allowed, false, valueCheckingOptions.implicit_omit,
					valueCheckingOptions.str_elem, valueCheckingOptions.from_subtype));
			final Definition def = value.getDefiningAssignment();
			if (def != null) {
				final String referingModuleName = getMyScope().getModuleScope().getName();
				if (!def.referingHere.contains(referingModuleName)) {
					def.referingHere.add(referingModuleName);
				}
			}
		}

		if (valueCheckingOptions.sub_check && subType != null) {
			subType.checkThisValue(timestamp, value);
		}

		value.setLastTimeChecked(timestamp);

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplate(final CompilationTimeStamp timestamp, final ITTCN3Template template, final boolean isModified,
			final boolean implicitOmit, final Assignment lhs) {
		if (getIsErroneous(timestamp)) {
			return false;
		}

		registerUsage(template);
		boolean selfReference = false;
		final IType tempType = getTypeRefdLast(timestamp);
		if (tempType != this) {
			selfReference = tempType.checkThisTemplate(timestamp, template, isModified, implicitOmit, lhs);
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public IType getTypeRefd(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (refChain.add(this) && reference != null && !getIsErroneous(timestamp)) {
			if (referenced != null) {
				return referenced;
			}

			Assignment ass = reference.getRefdAssignment(timestamp, true, refChain);

			if (ass != null && Assignment_type.A_UNDEF.semanticallyEquals(ass.getAssignmentType())) {
				ass = ((Undefined_Assignment) ass).getRealAssignment(timestamp);
			}

			if (ass == null || ass.getIsErroneous()) {
				// The referenced assignment was not found, or is erroneous
				isErroneous = true;
				lastTimeChecked = timestamp;
				return this;
			}

			switch (ass.getAssignmentType()) {
			case A_TYPE: {
				IType tempType = ass.getType(timestamp);
				if (tempType != null) {
					if (!tempType.getIsErroneous(timestamp)) {
						tempType.check(timestamp);
						tempType = tempType.getFieldType(timestamp, reference, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, refChain, false);
						if (tempType == null) {
							isErroneous = true;
							return this;
						}

						referenced = tempType;
						return referenced;
					}
				}
				break;
			}
			case A_VS: {
				final IType tempType = ass.getType(timestamp);
				if (tempType == null) {
					isErroneous = true;
					lastTimeChecked = timestamp;
					return this;
				}

				referenced = tempType;
				return referenced;
			}
			case A_OC:
			case A_OBJECT:
			case A_OS:
				final ISetting setting = reference.getRefdSetting(timestamp);
				if (setting == null || setting.getIsErroneous(timestamp)) {
					isErroneous = true;
					lastTimeChecked = timestamp;
					return this;
				}

				if (!Setting_type.S_T.equals(setting.getSettingtype())) {
					reference.getLocation().reportSemanticError(MessageFormat.format(TYPEREFERENCEEXPECTED, reference.getDisplayName()));
					isErroneous = true;
					lastTimeChecked = timestamp;
					return this;
				}

				referenced = (Type) setting;

				if (referenced.getOwnertype() == TypeOwner_type.OT_UNKNOWN) {
					referenced.setOwnertype(TypeOwner_type.OT_REF, this);
				}

				if (referenced.getMyScope() != null) {
					// opentype or OCFT
					referenced.setMyScope(getMyScope());
					referenced.setParentType(getParentType());
//					referenced.setGenName(getGenNameOwn(), "type");
					referenced.setFullNameParent(new BridgingNamedNode(this, ".type"));
				}
				//FIXME add warning
				return referenced;
			default:
				reference.getLocation().reportSemanticError(MessageFormat.format(TYPEREFERENCEEXPECTED, reference.getDisplayName()));
				break;
			}
		}

		isErroneous = true;
		lastTimeChecked = timestamp;
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public IType getTypeRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		final boolean newChain = null == referenceChain;
		IReferenceChain tempReferenceChain;
		if (newChain) {
			tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		} else {
			tempReferenceChain = referenceChain;
		}

		IType t = this;
		while (t instanceof IReferencingType && !t.getIsErroneous(timestamp)) {
			t = ((IReferencingType) t).getTypeRefd(timestamp, tempReferenceChain);
		}

		if (newChain) {
			tempReferenceChain.release();
		}

		if (t != null && t.getIsErroneous(timestamp)) {
			setIsErroneous(true);
		}

		return t;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}

		if (referenceChain.add(this)) {
			final IReferenceChain tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			final IType t = getTypeRefd(timestamp, tempReferenceChain);
			tempReferenceChain.release();

			if (!t.getIsErroneous(timestamp) && !this.equals(t)) {
				t.checkRecursions(timestamp, referenceChain);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkCodingAttributes(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}

		if (referencedLast == null || referencedLast.getIsErroneous(CompilationTimeStamp.getBaseTimestamp()) || referencedLast == this) {
			return;
		}

		//check raw attributes
		if (subType != null) {
			final int restrictionLength = subType.get_length_restriction();
			if (restrictionLength != -1) {
				if (rawAttribute == null) {
					rawAttribute = new RawAST(getDefaultRawFieldLength());
				}
				if (rawAttribute.fieldlength == 0) {
					rawAttribute.fieldlength = restrictionLength;
					rawAttribute.length_restriction = -1;
				} else {
					rawAttribute.length_restriction = restrictionLength;
				}
			}
		}

		if (rawAttribute != null) {

			referenced.forceRaw(timestamp);
			if (rawAttribute.fieldlength == 0 && rawAttribute.length_restriction != -1) {
				switch (referencedLast.getTypetype()) {
				case TYPE_BITSTRING:
					rawAttribute.fieldlength = rawAttribute.length_restriction;
					rawAttribute.length_restriction = -1;
					break;
				case TYPE_HEXSTRING:
					rawAttribute.fieldlength = rawAttribute.length_restriction * 4;
					rawAttribute.length_restriction = -1;
					break;
				case TYPE_OCTETSTRING:
					rawAttribute.fieldlength = rawAttribute.length_restriction * 8;
					rawAttribute.length_restriction = -1;
					break;
				case TYPE_CHARSTRING:
				case TYPE_UCHARSTRING:
					rawAttribute.fieldlength = rawAttribute.length_restriction * 8;
					rawAttribute.length_restriction = -1;
					break;
				case TYPE_SEQUENCE_OF:
				case TYPE_SET_OF:
					rawAttribute.fieldlength = rawAttribute.length_restriction;
					rawAttribute.length_restriction = -1;
					break;
				default:
					break;
				}
			}
		}

		if (referenced.getJsonAttribute() == null) {
			referenced.forceJson(timestamp);
		}
		checkJson(timestamp);

		//TODO add checks for other encodings.

		if (refChain.contains(this)) {
			return;
		}

		referencedLast.checkCodingAttributes(timestamp, refChain);
	}

	@Override
	public void checkJson(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (jsonAttribute == null || !hasEncodeAttribute(MessageEncoding_type.JSON)) {
			return;
		}

		if (jsonAttribute.omit_as_null && !isOptionalField()) {
			getLocation().reportSemanticError("Invalid attribute, 'omit as null' requires optional field of a record or set.");
		}

		if (jsonAttribute.as_value) {
			switch(referencedLast.getTypetype()) {
			case TYPE_TTCN3_CHOICE:
			case TYPE_ANYTYPE:
				break; // OK
			case TYPE_TTCN3_SEQUENCE:
			case TYPE_TTCN3_SET:
				if (((TTCN3_Set_Seq_Choice_BaseType)referencedLast).getNofComponents() == 1) {
					break; // OK
				}
				// fallthrough
			default:
				getLocation().reportSemanticError("Invalid attribute, 'as value' is only allowed for unions, the anytype, or records or sets with one field");
				break;
			}
		}

		if (jsonAttribute.alias != null) {
			final IType parent = getParentType();
			if (parent == null) {
				// only report this error when using the new codec handling, otherwise
				// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
				getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
			} else {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					break;
				default:
					// only report this error when using the new codec handling, otherwise
					// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
					getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
					break;
				}
			}

			if (parent != null && parent.getJsonAttribute() != null && parent.getJsonAttribute().as_value) {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					// parent_type_name remains null if the 'as value' attribute is set for an invalid type
					getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					break;
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
					if (((TTCN3_Set_Seq_Choice_BaseType)parent).getNofComponents() == 1) {
						// parent_type_name remains null if the 'as value' attribute is set for an invalid type
						getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					}
					break;
				default:
					break;
				}
			}
		}

		if (jsonAttribute.parsed_default_value != null) {
			checkJsonDefault(timestamp);
		}

		//TODO: check schema extensions 

		if (jsonAttribute.metainfo_unbound) {
			if (referencedLast.getTypetype() == Type_type.TYPE_TTCN3_SEQUENCE || 
					referencedLast.getTypetype() == Type_type.TYPE_TTCN3_SET) {
				final TTCN3_Set_Seq_Choice_BaseType last = (TTCN3_Set_Seq_Choice_BaseType)referencedLast;
				final int nofComponents = last.getNofComponents();
				if (jsonAttribute.as_value && nofComponents == 1) {
					getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'metainfo for unbound' will be ignored, because the {0} is encoded without field names.", referencedLast.getTypetype().getName()));
				} else {
					for (int i = 0; i < nofComponents; i++) {
						final Type componentType = last.getComponentByIndex(i).getType();
						if (componentType.jsonAttribute == null) {
							componentType.jsonAttribute = new JsonAST();
						}
						componentType.jsonAttribute.metainfo_unbound = true;
					}
				}
			} else {
				switch (referencedLast.getTypetype()) {
				case TYPE_SEQUENCE_OF:
				case TYPE_SET_OF:
				case TYPE_ARRAY:
					break;
				default:
					if (getParentType() == null || (getParentType().getTypetype() != Type_type.TYPE_TTCN3_SEQUENCE &&
					getParentType().getTypetype() != Type_type.TYPE_TTCN3_SET)) {
						// only allowed if it's an array type or a field of a record/set
						getLocation().reportSemanticError("Invalid attribute 'metainfo for unbound', requires record, set, record of, set of, array or field of a record or set");
					}
					break;
				}
			}
		}

		if (jsonAttribute.as_number) {
			if (referencedLast.getTypetypeTtcn3() != Type_type.TYPE_TTCN3_ENUMERATED) { 
				getLocation().reportSemanticError("Invalid attribute, 'as number' is only allowed for enumerated types");
			} else if (!jsonAttribute.enum_texts.isEmpty()) {
				getLocation().reportSemanticWarning("Attribute 'text ... as ...' will be ignored, because the enumerated values are encoded as numbers");
			}
		}

		//FIXME: check tag_list

		if (jsonAttribute.as_map) {
			if (referencedLast.getTypetype() != Type_type.TYPE_SEQUENCE_OF && referencedLast.getTypetype() != Type_type.TYPE_SET_OF) {
				getLocation().reportSemanticError("Invalid attribute, 'as map' requires record of or set of");
			} else {  // T_SEQOF && T_SETOF
				final AbstractOfType last = (AbstractOfType) referencedLast;
				final IType ofType = last.getOfType();
				final IType ofTypeLast = ofType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
				if ((ofTypeLast.getTypetype() == Type_type.TYPE_TTCN3_SEQUENCE && ((TTCN3_Sequence_Type) ofTypeLast).getNofComponents() == 2 )) {
		        	final Type keyType = ((TTCN3_Sequence_Type) ofTypeLast).getComponentByIndex(0).getType();
		        	if (keyType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp()).getTypetype() != Type_type.TYPE_UCHARSTRING) {
		        		getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be a universal charstring");
		        	}

		        	if (keyType.isOptionalField()) {
		        		getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be mandatory");
		        	}
		        } else if (ofTypeLast.getTypetype() == Type_type.TYPE_TTCN3_SET || ((TTCN3_Set_Type) ofTypeLast).getNofComponents() == 2 ) {
		        	final Type keyType = ((TTCN3_Set_Type) ofTypeLast).getComponentByIndex(0).getType();
		        	if (keyType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp()).getTypetype() != Type_type.TYPE_UCHARSTRING) {
		        		getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be a universal charstring");
		        	}

		        	if (keyType.isOptionalField()) {
		        		getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be mandatory");
		        	}
		        } else {
		        	getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type to be a record or set with 2 fields");
		        }
			}
		}

		if (!jsonAttribute.enum_texts.isEmpty()) {
			if (referencedLast.getTypetypeTtcn3() != Type_type.TYPE_TTCN3_ENUMERATED) {
				getLocation().reportSemanticError("Invalid attribute, 'text ... as ...' requires an enumerated type");
			} else {
				for (int i = 0; i < jsonAttribute.enum_texts.size(); i++) {
					final Identifier identifier = new Identifier(Identifier_type.ID_TTCN, jsonAttribute.enum_texts.get(i).from, NULL_Location.INSTANCE, true);
					if (!((TTCN3_Enumerated_Type)referencedLast).hasEnumItemWithName(identifier)) {
						getLocation().reportSemanticError(MessageFormat.format("Invalid JSON default value for enumerated type `{0}''", getTypename()));
					} else {
						final EnumItem enumItem = ((TTCN3_Enumerated_Type)referencedLast).getEnumItemWithName(identifier);
						final int index = (int) ((Integer_Value) enumItem.getValue()).getValue();
						jsonAttribute.enum_texts.get(i).index = index;
						for (int j = 0; j < i; j++) {
							if (jsonAttribute.enum_texts.get(j).index == index) {
								getLocation().reportSemanticError(MessageFormat.format("Duplicate attribute 'text ... as ...' for enumerated value `{0}''", jsonAttribute.enum_texts.get(i).from));
							}
						}
					}
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public int getDefaultRawFieldLength() {
		if (referencedLast != null && !referencedLast.getIsErroneous(CompilationTimeStamp.getBaseTimestamp()) && referencedLast != this) {
			return referencedLast.getDefaultRawFieldLength();
		}

		return 0;
	}

	@Override
	/** {@inheritDoc} */
	public int getRawLength(final BuildTimestamp timestamp) {
		if (referencedLast != null && !referencedLast.getIsErroneous(CompilationTimeStamp.getBaseTimestamp()) && referencedLast != this) {
			return referencedLast.getRawLength(timestamp);
		}

		return -1;
	}

	@Override
	/** {@inheritDoc} */
	public int getLengthMultiplier() {
		if (referencedLast != null && !referencedLast.getIsErroneous(CompilationTimeStamp.getBaseTimestamp()) && referencedLast != this) {
			return referencedLast.getLengthMultiplier();
		}

		return 1;
	}

	@Override
	/** {@inheritDoc} */
	public void checkMapParameter(final CompilationTimeStamp timestamp, final IReferenceChain refChain, final Location errorLocation) {
		if (isBuildCancelled()) {
			return;
		}

		if (refChain.contains(this)) {
			return;
		}

		refChain.add(this);
		if (referencedLast != null) {
			referencedLast.checkMapParameter(timestamp, refChain, errorLocation);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void forceRaw(final CompilationTimeStamp timestamp) {
		if (referenced != null && !referenced.getIsErroneous(CompilationTimeStamp.getBaseTimestamp()) && referencedLast != this) {
			referenced.forceRaw(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void forceJson(final CompilationTimeStamp timestamp) {
		if (referenced != null && !referenced.getIsErroneous(CompilationTimeStamp.getBaseTimestamp()) && referencedLast != this) {
			referenced.forceJson(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		reference.updateSyntax(reparser, false);
		reparser.updateLocation(reference.getLocation());

		if (subType != null) {
			subType.updateSyntax(reparser, false);
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (reference != null) {
			reference.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (reference!=null && !reference.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRep_for_OpenType_AltName(final CompilationTimeStamp timestamp) {
		if (isTagged() || constraints != null ||
			(withAttributesPath != null && withAttributesPath.hasAttributes(timestamp))) {
			return "FIXLSP";
		} else {
			return "FIXLSP";
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isPresentAnyvalueEmbeddedField(final ExpressionStruct expression, final List<ISubReference> subreferences, final int beginIndex) {
		if (this == referencedLast || referencedLast == null) {
			TitanLogger.logFatal("Code generator reached erroneous type reference `" + getFullName() + "''");
			expression.expression.append("FATAL_ERROR encountered while processing `" + getFullName() + "''\n");
			return false;
		}

		return referencedLast.isPresentAnyvalueEmbeddedField(expression, subreferences, beginIndex);
	}

	@Override
	public String getDefaultSnippet(String lineEnding, int indentation, AtomicInteger placeholderIdx) {
		if (referencedLast == null || this == referencedLast) {
			return "";
		}
		return ((Type)referencedLast).getDefaultSnippet(lineEnding, indentation, placeholderIdx);
	}

	@Override
	public String getRestrictionDesc() {
		if (referencedLast == null || this == referencedLast) {
			return null;
		}
		return ((Type)referencedLast).getRestrictionDesc();
	}
	
	@Override
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(location, SemanticType.Type);
		super.setSemanticInformation();
	}
}
