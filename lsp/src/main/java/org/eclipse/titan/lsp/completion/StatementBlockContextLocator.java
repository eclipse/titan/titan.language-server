/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Assignment_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.core.Position;

public class StatementBlockContextLocator implements IContextLocator {
	private final Position cursorPosition;
	private final StatementBlock block;
	private final TTCN3Module module;
	private final String sourceText;
	private String contextString;
	private Statement actualStatement;
	private Statement lastStatementBeforeCursorPos;

	public StatementBlockContextLocator(final StatementBlock block, final TTCN3Module module, String sourceText, final Position cursorPosition) {
		this.cursorPosition = cursorPosition;
		this.block = block;
		this.module = module;
		this.sourceText = sourceText;
		contextString = findContextString();
	}

	private boolean isCursorOnStatement() {
		for (int i = 0; i < block.getSize(); i++) {
			final Statement statement = block.getStatementByIndex(i);
			if (statement.getLocation().containsPosition(cursorPosition)) {
				actualStatement = statement;
				return true;
			}
		}
		return false;
	}

	private boolean isCursorBetweenStatements() {
		Statement statementBeforeCursor = null;
		for (int i = 0; i < block.getSize(); i++) {
			final Statement statement = block.getStatementByIndex(i);
			if (cursorPosition.before(statement.getLocation().getStartPosition())) {
				break;
			}
			if (cursorPosition.after(statement.getLocation().getEndPosition())) {
				statementBeforeCursor = statement;
			}
		}

		if (statementBeforeCursor != null) {
			if (statementBeforeCursor instanceof Assignment_Statement &&
					((Assignment_Statement)statementBeforeCursor).getTemplate() == null) {
				actualStatement = statementBeforeCursor;
				return false;
			}

			lastStatementBeforeCursorPos = statementBeforeCursor;
			return true;
		}
		return false;
	}

	private String findContextString() {
		if (isCursorOnStatement() || actualStatement != null) {
			return extractContextStringFromSource(
					actualStatement.getLocation().getStartPosition(),
					cursorPosition,
					sourceText);
		}

		if (isCursorBetweenStatements()) {
			return extractContextStringFromSource(
					lastStatementBeforeCursorPos.getLocation().getEndPosition(),
					cursorPosition,
					sourceText);
		}

		return extractContextStringFromSource(
				new Position(cursorPosition.getLine(), 0),
				cursorPosition,
				sourceText);
	}

	@Override
	public String getContextString() {
		return contextString;
	}

	public Statement getActualStatement() {
		return actualStatement;
	}
}
