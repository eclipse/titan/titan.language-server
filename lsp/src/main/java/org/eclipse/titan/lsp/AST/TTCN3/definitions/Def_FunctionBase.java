/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.DocumentComment.CommentTag;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.codeAction.CodeActionHelpers;
import org.eclipse.titan.lsp.codeAction.CodeActionsForDocumentComments.CodeActionType_DocumentComment;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;
import org.eclipse.titan.lsp.codeAction.data.MissingParamTagData;
import org.eclipse.titan.lsp.codeAction.data.MissingParamTagData.MissingParamTagEntry;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Base class for function definitions, including:
 * <ul>
 * <li>Plain ttcn3 functions</li>
 * <li>Class constructors</li>
 * <li>Abstract class methods</li>
 * <li>External functions</li>
 * </ul>
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public abstract class Def_FunctionBase extends Definition implements ISignatureHelp {
	/**
	 * The encoding prototype. Used with the extension attributes. Also used
	 * by Def_ExtFunction
	 * */
	public enum EncodingPrototype_type {
		/** no prototype extension. */
		NONE("<no prototype>"),
		/** backtrack prototype. */
		BACKTRACK("backtrack"),
		/** conversion prototype. */
		CONVERT("convert"),
		/** fast prototype. */
		FAST("fast"),
		/** sliding window prototype */
		SLIDING("sliding");
	
		private String name;
	
		private EncodingPrototype_type(final String name) {
			this.name = name;
		}
	
		public String getName() {
			return name;
		}
	}

	/** Location of the function's signature (from 'function' keyword up to the return value)
	 *  Used when we want to report errors/warnings for a whole function but underlining the 
	 *  entire definition (including the body and finally block) looks ugly
	 *  
	 *  Right now, only used for class methods.
	 */
	protected Location signatureLocation = null;
	
	protected Assignment_type assignmentType;
	protected final FormalParameterList formalParameterList;
	protected final StatementBlock statementBlock;
	protected final Type returnType;
	protected final boolean isFinal;
	protected final boolean isAbstract;
	protected final boolean isDeterministic;
	protected final boolean isControl;
	protected NamedBridgeScope bridgeScope = null;
	
	protected Def_FunctionBase(final Identifier identifier, final StatementBlock statementBlock, final boolean isAbstract,
			final boolean isFinal, final boolean isDeterministic, final boolean isControl,
			FormalParameterList formalParameterList, Type returnType) {
		super(identifier);
		this.statementBlock = statementBlock;
		this.isAbstract = isAbstract;
		this.isControl = isControl;
		this.isDeterministic = isDeterministic;
		this.isFinal = isFinal;
		this.formalParameterList = formalParameterList;
		this.returnType = returnType;
	}

	@Override
	/** {@inheritDoc} */
	public FormalParameterList getFormalParameterList() {
		return formalParameterList;
	}

	@Override
	public Assignment_type getAssignmentType() {
		return assignmentType;
	}
	
	@Override
	/** {@inheritDoc} */
	public Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return returnType;
	}
	
	/**
	 * Gets the location of function signature (name and parameters)
	 * 
	 * @return
	 */
	public Location getSignatureLocation() {
		return signatureLocation;
	}

	/**
	 * Gets the location of function signature (name and parameters)
	 * 
	 * @param signatureLocation
	 */
	public void setSignatureLocation(Location signatureLocation) {
		this.signatureLocation = signatureLocation;
	}
	
	/**
	 * Checks if a function is final (cannot be overridden)
	 * @return
	 */
	@Override
	public boolean isFinal() {
		return isFinal;
	}
	
	/**
	 * Checks if a function is declared as abstract
	 * @return
	 */
	@Override
	public boolean isAbstract() {
		return isAbstract;
	}
	
	/**
	 * Checks if the function has a body
	 * @return
	 */
	public boolean hasBody() {
		return statementBlock != null;
	}

	/**
	 * Removes the name bridging scope.
	 * */
	protected void removeBridge() {
		if (bridgeScope != null) {
			bridgeScope.remove();
			bridgeScope = null;
		}
	}
	
	/**
	 * Generates common part of documentation comment text
	 * 
	 * @param indentation
	 * @param formalParameterList
	 * @param returnType
	 * @return
	 */
	public String generateCommonDocComment(final String indentation, FormalParameterList formalParameterList, Type returnType) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append("\n");

		if (formalParameterList!= null && formalParameterList.size() > 0) {
			for (final FormalParameter param : formalParameterList) {
				sb.append(ind).append(ICommentable.PARAM_TAG).append(" ")
					.append(param.getIdentifier().getDisplayName()).append("\n");
			}
		}

		if (returnType != null) {
			sb.append(ind).append(ICommentable.RETURN_TAG).append("\n");
		}
		sb.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}
	
	/**
	 * Returns hover info based on document comments
	 * 
	 * @param hoverContent
	 * @param dc
	 * @param formalParameterList
	 * @return
	 */
	public Ttcn3HoverContent getHoverContentFromComment(Ttcn3HoverContent hoverContent, DocumentComment dc, FormalParameterList formalParameterList) {
		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addParamsContent(hoverContent, formalParameterList);
			dc.addReturnContent(hoverContent);
			dc.addVerdictsContent(hoverContent);
			dc.addRequirementsContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		} else {
			addFormalParameterListToHoverContent(hoverContent, lastTimeChecked);
		}
		return hoverContent;
	}
	
	/**
	 * Checks whether two functions have the same signature
	 * 
	 * @param timestamp
	 * @param other
	 * @return
	 */
	public abstract IsIdenticalResult isIdentical(CompilationTimeStamp timestamp, Def_FunctionBase other);
	
	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = super.getCompletionItem();
		item.setInsertText(getIdentifier().getDisplayName() + "()");
		item.setKind(CompletionItemKind.Function);
		return item;
	}
	
	public CodeLens getCodeLens() {
		final int refcount = getReferenceCount();
		final List<org.eclipse.titan.lsp.AST.Location> referenceList = getDeclaration().getAssignment().getReferenceLocationList();
		if (referenceList == null) {
			return null;
		}

		final List<org.eclipse.lsp4j.Location> refLocations = referenceList.stream()
				.map(m -> m.getLspLocation())
				.collect(Collectors.toList());

		final CodeLens lens = new CodeLens(identifier.getLocation().getRange());
		final Command command = new Command();

		final List<Object> args = new ArrayList<>();
		args.add(getLocation().getFile().toURI().toString());
		args.add(lens.getRange().getStart());
		args.add(refLocations);

		command.setTitle("Referenced " + refcount + " times");
		command.setCommand("titan.getReferencesForCodelens");
		command.setArguments(args);
		lens.setCommand(command);

		return lens;
	}
	
	@Override
	public void checkDocumentComment() {
		if (!hasDocumentComment()) {
			return;
		}
		
		final Map<String,String> params = documentComment.getParams();
		if (params != null && !params.isEmpty()) {
			final Map<String,Location> paramLocations = documentComment.getParamLocations();
			params.entrySet().stream()
				.forEach(param -> {
					final String paramName = param.getKey();
					if (formalParameterList.get(new Identifier(Identifier_type.ID_TTCN, paramName)) == null) {
						paramLocations.get(paramName).reportSemanticWarning(
								MessageFormat.format("The function has no parameter named `{0}''", paramName));
					}
				});

			Location tempFirstLocation = null;
			final Map<String,Location> locs = documentComment.getParamLocations();
			if (locs.size() != 0) {
				List<Location> locations = paramLocations.values().stream()
					.sorted(Location.getComparator())
					.collect(Collectors.toList());
				tempFirstLocation = locations.get(locations.size() - 1);
			}
			final Location firstLocation = tempFirstLocation;
			
			/**
			 * Report undocumented parameters.
			 * On purpose, this is only checked if at least one @param tag exists already. This is to avoid
			 * warnings if there is no intention to document the parameters.
			 * 
			 * Code actions are provided to add the current or all missing @param tags.
			 */
			final Map<MissingParamTagEntry,Location> missingEntries = new HashMap<>();
			final List<MissingParamTagEntry> missingEntriesList = new ArrayList<>();
			formalParameterList.forEach(param -> {
				if (!params.containsKey(param.getIdentifier().getDisplayName())) {
					MissingParamTagEntry paramData = new MissingParamTagEntry(param.getIdentifier().getDisplayName(), firstLocation);
					missingEntries.put(paramData, param.getIdentifier().getLocation());
					missingEntriesList.add(paramData);
				}
			});
			missingEntries.forEach((k, v) -> {
				MissingParamTagData missingData = new MissingParamTagData(k, missingEntriesList);
				final String data = CodeActionHelpers.getJSON(missingData);
				if (data != null) {
					final List<CodeActionType_DocumentComment> actionTypes = new ArrayList<>(Arrays.asList(
							CodeActionType_DocumentComment.ADDMISSINGPARAMETERTAG,
							CodeActionType_DocumentComment.ADDALLMISSINGPARAMETERTAGS
					));
					v.reportSemanticWarning(
						"Parameter has no related `@param` comment tag", actionTypes, CodeActionProviderType.MISSINGPARAMTAG, data);
				}
			});
		}
			
		getDocumentComment().checkNonApplicableTags(
			EnumSet.of(CommentTag.Config, CommentTag.Exception, CommentTag.Priority, CommentTag.Purpose), "functions");
		
		super.checkDocumentComment();
	}
}
