/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.util.Collections;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;

/**
 * @author Kristof Szabados
 * */
public final class Deactivate_Statement extends Statement {
	private static final String DEFAULTEXPECTED = "Default value expected";

	private static final String FULLNAMEPART = ".deactivate";
	private static final String STATEMENT_NAME = "deactivate";

	private final Value deactivate;

	public Deactivate_Statement(final Value deactivate) {
		this.deactivate = deactivate;

		if (deactivate != null) {
			deactivate.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Statement_type getType() {
		return Statement_type.S_DEACTIVATE;
	}

	@Override
	/** {@inheritDoc} */
	public String getStatementName() {
		return STATEMENT_NAME;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (deactivate == child) {
			return builder.append(FULLNAMEPART);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (deactivate != null) {
			deactivate.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;

		if (deactivate == null) {
			return;
		}

		deactivate.setLoweridToReference(timestamp);
		final Type_type temporalType = deactivate.getExpressionReturntype(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);

		switch (temporalType) {
		case TYPE_DEFAULT:
			deactivate.getValueRefdLast(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null);
			return;
		default:
			location.reportSemanticError(DEFAULTEXPECTED);
			return;
		}
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		return deactivate == null ? List.of(Ttcn3Lexer.LPAREN) : Collections.emptyList();
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (deactivate != null) {
			deactivate.updateSyntax(reparser, false);
			reparser.updateLocation(deactivate.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (deactivate != null) {
			deactivate.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (deactivate != null && !deactivate.accept(v)) {
			return false;
		}
		return true;
	}
}
