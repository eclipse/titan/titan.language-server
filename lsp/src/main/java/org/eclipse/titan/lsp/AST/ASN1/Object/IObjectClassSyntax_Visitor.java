/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

/**
 * Interface for OCS visitors.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 */
public interface IObjectClassSyntax_Visitor {
	/**
	 * Visit the provided syntax root parameter.
	 * @param parameter the root syntax node to visit.
	 */
	void visitRoot(ObjectClassSyntax_root parameter);

	/**
	 * Visit the provided sequence parameter.
	 * @param parameter the sequence node to visit.
	 */
	void visitSequence(ObjectClassSyntax_sequence parameter);

	/**
	 * Visit the provided syntax literal parameter.
	 * @param parameter the syntax liter node to visit.
	 */
	default void visitLiteral(ObjectClassSyntax_literal parameter) {
		// Do nothing by default
	}

	/**
	 * Visit the provided setting syntax parameter.
	 * @param parameter the setting syntax node to visit.
	 */
	default void visitSetting(ObjectClassSyntax_setting parameter) {
		// Do nothing by default
	}
}
