/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.ISetting;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Class to represent FieldSettings.
 *
 * @author Kristof Szabados
 */
public abstract class FieldSetting extends ASTNode implements ILocateableNode, ICancelBuild {

	/**
	 * The location of the whole field setting. This location encloses the
	 * field setting fully, as it is used to report errors to.
	 */
	protected Location location = Location.getNullLocation();

	protected Identifier name;

	/** the time when this field setting was check the last time. */
	protected CompilationTimeStamp lastTimeChecked;

	protected FieldSetting(final Identifier name) {
		this.name = name;
	}

	/** @return a new instance */
	public abstract FieldSetting newInstance();

	public final Identifier getName() {
		return name;
	}

	@Override
	/** {@inheritDoc} */
	public final void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getLocation() {
		return location;
	}

	/**
	 * @return the setting of this FieldSetting.
	 */
	public abstract ISetting getSetting();

	@Override
	/** {@inheritDoc} */
	public final void setMyScope(final Scope scope) {
		getSetting().setMyScope(scope);
	}

	/**
	 * Check the field setting.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param fieldSpecification the field specification to check against.
	 */
	public abstract void check(final CompilationTimeStamp timestamp, FieldSpecification fieldSpecification);
}
