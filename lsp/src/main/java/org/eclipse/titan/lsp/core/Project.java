/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.core;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.WorkspaceFolder;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.TitanLanguageServer.ServerMode;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.IResourceChangedListener;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.OutdatedFileCollector;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;

/**
 * 
 * @author Miklos Magyari
 *
 */
public final class Project {
	public static final Project INSTANCE = new Project();
	private Project() {}

	private Path root;

	private Map<Path,WorkspaceFolder> workspaceFolders = new HashMap<>();
	private static Map<Path,ProjectItem> projectItems = new ConcurrentHashMap<>();

	/** List of registered resource change listeners */
	private static List<IResourceChangedListener> listeners = new CopyOnWriteArrayList<>();

	/**
	 * Adds a preference change listener for a given preference.
	 * @param listener Event handler to be called
	 */
	public void addResourceChangedListener(IResourceChangedListener listener) {
		listeners.add(listener);
	}

	/**
	 * Calls all event handlers watching the given resource.
	 * @param item project item that has changed
	 */
	public static void fireResourceChanged(final ProjectItem item) {
		listeners.stream()
		.forEach(listener ->
			listener.changed(item)
		);
	}

	public boolean addWorkspaceFolders(final List<WorkspaceFolder> wsFolders) {
		if (wsFolders == null) {
			return false;
		}
		boolean addedNewFolder = false;
		for (WorkspaceFolder folder : wsFolders) {
			Path path = null;
			try {
				path = Path.of((new URL(folder.getUri()).toURI()));
				if (root == null) {
					root = path;
				}
				if (!workspaceFolders.containsKey(path)) {
					workspaceFolders.put(path, folder);
					final WorkspaceVisitor visitor = new WorkspaceVisitor();
					try {
						Files.walkFileTree(path, visitor);
					} catch (IOException e) {
						TitanLogger.logError(e);
					}
					final List<Path> fileList = visitor.getFileList();

					addProjectItems(path, fileList);
					addedNewFolder = true;
				}
			} catch (MalformedURLException | URISyntaxException e) {
				TitanLogger.logError(e);
			}
		}
		return addedNewFolder;
	}

	public void removeWorkspaceFolders(final List<WorkspaceFolder> removedFolders) {
		if (removedFolders == null) {
			return;
		}
		for (final WorkspaceFolder folder : removedFolders) {
			try {
				final Path path = Path.of((new URL(folder.getUri()).toURI()));
				projectItems.entrySet().stream()
					.forEach(item -> {
						if (item.getKey().startsWith(path)) {
							projectItems.remove(item.getKey());
						}
					});
			} catch (MalformedURLException | URISyntaxException e) {
				TitanLogger.logError(e);
			}
			
		}
	}

	public Map<Path,WorkspaceFolder> getWorkspaceFolders() {
		return workspaceFolders;
	}

	public Path getRoot() {
		return root;
	}

	/**
	 * Adds a new item to the project and returns it.
	 * 
	 * @param path
	 * @return The item added. If the item is already in the project, it is returned.
	 */
	public ProjectItem addProjectItem(final Path path) {
		if (! projectItems.containsKey(path)) {
			final ProjectItem projectItem = new ProjectItem(this, path);
			projectItems.put(path, projectItem);
			return projectItem;
		}
		return projectItems.get(path);
	}

	/**
	 * Adds a list of items to the project
	 */
	public void addProjectItems(final Path folder, final List<Path> fileList) {
		fileList.forEach(file ->
			addProjectItem(file)
		);
	}

	public void removeProjectItem(final ProjectItem item) {
		if (item != null) {
			projectItems.remove(item.getPath());
		}
	}

	public ProjectItem getProjectItem(final Path path) {
		return path != null ? projectItems.get(path) : null;
	}

	public ProjectItem getProjectItem(final String uri) {
		return getProjectItem(IOUtils.getPath(uri));
	}

	public Map<Path,ProjectItem> getAllProjectItems() {
		return projectItems;
	}

	public Module getModuleByPath(final Path path) {
		if (path == null) {
			return null;
		}
		return projectItems.get(path) == null ? null :
			GlobalParser.getProjectSourceParser(this).containedModule(path);
	}

	public Module getModuleByURI(final String uri) {
		return getModuleByPath(IOUtils.getPath(uri));
	}

	public LspMarker addMarker(final File file, final Diagnostic diagnostic) {
		return addMarker(file, new LspMarker(diagnostic));
	}

	public LspMarker addMarker(final File file, final LspMarker marker) {
		final ProjectItem projectItem = projectItems.get(file.toPath());
		return projectItem == null ? null : projectItem.addMarker(marker);
	}

	public void resetProject() {
		final ProjectSourceParser projectParser = GlobalParser.getProjectSourceParser(this);

		projectItems.values().stream().forEach(projectItem -> {
			if (projectItem.isExcluded() || projectItem.isDirectory()) {
				return;
			}
			projectItem.reset();
		});

		// reset AST modules
		projectParser.resetParser();
	}

	public void removeMarkers(final Path path) {
		final ProjectItem projectItem = projectItems.get(path);
		if (projectItem != null) {
			projectItem.removeMarkers();
		}
	}

	public void removeMarkers(final File file) {
		removeMarkers(file.toPath());
	}

	public void removeMarkers(final File file, final Position start, final Position end) {
		removeMarkers(file, start, end, false);
	}

	public void removeMarkers(final File file, final Position start, final Position end, final boolean semanticOnly) {
		final ProjectItem projectItem = projectItems.get(file.toPath());
		if (projectItem != null) {
			projectItem.removeMarkers(start, end, semanticOnly);
		}
	}

	public void restoreMarkers(final File file) {
		final ProjectItem projectItem = projectItems.get(file.toPath());
		if (projectItem != null) {
			projectItem.restoreMarkers();
		}
	}

	public final List<Diagnostic> getMarkers(final Path path) {
		final ProjectItem projectItem = projectItems.get(path);
		return projectItem == null ? Collections.emptyList() : projectItem.getMarkerList();
	}

	public void collectOutdatedFiles(OutdatedFileCollector visitor) {
		projectItems.forEach((k, v) -> {
			if (v.isExcluded() || v.isDirectory()) { 
				return;
			}

			visitor.visitFile(k);
		});
	}

	public void setTpdFiles(final List<Path> filelist) {
		projectItems.entrySet().stream().forEach(item -> {
			item.getValue().setExcluded(true);

			/* clear error markers */
			if (TitanLanguageServer.getMode() == ServerMode.LanguageServer) {
				final PublishDiagnosticsParams diagParams = new PublishDiagnosticsParams(item.getKey().toUri().toString(), new ArrayList<>());
				TitanLanguageServer.getClient().publishDiagnostics(diagParams);
			}
		});

		filelist.stream().forEach(toBeIncluded -> {
			final ProjectItem existing = getProjectItem(toBeIncluded);
			if (existing != null) {
				if (existing.isDirectory()) {
					includeFolder(existing.getPath());
					existing.setExcluded(false);
				} else {
					existing.setExcluded(false);
				}
			}
		});
	}

	private void includeFolder(final Path path) {
		final String folder = path.toString();
		projectItems.forEach((p, item) -> {
			if (p.toString().startsWith(folder)) {
				item.setExcluded(false);
			}
		});
	}

	/**
	 * Sets all project items as syntactically outdated. The next project
	 * analysis will reanalyze all files.
	 */
	public void outdateAllItems() {
		final ProjectSourceParser projectParser = GlobalParser.getProjectSourceParser(this);
		projectItems.forEach((path, item) -> {
			item.setSyntacticallyUpToDate(false);
			projectParser.reportOutdating(path);
		});
	}
}
