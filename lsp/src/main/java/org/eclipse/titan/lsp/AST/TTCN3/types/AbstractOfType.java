/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.ASN1.IASN1Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Sequence_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Set_Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.JsonAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST;
import org.eclipse.titan.lsp.AST.TTCN3.templates.SingleLenghtRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.Length_ParsedSubType;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.IParsedSubType;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.SubType;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimension;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.SetOf_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ExpressionStruct;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * */
public abstract class AbstractOfType extends Type implements IASN1Type {

	public static final String INCOMPLETEPRESENTERROR = "Not used symbol `-' is not allowed in this context";

	private static final String FULLNAMEPART = ".oftype";

	private final IType ofType;
	private boolean componentInternal;

	private boolean insideCanHaveCoding = false;

	protected AbstractOfType(final IType ofType) {
		this.ofType = ofType;

		if (ofType != null) {
			ofType.setOwnertype(TypeOwner_type.OT_RECORD_OF, this);
			ofType.setFullNameParent(this);
		}
	}

	public IType getOfType() {
		return ofType;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (ofType == child) {
			return builder.append(FULLNAMEPART);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (ofType != null) {
			ofType.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isIdentical(final CompilationTimeStamp timestamp, final IType type) {
		check(timestamp);
		type.check(timestamp);
		final IType temp = type.getTypeRefdLast(timestamp);

		if (getIsErroneous(timestamp) || temp.getIsErroneous(timestamp)) {
			return true;
		}

		return this == temp;
	}

	/**
	 * Checks that the provided type is sub-type compatible with the actual
	 * set of type.
	 * <p>
	 * In case of sequence/set/array this means that the number of their
	 * fields fulfills the length restriction of the set of type.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle
	 * @param other
	 *                the type to check against.
	 *
	 * @return true if they are sub-type compatible, false otherwise.
	 * */
	public boolean isSubtypeCompatible(final CompilationTimeStamp timestamp, final IType other) {
		if (subType == null || other == null) {
			return true;
		}

		long nofComponents;
		switch (other.getTypetype()) {
		case TYPE_ASN1_SEQUENCE:
			nofComponents = ((ASN1_Sequence_Type) other).getNofComponents();
			break;
		case TYPE_TTCN3_SEQUENCE:
			nofComponents = ((TTCN3_Sequence_Type) other).getNofComponents();
			break;
		case TYPE_ASN1_SET:
			nofComponents = ((ASN1_Set_Type) other).getNofComponents();
			break;
		case TYPE_TTCN3_SET:
			nofComponents = ((TTCN3_Set_Type) other).getNofComponents();
			break;
		case TYPE_SEQUENCE_OF:
		case TYPE_SET_OF:
			if (other.getSubtype() == null) {
				return true;
			}

			return subType.isCompatible(timestamp, other.getSubtype());
		case TYPE_ARRAY: {
			final ArrayDimension dimension = ((Array_Type) other).getDimension();
			if (dimension.getIsErroneous(timestamp)) {
				return false;
			}

			nofComponents = dimension.getSize();
			break;
		}
		default:
			return false;
		}

		final List<IParsedSubType> tempRestrictions = new ArrayList<IParsedSubType>(1);
		final Integer_Value length = new Integer_Value(nofComponents);
		tempRestrictions.add(new Length_ParsedSubType(new SingleLenghtRestriction(length)));
		final SubType tempSubtype = new SubType(getSubtypeType(), this, tempRestrictions, null);
		tempSubtype.check(timestamp);
		return subType.isCompatible(timestamp, tempSubtype);
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetypeTtcn3() {
		if (isErroneous) {
			return Type_type.TYPE_UNDEFINED;
		}

		return getTypetype();
	}

	@Override
	/** {@inheritDoc} */
	public String getTypename() {
		return getFullName();
	}

	@Override
	/** {@inheritDoc} */
	public boolean isComponentInternal(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return componentInternal;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		if (myScope != null && null != lastTimeChecked) {
			final Module module = myScope.getModuleScope();
			if (module != null && module.getSkippedFromSemanticChecking()) {
				lastTimeChecked = timestamp;
				return;
			}
		}

		lastTimeChecked = timestamp;
		componentInternal = false;
		isErroneous = false;

		initAttributes(timestamp);

		if (ofType == null) {
			setIsErroneous(true);
		} else {
//			ofType.setGenName(getGenNameOwn(), "0");
			ofType.setParentType(this);
			ofType.check(timestamp);
			if (!isAsn()) {
				ofType.checkEmbedded(timestamp, ofType.getLocation(), true, "embedded into another type");
			}
			componentInternal = ofType.isComponentInternal(timestamp);
		}

		if (constraints != null) {
			constraints.check(timestamp);
		}

		checkSubtypeRestrictions(timestamp);

		if (myScope != null) {
			checkEncode(timestamp);
			checkVariants(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkComponentInternal(final CompilationTimeStamp timestamp, final Set<IType> typeSet, final String operation) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (typeSet.contains(this)) {
			return;
		}

		if (ofType != null && ofType.isComponentInternal(timestamp)) {
			typeSet.add(this);
			ofType.checkComponentInternal(timestamp, typeSet, operation);
			typeSet.remove(this);
		}
	}

	/**
	 * Checks the SequenceOf_value kind value against this type.
	 * SequenceOf_value kinds have to be converted before calling this
	 * function.
	 * <p>
	 * Please note, that this function can only be called once we know for
	 * sure that the value is of set-of type.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param value
	 *                the value to be checked
	 * @param expectedValue
	 *                the kind of value expected here.
	 * @param incompleteAllowed
	 *                wheather incomplete value is allowed or not.
	 * @param implicitOmit
	 *                true if the implicit omit optional attribute was set
	 *                for the value, false otherwise
	 * */
	public boolean checkThisValueSetOf(final CompilationTimeStamp timestamp, final SetOf_Value value, final Assignment lhs, final Expected_Value_type expectedValue,
			final boolean incompleteAllowed, final boolean implicitOmit, final boolean strElem) {
		boolean selfReference = false;

		if (value.isIndexed()) {
			boolean checkHoles = Expected_Value_type.EXPECTED_CONSTANT.equals(expectedValue);
			BigInteger maxIndex = BigInteger.valueOf(-1);
			final Map<BigInteger, Integer> indexMap = new HashMap<BigInteger, Integer>(value.getNofComponents());
			for (int i = 0, size = value.getNofComponents(); i < size; i++) {
				final IValue component = value.getValueByIndex(i);
				final Value index = value.getIndexByIndex(i);
				final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
				final IValue indexLast = index.getValueRefdLast(timestamp, referenceChain);
				referenceChain.release();

				if (indexLast.getIsErroneous(timestamp) || !Value_type.INTEGER_VALUE.equals(indexLast.getValuetype())) {
					checkHoles = false;
				} else {
					final BigInteger tempIndex = ((Integer_Value) indexLast).getValueValue();
					if (tempIndex.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0) {
						index.getLocation()
						.reportSemanticError(
								MessageFormat.format(
										"A integer value less than `{0}'' was expected for indexing type `{1}'' instead of `{2}''",
										Integer.MAX_VALUE, getTypename(), tempIndex));
						checkHoles = false;
					} else if (tempIndex.compareTo(BigInteger.ZERO) < 0) {
						index.getLocation().reportSemanticError(MessageFormat.format(
								"A non-negative integer value was expected for indexing type `{0}'' instead of `{1}''", getTypename(), tempIndex));
						checkHoles = false;
					} else if (indexMap.containsKey(tempIndex)) {
						index.getLocation().reportSemanticError(
								MessageFormat.format("Duplicate index value `{0}'' for components {1} and {2}",
										tempIndex, indexMap.get(tempIndex), i + 1));
						checkHoles = false;
					} else {
						indexMap.put(tempIndex, Integer.valueOf(i + 1));
						if (maxIndex.compareTo(tempIndex) < 0) {
							maxIndex = tempIndex;
						}
					}
				}

				component.setMyGovernor(getOfType());
				final IValue tempValue2 = getOfType().checkThisValueRef(timestamp, component);
				selfReference |= getOfType().checkThisValue(timestamp, tempValue2, lhs,
						new ValueCheckingOptions(expectedValue, incompleteAllowed, false, true, implicitOmit, strElem));
			}
			if (checkHoles && maxIndex.compareTo(BigInteger.valueOf(indexMap.size() - 1L)) != 0) {
				value.getLocation().reportSemanticError("It's not allowed to create hole(s) in constant values");
			}
		} else {
			for (int i = 0, size = value.getNofComponents(); i < size; i++) {
				final IValue component = value.getValueByIndex(i);
				component.setMyGovernor(getOfType());
				if (Value_type.NOTUSED_VALUE.equals(component.getValuetype())) {
					if (!incompleteAllowed) {
						component.getLocation().reportSemanticWarning(INCOMPLETEPRESENTERROR);
					}
				} else {
					final IValue tempValue2 = getOfType().checkThisValueRef(timestamp, component);
					selfReference |= getOfType().checkThisValue(timestamp, tempValue2, lhs,
							new ValueCheckingOptions(expectedValue, incompleteAllowed, false, true, implicitOmit, strElem));
				}
			}
		}

		value.setLastTimeChecked(timestamp);

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean canHaveCoding(final CompilationTimeStamp timestamp, final MessageEncoding_type coding) {
		if (insideCanHaveCoding) {
			return true;
		}
		insideCanHaveCoding = true;

		for (int i = 0; i < codingTable.size(); i++) {
			final Coding_Type tempCodingType = codingTable.get(i);

			if (tempCodingType.builtIn && tempCodingType.builtInCoding.equals(coding)) {
				insideCanHaveCoding = false;
				return true; // coding already added
			}
		}

		final IType refdLast = ofType.getTypeRefdLast(timestamp);
		final boolean result = refdLast.canHaveCoding(timestamp, coding);

		insideCanHaveCoding = false;
		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void checkCodingAttributes(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		//check raw attributes
		if (subType != null) {
			final int restrictionLength = subType.get_length_restriction();
			if (restrictionLength != -1) {
				if (rawAttribute == null) {
					rawAttribute = new RawAST(getDefaultRawFieldLength());
				}
				rawAttribute.length_restriction = restrictionLength;

				ofType.forceRaw(timestamp);
				if (rawAttribute.fieldlength == 0 && rawAttribute.length_restriction != -1) {
					rawAttribute.fieldlength = rawAttribute.length_restriction;
					rawAttribute.length_restriction = -1;
				}
				if (rawAttribute.length_restriction != -1 && rawAttribute.length_restriction != rawAttribute.fieldlength) {
					getLocation().reportSemanticError(MessageFormat.format("Invalid length specified in parameter FIELDLENGTH for type `{0}''. The FIELDLENGTH must be equal to specified length restriction", getFullName()));
				}
			}
		}

		checkJson(timestamp);

		//TODO add checks for other encodings.

		if (refChain.contains(this)) {
			return;
		}

		refChain.add(this);
		refChain.markState();

		ofType.checkCodingAttributes(timestamp, refChain);

		refChain.previousState();
	}

	@Override
	/** {@inheritDoc} */
	public void checkJson(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (jsonAttribute == null && !hasEncodeAttribute(MessageEncoding_type.JSON)) {
			return;
		}

		ofType.forceJson(timestamp);

		if (jsonAttribute == null) {
			return;
		}

		if (jsonAttribute.omit_as_null && !isOptionalField()) {
			getLocation().reportSemanticError("Invalid attribute, 'omit as null' requires optional field of a record or set.");
		}

		if (jsonAttribute.as_value) {
			getLocation().reportSemanticError("Invalid attribute, 'as value' is only allowed for unions, the anytype, or records or sets with one field");
		}

		if (jsonAttribute.alias != null) {
			final IType parent = getParentType();
			if (parent == null) {
				// only report this error when using the new codec handling, otherwise
				// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
				getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
			} else {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
				case TYPE_TTCN3_CHOICE:
					break;
				default:
					// only report this error when using the new codec handling, otherwise
					// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
					getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
					break;
				}
			}

			if (parent != null && parent.getJsonAttribute() != null && parent.getJsonAttribute().as_value) {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					// parent_type_name remains null if the 'as value' attribute is set for an invalid type
					getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					break;
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
					if (((TTCN3_Set_Seq_Choice_BaseType)parent).getNofComponents() == 1) {
						// parent_type_name remains null if the 'as value' attribute is set for an invalid type
						getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					}
					break;
				default:
					break;
				}
			}
		}

		if (jsonAttribute.parsed_default_value != null) {
			checkJsonDefault(timestamp);
		}

		//TODO: check schema extensions

		if (jsonAttribute.as_number) {
			getLocation().reportSemanticError("Invalid attribute, 'as number' is only allowed for enumerated types");
		}

		//FIXME: check tag_list

		if (jsonAttribute.as_map) {
			final IType ofTypeLast = ofType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
			if ((ofTypeLast.getTypetype() == Type_type.TYPE_TTCN3_SEQUENCE && ((TTCN3_Sequence_Type) ofTypeLast).getNofComponents() == 2 )) {
				final Type keyType = ((TTCN3_Sequence_Type) ofTypeLast).getComponentByIndex(0).getType();
				if (keyType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp()).getTypetype() != Type_type.TYPE_UCHARSTRING) {
					getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be a universal charstring");
				}

				if (keyType.isOptionalField()) {
					getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be mandatory");
				}
				ofTypeLast.setJsonAttributes(new JsonAST(jsonAttribute));
			} else if (ofTypeLast.getTypetype() == Type_type.TYPE_TTCN3_SET || ((TTCN3_Set_Type) ofTypeLast).getNofComponents() == 2 ) {
				final Type keyType = ((TTCN3_Set_Type) ofTypeLast).getComponentByIndex(0).getType();
				if (keyType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp()).getTypetype() != Type_type.TYPE_UCHARSTRING) {
					getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be a universal charstring");
				}

				if (keyType.isOptionalField()) {
					getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type's first field to be mandatory");
				}
				ofTypeLast.setJsonAttributes(new JsonAST(jsonAttribute));
			} else {
				getLocation().reportSemanticError("Invalid attribute, 'as map' requires the element type to be a record or set with 2 fields");
			}
		}

		if (!jsonAttribute.enum_texts.isEmpty()) {
			getLocation().reportSemanticError("Invalid attribute, 'text ... as ...' requires an enumerated type");
		}
	}

	@Override
	/** {@inheritDoc} */
	public void getTypesWithNoCodingTable(final CompilationTimeStamp timestamp, final List<IType> typeList, final boolean onlyOwnTable) {
		super.getTypesWithNoCodingTable(timestamp, typeList, onlyOwnTable);
		ofType.getTypesWithNoCodingTable(timestamp, typeList, onlyOwnTable);
	}

	@Override
	/** {@inheritDoc} */
	public boolean getSubrefsAsArray(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final List<Integer> subrefsArray, final List<IType> typeArray) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return true;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);
		if (subreference.getReferenceType() != Subreference_type.arraySubReference) {
			TitanLogger.logFatal();
			return false;
		}

		final Value indexValue = ((ArraySubReference) subreference).getValue();
		if (indexValue == null) {
			TitanLogger.logFatal();
			return false;
		}

		final IValue last = indexValue.getValueRefdLast(timestamp, Expected_Value_type.EXPECTED_CONSTANT, null);
		if (last == null) {
			TitanLogger.logFatal();
			return false;
		}
		if (last.getExpressionReturntype(timestamp, Expected_Value_type.EXPECTED_CONSTANT) != Type_type.TYPE_INTEGER) {
			return false;
		}
		if (!Value_type.INTEGER_VALUE.equals(last.getValuetype())) {
			return false;
		}

		final Integer_Value lastInteger = (Integer_Value) last;
		if (lastInteger.isNative()) {
			final int fieldIndex = (int) lastInteger.getValue();
			if (fieldIndex < 0) {
				return false;
			}
			subrefsArray.add(fieldIndex);
			typeArray.add(this);
		} else {
			return false;
		}
		if (ofType == null) {
			TitanLogger.logFatal();
			return false;
		}
		return ofType.getSubrefsAsArray(timestamp, reference, actualSubReference + 1, subrefsArray, typeArray);
	}

	@Override
	/** {@inheritDoc} */
	public IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final IReferenceChain refChain, final boolean interruptIfOptional) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return this;
		}

		final Expected_Value_type internalExpectation = expectedIndex == Expected_Value_type.EXPECTED_TEMPLATE ? Expected_Value_type.EXPECTED_DYNAMIC_VALUE
				: expectedIndex;
		final ISubReference subreference = subreferences.get(actualSubReference);
		switch (subreference.getReferenceType()) {
		case arraySubReference:
			final Value indexValue = ((ArraySubReference) subreference).getValue();
			if (indexValue != null) {
				indexValue.setLoweridToReference(timestamp);
				final Type_type tempType = indexValue.getExpressionReturntype(timestamp, expectedIndex);

				switch (tempType) {
				case TYPE_INTEGER:
					final IValue last = indexValue.getValueRefdLast(timestamp, expectedIndex, refChain);
					if (Value_type.INTEGER_VALUE.equals(last.getValuetype())) {
						final Integer_Value lastInteger = (Integer_Value) last;
						if (lastInteger.isNative()) {
							final long temp = lastInteger.getValue();
							if (temp < 0) {
								indexValue.getLocation().reportSemanticError(
										MessageFormat.format(SequenceOf_Type.NONNEGATIVINDEXEXPECTED, temp));
								indexValue.setIsErroneous(true);
							}
						} else {
							indexValue.getLocation().reportSemanticError(
									MessageFormat.format(SequenceOf_Type.TOOBIGINDEX, lastInteger.getValueValue(), getTypename()));
							indexValue.setIsErroneous(true);
						}
					}
					break;
				case TYPE_UNDEFINED:
					indexValue.setIsErroneous(true);
					break;
				default:
					indexValue.getLocation().reportSemanticError(SequenceOf_Type.INTEGERINDEXEXPECTED);
					indexValue.setIsErroneous(true);
					break;
				}
			}

			if (getOfType() != null) {
				return getOfType().getFieldType(timestamp, reference, actualSubReference + 1, internalExpectation, refChain,
						interruptIfOptional);
			}

			return null;
		case fieldSubReference:
			subreference.getLocation().reportSemanticError(
					MessageFormat.format(FieldSubReference.INVALIDSUBREFERENCE, ((FieldSubReference) subreference).getId()
							.getDisplayName(), getTypename()));
			return null;
		case parameterisedSubReference:
			subreference.getLocation().reportSemanticError(
					MessageFormat.format(FieldSubReference.INVALIDSUBREFERENCE, ((ParameterisedSubReference) subreference)
							.getId().getDisplayName(), getTypename()));
			return null;
		default:
			subreference.getLocation().reportSemanticError(ISubReference.INVALIDSUBREFERENCE);
			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean getFieldTypesAsArray(final Reference reference, final int actualSubReference, final List<IType> typeArray) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return true;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);
		if (subreference.getReferenceType() != Subreference_type.arraySubReference) {
			return false;
		}
		typeArray.add(this);
		if (ofType == null) {
			return false;
		}
		return ofType.getFieldTypesAsArray(reference, actualSubReference + 1, typeArray);
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean handled = false;

			if (ofType != null && reparser.envelopsDamage(ofType.getLocation())) {
				ofType.updateSyntax(reparser, true);
				reparser.updateLocation(ofType.getLocation());
				handled = true;
			}

			if (subType != null) {
				subType.updateSyntax(reparser, false);
				handled = true;
			}

			if (handled) {
				return;
			}

			throw new ReParseException();
		}

		if (ofType != null) {
			ofType.updateSyntax(reparser, false);
			reparser.updateLocation(ofType.getLocation());
		}

		if (subType != null) {
			subType.updateSyntax(reparser, false);
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void getEnclosingField(final Position offset, final ReferenceFinder rf) {
		if (ofType == null) {
			return;
		}

		ofType.getEnclosingField(offset, rf);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (ofType != null) {
			ofType.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (ofType != null && !ofType.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isPresentAnyvalueEmbeddedField(final ExpressionStruct expression, final List<ISubReference> subreferences, final int beginIndex) {
		return false;
	}
}
