/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.ttcn3parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.atn.DecisionInfo;
import org.antlr.v4.runtime.atn.DecisionState;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.Interval;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.parsers.AntlrErrorStrategy;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ISourceAnalyzer;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.TITANMarker;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.titan.lsp.parsers.TitanParseTreeListener;

/**
 * TTCN3Analyzer
 * 
 * @author Arpad Lovassy
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public class TTCN3Analyzer implements ISourceAnalyzer {
	private List<TITANMarker> warningsAndErrors;
	private List<TITANMarker> unsupportedConstructs;
	private Interval rootInterval;
	private TTCN3Module actualTtc3Module;

	/**
	 * The list of markers (ERROR and WARNING) created during parsing
	 * NOTE: used from ANTLR v4
	 */
	private List<SyntacticErrorStorage> mErrorsStored = null;

	@Override
	public List<SyntacticErrorStorage> getErrorStorage() {
		return mErrorsStored == null ? Collections.emptyList() : mErrorsStored;
	}

	@Override
	public List<TITANMarker> getWarnings() {
		return warningsAndErrors == null ? Collections.emptyList() : warningsAndErrors;
	}

	@Override
	public List<TITANMarker> getUnsupportedConstructs() {
		return unsupportedConstructs == null ? Collections.emptyList() : unsupportedConstructs;
	}

	@Override
	public TTCN3Module getModule() {
		return actualTtc3Module;
	}

	@Override
	public Interval getRootInterval() {
		return rootInterval;
	}

	/**
	 * Parse TTCN-3 file using ANTLR v4
	 * 
	 * @param aFile TTCN-3 file to parse, It cannot be null
	 * @param aCode TTCN-3 code to parse in string format
	 *              It can be null, in this case code is read from file
	 */
	public void parse(final Path aFile, final String aCode) {
		if (aCode == null) {
			return;
		} 
		
		final Reader reader = new StringReader(aCode);
		parse(reader, aCode.length(), aFile);
	}

	/**
	 * Parse TTCN-3 file using ANTLR v4
	 * @param aReader file to parse (cannot be null, closes aReader)
	 * @param aFileLength file length
	 * @param aFile Resource file
	 */
	private void parse(final Reader aReader, final int aFileLength, final Path aFile) {
		final Project project = Project.INSTANCE;
		final ProjectItem item = project.getProjectItem(aFile);
		if (item == null) {
			return;
		}
		
		final boolean realtimeEnabled = Configuration.INSTANCE.getBoolean(Configuration.ENABLE_REALTIME_EXTENSION, false);
		final boolean oopEnabled = Configuration.INSTANCE.getBoolean(Configuration.ENABLE_OOP_EXTENSION, false);
		final boolean reportReserved = Configuration.INSTANCE.getBoolean(Configuration.REPORT_KEYWORD_USED_AS_IDENTIFIER, false);
		
		final CharStream charStream = new UnbufferedCharStream( aReader );
		final Ttcn3Lexer lexer = new Ttcn3Lexer( charStream );
		lexer.enableOop(oopEnabled);
		lexer.setReportinReservedKeywords(reportReserved);
		lexer.setActualFile(aFile.toFile());
		lexer.setCommentTodo( true );
		lexer.setTokenFactory( new CommonTokenFactory( true ) );
		lexer.initRootInterval( aFileLength );
		if (realtimeEnabled) {
			lexer.enableRealtime();
		}

		final TitanErrorListener lexerListener = new TitanErrorListener(aFile.toFile());
		// remove ConsoleErrorListener
		lexer.removeErrorListeners();
		lexer.addErrorListener(lexerListener);

		Ttcn3Parser parser;
		AntlrErrorStrategy strategy = new AntlrErrorStrategy();
		PreprocessedTokenStream preprocessor = null;

		final String filename = aFile.getFileName().toString();
		if (aFile != null && GlobalParser.isSupportedTTCNINExtension(
				filename.substring(filename.lastIndexOf(GeneralConstants.DOT) + 1))) {
			lexer.setTTCNPP();
			preprocessor = new PreprocessedTokenStream(lexer);
			preprocessor.setActualFile(aFile);
			preprocessor.setMacros(Configuration.INSTANCE.getAllKeys("env").stream().toArray(String[]::new)); 
			
			parser = new Ttcn3Parser( preprocessor );
			parser.setErrorHandler(strategy);
			parser.enableOop(oopEnabled);
			
			ParserUtilities.setBuildParseTree( parser );
			preprocessor.setActualLexer(lexer);
			preprocessor.setParser(parser);
		} else {
			// 1. Previously it was UnbufferedTokenStream(lexer), but it was changed to BufferedTokenStream, because UnbufferedTokenStream seems to be unusable. It is an ANTLR 4 bug.
			// Read this: https://groups.google.com/forum/#!topic/antlr-discussion/gsAu-6d3pKU
			// pr_PatternChunk[StringBuilder builder, boolean[] uni]:
			//   $builder.append($v.text); <-- exception is thrown here: java.lang.UnsupportedOperationException: interval 85..85 not in token buffer window: 86..341
			// 2. Changed from BufferedTokenStream to CommonTokenStream, otherwise tokens with "-> channel(HIDDEN)" are not filtered out in lexer.
			final CommonTokenStream tokenStream = new CommonTokenStream( lexer );

			parser = new Ttcn3Parser( tokenStream );
			parser.setErrorHandler(strategy);
			parser.enableOop(oopEnabled);
			parser.addParseListener(new TitanParseTreeListener());
			ParserUtilities.setBuildParseTree( parser );
		}

		if ( aFile != null ) {
			lexer.setActualFile( aFile.toFile() );
			parser.setActualFile( aFile.toFile() );
			//parser.setProject( aEclipseFile.getProject() );
		}

		// remove ConsoleErrorListener
		parser.removeErrorListeners();
		final TitanErrorListener parserListener = new TitanErrorListener(aFile.toFile());
		parser.addErrorListener( parserListener );

		// This is added because of the following ANTLR 4 bug:
		// Memory Leak in PredictionContextCache #499
		// https://github.com/antlr/antlr4/issues/499
		final DFA[] decisionToDFA = parser.getInterpreter().decisionToDFA;
		parser.setInterpreter(new ParserATNSimulator(parser, parser.getATN(), decisionToDFA, new PredictionContextCache()));
		
		/** uncomment this to enable parser profiling */
		//parser.setProfile(true);
		
		//try SLL mode
		try {
			parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
			final ParseTree root = parser.pr_TTCN3File();
			ParserUtilities.logParseTree( root, parser );
			warningsAndErrors = parser.getWarningsAndErrors();
			warningsAndErrors.addAll(lexer.getWarningsAndErrors());
			mErrorsStored = lexerListener.getErrorsStored();
			mErrorsStored.addAll( parserListener.getErrorsStored() );
		} catch (RecognitionException | ParseCancellationException e) {
			// quit
		}
		
		if (!warningsAndErrors.isEmpty() || !mErrorsStored.isEmpty()) {
			//SLL mode might have failed, try LL mode
			try {
				final CharStream charStream2 = new UnbufferedCharStream( aReader );
				lexer.setInputStream(charStream2);
				//lexer.reset();
				parser.reset();
				parserListener.reset();
				parser.getInterpreter().setPredictionMode(PredictionMode.LL);
				final ParseTree root = parser.pr_TTCN3File();
				ParserUtilities.logParseTree( root, parser );
				warningsAndErrors = parser.getWarningsAndErrors();
				warningsAndErrors.addAll(lexer.getWarningsAndErrors());
				mErrorsStored = lexerListener.getErrorsStored();
				mErrorsStored.addAll( parserListener.getErrorsStored() );
			} catch(RecognitionException | ParseCancellationException e) {

			}
		}

		unsupportedConstructs = parser.getUnsupportedConstructs();
		rootInterval = lexer.getRootInterval();
		actualTtc3Module = parser.getModule();
		if ( preprocessor != null ) {
			// if the file was preprocessed
			mErrorsStored.addAll(preprocessor.getErrorStorage());
			warningsAndErrors.addAll( preprocessor.getWarnings() );
			unsupportedConstructs.addAll( preprocessor.getUnsupportedConstructs() );
			if ( actualTtc3Module != null ) {
				actualTtc3Module.setIncludedFiles( preprocessor.getIncludedFiles() );
				actualTtc3Module.setInactiveCodeLocations( preprocessor.getInactiveCodeLocations() );
			}
		}
		//TODO: empty mErrorsStored not to store errors from the previous parse round in case of exception

		try {
			aReader.close();
		} catch (IOException e) {
		}
		
		item.setSyntacticallyUpToDate(true);
	}
	
	/**
	 * Code for printing data collected by parser profiling 
	 * @param parser
	 */
	private void profileParser(Ttcn3Parser parser) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-" + 35 + "s", "rule"))
			.append(String.format("%-" + 15 + "s", "time"))
			.append(String.format("%-" + 15 + "s", "invocations"))
			.append(String.format("%-" + 15 + "s", "lookahead"))
			.append(String.format("%-" + 15 + "s", "lookahead(max)"))
			.append(String.format("%-" + 15 + "s", "ambiguities"))
			.append(String.format("%-" + 15 + "s\n", "errors"));
	    for (DecisionInfo decisionInfo : parser.getParseInfo().getDecisionInfo()) {
	        DecisionState state = parser.getATN().getDecisionState(decisionInfo.decision);
	        String rule = parser.getRuleNames()[state.ruleIndex];
	        if (decisionInfo.timeInPrediction > 0) {
	        	sb.append(String.format("%-" + 35 + "s", rule))
		        	.append(String.format("%-" + 15 + "s", decisionInfo.timeInPrediction / 100000L))
		        	.append(String.format("%-" + 15 + "s", decisionInfo.invocations))
		        	.append(String.format("%-" + 15 + "s", decisionInfo.SLL_TotalLook))
		        	.append(String.format("%-" + 15 + "s", decisionInfo.SLL_MaxLook))
		        	.append(String.format("%-" + 15 + "s", decisionInfo.ambiguities))
		        	.append(String.format("%-" + 15 + "s\n", decisionInfo.errors));
	        }
	    }
	    TitanLogger.logTrace(sb.toString());
	}
}
