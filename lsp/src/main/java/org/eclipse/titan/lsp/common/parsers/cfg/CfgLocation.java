/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.parsers.cfg;

import java.nio.file.Path;

import org.antlr.v4.runtime.Token;

/**
 * The Location class represents a location in configuration files.  It was
 * stolen from "org.eclipse.titan.designer.AST.Location".  It stores only
 * location information.
 *
 * @author eferkov
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public final class CfgLocation {
	private Path file;
	private int startLine;
	private int startColumn;
	private int endLine;
	private int endColumn;

	/**
	 * Constructor for ANTLR v4 tokens
	 * @param aFile the parsed file
	 * @param aStartToken the 1st token, its line and start position will be used for the location
	 *                  NOTE: start position is the column index of the tokens 1st character.
	 *                        Column index starts with 0.
	 * @param aEndToken the last token, its end position will be used for the location.
	 *                  NOTE: end position is the column index after the token's last character.
	 */
	public CfgLocation( final Path aFile, final Token aStartToken, final Token aEndToken ) {
		setLocation(aFile, aStartToken.getLine(), aStartToken.getCharPositionInLine(), aEndToken.getLine(), aEndToken.getCharPositionInLine());
	}

	private final void setLocation(final Path file, final int startline, final int startcolumn, 
		final int endline, final int endcolumn) {
		this.file = file;
		this.startLine = startline;
		this.startColumn = startcolumn;
		this.endLine = endline;
		this.endColumn = endcolumn;
	}

	public String toString() {
		return "{ " + file + ", "  + startLine + ":" + startColumn + ", " + endLine + ":" + endColumn + " }";
	}

	public int getStartLine() {
		return startLine;
	}
	
	public int getStartColumn() {
		return startColumn;
	}
	
	public int getEndLine() {
		return endLine;
	}
	
	public int getEndColumn() {
		return endColumn;
	}
	
}

