/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * This node represents an invalid element in the source code (for example a partial
 * word not fully typed yet).
 * 
 * @author Miklos Magyari
 *
 */
public class InvalidASTNode extends ASTNode implements ILocateableNode, IIncrementallyUpdatable {
	private final String markerMessage;
	private final DiagnosticSeverity severity;
	private Location location = Location.getNullLocation();
	public InvalidASTNode(final String markerMessage, final DiagnosticSeverity severity) {
		this.markerMessage = markerMessage;
		this.severity = severity;
	}

	public InvalidASTNode(final String markerMessage) {
		this(markerMessage, DiagnosticSeverity.Error);
	}
	
	/**
	 * Performs the semantic checking of the node.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 **/
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		switch(severity) {
		case Error:
			location.reportSemanticError(markerMessage);
			break;
		case Warning:
			location.reportSemanticWarning(markerMessage);
			break;
		case Information:
		case Hint:
			// TODO
		}
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		// Do nothing
	}

	@Override
	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	protected boolean memberAccept(ASTVisitor v) {
		return true;
	}
}
