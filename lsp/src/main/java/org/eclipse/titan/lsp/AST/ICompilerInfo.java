/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

/**
 * This is an interface for all nodes, providing information about the compiler environment,
 * especially the currently used TTCN3 version chosen by the user.
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public interface ICompilerInfo {

	/**
	 * Version of TTCN3 supported by the compiler, specified by the year of publication.<br>
	 * Default version is v4.3.1 (published in 2011).
	 **/
	enum Ttcn_version {
		/** Year 2011 */
		Y2011(2011),
		/** Year 2023 */
		Y2023(2023);

		private int version;

		/**
		 * @param year the year when the given version was released 
		 */
		Ttcn_version(final int year) {
			version = year;
		}

		/**
		 * @return the year when the given version was released as {@code int}
		 */
		public int toInt() {
			return version;
		}

		/**
		 * @return the year when the given version was released as {@code String}
		 */
		@Override
		public String toString() {
			return Integer.toString(version);
		}

		/**
		 * Creates an instance of this enum
		 * @param year the year when the given version was released
		 * @return Instance of {@code Ttcn_version}
		 */
		public static Ttcn_version createTtcnVersion(final String year) {
			if (year == null) {
				return Y2011;
			}
			switch(year) {
			case("2023"):
				return Y2023;
			case("2011"):
			default:
				return Y2011;
			}
		}
	}

	/**
	 * Version of TTCN3 supported by the compiler, specified by the year of publication.<br>
	 * Default version is v4.3.1 (published in 2011).
	 * @return version of TTCN3 supported by the compiler
	 **/
	Ttcn_version getTtcnVersion();

}
