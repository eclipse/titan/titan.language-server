/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1;

import org.eclipse.titan.lsp.AST.IGoverned;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Setting;
import org.eclipse.titan.lsp.AST.ASN1.Object.Object_Definition;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Class to represent an ASN.1 Object.
 *
 * @author Kristof Szabados
 */
public abstract class ASN1Object extends Setting implements IObjectSetElement, ICancelBuild, IGoverned {

	protected ObjectClass myGovernor;

	@Override
	/** {@inheritDoc} */
	public final Setting_type getSettingtype() {
		return Setting_type.S_O;
	}

	/** @return a new instance of this AST node */
	public abstract ASN1Object newInstance();

	@Override
	/** {@inheritDoc} */
	public final IObjectSetElement newOseInstance() {
		return newInstance();
	}

	@Override
	/** {@inheritDoc} */
	public final ObjectClass getMyGovernor() {
		return myGovernor;
	}

	public abstract Object_Definition getRefdLast(final CompilationTimeStamp timestamp, IReferenceChain referenceChain);

	/**
	 * Sets the governing ObjectClass of the Object.
	 *
	 * @param governor
	 *                the governor of the Object.
	 * */
	public final void setMyGovernor(final ObjectClass governor) {
		myGovernor = governor;
	}

	@Override
	/** {@inheritDoc} */
	public final void accept(final ObjectSetElement_Visitor visitor) {
		visitor.visitObject(this);
	}

	@Override
	/** {@inheritDoc} */
	public final void setMyScopeOse(final Scope scope) {
		setMyScope(scope);
	}

	/**
	 * Does the semantic checking of the ASN.1 Object.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * */
	public abstract void check(final CompilationTimeStamp timestamp);
}
