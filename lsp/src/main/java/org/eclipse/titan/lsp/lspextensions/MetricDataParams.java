/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.generator.JsonRpcData;

/**
 * @author Csilla Farkas
 **/

@JsonRpcData
public class MetricDataParams {
	private TextDocumentIdentifier identifier;
	private String scope;
	private String targetName;
	private String targetType;
	
	public MetricDataParams(final TextDocumentIdentifier identifier, final String scope, final String targetName, final String targetType) {
		this.identifier = identifier;
		this.scope = scope;
		this.targetName = targetName;
	}

	public TextDocumentIdentifier getIdentifier() {
		return identifier;
	}
	
	public String getScope() {
		return scope;
	}
	
	public String getTargetName() {
		return targetName;
	}
	
	public String getTargetType() {
		return targetType;
	}
}
