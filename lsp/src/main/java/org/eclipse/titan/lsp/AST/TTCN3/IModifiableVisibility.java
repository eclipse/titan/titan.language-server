/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3;

import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;

/**
 * AST elements that can have a visibility modifier 
 * 
 * @author Adam Knapp
 * */
public interface IModifiableVisibility {
	/**
	 * Returns the non-null visibility modifier of this AST element
	 * @return the non-null visibility modifier of this AST element
	 */
	VisibilityModifier getVisibilityModifier();

	/**
	 * Returns the location of the visibility modifier applied to this AST element.
	 * If no visibility modifier is specified, i.e. the default visibility modifier is the valid one,
	 * the {@link NULL_Location} is returned.
	 * @return the location of the visibility modifier applied to this AST element
	 */
	Location getVisibilityModifierLocation();

	/**
	 * Sets the visibility modifier of this AST element with {@link NULL_Location} as location.
	 * @param modifier visibility modifier to set
	 */
	default void setVisibilityModifier(final VisibilityModifier modifier) {
		setVisibilityModifier(modifier, NULL_Location.INSTANCE);
	}

	/**
	 * Sets the visibility modifier and its location of this AST element
	 * @param modifier visibility modifier to set
	 * @param location location of the visibility modifier
	 */
	 void setVisibilityModifier(final VisibilityModifier modifier, final Location location);
}
