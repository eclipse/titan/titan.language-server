/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.Position;

/**
 * @author Adam Delic
 * @author Arpad Lovassy
 */
public class ASTLocationChainVisitor extends ASTVisitor {
	private final List<IVisitableNode> chain = new ArrayList<IVisitableNode>();
	private final Position offset;

	public ASTLocationChainVisitor(final Position offset) {
		this.offset = offset;
	}

	public List<IVisitableNode> getChain() {
		return chain;
	}

	@Override
	/** {@inheritDoc} */
	public int visit(final IVisitableNode node) {
		if (node instanceof ILocateableNode) {
			final Location loc = ((ILocateableNode)node).getLocation();
			if (loc != null && loc.containsPosition(offset)) {
				chain.add(node);
			} else {
				// skip the children, the offset is not inside this node
				return V_SKIP;
			}
		}
		return V_CONTINUE;
	}

	public void printChain() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Node chain for offset ").append(offset).append(" : ");
		boolean first = true;
		for (final IVisitableNode node : chain) {
			if (!first) {
				sb.append(" -> ");
			} else {
				first = false;
			}
			sb.append(node.getClass().getSimpleName());
		}
		TitanLogger.logTrace(sb.toString());
	}

}
