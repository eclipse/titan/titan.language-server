/******************************************************************************

 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.ttcn3parser;

import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.dfa.DFA;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.parsers.AntlrErrorStrategy;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.TITANMarker;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;

/**
 * This class directs the incremental parsing. Stores all information about the nature and size of the damage done to the system, helps in reparsing
 * only the needed part of the file. And also takes care of cleaning and reporting errors inside the damaged area.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public final class TTCN3ReparseUpdater {

	/** Errors from the parser (indicating syntax errors). */
	private List<SyntacticErrorStorage> mErrors;

	/** syntactic warnings and errors created by the parser. */
	private List<TITANMarker> warningsAndErrors;
	/** list of markers marking the locations of unsupported constructs. */
	private List<TITANMarker> unsupportedConstructs;
	/** map of markers marking the locations of unsupported constructs. */
	private Map<Path, List<TITANMarker>> unsupportedConstructMap;

	/** The file being checked. */
	private final Path file;
	/** The string form of the document. */
	private final String code;
	/** The line in which the actual damage starts. */
	private int firstLine;

	/* LSP */
	private Position originalModificationStartPosition;
	private Position originalModificationEndPosition;
	/** The believed start of the interval which might be effected by the modification. */
	private Position modificationStartPosition;
	/** The believed end of the interval which might be effected by the modification. */
	private Position modificationEndPosition;
	private int shift;
	/** sum of lines added with all the modifications */
	private final int allLineShift;
	private final int columnShift;
	
	private boolean isDamageExtended = false; 
	
	/** byte offset of the modified region's start and end positions */
	private int modificationStartOffset;
	private int modificationEndOffset;
	
	// Additional information
	/**
	 * Stores if the name of entity has changed or not. This information can indicate a level higher, that a uniqueness check might be needed
	 */
	private boolean namechanged = false;
	
	private final String lineEnding;
	
	/* true if a document comment is inserted */
	private boolean isDocumentCommentInserted;
	
	/* true if one or more lines or part of a line is commented out */
	private boolean codeCommented;

	public TTCN3ReparseUpdater(final Path file, final String code, final Location modificationLocation, final String text, final int shift) {
		this.file = file;
		this.code = code;
		
		final String inserted = text.replaceAll("\\r|\\n", "").trim();
		if (inserted.startsWith("/**") && inserted.endsWith("*/")) {
			isDocumentCommentInserted = true;
		} else if (inserted.startsWith("//") || inserted.startsWith("/*")) {
			/*
			 * Probably one or more lines or part of a line were commented out.
			 * Maybe this check should be refined.
			 */
			codeCommented = true;
		} else if (modificationLocation.getStartColumn() > 0) {
			/*
			 * Maybe something is commented out. 
			 * This is hard to check for sure, so we play safe.
			 */
			final Location prevCharLocation = new Location(modificationLocation);
			prevCharLocation.setStartColumn(prevCharLocation.getStartColumn() - 1);
			int prevCharOffset = prevCharLocation.getStartPosition().toOffset(code);
			char prevChar = code.charAt(prevCharOffset);
			if (prevChar == '/' && (inserted.startsWith("/") || inserted.startsWith("*"))) {
				codeCommented = true;
			}
		}
		
		originalModificationStartPosition = modificationLocation.getStartPosition();
		modificationStartPosition = modificationLocation.getStartPosition();
		originalModificationEndPosition = modificationLocation.getEndPosition();
		modificationEndPosition = modificationLocation.getEndPosition();
		this.shift = shift;
		unsupportedConstructs = new ArrayList<TITANMarker>();
		unsupportedConstructMap = new ConcurrentHashMap<Path, List<TITANMarker>>();
		
		final Project project = Project.INSTANCE;
		ProjectItem projectItem = project.getProjectItem(file);
		lineEnding = projectItem != null ? projectItem.getLineEnding() : "\n";
		
		final int insertedLines = (int)text.chars().filter(f -> f == '\n').count();
		allLineShift = modificationLocation.getRange().getEnd().getLine() - modificationLocation.getRange().getStart().getLine() + insertedLines;
		if (text.contains(lineEnding)) {
			columnShift = text.substring(text.lastIndexOf(lineEnding)).length();
		} else {
			columnShift = text.length();
		}
		
		calculateOffsets();
	}
	
	private void calculateOffsets() {
		final String[] lines = code.split(lineEnding);
		int offset = 0;
		for (int i = 0; i < modificationEndPosition.getLine() + allLineShift; i++) {
			offset += lines[i].length() + lineEnding.length();
			if (i == modificationStartPosition.getLine() - 1) {
				modificationStartOffset = offset + modificationStartPosition.getCharacter();
			}
		}
		modificationEndOffset = offset + modificationEndPosition.getCharacter();
	}
	
	public void setUnsupportedConstructs(final Map<Path,List<TITANMarker>> unsupportedConstructMap) {
		this.unsupportedConstructMap = unsupportedConstructMap;
		if (unsupportedConstructMap.containsKey(file)) {
			unsupportedConstructs = unsupportedConstructMap.get(file);
		}
	}

	/* LSP */
	
	/**
	 * Returns the LSP Position where the damage starts
	 * @return
	 */
	public final Position getDamageStartPosition() {
		return modificationStartPosition;
	}
	
	/**
	 * Returns the LSP Position where the damage ends
	 * @return
	 */
	public final Position getDamageEndPosition() {
		return modificationEndPosition;
	}
	
	public final boolean envelopsDamage(final Location location) {
		return location.getStartPosition().before(modificationStartPosition) && 
			   //location.getEndPosition().afterOrEquals(modificationEndPosition);
			   location.getEndPosition().after(modificationEndPosition);
	}
	
	public final boolean isDamaged(final Location location) {
		return location.getEndPosition().afterOrEquals(modificationStartPosition) && 
			   modificationEndPosition.afterOrEquals(location.getStartPosition());
	}

	public final boolean isAffected(final Location location) {
		return location.getEndPosition().after(modificationStartPosition);
	}

	public final boolean isAffectedAppended(final Location location) {
		return location.getEndPosition().afterOrEquals(modificationStartPosition);
	}
	
	// only extension on the end shall be allowed
	public final boolean isExtending(final Location location) {
		return location.getEndPosition().equals(modificationStartPosition);
	}
	/* end LSP */
	
	public final String getCode() {
		return code;
	}

	public final int getShift() {
		return shift;
	}
	
	/**
	 * Updates to location of an AST node based on the line and column shift
	 * caused by an edit in a file. Both start and end positions can be affected,
	 * depending on how the location and and the dirty region overlap.
	 * 
	 * @param location of the given node
	 */
	public final void updateLocation(final Location location) {
		if (NULL_Location.INSTANCE == location) {
			return;
		}

		Position offset = location.getStartPosition();
		if (offset.afterOrEquals(originalModificationStartPosition)) {
			location.setStartLine(offset.getLine() + allLineShift);
			if (location.getStartPosition().getLine() == originalModificationEndPosition.getLine()) {
				location.setStartColumn(location.getStartColumn() + columnShift);
			}
		}
		offset = location.getEndPosition();
		if (offset.after(originalModificationStartPosition)) {
			location.setEndLine(offset.getLine() + allLineShift);
			if (location.getEndPosition().getLine() == originalModificationEndPosition.getLine()) {
				location.setEndColumn(location.getEndColumn() + columnShift);
			}
		}
	}

	/**
	 * Checks if the first TTCN-3 lexical token in the substring, that covers the possibly changed interval of the document belongs to a given list of
	 * expected tokens or not.
	 *
	 * @param followSet the possible tokens that can follow the element before the suspected
	 *
	 * @return true if the first lexical token is part of the followset, false otherwise
	 * */
	public boolean startsWithFollow(final List<Integer> followSet) {
		final boolean oopEnabled = Configuration.INSTANCE.getBoolean(Configuration.ENABLE_OOP_EXTENSION, true);
		
		if (followSet.isEmpty()) {
			return false;
		}

		if (code == null) {
			return false;
		}

		final int line = modificationStartPosition.getLine();
		final int column = modificationStartPosition.getCharacter();
		String substring;
		if (code.length() <= modificationEndOffset + shift) {
			substring = code.substring(modificationStartOffset);
		} else {
			substring = code.substring(modificationStartOffset, modificationEndOffset + shift);
		}

		final Reader reader = new StringReader(substring);
		final CharStream charStream = new UnbufferedCharStream(reader);
		final Ttcn3Lexer lexer = new Ttcn3Lexer(charStream);
		lexer.enableOop(oopEnabled);
		lexer.setTokenFactory( new CommonTokenFactory( true ) );
		lexer.setLine( line + 1 );
		lexer.setCharPositionInLine(column);
		lexer.initRootInterval(modificationEndOffset - modificationStartOffset + 1);

		final Token token = lexer.nextToken();
		if (token == null) {
			return false;
		}

		return followSet.contains( token.getType() );
	}

	/**
	 * Checks if the last TTCN-3 lexical token in the substring, that covers the possibly changed interval of the document belongs to a given list of
	 * expected tokens or not.
	 *
	 * @param followSet the possible tokens that can prepend the element before the suspected
	 *
	 * @return true if the first lexical token is part of the followset, false otherwise
	 * */
	public boolean endsWithToken(final List<Integer> followSet) {
		final boolean oopEnabled = Configuration.INSTANCE.getBoolean(Configuration.ENABLE_OOP_EXTENSION, true);
		
		if (followSet.isEmpty()) {
			return false;
		}

		if (code == null) {
			return false;
		}

		final int line = modificationStartPosition.getLine();
		final int column = modificationStartPosition.getCharacter();
		String substring;
		if (code.length() <= modificationEndOffset + shift) {
			substring = code.substring(modificationStartOffset);
		} else {
			substring = code.substring(modificationStartOffset, modificationEndOffset + shift);
		}

		final Reader reader = new StringReader(substring);
		final CharStream charStream = new UnbufferedCharStream(reader);
		final Ttcn3Lexer lexer = new Ttcn3Lexer(charStream);
		lexer.enableOop(oopEnabled);
		lexer.setTokenFactory( new CommonTokenFactory( true ) );
		lexer.setLine( line + 1 );
		lexer.setCharPositionInLine(column);
		lexer.initRootInterval(modificationEndOffset - modificationStartOffset + 1);

		final Token token = lexer.nextToken();
		if (token == null) {
			return false;
		}

		return followSet.contains( token.getType() );
	}

	public final void extendDamagedRegion(final Location location) {
		extendDamagedRegion(
			new Position(location.getStartLine(), location.getStartColumn()),
			new Position(location.getEndLine(), location.getEndColumn()));
	}
	
	public final void extendDamagedRegion(final Position start, final Position end) {
		boolean changed = false;
		if (start.before( modificationStartPosition)) {
			changed = true;
			modificationStartPosition = start;
		}
		if (end.after(modificationEndPosition)) {
			changed = true;
			modificationEndPosition = end;
		}
		if (changed) {
			calculateOffsets();
			isDamageExtended = true;
		}
	}

	public final void extendDamagedRegionTillFileEnd() {
//		modificationEndOffset = code.length() - shift;
	}

	public final void maxDamage() {
		modificationStartOffset = 0;
		modificationEndOffset = code.length() - 1;// - shift;
		final int lines = countLines(code);
		final int lastColumn = code.length() - code.lastIndexOf(lineEnding) - lineEnding.length();
		modificationStartPosition = new Position(0, 0);
		modificationEndPosition = new Position(lines - 1, lastColumn);
	}

	public void setNameChanged(final boolean namechanged) {
		this.namechanged = namechanged;
	}

	public boolean getNameChanged() {
		return namechanged;
	}

	protected int measureIntervalDamage() {
		String substring;
		if (code.length() <= modificationEndOffset + shift) {
			substring = code.substring(modificationStartOffset);
		} else {
			substring = code.substring(modificationStartOffset, modificationEndOffset + shift);
		}

		final int rangeEnd = substring.length();
		int nextPos = 0;
		boolean insideString = false;
		boolean insideSingleComment = false;
		boolean insideMultiComment = false;
		final Stack<String> elements = new Stack<String>();
		int unclosedStarting = 0;
		int unclosedEnding = 0;
		try {
			while (nextPos < rangeEnd) {
				switch (substring.charAt(nextPos)) {
				case '(':
					elements.push("(");
					unclosedStarting++;
					break;
				case ')':
					if (!elements.isEmpty() && "(".equals(elements.peek())) {
						elements.pop();
						unclosedStarting--;
					} else {
						elements.push(")");
						unclosedEnding++;
					}
					break;
				case '[':
					elements.push("[");
					unclosedStarting++;
					break;
				case ']':
					if (!elements.isEmpty() && "[".equals(elements.peek())) {
						elements.pop();
						unclosedStarting--;
					} else {
						elements.push("]");
						unclosedEnding++;
					}
					break;
				case '{':
					elements.push("{");
					unclosedStarting++;
					break;
				case '}':
					if (!elements.isEmpty() && "{".equals(elements.peek())) {
						elements.pop();
						unclosedStarting--;
					} else {
						elements.push("}");
						unclosedEnding++;
					}
					break;
				case '/':
					if (nextPos + 1 < rangeEnd) {
						switch (substring.charAt(nextPos + 1)) {
						case '*':
							insideMultiComment = true;
							nextPos += 2;
							while (nextPos < rangeEnd
									&& ('*' != substring.charAt(nextPos) || nextPos + 1 >= rangeEnd || '/' != substring.charAt(nextPos + 1))) {
								nextPos++;
							}
							if (nextPos < rangeEnd) {
								insideMultiComment = false;
							} else {
								nextPos++;
							}
							break;
						case '/':
							insideSingleComment = true;
							nextPos += 2;
							while (nextPos < rangeEnd && '\n' != substring.charAt(nextPos)) {
								nextPos++;
							}
							if (nextPos < rangeEnd) {
								insideSingleComment = false;
							}
							break;
						default:
							break;
						}
					}
					break;
				case '"':
					insideString = true;
					nextPos++;
					while (nextPos < rangeEnd && ('\"' != substring.charAt(nextPos) || '\"' == substring.charAt(nextPos - 1))) {
						if('\"' != substring.charAt(nextPos)) {
							nextPos++;
						} else if (nextPos + 1 < rangeEnd && '\"' == substring.charAt(nextPos + 1)) {
							nextPos += 2;
						} else {
							break;
						}
					}
					if (nextPos < rangeEnd) {
						insideString = false;
					}
					break;
				case '#':
					insideSingleComment = true;
					nextPos++;
					while (nextPos < rangeEnd && '\n' != substring.charAt(nextPos)) {
						nextPos++;
					}
					if (nextPos < rangeEnd) {
						insideSingleComment = false;
					}
					break;
				default:
					break;
				}
				nextPos++;
			}
		} catch (IndexOutOfBoundsException e) {
			TitanLogger.logError(e);
		}

		if (insideSingleComment || insideMultiComment || insideString) {
			return Integer.MAX_VALUE;
		}

		return Math.max(unclosedStarting, unclosedEnding);
	}

	/**
	 * Specific part of marker handling
	 * FIXME
	 */
	private void reportSpecificSyntaxErrors() {
		if (mErrors != null) {
			//final Location temp = new Location(file, firstLine, modificationStartOffset, modificationEndOffset + shift);
			for (int i = 0; i < mErrors.size(); i++) {
				// LSPFIX ParserMarkerSupport.createOnTheFlySyntacticMarker(file, mErrors.get(i), IMarker.SEVERITY_ERROR, temp);
			}
		}
	}

	public final void reportSyntaxErrors() {
		reportSpecificSyntaxErrors();
		if (warningsAndErrors != null) {
			for (final TITANMarker marker : warningsAndErrors) {
//	LSPFIX			if (file.isAccessible()) {
//					final Location location = new Location(file, marker.getLine(), marker.getOffset(), marker.getEndOffset());
//					location.reportExternalProblem(marker.getMessage(), marker.getSeverity(), GeneralConstants.ONTHEFLY_SYNTACTIC_MARKER);
//				}
			}
		}
		if (unsupportedConstructs != null && !unsupportedConstructs.isEmpty()) {
			final Iterator<TITANMarker> iterator = unsupportedConstructs.iterator();
//			while (iterator.hasNext()) {
//				final TITANMarker marker = iterator.next();
//				if (marker.getOffset() >= modificationEndOffset) {
//					marker.setOffset(marker.getOffset() + shift);
//					marker.setEndOffset(marker.getEndOffset() + shift);
//				}
//			}
			unsupportedConstructMap.put(file, unsupportedConstructs);
		}
	}

	public int parse(final ITTCN3ReparseBase userDefined) {
//		final Position shiftedEndPosition = new Position(modificationEndOffset.getLine() + allLineShift, modificationEndOffset.getCharacter());
//		
//		if (modificationStartPosition == modificationEndOffset + shift) {
//			return 0;
//		}
		mErrors = null;
		warningsAndErrors = null;
		final Iterator<TITANMarker> iterator = unsupportedConstructs.iterator();
//		while (iterator.hasNext()) {
//			final TITANMarker marker = iterator.next();
//			if ((marker.getOffset() > modificationStartPosition && marker.getOffset() <= modificationEndOffset)
//					|| (marker.getEndOffset() > modificationStartPosition && marker.getEndOffset() <= modificationEndOffset)) {
//				iterator.remove();
//			}
//		}

		// LSPFIX MarkerHandler.markAllOnTheFlyMarkersForRemoval(file, modificationStartOffset, modificationEndOffset + shift);

		if (code == null) {
			return Integer.MAX_VALUE;
		}
		
		AstSemanticHighlighting.clearSemanticTokensAndModifiers(file, modificationStartPosition, modificationEndPosition);

		final int line = modificationStartPosition.getLine();
		final String substring = code.substring(modificationStartOffset);
		final Configuration config = Configuration.INSTANCE;
		final boolean realtimeEnabled = config.getBoolean(Configuration.ENABLE_REALTIME_EXTENSION, false);
		final boolean oopEnabled = config.getBoolean(Configuration.ENABLE_OOP_EXTENSION, true);

		final Reader reader = new StringReader(substring);
		
		final CharStream charStream = new UnbufferedCharStream(reader);
		final Ttcn3Lexer lexer = new Ttcn3Lexer(charStream);
		lexer.enableOop(oopEnabled);
		lexer.setActualFile(file.toFile());
		lexer.setTokenFactory( new CommonTokenFactory( true ) );
		lexer.initRootInterval(modificationEndOffset - modificationStartOffset + 1);
		if (realtimeEnabled) {
			lexer.enableRealtime();
		}

		// lexer and parser listener
		final TitanErrorListener lexerListener = new TitanErrorListener(file.toFile());
		// remove ConsoleErrorListener
		lexer.removeErrorListeners();
		lexer.addErrorListener(lexerListener);

		// 1. Previously it was UnbufferedTokenStream(lexer), but it was changed to BufferedTokenStream, because UnbufferedTokenStream seems to be unusable. It is an ANTLR 4 bug.
		// Read this: https://groups.google.com/forum/#!topic/antlr-discussion/gsAu-6d3pKU
		// pr_PatternChunk[StringBuilder builder, boolean[] uni]:
		//   $builder.append($v.text); <-- exception is thrown here: java.lang.UnsupportedOperationException: interval 85..85 not in token buffer window: 86..341
		// 2. Changed from BufferedTokenStream to CommonTokenStream, otherwise tokens with "-> channel(HIDDEN)" are not filtered out in lexer.
		final CommonTokenStream tokenStream = new CommonTokenStream( lexer );

		final Ttcn3Reparser parser = new Ttcn3Reparser( tokenStream );
		parser.setErrorHandler(new AntlrErrorStrategy());
		ParserUtilities.setBuildParseTree( parser );

		lexer.setActualFile(file.toFile());
		parser.setActualFile(file.toFile());
//		parser.setProject(file.getProject());
		parser.setOffset( modificationStartOffset);
		parser.setOffsetPosition(modificationStartPosition);
		parser.setLine( line + 1 );

		parser.removeErrorListeners();
		final TitanErrorListener parserListener = new TitanErrorListener(file.toFile());
		parser.addErrorListener( parserListener );

		// This is added because of the following ANTLR 4 bug:
		// Memory Leak in PredictionContextCache #499
		// https://github.com/antlr/antlr4/issues/499
		final DFA[] decisionToDFA = parser.getInterpreter().decisionToDFA;
		parser.setInterpreter(new ParserATNSimulator(parser, parser.getATN(), decisionToDFA, new PredictionContextCache()));

		//try SLL mode
		try {
			parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
	
			userDefined.reparse(parser);
			mErrors = lexerListener.getErrorsStored();
			mErrors.addAll( parserListener.getErrorsStored() );
			mErrors = parserListener.getErrorsStored();
			warningsAndErrors = parser.getWarningsAndErrors();
			unsupportedConstructs.addAll(parser.getUnsupportedConstructs());
		} catch (RecognitionException e) {
			// quit
		}

		if (!warningsAndErrors.isEmpty() || !mErrors.isEmpty()) {
			//SLL mode might have failed, try LL mode
			try {
				final CharStream charStream2 = new UnbufferedCharStream( reader );
				lexer.setInputStream(charStream2);
				//lexer.reset();
				parser.reset();
				parserListener.reset();
				parser.getInterpreter().setPredictionMode(PredictionMode.LL);
				userDefined.reparse(parser);
				mErrors = parserListener.getErrorsStored();
				warningsAndErrors = parser.getWarningsAndErrors();
				unsupportedConstructs.addAll(parser.getUnsupportedConstructs());

			} catch(RecognitionException e) {

			}
		}

		int result = measureIntervalDamage();
		if(!parser.isErrorListEmpty()){
			++result;
		}

		return result;
	}
	
	public int getAllLineShift() {
		return allLineShift;
	}
	
	public int getColumnShift() {
		return columnShift;
	}

	public Position getOriginalModificationStartPosition() {
		return originalModificationStartPosition;
	}

	public Position getOriginalModificationEndPosition() {
		return originalModificationEndPosition;
	}
	
	public int countLines(String str) {
	    if(str == null || str.isEmpty())
	    {
	        return 0;
	    }
	    int lines = 1;
	    int pos = 0;
	    while ((pos = str.indexOf(lineEnding, pos) + 1) != 0) {
	        lines++;
	    }
	    return lines;
	}
	
	public boolean documentCommentInserted() {
		return isDocumentCommentInserted;
	}

	public boolean isCodeCommented() {
		return codeCommented;
	}
}

