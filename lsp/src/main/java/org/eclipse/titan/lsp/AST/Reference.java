/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.ISetting.Setting_type;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.ASN1.ASN1Assignment;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ActualParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.IParameterisedAssignment;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticModifier;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * The Reference class represents a general reference used to refer to an element
 * in the source code.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public class Reference extends ASTNode implements ILocateableNode, IIncrementallyUpdatable, IReferencingElement {
	private static final String STRINGELEMENTUNUSABLE = "Reference to a string element of type `{0}'' cannot be used in this context";
	private static final String VARIABLEXPECTED = "Reference to a variable or value parameter was expected instead of {0}";
	private static final String ALTSTEPEXPECTED = "Reference to an altstep was expected in the argument instead of {0}";
	private static final String THISORSUPEROUTSIDECLASS = "`{0}'' reference is only valid inside class bodies";

	private static final String FULLNAMEPART = ".<sub_reference";

	public static final String COMPONENTEXPECTED = "component type expected";
	public static final String CLASSEXPECTED = "class type expected";
	public static final String TYPEEXPECTED = "Type reference expected";
	public static final String ASN1SETTINGEXPECTED = "Reference to ASN.1 setting expected";
	
	private static final String INVALIDVALUEREFERENCE = "Reference to `value` is only allowed inside a dynamic template or property setter";
	private static final String VALUENOTSETTER = "Reference to `value` is not allowed in a property getter";
	private static final String DEFAULTEXPECTED = "Reference to a default value was expected in the argument instead of {0}";
	
	public enum Ref_Type {
		/** basic reference (not related to a class scope or a dynamic template) */
		REF_BASIC,
		/** reference to the current class object */
		REF_THIS,
		/** reference to the superclass */
		REF_SUPER,
		/** reference to the value being matched in a dynamic template or to be used in an OOP property */
		REF_VALUE
	}
	
	 public enum Ref_Side {
		/** indicates that an error related to the reference side or visibility has already been reported */
	    REF_SIDE_ERROR,
	    /** the reference is on the right-hand-side (i.e. the referenced assignment is read) */
	    RHS_REF, 
	    /** the reference is on the left-hand-side (i.e. the referenced assignment is written) */
	    LHS_REF,
	    /** the reference is on both sides (i.e. the referenced assignment is read and written) */
	    BOTH_SIDE_REF
	}
	
	/**
	 * Module identifier. Might be null. In that case it means, that we were
	 * not able to decide if the reference has a module identifier or not
	 * */
	protected Identifier modid;

	/**
	 * The list of sub-references.
	 * <p>
	 * The first element might be the id of the module until the module id
	 * detection is not executed.
	 * */
	protected ArrayList<ISubReference> subReferences;

	private boolean refersToStringElement = false;

	/**
	 * Stores weather we have already tried to detect the module id.
	 * */
	protected boolean detectedModuleId = false;

	/**
	 * indicates if this reference has already been found erroneous in the
	 * actual checking cycle.
	 */
	private boolean isErroneous;
	/**
	 * The assignment referred to by this reference.
	 * */
	protected Assignment referredAssignment;
	protected CompilationTimeStamp lastTimeChecked;

	/**
	 * Stores whether this reference is used on the left hand side of an
	 * assignment or not.
	 */
	private boolean usedOnLeftSide = false;

	/**
	 * Stores whether this reference is used directly in an isbound call or
	 * not.
	 */
	private boolean usedInIsbound = false;
	
	private Ref_Type reftype = Ref_Type.REF_BASIC;

	public Reference(final Identifier modid) {
		this.modid = modid;
		detectedModuleId = modid != null;
		subReferences = new ArrayList<ISubReference>();
	}

	public Reference(final Identifier modid, Ref_Type reftype) {
		this(null);
		this.reftype = reftype;
	}
	
	public Reference(final Identifier modid, final List<ISubReference> subReferences) {
		this.modid = modid;
		detectedModuleId = modid != null;
		this.subReferences = new ArrayList<ISubReference>(subReferences);
		this.subReferences.trimToSize();

		for (int i = 0; i < subReferences.size(); i++) {
			subReferences.get(i).setFullNameParent(this);
		}
	}
	
	public Reference(final Identifier modid, Ref_Type reftype, final List<ISubReference> subReferences) {
		this(modid, subReferences);
		this.reftype = reftype;
	}
	
	public Reference(final Identifier modid, final Identifier referencedId) {
		final FieldSubReference fieldsubref = new FieldSubReference(referencedId);
		final List<ISubReference> subrefs = new ArrayList<>(1);
		subrefs.add(fieldsubref);
				
		this.modid = modid;
		detectedModuleId = modid != null;
		this.subReferences = new ArrayList<ISubReference>(subrefs);
		this.subReferences.trimToSize();

		for (int i = 0; i < subReferences.size(); i++) {
			subReferences.get(i).setFullNameParent(this);
		}
	}

	/** @return a new instance of this reference */
	public Reference newInstance() {
		return new Reference(modid, reftype, subReferences);
	}

	/**
	 * @return the assignment this reference was last evaluated to point to.
	 *         Might be outdated information.
	 * */
	public Assignment getAssOld() {
		return referredAssignment;
	}

	/**
	 * Sets the scope of the base reference without setting the scope of the
	 * sub references.
	 *
	 * @param scope
	 *                the scope to set.
	 * */
	public void setBaseScope(final Scope scope) {
		super.setMyScope(scope);
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(Scope scope) {
		super.setMyScope(scope);
		subReferences.trimToSize();
		for (int i = 0; i < subReferences.size(); i++) {
			subReferences.get(i).setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0; i < subReferences.size(); i++) {
			if (child == subReferences.get(i)) {
				return builder.append(FULLNAMEPART).append(String.valueOf(i + 1)).append(INamedNode.MORETHAN);
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		// Do nothing
	}

	// Location is optimized not to store an object at it is not needed
	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		Location temp;
		if (modid != null) {
			temp = new Location(modid.getLocation());
		} else if (!subReferences.isEmpty()) {
			temp = new Location(subReferences.get(0).getLocation());
		} else {
			return NULL_Location.getInstance();
		}

		if (!subReferences.isEmpty()) {
			for (int i = subReferences.size() - 1; i >= 0; i--) {
				if (!NULL_Location.getInstance().equals(subReferences.get(i).getLocation())) {
					temp.setEndPosition(subReferences.get(i).getLocation().getEndPosition());
					break;
				}
			}
		}

		return temp;
	}

	public final CompilationTimeStamp getLastTimeChecked() {
		return lastTimeChecked;
	}

	/**
	 * Checks if the reference was evaluated to be erroneous in this time
	 * frame.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @return true if the setting is erroneous, or false otherwise
	 * */
	public final boolean getIsErroneous(final CompilationTimeStamp timestamp) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return isErroneous;
		}

		return false;
	}

	/**
	 * Sets the erroneousness of the setting.
	 *
	 * @param isErroneous
	 *                set the erroneousness property of the references.
	 * */
	public void setIsErroneous(final boolean isErroneous) {
		this.isErroneous = isErroneous;
	}

	/**
	 * @return whether this reference is used on the left hand side of an
	 *         assignment or not.
	 * */
	public boolean getUsedOnLeftHandSide() {
		return usedOnLeftSide;
	}

	/**
	 * Sets this reference to be used on the left hand side of an
	 * assignment.
	 */
	public void setUsedOnLeftHandSide() {
		usedOnLeftSide = true;
	}

	/**
	 * @return whether this reference is used directly in an isbound call or
	 *         not.
	 * */
	public boolean getUsedInIsbound() {
		return usedInIsbound;
	}

	/** Sets that this reference is used directly inside an isbound call */
	public void setUsedInIsbound() {
		usedInIsbound = true;
	}

	/**
	 * Detects and returns the module identifier.
	 *
	 * @return the module identifier, might be null if the reference did not
	 *         contain one.
	 * */
	public Identifier getModuleIdentifier() {
		detectModid();
		return modid;
	}

	/**
	 * Returns the identifier of the reference.
	 * <p>
	 * Before returning, the module identifier detection is run. After the
	 * module identifier detection, the first sub-reference must be the
	 * identifier of an assignment (if the sub-reference exists).
	 *
	 * @return the identifier contained in this reference
	 * */
	public Identifier getId() {
		detectModid();

		if (!subReferences.isEmpty()) {
			return subReferences.get(0).getId();
		}

		return null;
	}

	/**
	 * @return the sub-references of this reference, with any previous
	 *         checks.
	 * */
	public List<ISubReference> getSubreferences() {
		return subReferences;
	}

	/**
	 * Collects the sub-references of the reference.
	 * <p>
	 * It is important that the name of the reference is the first member of
	 * the list.
	 *
	 * @param from
	 *                the first index to include.
	 * @param till
	 *                the last index to include.
	 * @return the sub-references of this reference, with any previous
	 *         checks.
	 * */
	public List<ISubReference> getSubreferences(final int from, final int till) {
		final List<ISubReference> result = new ArrayList<ISubReference>();

		final int size = Math.min(subReferences.size() - 1, till);
		for (int i = Math.max(0, from); i <= size; i++) {
			result.add(subReferences.get(i));
		}

		return result;
	}

	/**
	 * Adds a sub-reference to the and of the list of sub-references.
	 *
	 * @param subReference
	 *                the sub-reference to be added.
	 * */
	public void addSubReference(final ISubReference subReference) {
		subReferences.add(subReference);
		subReference.setFullNameParent(this);
	}

	/**
	 * Deletes and returns the last sub-reference.
	 *
	 * @return the last sub-reference before the deletion, or null if there
	 *         was none.
	 * */
	public ISubReference removeLastSubReference() {
		if (subReferences.isEmpty()) {
			return null;
		}

		final ISubReference result = subReferences.remove(subReferences.size() - 1);

		if (subReferences.isEmpty() && modid != null) {
			subReferences.add(new FieldSubReference(modid));
			modid = null;
			detectedModuleId = false;
		}

		return result;
	}

	/**
	 * @return true if the reference references a string element, false
	 *         otherwise.
	 * */
	public boolean refersToStringElement() {
		return refersToStringElement;
	}

	/** set the reference to be referencing string elements. */
	public void setStringElementReferencing() {
		refersToStringElement = true;
	}

	/**
	 * clears previous information on whether the reference to be
	 * referencing string elements or not.
	 */
	public void clearStringElementReferencing() {
		refersToStringElement = false;
	}

	/**
	 * Clears the cache of this reference.
	 **/
	public void clear() {
		referredAssignment = null;
		lastTimeChecked = null;
	}

	/**
	 * Tries to detect if the list of sub-references contains the module
	 * identifier.
	 * <p>
	 * In parsing time the module identifier can only be detected /
	 * separated from sub-references, if the reference contains "objid"s.
	 * */
	public void detectModid() {
		if (detectedModuleId || modid != null) {
			return;
		}

		Identifier firstId = null;
		if (!subReferences.isEmpty() && Subreference_type.fieldSubReference.equals(subReferences.get(0).getReferenceType())) {
			firstId = subReferences.get(0).getId();
			if (subReferences.size() > 1 && !Subreference_type.arraySubReference.equals(subReferences.get(1).getReferenceType())) {
				// there are 3 possible situations:
				// 1. first_id points to a local definition
				// (this has the priority)
				// modid: 0, id: first_id
				// 2. first_id points to an imported module
				// (trivial case)
				// modid: first_id, id: second_id
				// 3. none of the above (first_id might be an
				// imported symbol)
				// modid: 0, id: first_id
				// Note: Rule 1 has the priority because it can
				// be overridden using
				// the notation <id>.objid { ... }.<id> but
				// there is no work-around in the reverse way.
				if (myScope != null && !myScope.hasAssignmentWithId(CompilationTimeStamp.getBaseTimestamp(), firstId)
						&& myScope.isValidModuleId(firstId)) {
					// rule 1 is not fulfilled, but rule 2
					// is fulfilled
					modid = firstId;
					subReferences.remove(0);
				}
			}
		}

		detectedModuleId = true;
	}

	/**
	 * IDentifies and returns the referred setting.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @return the setting referenced or an Error_Setting instance in case
	 *         of an error
	 * */
	public ISetting getRefdSetting(final CompilationTimeStamp timestamp) {
		final Assignment assignment = getRefdAssignment(timestamp, true);
		if (assignment != null) {
			return assignment.getSetting(timestamp);
		}

		return new Error_Setting();
	}

	/**
	 * Detects and returns the referred assignment.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param checkParameterList
	 *                whether the parameter list of the reference should be
	 *                checked or not.
	 *
	 * @return the assignment referred by this reference, or null if not
	 *         found.
	 * */
	public Assignment getRefdAssignment(final CompilationTimeStamp timestamp, final boolean checkParameterList) {
		return getRefdAssignment(timestamp, checkParameterList, null);
	}
	
	public Assignment getRefdAssignment(final CompilationTimeStamp timestamp, final boolean checkParameterList, final IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp) && !checkParameterList) {
			return referredAssignment;
		}

		lastTimeChecked = timestamp;

		final StatementBlock statementBlock = myScope != null ? myScope.getStatementBlockScope() : null;
		if (reftype == Ref_Type.REF_VALUE) {
			if (statementBlock != null && statementBlock.isInDynamicTemplate()) {
				referredAssignment = statementBlock.getDynamicTemplate().getDynamicFormalParameter();
			} else if (myScope != null && myScope.isInSetterScope()) {
				referredAssignment = myScope.getScopeSetter().getFormalParameter();
			} else {				
				getLocation().reportSemanticError(INVALIDVALUEREFERENCE);
				isErroneous = true;
				return null;
			}
		}
		
		if (myScope == null || getId() == null) {
			return null;
		}

		final boolean newChain = null == referenceChain;
		IReferenceChain tempReferenceChain;
		if (newChain) {
			tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		} else {
			tempReferenceChain = referenceChain;
		}

		isErroneous = false;
		detectedModuleId = false;
		detectModid();

		if (reftype == Ref_Type.REF_THIS || reftype == Ref_Type.REF_SUPER) {
			Scope s = myScope;
			while (s != null && s instanceof StatementBlock || s instanceof NamedBridgeScope || s instanceof FormalParameterList) {
				s = s.getParentScope();
			}
			if (s == null) {
				return null;
			}
			if (! (s instanceof ClassTypeBody)) {
				getLocation().reportSemanticError(MessageFormat.format(THISORSUPEROUTSIDECLASS, reftype == Ref_Type.REF_THIS ? "this" : "super"));
				setIsErroneous(true);
				return null;
			}
			if (reftype == Ref_Type.REF_SUPER) {
				final ClassTypeBody body = (ClassTypeBody)s;
				referredAssignment = body.getAssFromParents(timestamp, this);
			}
			if (reftype == Ref_Type.REF_THIS) {
				referredAssignment = s.getAssBySRef(timestamp, this, referenceChain);
			}
		} else if (referredAssignment == null) {
			referredAssignment = myScope.getAssBySRef(timestamp, this, referenceChain);
		}
		if (referredAssignment == null) {
			isErroneous = true;
			return referredAssignment;
		}

		referredAssignment.check(timestamp, tempReferenceChain);

		if (referredAssignment instanceof Definition) {
			final String referingModuleName = getMyScope().getModuleScope().getName();
			if (!((Definition) referredAssignment).referingHere.contains(referingModuleName)) {
				((Definition) referredAssignment).referingHere.add(referingModuleName);
			}
		}

		if (checkParameterList) {
			if (referredAssignment.getAssignmentType() == Assignment_type.A_TYPE) {
				if (!subReferences.isEmpty() && subReferences.get(0) instanceof ParameterisedSubReference) {
					final Def_Type defType = (Def_Type)referredAssignment;
					final Type type = defType.getType(timestamp);
					if (type.getTypetype() == Type_type.TYPE_CLASS) {
						referredAssignment = type.getClassType().getConstructor(timestamp);
						if (referredAssignment == null) {
							return null;
						}
					}
				}
			}
			FormalParameterList formalParameterList = null;

			if (referredAssignment instanceof IParameterisedAssignment) {
				formalParameterList = ((IParameterisedAssignment) referredAssignment).getFormalParameterList();
			}

			if (formalParameterList == null) {
				if (!subReferences.isEmpty() && subReferences.get(0) instanceof ParameterisedSubReference) {
					final String message = MessageFormat.format("The referenced {0} cannot have actual parameters",
							referredAssignment.getDescription());
					getLocation().reportSemanticError(message);
					setIsErroneous(true);
				}
			} else if (!subReferences.isEmpty()) {
				final ISubReference firstSubReference = subReferences.get(0);
				if (firstSubReference instanceof ParameterisedSubReference) {
					formalParameterList.check(timestamp, referredAssignment.getAssignmentType());
					isErroneous = ((ParameterisedSubReference) firstSubReference).checkParameters(timestamp,
							formalParameterList);
				} else {
					// if it is not a parameterless
					// template reference pointing
					// to a template having formal
					// parameters, where each has
					// default values
					if (!Assignment.Assignment_type.A_TEMPLATE.semanticallyEquals(referredAssignment.getAssignmentType())
							|| !formalParameterList.hasOnlyDefaultValues()) {
						final String message = MessageFormat.format(
								"Reference to parameterized definition `{0}'' without actual parameter list",
								referredAssignment.getIdentifier().getDisplayName());
						getLocation().reportSemanticError(message);
						setIsErroneous(true);
					}
				}
			}
		}

		referredAssignment.addReference(this);
		return referredAssignment;
	}

	/**
	 * Returns the referenced declaration. Referenced by the given
	 * sub-reference. If the parameter is null, the module declaration will
	 * be returned.
	 *
	 * @param subReference
	 * @return The referenced declaration
	 */
	public IDeclaration getReferencedDeclaration(final ISubReference subReference) {
		final Assignment ass = getRefdAssignment(CompilationTimeStamp.getBaseTimestamp(), false);
		if (ass == null) {
			return null;
		}

		if (subReference == null) {
			return IDeclaration.createInstance(ass.getMyScope().getModuleScope());
		}

		if (subReferences.size() == 1 || subReferences.get(0) == subReference) {
			return IDeclaration.createInstance(ass);
		}

		final IType assignmentType = ass.getType(CompilationTimeStamp.getBaseTimestamp());
		if ( assignmentType == null ) {
			return null;
		}
		final IType type = assignmentType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());

		if (type instanceof IReferenceableElement) {
			final IReferenceableElement iTypeWithComponents = (IReferenceableElement) type;
			return iTypeWithComponents.resolveReference(this, 1, subReference);
		}

		return IDeclaration.createInstance(ass);
	}

	/**
	 * Checks and returns the type of the component referred to by this
	 * reference.
	 * <p>
	 * This is used to detect the type of "runs on" and "system" clause
	 * elements. In any other case a semantic error will be reported.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return the type of the referred component or null in case of
	 *         problems.
	 * */
	public final Component_Type chkComponentypeReference(final CompilationTimeStamp timestamp) {
		final Assignment assignment = getRefdAssignment(timestamp, true);
		if (assignment != null) {
			if (Assignment_type.A_TYPE.semanticallyEquals(assignment.getAssignmentType())) {
				IType type = assignment.getType(timestamp);
				if (type != null) {
					type = type.getTypeRefdLast(timestamp);
					if (type != null && !type.getIsErroneous(timestamp)) {
						switch (type.getTypetype()) {
						case TYPE_COMPONENT:
							return (Component_Type) type;
						case TYPE_REFERENCED:
							return null;
						default:
							getLocation().reportSemanticError(COMPONENTEXPECTED);
							setIsErroneous(true);
							break;
						}
					}
				}
			} else {
				getLocation().reportSemanticError(TYPEEXPECTED);
				setIsErroneous(true);
			}
		}
		return null;
	}
	
	public final Class_Type chkClassReference(final CompilationTimeStamp timestamp) {
		final Assignment assignment = getRefdAssignment(timestamp, true);
		if (assignment != null) {
			if (Assignment_type.A_TYPE.semanticallyEquals(assignment.getAssignmentType())) {
				IType type = assignment.getType(timestamp);
				if (type != null) {
					type = type.getTypeRefdLast(timestamp);
					if (type != null && !type.getIsErroneous(timestamp)) {
						switch (type.getTypetype()) {
						case TYPE_CLASS:
							return (Class_Type) type;
						default:
							getLocation().reportSemanticError(CLASSEXPECTED);
							setIsErroneous(true);
							break;
						}
					}
				}
			} else {
				getLocation().reportSemanticError(TYPEEXPECTED);
				setIsErroneous(true);
			}
		}
		return null;
	}

	/**
	 * Checks and returns the type of the variable referred to by this
	 * reference.
	 * <p>
	 * This is used to detect the type of value redirects.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return the type of the referred variable or null in case of
	 *         problems.
	 * */
	public IType checkVariableReference(final CompilationTimeStamp timestamp) {
		final Assignment assignment = getRefdAssignment(timestamp, true);
		if (assignment == null) {
			return null;
		}

		IType type;
		switch (assignment.getAssignmentType()) {
		case A_PAR_VAL_IN:
			((FormalParameter) assignment).useAsLValue(this);
			type = ((FormalParameter) assignment).getType(timestamp);
			((FormalParameter) assignment).setWritten();
			((FormalParameter) assignment).addReference(this);
			break;
		case A_PAR_VAL:
		case A_PAR_VAL_OUT:
		case A_PAR_VAL_INOUT:
			type = ((FormalParameter) assignment).getType(timestamp);
			((FormalParameter) assignment).setWritten();
			((FormalParameter) assignment).addReference(this);
			break;
		case A_VAR:
			type = ((Def_Var) assignment).getType(timestamp);
			((Def_Var) assignment).setWritten();
			((Def_Var) assignment).addReference(this);
			break;
		default:
			getLocation().reportSemanticError(MessageFormat.format(VARIABLEXPECTED, assignment.getDescription()));
			setIsErroneous(true);
			return null;
		}

		final IType result = type.getFieldType(timestamp, this, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null, false);
		if (result != null && subReferences != null && refersToStringElement()) {
			getLocation().reportSemanticError(MessageFormat.format(STRINGELEMENTUNUSABLE, result.getTypename()));
			setIsErroneous(true);
		}

		return result;
	}

	/**
	 * Checks if a reference is parameterized.
	 * @return
	 */
	private boolean hasParameters() {
		return !subReferences.isEmpty() && 
			Subreference_type.parameterisedSubReference.equals(subReferences.get(0).getReferenceType());
		}
	
	/**
	 * Checks whether this is a correct activation reference.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return true if the altstep reference is correct, false otherwise.
	 * */
	public final boolean checkActivateArgument(final CompilationTimeStamp timestamp) {
		final Assignment assignment = getRefdAssignment(timestamp, true);
		if (assignment == null) {
			return false;
		}

		if (!hasParameters()) {
			switch (assignment.getAssignmentType()) {
			case A_CONST:
			case A_VAR:
			case A_MODULEPAR:
			case A_PAR_VAL:
			case A_PAR_VAL_IN:
			case A_PAR_VAL_INOUT:
			case A_PAR_VAL_OUT:
				if (assignment.getType(timestamp).getTypeRefdLast(timestamp).getTypetype() == Type_type.TYPE_DEFAULT) {
					return true;
				}
			default:
				assignment.getType(timestamp).getTypeRefdLast(timestamp);
				break;
			}
			getLocation().reportSemanticError(
				MessageFormat.format(DEFAULTEXPECTED, assignment.getDescription()));
			return false;
		}
		
		if (!Assignment_type.A_ALTSTEP.semanticallyEquals(assignment.getAssignmentType())) {
			getLocation().reportSemanticError(MessageFormat.format(ALTSTEPEXPECTED, assignment.getDescription()));
			setIsErroneous(true);
			return false;
		}

		if (myScope != null) {
			myScope.checkRunsOnScope(timestamp, assignment, this, "activate");
		}

		final ActualParameterList actualParameters = ((ParameterisedSubReference) subReferences.get(0)).getActualParameters();
		final FormalParameterList formalParameterList = ((Def_Altstep) assignment).getFormalParameterList();

		if (formalParameterList != null) {
			return formalParameterList.checkActivateArgument(timestamp, actualParameters, assignment.getDescription());
		}

		return false;
	}

	/**
	 * Checks if the reference has unfoldable index sub-references.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return true if the reference contains unfoldable index
	 *         sub-references, false otherwise.
	 * */
	public final boolean hasUnfoldableIndexSubReference(final CompilationTimeStamp timestamp) {
		for (final ISubReference subReference: subReferences) {
			if (Subreference_type.arraySubReference.equals(subReference.getReferenceType())) {
				IValue value = ((ArraySubReference) subReference).getValue();
				if (value != null) {
					value = value.setLoweridToReference(timestamp);
					if (value.isUnfoldable(timestamp)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Checks if the reference is actually refering to a setting of the
	 * provided type, or not.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param settingType
	 *                the setting type to check the reference against.
	 * @param referenceChain
	 *                a referencechain to detect cyclic references.
	 *
	 * @return true if the reference refers to a setting of the provided
	 *         kind, false otherwise.
	 * */
	public boolean refersToSettingType(final CompilationTimeStamp timestamp, final Setting_type settingType, final IReferenceChain referenceChain) {
		if (myScope == null) {
			return Setting_type.S_ERROR.equals(settingType);
		}

		final Assignment assignment = getRefdAssignment(timestamp, true);

		if (assignment == null) {
			return false;
		}

		if (!(assignment instanceof ASN1Assignment)) {
			getLocation().reportSemanticError(ASN1SETTINGEXPECTED);
			setIsErroneous(true);
			return false;
		}

		final ASN1Assignment asnAssignment = (ASN1Assignment) assignment;

		switch (settingType) {
		case S_OC:
			return asnAssignment.isAssignmentType(timestamp, Assignment_type.A_OC, referenceChain);
		case S_T:
			return asnAssignment.isAssignmentType(timestamp, Assignment_type.A_TYPE, referenceChain);
		case S_O:
			return asnAssignment.isAssignmentType(timestamp, Assignment_type.A_OBJECT, referenceChain);
		case S_V:
			return asnAssignment.isAssignmentType(timestamp, Assignment_type.A_CONST, referenceChain);
		case S_OS:
			return asnAssignment.isAssignmentType(timestamp, Assignment_type.A_OS, referenceChain);
		case S_VS:
			return asnAssignment.isAssignmentType(timestamp, Assignment_type.A_VS, referenceChain);
		case S_ERROR:
			return asnAssignment.getIsErroneous();
		default:
			// FATAL_ERROR
			return false;
		}
	}

	@Override
	/** {@inheritDoc} */
	public final String toString() {
		return getDisplayName();
	}

	public String getDisplayName() {
		final StringBuilder builder = new StringBuilder();
		if (modid != null) {
			builder.append(modid.getDisplayName());
		}
		for (int i = 0; i < subReferences.size(); i++) {
			subReferences.get(i).appendDisplayName(builder);
		}
		return builder.toString();
	}

	/**
	 * Handles the incremental parsing of this reference.
	 *
	 * @param reparser
	 *                the parser doing the incremental parsing.
	 * @param isDamaged
	 *                true if the location contains the damaged area, false
	 *                if only its' location needs to be updated.
	 * */
	@Override
	public final void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		if (modid != null) {
			reparser.updateLocation(modid.getLocation());
		}

		for (final ISubReference subreference : subReferences) {
			subreference.updateSyntax(reparser, false);
			reparser.updateLocation(subreference.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (int i = 0; i < subReferences.size(); i++) {
			subReferences.get(i).findReferences(referenceFinder, foundIdentifiers);
		}

		if (referredAssignment == null) {
			return;
		}

		if (referenceFinder.fieldId == null) {
			// we are searching for the assignment itself
			if (referenceFinder.assignment != referredAssignment) {
				return;
			}
			foundIdentifiers.add(new Hit(getId(), this));
		} else {
			// we are searching for a field of a type
			final IType t = referredAssignment.getType(CompilationTimeStamp.getBaseTimestamp());
			if (t == null) {
				return;
			}

			final List<IType> typeArray = new ArrayList<IType>();
			final boolean success = t.getFieldTypesAsArray(this, 1, typeArray);
			if (!success) {
				// TODO: maybe a partially erroneous reference could be searched too
				return;
			}
			//TODO: subReferences.size()>0 is just temporary. Rethink if it is correct or or not
			if (!subReferences.isEmpty() && subReferences.size() != typeArray.size() + 1) {
				TitanLogger.logFatal();
				return;
			}
			for (int i = 1; i < subReferences.size(); i++) {
				if (typeArray.get(i - 1) == referenceFinder.type && !(subReferences.get(i) instanceof ArraySubReference)
						&& subReferences.get(i).getId().equals(referenceFinder.fieldId)) {
					foundIdentifiers.add(new Hit(subReferences.get(i).getId()));
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (modid != null && !modid.accept(v)) {
			return false;
		}

		if (subReferences != null) {
			for (int i = 0; i < subReferences.size(); i++) {
				if (!subReferences.get(i).accept(v)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IDeclaration getDeclaration() {
		return getReferencedDeclaration(null);
	}
	
	public Ref_Type getReferenceType() {
		return reftype;
	}
	
	public void setReferenceType(Ref_Type type) {
		reftype = type;
	}
	
	@Override
	public void setSemanticInformation() {
		if (referredAssignment instanceof Definition && getId() != null) {
			final Definition definition = (Definition)referredAssignment;
			final Location location = getId().getLocation();
			switch (definition.getAssignmentType()) {
			case A_ALTSTEP:
				AstSemanticHighlighting.addSemanticToken(location, SemanticType.Function);
				break;
			case A_TEMPLATE:
				AstSemanticHighlighting.addSemanticToken(location, SemanticType.Type);
				break;
			case A_CONST:
				AstSemanticHighlighting.addSemanticTokenAndModifier(location, SemanticType.Variable, SemanticModifier.Readonly);
				break;
			case A_VAR:
				AstSemanticHighlighting.addSemanticToken(location, SemanticType.Variable);
				break;
			case A_TYPE:
				AstSemanticHighlighting.addSemanticToken(location, SemanticType.Type);
				break;
			case A_PAR_PORT:
			case A_PAR_TEMP_IN:
			case A_PAR_TEMP_INOUT:
			case A_PAR_TEMP_OUT:
			case A_PAR_TIMER:
			case A_PAR_VAL:
			case A_PAR_VAL_IN:
			case A_PAR_VAL_INOUT:
			case A_PAR_VAL_OUT:
				AstSemanticHighlighting.addSemanticToken(location, SemanticType.Parameter);
				break;
			case A_FUNCTION:
			case A_FUNCTION_RTEMP:
			case A_FUNCTION_RVAL:
			case A_EXT_FUNCTION:
			case A_EXT_FUNCTION_RTEMP:
			case A_EXT_FUNCTION_RVAL:
				AstSemanticHighlighting.addSemanticToken(location, SemanticType.Function);
				break;
			default:
			}
		} 
		super.setSemanticInformation();
	}
}
