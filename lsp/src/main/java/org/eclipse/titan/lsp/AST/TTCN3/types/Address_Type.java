/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferencingType;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * address type (TTCN-3).
 *
 * @author Kristof Szabados
 * */
public final class Address_Type extends Type implements IReferencingType {

	/**
	 * Pointer to the real address type. Does not belong to this class.
	 * */
	private IType address;

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetype() {
		return Type_type.TYPE_ADDRESS;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isCompatible(final CompilationTimeStamp timestamp, final IType otherType, final TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain) {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetypeTtcn3() {
		return isErroneous ? Type_type.TYPE_UNDEFINED : getTypetype();
	}

	@Override
	/** {@inheritDoc} */
	public String getTypename() {
		return getTypetype().getName();
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;
		isErroneous = false;

		initAttributes(timestamp);

		final IType referencedLast = getTypeRefdLast(timestamp);

		if (referencedLast == null || referencedLast.getIsErroneous(timestamp)) {
			return;
		}

		referencedLast.check(timestamp);
	}

	public static void checkAddress(final CompilationTimeStamp timestamp, final Type type) {
		final IType referencedLast = type.getTypeRefdLast(timestamp);

		if (referencedLast == null || referencedLast.getIsErroneous(timestamp)) {
			return;
		}

		referencedLast.check(timestamp);

		switch (referencedLast.getTypetype()) {
		case TYPE_PORT:
			type.getLocation().reportSemanticError(MessageFormat.format("Port type `{0}'' cannot be the address type", referencedLast.getTypename()));
			break;
		case TYPE_COMPONENT:
			type.getLocation().reportSemanticError(MessageFormat.format("Component type `{0}'' cannot be the address type", referencedLast.getTypename()));
			break;
		case TYPE_SIGNATURE:
			type.getLocation().reportSemanticError(MessageFormat.format("Signature type `{0}'' cannot be the address type", referencedLast.getTypename()));
			break;
		case TYPE_DEFAULT:
			type.getLocation().reportSemanticError("The default type cannot be the address type");
			break;
		case TYPE_ANY:
			type.getLocation().reportSemanticError("The any type cannot be the address type");
			break;
		default:
			break;
		}
	}

	@Override
	/** {@inheritDoc} */
	public IValue checkThisValueRef(final CompilationTimeStamp timestamp, final IValue value) {
		if (Value_type.UNDEFINED_LOWERIDENTIFIER_VALUE.equals(value.getValuetype())) {
			final IReferenceChain tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			final IType refd = getTypeRefd(timestamp, tempReferenceChain);
			tempReferenceChain.release();

			if (this.equals(refd)) {
				return value;
			}

			return refd.checkThisValueRef(timestamp, value);
		}

		return value;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisValue(final CompilationTimeStamp timestamp, final IValue value, final Assignment lhs, final ValueCheckingOptions valueCheckingOptions) {
		if (getIsErroneous(timestamp)) {
			return false;
		}

		boolean selfReference = false;
		final IType tempType = getTypeRefd(timestamp, null);
		if (tempType != this) {
			selfReference = tempType.checkThisValue(timestamp, value, lhs, valueCheckingOptions);
		}

		value.setLastTimeChecked(timestamp);

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplate(final CompilationTimeStamp timestamp, final ITTCN3Template template,
			final boolean isModified, final boolean implicitOmit, final Assignment lhs) {
		registerUsage(template);
		template.setMyGovernor(this);

		boolean selfReference = false;
		final IType tempType = getTypeRefd(timestamp, null);
		if (tempType != this) {
			selfReference = tempType.checkThisTemplate(timestamp, template, isModified, implicitOmit, lhs);
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public IType getTypeRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		final boolean newChain = null == referenceChain;
		IReferenceChain tempReferenceChain;
		if (newChain) {
			tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		} else {
			tempReferenceChain = referenceChain;
		}

		IType lastType = this;
		while (lastType instanceof IReferencingType && !lastType.getIsErroneous(timestamp)) {
			lastType = ((IReferencingType) lastType).getTypeRefd(timestamp, tempReferenceChain);
		}

		if (newChain) {
			tempReferenceChain.release();
		}

		if (lastType != null && lastType.getIsErroneous(timestamp)) {
			setIsErroneous(true);
		}

		return lastType;
	}

	@Override
	/** {@inheritDoc} */
	public IType getTypeRefd(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (refChain != null && !refChain.add(this)) {
			setIsErroneous(true);
			return this;
		}

		if (myScope != null) {
			final TTCN3Module module = (TTCN3Module) myScope.getModuleScope();
			address = module.getAddressType(timestamp);
			if (address != null) {
				return address;
			}

			location.reportSemanticError("Type `address' is not defined in this module");
		}

		setIsErroneous(true);
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final IReferenceChain refChain, final boolean interruptIfOptional) {
		if (lastTimeChecked == null) {
			check(timestamp);
		}

		if (address != null && this != address) {
			final Expected_Value_type internalExpectation =
					(expectedIndex == Expected_Value_type.EXPECTED_TEMPLATE) ? Expected_Value_type.EXPECTED_DYNAMIC_VALUE : expectedIndex;
			final IType temp = address.getFieldType(timestamp, reference, actualSubReference, internalExpectation, refChain, false);
			if (reference.getIsErroneous(timestamp)) {
				setIsErroneous(true);
			}
			return temp;
		}

		return this;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}

		if (referenceChain.add(this)) {
			final IReferenceChain tempReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			final IType lastType = getTypeRefd(timestamp, tempReferenceChain);
			tempReferenceChain.release();

			if (!lastType.getIsErroneous(timestamp) && !this.equals(lastType)) {
				lastType.checkRecursions(timestamp, referenceChain);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkMapParameter(final CompilationTimeStamp timestamp, final IReferenceChain refChain, final Location errorLocation) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (refChain.contains(this)) {
			return;
		}

		refChain.add(this);
		final IType referencedLast = getTypeRefdLast(timestamp);
		if (referencedLast != null) {
			referencedLast.checkMapParameter(timestamp, refChain, errorLocation);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (address != null) {
			address.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (address!=null && !address.accept(v)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String getDefaultSnippet(String lineEnding, int indentation, AtomicInteger placeholderIdx) {
		if (address == null) {
			return "";
		}

		return ((Type)address).getDefaultSnippet(lineEnding, indentation, placeholderIdx);
	}
}
