/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.logging;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.NoViableAltException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.Interval;
import org.eclipse.titan.lsp.common.parsers.cfg.CfgInterval;
import org.eclipse.titan.lsp.parsers.AddedParseTree;

/**
 * USED FOR LOGGING PURPOSES.<br>
 * Logger utility to print parse tree and interval tree.
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public final class ParserLogger {
	private ParserLogger() {
		// intentionally private to disable instantiation
	}

	/**
	 * Logs a parse tree. (General version)
	 * @param aRoot parse tree
	 * @param aParser parser to get rule names
	 * @param aPrinter printer
	 * @param aDescription description of the parsing type, for the logging (for example: TTCN-3, Cfg, ASN.1)
	 */
	public static synchronized void log( final ParseTree aRoot,
			final Parser aParser,
			final String aDescription ) {
		
		/** date format in ISO 8601 */
		final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if ( !aParser.getBuildParseTree() ) {
			// Parse tree logging is not requested
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append( "---- parse tree\n" );
		sb.append( DATE_FORMAT.format( new Date() ) );
		sb.append( "\nParser type: " + aDescription + ", Prediction mode: " + aParser.getInterpreter().getPredictionMode() );
		sb.append( "\nCall stack: " + printCallStackReverse( sb, 4 ) ).append("\n");
		final TokenStream tokenStream = aParser.getTokenStream();
		if ( ! ( tokenStream instanceof CommonTokenStream ) ) {
			sb.append("ERROR: tokenStream is not CommonTokenStream\n");
			TitanLogger.logTrace(sb.toString());
			return;
		}
		final CommonTokenStream commonTokenStream = (CommonTokenStream)tokenStream;
		final List<Token> tokens = commonTokenStream.getTokens();
		log( sb, aRoot, aParser, tokens );
		TitanLogger.logTrace(sb.toString());
	}

	/**
	 * Logs a parse tree
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aRoot parse tree
	 * @param aParser parser to get rule names
	 * @param aTokens token list to get tokens by index (for getting tokens of a rule)
	 */
	private static void log( final StringBuilder aSb,
			final ParseTree aRoot,
			final Parser aParser,
			final List<Token> aTokens ) {
		try {
			log( aSb, aRoot, aParser, aTokens, new ArrayList<Boolean>(), false, false );
		} catch( Exception e ) {
			TitanLogger.logTrace(e);
		}
	}

	/**
	 * Logs a parse tree.
	 * Internal version.
	 * RECURSIVE
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aRoot parse tree
	 * @param aParser parser to get rule name
	 * @param aTokens token list to get tokens by index (for getting tokens of a rule)
	 * @param aLevel a list, that tells, if tree section is drawn for that level (parent).
	 *               If the parent of the given level is already the last child, tree sections are not drawn below.
	 *               NOTE: indentation level is aLevel.size()
	 * @param aParentOneChild parent has 1 child
	 * @param aLastChild this node is the last child of its parent
	 */
	private static void log( final StringBuilder aSb,
			final ParseTree aRoot,
			final Parser aParser,
			final List<Token> aTokens,
			final List<Boolean> aLevel,
			final boolean aParentOneChild,
			final boolean aLastChild ) {
		if ( aRoot == null ) {
			aSb.append( "ERROR: ParseTree root is null\n" );
			TitanLogger.logTrace(aSb.toString());
			return;
		}
		if ( !aParser.getBuildParseTree() ) {
			aSb.append( "ERROR: ParseTree is not build. Call Parser.setBuildParseTree( true ); BEFORE parsing."
					+ "Or do NOT call Parser.setBuildParseTree( false );\n" );
			TitanLogger.logTrace(aSb.toString());
			return;
		}

		if ( aRoot instanceof ParserRuleContext ) {
			final ParserRuleContext rule = (ParserRuleContext)aRoot;
			final String ruleInfo = getRuleInfo( rule, aParser );
			if ( aParentOneChild ) {
				aSb.append(printArrow( aSb, ruleInfo ));
			} else {
				aSb.append(printIndent( aSb, ruleInfo, aLevel ));
			}

			final int count = rule.getChildCount();
			final boolean oneChild = count == 1 && rule.exception == null;
			if ( !oneChild ) {
				aSb.append( ": '" ).append(getEscapedRuleText( rule, aTokens ))
					.append( "'" ).append(getExceptionInfo( rule, aParser )).append("\n");
			}

			for ( int i = 0; i < count; i++ ) {
				final ParseTree child = rule.getChild( i );
				List<Boolean> level = aLevel;
				if ( !aParentOneChild ) {
					level = new ArrayList<Boolean>( aLevel );
					if ( aLastChild ) {
						level.set( level.size() - 1, false );
					}
					level.add( true );
				}
				log( aSb, child, aParser, aTokens, level, oneChild, i == count - 1 );
			}
		} else if ( aRoot instanceof TerminalNodeImpl ) {
			final TerminalNodeImpl tn = (TerminalNodeImpl)aRoot;
			if ( aParentOneChild ) {
				aSb.append( ": '" ).append(getEscapedTokenText( tn.getSymbol() )).append( "'\n" );
			}

			printIndent( aSb, getTokenInfo( tn.getSymbol(), aParser ), aLevel );
			if ( tn.parent == null ) {
				aSb.append(", parent == null <-------------------------------------------------------------- ERROR");
			}
			aSb.append("\n");
		} else if ( aRoot instanceof AddedParseTree ) {
			final AddedParseTree apt = (AddedParseTree)aRoot;
			if ( aParentOneChild ) {
				aSb.append( ": '").append(getEscapedText( apt.getText() )).append( "'\n" );
			}
			aSb.append(printIndent( aSb, "AddedParseString: " + getEscapedText( apt.getText() ), aLevel ));
			if ( apt.getParent() == null ) {
				aSb.append(", parent == null <-------------------------------------------------------------- ERROR");
			}
			aSb.append("\n");
		} else {
			aSb.append( "ERROR: INVALID ParseTree type\n" );
		}
	}

	/**
	 * Rule exception info in string format for logging purpose
	 * @param aRule rule
	 * @param aParser parser to get token name
	 * @return exception stack trace + some other info from the exception object
	 */
	private static String getExceptionInfo( final ParserRuleContext aRule, final Parser aParser ) {
		final RecognitionException e = aRule.exception;
		if ( e == null ) {
			return GeneralConstants.EMPTY_STRING;
		}
		final StringBuilder sb = new StringBuilder();
		sb.append(", ERROR: mismatch, expected tokens: [" );
		final List<Integer> expectedTokens = e.getExpectedTokens().toList();
		for ( int i = 0; i < expectedTokens.size(); i++ ) {
			if ( i > 0 ) {
				sb.append(", ");
			}
			final int tokenType = expectedTokens.get( i );
			sb.append( getTokenName( tokenType, aParser ) );
		}
		sb.append(']');
		if ( e instanceof NoViableAltException ) {
			final NoViableAltException nvae = (NoViableAltException)e;
			sb.append( ", start token: " + getTokenInfo( nvae.getStartToken(), aParser ) );
		}

		return sb.toString();
	}

	/**
	 * Rule info in string format for logging purpose
	 * @param aRule rule
	 * @param aParser parser to get rule name
	 * @return rule name and exception and some other info if rule is erroneous
	 */
	private static String getRuleInfo( final ParserRuleContext aRule, final Parser aParser ) {
		// only rule name
		return aParser.getRuleInvocationStack( aRule ).get( 0 );
	}

	/**
	 * Escaped rule text including hidden tokens
	 * For logging purposes
	 * @param aRule rule
	 * @param aTokens token list to get the tokens (all, hidden and not hidden also) from
	 * @return escaped rule text
	 */
	private static String getEscapedRuleText( final ParserRuleContext aRule,
			final List<Token> aTokens ) {
		final Token startToken = aRule.start;
		if ( startToken == null ) {
			TitanLogger.logTrace( "ERROR: ParseLogger.getEscapedRuleText() startToken == null\n" );
			return GeneralConstants.EMPTY_STRING;
		}

		final Token stopToken = aRule.stop;
		if ( stopToken == null ) {
			TitanLogger.logTrace( "ERROR: ParseLogger.getEscapedRuleText() stopToken == null\n" );
			return GeneralConstants.EMPTY_STRING;
		}

		final int startIndex = startToken.getTokenIndex();
		final int stopIndex = stopToken.getTokenIndex();
		final StringBuilder sb = new StringBuilder();
		for ( int i = startIndex; i <= stopIndex; i++ ) {
			try {
				sb.append( getEscapedTokenText( aTokens.get( i ) ) );
			} catch ( IndexOutOfBoundsException e ) {
				sb.append(GeneralConstants.UNDERSCORE);
			}
		}
		return sb.toString();
	}

	/**
	 * Rule text including hidden tokens
	 * For logging purposes
	 * @param aRule rule
	 * @param aTokens token list to get the tokens (all, hidden and not hidden also) from
	 * @return rule text including hidden tokens. First and last tokens are non-hidden.
	 */
	public static String getRuleText( final ParserRuleContext aRule,
			final List<Token> aTokens ) {
		final Token startToken = aRule.start;
		if ( startToken == null ) {
			TitanLogger.logTrace( "ERROR: ParseLogger.getEscapedRuleText() startToken == null\n" );
			return GeneralConstants.EMPTY_STRING;
		}

		final Token stopToken = aRule.stop;
		if ( stopToken == null ) {
			TitanLogger.logTrace( "ERROR: ParseLogger.getEscapedRuleText() stopToken == null\n" );
			return GeneralConstants.EMPTY_STRING;
		}

		final int startIndex = startToken.getTokenIndex();
		final int stopIndex = stopToken.getTokenIndex();
		final StringBuilder sb = new StringBuilder();
		for ( int i = startIndex; i <= stopIndex; i++ ) {
			sb.append( aTokens.get( i ) );
		}
		return sb.toString();
	}

	/**
	 * Token info in string format for logging purpose
	 * @param aToken token
	 * @param aParser parser to get token name
	 * @return &lt;token name&gt;: '&lt;token text&gt;', @&lt;token index&gt;, &lt;line&gt;:&lt;column&gt;[, channel=&lt;channel&gt;]
	 *         <br>where
	 *         <br>&lt;token index&gt; starts  from 0,
	 *         <br>&lt;line&gt; starts from 1,
	 *         <br>&lt;column&gt; starts from 0,
	 *         <br>channel info is provided if &lt;channel&gt; > 0 (hidden channel)
	 */
	private static String getTokenInfo( final Token aToken, final Parser aParser ) {
		final StringBuilder sb = new StringBuilder();
		final int tokenType = aToken.getType();
		final String tokenName = getTokenName( tokenType, aParser );
		sb.append( tokenName );
		sb.append( ": " );

		sb.append( '\'' );
		sb.append( getEscapedTokenText( aToken ) );
		sb.append( '\'' );

		sb.append( ", @" + aToken.getTokenIndex() );
		sb.append( ", " + aToken.getLine() + ":" + aToken.getCharPositionInLine() );
		if ( aToken.getChannel() > 0 ) {
			sb.append( ", channel=" );
			sb.append( aToken.getChannel() );
		}
		return sb.toString();
	}

	/**
	 * Gets escaped token text
	 * Escaped chars are converted to printable strings.
	 * @param aToken input token
	 * @return escaped token text
	 */
	private static String getEscapedTokenText( final Token aToken ) {
		return getEscapedText( aToken.getText() );
	}

	/**
	 * Gets escaped text
	 * Escaped chars are converted to printable strings.
	 * @param aText input text
	 * @return escaped text
	 */
	public static String getEscapedText( final String aText ) {
		String txt = aText;
		if ( txt == null ) {
			return "";
		}
		txt = txt.replace( "\n", "\\n" );
		txt = txt.replace( "\r", "\\r" );
		txt = txt.replace( "\t", "\\t" );
		return txt;
	}

	/**
	 * @param aTokenType token type index
	 * @param aParser parser to resolve token name
	 * @return resolved token name
	 */
	private static String getTokenName( final int aTokenType, final Parser aParser ) {
		return aParser.getVocabulary().getSymbolicName( aTokenType );
	}

	/**
	 * Logs an interval tree.
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aRootInterval the root of the interval tree
	 * @return A reference to {@code aSb} containing the interval tree
	 */
	public static StringBuilder logInterval( final StringBuilder aSb, final Interval aRootInterval ) {
		return logInterval( aSb, aRootInterval, 0 );
	}

	/**
	 * Logs an interval tree.
	 * Internal version.
	 * RECURSIVE
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aRootInterval the root of the interval tree
	 * @param aLevel indentation level
	 * @return A reference to {@code aSb} containing the interval tree
	 */
	private static StringBuilder logInterval( final StringBuilder aSb, final Interval aRootInterval, final int aLevel ) {
		// root interval info
		final StringBuilder sb = new StringBuilder();
		sb.append( "" + aRootInterval.getDepth() );
		sb.append( ", " + aRootInterval.getStartOffset() );
		sb.append( ", " + aRootInterval.getStartLine() );
		sb.append( ", " + aRootInterval.getEndOffset() );
		sb.append( ", " + aRootInterval.getEndLine() );
		sb.append( ", " + aRootInterval.getType() );
		if ( aRootInterval instanceof CfgInterval ) {
			sb.append( ", " + ( ( CfgInterval )aRootInterval).getSectionType() );
		}
		if ( aRootInterval.getErroneous() ) {
			sb.append( ", ERRONEOUS" );
		}
		printIndent( aSb, sb.toString(), aLevel );
		aSb.append("\n");
		if ( aRootInterval.getSubIntervals() != null ) {
			for ( final Interval interval : aRootInterval.getSubIntervals() ) {
				logInterval( aSb, interval, aLevel + 1 );
			}
		}
		return aSb;
	}

	/**
	 * -> aMsg
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aMsg
	 * @return A reference to {@code aSb} containing {@code aMsg}
	 */
	private static StringBuilder printArrow( final StringBuilder aSb, final String aMsg ) {
		return aSb.append( " -> " ).append( aMsg );
	}

	/**
	 * Adds message to the specified string for logging purpose with level dependent indentation
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aMsg message
	 * @param aLevel indentation level
	 * @return A reference to {@code aSb} containing {@code aMsg}
	 */
	private static StringBuilder printIndent( final StringBuilder aSb, final String aMsg, final int aLevel ) {
		final StringBuilder sb = new StringBuilder();
		for ( int i = 0; i < aLevel; i++ ) {
			sb.append( "  " );
		}
		return aSb.append( sb ).append( aMsg );
	}

	/**
	 * Adds message to the specified string for logging purpose with level dependent indentation
	 * and also with tree structure, so a rectangular line is drawn between the parent and the children.
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aMsg message
	 * @param aLevel a list, that tells, if tree section is drawn for that level (parent).
	 *               If the parent of the given level is already the last child, tree sections are not drawn below.
	 *               NOTE: indentation level is aLevel.size()
	 * @return A reference to {@code aSb} containing {@code aMsg}
	 */
	private static StringBuilder printIndent( final StringBuilder aSb, final String aMsg, final List<Boolean> aLevel ) {
		final StringBuilder sb = new StringBuilder();
		final int size = aLevel.size();
		for ( int i = 0; i < size; i++ ) {
			if ( i < size - 1 ) {
				sb.append( Boolean.TRUE.equals(aLevel.get( i )) ? "| " : "  " );
			} else {
				sb.append( "+-" );
			}
		}
		return aSb.append( sb ).append( aMsg );
	}

	/**
	 * Prints full stack trace in one line.
	 * Short class name is used without full qualification.
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aN [in] number of methods, which are not needed. These are the top of the call stack
	 *                getStackTrace()
	 *                this function
	 *                logParseTree() function(s)
	 * @return A reference to {@code aSb} containing the representation of the call stack
	 */
	public static StringBuilder printCallStack( final StringBuilder aSb, final int aN ) {
		final StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		final StringBuilder sb = new StringBuilder();
		final int first = stackTrace.length - 1;
		for ( int i = first; i >= aN; i-- ) {
			final StackTraceElement ste = stackTrace[ i ];
			if ( i < first ) {
				sb.append(" -> ");
			}
			printStackTraceElement( sb, ste );
		}
		return aSb.append(sb);
	}

	/**
	 * Prints full stack trace in one line in reversed order.
	 * Short class name is used without full qualification.
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aN [in] number of methods, which are not needed. These are the top of the call stack
	 *                getStackTrace()
	 *                this function
	 *                logParseTree() function(s)
	 * @return A reference to {@code aSb} containing the representation of the call stack
	 */
	public static StringBuilder printCallStackReverse( final StringBuilder aSb, final int aN ) {
		final StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		final StringBuilder sb = new StringBuilder();
		for ( int i = aN; i < stackTrace.length; i++ ) {
			final StackTraceElement ste = stackTrace[ i ];
			if ( i > aN ) {
				sb.append(" <- ");
			}
			printStackTraceElement( sb, ste );
		}
		return aSb.append(sb);
	}

	/**
	 * Prints a stack trace element (method call of a call chain) with short class name and method name
	 * @param aSb [in/out] the log string, where new strings are added
	 * @param aSte stack trace element
	 * @return string representation of the stack trace element
	 */
	private static StringBuilder printStackTraceElement( final StringBuilder aSb, final StackTraceElement aSte ) {
		final String className = aSte.getClassName();
		final String shortClassName = className.substring( className.lastIndexOf(GeneralConstants.DOT_CHAR) + 1 );
		aSb.append( shortClassName );
		aSb.append( GeneralConstants.DOT );
		aSb.append( aSte.getMethodName() );
		aSb.append( ':' );
		aSb.append( aSte.getLineNumber() );
		return aSb;
	}
}
