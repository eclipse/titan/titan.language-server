/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.logging;

import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.common.product.Configuration;

/**
 * Main logger component using the <em>java.util.logging</em> package
 * 
 * @author Adam Knapp
 * */
public final class TitanLogger {
	private static final String INTERNAL_ERROR_MESSAGE = "An " + GeneralConstants.INTERNAL_ERROR + " has occured";
	private static final Logger LOGGER = Logger.getLogger(TitanLogger.class.getName());
	private static String logLevel = Configuration.INSTANCE.getString(Configuration.LOG_LEVEL, "info");

	static {
		LogManager.getLogManager().reset();
        Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.OFF);
		// Uses System.err by default
		ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new SimpleFormatter() {
			private static final String format = "[%1$tF %1$tT] [%2$-7s] %3$s %n";
			@Override
			public String formatMessage(LogRecord rec) {
				return String.format(format,
						new Date(rec.getMillis()),
						rec.getLevel().getLocalizedName(),
						rec.getMessage()
						);
			}
		});
		LOGGER.setUseParentHandlers(false);
		LOGGER.addHandler(handler);

		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.LOG_LEVEL, pref -> {
			logLevel = Configuration.INSTANCE.getString(Configuration.LOG_LEVEL, "info");
			setLevel(logLevel);
		});
	}

	private TitanLogger() {
		// Do nothing
	}

	/**
	 * Sets the log level to the specified level
	 * @param logLevel
	 * @see Level
	 */
	public static synchronized void setLevel(final Level logLevel) {
		LOGGER.setLevel(logLevel);
		final Handler[] handlers = LOGGER.getHandlers();
		handlers[0].setLevel(logLevel);
	}

	/**
	 * Sets the log level to the specified level. Maps the log levels in the Titan terminology
	 * to the ones of {@link Level}.
	 * <p>The possible Titan specific log levels in descending order are:
	 * <ul>
	 * <li>{@code OFF} -> {@link Logger#OFF}
	 * <li>{@code FATAL/ERROR} -> {@link Logger#SEVERE}
	 * <li>{@code WARNING} -> {@link Logger#WARNING}
	 * <li>{@code INFO} -> {@link Logger#INFO}
	 * <li>{@code DEBUG} -> {@link Logger#FINE}
	 * <li>{@code TRACE} -> {@link Logger#FINEST}
	 * </ul>
	 * @param logLevel New log level to set
	 * @throws IllegalArgumentException if invalid log level was provided
	 * @see Level
	 */
	public static void setLevel(final String logLevel) {
		switch(logLevel.toUpperCase()) {
		case "OFF":
			setLevel(Level.OFF);
			break;
		case "FATAL":
		case "ERROR":
			setLevel(Level.SEVERE);
			break;
		case "WARNING":
			setLevel(Level.WARNING);
			break;
		case "INFO":
			setLevel(Level.INFO);
			break;
		case "DEBUG":
			setLevel(Level.FINE);
			break;
		case "TRACE":
			setLevel(Level.FINEST);
			break;
		default:
			logError(new IllegalArgumentException("Invalid log level: " + logLevel));
			setLevel(Level.INFO);
		}
	}

	/**
	 * Logs a <em>fatal/internal</em> error that should not happen.
	 * Typically used when the AST is not properly built.
	 * Stack trace is always logged. 
	 */
	public static void logFatal() {
		logFatal(INTERNAL_ERROR_MESSAGE);
	}

	/**
	 * Logs a <em>fatal/internal</em> error that should not happen.
	 * Typically used when the AST is not properly built.
	 * Stack trace is always logged. 
	 * @param message message to log
	 */
	public static void logFatal(final String message) {
		logFatal(message, null);
	}

	/**
	 * Logs a <em>fatal/internal</em> error that should not happen.
	 * Typically used when the AST is not properly built.
	 * Stack trace is always logged.
	 * The log message is extracted from the {@link Throwable}.
	 * @param e exception to log
	 */
	public static void logFatal(final Throwable e) {
		logFatal(e.getMessage(), e);
	}

	/**
	 * Logs a <em>fatal/internal</em> error that should not happen.
	 * Typically used when the AST is not properly built.
	 * Stack trace is always logged. 
	 * @param message message to log
	 * @param e stack trace to log, if {@code null},
	 * then an {@link Exception} is generated with the provided message
	 */
	public static void logFatal(final String message, final Throwable e) {
		LOGGER.log(Level.SEVERE, message, e == null ? new Exception(message) : e);
	}

	public static void logError(final String message) {
		logError(message, null);
	}

	public static void logError(final Throwable e) {
		logError(e.getMessage(), e);
	}

	public static void logError(final String message, final Throwable e) {
		LOGGER.log(Level.SEVERE, message, e);
	}

	public static void logWarning(final String message) {
		logWarning(message, null);
	}

	public static void logWarning(final Throwable e) {
		logWarning(null, e);
	}

	public static void logWarning(final String message, final Throwable e) {
		LOGGER.log(Level.WARNING, message, e);
	}

	public static void logInfo(final String message) {
		logInfo(message, null);
	}

	public static void logInfo(final Throwable e) {
		logInfo(null, e);
	}

	public static void logInfo(final String message, final Throwable e) {
		LOGGER.log(Level.INFO, message, e);
	}

	public static void logDebug(final String message) {
		logDebug(message, null);
	}

	public static void logDebug(final Throwable e) {
		logDebug(null, e);
	}

	public static void logDebug(final String message, final Throwable e) {
		LOGGER.log(Level.FINE, message, e);
	}

	public static void logTrace(final String message) {
		logTrace(message, null);
	}

	public static void logTrace(final Throwable e) {
		logTrace(null, e);
	}

	public static void logTrace(final String message, final Throwable e) {
		LOGGER.log(Level.FINEST, message, e);
	}

	/**
	 * It cuts the stack trace to its first element, when {@code debug} loglevel is used.
	 * Contrarily, it logs the whole stack trace, when {@code trace} loglevel is used. 
	 * @param e the Throwable to log
	 */
	public static void logMixedDebugAndTrace(final Throwable e) {
		if (Level.FINE.equals(LOGGER.getLevel())) {
			final StackTraceElement[] elements = e.getStackTrace();
			e.setStackTrace(new StackTraceElement[] { elements[0] });
			logDebug(e);
		} else {
			logTrace(e);
		}
	}
}
