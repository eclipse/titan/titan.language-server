/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsp4j.generator.JsonRpcData;

/**
 * This class represents the list of resources with the 'excluded state' changed. 
 * 
 * @author Miklos Magyari
 *
 */
@JsonRpcData
public class ExcludedStatesChangedParams {
	private List<ExcludedStateParams> excludedStates = new ArrayList<>();
	
	public ExcludedStatesChangedParams(final List<ExcludedStateParams> list) {
		if (list != null) {
			this.excludedStates = list;
		}
	}

	public final List<ExcludedStateParams> getExcludedStates() {
		return excludedStates;
	}
}
