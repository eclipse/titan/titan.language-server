/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.typeHierarchy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.TypeHierarchyItem;
import org.eclipse.lsp4j.TypeHierarchyPrepareParams;
import org.eclipse.lsp4j.TypeHierarchySubtypesParams;
import org.eclipse.lsp4j.TypeHierarchySupertypesParams;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ASN1.definitions.ASN1Module;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

public final class TypeHierarchyProvider {
	private TypeHierarchyProvider() {
		// Hide constructor
	}

	private static Optional<IDeclaration> getDeclaration(String uri, Position position) {
		final Module module = Project.INSTANCE.getModuleByPath(IOUtils.getPath(uri));
		if (module instanceof TTCN3Module || module instanceof ASN1Module) {
			final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(position);
			module.accept(visitor);
			return Optional.of(visitor.getReferencedDeclaration());
		}
		return Optional.empty();
	}

	private static TypeHierarchyItem createTypeHierarchyItem(final Assignment ass) {
		final Location location = ass.getLocation();
		final String name = ass.getIdentifier().getName();
		final String uri = location.getFile().toPath().toUri().toString();
		final Range range = location.getRange();
		return new TypeHierarchyItem(name, SymbolKind.Function, uri, range, range);
	}

	private static IType getAssignmnetType(Assignment ass) {
		final CompilationTimeStamp lastTimeChecked = ass.getLastTimeChecked();
		final CompilationTimeStamp timestamp = lastTimeChecked != null ? lastTimeChecked : CompilationTimeStamp.getBaseTimestamp();
		return ass.getType(timestamp);
	}

	public static List<TypeHierarchyItem> prepareTypeHierarchy(TypeHierarchyPrepareParams params) {
		return getDeclaration(params.getTextDocument().getUri(), new Position(params.getPosition()))
				.map(declaration -> {
					Assignment ass = declaration.getAssignment();
					if (! (ass instanceof Def_Type)) {
						return null;
					}

					final TypeHierarchyItem item = createTypeHierarchyItem(ass);
					return new ArrayList<>(Arrays.asList(item));
				}).orElse(null);
	}

	public static List<TypeHierarchyItem> getSuperTypes(TypeHierarchySupertypesParams params) {
		return getDeclaration(params.getItem().getUri(), new Position(params.getItem().getRange().getStart()))
				.map(declaration -> {
					final IType assType = getAssignmnetType(declaration.getAssignment());

					if (assType.getTypetype() == Type_type.TYPE_COMPONENT) {
						final ComponentTypeHierarchy hierarchy = new ComponentTypeHierarchy((Component_Type) assType);
						return hierarchy.getSuperTypes();
					}

					if (assType.getTypetype() == Type_type.TYPE_CLASS) {
						final ClassTypeHierarchy hierarchy = new ClassTypeHierarchy((Class_Type) assType);
						return hierarchy.getSuperTypes();
					}
					return null;
				}).orElse(null);
	}

	public static List<TypeHierarchyItem> getSubTypes(TypeHierarchySubtypesParams params) {
		return getDeclaration(params.getItem().getUri(), new Position(params.getItem().getRange().getStart()))
				.map(declaration -> {
					Assignment ass = declaration.getAssignment();
					final IType assType = getAssignmnetType(ass);

					if (assType.getTypetype() == Type_type.TYPE_COMPONENT) {
						final ComponentTypeHierarchy hierarchy = new ComponentTypeHierarchy(ass.getReferences());
						return hierarchy.getSubTypes();
					}
					return null;
				}).orElse(null);
	}
}
