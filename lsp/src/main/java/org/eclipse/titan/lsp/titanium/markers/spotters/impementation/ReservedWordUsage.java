/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class ReservedWordUsage extends BaseModuleCodeSmellSpotter {
	private List<String> reservedKeywords;
	
	public ReservedWordUsage() {
		super(CodeSmellType.RESERVED_WORD_USAGE);
		reservedKeywords = Arrays.asList(
			/** OOP keywords **/
			"class",
			"finally",
			"object",
			"this",
			"@final",
			"@abstract",
			"@get",
			"@set",
			"@property",
			
			/** realtime keywords */
			"now",
			"realtime",
			"timestamp"
		);
		
		addStartNode(Identifier.class);
	}

	@Override
	protected void process(IVisitableNode node, Problems problems) {
		if (node instanceof Identifier) {
			final Identifier id = (Identifier)node;
			if (reservedKeywords.contains(id.getDisplayName())) {
				problems.report(id.getLocation(), 
					MessageFormat.format("Reserved keyword `{0}'' is used as an identifier", id.getDisplayName()));
			}
		}
	}
}
