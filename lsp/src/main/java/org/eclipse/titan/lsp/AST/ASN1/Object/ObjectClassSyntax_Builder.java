/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Token;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.ASN1.Block;
import org.eclipse.titan.lsp.AST.ASN1.Object.ObjectClassSyntax_setting.SyntaxSetting_types;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.asn1parser.Asn1Lexer;
import org.eclipse.titan.lsp.parsers.asn1parser.Asn1Parser;
import org.eclipse.titan.lsp.parsers.asn1parser.SyntaxLevelTokenStreamTracker;
import org.eclipse.titan.lsp.parsers.asn1parser.TokenWithIndexAndSubTokens;

/**
 * OCS visitor to build the OCS. :) It's clear and simple, isn't it?
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public final class ObjectClassSyntax_Builder implements IObjectClassSyntax_Visitor {
	private static final String EMPTY_OPTIONAL_ERROR = "Empty optional group is not allowed";

	private Block mBlock;
	private final FieldSpecifications fieldSpecifications;

	public ObjectClassSyntax_Builder(final Block aBlock, final FieldSpecifications aFieldSpecifications) {
		this.mBlock = aBlock;
		this.fieldSpecifications = aFieldSpecifications;
		if (aBlock != null) {
			final List<Token> internalTokens = new ArrayList<Token>(aBlock.getTokenList().size());

			for (final Token token : aBlock.getTokenList()) {
				if (token.getType() == Asn1Lexer.LEFTVERSIONBRACKETS) {
					final CommonToken token2 = ((TokenWithIndexAndSubTokens) token).copy();
					token2.setType(Asn1Lexer.SQUAREOPEN);
					internalTokens.add(token2);
					internalTokens.add(token2);
				} else if (token.getType() == Asn1Lexer.RIGHTVERSIONBRACKETS) {
					final CommonToken token2 = ((TokenWithIndexAndSubTokens) token).copy();
					token2.setType(Asn1Lexer.SQUARECLOSE);
					internalTokens.add(token2);
					internalTokens.add(token2);
				} else {
					internalTokens.add(token);
				}
			}
			internalTokens.add(new TokenWithIndexAndSubTokens(Token.EOF));

			this.mBlock = new Block(internalTokens, aBlock.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void visitRoot(final ObjectClassSyntax_root parameter) {
		if (parameter.getIsBuilt()) {
			return;
		}

		if (mBlock == null) {
			final ObjectClassSyntax_sequence sequence = parameter.getSequence();
			for (FieldSpecification fieldSpecification : fieldSpecifications) {
				fieldSpecification = fieldSpecification.getLast();
				final ObjectClassSyntax_sequence temporalSequence = new ObjectClassSyntax_sequence(fieldSpecification.isOptional()
						|| fieldSpecification.hasDefault(), true);

				final Identifier newIdentifier = fieldSpecification.getIdentifier().newInstance();
				final ObjectClassSyntax_literal literal = new ObjectClassSyntax_literal(newIdentifier);
				literal.setLocation(fieldSpecification.getLocation());

				ObjectClassSyntax_setting setting = null;
				switch (fieldSpecification.getFieldSpecificationType()) {
				case FS_T: {
					final Identifier newIdentifier2 = newIdentifier.newInstance();
					setting = new ObjectClassSyntax_setting(SyntaxSetting_types.S_T, newIdentifier2);
					break;
				}
				case FS_V_FT:
				case FS_V_VT: {
					final Identifier newIdentifier2 = newIdentifier.newInstance();
					setting = new ObjectClassSyntax_setting(SyntaxSetting_types.S_V, newIdentifier2);
					break;
				}
				case FS_VS_FT:
				case FS_VS_VT: {
					final Identifier newIdentifier2 = newIdentifier.newInstance();
					setting = new ObjectClassSyntax_setting(SyntaxSetting_types.S_VS, newIdentifier2);
					break;
				}
				case FS_O: {
					final Identifier newIdentifier2 = newIdentifier.newInstance();
					setting = new ObjectClassSyntax_setting(SyntaxSetting_types.S_O, newIdentifier2);
					break;
				}
				case FS_OS: {
					final Identifier newIdentifier2 = newIdentifier.newInstance();
					setting = new ObjectClassSyntax_setting(SyntaxSetting_types.S_OS, newIdentifier2);
					break;
				}
				case FS_ERROR:
				default:
					break;
				}

				if (setting != null) {
					setting.setLocation(fieldSpecification.getLocation());

					temporalSequence.add(literal);
					temporalSequence.add(setting);
					temporalSequence.trimToSize();

					sequence.add(temporalSequence);
				}
			}

			sequence.trimToSize();
		} else {
			parameter.getSequence().accept(this);
		}

		parameter.setIsBuilt(true);
	}

	@Override
	/** {@inheritDoc} */
	public void visitSequence(final ObjectClassSyntax_sequence parameter) {
		if (parameter.getIsBuilt()) {
			return;
		}

		final Asn1Parser parser = SyntaxLevelTokenStreamTracker.getASN1ParserForBlock(mBlock);
		if (parser == null) {
			return;
		}

		final List<ObjectClassSyntax_Node> nodes = parser.pr_special_ObjectClassSyntax_Builder(fieldSpecifications).nodes;
		if (nodes != null) {
			for (final ObjectClassSyntax_Node node : nodes) {
				parameter.add(node);
			}
		}

		final List<SyntacticErrorStorage> errors = parser.getErrorStorage();
		if (errors != null && !errors.isEmpty()) {
			for (final SyntacticErrorStorage ses : errors) {
				ses.reportSyntacticError();
			}
		} else if (parameter.isOptional() && parameter.isEmpty()) {
			parameter.getLocation().reportSemanticError(EMPTY_OPTIONAL_ERROR);
		}

		parameter.setIsBuilt(true);
		parameter.trimToSize();
	}
}
