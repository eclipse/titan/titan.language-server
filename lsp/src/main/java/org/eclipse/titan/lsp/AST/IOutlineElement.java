/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public interface IOutlineElement extends IIdentifiable {

	String getOutlineText();

	/**
	 * @return those objects which should be displayed as the children
	 * of this particular object.
	 * */
	Object[] getOutlineChildren();
	
	/**
	 * Gets the nested DocumentSymbol structure for the element, including all outline children
	 * @return
	 */
	default DocumentSymbol getOutlineSymbol() {
		return null;
	}
	
	/**
	 * Gets the list of child elements for this outline element
	 * @return
	 */
	default List<DocumentSymbol> getOutlineSymbolChildren() {
		return Collections.emptyList();
	}
	
	/**
	 * Returns the category of this outline element (module, class, function etc.)
	 * 
	 * @return Element of the SymbolKind enum
	 */
	default SymbolKind getOutlineSymbolKind() {
		return null;
	}
	
	/**
	 * Gets the location of the element in the source code
	 * @return
	 */
	default Range getOutlineRange() {
		return null;
	}
	
	/**
	 * Gets the location of the source code that is selected when the outline element is active
	 * @return
	 */
	default Range getOutlineSelectionRange() {
		return null;
	}

	/**
	 * Returns the category of this element. The category is a
	 * number used to allocate elements to bins; the bins are arranged
	 * in ascending numeric order. The elements within a bin are arranged
	 * via a second level sort criterion.
	 * <p>
	 * By default this method should return <code>0</code> if no category can be defined.
	 * Subclasses may reimplement this method to provide non-trivial categorization.
	 * </p>
	 *
	 * @return the category
	 */
	int category();

	/**
	 * Returns a default {@link Comparator} for the comparison of IOutlineElements.
	 * The comparison is based on the location. 
	 * @return a default Comparator
	 */
	static Comparator<IOutlineElement> getComparator() {
		return (o1, o2) -> {
			final Location l1 = o1.getIdentifier().getLocation();
			final Location l2 = o2.getIdentifier().getLocation();
			if (l1.getStartPosition().before(l2.getStartPosition())) {
				return -1;
			} else if (l1.getStartPosition().after(l2.getStartPosition())) {
				return 1;
			}
			return 0;
		};
	}
}
