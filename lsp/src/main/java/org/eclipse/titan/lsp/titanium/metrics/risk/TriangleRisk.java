/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.risk;

import org.eclipse.titan.lsp.titanium.metrics.common.IMetricEnum;

public class TriangleRisk extends BaseRisk {
	public TriangleRisk(final IMetricEnum metric) {
		super(metric);
	}

	@Override
	public double getRiskValue(final Number value) {
		double risk = 0;
		double limit1;
		double limit2;
		final double dist = 0.5 - Math.abs(value.doubleValue() - 0.5);
		switch (method) {
		case NEVER:
			// cheat to never reach 1.0
			risk = dist * 1.999;
			break;
		case NO_HIGH:
			limit1 = limits[0].doubleValue();
			if (dist >= limit1) {
				risk = 2 + (dist - limit1) / (0.5 - limit1);
			} else {
				risk = dist / limit1;
			}
			break;
		case NO_LOW:
			limit1 = limits[0].doubleValue();
			if (dist >= limit1) {
				// 0.501 instead of 0.5 is again a cheat, like above.
				risk = 1 + (dist - limit1) / (0.501 - limit1);
			} else {
				risk = dist / limit1;
			}
			break;
		case NO_LOW_HIGH:
			limit1 = limits[0].doubleValue();
			limit2 = limits[1].doubleValue();
			if (dist < limit1) {
				risk = dist / limit1;
			} else if (dist >= limit2) {
				risk = 2 + (dist - limit2) / (0.5 - limit2);
			} else {
				risk = 1 + (dist - limit1) / (limit2 - limit1);
			}
			break;
		}
		return risk;
	}
}
