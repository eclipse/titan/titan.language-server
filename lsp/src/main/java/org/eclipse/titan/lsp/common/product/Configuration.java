/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.product;

import java.io.StringReader;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

import org.eclipse.lsp4j.ConfigurationItem;
import org.eclipse.lsp4j.ConfigurationParams;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.common.logging.TitanLogger;

/**
 * 
 * Configuration preferences fetched from the LSP client.
 * 
 * @author Miklos Magyari
 *
 */
public class Configuration {
	// on-the-fly checker preferences
	public static final String PREFIX = "titan.";
	public static final String LOG_LEVEL = PREFIX + "logLevel";
	public static final String REPORT_IGNORED_PREPROCESSOR_DIRECTIVES = PREFIX + "reportIgnoredPreprocessorDirectives";
	public static final String LIMIT_ALL_THREAD_CREATION = PREFIX + "limitAllThreadCreation";
	public static final String PROCESSING_UNITS_TO_USE = PREFIX + "processingUnitsToUse";
	public static final String ENABLE_TITANIUM = PREFIX + "enableTitanium";
	public static final String ENABLE_INLAY_HINTS = PREFIX + "enableInlayHints";

	public static final String ON_THE_FLY_PREFERENCES = PREFIX + "onTheFlyChecker";
	public static final String USE_INCREMENTAL_PARSING = ON_THE_FLY_PREFERENCES + ".useIncrementalParsing";
	public static final String ENABLE_OOP_EXTENSION = ON_THE_FLY_PREFERENCES + ".enableOop";
	public static final String ENABLE_REALTIME_EXTENSION = ON_THE_FLY_PREFERENCES + ".enableRealtime";
	public static final String REPORT_NAMING_CONVENTION_PROBLEMS = ON_THE_FLY_PREFERENCES + ".enableNamingConventionCheck";
	public static final String NAMING_CONVENTION_SEVERITY = ON_THE_FLY_PREFERENCES + ".namingConventionSeverity";
	public static final String REPORT_STRICT_CONSTANTS = ON_THE_FLY_PREFERENCES + ".reportStrictConstants";
	public static final String REPORT_DOC_COMMENT_INCONSISTENCY = ON_THE_FLY_PREFERENCES + ".reportDocumentCommentIncosistency";
	public static final String REPORT_MODULE_NAME_REUSE = ON_THE_FLY_PREFERENCES + ".reportModuleNameReuse";
	public static final String REPORT_ERRORS_IN_EXTENSION_SYNTAX = ON_THE_FLY_PREFERENCES + ".reportErrorsInExtensionSyntax";
	public static final String REPORT_KEYWORD_USED_AS_IDENTIFIER = ON_THE_FLY_PREFERENCES + ".reportKeywordUsedAsIdentifier";
	public static final String REPORT_DUPLICATED_UNIQUE_FIELD_VALUE = ON_THE_FLY_PREFERENCES + ".reportDuplicatedUniqueFieldValue";

	public static final String TYPE_COMPATIBILITY_SEVERITY = ON_THE_FLY_PREFERENCES + ".typeCompatibilitySeverity";
	public static final String UNSUPPORTED_CONSTRUCT_SEVERITY = ON_THE_FLY_PREFERENCES + ".unsupportedConstructSeverity";

	public static final String TEST_MODE = ON_THE_FLY_PREFERENCES + ".onTheFlyCheckerTestMode";

	public static final String OUTLINE = PREFIX + "outline";
	public static final String OUTLINE_SHOW_FUNCTIONS = OUTLINE + ".showFunctions";
	public static final String OUTLINE_SHOW_TESTCASES = OUTLINE + ".showTestcases";
	public static final String OUTLINE_SHOW_CLASSES = OUTLINE + ".showClasses";

	public static final String NAMING_CONVENTION = PREFIX + "namingConvention";
	public static final String NAMING_CONVENTION_EXTFUNCTION = NAMING_CONVENTION + ".externalFunction";
	public static final String NAMING_CONVENTION_FORMALPARAM = NAMING_CONVENTION + ".formalParameter";
	public static final String NAMING_CONVENTION_PORT = NAMING_CONVENTION + ".port";
	public static final String NAMING_CONVENTION_TESTCASE = NAMING_CONVENTION + ".testcase";

	public static final String COMPILER_PREFERENCES = PREFIX + "compiler";
	public static final String TTCN_VERSION = COMPILER_PREFERENCES + ".ttcnVersion"; 
	public static final String ANALYZE_ONLY_WHEN_TPD_IS_ACTIVE = COMPILER_PREFERENCES + ".analyzeOnlyWhenTpdIsActive";
	public static final String EXCLUDE_BY_TPD = COMPILER_PREFERENCES + ".excludeByTpd";
	public static final String ACTIVATED_TPD = COMPILER_PREFERENCES + ".activatedTpd";
	public static final String LOG_PARSE_TREE = COMPILER_PREFERENCES + ".logParseTree";
	public static final String TITAN_INSTALLATION_PATH = COMPILER_PREFERENCES + ".titanInstallationPath";
	public static final String LICENSE_FILE_PATH = COMPILER_PREFERENCES + ".licenseFilePath";

	public static final String EDITOR = "editor";
	public static final String TABSIZE = EDITOR + ".tabSize";
	public static final String INSERTSPACES = EDITOR + ".insertSpaces";

	public static final String EXTENSION = PREFIX + "extension";
	public static final String EXCLUDED_FROM_BUILD = EXTENSION + ".excludedFromBuild";

	public static final String TPDFILE = "tpdFile";
	public static final String PROJECTROOT = "projectRoot";

	public static final Configuration INSTANCE = new Configuration();

	/** Map of all known preferences. Values are stored as objects */
	private static Map<String,Object> preferences = new ConcurrentHashMap<>();

	/** List of registered preference change listeners */
	private static List<SimpleImmutableEntry<String,IPreferenceChangedListener>> listeners = new CopyOnWriteArrayList<>();

	public static final CountDownLatch INITIALIZED_LATCH = new CountDownLatch(1);

	private static final Object lock = new Object();

	private Configuration() {
		// Do nothing
	}

	/**
	 * Checks if the given preference exists in the store. 
	 * @param key
	 * @return
	 */
	public boolean contains(final String key) {
		return preferences.containsKey(key);
	}

	/**
	 * Gets a boolean value of the given preference.
	 * @param key
	 * @param defaultValue
	 * @return Value of the preference or the default value if the preference does not exist or not boolean.
	 */
	public boolean getBoolean(final String key, final boolean defaultValue) {
		return getBooleanObject(key, defaultValue).booleanValue();
	}

	/**
	 * Gets a Boolean object of the given preference.
	 * @param key
	 * @param defaultValue
	 * @return Value of the preference or the default value if the preference does not exist or not boolean.
	 */
	public Boolean getBooleanObject(final String key, final boolean defaultValue) {
		final Object value = preferences.get(key);
		if (value instanceof Boolean) {
			// To make sure that only Boolean.TRUE and Boolean.FALSE static instances are used
			return Boolean.TRUE.equals(value) ? Boolean.TRUE : Boolean.FALSE;
		}
		return Boolean.valueOf(defaultValue);
	}

	/**
	 * Gets a String value of the given preference.
	 * @param key
	 * @param defaultValue
	 * @return Value of the preference or the default value if the preference does not exist or not a string.
	 */
	public String getString(final String key, final String defaultValue) {
		final Object value = preferences.get(key);
		if (value instanceof String) {
			return (String)value;
		}
		return defaultValue;
	}

	/**
	 * Gets an integer value of the given preference.
	 * @param key
	 * @param defaultValue
	 * @return Value of the preference or the default value if the preference does not exist or not an integer.
	 */
	public int getInt(final String key, final int defaultValue) {
		final Object value = preferences.get(key);
		if (value instanceof Integer) {
			return (int)value;
		}
		return defaultValue;
	}

	/**
	 * Gets a list of objects.<br><br>
	 * <b>Note:</b> When parsing, JSON arrays are also converted to list of objects and not stored as arrays. 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public List<Object> getList(final String key, final List<Object> defaultValue) {
		final Object value = preferences.get(key);
		if (value instanceof ArrayList) {
			return Collections.unmodifiableList((List<?>)value);
		}
		return defaultValue;
	}

	/**
	 * Adds a preference change listener for a given preference.
	 * @param event Name of the preference to be watched
	 * @param listener Event handler to be called
	 */
	public void addPreferenceChangedListener(final String event, IPreferenceChangedListener listener) {
		listeners.add(new SimpleImmutableEntry<String,IPreferenceChangedListener>(event, listener));
	}

	/**
	 * Adds a preference change listener for a given list of preferences.
	 * @param events List of preferences to be watched
	 * @param listener Event handler to be called
	 */
	public void addPreferenceChangedListeners(final List<String> events, IPreferenceChangedListener listener) {
		events.forEach(e ->
			addPreferenceChangedListener(e, listener)
		);
	}

	/**
	 * Calls all event handlers watching the given preference.
	 * @param preference
	 */
	private static void firePreferenceChanged(final String preference) {
		listeners.stream()
			.filter(f -> f.getKey().equals(preference))
			.map(m -> m.getValue())
			.collect(Collectors.toList())
			.forEach(listener ->
				listener.changed(preference)
			);
	}

	/**
	 * Calls all event handlers watching the given preference.
	 * @param preference
	 * @param oldValue
	 */
	private static void firePreferenceChanged(final String preference, final Object oldValue) {
		listeners.stream()
			.filter(f -> f.getKey().equals(preference))
			.map(m -> m.getValue())
			.collect(Collectors.toList())
			.forEach(listener ->
				listener.changed(preference, oldValue)
			);
	}

	public void addPreference(final String preference, final Object defaultValue) {
		preferences.put(preference, defaultValue);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		preferences.entrySet().stream()
			.sorted(Map.Entry.<String,Object>comparingByKey())
			.forEach(pref ->
				sb.append(pref.getKey()).append(": ").append(pref.getValue()).append('\n')
			);
		return sb.toString();
	}

	/**
	 * Sends a configuration request to the client and waits for the response.
	 */
	public CompletableFuture<Void> getConfiguration() {
		final String[] sections = { Configuration.EDITOR, GeneralConstants.TITAN };
		final List<ConfigurationItem> itemList = new ArrayList<>();
		for (String section : sections) {
			final ConfigurationItem item = new ConfigurationItem();
			item.setSection(section);
			itemList.add(item);
		}
		final ConfigurationParams configParams = new ConfigurationParams();
		configParams.setItems(itemList);

		return TitanLanguageServer.getClient().configuration(configParams).thenAccept(json -> {
			for (int i = 0; i < sections.length; i++) {
				final StringBuilder sb = new StringBuilder();
				sb.append("{\"").append(sections[i]).append("\": ")
					.append(json.get(i).toString()).append("}");
				parseConfiguration(sb.toString());
			}
		});
	}

	/**
	 * Parses the JSON object containing client side preference settings.
	 * Uses recursion to handle arbitrary depth of settings. 
	 * @param json
	 */
	public boolean parseConfiguration(final String json) {
		synchronized (lock) {
			try {
				final Map<String, Object> entryMap = new HashMap<>();
				JsonParser parser = Json.createParser(new StringReader(json));
				iterateJson(parser, GeneralConstants.EMPTY_STRING, entryMap);
				entryMap.forEach((pref, value) -> {
					if (!pref.equals(Configuration.EXCLUDED_FROM_BUILD) &&
							(!preferences.containsKey(pref) || !preferences.get(pref).equals(pref))) {
						preferences.put(pref, value);
						firePreferenceChanged(pref);
					}
				});
				// EXCLUDED_FROM_BUILD is handled by the TitanLspExtensionsService#setExcludedState
			} catch (Exception e) {
				TitanLogger.logError(e);
				return false;
			}

			TitanLogger.logDebug(INSTANCE.toString());
			INITIALIZED_LATCH.countDown();
			return true;
		}
	}

	public static void iterateJson(JsonParser parser, final String prefix, final Map<String,Object> map) {
		String key = null;
		Object value = null;
		String entryName = GeneralConstants.EMPTY_STRING;
		boolean isArray = false;
		List<Object> arrayValue = new ArrayList<>();
		while (parser.hasNext()) {
			final Event event = parser.next();
			switch (event) {
			case KEY_NAME:
				key = parser.getString();
				entryName = prefix + (!prefix.isEmpty() ? GeneralConstants.DOT : GeneralConstants.EMPTY_STRING) + key;
				break;
			case VALUE_STRING:
				value = parser.getString();
				break;
			case VALUE_FALSE:
				value = false;
				break;
			case VALUE_TRUE:
				value = true;
				break;
			case VALUE_NUMBER:
				value = parser.getInt();
				break;
			case START_ARRAY:
				isArray = true;
				continue;
			case START_OBJECT:
				iterateJson(parser, entryName, map);
				break;
			case END_OBJECT:
				return;
			case END_ARRAY:
				isArray = false;
				value = arrayValue;
				break;
			default:
				break;
			}
			if (isArray && value != null) {
				arrayValue.add(value);
				continue;
			}
			if (key != null && value != null) {
				map.put(entryName, value);
			}
		}
	}

	/**
	 * Returns all available preference keys from a given section.
	 * The prefixes are removed (including the trailing dot).
	 * 
	 * @return
	 */
	public Set<String> getAllKeys(final String section) {
		return preferences.keySet().stream()
			.filter(k ->
				k.startsWith(section + GeneralConstants.DOT) && k.length() > section.length() + 1
			)
			.map(k ->
				k.substring(section.length() + 1)
			)
			.collect(Collectors.toSet());
	}

	/**
	 * Updates the list of files that are excluded from build.
	 * It also fires the change listeners of this preference providing the 
	 * @param excludedFileList
	 */
	public void updateExcludedFromBuild(final List<String> excludedFileList) {
		if (excludedFileList == null) {
			return;
		}
		final Object o = preferences.put(Configuration.EXCLUDED_FROM_BUILD, excludedFileList);
		if (o instanceof List) {
			final List<Object> oldList = Collections.unmodifiableList((List<?>)o);
			if (!excludedFileList.equals(oldList)) {
				firePreferenceChanged(Configuration.EXCLUDED_FROM_BUILD, oldList);
			}
		} else {
			firePreferenceChanged(Configuration.EXCLUDED_FROM_BUILD, null);
		}
	}
}
