/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types.subtypes;

import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.templates.LengthRestriction;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a length sub-type restriction as it was parsed.
 *
 * @author Adam Delic
 * */
public final class Length_ParsedSubType implements IParsedSubType {
	private final LengthRestriction length;

	public Length_ParsedSubType(final LengthRestriction length) {
		this.length = length;
	}

	@Override
	/** {@inheritDoc} */
	public ParsedSubType_type getSubTypetype() {
		return ParsedSubType_type.LENGTH_PARSEDSUBTYPE;
	}

	public LengthRestriction getLength() {
		return length;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IParsedSubType.super.updateSyntax(reparser, isDamaged);
		length.updateSyntax(reparser, false);
		reparser.updateLocation(length.getLocation());
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return length.getLocation();
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		length.findReferences(referenceFinder, foundIdentifiers);
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		if (!length.accept(v)) {
			return false;
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}
}
