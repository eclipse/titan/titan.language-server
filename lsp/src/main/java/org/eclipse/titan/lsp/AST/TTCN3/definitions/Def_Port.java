/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimensions;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The Def_Port class represents TTCN3 port definitions.
 *
 * @author Kristof Szabados
 * */
public final class Def_Port extends Definition {
	private static final String FULLNAMEPART1 = ".<type>";
	private static final String FULLNAMEPART2 = ".<dimensions>";
	public static final String PORTEXPECTED = "Port type expected";
	public static final String TYPEEXPECTED = "Type reference expected";

	private static final String KIND = "port definition";

	private final Reference portTypeReference;
	private Port_Type portType = null;
	private final ArrayDimensions dimensions;

	public Def_Port(final Identifier identifier, final Reference portTypeReference, final ArrayDimensions dimensions) {
		super(identifier);
		this.portTypeReference = portTypeReference;
		this.dimensions = dimensions;

		if (portTypeReference != null) {
			portTypeReference.setFullNameParent(this);
		}
		if (dimensions != null) {
			dimensions.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_PORT;
	}

	public static String getKind() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (dimensions != null) {
			dimensions.setMyScope(scope);
		}
		if (portTypeReference != null) {
			portTypeReference.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (portTypeReference == child) {
			return builder.append(FULLNAMEPART1);
		} else if (dimensions == child) {
			return builder.append(FULLNAMEPART2);
		}

		return builder;
	}

	public ArrayDimensions getDimensions() {
		return dimensions;
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return "port";
	}

	@Override
	/** {@inheritDoc} */
	public Port_Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return portType;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;

		NamingConventionHelper.checkConvention(NamingConventionElement.Port, identifier, this);
		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (portTypeReference == null) {
			return;
		}

		final Assignment assignment = portTypeReference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			return;
		}

		if (Assignment_type.A_TYPE.semanticallyEquals(assignment.getAssignmentType()) && assignment.getType(timestamp) != null) {
			IType type = assignment.getType(timestamp);
			type = type.getTypeRefdLast(timestamp);
			if (type != null && !type.getIsErroneous(timestamp)) {
				switch (type.getTypetype()) {
				case TYPE_PORT:
					portType = (Port_Type) type;
					break;
				case TYPE_REFERENCED:
					break;
				default:
					portTypeReference.getLocation().reportSemanticError(PORTEXPECTED);
					break;
				}
			}
		} else {
			portTypeReference.getLocation().reportSemanticError(TYPEEXPECTED);
		}

		if (dimensions != null) {
			dimensions.check(timestamp);
		}

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

	}

	@Override
	/** {@inheritDoc} */
	public boolean checkIdentical(final CompilationTimeStamp timestamp, final Definition definition) {
		check(timestamp);
		definition.check(timestamp);

		if (!Assignment_type.A_PORT.semanticallyEquals(definition.getAssignmentType())) {
			location.reportSemanticError(MessageFormat.format(
					"Local definition `{0}'' is a port, but the definition inherited from component type `{1}'' is a {2}",
					identifier.getDisplayName(), definition.getMyScope().getFullName(), definition.getAssignmentName()));
			return false;
		}

		final Def_Port otherPort = (Def_Port) definition;
		if (!portType.isIdentical(timestamp, otherPort.portType)) {
			final String mesage = MessageFormat.format(
					"Local port `{0}'' has type `{1}'', but the port inherited from component type `{2}'' has type `{3}''",
					identifier.getDisplayName(), portType.getTypename(), otherPort.getMyScope().getFullName(),
					otherPort.portType.getTypename());
			portTypeReference.getLocation().reportSemanticError(mesage);
			return false;
		}

		if (dimensions != null) {
			if (otherPort.dimensions != null) {
				if (!dimensions.isIdenticial(timestamp, otherPort.dimensions)) {
					location.reportSemanticError(MessageFormat
							.format("Local port `{0}'' and the port inherited from component type `{1}'' have different array dimensions",
									identifier.getDisplayName(), otherPort.getMyScope().getFullName()));
					return false;
				}
			} else {
				location.reportSemanticError(MessageFormat
						.format("Local definition `{0}'' is a port array, but the definition inherited from component type `{1}'' is a single port",
								identifier.getDisplayName(), otherPort.getMyScope().getFullName()));
				return false;
			}
		} else if (otherPort.dimensions != null) {
			location.reportSemanticError(MessageFormat
					.format("Local definition `{0}'' is a single port, but the definition inherited from component type `{1}'' is a port array",
							identifier.getDisplayName(), otherPort.getMyScope().getFullName()));
			return false;
		}

		return true;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					throw new ReParseException(result);
				}
			}

			if (portTypeReference != null) {
				portTypeReference.updateSyntax(reparser, false);
				reparser.updateLocation(portTypeReference.getLocation());
			}

			if (dimensions != null) {
				dimensions.updateSyntax(reparser, false);
			}

			if (withAttributesPath != null) {
				withAttributesPath.updateSyntax(reparser, false);
				reparser.updateLocation(withAttributesPath.getLocation());
			}

			if (!enveloped) {
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (portTypeReference != null) {
			portTypeReference.updateSyntax(reparser, false);
			reparser.updateLocation(portTypeReference.getLocation());
		}

		if (dimensions != null) {
			dimensions.updateSyntax(reparser, false);
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (portTypeReference != null) {
			portTypeReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (dimensions != null) {
			dimensions.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (portTypeReference != null && !portTypeReference.accept(v)) {
			return false;
		}
		if (dimensions != null && !dimensions.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}
		
		final IType assType = getType(getLastTimeChecked());
		hoverContent.addStyledText(getAssignmentName(), Ttcn3HoverContent.BOLD).addText(" ");
		hoverContent.addStyledText(assType != null ? assType.getTypename() : "<?>")
			.addStyledText(" ").addStyledText(getFullName());

		if (hasDocumentComment()) {
			dc.addDescsContent(hoverContent);
			dc.addMembersContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding)
			.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = super.getCompletionItem();
		item.setKind(CompletionItemKind.Interface);
		return item;
	}
}
