/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.TitanLanguageServer.ServerMode;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.brokenpartsanalyzers.BrokenPartsViaReferences;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.core.LspMarker;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.parsers.Status.FutureStatus;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.titanium.markers.handlers.Marker;
import org.eclipse.titan.lsp.titanium.markers.utils.AnalyzerCache;

/**
 * This is project level root of all parsing related activities. Every data that
 * was extracted from files while parsing them has its root here.
 * <p>
 * Outdated modules are kept around till the next semantic checking because,
 * when a file is being edited its AST although being outdated might be queried
 * by code completion.
 * <p>
 * In not noted elsewhere all operations that modify the internal states are
 * executed in a parallel WorkspaceJob, which will have scheduling rules
 * required to access related resources.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class ProjectSourceParser implements ICancelBuild{
	private final Project project;

	private final ProjectSourceSyntacticAnalyzer syntacticAnalyzer;
	private final ProjectSourceSemanticAnalyzer semanticAnalyzer;

	/**
	 * Counts how many parallel full analyzer threads are running. Should
	 * not be more than 2. It can be 2 if there were changes while the
	 * existing analyzes run, which have to be checked by a subsequent
	 * check.
	 * */
	private final AtomicInteger fullAnalyzersRunning = new AtomicInteger();
	// The workspacejob of the last registered full analysis. External users
	// might need this to synchronize to.
	private volatile Future<?> lastFullAnalysis = null;
	private volatile CompletableFuture<FutureStatus> lastExtension = null;
	/**
	 * Counts how many parallel syntax only analyzer threads are running.
	 * Should not be more than 2. It can be 2 if there were changes while
	 * the existing analysis runs, which have to be checked by a subsequent
	 * check.
	 * */
	private final AtomicInteger syntaxAnalyzersRunning = new AtomicInteger();
	// The workspacejob of the last registered full analysis. External users
	// might need this to synchronize to.
	private volatile CompletableFuture<FutureStatus> lastSyntaxAnalyzes = null;

	private CompilationTimeStamp lastTimeChecked;

	/**
	 * Basic constructor initializing the class's members, for the given
	 * project.
	 *
	 * @param project
	 *                the project for which this instance will be
	 *                responsible for.
	 **/
	public ProjectSourceParser(final Project project) {
		this.project = project;
		syntacticAnalyzer = new ProjectSourceSyntacticAnalyzer(project, this);
		semanticAnalyzer = new ProjectSourceSemanticAnalyzer();
	}

	public void resetParser() {
		syntacticAnalyzer.reset();
		semanticAnalyzer.reset();
	}

	public ProjectSourceSyntacticAnalyzer getSyntacticAnalyzer() {
		return syntacticAnalyzer;
	}

	public ProjectSourceSemanticAnalyzer getSemanticAnalyzer() {
		return semanticAnalyzer;
	}

	public CompilationTimeStamp getLastTimeChecked() {
		return lastTimeChecked;
	}

	void setLastTimeChecked(final CompilationTimeStamp lastTimeChecked) {
		this.lastTimeChecked = lastTimeChecked;
	}

	/**
	 * Checks whether the internal data belonging to the provided file is
	 * out-dated.
	 *
	 * @param file
	 *                the file to check.
	 *
	 * @return true if the data was reported to be out-dated since the last
	 *         analysis.
	 * */
	public boolean isOutdated(final File file) {
		return syntacticAnalyzer.isOutdated(file.toPath()) || semanticAnalyzer.isOutdated(file.toPath());
	}

	/**
	 * Returns the module with the provided name, or null
	 * <p>
	 * Does check not only the actual project, but all referenced ones too.
	 *
	 * @param name
	 *                the name of the module to return
	 *
	 * @return the module having the provided name
	 * */
	public Module getModuleByName(final String name) {
		if (name == null) {
			return null;
		}

		return internalGetModuleByName(name, null, false);
	}

	/**
	 * Returns the TTCN-3 include file with the provided name, or null
	 * <p>
	 * Does check not only the actual project, but all referenced ones too.
	 *
	 * @param name
	 *                the name of the include file to return
	 * @param uptodateOnly
	 *                allow finding only the up-to-date modules.
	 *
	 * @return the TTCN-3 include file having the provided name
	 * */
	public Path getTTCN3IncludeFileByName(final String name) {
		if (name == null) {
			return null;
		}

		return internalTTCN3IncludeFileByName(name, null);
	}

	/**
	 * Returns the TTCN-3 include file with the provided name, or null.
	 *
	 * @param name
	 *                the name of the inlude file to return.
	 * @param visitedprojects
	 *                the list of project already visited, to break infinite
	 *                loops.
	 * @param uptodateOnly
	 *                allow finding only the up-to-date modules.
	 *
	 * @return the TTCN-3 include file having the provided name
	 * */
	private Path internalTTCN3IncludeFileByName(final String name, final List<Project> visitedprojects) {
		Path tempFile;
		tempFile = syntacticAnalyzer.internalGetTTCN3IncludeFileByName(name);
		if (tempFile != null) {
			return tempFile;
		}

		List<Project> visited = visitedprojects;
		if (visited == null) {
			visited = new ArrayList<Project>();
		}
		visited.add(project);

		return null;
	}

	public Collection<Module> getModules() {
		return semanticAnalyzer.internalGetModules();
	}

	/**
	 * Returns the module with the provided name, or null.
	 *
	 * @param name
	 *                the name of the module to return.
	 * @param visitedprojects
	 *                the list of project already visited, to break infinite
	 *                loops.
	 * @param uptodateOnly
	 *                allow finding only the up-to-date modules.
	 *
	 * @return the module having the provided name
	 * */
	private Module internalGetModuleByName(final String name, final List<Project> visitedprojects, final boolean uptodateOnly) {
		Module tempModule = null;
		tempModule = semanticAnalyzer.internalGetModuleByName(name, uptodateOnly);
		if (tempModule != null) {
			return tempModule;
		}

		List<Project> visited = visitedprojects;
		if (visited == null) {
			visited = new ArrayList<Project>();
		}
		visited.add(project);

		return null;
	}

	/**
	 * Returns the actually known module's names.
	 * <p>
	 * Does check not only the actual project, but all referenced ones too.
	 *
	 * @return a set of the module names known in this project or in the
	 *         ones referenced.
	 * */
	public Set<String> getKnownModuleNames() {
		return internalGetKnownModuleNames(new ArrayList<Project>());
	}

	/**
	 * Returns the actually known module's names.
	 * <p>
	 * Does check not only the actual project, but all referenced ones too.
	 *
	 * @param visitedprojects
	 *                the list of project already visited, to break infinite
	 *                loops.
	 *
	 * @return a set of the module names known in this project or in the
	 *         ones referenced.
	 * */
	private Set<String> internalGetKnownModuleNames(final List<Project> visitedprojects) {
		final Set<String> temp = new HashSet<String>();

		if (visitedprojects.contains(project)) {
			return temp;
		}

		temp.addAll(semanticAnalyzer.internalGetKnownModuleNames());

		visitedprojects.add(project);

		return temp;
	}

	/**
	 * Returns the name of the module contained in the provided file, or
	 * null.
	 *
	 * @param file
	 *                the file whose module we are interested in
	 *
	 * @return the name of the module found in the file, or null
	 * */
	public String containedModuleName(final Path file) {
		final Module module = semanticAnalyzer.getModulebyFile(file);
		if (module != null) {
			return module.getName();
		}

		return null;
	}

	/**
	 * Returns the module contained in the provided file, or
	 * null.
	 *
	 * @param file
	 *                the file whose module we are interested in
	 *
	 * @return the module found in the file, or null
	 * */
	public Module containedModule(final Path file) {
		return semanticAnalyzer.getModulebyFile(file);
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date.
	 * <p>
	 * Stores that this file is out of date for later
	 * <p>
	 *
	 * @param outdatedFile
	 *                the file which seems to have changed
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> reportOutdating(final Path outdatedPath) {
		return CompletableFuture.supplyAsync(() -> {
			syntacticAnalyzer.reportOutdating(outdatedPath);
			semanticAnalyzer.reportSemanticOutdating(outdatedPath);
			return FutureStatus.OK_STATUS;
		});
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date. This version does not mark the
	 * semantic data out-of-date.
	 * <p>
	 * Stores that this file is out of date for later usage
	 * <p>
	 *
	 * @param outdatedFile
	 *                the file which seems to have changed
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> reportSyntacticOutdatingOnly(final File outdatedFile) {
		return CompletableFuture.supplyAsync(() -> {
			syntacticAnalyzer.reportSyntacticOutdatingOnly(outdatedFile.toPath());
			return FutureStatus.OK_STATUS;
		});
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date.
	 * <p>
	 * Stores that this file is out of date for later
	 * <p>
	 *
	 * @param outdatedFiles
	 *                the file which seems to have changed
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> reportOutdating(final List<File> outdatedFiles) {
		return CompletableFuture.supplyAsync(() -> {
			for (final File file : outdatedFiles) {
				syntacticAnalyzer.reportOutdating(file.toPath());
			}
			return FutureStatus.OK_STATUS;
		});		
	}

	/**
	 * Reports that the semantic meaning of the provided file might have
	 * changed and so it's stored information became out of date.
	 * <p>
	 * Stores that this file is semantically out of date for later
	 * <p>
	 *
	 * @param outdatedFile
	 *                the file which seems to have changed
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> reportSemanticOutdating(final List<Path> outdatedFiles) {
		return CompletableFuture.supplyAsync(() -> {
			for (final Path file : outdatedFiles) {
				for (final Path outdatedFile : outdatedFiles) {
					semanticAnalyzer.reportSemanticOutdating(outdatedFile);
				}
			}
			return FutureStatus.OK_STATUS;
		});
	}

	/**
	 * Force the next semantic analyzation to reanalyze everything.
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> clearSemanticInformation() {
		return CompletableFuture.supplyAsync(() -> {
			semanticAnalyzer.clearSemanticInformation();
			return FutureStatus.OK_STATUS;
		});
	}

	/**
	 * The entry point of incremental parsing.
	 * <p>
	 * Handles the data storages, calls the module level incremental parser
	 * on the file, and if everything fails does a full parsing to correct
	 * possibly invalid states.
	 *
	 * @param file
	 *                the edited file
	 * @param reparser
	 *                the parser doing the incremental parsing.
	 *
	 * @return the WorkspaceJob in which the operation is running
	 * */
	public CompletableFuture<FutureStatus> updateSyntax(final Path file, final TTCN3ReparseUpdater reparser) {
		return CompletableFuture.supplyAsync(() -> {
			final double parserStart = System.nanoTime();

			syntacticAnalyzer.updateSyntax(file, reparser);
			TitanLogger.logDebug("Refreshing the syntax took " + (System.nanoTime() - parserStart) * (1e-9) + " secs");

			// FIXME
			final TTCN3Module actualModule = (TTCN3Module)GlobalParser.getProjectSourceParser(Project.INSTANCE).containedModule(file);
			
			return FutureStatus.OK_STATUS;
		});
	}

	public void analyzeAll() {
		final CompilationTimeStamp compilationCounter = CompilationTimeStamp.getNewCompilationCounter();
		BrokenPartsViaReferences selectionMethod = null;

		TitanLogger.logDebug("Started on-the-fly analysis of project");

		Project proj = Project.INSTANCE;
		try {
			long absoluteStart = System.nanoTime();
			GlobalParser.getProjectSourceParser(proj).syntacticAnalyzer.internalDoAnalyzeSyntactically();
			TitanLogger.logDebug("The whole analysis block took " + (System.nanoTime() - absoluteStart)
					* (1e-9) + " seconds to complete");
		} catch (Exception e) {
			TitanLogger.logError(e);
		}
		
		selectionMethod = ProjectSourceSemanticAnalyzer.analyzeMultipleProjectsSemantically(proj, compilationCounter);

		try {
			GlobalParser.getConfigSourceParser(proj).doSyntaticAnalysis().get();
			GlobalParser.getConfigSourceParser(proj).doSemanticCheck();
		} catch (ExecutionException e) {
			TitanLogger.logError(e);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}

		lastTimeChecked = compilationCounter;
		
		if (isTitaniumNeeded()) {
			synchronized (proj) {
				if (isBuildCancelled()) {
					return;
				}
				final List<Marker> markers = AnalyzerCache.withPreference().analyzeProject(proj);
				for (final Marker marker : markers) {
					final ProjectItem item = marker.getResource();
					// TODO: Check in which cases item can be null 
					// (Remove null check, then turn Configuration.ENABLE_TITANIUM on and off)

					if (item != null && !(marker.getLocation() instanceof NULL_Location)) {
						final Diagnostic diagnostic = new Diagnostic(
							marker.getLocation().getRange(),
							marker.getMessage(), marker.getSeverity(), GeneralConstants.TITANIUM);
						if (marker.hasQuickFix()) {
							diagnostic.setData(marker.getDiagnosticData());
						}
						final LspMarker reportedMarker = new LspMarker(diagnostic);
						if (marker.hasRelatedInformations()) {
							reportedMarker.addRelatedInformations(marker.getRelatedInformations());
						}
						item.addMarker(reportedMarker);
					}
				}
			}
		}
		
		if (TitanLanguageServer.getMode() == ServerMode.LanguageServer && selectionMethod != null) {
			for (final Module module : selectionMethod.getModulesToCheck()) {
				if (isBuildCancelled()) {
					return;
				}
				module.postCheck();
				final Path path = module.getIdentifier().getLocation().getFile().toPath();
				final ProjectItem projectItem = Project.INSTANCE.getProjectItem(path);
				if (projectItem != null) {
					final List<Diagnostic> markerList = projectItem.getMarkerList(); 
					if (markerList != null) {
						PublishDiagnosticsParams diagpar;
						if (projectItem.isUntitled()) {
							final String fname = "untitled:" + path.getFileName().toString();
							diagpar = new PublishDiagnosticsParams(fname, projectItem.getMarkerList());
						} else {
							diagpar = new PublishDiagnosticsParams(path.toUri().toString(), projectItem.getMarkerList());
						}
						if (diagpar != null) {
							TitanLanguageServer.getClient().publishDiagnostics(diagpar);
						}
					}
				}
			}
		}
	}

	private boolean isTitaniumNeeded() {
		return Configuration.INSTANCE.getBoolean(Configuration.ENABLE_TITANIUM, false) ||
				Configuration.INSTANCE.getBoolean(Configuration.REPORT_KEYWORD_USED_AS_IDENTIFIER, false);
	}
}
