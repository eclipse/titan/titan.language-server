/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.utils;

import org.eclipse.titan.lsp.GeneralConstants;

public final class FileExtensionUtils {
	private FileExtensionUtils() {
		// Hide constructor
	}

	/**
	 * Checks if file extension (the last segment of the specified String)
	 * is supported by the language server.
	 * <p>
	 * Includes <code>ttcn, ttcn3, ttcnpp, asn, ans1, cfg</code>
	 * 
	 * @param filename
	 * @return whether the file has supported extension or not
	 */
	public static boolean isSupportedExtension(final String filename) {
		if (filename == null || !filename.contains(GeneralConstants.DOT)) {
			return false;
		}
		final String extension = filename.substring(filename.lastIndexOf(GeneralConstants.DOT_CHAR) + 1);
		switch (extension) {
		case "ttcn":
		case "ttcn3":
		case "ttcnpp":
		case "asn":
		case "asn1":
		case "cfg":
			return true;
		default:
			return false;
		}
	}
}
