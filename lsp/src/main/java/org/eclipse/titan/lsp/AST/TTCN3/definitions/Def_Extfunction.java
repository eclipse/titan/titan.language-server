/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferencingType;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.MessageEncoding_type;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.DecodeAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.EncodeAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ErrorBehaviorAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ErrorBehaviorList;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.PrintingAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.PrintingType;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.PrototypeAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.extensionattributeparser.ExtensionAttributeAnalyzer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The Def_ExtFunction class represents TTCN3 external function definitions.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Adam Knapp
 * */
public final class Def_Extfunction extends Def_FunctionBase {
	public enum ExternalFunctionEncodingType_type {
		/** manual encoding. */
		MANUAL,
		/** generated encoder function. */
		ENCODE,
		/** generated decoder function. */
		DECODE
	}

	private static final String FULLNAMEPART1 = ".<formal_parameter_list>";
	private static final String FULLNAMEPART2 = ".<type>";
	private static final String FULLNAMEPART3 = ".<errorbehavior_list>";
	private static final String PORTRETURNNOTALLOWED = "External functions can not return ports";

	private static final String KIND = "external function";

	private final TemplateRestriction.Restriction_type templateRestriction;
	private EncodingPrototype_type prototype;
	private Type inputType;
	private Type outputType;
	private ExternalFunctionEncodingType_type functionEncodingType;
	private MessageEncoding_type encodingType;
	private String encodingOptions;
	private ErrorBehaviorList errorBehaviorList;
	private PrintingType printingType;

	public Def_Extfunction(final Identifier identifier, final FormalParameterList formalParameters, final Type returnType,
			final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction) {
		super(identifier, null, false, false, false, false, formalParameters, returnType);
		assignmentType = (returnType == null) ? Assignment_type.A_EXT_FUNCTION : (returnsTemplate ? Assignment_type.A_EXT_FUNCTION_RTEMP
				: Assignment_type.A_EXT_FUNCTION_RVAL);
		formalParameterList.setMyDefinition(this);
		this.templateRestriction = templateRestriction;
		prototype = EncodingPrototype_type.NONE;
		functionEncodingType = ExternalFunctionEncodingType_type.MANUAL;
		encodingType = MessageEncoding_type.UNDEFINED;
		encodingOptions = null;
		errorBehaviorList = null;
		printingType = null;

		formalParameterList.setFullNameParent(this);
		if (this.returnType != null) {
			this.returnType.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return assignmentType;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (formalParameterList == child) {
			return builder.append(FULLNAMEPART1);
		} else if (returnType == child) {
			return builder.append(FULLNAMEPART2);
		} else if (errorBehaviorList == child) {
			return builder.append(FULLNAMEPART3);
		}

		return builder;
	}

	public EncodingPrototype_type getPrototype() {
		return prototype;
	}

	public Type getInputType() {
		return inputType;
	}

	public Type getOutputType() {
		return outputType;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		return getAssignmentName();
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		formalParameterList.setMyScope(scope);
		if (returnType != null) {
			returnType.setMyScope(scope);
		}
		scope.addSubScope(formalParameterList.getLocation(), formalParameterList);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		prototype = EncodingPrototype_type.NONE;
		functionEncodingType = ExternalFunctionEncodingType_type.MANUAL;
		encodingType = MessageEncoding_type.UNDEFINED;
		encodingOptions = null;
		errorBehaviorList = null;
		printingType = null;
		lastTimeChecked = timestamp;

		NamingConventionHelper.checkConvention(NamingConventionElement.ExternalFunction, identifier, this);
		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (returnType != null) {
			returnType.check(timestamp);
			final IType returnedType = returnType.getTypeRefdLast(timestamp);
			if (returnedType != null && Type_type.TYPE_PORT.equals(returnedType.getTypetype()) && returnType.getLocation() != null) {
				returnType.getLocation().reportSemanticError(PORTRETURNNOTALLOWED);
			}
		}

		formalParameterList.reset();
		formalParameterList.check(timestamp, getAssignmentType());

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
			analyzeExtensionAttributes(timestamp);
			checkPrototype(timestamp);
			checkFunctionType(timestamp);
		}
	}

	/**
	 * Convert and check the encoding attributes applied to this external function.
	 * @param timestamp the timestamp of the actual build cycle.
	 */
	public void analyzeExtensionAttributes(final CompilationTimeStamp timestamp) {
		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);

		List<AttributeSpecification> specifications = null;
		for (final SingleWithAttribute attribute : realAttributes) {
			if (Attribute_Type.Extension_Attribute.equals(attribute.getAttributeType())) {
				final Qualifiers qualifiers = attribute.getQualifiers();
				if (qualifiers == null || qualifiers.size() == 0) {
					if (specifications == null) {
						specifications = new ArrayList<AttributeSpecification>();
					}
					specifications.add(attribute.getAttributeSpecification());
				}
			}
		}

		if (specifications == null) {
			return;
		}

		final List<ExtensionAttribute> attributes = new ArrayList<ExtensionAttribute>();
		for (final AttributeSpecification specification : specifications) {
			final ExtensionAttributeAnalyzer analyzer = new ExtensionAttributeAnalyzer();
			analyzer.parse(specification);
			final List<ExtensionAttribute> temp = analyzer.getAttributes();
			if (temp != null) {
				attributes.addAll(temp);
			}
		}

		for (final ExtensionAttribute extensionAttribute : attributes) {
			switch (extensionAttribute.getAttributeType()) {
			case PROTOTYPE:
				if (EncodingPrototype_type.NONE.equals(prototype)) {
					prototype = ((PrototypeAttribute) extensionAttribute).getPrototypeType();
				} else {
					location.reportSemanticError("duplicate attribute `prototype'.");
				}
				break;
			case ENCODE:
				switch (functionEncodingType) {
				case MANUAL:
					break;
				case ENCODE:
					location.reportSemanticError("duplicate attribute `encode'.");
					break;
				case DECODE:
					location.reportSemanticError("`decode' and `encode' attributes cannot be used at the same time.");
					break;
				default:
					break;
				}
				encodingType = ((EncodeAttribute) extensionAttribute).getEncodingType();
				encodingOptions = ((EncodeAttribute) extensionAttribute).getOptions();
				functionEncodingType = ExternalFunctionEncodingType_type.ENCODE;
				break;
			case DECODE:
				switch (functionEncodingType) {
				case MANUAL:
					break;
				case ENCODE:
					location.reportSemanticError("`decode' and `encode' attributes cannot be used at the same time.");
					break;
				case DECODE:
					location.reportSemanticError("duplicate attribute `decode'.");
					break;
				default:
					break;
				}
				encodingType = ((DecodeAttribute) extensionAttribute).getEncodingType();
				encodingOptions = ((DecodeAttribute) extensionAttribute).getOptions();
				functionEncodingType = ExternalFunctionEncodingType_type.DECODE;
				break;
			case ERRORBEHAVIOR:
				if (errorBehaviorList == null) {
					errorBehaviorList = ((ErrorBehaviorAttribute) extensionAttribute).getErrrorBehaviorList();
				} else {
					errorBehaviorList.addAllBehaviors(((ErrorBehaviorAttribute) extensionAttribute).getErrrorBehaviorList());
				}
				break;
			case PRINTING:
				if (printingType == null) {
					printingType = ((PrintingAttribute) extensionAttribute).getPrintingType();
				} else {
					location.reportSemanticError("duplicate attribute `printing'.");
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Checks the prototype attribute set for this external function definition.
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	private void checkPrototype(final CompilationTimeStamp timestamp) {
		if (EncodingPrototype_type.NONE.equals(prototype)) {
			return;
		}

		// checking formal parameter list
		if (EncodingPrototype_type.CONVERT.equals(prototype)) {
			if (formalParameterList.size() == 1) {
				final FormalParameter parameter = formalParameterList.get(0);
				switch (parameter.getRealAssignmentType()) {
				case A_PAR_VAL:
				case A_PAR_VAL_IN:
					inputType = parameter.getType(timestamp);
					break;
				default: {
					final String message = MessageFormat
							.format("The parameter must be an `in'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), parameter.getAssignmentName());
					parameter.getLocation().reportSemanticError(message);
					break;
				}
				}
			} else {
				final String message = MessageFormat.format(
						"The external function must have one parameter instead of {0} for attribute `prototype({1})''",
						formalParameterList.size(), prototype.getName());
				formalParameterList.getLocation().reportSemanticError(message);
			}
		} else if (formalParameterList.size() == 2) {
			final FormalParameter firstParameter = formalParameterList.get(0);
			if (EncodingPrototype_type.SLIDING.equals(prototype)) {
				if (Assignment_type.A_PAR_VAL_INOUT.semanticallyEquals(firstParameter.getRealAssignmentType())) {
					final Type firstParameterType = firstParameter.getType(timestamp);
					final IType last = firstParameterType.getTypeRefdLast(timestamp);
					if (last.getIsErroneous(timestamp)) {
						inputType = firstParameterType;
					} else {
						switch (last.getTypetypeTtcn3()) {
						case TYPE_OCTETSTRING:
						case TYPE_CHARSTRING:
						case TYPE_BITSTRING:
							inputType = firstParameterType;
							break;
						default: {
							final String message = MessageFormat
									.format("The type of the first parameter must be `octetstring'' or `charstring'' for attribute `prototype({0})''",
											prototype.getName());
							firstParameter.getLocation().reportSemanticError(message);

							break;
						}
						}
					}
				} else {
					firstParameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The first parameter must be an `inout'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), firstParameter.getAssignmentName()));
				}
			} else {
				if (Assignment_type.A_PAR_VAL_IN.semanticallyEquals(firstParameter.getRealAssignmentType())) {
					inputType = firstParameter.getType(timestamp);
				} else {
					firstParameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The first parameter must be an `in'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), firstParameter.getAssignmentName()));
				}
			}

			final FormalParameter secondParameter = formalParameterList.get(1);
			if (Assignment_type.A_PAR_VAL_OUT.semanticallyEquals(secondParameter.getRealAssignmentType())) {
				outputType = secondParameter.getType(timestamp);
			} else {
				secondParameter.getLocation()
				.reportSemanticError(
						MessageFormat.format(
								"The second parameter must be an `out'' value parameter for attribute `prototype({0})'' instead of {1}",
								prototype.getName(), secondParameter.getAssignmentName()));
			}
		} else {
			formalParameterList.getLocation().reportSemanticError(
					MessageFormat.format("The function must have two parameters for attribute `prototype({0})'' instead of {1}",
							prototype.getName(), formalParameterList.size()));
		}

		// checking the return type
		if (EncodingPrototype_type.FAST.equals(prototype)) {
			if (returnType != null) {
				returnType.getLocation().reportSemanticError(
						MessageFormat.format("The external function cannot have return type fo attribute `prototype({0})''",
								prototype.getName()));
			}
		} else {
			if (returnType == null) {
				location.reportSemanticError(MessageFormat.format(
						"The external function must have a return type for attribute `prototype({0})''", prototype.getName()));
			} else {
				if (Assignment_type.A_FUNCTION_RTEMP.semanticallyEquals(assignmentType)) {
					returnType.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The external function must return a value instead of a template for attribute `prototype({0})''",
									prototype.getName()));
				}

				if (EncodingPrototype_type.CONVERT.equals(prototype)) {
					outputType = returnType;
				} else {
					final IType last = returnType.getTypeRefdLast(timestamp);

					if (!last.getIsErroneous(timestamp) && !Type_type.TYPE_INTEGER.equals(last.getTypetypeTtcn3())) {
						returnType.getLocation()
						.reportSemanticError(
								MessageFormat.format(
										"The return type of the function must be `integer'' instead of `{0}'' for attribute `prototype({1})''",
										returnType.getTypename(), prototype.getName()));
					}
				}
			}
		}
	}

	/**
	 * Checks that the encoding/decoding attributes set for this external function
	 * definition are valid according to the encoding/decoding type of the function.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	public void checkFunctionType(final CompilationTimeStamp timestamp) {
		switch (functionEncodingType) {
		case MANUAL:
			if (errorBehaviorList != null) {
				errorBehaviorList.getLocation().reportSemanticError(
						"Attribute `errorbehavior' can only be used together with `encode' or `decode'");
				errorBehaviorList.check(timestamp);
			}
			break;
		case ENCODE:
			switch (prototype) {
			case NONE:
				location.reportSemanticError("Attribute `encode' cannot be used without `prototype'");
				break;
			case BACKTRACK:
			case SLIDING:
				location.reportSemanticError(MessageFormat.format("Attribute `encode'' cannot be used without `prototype({0})''",
						prototype.getName()));
				break;
			default:
				break;
			}

			if (inputType != null) {
				if (inputType.hasEncoding(timestamp, encodingType, encodingOptions)) {
					switch (encodingType) {
					case XER:
						//TODO add check for XER
						break;
					case CUSTOM:
					case PER:
						if (prototype != EncodingPrototype_type.CONVERT) {
							location.reportSemanticError(MessageFormat.format("Only `prototype(convert)'' is allowed for `{0}'' encoding functions", encodingType.getEncodingName()));
						} else if (inputType instanceof Referenced_Type) {
							// let the input type know that this is its encoding function
							final IReferenceChain refChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
							final IType refd = ((IReferencingType) inputType).getTypeRefd(timestamp, refChain);
							refChain.release();
							refd.setEncodingFunction(encodingType == MessageEncoding_type.PER? "PER": encodingOptions, this);
						}
						break;
					default:
						break;
					}
				} else {
					if (encodingType == MessageEncoding_type.CUSTOM) {
						inputType.getLocation().reportSemanticError(MessageFormat.format("Input type `{0}'' does not support custom encoding `{1}''",
								inputType.getTypename(), encodingOptions));
					} else {
						inputType.getLocation().reportSemanticError(MessageFormat.format("Input type `{0}'' does not support `{1}'' encoding.",
								inputType.getTypename(), encodingType.getEncodingName()));
					}
				}
			}

			if (outputType != null) {
				switch (encodingType) {
				case TEXT: {
					final Type streamType = Type.getStreamType(encodingType, 0);
					final Type streamType2 = Type.getStreamType(encodingType, 1);
					final Type streamType3 = Type.getStreamType(encodingType, 2);
					if (!streamType.isIdentical(timestamp, outputType) &&
							!streamType2.isIdentical(timestamp, outputType) &&
							!streamType3.isIdentical(timestamp, outputType)) {
						outputType.getLocation().reportSemanticError(MessageFormat.format("The output type of {0} encoding should be `{1}'', `{2}'' or `{3}'' instead of `{4}''",
								encodingType.getEncodingName(), streamType.getTypename(), streamType2.getTypename(), streamType3.getTypename(), outputType.getTypename()));
					}
					break;
				}
				case CUSTOM:
				case PER:{
					// custom and PER encodings only support the bitstring stream type
					final Type streamType = Type.getStreamType(encodingType, 0);
					if (!streamType.isIdentical(timestamp, outputType)) {
						outputType.getLocation().reportSemanticError(MessageFormat.format("The output type of {0} encoding should be `{1}'' instead of `{2}''",
								encodingType.getEncodingName(), streamType.getTypename(), outputType.getTypename()));
					}
					break;
				}
				default:{
					final Type streamType = Type.getStreamType(encodingType, 0);
					final Type streamType2 = Type.getStreamType(encodingType, 1);
					if (!streamType.isIdentical(timestamp, outputType) &&
							!streamType2.isIdentical(timestamp, outputType)) {
						outputType.getLocation().reportSemanticError(MessageFormat.format("The output type of {0} encoding should be `{1}'' or `{2}'' instead of `{3}''",
								encodingType.getEncodingName(), streamType.getTypename(), streamType2.getTypename(), outputType.getTypename()));
					}
					break;
				}
				}
			}
			if (errorBehaviorList != null) {
				errorBehaviorList.check(timestamp);
			}
			if (printingType != null) {
				printingType.check(timestamp);
			}
			break;
		case DECODE:
			if (EncodingPrototype_type.NONE.equals(prototype)) {
				location.reportSemanticError("Attribute `decode' cannot be used without `prototype'");
			}

			if (inputType != null) {
				switch (encodingType) {
				case TEXT: {
					final Type streamType = Type.getStreamType(encodingType, 0);
					final Type streamType2 = Type.getStreamType(encodingType, 1);
					final Type streamType3 = Type.getStreamType(encodingType, 2);
					if (!streamType.isIdentical(timestamp, inputType) &&
							!streamType2.isIdentical(timestamp, inputType) &&
							!streamType3.isIdentical(timestamp, inputType)) {
						inputType.getLocation().reportSemanticError(MessageFormat.format("The input type of {0} decoding should be `{1}'', `{2}'' or `{3}'' instead of `{4}''",
								encodingType.getEncodingName(), streamType.getTypename(), streamType2.getTypename(), streamType3.getTypename(), inputType.getTypename()));
					}
					break;
				}
				case CUSTOM:
				case PER:{
					// custom and PER encodings only support the bitstring stream type
					final Type streamType = Type.getStreamType(encodingType, 0);
					if (!streamType.isIdentical(timestamp, inputType)) {
						inputType.getLocation().reportSemanticError(MessageFormat.format("The input type of {0} decoding should be `{1}'' instead of `{2}''",
								encodingType.getEncodingName(), streamType.getTypename(), inputType.getTypename()));
					}
					break;
				}
				default:{
					final Type streamType = Type.getStreamType(encodingType, 0);
					final Type streamType2 = Type.getStreamType(encodingType, 1);
					if (!streamType.isIdentical(timestamp, inputType) &&
							!streamType2.isIdentical(timestamp, inputType)) {
						outputType.getLocation().reportSemanticError(MessageFormat.format("The input type of {0} decoding should be `{1}'' or `{2}'' instead of `{3}''",
								encodingType.getEncodingName(), streamType.getTypename(), streamType2.getTypename(), inputType.getTypename()));
					}
					break;
				}
				}
			}
			if (outputType != null && !outputType.hasEncoding(timestamp, encodingType, encodingOptions)) {
				if (encodingType == MessageEncoding_type.CUSTOM) {
					outputType.getLocation().reportSemanticError(MessageFormat.format("Output type `{0}'' does not support custom encoding `{1}''", outputType.getTypename(), encodingOptions));
				} else {
					outputType.getLocation().reportSemanticError(MessageFormat.format("Output type `{0}'' does not support {1} encoding", outputType.getTypename(), encodingType.getEncodingName()));
				}
			} else {
				if (encodingType == MessageEncoding_type.CUSTOM || encodingType == MessageEncoding_type.PER) {
					if (prototype != EncodingPrototype_type.SLIDING) {
						getLocation().reportSemanticError(MessageFormat.format("Only `prototype(sliding)'' is allowed for {0} decoding functions", encodingType.getEncodingName()));
					} else if (outputType instanceof IReferencingType){
						final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
						((IReferencingType)outputType).getTypeRefd(timestamp, chain).setDecodingFunction(encodingType == MessageEncoding_type.PER ? "PER" : encodingOptions, this);
						chain.release();
					}
					// functionEncodingType = ExternalFunctionEncodingType_type.MANUAL;
				}
			}
			if (errorBehaviorList != null) {
				errorBehaviorList.check(timestamp);
			}
			break;
		default:
			// no other option possible
			break;
		}

		if (printingType != null && (functionEncodingType != ExternalFunctionEncodingType_type.ENCODE ||
				encodingType != MessageEncoding_type.JSON)) {
			location.reportSemanticError("Attribute `printing' is only allowed for JSON encoding functions");
		}
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalDescription() {
		final StringBuilder nameBuilder = new StringBuilder(identifier.getDisplayName());
		nameBuilder.append('(');
		formalParameterList.getAsProposalDesriptionPart(nameBuilder);
		nameBuilder.append(')');
		return nameBuilder.toString();
	}

//	@Override
//	/** {@inheritDoc} */
//	public void addProposal(final ProposalCollector propCollector, final int index) {
//		final List<ISubReference> subrefs = propCollector.getReference().getSubreferences();
//		if (subrefs.size() <= index || Subreference_type.arraySubReference.equals(subrefs.get(index).getReferenceType())) {
//			return;
//		}
//
//		if (subrefs.size() == index + 1 && identifier.getName().toLowerCase(Locale.ENGLISH).startsWith(subrefs.get(index).getId().getName().toLowerCase(Locale.ENGLISH))) {
//			final StringBuilder patternBuilder = new StringBuilder(identifier.getDisplayName());
//			patternBuilder.append('(');
//			formalParameterList.getAsProposalPart(patternBuilder);
//			patternBuilder.append(')');
//			propCollector.addTemplateProposal(identifier.getDisplayName(),
//					new Template(getProposalDescription(), "", propCollector.getContextIdentifier(), patternBuilder.toString(),
//							false), TTCN3CodeSkeletons.SKELETON_IMAGE);
//			super.addProposal(propCollector, index);
//		} else if (subrefs.size() > index + 1 && returnType != null
//				&& Subreference_type.parameterisedSubReference.equals(subrefs.get(index).getReferenceType())
//				&& identifier.getName().equals(subrefs.get(index).getId().getName())) {
//			// perfect match
//			returnType.addProposal(propCollector, index + 1);
//		}
//	}
//
//	@Override
//	/** {@inheritDoc} */
//	public void addDeclaration(final DeclarationCollector declarationCollector, final int index) {
//		final List<ISubReference> subrefs = declarationCollector.getReference().getSubreferences();
//		if (subrefs.size() <= index || Subreference_type.arraySubReference.equals(subrefs.get(index).getReferenceType())) {
//			return;
//		}
//
//		if (identifier.getName().equals(subrefs.get(index).getId().getName())) {
//			if (subrefs.size() > index + 1 && returnType != null) {
//				returnType.addDeclaration(declarationCollector, index + 1);
//			} else {
//				declarationCollector.addDeclaration(this);
//			}
//		}
//	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		final StringBuilder text = new StringBuilder(identifier.getDisplayName());
		if (formalParameterList == null || lastTimeChecked == null) {
			return text.toString();
		}

		text.append('(');
		for (int i = 0; i < formalParameterList.size(); i++) {
			if (i != 0) {
				text.append(", ");
			}
			final FormalParameter parameter = formalParameterList.get(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				text.append("timer");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					text.append("Unknown type");
				} else {
					text.append(type.getTypename());
				}
			}
		}
		text.append(')');
		return text.toString();
	}

	@Override
	/** {@inheritDoc} */
	public TemplateRestriction.Restriction_type getTemplateRestriction() {
		return templateRestriction;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					throw new ReParseException(result);
				}
			}
			if (formalParameterList != null) {
				if (enveloped) {
					formalParameterList.updateSyntax(reparser, false);
					reparser.updateLocation(formalParameterList.getLocation());
				} else if (reparser.envelopsDamage(formalParameterList.getLocation())) {
					formalParameterList.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(formalParameterList.getLocation());
				}
			}
			if (returnType != null) {
				if (enveloped) {
					returnType.updateSyntax(reparser, false);
					reparser.updateLocation(returnType.getLocation());
				} else if (reparser.envelopsDamage(returnType.getLocation())) {
					returnType.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(returnType.getLocation());
				}
			}
			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					withAttributesPath.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(withAttributesPath.getLocation());
				}
			}

			if (!enveloped) {
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (formalParameterList != null) {
			formalParameterList.updateSyntax(reparser, false);
			reparser.updateLocation(formalParameterList.getLocation());
		}
		if (returnType != null) {
			returnType.updateSyntax(reparser, false);
			reparser.updateLocation(returnType.getLocation());
		}
		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (formalParameterList != null) {
			formalParameterList.findReferences(referenceFinder, foundIdentifiers);
		}
		if (returnType != null) {
			returnType.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (formalParameterList != null && !formalParameterList.accept(v)) {
			return false;
		}
		if (returnType != null && !returnType.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}

		hoverContent.addStyledText(KIND, Ttcn3HoverContent.BOLD);

		hoverContent
		.addText("\n\n")
		.addText(getFullName())
		.addText("\n\n");

		hoverContent.addText(returnType != null ? " return " + returnType.getTypename() + "\n\n" : "\n\n");

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addParamsContent(hoverContent, formalParameterList);
			dc.addReturnContent(hoverContent);
			dc.addVerdictsContent(hoverContent);
			dc.addRequirementsContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		} else {
			if (formalParameterList != null && formalParameterList.size() > 0) {
				hoverContent.addStyledText("Parameters", Ttcn3HoverContent.BOLD).addText("\n\n");
				for (final FormalParameter param : formalParameterList) {
					String paramType = param.getFormalParamType();
					final StringBuilder sb = new StringBuilder(param.getIdentifier().getDisplayName());
					sb.append(" (").append(paramType != null ? paramType : "<?>").append(")");
					sb.append(" ").append(param.getType(lastTimeChecked).getTypename());
					hoverContent.addText("- ").addIndentedText(sb.toString(), "\n");
				}
			}
		}
		hoverContent.addContent(HoverContentType.INFO);		
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(indentation).append(ICommentable.DESC_TAG).append(lineEnding);

		if (formalParameterList!= null && formalParameterList.size() > 0) {
			for (final FormalParameter param : formalParameterList) {
				sb.append(ind).append(ICommentable.PARAM_TAG).append(" ")
					.append(param.getIdentifier().getDisplayName()).append(lineEnding);
			}
		}

		if (returnType != null) {
			sb.append(ind).append(ICommentable.RETURN_TAG).append(lineEnding);
		}
		sb.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	@Override
	public IsIdenticalResult isIdentical(CompilationTimeStamp timestamp, Def_FunctionBase other) {
		// TODO Auto-generated method stub
		return null;
	}
}
