/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.GeneralConstants;

/**
 * Provides an interface for nodes in the AST that can have names,
 * or can be part of a naming chain.
 *
 * @author Kristof Szabados
 * */
public interface INamedNode {

	String EMPTY_STRING = GeneralConstants.EMPTY_STRING;
	String MODULENAMEPREFIX = GeneralConstants.AT;
	String COMMA = GeneralConstants.COMMA;
	String DOT = GeneralConstants.DOT;
	char   DOT_CHAR = GeneralConstants.DOT_CHAR;
	String DOUBLE_UNDERSCORE = GeneralConstants.DOUBLE_UNDERSCORE;
	String FEW = GeneralConstants.FEW;
	String IN = GeneralConstants.IN;
	String MANY = GeneralConstants.MANY;
	String MINUS_SIGN = GeneralConstants.MINUS_SIGN;
	char   MINUS_SIGN_CHAR = GeneralConstants.MINUS_SIGN_CHAR;
	String NULL = GeneralConstants.NULL;
	String OUT = GeneralConstants.OUT;
	String UNDERSCORE = GeneralConstants.UNDERSCORE;
	char   UNDERSCORE_CHAR = GeneralConstants.UNDERSCORE_CHAR;
	String COMPLEMENT = "complement";
	String CONJUNCT = "conjunct";
	String DEFINITIONS = "definitions";
	String ELLIPSIS = "...";
	String OMIT = "omit";
	String PERMUTATION = "permutation";
	String SUBSET = "subset";
	String SUPERSET = "superset";
	String LESSTHAN = "<";
	String MORETHAN = ">";
	String LESSMORE = "<>";
	String LEFTPARENTHESES = "(";
	String RIGHTPARENTHESES = ")";
	String PARENTHESESOPENCLOSE = "()";
	String SQUAREOPEN = "[";
	String SQUARECLOSE = "]";
	String SQUAREOPENCLOSE = "[]";
	String CURLYOPEN = "{";
	String CURLYCLOSE = "}";
	String CURLYOPENCLOSE = "{}";

	/**
	 * Sets the full name of the node.
	 *
	 * @param nameParent the name to be set
	 * */
	void setFullNameParent(INamedNode nameParent);

	/**
	 * @param child create the first part of the child's name
	 * @return the full name of the node
	 * */
	default StringBuilder getFullName(INamedNode child) {
		if (getNameParent() != null) {
			return getNameParent().getFullName(this);
		}
		return new StringBuilder();
	}

	/**
	 * @return the full name of the node
	 * */
	default String getFullName() {
		return getFullName(this).toString();
	}

	/**
	 * @return the naming parent of this node, or null if none
	 * */
	INamedNode getNameParent();
}
