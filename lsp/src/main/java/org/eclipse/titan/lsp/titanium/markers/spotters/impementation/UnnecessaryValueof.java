/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ValueofExpression;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class UnnecessaryValueof extends BaseModuleCodeSmellSpotter {
	public static final String TEXT = "Applying the `valueof' operation to '{0}' will result in the original value";

	public UnnecessaryValueof() {
		super(CodeSmellType.UNNECESSARY_VALUEOF);
		addStartNode(ValueofExpression.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof ValueofExpression) {
			final ValueofExpression exp = (ValueofExpression) node;
			final CompilationTimeStamp stamp = CompilationTimeStamp.getBaseTimestamp();
			if (exp.getIsErroneous(stamp) || exp.isUnfoldable(stamp, null)) {
				return;
			}
			final TemplateInstance inst = exp.getTemplateInstance();
			if (inst != null && inst.getDerivedReference() == null && inst.getTemplateBody().isValue(stamp)) {
				final String msg = MessageFormat.format(TEXT, inst.getTemplateBody().getValue().createStringRepresentation());
				problems.report(exp.getLocation(), msg);
			}
		}
	}
}
