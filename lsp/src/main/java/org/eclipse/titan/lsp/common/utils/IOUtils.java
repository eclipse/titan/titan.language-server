/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.titan.lsp.common.logging.TitanLogger;

/**
 * This is a utility class providing IO handling routines.
 */
public final class IOUtils {

	public static final Charset UTF_8_CHARSET = StandardCharsets.UTF_8;
	public static final Charset UTF_16_CHARSET = StandardCharsets.UTF_16LE;

	public static final class IOStreams {
		public Closeable closeable = null;
		public InputStream in = null;
		public OutputStream out = null;
		public IOStreams() { }
		public IOStreams(Closeable closeable, InputStream in, OutputStream out) {
			this.closeable = closeable;
			this.in = in; this.out = out;
		}
	}

	private IOUtils() {
		// Do nothing
	}

	/**
	 * Flush the contents of single string into a file, in a single step.
	 *
	 * @param file the file to flush the string to.
	 * @param data the string data to write into the file.
	 *
	 * @throws IOException when there was an error writing to the file.
	 * */
	public static void writeStringToFile(final File file, final String data) throws IOException {
		try (BufferedWriter outStream = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(file), IOUtils.UTF_8_CHARSET))) {
			outStream.write(data);
		}
	}

	/**
	 * Equivalent to Closeable.close(), except any exceptions will be ignored. This is typically used in finally blocks.
	 *
	 * see org.apache.commons.io.IOUtils.closeQuietly()
	 *
	 * @param closeable the objects to close, may be null or already closed
	 */
	public static void closeQuietly(final Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (final IOException e) {
				TitanLogger.logError("Error while closing a resource", e);
			}
		}
	}

	/**
	 * Equivalent to Closeable.close(), except any exceptions will be ignored. This is typically used in finally blocks.
	 *
	 * see org.apache.commons.io.IOUtils.closeQuietly()
	 *
	 * @param closeableArr the objects to close, may contain null or already closed objects
	 */
	public static void closeQuietly(final Closeable ... closeableArr) {
		for (final Closeable closeable : closeableArr) {
			closeQuietly(closeable);
		}
	}

	/**
	 * Converts the provided input stream into a single string.
	 *
	 * @param input the stream to read and convert.
	 *
	 * @return the string read from the provided input stream.
	 * */
	public static String inputStreamToString(final InputStream input) throws IOException {
		final Reader reader = new BufferedReader(new InputStreamReader(input, IOUtils.UTF_8_CHARSET));
		final StringBuilder content = new StringBuilder();
		final char[] buffer = new char[1024];
		int n = reader.read(buffer);

		while (n != -1) {
			content.append(buffer, 0, n);
			n = reader.read(buffer);
		}

		return content.toString();
	}

	public static void checkPipe(final String path) throws IOException {
		// Do nothing, as pipes are not supported yet
		return;
		/*final Path pipePath = Paths.get(path);
		if (!Files.isReadable(pipePath)) {
			throw new IOException("Pipe is unreadable");
		}
		if (!Files.isWritable(pipePath)) {
			throw new IOException("Pipe is unwritable");
		}*/
	}

	public static IOStreams openPipe(final String path) throws IOException {
		// Do nothing, as pipes are not supported yet
		return new IOStreams();
	}

	/**
	 * Decodes the specified URI according to the UTF-8 character set.
	 * <p>
	 * By default, the URIs are received from VS Code as escaped Strings,
	 * thus decoding them is not necessary to create {@link URI} objects.
	 * @param uri the URI to decode
	 * @return the decoded String
	 * @see StandardCharsets#UTF_8
	 */
	public static String getFileName(final String uri) {
		try {
			return URLDecoder.decode(uri, IOUtils.UTF_8_CHARSET)
					.replace("\\", "/")
//					.replace(" ", "%20")
					.replace("#", "%23");
//					.replace(":", "%3A");
		} catch (Exception e) {
			TitanLogger.logError(e);
		}
		return null;
	}

	/**
	 * Constructs a {@linkplain Path} object based on the specified URI or path String
	 * @param uri URI or path String
	 * @return the created Path object, or {@code null} on error
	 */
	public static Path getPath(final String uri) {
		try {
			final URI u = new URI(uri);
			return validateURIScheme(u) ? Path.of(u) : null;
		} catch (URISyntaxException e) {
			TitanLogger.logError(e);
		}
		return null;
	}

	public static Path getTextDocumentPath(TextDocumentIdentifier document) {
		return getPath(document.getUri());
	}

	private static boolean validateURIScheme(URI u) {
		if (u.getScheme() == null) {
			TitanLogger.logError("URI without scheme: " + u.toString());
		}
		// TODO: add checks for other schemes, when they become supported
		// maybe transformation or a schema specific provider will be required,
		// e.g. vsls (Visual Studio Live Share)
		switch (u.getScheme()) {
		case "file":
			return true;
		default:
			return false;
		}
	}
}
