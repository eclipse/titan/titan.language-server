/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class Variable_Entries extends ASTNode implements IIncrementallyUpdatable, ICollection<Variable_Entry> {
	private static final String FULLNAMEPART = ".ve_";

	private final List<Variable_Entry> entries;

	public Variable_Entries() {
		super();
		entries = new ArrayList<Variable_Entry>();
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0; i < entries.size(); i++) {
			if (entries.get(i) == child) {
				return builder.append(FULLNAMEPART).append(Integer.toString(i + 1));
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (Variable_Entry ve : entries) {
			ve.setMyScope(scope);
		}
	}

	public boolean add(final Variable_Entry entry) {
		if (entry != null) {
			entry.setFullNameParent(this);
			return entries.add(entry);
		}
		return false;
	}

	public int size() {
		return entries.size();
	}

	public Variable_Entry get(final int index) {
		return entries.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final Variable_Entry entry : entries) {
			entry.updateSyntax(reparser, isDamaged);
			reparser.updateLocation(entry.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final Variable_Entry ve : entries) {
			ve.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final Variable_Entry ve : entries) {
			if (!ve.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<Variable_Entry> iterator() {
		return entries.iterator();
	}
}
