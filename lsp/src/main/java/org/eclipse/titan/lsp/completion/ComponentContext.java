/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.InsertReplaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Range;

public class ComponentContext extends CompletionContext {

	public ComponentContext(CompletionContextInfo contextInfo) {
		super(contextInfo);
	}

	@Override
	public Either<List<CompletionItem>, CompletionList> getCompletions() {
		final String identifier = contextInfo.matcher.group(2);
		final Position replacementStart = identifier != null ? new Position(contextInfo.cursorPosition.getLine(), contextInfo.cursorPosition.getCharacter() - identifier.length()) : contextInfo.cursorPosition;
		final Range replacementRange = new Range(replacementStart, contextInfo.cursorPosition);
		final Set<CompletionItem> completions = new HashSet<>();

		if (contextInfo.module instanceof TTCN3Module) {
			final TTCN3Module module = (TTCN3Module)contextInfo.module;
			final List<Definition> visibleDefinitions = module.getAllVisibleDefinitions(contextInfo.cursorPosition);
			visibleDefinitions.forEach(def -> {
				final IType defType = def.getType(module.getLastCompilationTimeStamp());
				if (defType != null) {
					Type_type ttcnType = defType instanceof Referenced_Type ? ((Referenced_Type)defType).getTypeRefdLast(module.getLastCompilationTimeStamp()).getTypetypeTtcn3() : defType.getTypetype();
					if (ttcnType == Type_type.TYPE_COMPONENT) {
						completions.add(def.getCompletionItem());
					}
				}
			});

			completions.forEach(completionItem -> {
				final InsertReplaceEdit insRepEdit = new InsertReplaceEdit();
				completionItem.setInsertText(completionItem.getLabel());
				insRepEdit.setInsert(replacementRange);
				insRepEdit.setReplace(replacementRange);
				insRepEdit.setNewText(completionItem.getInsertText());
				completionItem.setTextEdit(Either.forRight(insRepEdit));
			});
		}
		return Either.forLeft(new ArrayList<CompletionItem>(completions));
	}

}
