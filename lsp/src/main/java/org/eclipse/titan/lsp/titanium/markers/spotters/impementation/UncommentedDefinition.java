/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.Arrays;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Testcase;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class UncommentedDefinition extends BaseModuleCodeSmellSpotter {
	public UncommentedDefinition() {
		super(CodeSmellType.UNCOMMENTED_FUNCTION);
		addStartNodes(Arrays.asList(Def_Altstep.class, Def_Function.class, Def_Testcase.class));
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof Definition) {
			final Definition s = (Definition) node;
			if (!s.hasDocumentComment()) {
				final String msg = MessageFormat.format("The {0} {1} should have a comment", s.getAssignmentName(),
						s.getIdentifier().getDisplayName());
				problems.report(s.getIdentifier().getLocation(), msg);
			}
		}
	}
}
