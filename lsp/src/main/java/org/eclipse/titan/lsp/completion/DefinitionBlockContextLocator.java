/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FriendModule;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ImportModule;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.core.Position;

public class DefinitionBlockContextLocator implements IContextLocator {
	private final Position cursorPosition;
	private final Definitions block;
	private final TTCN3Module module;
	private final String sourceText;
	private String contextString;
	private Definition actualDefinition;
	private Definition lastDefinitionBeforecursorPos;
	private ImportModule actualImportStatement;
	private FriendModule actualFriendModuleStatement;

	public DefinitionBlockContextLocator(final Definitions block, final TTCN3Module module, final String sourceText, final Position cursorPosition) {
		this.cursorPosition = cursorPosition;
		this.block = block;
		this.module = module;
		this.sourceText = sourceText;
		contextString = findContextString();
	}

	private boolean isCursorOnImportStatement() {
		for (ImportModule impMod : module.getImports()) {
			if (impMod.getLocation().containsPosition(cursorPosition)) {
				actualImportStatement = impMod;
				return true;
			}
		}
		return false;
	}

	private boolean isCursorOnFriendModuleStatement() {
		for (FriendModule friend : module.getFriendModules()) {
			if (friend.getLocation().containsPosition(cursorPosition)) {
				actualFriendModuleStatement = friend;
				return true;
			}
		}
		return false;
	}

	private boolean isCursorOnDefinition() {
		for (Definition def : block.getDefinitions()) {
			if (def.getLocation().containsPosition(cursorPosition)) {
				actualDefinition = def;
				return true;
			}
		}
		return false;
	}

	private boolean isCursorBetweenDefinitions() {
		Definition defBeforeCursor = null;
		for (Definition def : block.getDefinitions()) {
			if (cursorPosition.before(def.getLocation().getStartPosition())) {
				break;
			}
			if (cursorPosition.after(def.getLocation().getEndPosition())) {
				defBeforeCursor = def;
			}
		}

		if (defBeforeCursor != null) {
			lastDefinitionBeforecursorPos = defBeforeCursor;
			return true;
		}

		return false;
	}

	private String findContextString() {
		if (isCursorOnImportStatement()) {
			return extractContextStringFromSource(
					actualImportStatement.getLocation().getStartPosition(),
					cursorPosition,
					sourceText);
		}

		if (isCursorOnFriendModuleStatement()) {
			return extractContextStringFromSource(
					actualFriendModuleStatement.getLocation().getStartPosition(),
					cursorPosition,
					sourceText);
		}

		if (isCursorOnDefinition()) {
			return extractContextStringFromSource(
					actualDefinition.getLocation().getStartPosition(),
					cursorPosition,
					sourceText);
		}

		if(isCursorBetweenDefinitions()) {
			return extractContextStringFromSource(
					lastDefinitionBeforecursorPos.getLocation().getEndPosition(),
					cursorPosition,
					sourceText);
		}

		return extractContextStringFromSource(
				new Position(cursorPosition.getLine(), 0),
				cursorPosition,
				sourceText);
	}

	@Override
	public String getContextString() {
		return contextString;
	}

	public Definition getActualDefinition() {
		return actualDefinition;
	}

}
