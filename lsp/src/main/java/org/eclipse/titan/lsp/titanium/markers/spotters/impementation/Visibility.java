/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Group;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class Visibility extends BaseModuleCodeSmellSpotter {
	private static final Pattern VISIBILITY_PATTERN = Pattern.compile(".*(?:public|private|friend).*");
	private static final String REPORT = "The name {1} of the {0} contains visibility attributes";

	public Visibility() {
		super(CodeSmellType.VISIBILITY_IN_DEFINITION);
		addStartNodes(Arrays.asList(Definition.class, Group.class));
	}

	@Override
	protected void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof FormalParameter) {
			return;
		} else if (node instanceof Definition) {
			final Definition s = (Definition) node;
			final Identifier identifier = s.getIdentifier();
			check(identifier, s.getDescription(), problems);
		} else if (node instanceof Group) {
			final Group s = (Group) node;
			final Identifier identifier = s.getIdentifier();
			check(identifier, "group", problems);
		} else {
			return;
		}
	}

	protected void check(final Identifier identifier, final String description, final Problems problems) {
		final String displayName = identifier.getDisplayName();
		if (VISIBILITY_PATTERN.matcher(displayName).matches()) {
			final String msg = MessageFormat.format(REPORT, description, displayName);
			problems.report(identifier.getLocation(), msg);
		}
	}
}
