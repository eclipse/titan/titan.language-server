/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.parsers.asn1parser.ASN1Analyzer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ITtcn3FileReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3Analyzer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3FileReparser;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;

/**
 * Helper class to separate the responsibility of the source parser into smaller
 * parts. This class is responsible for handling the syntactic checking of the
 * source code of the projects
 *
 * @author Kristof Szabados
 * @author Jeno Attila Balasko
 * @author Miklos Magyari
 */
public final class ProjectSourceSyntacticAnalyzer {
	private static final String DEBUG_MESSAGE_TIME_INTERVAL = "**It took ({0}, {1}) {2} "
			+ " seconds for Titan to syntactically check {3}";

	private final Project project;
	private final ProjectSourceParser sourceParser;

	// file, module name
	private final Map<Path, String> uptodateFiles;
	// file : these are parsed, but contain such errors that we can not
	// determine even the module.
	private final Set<Path> highlySyntaxErroneousFiles;
	// file, module name might be outdated
	private final Map<Path, String> fileMap;
	// include files
	private final Map<String, Path> includeFileMap;
	Map<Path, List<TITANMarker>> unsupportedConstructMap;

	private volatile boolean syntacticallyOutdated = true;

	AtomicInteger lastProgressReported;
	AtomicInteger progressCounter;

	int nrOfItems;
//	
//	private static Vector<IUpdateSyntaxEventListener> eventListener = new Vector<>();

	/**
	 * A helper class to store parsed data, which was generated in parse
	 * threads running in parallel, but must be processed in a given fixed order.
	 */
	static final class TemporalParseData {
		private final Module module;
		private final Path file;
		private final List<TITANMarker> unsupportedConstructs;
		private final boolean hadParseErrors;

		public TemporalParseData(final Module module, final Path file, final List<TITANMarker> unsupportedConstructs,
				final boolean hadParseErrors) {
			this.module = module;
			this.file = file;
			this.unsupportedConstructs = unsupportedConstructs;
			this.hadParseErrors = hadParseErrors;
		}

		public Module getModule() {
			return module;
		}

		public Path getFile() {
			return file;
		}

		public List<TITANMarker> getUnsupportedConstructs() {
			return unsupportedConstructs;
		}

		public boolean hadParseErrors() {
			return hadParseErrors;
		}
	}

	abstract class ParserRunnable implements Runnable {
		protected int index;

		ParserRunnable(final int index) {
			this.index = index;
		}
	}

	public ProjectSourceSyntacticAnalyzer(final Project project, final ProjectSourceParser sourceParser) {
		this.project = project;
		this.sourceParser = sourceParser;

		uptodateFiles = new ConcurrentHashMap<Path, String>();
		highlySyntaxErroneousFiles = Collections.synchronizedSet(new HashSet<Path>());
		fileMap = new ConcurrentHashMap<Path, String>();
		includeFileMap = new ConcurrentHashMap<String, Path>();
		unsupportedConstructMap = new ConcurrentHashMap<Path, List<TITANMarker>>();
	}

	public void reset() {
		uptodateFiles.clear();
		highlySyntaxErroneousFiles.clear();
		fileMap.clear();
		includeFileMap.clear();
		unsupportedConstructMap.clear();
	}

	/**
	 * Checks whether the internal data belonging to the provided file is syntactically out-dated.
	 * <p>
	 * If the project is syntactically out-dated, all files are handled as out-dated.
	 *
	 * @param file the file to check.
	 * @return {@code true} if the data was reported to be out-dated since the last analysis.
	 */
	public boolean isOutdated(final Path file) {
		return syntacticallyOutdated || !uptodateFiles.containsKey(file);
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date.
	 * <p>
	 * Stores that this file is out of date for later usage
	 *
	 * @param outdatedFile the file which seems to have changed
	 */
	public void reportOutdating(final Path outdatedFile) {
		final Project proj = Project.INSTANCE;
		final ProjectItem item = proj.getProjectItem(outdatedFile);
		if (item == null) {
			return;
		}

		synchronized (this) {
			if (item.isDirectory()) {
				syntacticallyOutdated = true;
				// FIXME
//				for (final Iterator<File> iterator = uptodateFiles.keySet().iterator(); iterator.hasNext();) {
//					final File tempFile = iterator.next();
//					final Path filepath = tempFile.getProjectRelativePath();
//					if (folderPath.isPrefixOf(filepath)) {
//						sourceParser.getSemanticAnalyzer().reportOutdating(tempFile, useOnTheFlyParsing);
//						iterator.remove();
//						unsupportedConstructMap.remove(tempFile);
//					}
//				}
//				for (final Iterator<File> iterator = highlySyntaxErroneousFiles.iterator(); iterator.hasNext();) {
//					final File tempFile = iterator.next();
//					final Path filepath = tempFile.getProjectRelativePath();
//					if (folderPath.isPrefixOf(filepath)) {
//						iterator.remove();
//					}
//				}
			} else {
				syntacticallyOutdated = true;
				if (uptodateFiles.containsKey(outdatedFile)) {
					uptodateFiles.remove(outdatedFile);
					unsupportedConstructMap.remove(outdatedFile);
				}
				if (highlySyntaxErroneousFiles.contains(outdatedFile)) {
					highlySyntaxErroneousFiles.remove(outdatedFile);
				}
				sourceParser.getSemanticAnalyzer().reportOutdating(outdatedFile);
			}
		}
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date.
	 * <p>
	 * Stores that this file is out of date for later usage, but leaves the
	 * semantic information intact.
	 *
	 * @param outdatedFile the file which seems to have changed
	 */
	public void reportSyntacticOutdatingOnly(final Path outdatedFile) {
		synchronized (this) {
			syntacticallyOutdated = true;
			if (uptodateFiles.containsKey(outdatedFile)) {
				uptodateFiles.remove(outdatedFile);
				unsupportedConstructMap.remove(outdatedFile);
			}
			if (highlySyntaxErroneousFiles.contains(outdatedFile)) {
				highlySyntaxErroneousFiles.remove(outdatedFile);
			}
		}
	}

	/**
	 * Removes data related to modules, that were deleted or moved.
	 */
	private void removedReferencestoRemovedFiles() {
		// FIXME
//		final List<File> filesToRemove = new ArrayList<File>();
//		for (final File file : fileMap.keySet()) {
//			if (!file.isAccessible()) {
//				uptodateFiles.remove(file);
//				highlySyntaxErroneousFiles.remove(file);
//				final String moduleName = fileMap.get(file);
//				filesToRemove.add(file);
//
//				sourceParser.getSemanticAnalyzer().removedReferencestoRemovedFiles(file, moduleName);
//			}
//		}
//
//		for (final File file : filesToRemove) {
//			fileMap.remove(file);
//			unsupportedConstructMap.remove(file);
//
//			MarkerHandler.markAllMarkersForRemoval(file);
//			MarkerHandler.removeAllMarkedMarkers(file);
//		}
	}

	/**
	 * The entry point of incremental parsing.
	 * <p>
	 * Handles the data storages, calls the module level incremental parser
	 * on the file, and if everything fails does a full parsing to correct
	 * possibly invalid states.
	 *
	 * @param path the edited file
	 * @param reparser the parser doing the incremental parsing.
	 */
	public void updateSyntax(final Path path, final TTCN3ReparseUpdater reparser) {
		if (uptodateFiles.containsKey(path)) {
			final Module module = sourceParser.getSemanticAnalyzer().getModulebyFile(path);
			sourceParser.getSemanticAnalyzer().reportSemanticOutdating(path);

			if (module instanceof TTCN3Module) {
				final TTCN3Module ttcn3module = (TTCN3Module)module;
				try {
					reparser.setUnsupportedConstructs(unsupportedConstructMap);
					try {
						if (reparser.isCodeCommented() || reparser.documentCommentInserted()) {
							/*
							 * Probably one or more lines or part of a line is commented out.
							 * To be safe, we do a maximal damage. Maybe this could be refined.
							 */
							throw new ReParseException();
						}
						ttcn3module.updateSyntax(reparser, sourceParser);
						reparser.updateLocation(ttcn3module.getLocation());
					} catch (ReParseException e) {
						syntacticallyOutdated = true;
						uptodateFiles.remove(path);
						sourceParser.getSemanticAnalyzer().reportSemanticOutdating(path);
						final String oldModuleName = fileMap.get(path);
						if (oldModuleName != null) {
							sourceParser.getSemanticAnalyzer().removeModule(oldModuleName);
							fileMap.remove(path);
						}
						unsupportedConstructMap.remove(path);

						reparser.maxDamage();
						AstSemanticHighlighting.clearSemanticTokensAndModifiers(path);
						ttcn3module.clearExistingReferences();

						final ITtcn3FileReparser r = new Ttcn3FileReparser(reparser, path, sourceParser, fileMap, uptodateFiles, highlySyntaxErroneousFiles);
						syntacticallyOutdated = r.parse();
					}
					MarkerHandler.removeMarkers(path.toFile());
				} catch (Exception e) {
					// This catch is extremely important, as it is supposed to protect
					// the project parser, from whatever might go wrong inside the analysis.
					TitanLogger.logError(e);
				}
			} else {
				reportOutdating(path);
			}
		} else if (highlySyntaxErroneousFiles.contains(path)) {
			reportOutdating(path);
		} else {
			MarkerHandler.removeMarkers(path);
			final TemporalParseData temp = fileBasedTTCN3Analysis(path);
			postFileBasedGeneralAnalysis(temp);
		}

		reparser.reportSyntaxErrors();

//		for (int i = 0; i < eventListener.size(); i++) {
//			IUpdateSyntaxEventListener updateListener = eventListener.elementAt(i);
//			updateListener.syntaxUpdated(new UpdateSyntaxEvent(this));
//		}
	}

	void removeTTCNPPFilesIndirectlyModifiedByTTCNINFiles() {
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(project);
		final Set<String> moduleNames = projectSourceParser.getKnownModuleNames();
		for (final String moduleName : moduleNames) {
			final Module module = projectSourceParser.getModuleByName(moduleName);
			if (!(module instanceof TTCN3Module)) {
				continue;
			}

			final TTCN3Module ttcnppModule = (TTCN3Module) module;
			final Set<Path> includedFiles = ttcnppModule.getIncludedFiles();
			if (includedFiles == null || includedFiles.isEmpty()) {
				continue;
			}
			boolean isTTCNPPupToDate = true;
			for (final Path p : includedFiles) {
				if (!uptodateFiles.containsKey(p)) {
					isTTCNPPupToDate = false;
					break;
				}
			}
			if (!isTTCNPPupToDate) {
				uptodateFiles.remove(ttcnppModule.getLocation().getFile().toPath());
			}
		}
	}

	/**
	 * Internal function.
	 * @return the status of the operation when it finished.
	 */
	public synchronized void internalDoAnalyzeSyntactically() {
		if (syntacticallyOutdated) {
			progressCounter = new AtomicInteger(0);
			lastProgressReported = new AtomicInteger(0);
			List<CompletableFuture<Void>> futureList = new ArrayList<>();
			AtomicInteger index = new AtomicInteger(-1);

			syntacticallyOutdated = false;

			final long absoluteStart = System.nanoTime();

			removedReferencestoRemovedFiles();
			removeTTCNPPFilesIndirectlyModifiedByTTCNINFiles();

			final OutdatedFileCollector visitor = new OutdatedFileCollector(/*workingDirectories,*/ uptodateFiles, highlySyntaxErroneousFiles);
			project.collectOutdatedFiles(visitor);

			final List<Path> ttcn3FilesToCheck = visitor.getTTCN3FilesToCheck();
			final List<Path> asn1FilesToCheck = visitor.getASN1FilesToCheck();
			final List<Path> ttcninFilesModified = visitor.getTtcninFilesModified();

			// nothing to do with these files
			for (final Path f : ttcninFilesModified) {
				uptodateFiles.put(f, f.getFileName().toString());
				includeFileMap.put(f.getFileName().toString(), f);
			}

			final List<Path> allCheckedFiles = new ArrayList<Path>();

			allCheckedFiles.addAll(uptodateFiles.keySet());

			// remove all markers from the files that need to be
			// parsed
			for (final Path file : ttcn3FilesToCheck) {
				final ProjectItem item = Project.INSTANCE.getProjectItem(file);
				item.clearMarkers();
			}

			allCheckedFiles.addAll(ttcn3FilesToCheck);
			allCheckedFiles.addAll(asn1FilesToCheck);
			nrOfItems = ttcn3FilesToCheck.size() + asn1FilesToCheck.size();
			final TemporalParseData[] tempResults = new TemporalParseData[nrOfItems];

			final String progressToken = TitanLanguageServer.createProgress("Building project", 5L);
			final String progressLabel = "Parsing";
			final Project proj = Project.INSTANCE;

			final int availableProcessors = Runtime.getRuntime().availableProcessors();
			ExecutorService executor = Executors.newFixedThreadPool(availableProcessors - 1);
			TitanLanguageServer.beginProgress(progressToken, progressLabel, 0, "0/" + nrOfItems + " files");
			for (final Path file : ttcn3FilesToCheck) {
				final ProjectItem item = proj.getProjectItem(file);
				if (item.isSyntacticallyUpToDate()) {
					TitanLogger.logDebug("**File is up to date: " + file.getFileName());
					updateProgress(progressToken);
					continue;
				}

				futureList.add(CompletableFuture.runAsync(() -> {
					final Path tempFile = file;
					final long absoluteStart2 = System.nanoTime();
					final TemporalParseData temp = fileBasedTTCN3Analysis(tempFile);
					tempResults[index.incrementAndGet()] = temp;
					final long now = System.nanoTime();
					final StringBuilder sb = new StringBuilder();
					sb.append("**It took (").append(absoluteStart2).append(", ").append(now).append(") ").append((now - absoluteStart2) * (1e-9)).append(" seconds for Titan to syntactically check ").append(file.getFileName());
					TitanLogger.logDebug(MessageFormat.format(DEBUG_MESSAGE_TIME_INTERVAL,
							absoluteStart2, now, (now - absoluteStart2) * (1e-9), file.getFileName()));
					updateProgress(progressToken);
				}));
			}
			ttcn3FilesToCheck.clear();

			asn1FilesToCheck.stream().forEach(asn1file -> {
				final ProjectItem item = Project.INSTANCE.getProjectItem(asn1file);
				if (item != null) {
					Project.INSTANCE.getProjectItem(asn1file).clearMarkers();
				}
			});

			for (final Path file : asn1FilesToCheck) {
				final ProjectItem item = proj.getProjectItem(file);
				if (item.isSyntacticallyUpToDate()) {
					TitanLogger.logDebug("**File is up to date: " + file.getFileName());
					updateProgress(progressToken);
					continue;
				}

				futureList.add(CompletableFuture.runAsync(() -> {
					final long absoluteStart2 = System.nanoTime();
					final Path tempFile = file;
					final TemporalParseData temp = fileBasedASN1Analysis(tempFile);
					tempResults[index.incrementAndGet()] = temp;
					final long now = System.nanoTime();
					TitanLogger.logDebug(MessageFormat.format(DEBUG_MESSAGE_TIME_INTERVAL,
							absoluteStart2 - absoluteStart, now - absoluteStart, (now - absoluteStart2) * (1e-9), file.getFileName()));
					updateProgress(progressToken);
				}));
			}

			futureList.forEach(CompletableFuture::join);
			executor.shutdown();
			try {
				executor.awaitTermination(30, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				TitanLogger.logError(e);
				Thread.currentThread().interrupt();
			}
			executor.shutdownNow();

			TitanLanguageServer.endProgress(progressToken);

			for (final TemporalParseData temp : tempResults) {
				if (temp != null) {
					postFileBasedGeneralAnalysis(temp);
				}
			}
		}
	}

	/**
	 * Parses the provided file.
	 *
	 * @param file the file to be parsed
	 * @return the temporal data structure needed to insert the parsed
	 *         module in the list of modules, in the post-analyzes step.
	 */
	private TemporalParseData fileBasedTTCN3Analysis(final Path file) {
		return fileBasedGeneralAnalysis(file, new TTCN3Analyzer());
	}

	/**
	 * Parses the provided file.
	 *
	 * @param file the file to be parsed
	 * @return the temporal data structure needed to insert the parsed
	 *         module in the list of modules, in the post-analyzes step.
	 */
	private TemporalParseData fileBasedASN1Analysis(final Path file) {
		return fileBasedGeneralAnalysis(file, new ASN1Analyzer());
	}

	/**
	 * Parses the provided file.
	 *
	 * @param file the file to be parsed
	 * @param analyzer the source code analyzer that should be used to analyze the code
	 * @return the temporal data structure needed to insert the parsed
	 *         module in the list of modules, in the post-analyzes step.
	 */
	private TemporalParseData fileBasedGeneralAnalysis(final Path file, final ISourceAnalyzer analyzer) {
		if (analyzer == null) {
			return null;
		}

		final Project proj = Project.INSTANCE;
		final ProjectItem item = proj.getProjectItem(file);
		if (item == null) {
			return null;
		}

		unsupportedConstructMap.remove(file);

		final String code = item.getSource();
		try {
			analyzer.parse(file, code);
		} catch (FileNotFoundException e) {
			TitanLogger.logError(e);
			return null;
		}
		final Module ttcn3module = analyzer.getModule();
		if (ttcn3module instanceof TTCN3Module) {
			item.setModule(ttcn3module);
		}

		final boolean hadParseErrors = processParserErrors(analyzer);

		final List<TITANMarker> unsupportedConstructs = analyzer.getUnsupportedConstructs();
		final Module module = analyzer.getModule();

//		if (document != null) {
//			GlobalIntervalHandler.putInterval(document, analyzer.getRootInterval());
//		}

		return new TemporalParseData(module, file, unsupportedConstructs, hadParseErrors);
	}

	/**
	 * Handle the errors in fileBasedGeneralAnalysis(), and add them to the markers
	 * @param aFile the parsed file
	 * @param aAnalyzer analyzer, that collected the errors
	 * @return true if it had parse errors
	 */
	private boolean processParserErrors(final ISourceAnalyzer aAnalyzer) {
		List<SyntacticErrorStorage> errors = aAnalyzer.getErrorStorage();
		for (SyntacticErrorStorage storage : errors) {
			final int line = storage.getLocation().getStartLine() - 1;
			storage.getLocation().setStartLine(line);
			storage.getLocation().setEndLine(line);
			storage.reportSyntacticError();
		}

		return !errors.isEmpty();
	}

	/**
	 * Uses the parsed data structure to decide if the module found can be
	 * inserted into the list of known modules. And inserts it if possible.
	 *
	 * @param parsedData the parsed data to insert into the semantic database.
	 */
	private void postFileBasedGeneralAnalysis(final TemporalParseData parsedData) {
		final Module module = parsedData.getModule();
		if (module != null && module.getIdentifier() != null && module.getLocation() != null) {
			sourceParser.getSemanticAnalyzer().addModule(module);

			final Path file = parsedData.getFile();
			fileMap.put(file, module.getName());
			uptodateFiles.put(file, module.getName());

			final List<TITANMarker> unsupportedConstructs = parsedData.getUnsupportedConstructs();
			if (!unsupportedConstructs.isEmpty()) {
				unsupportedConstructMap.put(file, unsupportedConstructs);
			}

//			if (module.getLocation().getEndOffset() == -1 && parsedData.hadParseErrors()) {
//				if (parsedData.getDocument() == null) {
//					module.getLocation().setEndOffset((int) new File(file.getLocationURI()).length());
//				} else {
//					module.getLocation().setEndOffset(parsedData.getDocument().getLength());
//				}
//			}
		} else {
			syntacticallyOutdated = true;
			highlySyntaxErroneousFiles.add(parsedData.getFile());
		}
	}

	/**
	 * Returns the TTCN-3 include file with the provided name, or null.
	 *
	 * @param name the file name to return.
	 * @param uptodateOnly allow finding only the up-to-date modules.
	 * @return the file handler having the provided name
	 */
	Path internalGetTTCN3IncludeFileByName(final String name) {
		if (includeFileMap.containsKey(name)) {
			return includeFileMap.get(name);
		}
		return null;
	}

	private void updateProgress(final String progressToken) {
		if (nrOfItems == 0 || progressToken == null) {
			return;
		}
		int counter = progressCounter.incrementAndGet();
		final double percentage = ((double)counter / nrOfItems) * 100;
		final boolean isNotDone = percentage > lastProgressReported.get();
		if (isNotDone) {
			lastProgressReported.set((int)percentage);
			TitanLanguageServer.reportProgress(progressToken, lastProgressReported.get(), counter + "/" + nrOfItems + " files");
		}
	}
}
