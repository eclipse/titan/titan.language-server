/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public abstract class CompositeTemplate extends TTCN3Template implements ICollection<TTCN3Template> {
	private static final String FULLNAMEPART = ".list_item(";
	private static final String EXCEPTION_TEXT = "add() operation is not supported for CompositeTemplate";

	protected final ListOfTemplates templates;

	protected CompositeTemplate(final ListOfTemplates templates) {
		this.templates = templates;

		for (final TTCN3Template t : this.templates) {
			t.setFullNameParent(this);
		}
	}

	@Override
	public TTCN3Template get(final int index) {
		return templates.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		templates.setMyScope(scope);
	}

	@Override
	public int size() {
		return templates.size();
	}

	/**
	 * Calculates the number of list members which are not the any or none symbol.
	 * @return the number calculated.
	 */
	public int getNofTemplatesNotAnyornone(final CompilationTimeStamp timestamp) {
		int result = 0;
		for (final TTCN3Template template : templates) {
			switch (template.getTemplatetype()) {
			case ANY_OR_OMIT:
				break;
			case PERMUTATION_MATCH:
				result += ((PermutationMatch_Template) template).getNofTemplatesNotAnyornone(timestamp);
				break;
			case ALL_FROM:
				result += ((All_From_Template) template).getNofTemplatesNotAnyornone(timestamp);
				break;
			default:
				result++;
				break;
			}
		}
		return result;
	}

	/**
	 * Checks if the list of templates has at least one any or none symbol.
	 * @return true if an any or none symbol was found, false otherwise.
	 */
	public boolean templateContainsAnyornone() {
		for (final TTCN3Template template : templates) {
			switch (template.getTemplatetype()) {
			case ANY_OR_OMIT:
				return true;
			case PERMUTATION_MATCH:
				if (((PermutationMatch_Template) template).templateContainsAnyornone()) {
					return true;
				}
				break;
			default:
				break;
			}
		}
		return false;
	}

	/**
	 * Checks if the list of templates has at least one any or none or permutation symbol.
	 * <p> It is prohibited after "all from".
	 *
	 * @return {@code true} if an any or none symbol was found, {@code false} otherwise.
	 */
	public boolean containsAnyornoneOrPermutation(final CompilationTimeStamp timestamp) {
		for (final TTCN3Template template : templates) {
			switch (template.getTemplatetype()) {
			case ANY_OR_OMIT:
			case PERMUTATION_MATCH:
				return true;
			case ALL_FROM:
				return ((All_From_Template)template).containsAnyornoneOrPermutation(timestamp);
			default:
				break;
			}
		}

		return false;
	}


	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = templates.size(); i < size; i++) {
			if (templates.get(i) == child) {
				return builder.append(FULLNAMEPART).append(String.valueOf(i)).append(INamedNode.RIGHTPARENTHESES);
			}
		}

		return builder;
	}

	protected abstract String getNameForStringRep();

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		builder.append(getNameForStringRep()).append("( ");
		for (int i = 0, size = templates.size(); i < size; i++) {
			if (i > 0) {
				builder.append(", ");
			}

			final ITTCN3Template template = templates.get(i);
			builder.append(template.createStringRepresentation());
		}
		builder.append(" )");

		return addToStringRepresentation(builder).toString();
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}

		if (referenceChain.add(this)) {
			for (final TTCN3Template template : templates) {
				if (template != null) {
					referenceChain.markState();
					template.checkRecursions(timestamp, referenceChain);
					referenceChain.previousState();
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);
		templates.updateSyntax(reparser, false);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (templates == null) {
			return;
		}

		templates.findReferences(referenceFinder, foundIdentifiers);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (templates != null && !templates.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyDefinition(Definition definition) {
		for (final TTCN3Template t : templates) {
			t.setMyDefinition(definition);
		}
	}

	@Override
	public boolean add(TTCN3Template e) {
		throw new UnsupportedOperationException(EXCEPTION_TEXT);
	}

	@Override
	public Iterator<TTCN3Template> iterator() {
		return templates.iterator();
	}
}
