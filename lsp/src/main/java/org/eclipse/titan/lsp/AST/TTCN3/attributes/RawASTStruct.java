/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ExpressionStruct;

/**
 * Represents the RAW encoding related setting extracted from variant attributes, for the code generator.
 *
 * @author Kristof Szabados
 * */
public class RawASTStruct extends RawAST_Base {

	public enum rawAST_coding_field_type {
		MANDATORY_FIELD,
		OPTIONAL_FIELD,
		UNION_FIELD
	}

	public static class rawAST_coding_fields {
		public int nthfield;
		public String nthfieldname;
		public rawAST_coding_field_type fieldtype;
		public String type;
		public String typedesc;
		// only when fieldtype is UNION_FIELD
		public String unionType;
		//used to generate more optimal code for crosstag
		public boolean refersEnum = false;
		public String enumValue = null;
	}

	public static class rawAST_coding_field_list {
		public List<rawAST_coding_fields> fields;
		public String value;
		public ExpressionStruct expression;
		public ExpressionStruct nativeExpression;
		public boolean isOmitValue;
		public int start_pos;
		public int temporal_variable_index;
	}

	public static class rawAST_coding_taglist {
		public String fieldname;
		public String varName;
		public int fieldnum;
		public List<rawAST_coding_field_list> fields;
	}

	public static class rawAST_coding_taglist_list {
		public List<rawAST_coding_taglist> list;
	}

	public static class rawAST_coding_ext_group {
		public int ext_bit;
		public int from;
		public int to;
	}

	//ext_bit_goup_num is not stored as it is the sie of ext_bit_groups
	public List<rawAST_coding_ext_group> ext_bit_groups;
	public List<Integer> lengthto; //list of fields to generate length for
	public int pointerto; //pointer to the specified field is contained in this field

	/**< offset to the value in bits
	 Actual position will be:
	 pointerto*ptrunit + ptroffset */
	public int ptrunit; // number of bits in pointerto value
	public int pointerbase; //the identifier in PTROFFSET(identifier)
	public rawAST_coding_fields lengthindex; // stores subattribute of the lengthto attribute
	// field IDs in form of [unionField.sub]field_N, keyField.subfield_M = tagValue multiple tagValues may be specified
	public rawAST_coding_taglist_list crosstaglist;
	public rawAST_coding_taglist_list taglist;

	public rawAST_coding_taglist presence; // Presence indicator expressions for an optional field
	public int union_member_num;
	public List<String> member_name;
	public int length;

	public RawASTStruct(final RawAST from, final boolean copy_presences) {
		fieldlength = from.fieldlength;
		comp = from.comp;
		byteorder = from.byteorder;
		align = from.align;
		bitorderinfield = from.bitorderinfield;
		bitorderinoctet = from.bitorderinoctet;
		extension_bit = from.extension_bit;
		if (from.ext_bit_groups != null) {
			ext_bit_groups = new ArrayList<RawASTStruct.rawAST_coding_ext_group>(from.ext_bit_groups.size());
		}
		hexorder = from.hexorder;
		padding = from.padding;
		if (from.lengthto != null) {
			lengthto = new ArrayList<Integer>(from.lengthto.size());
		}
		lengthto_offset = from.lengthto_offset;
		pointerto = -1;
		ptroffset = from.ptroffset;
		unit = from.unit;
		if (from.lengthindex != null) {
			lengthindex = new rawAST_coding_fields();
		}
		if (from.crosstaglist != null && !from.crosstaglist.isEmpty()) {
			crosstaglist = new rawAST_coding_taglist_list();
			crosstaglist.list = new ArrayList<RawASTStruct.rawAST_coding_taglist>(from.crosstaglist.size());
			for (int i = 0; i < from.crosstaglist.size(); i++) {
				crosstaglist.list.add(new rawAST_coding_taglist());
			}
		}
		if (from.taglist != null && !from.taglist.isEmpty()) {
			taglist = new rawAST_coding_taglist_list();
			taglist.list = new ArrayList<RawASTStruct.rawAST_coding_taglist>(from.taglist.size());
			for (int i = 0; i < from.taglist.size(); i++) {
				taglist.list.add(new rawAST_coding_taglist());
			}
		}
		if (copy_presences && from.presence != null && from.presence.keyList != null && !from.presence.keyList.isEmpty()) {
			presence = new rawAST_coding_taglist();
			presence.fields = new ArrayList<RawASTStruct.rawAST_coding_field_list>(from.presence.keyList.size());
		}
		toplevelind = from.toplevelind;
		toplevel.bitorder = from.toplevel.bitorder;
		union_member_num = 0;
		member_name = null;
		repeatable = from.repeatable;
		length = -1;
	}
}
