/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FriendModule;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class MissingFriend extends BaseModuleCodeSmellSpotter {
	public static final String MISSING_MODULE = "There is no module with name `{0}''";

	public MissingFriend() {
		super(CodeSmellType.MISSING_FRIEND);
		addStartNode(FriendModule.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof FriendModule) {
			final FriendModule s = (FriendModule) node;
			final Identifier identifier = s.getIdentifier();
			final ProjectSourceParser parser = GlobalParser.getProjectSourceParser(Project.INSTANCE);
			if (parser != null && identifier != null) {
				final Module referredModule = parser.getModuleByName(identifier.getName());
				if (referredModule == null) {
					final String msg = MessageFormat.format(MISSING_MODULE, identifier.getDisplayName());
					problems.report(identifier.getLocation(), msg);
				}
			}
		}
	}
}
