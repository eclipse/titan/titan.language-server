package org.eclipse.titan.lsp.parsers.ttcn3parser;

import java.io.File;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Parser.Pr_FunctionAndTestcaseCallsContext;

public class UnfinishedStatementParser {
	private UnfinishedStatementParser() {
		// Hide constructor
	}

	private static CharStream getCharStream(final String source) {
		return new UnbufferedCharStream(new StringReader(source));
	}

	public static List<Reference> getReferencesForSignatureHelp(String source, File file, Position position) {
		final Configuration config = Configuration.INSTANCE;
		final boolean realtimeEnabled = config.getBoolean(Configuration.ENABLE_REALTIME_EXTENSION, false);
		final boolean oopEnabled = config.getBoolean(Configuration.ENABLE_OOP_EXTENSION, true);

		final Ttcn3Lexer lexer = new Ttcn3Lexer(getCharStream(source));
		lexer.enableOop(oopEnabled);
		lexer.setTokenFactory(new CommonTokenFactory(true));
		if (realtimeEnabled) {
			lexer.enableRealtime();
		}
		lexer.initRootInterval( source.length() );
		lexer.setActualFile(file);
		lexer.removeErrorListeners();
		final CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		final Ttcn3Parser parser = new Ttcn3Parser(tokenStream);
		ParserUtilities.setBuildParseTree( parser );
		parser.setActualFile(file);
		parser.setLine(position.getLine());
		final int offset = ParserUtilities.calculateOffset(file, position);
		parser.setOffset(offset);
		parser.removeErrorListeners();

		final List<Reference> references = new LinkedList<>();
		final Pr_FunctionAndTestcaseCallsContext  root = parser.pr_FunctionAndTestcaseCalls(references);
		ParserUtilities.logParseTree( root, parser );
		return references;
	}
}
