/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents an actual parameter that has the value of a default actual
 * parameter that was assigned to the formal parameter.
 *
 * @author Kristof Szabados
 * */
public final class Default_ActualParameter extends ActualParameter {
	// generated value
	private final ActualParameter defaultActualParameter;

	public Default_ActualParameter(final ActualParameter defaultActualParameter) {
		this.defaultActualParameter = defaultActualParameter;
	}

	public ActualParameter getActualParameter() {
		return defaultActualParameter;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (defaultActualParameter != null) {
			referenceChain.markState();
			defaultActualParameter.checkRecursions(timestamp, referenceChain);
			referenceChain.previousState();
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (defaultActualParameter != null) {
			if (!defaultActualParameter.accept(v)) {
				return false;
			}
		}
		return true;
	}
}
