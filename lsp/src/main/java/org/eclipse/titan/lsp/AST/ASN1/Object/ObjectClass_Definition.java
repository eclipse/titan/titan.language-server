/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ASN1.ASN1Object;
import org.eclipse.titan.lsp.AST.ASN1.Block;
import org.eclipse.titan.lsp.AST.ASN1.ObjectClass;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.asn1parser.Asn1Parser;
import org.eclipse.titan.lsp.parsers.asn1parser.BlockLevelTokenStreamTracker;

/**
 * Class to represent ObjectClassDefinition.
 *
 * @author Kristof Szabados
 */
public final class ObjectClass_Definition extends ObjectClass {
	private static final String MISSINGSETTING = "Missing setting for `{0}''";

	private final Block fieldSpecsBlock;
	private final Block withSyntaxBlock;

	private FieldSpecifications fieldSpecifications;
	private ObjectClassSyntax_root ocsRoot;

	private BuildTimestamp lastBuildTimestamp;

	public ObjectClass_Definition(final Block fieldSpecsBlock, final Block withSyntaxBlock) {
		this.fieldSpecsBlock = fieldSpecsBlock;
		this.withSyntaxBlock = withSyntaxBlock;
	}

	public ObjectClass_Definition() {
		this.fieldSpecsBlock = null;
		this.withSyntaxBlock = null;
	}

	public ObjectClass_Definition newInstance() {
		return new ObjectClass_Definition(fieldSpecsBlock, withSyntaxBlock);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;
		isErroneous = false;

		if (ocsRoot != null) {
			return;
		}

		if (fieldSpecifications == null) {
			parseBlockFieldSpecs();
		}

		if (getIsErroneous(timestamp) || fieldSpecifications == null) {
			return;
		}

		fieldSpecifications.check(timestamp);

		if (ocsRoot == null) {
			ocsRoot = new ObjectClassSyntax_root();
			final ObjectClassSyntax_Builder builder = new ObjectClassSyntax_Builder(withSyntaxBlock, fieldSpecifications);
			ocsRoot.accept(builder);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void check(CompilationTimeStamp timestamp, IReferenceChain refChain) {
		// Do nothing
	}

	@Override
	/** {@inheritDoc} */
	public void checkThisObject(final CompilationTimeStamp timestamp, final ASN1Object object) {
		if (isBuildCancelled()) {
			return;
		}

		if (object == null || isErroneous || fieldSpecifications == null) {
			return;
		}

		final Object_Definition objectDefinition = object.getRefdLast(timestamp, null);
		for (FieldSpecification fieldSpecification : fieldSpecifications) {
			fieldSpecification = fieldSpecification.getLast();
			if (objectDefinition.contains(fieldSpecification.getIdentifier())) {
				final FieldSetting fieldSetting = objectDefinition.get(fieldSpecification.getIdentifier());
				if (fieldSetting != null) {
					fieldSetting.check(timestamp, fieldSpecification);
				}
			} else {
				if (!fieldSpecification.isOptional() && !fieldSpecification.hasDefault()
						&& !objectDefinition.getIsErroneous(timestamp)) {
					objectDefinition.getLocation().reportSemanticError(
							MessageFormat.format(MISSINGSETTING, fieldSpecification.identifier.getDisplayName()));
					objectDefinition.setIsErroneous(true);
				}
			}
		}
	}

	private void parseBlockFieldSpecs() {
		if (fieldSpecsBlock == null) {
			return;
		}

		final Asn1Parser parser = BlockLevelTokenStreamTracker.getASN1ParserForBlock(fieldSpecsBlock);
		if (parser == null) {
			return;
		}

		fieldSpecifications = parser.pr_special_FieldSpecList().fieldSpecifications;
		final List<SyntacticErrorStorage> errors = parser.getErrorStorage();
		if (errors != null && !errors.isEmpty()) {
			fieldSpecifications = null;
			for (SyntacticErrorStorage ses : errors) {
				ses.reportSyntacticError();
			}
		}

		if (fieldSpecifications == null) {
			fieldSpecifications = new FieldSpecifications(true);
			isErroneous = true;
		}

		fieldSpecifications.setFullNameParent(this);
		fieldSpecifications.setMyObjectClass(this);
	}

	@Override
	/** {@inheritDoc} */
	public ObjectClassSyntax_root getObjectClassSyntax(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return ocsRoot;
	}

	@Override
	/** {@inheritDoc} */
	public FieldSpecifications getFieldSpecifications() {
		if (null == lastTimeChecked) {
			check(CompilationTimeStamp.getBaseTimestamp());
		}

		return fieldSpecifications;
	}

	@Override
	/** {@inheritDoc} */
	public ObjectClass_Definition getRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		check(timestamp);

		return this;
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (fieldSpecifications != null && !fieldSpecifications.accept(v)) {
			return false;
		}

		return true;
	}
}
