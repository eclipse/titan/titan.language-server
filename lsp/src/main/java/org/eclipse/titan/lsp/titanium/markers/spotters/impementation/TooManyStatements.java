/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class TooManyStatements extends BaseModuleCodeSmellSpotter {
	private final int reportTooManyStatementsSize;
	private static final String TOOMANYSTATEMENTSWARNING = "More than {0} statements in a single statementblock: {1}";

	public TooManyStatements() {
		super(CodeSmellType.TOO_MANY_STATEMENTS);
		reportTooManyStatementsSize = 150;
		
		addStartNode(StatementBlock.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof StatementBlock) {
			final StatementBlock s = (StatementBlock) node;
			if (s.getSize() > reportTooManyStatementsSize) {
				final String msg = MessageFormat.format(TOOMANYSTATEMENTSWARNING, reportTooManyStatementsSize, s.getSize());
				problems.report(s.getLocation(), msg);
			}
		}

	}
}
