/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.values.expressions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ActualParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Constructor;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * UndefCreateExpression represents an expression in the source code that is 
 * either a create operation or a constructor call.
 * 
 * Ttcn grammar is ambiguous for some cases and the parser is unable to 
 * differentiate between createop and constructor call in all circumstances 
 * so we need to store parsed data in a temporary form to postpone the decision 
 * for the semantic analysis phase.
 *  
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public class UndefCreateExpression extends Expression_Value {
	public static final String OPERATIONNAME = "create()";

	private final Reference componentReference;
	private final Value name;
	private final Value componentLocation;
	private final boolean isAlive;
	private final ParsedActualParameters parameters;

	private Expression_Value realExpression;

	private CompilationTimeStamp checkCreateTimestamp;
	private IType checkCreateCache;

	public UndefCreateExpression(final Reference reference, final Value name, final Value location, 
								 final boolean isAlive, final ParsedActualParameters parameters) {
		this.componentReference = reference;
		this.name = name;
		this.componentLocation = location;
		this.isAlive = isAlive;
		this.parameters = parameters;

		if (reference != null) {
			reference.setFullNameParent(this);
		}
		if (name != null) {
			name.setFullNameParent(this);
		}
		if (location != null) {
			location.setFullNameParent(this);
		}
	}

	public UndefCreateExpression(final Reference reference, final Value name, final Value location, final boolean isAlive) {
		this(reference, name, location, isAlive, null);
	}

	public UndefCreateExpression(final Reference reference, final ParsedActualParameters parameters) {
		this(reference, null, null, false, parameters);
	}

	@Override
	/** {@inheritDoc} */
	public Operation_type getOperationType() {
		return Operation_type.UNDEFINED_CREATE_OPERATION;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReference(final CompilationTimeStamp timestamp, final Assignment lhs) {
		// assume no self-ref
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (componentReference != null) {
			componentReference.setMyScope(scope);
		}
		if (name != null) {
			name.setMyScope(scope);
		}
		if (componentLocation != null) {
			componentLocation.setMyScope(scope);
		}
		if (parameters != null) {
			parameters.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (componentReference == child) {
			return builder.append(OPERAND1);
		} else if (name == child) {
			return builder.append(OPERAND2);
		} else if (componentLocation == child) {
			return builder.append(OPERAND3);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (realExpression == null) {
			checkCreate(timestamp);
			return checkCreateCache == null ? null : checkCreateCache.getTypetype();
		}
		return realExpression.getExpressionReturntype(timestamp, expectedValue);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnfoldable(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return realExpression == null ? checkCreate(timestamp) : realExpression.getExpressionGovernor(timestamp, expectedValue);
	}

	/**
	 * Checks the parameters of the expression and if they are valid in
	 * their position in the expression or not.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param expectedValue the kind of value expected.
	 * @param referenceChain a reference chain to detect cyclic references.
	 */
	private void checkExpressionOperands(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		setIsErroneous(false);

		checkCreate(timestamp);
		if (componentReference == null) {
			return;
		}

		final Assignment assignment = componentReference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			setIsErroneous(true);
			return;
		}

		IType assignmentType = assignment.getType(timestamp);
		if (assignmentType instanceof Component_Type) {
			realExpression = new ComponentCreateExpression(componentReference, name, componentLocation, isAlive);
			realExpression.setMyScope(getMyScope());
			realExpression.setFullNameParent(this);
			realExpression.setLocation(getLocation());
			realExpression.evaluateValue(timestamp, expectedValue, referenceChain);
			checkExpressionDynamicPart(expectedValue, OPERATIONNAME, false, true, false);
		} else if (assignmentType instanceof Class_Type) {
			final Class_Type classInstance = (Class_Type)assignmentType;
			realExpression = new ClassConstructorExpression(componentReference, parameters);
			realExpression.setMyScope(getMyScope());
			realExpression.setFullNameParent(this);
			realExpression.setLocation(getLocation());
			realExpression.evaluateValue(timestamp, expectedValue, referenceChain);

			final ActualParameterList tempActualParameters = new ActualParameterList();
			final Def_Constructor constructor = classInstance.getConstructor(timestamp);
			if (constructor != null) {
				FormalParameterList fpList = constructor.getFormalParameterList();
				fpList.checkActualParameterList(timestamp, parameters, tempActualParameters);
			}
			// FIXME : implement
		}
	}

	@Override
	/** {@inheritDoc} */
	public IValue evaluateValue(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}

		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;

		if (componentReference == null) {
			return lastValue;
		}

		checkExpressionOperands(timestamp, expectedValue, referenceChain);
		checkCreate(timestamp);
		return lastValue;
	}

	private IType checkCreate(final CompilationTimeStamp timestamp) {
		if (checkCreateTimestamp != null && !checkCreateTimestamp.isLess(timestamp)) {
			return checkCreateCache;
		}

		checkCreateTimestamp = timestamp;
		checkCreateCache = null;

		if (componentReference == null) {
			return null;
		}

		final Assignment assignment = componentReference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			setIsErroneous(true);
			return null;
		}

		if (!Assignment_type.A_TYPE.semanticallyEquals(assignment.getAssignmentType())) {
			componentReference.getLocation().reportSemanticError(
					MessageFormat.format(ComponentCreateExpression.COMPONENTEXPECTED, assignment.getDescription()));
			setIsErroneous(true);
			return null;
		}

		final IType type = ((Def_Type) assignment).getType(timestamp).getFieldType(timestamp, componentReference, 1,
				Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
		if (type == null) {
			setIsErroneous(true);
			return null;
		}

		IType assignmentType = assignment.getType(timestamp);
		if (!(assignmentType instanceof Component_Type) &&
				!Type_type.TYPE_COMPONENT.equals(type.getTypetype())) {
			componentReference.getLocation().reportSemanticError(
					MessageFormat.format(ComponentCreateExpression.TYPEMISMATCH1, type.getTypename()));
			setIsErroneous(true);
			return null;
		}

		if (myGovernor != null) {
			final IType last = myGovernor.getTypeRefdLast(timestamp);
			if (Type_type.TYPE_COMPONENT.equals(last.getTypetype()) && !last.isCompatible(timestamp, type, null, null, null)) {
				componentReference.getLocation().reportSemanticError(
						MessageFormat.format(ComponentCreateExpression.TYPEMISMATCH2, last.getTypename(), type.getTypename()));
				setIsErroneous(true);
				return null;
			}
		}

		checkCreateCache = type;
		return checkCreateCache;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (componentReference != null) {
			componentReference.updateSyntax(reparser, false);
			reparser.updateLocation(componentReference.getLocation());
		}
		if (name != null) {
			name.updateSyntax(reparser, false);
			reparser.updateLocation(name.getLocation());
		}
		if (componentLocation != null) {
			componentLocation.updateSyntax(reparser, false);
			reparser.updateLocation(componentLocation.getLocation());
		}
		if (parameters != null) {
			parameters.updateSyntax(reparser, isDamaged);
			reparser.updateLocation(parameters.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (componentReference != null) {
			componentReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (name != null) {
			name.findReferences(referenceFinder, foundIdentifiers);
		}
		if (componentLocation != null) {
			componentLocation.findReferences(referenceFinder, foundIdentifiers);
		}
		if (parameters != null) {
			parameters.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (componentReference != null && !componentReference.accept(v)) {
			return false;
		}
		if (name != null && !name.accept(v)) {
			return false;
		}
		if (componentLocation != null && !componentLocation.accept(v)) {
			return false;
		}
		if (parameters != null && !parameters.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	public String createStringRepresentation() {
		if (componentReference == null) {
			return ERRONEOUS_VALUE;
		}

		if (realExpression != null) {
			return realExpression.createStringRepresentation();
		}

		return null;
	}
}
