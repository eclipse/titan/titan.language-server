/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.util.Arrays;
import org.eclipse.lsp4j.CodeLensOptions;
import org.eclipse.lsp4j.CompletionOptions;
import org.eclipse.lsp4j.DocumentSymbolOptions;
import org.eclipse.lsp4j.ReferenceOptions;
import org.eclipse.lsp4j.RenameOptions;
import org.eclipse.lsp4j.SemanticTokensLegend;
import org.eclipse.lsp4j.SemanticTokensWithRegistrationOptions;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.SignatureHelpOptions;
import org.eclipse.lsp4j.TextDocumentSyncKind;
import org.eclipse.lsp4j.WorkspaceFoldersOptions;
import org.eclipse.lsp4j.WorkspaceServerCapabilities;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;

/** 
* Static helper class for assembling the supported server capabilities.
* 
* @author Adam Knapp
*
*/
public final class ServerCapabilityHelper {

	private static final String[] TRIGGER_CHARACTERS = { "(" };
	private static final String[] COMPLETION_TRIGGER_CHARACTERS = { ":=" };
	private static final String[] RETRIGGER_CHARACTERS = { "," };
	private static final String DIAGNOSTIC_REGISTRACTION_OPTION_ID = "ttcn";
	private static final String SEMANTIC_OPTIONS_ID = "Semantic";

	private ServerCapabilityHelper() {
		// Do nothing
	}

	/**
	 * Creates the server capabilities
	 * @return the server capabilities
	 */
	public static ServerCapabilities getServerCapabilites() {
		final ServerCapabilities capabilities = new ServerCapabilities();
        // Set the capabilities of the LS to inform the client.
        capabilities.setTextDocumentSync(TextDocumentSyncKind.Incremental);
        //capabilities.setCompletionProvider(new CompletionOptions());
        capabilities.setHoverProvider(true);
        // FIXME:
        // Sending publishDiagnostics notifications and response for diagnostic pull requests
        // causes duplicated diagnostic report on the client.
        // The diagnostic provider has been temporarily commented out.
//        final DiagnosticRegistrationOptions diagnosticOptions = new DiagnosticRegistrationOptions(true, true);
//        diagnosticOptions.setId(DIAGNOSTIC_REGISTRACTION_OPTION_ID);
//        capabilities.setDiagnosticProvider(diagnosticOptions);
        capabilities.setDocumentSymbolProvider(new DocumentSymbolOptions());
        capabilities.setSignatureHelpProvider(new SignatureHelpOptions(
        		Arrays.asList(TRIGGER_CHARACTERS), Arrays.asList(RETRIGGER_CHARACTERS)));
        capabilities.setDefinitionProvider(true);
        capabilities.setCodeActionProvider(true);
        capabilities.setInlayHintProvider(true);
        capabilities.setCodeLensProvider(new CodeLensOptions());
        capabilities.setCompletionProvider(new CompletionOptions(true, Arrays.asList(COMPLETION_TRIGGER_CHARACTERS)));
        capabilities.setRenameProvider(new RenameOptions(true));

        final SemanticTokensWithRegistrationOptions semanticOptions = new SemanticTokensWithRegistrationOptions();
        semanticOptions.setFull(true);
        semanticOptions.setRange(false);
        semanticOptions.setDocumentSelector(null);
        semanticOptions.setId(SEMANTIC_OPTIONS_ID);
        semanticOptions.setLegend(new SemanticTokensLegend(AstSemanticHighlighting.getSemanticTokensLegend(),
        		AstSemanticHighlighting.getSemanticModifiersLegend()));
        capabilities.setSemanticTokensProvider(semanticOptions);

        capabilities.setReferencesProvider(new ReferenceOptions());
        capabilities.setTypeDefinitionProvider(true);
//        capabilities.setSelectionRangeProvider(true);
        
//        final TypeHierarchyRegistrationOptions typeHierarchyOptions = new TypeHierarchyRegistrationOptions();
//        typeHierarchyOptions.setId("Hierarchy");
//        typeHierarchyOptions.setWorkDoneProgress(false);
//        typeHierarchyOptions.setDocumentSelector(null);
//        capabilities.setTypeHierarchyProvider(true);

        final WorkspaceFoldersOptions workspaceOptions = new WorkspaceFoldersOptions();
        workspaceOptions.setChangeNotifications(true);
        workspaceOptions.setSupported(true);
        capabilities.setWorkspace(new WorkspaceServerCapabilities(workspaceOptions));

        return capabilities;
	}
}
