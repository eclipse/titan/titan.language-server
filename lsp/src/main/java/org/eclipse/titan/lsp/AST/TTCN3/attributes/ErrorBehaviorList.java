/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a list of error behavior settings in the codec API of the run-time
 * environment.
 *
 * @author Kristof Szabados
 */
public final class ErrorBehaviorList extends ASTNode implements ILocateableNode, IIncrementallyUpdatable {
	private static final String PREVIOUS_SETTING_IGNORED = "The previous setting is ignored";
	private static final String DUPLICATE_SETTING = "Duplicate setting for error type `{0}''";
	private static final String NOT_VALID_ERROR = "String `{0}'' is not a valid error {1}";
	private static final String[] VALID_TYPES = { "UNBOUND", "INCOMPL_ANY", "ENC_ENUM", "INCOMPL_MSG", "LEN_FORM", "INVAL_MSG", "REPR",
		"CONSTRAINT", "TAG", "SUPERFL", "EXTENSION", "DEC_ENUM", "DEC_DUPFLD", "DEC_MISSFLD", "DEC_OPENTYPE", "DEC_UCSTR", "LEN_ERR",
		"SIGN_ERR", "INCOMP_ORDER", "TOKEN_ERR", "LOG_MATCHING", "FLOAT_TR", "FLOAT_NAN", "OMITTED_TAG", "NEGTEST_CONFL", "EXTRA_DATA" };
	private static final String[] VALID_HANDLINGS = { "DEFAULT", "ERROR", "WARNING", "IGNORE" };

	// TODO could be optimized using real-life data
	private final List<ErrorBehaviorSetting> settings = new ArrayList<ErrorBehaviorSetting>(1);
	private final Map<String, ErrorBehaviorSetting> settingMap = new HashMap<String, ErrorBehaviorSetting>();
	private ErrorBehaviorSetting settingAll;

	/** the time when this error behavior list was checked the last time. */
	private CompilationTimeStamp lastTimeChecked;
	private Location location = Location.getNullLocation();

	public ErrorBehaviorList() {
		// do nothing
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = settings.size(); i < size; i++) {
			final ErrorBehaviorSetting setting = settings.get(i);
			if (setting == child) {
				return builder.append(".<setting ").append(i + 1).append('>');
			}
		}

		return builder;
	}

	public void addSetting(final ErrorBehaviorSetting setting) {
		if (setting != null) {
			settings.add(setting);
		}
	}

	public void addAllBehaviors(final ErrorBehaviorList other) {
		if (other != null) {
			settings.addAll(other.settings);
		}
	}

	public int getNofErrorBehaviors() {
		return settings.size();
	}

	public ErrorBehaviorSetting getBehaviorByIndex(final int index) {
		return settings.get(index);
	}

	public boolean hasSetting(final CompilationTimeStamp timestamp, final String errorType) {
		check(timestamp);
		return settingAll != null || settingMap.containsKey(errorType);
	}

	public String getHandling(final CompilationTimeStamp timestamp, final String errorType) {
		check(timestamp);
		if (settingMap.containsKey(errorType)) {
			return settingMap.get(errorType).getErrorHandling();
		} else if (settingAll != null) {
			return settingAll.getErrorHandling();
		}

		return "DEFAULT";
	}

	/**
	 * Does the semantic checking of the error behavior list..
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		for (final ErrorBehaviorSetting setting: settings) {
			final String errorType = setting.getErrorType();
			if ("ALL".equals(errorType)) {
				if (settingAll != null) {
					setting.getLocation().reportSemanticWarning(
							MessageFormat.format(DUPLICATE_SETTING, "ALL"));
					settingAll.getLocation().reportSemanticWarning(PREVIOUS_SETTING_IGNORED);
				}
				settingAll = setting;
				if (!settingMap.isEmpty()) {
					setting.getLocation().reportSemanticWarning("All setting before `ALL' are ignored");
					settingMap.clear();
				}
			} else {
				if (settingMap.containsKey(errorType)) {
					setting.getLocation().reportSemanticWarning(
							MessageFormat.format(DUPLICATE_SETTING, errorType));
					settingMap.get(errorType).getLocation().reportSemanticWarning(PREVIOUS_SETTING_IGNORED);
					settingMap.put(errorType, setting);
				} else {
					settingMap.put(errorType, setting);
				}

				boolean typeFound = false;
				for (final String validType : VALID_TYPES) {
					if (validType.equals(errorType)) {
						typeFound = true;
						break;
					}
				}
				if (!typeFound) {
					setting.getLocation().reportSemanticWarning(
							MessageFormat.format(NOT_VALID_ERROR, errorType, "type"));
				}
			}

			final String errorHandling = setting.getErrorHandling();
			boolean handlingFound = false;
			for (final String validHandling : VALID_HANDLINGS) {
				if (validHandling.equals(errorHandling)) {
					handlingFound = true;
					break;
				}
			}
			if (!handlingFound) {
				setting.getLocation().reportSemanticWarning(
						MessageFormat.format(NOT_VALID_ERROR, errorHandling, "handling"));
			}
		}

		lastTimeChecked = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final ErrorBehaviorSetting setting : settings) {
			setting.updateSyntax(reparser, false);
			reparser.updateLocation(setting.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final ErrorBehaviorSetting s : settings) {
			if (!s.accept(v)) {
				return false;
			}
		}
		return true;
	}
}
