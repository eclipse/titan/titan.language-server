package org.eclipse.titan.lsp.signatureHelp;

import java.util.List;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SignatureHelpParams;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ISignatureHelp;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.templates.NamedParameters;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstances;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.parsers.ttcn3parser.UnfinishedStatementParser;
import org.eclipse.titan.lsp.AST.Module;

public final class SignatureHelpProvider {
	private final Position cursorPosition;
	private Module module;
	private String sourceText;

	public SignatureHelpProvider(SignatureHelpParams params) {
		cursorPosition = new Position(params.getPosition());
		final Project project = Project.INSTANCE;
		final ProjectItem projectItem = project.getProjectItem(IOUtils.getTextDocumentPath(params.getTextDocument()));
		if (projectItem != null) {
			sourceText = projectItem.getSource();
			module = projectItem.getModule();
		}
	}

	private SignatureHelp getSignatureHelpForAssignment(Assignment ass, int actualParamIdx) {
		if (ass instanceof ISignatureHelp) {
			final int nofParams = ((ISignatureHelp)ass).getFormalParameterList().size();
			if (nofParams - 1 >= actualParamIdx) {
				final SignatureHelp sh = ((ISignatureHelp)ass).getSignatureHelp();
				sh.setActiveSignature(0);
				sh.getSignatures().get(0).setActiveParameter(actualParamIdx);
				sh.setActiveParameter(actualParamIdx);
				return sh;
			}
		}
		return null;
	}

	private Assignment getReferencedVisibleAssignment(Reference ref) {
		if (module instanceof TTCN3Module) {
			final Assignment ass = ((TTCN3Module) module)
					.getAssignments()
					.getLocalAssignmentByID(module.getLastCompilationTimeStamp(), ref.getId());
			if (ass != null) {
				return ass;
			}
			return ((TTCN3Module) module).getAssBySRef(module.getLastCompilationTimeStamp(), ref);
		}
		return null;
	}

	private Reference getActualReference(List<Reference> references) {
		if (references.isEmpty()) {
			return null;
		}
		return references
				.stream()
				.filter(ref -> ref.getLocation().getRange().containsPositionClosed(cursorPosition))
				.reduce((ref1, ref2) -> {
					// search for the reference closest to cursor position
					int lineDiff1 = cursorPosition.getLine() - ref1.getLocation().getStartLine();
					int charDiff1 = cursorPosition.getCharacter() - ref1.getLocation().getStartColumn();

					int lineDiff2 = cursorPosition.getLine() - ref2.getLocation().getStartLine();
					int charDiff2 = cursorPosition.getCharacter() - ref2.getLocation().getStartColumn();

					if (lineDiff1 == lineDiff2) {
						return charDiff1 - charDiff2 < 0 ? ref1 : ref2;
					}
					return lineDiff1 - lineDiff2 < 0 ? ref1 : ref2;
				}).orElse(null);
	}

	private int getActualParamIndex(Reference ref) {
		ISubReference subRef = ref.getSubreferences().get(0);
		// cursor on identifier
		if(subRef.getId().getLocation().getRange().containsPositionClosed(cursorPosition)) {
			return 0;
		}

		// cursor on parameter
		if (subRef.getReferenceType() == Subreference_type.parameterisedSubReference) {
			final ParsedActualParameters parsedParameters = ((ParameterisedSubReference) subRef).getParsedParameters();
			final NamedParameters namedParameters = parsedParameters.getNamedParameters();
			final TemplateInstances unnamedParameters = parsedParameters.getInstances();
			final int nofNamedParams = namedParameters.size();
			final int nofUnnamedParams = unnamedParameters.size();

			if (nofNamedParams == 0 && nofUnnamedParams == 0) {
				return 0;
			}

			final boolean isNamedParams = nofNamedParams - nofUnnamedParams > 0;

			int nofParams = isNamedParams ? nofNamedParams : nofUnnamedParams;

			for(int i = 0; i < nofParams; i++) {
				final TemplateInstance instance = isNamedParams ? namedParameters.get(i).getInstance() : unnamedParameters.get(i);
				if (instance.getLocation().getRange().containsPositionClosed(cursorPosition)) {
					return i;
				}
			}

			// cursor on last, unfinished parameter
			return nofParams;
		}

		return 0;
	}

	public SignatureHelp collectSignatureHelp() {
		final List<Reference> references = UnfinishedStatementParser
				.getReferencesForSignatureHelp(sourceText, module.getLocation().getFile(), cursorPosition);

		final Reference actualRef = getActualReference(references);
		if (actualRef != null) {
			final int paramIdx = getActualParamIndex(actualRef);
			final Assignment ass = getReferencedVisibleAssignment(actualRef);
			return getSignatureHelpForAssignment(ass, paramIdx);
		}
		return null;
	}
}
