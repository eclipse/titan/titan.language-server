/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import java.util.List;

import org.eclipse.lsp4j.generator.JsonRpcData;
import org.eclipse.lsp4j.jsonrpc.messages.Either;

@JsonRpcData
public class TpdProjectFiles {
	/* indicates if TPD processing was successful */
	public boolean success;
	
	/* List of filenames defined by TPD or error message */
	public Either<List<String>, String> result; 
}
