/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the parameter assignments of a parameter redirection.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class Parameter_Assignments extends ASTNode implements IIncrementallyUpdatable, ICollection<Parameter_Assignment> {
	private static final String FULLNAMEPART = ".parameterassignment_";

	private final List<Parameter_Assignment> parameterAssignments = new ArrayList<Parameter_Assignment>();

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0; i < parameterAssignments.size(); i++) {
			if (parameterAssignments.get(i) == child) {
				return builder.append(FULLNAMEPART).append(Integer.toString(i + 1));
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (Parameter_Assignment pa : parameterAssignments) {
			pa.setMyScope(scope);
		}
	}

	@Override
	public boolean add(final Parameter_Assignment parameterAssignment) {
		parameterAssignment.setFullNameParent(this);
		return parameterAssignments.add(parameterAssignment);
	}

	@Override
	public int size() {
		return parameterAssignments.size();
	}

	@Override
	public Parameter_Assignment get(final int index) {
		return parameterAssignments.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final Parameter_Assignment assignment : parameterAssignments) {
			assignment.updateSyntax(reparser, isDamaged);
			reparser.updateLocation(assignment.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final Parameter_Assignment pa : parameterAssignments) {
			pa.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final Parameter_Assignment pa : parameterAssignments) {
			if (!pa.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<Parameter_Assignment> iterator() {
		return parameterAssignments.iterator();
	}
}
