/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;

/**
 * This interface represents an element that can provide information about its members
 * 
 * @author Miklos Magyari
 *
 */
public interface IMemberInfo {
	/**
	 * Adds a formatted description of members to the hover content. The description is
	 * an arbitrary text that can include optional styling.<br>
	 * @param content The hover content info to be extended with the member information.
	 */
	void addMembersContent(Ttcn3HoverContent content);
}
