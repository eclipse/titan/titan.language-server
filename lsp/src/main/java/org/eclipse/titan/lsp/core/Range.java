/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.core;

import org.eclipse.lsp4j.jsonrpc.validation.NonNull;
import org.eclipse.titan.lsp.AST.Location;

/**
 * Titan Language Server specific range class implementing
 * the {@link ILocationComparer} interface
 * 
 * @author Miklos Magyari
 * */
public final class Range extends org.eclipse.lsp4j.Range implements ILocationComparer {
	/**
	 * Constructs an instance based on an LSP Range
	 * @param range
	 */
	public Range(org.eclipse.lsp4j.Range range) {
		super(range.getStart(), range.getEnd());
	}

	public Range(@NonNull final Position start, @NonNull final Position end) {
		super(start, end);
	}

	public Range(@NonNull final Location location) {
		super(location.getStartPosition(), location.getEndPosition());
	}

	@Override
	public boolean before(final ILocationComparer other) {
		if (!(other instanceof Range)) {
			throw new IllegalArgumentException();
		}
		final Range otherRange = (Range)other;
		final Position otherStart = new Position(otherRange.getStart());
		if (otherStart.after(new Position(getStart()))) {
			return true;
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean beforeOrEquals(final ILocationComparer other) {
		if (!(other instanceof Range)) {
			throw new IllegalArgumentException();
		}
		final Range otherRange = (Range)other;
		final Position otherStart = new Position(otherRange.getStart());
		if (otherStart.afterOrEquals(new Position(getStart()))) {
			return true;
		}
		return false;
	}

	@Override
	public boolean after(final ILocationComparer other) {
		if (!(other instanceof Range)) {
			throw new IllegalArgumentException();
		}
		final Range otherRange = (Range)other;
		final Position otherStart = new Position(otherRange.getStart());
		if (otherStart.before(new Position(getStart()))) {
			return true;
		}
		return false;
	}

	@Override
	public boolean afterOrEquals(final ILocationComparer other) {
		if (!(other instanceof Range)) {
			throw new IllegalArgumentException();
		}
		final Range otherRange = (Range)other;
		final Position otherStart = new Position(otherRange.getStart());
		if (otherStart.beforeOrEquals(new Position(getStart()))) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if a given range is inside of this range, including the range's endpoints
	 * 
	 * @param range
	 * @return
	 */
	public boolean containsClosed(final Range other) {
		final Position otherStart = new Position(other.getStart());
		final Position otherEnd = new Position(other.getEnd());
		return otherStart.afterOrEquals(new Position(this.getStart())) && otherEnd.beforeOrEquals(new Position(this.getEnd()));
	}

	/**
	 * Checks if a given position is inside of this range, including the range's endpoints
	 * 
	 * @param position
	 * @return
	 */
	public boolean containsPositionClosed(final Position position) {
		return position.afterOrEquals((Position)getStart()) && position.beforeOrEquals((Position)getEnd());
	}

	/**
	 * Checks if a given position is inside of this range, excluding the range's endpoints
	 * 
	 * @param position
	 * @return
	 */
	public boolean containsPositionOpen(final Position position) {
		return position.after((Position)getStart()) && position.before((Position)getEnd());
	}
}
