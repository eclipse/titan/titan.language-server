/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Constraint;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a permitted alphabet constraint
 *
 * @author Kristof Szabados
 */
public class PermittedAlphabetConstraint extends Constraint {
	private static final String FULLNAMEPART = ".<permitted alphabet constraint>";

	private final Constraint constraint;

	public PermittedAlphabetConstraint(final Constraint constraint) {
		super(Constraint_type.CT_PERMITTEDALPHABET);
		this.constraint = constraint;
	}

	@Override
	/** {@inheritDoc} */
	public PermittedAlphabetConstraint newInstance() {
		return new PermittedAlphabetConstraint(constraint);
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (constraint == child) {
			return builder.append(FULLNAMEPART);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		// not yet supported
		final String severity = Configuration.INSTANCE.getString(Configuration.UNSUPPORTED_CONSTRUCT_SEVERITY, "warning");
		myType.getLocation().reportConfigurableSemanticProblem(severity, "Permitted alphabet constraint not yet supported");
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		// TODO
		return true;
	}

}
