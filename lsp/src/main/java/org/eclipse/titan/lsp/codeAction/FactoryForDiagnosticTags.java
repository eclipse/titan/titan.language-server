/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.TextDocumentIdentifier;

public class FactoryForDiagnosticTags implements ICodeActionProviderFactory {

	@Override
	public ICodeActionProvider createProvider(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		return new CodeActionsForDiagnosticTag(doc, diagnostic);
	}
}
