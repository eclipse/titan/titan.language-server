/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.eclipse.titan.lsp.core.Project;

/**
 * General parsing related static class. Provides a common root for such
 * hierarchies
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public final class GlobalParser {
	// FIXME if ttcnpp is handled as ttcn file, this should be extended as well
	private static final String[] SUPPORTED_TTCN3_EXTENSIONS = new String[] { "ttcn3", "ttcn" };
	private static final String TTCNPP_EXTENSION = "ttcnpp";
	public static final String TTCNIN_EXTENSION = "ttcnin";
	private static final String[] SUPPORTED_ASN1_EXTENSIONS = new String[] { "asn1", "asn" };
	private static final String[] SUPPORTED_CONFIG_FILE_EXTENSIONS = new String[] { "cfg" };

	/** The Source Parsers whose project was already analyzed */
	private static final Map<Project, ProjectSourceParser> TTCN3_PARSERS = new ConcurrentHashMap<Project, ProjectSourceParser>();

	/** The Configuration Parsers whose project was already analyzed */
	private static final Map<Project, ProjectConfigurationParser> CFG_PARSERS = new ConcurrentHashMap<Project, ProjectConfigurationParser>();

	/** private constructor to disable instantiation */
	private GlobalParser() {}

	/**
	 * Returns whether the specified string is a supported file extension for TTCN-3 files or not.
	 * @param extension the string to check
	 * @return true if it is a supported TTCN-3 file extension, false otherwise
	 */
	public static boolean isSupportedTTCN3Extension(final String extension) {
		for (String ext : SUPPORTED_TTCN3_EXTENSIONS) {
			if (ext.equalsIgnoreCase(extension)) {
				return true;
			}
		}
		return isSupportedTTCNPPExtension(extension);
	}

	/**
	 * Returns whether the specified string is a supported file extension for TTCNIN files or not.
	 * @param extension the string to check
	 * @return true if it is a supported TTCNIN file extension, false otherwise
	 */
	public static boolean isSupportedTTCNINExtension(final String extension) {
		return TTCNIN_EXTENSION.equalsIgnoreCase(extension);
	}

	/**
	 * Returns whether the specified string is a supported file extension for TTCNPP files or not.
	 * @param extension the string to check
	 * @return true if it is a supported TTCNPP file extension, false otherwise
	 */
	public static boolean isSupportedTTCNPPExtension(final String extension) {
		return TTCNPP_EXTENSION.equalsIgnoreCase(extension);
	}

	/**
	 * Returns whether the specified string is a supported file extension for configuration files or not.
	 * @param extension the string to check
	 * @return true if it is a supported configuration file extension, false otherwise
	 */
	public static boolean isSupportedConfigFileExtension(final String extension) {
		for (String ext : SUPPORTED_CONFIG_FILE_EXTENSIONS) {
			if (ext.equalsIgnoreCase(extension)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns whether the specified string is a supported file extension for ASN.1 files or not.
	 * @param extension the string to check
	 * @return true if it is a supported ASN.1 file extension, false otherwise
	 */
	public static boolean isSupportedASN1Extension(final String extension) {
		for (String ext : SUPPORTED_ASN1_EXTENSIONS) {
			if (ext.equalsIgnoreCase(extension)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns whether the specified string is a supported file extension or not.
	 * @param extension the string to check
	 * @return true if it is a supported file extension, false otherwise
	 */
	public static boolean isSupportedExtension(final String extension) {
		return isSupportedTTCN3Extension(extension) ||
				isSupportedTTCNINExtension(extension) ||
				isSupportedConfigFileExtension(extension) ||
				isSupportedASN1Extension(extension);
	}

	/**
	 * Creates a parser that handles the parsing related jobs of the given project.
	 * @param project the project to create a parser for
	 * @return the parser which handles the parsing of the provided project
	 */
	public static ProjectSourceParser getProjectSourceParser(final Project project) {
		ProjectSourceParser tempParser;
		if (TTCN3_PARSERS.containsKey(project)) {
			tempParser = TTCN3_PARSERS.get(project);
		} else {
			tempParser = new ProjectSourceParser(project);
			TTCN3_PARSERS.put(project, tempParser);
		}
		return tempParser;
	}

	/**
	 * Creates a parser that handles the parsing related jobs of the given project.
	 * @param project the project to create a parser for
	 * @return the parser which handles the parsing of the provided project
	 */
	public static ProjectConfigurationParser getConfigSourceParser(final Project project) {
		ProjectConfigurationParser tempParser;
		if (CFG_PARSERS.containsKey(project)) {
			tempParser = CFG_PARSERS.get(project);
		} else {
			tempParser = new ProjectConfigurationParser(project);
			CFG_PARSERS.put(project, tempParser);
		}
		return tempParser;
	}

	/**
	 * Returns true if the resource (file, folder or project) contains ttcnpp file not excluded from the project.
	 * @param resource to start from.
	 * @return {@code true} if there is a ttcnpp file in the project, {@code false} otherwise.
	 */
	public static boolean hasTtcnppFiles(final File resource)  {
//		if(resource instanceof IFolder && ResourceExclusionHelper.isDirectlyExcluded((IFolder) resource)) {
//			return false;
//		} else if(resource instanceof IFile && ResourceExclusionHelper.isDirectlyExcluded((IFile) resource)) {
//			return false;
//		}
//
//		if (resource instanceof IProject || resource instanceof IFolder) {
//			final IResource[] children = resource instanceof IFolder ? ((IFolder) resource).members() : ((IProject) resource).members();
//			for (final IResource res : children) {
//				if (hasTtcnppFiles(res)) {
//					return true;
//				}
//			}
//		} else if (resource instanceof IFile) {
//			final IFile file = (IFile) resource;
//			return "ttcnpp".equals(file.getFileExtension());
//		}

		return false;
	}

	/**
	 * Clears all on-the-fly information stored about the project.
	 * @param project the project whose information is to be cleared.
	 */
	public static void clearAllInformation(final Project project) {
		if (TTCN3_PARSERS.containsKey(project)) {
			TTCN3_PARSERS.remove(project);
		}

		if (CFG_PARSERS.containsKey(project)) {
			CFG_PARSERS.remove(project);
		}
	}

	/**
	 * Clears all information about all of the known projects.
	 */
	public static void clearAllInformation() {
		TTCN3_PARSERS.clear();
		CFG_PARSERS.clear();
	}

	/**
	 * Force the next semantic analyzation to reanalyze everything.
	 */
	public static void clearSemanticInformation() {
		for (final ProjectSourceParser parser : TTCN3_PARSERS.values()) {
			parser.clearSemanticInformation();
		}
	}

	/**
	 * Returns the set of the analyzed projects
	 * @return the set of the analyzed projects
	 */
	public static Set<Project> getAllAnalyzedProjects() {
		return TTCN3_PARSERS.keySet();
	}
}
