/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.EnumSet;
import java.util.List;
import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.IType.ValueCheckingOptions;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.DocumentComment.CommentTag;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.ComponentTypeBody;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_ExpressionContext;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticModifier;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * The Def_Const class represents TTCN3 constant definitions.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public final class Def_Const extends Definition {
	private static final String FULLNAMEPART = ".<type>";
	private static final String PORTNOTALLOWED = "Constant cannot be defined for port type `{0}''";
	private static final String SIGNATURENOTALLOWED = "Constant cannot be defined for signature type `{0}''";
	private static final String CLASSNOTALLOWED = "Constant cannot be defined for class type `{0}''";
	private static final String CONTAINSCLASS = "Constant cannot be defined for type `{0}'', which contains a class";
	private static final String TRAITMEMBERS = "Trait class type `{0}'' cannot have constant members";
	private static final String MISSING_INIT_VALUE = "Missing initial value";

	private static final String KIND = "constant";

	private final Type type;
	private Value value;

	public Def_Const(final Identifier identifier, final Type type, final Value value) {
		super(identifier);
		this.type = type;
		this.value = value;

		if (type != null) {
			type.setOwnertype(TypeOwner_type.OT_CONST_DEF, this);
			type.setFullNameParent(this);
		}
		if (value != null) {
			value.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_CONST;
	}

	public static String getKind() {
		return KIND + " ";
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (type == child) {
			return builder.append(FULLNAMEPART);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (type != null) {
			type.setMyScope(scope);
		}
		if (value != null) {
			value.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return type;
	}

	public IValue getValue() {
		if (lastTimeChecked == null) {
			check(CompilationTimeStamp.getBaseTimestamp());
		}

		return value;
	}

	@Override
	/** {@inheritDoc} */
	public IValue getSetting(final CompilationTimeStamp timestamp) {
		return getValue();
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public String getDescription() {
		final StringBuilder builder = new StringBuilder();
		builder.append(getAssignmentName()).append(" `");

		if (isLocal()) {
			builder.append(identifier.getDisplayName());
		} else {
			builder.append(getFullName());
		}

		builder.append('\'');
		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		return SymbolKind.Constant;
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return location.getRange();
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() {
		return identifier.getLocation().getRange();
	}

	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		final DocumentSymbol symbol = new DocumentSymbol(identifier.getDisplayName(), getOutlineSymbolKind(),
			getOutlineRange(), getOutlineSelectionRange());
		symbol.setChildren(getOutlineSymbolChildren());

		return symbol;
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		int result = super.category();
		if (type != null) {
			result += type.category();
		}
		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;

		if (!getMyScope().isClassScope()) {
			if (getMyScope() instanceof ComponentTypeBody) {
				NamingConventionHelper.checkConvention(NamingConventionElement.ComponentConstant, identifier, this);
			} else if (isLocal()) {
				NamingConventionHelper.checkConvention(NamingConventionElement.LocalConstant, identifier, this);
			} else {
				NamingConventionHelper.checkConvention(NamingConventionElement.GlobalConstant, identifier, this);
			}
		} else {
			if (isLocal()) {
				NamingConventionHelper.checkConvention(NamingConventionElement.LocalConstant, identifier, this);
			} else {
				NamingConventionHelper.checkConvention(NamingConventionElement.GlobalConstant, identifier, this);
			}
		}

		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (type == null) {
			return;
		}

		type.check(timestamp);

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, true);
			withAttributesPath.checkAttributes(timestamp, type.getTypeRefdLast(timestamp).getTypetype());
		}

		if (value == null) {
			if (!getMyScope().isClassScope()) {
				location.reportSemanticError(MISSING_INIT_VALUE);
			}
			return;
		}

		value.setMyGovernor(type);
		final IValue temporalValue = type.checkThisValueRef(timestamp, value);

		final IType lastType = type.getTypeRefdLast(timestamp);
		switch (lastType.getTypetype()) {
		case TYPE_PORT:
			location.reportSemanticError(MessageFormat.format(PORTNOTALLOWED, lastType.getFullName()));
			break;
		case TYPE_SIGNATURE:
			location.reportSemanticError(MessageFormat.format(SIGNATURENOTALLOWED, lastType.getFullName()));
			break;
		case TYPE_CLASS:
			location.reportSemanticError(MessageFormat.format(CLASSNOTALLOWED, lastType.getTypename()));
			break;
		default:
			if (lastType.containsClass(timestamp)) {
				location.reportSemanticError(MessageFormat.format(CONTAINSCLASS, lastType.getTypename()));
				break;
			}
			if (myScope.isClassScope() && myScope.getScopeClass().isTrait()) {
				location.reportSemanticError(MessageFormat.format(TRAITMEMBERS,
					myScope.getScopeClass().getTypename()));
			}
			break;
		}

		type.checkThisValue(timestamp, temporalValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, true, false, true,
				hasImplicitOmitAttribute(timestamp), false));

		checkErroneousAttributes(timestamp);

		final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		chain.add(this);
		temporalValue.checkRecursions(timestamp, chain);
		chain.release();
		
		checkDocumentComment();
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkIdentical(final CompilationTimeStamp timestamp, final Definition definition) {
		check(timestamp);
		definition.check(timestamp);

		if (!Assignment_type.A_CONST.semanticallyEquals(definition.getAssignmentType())) {
			location.reportSemanticError(MessageFormat.format(
					"Local definition `{0}'' is a constant, but the definition inherited from component type `{1}'' is a {2}",
					identifier.getDisplayName(), definition.getMyScope().getFullName(), definition.getAssignmentName()));
			return false;
		}

		final Def_Const otherConstant = (Def_Const) definition;
		if (!type.isIdentical(timestamp, otherConstant.type)) {
			final String message = MessageFormat
					.format("Local constant `{0}'' has type `{1}'', but the constant inherited from component type `{2}'' has type `{3}''",
							identifier.getDisplayName(), type.getTypename(), otherConstant.getMyScope().getFullName(),
							otherConstant.type.getTypename());
			type.getLocation().reportSemanticError(message);
			return false;
		}

		if (!value.checkEquality(timestamp, otherConstant.value)) {
			final String message = MessageFormat.format(
					"Local constant `{0}'' and the constant inherited from component type `{1}'' have different values",
					identifier.getDisplayName(), otherConstant.getMyScope().getFullName());
			value.getLocation().reportSemanticError(message);
			return false;
		}

		return true;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		final StringBuilder builder = new StringBuilder(KIND).append(" ");
		if (type != null) {
			type.getProposalDescription(builder);
		}
		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		final List<Integer> result = super.getPossibleExtensionStarterTokens();

		result.add(Ttcn3Lexer.COMMA);
		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					throw new ReParseException(result);
				}
			}

			if (type != null) {
				if (enveloped) {
					type.updateSyntax(reparser, false);
					reparser.updateLocation(type.getLocation());
				} else if (reparser.envelopsDamage(type.getLocation())) {
					type.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(type.getLocation());
				}
			}

			if (value != null) {
				if (enveloped) {
					value.updateSyntax(reparser, false);
					reparser.updateLocation(value.getLocation());
				} else if (reparser.envelopsDamage(value.getLocation())) {
					reparser.extendDamagedRegion(value.getLocation().getStartPosition(), value.getLocation().getEndPosition());
					final int result = reparse( reparser );
					if (result == 0) {
						enveloped = true;
						value.setFullNameParent(this);
						value.setMyScope(getMyScope());
					} else {
						throw new ReParseException();
					}
				}
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					withAttributesPath.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(withAttributesPath.getLocation());
				}
			}

			if (!enveloped) {
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}

		if (value != null) {
			value.updateSyntax(reparser, false);
			reparser.updateLocation(value.getLocation());
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	private int reparse(final TTCN3ReparseUpdater aReparser) {
		return aReparser.parse(parser -> {
			final Pr_ExpressionContext root = parser.pr_Expression();
			ParserUtilities.logParseTree( root, parser );
			final Value newValue = root.value;

			final ParseTree rootEof =  parser.pr_EndOfFile();
			ParserUtilities.logParseTree( rootEof, parser );
			if ( parser.isErrorListEmpty() ) {
				if (newValue != null) {
					value = newValue;
				}
			}
		});
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (type != null) {
			type.findReferences(referenceFinder, foundIdentifiers);
		}
		if (value != null) {
			value.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (type != null && !type.accept(v)) {
			return false;
		}
		if (value != null && !value.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}

		final IType assType = getType(getLastTimeChecked());
		hoverContent.addStyledText(KIND, Ttcn3HoverContent.BOLD).addText(" ");
		hoverContent.addStyledText(assType != null ? assType.getTypename() : "<?>")
			.addStyledText(" ").addStyledText(getFullNameForDocComment());
		hoverContent.addTagWithText("Value: ", getValue() != null
				? getValue().createStringRepresentation(): GeneralConstants.UNKNOWN);

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addMembersContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding)
			.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = super.getCompletionItem();
		item.setKind(CompletionItemKind.Constant);
		return item;
	}
	
	@Override
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticTokenAndModifier(identifier.getLocation(), SemanticType.Variable, SemanticModifier.Readonly);
		super.setSemanticInformation();
	}

	@Override
	public void checkDocumentComment() {
		if (!hasDocumentComment()) {
			return;
		}

		final EnumSet<CommentTag> naTags =
			EnumSet.of(CommentTag.Config, CommentTag.Exception, CommentTag.Param, CommentTag.Priority, CommentTag.Purpose);
		naTags.addAll(EnumSet.of(CommentTag.Requirement, CommentTag.Return, CommentTag.Verdict));

		getDocumentComment().checkNonApplicableTags(naTags, "constants");
		super.checkDocumentComment();
	}
}
