/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.util.EnumSet;
import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.DocumentComment.CommentTag;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;


/**
 * The Def_Testcase class represents TTCN3 testcase definitions.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public final class Def_Testcase extends Definition implements ISignatureHelp {
	private static final String FULLNAMEPART1 = ".<formal_parameter_list>";
	private static final String FULLNAMEPART2 = ".<runs_on_type>";
	private static final String FULLNAMEPART3 = ".<system_type>";
	private static final String FULLNAMEPART4 = ".<statement_block>";
	private static final String FULLNAMEPART5 = ".<finally_block>";

	private static final String DASHALLOWEDONLYFORTEMPLATES = "Using not used symbol (`-') as the default parameter"
			+ " is allowed only for modified templates";
	private static final String MISSING_RUNSON = "The `runs on' clause is missing"; 

	private static final String KIND = "testcase";

	private final FormalParameterList formalParList;
	private final Reference runsOnReference;
	private Component_Type runsOnType = null;
	private final Reference systemReference;
	private Component_Type systemType = null;
	private final StatementBlock block;
	private NamedBridgeScope bridgeScope = null;

	public Def_Testcase(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference systemRef, final StatementBlock block) {
		super(identifier);
		this.formalParList = formalParameters;
		if (formalParList != null) {
			formalParList.setMyDefinition(this);
		}
		
		if (runsOnRef == null && getTtcnVersion().equals(Ttcn_version.Y2011)) {
			identifier.getLocation().reportSyntacticError(MISSING_RUNSON);
		}
		
		this.runsOnReference = runsOnRef;
		this.systemReference = systemRef;
		this.block = block;
		if (block != null) {
			block.setMyDefinition(this);
			block.setFullNameParent(this);
		}

		if (formalParList != null) {
			formalParList.setFullNameParent(this);
		}
		if (runsOnRef != null) {
			runsOnRef.setFullNameParent(this);
		}
		if (systemRef != null) {
			systemRef.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_TESTCASE;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (formalParList == child) {
			return builder.append(FULLNAMEPART1);
		} else if (runsOnReference == child) {
			return builder.append(FULLNAMEPART2);
		} else if (systemReference == child) {
			return builder.append(FULLNAMEPART3);
		} else if (block == child) {
			return builder.append(FULLNAMEPART4);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public FormalParameterList getFormalParameterList() {
		return formalParList;
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		if (Configuration.INSTANCE.getBoolean(Configuration.OUTLINE_SHOW_TESTCASES, true)) {
			return SymbolKind.Method;
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return location.getRange();
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() {
		return identifier.getLocation().getRange();
	}

	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		if (Configuration.INSTANCE.getBoolean(Configuration.OUTLINE_SHOW_FUNCTIONS, true)) {
			final DocumentSymbol symbol = new DocumentSymbol(identifier.getDisplayName(), getOutlineSymbolKind(),
				getOutlineRange(), getOutlineSelectionRange());
			symbol.setChildren(getOutlineSymbolChildren());
			
			return symbol;
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		final StringBuilder text = new StringBuilder(identifier.getDisplayName());
		if (formalParList == null || lastTimeChecked == null) {
			return text.toString();
		}

		text.append('(');
		for (int i = 0; i < formalParList.size(); i++) {
			if (i != 0) {
				text.append(", ");
			}

			final FormalParameter parameter = formalParList.get(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				text.append("timer");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					text.append("Unknown type");
				} else {
					text.append(type.getTypename());
				}
				text.append(' ')
					.append(parameter.getIdentifier().getDisplayName());
			}
		}
		text.append(')');
		return text.toString();
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalDescription() {
		final StringBuilder nameBuilder = new StringBuilder(identifier.getDisplayName());
		nameBuilder.append('(');
		formalParList.getAsProposalDesriptionPart(nameBuilder);
		nameBuilder.append(')');
		return nameBuilder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		if (bridgeScope != null && bridgeScope.getParentScope() == scope) {
			return;
		}

		bridgeScope = new NamedBridgeScope();
		bridgeScope.setParentScope(scope);
		scope.addSubScope(getLocation(), bridgeScope);
		bridgeScope.setScopeMacroName(identifier.getDisplayName());

		super.setMyScope(bridgeScope);
		if (runsOnReference != null) {
			runsOnReference.setMyScope(bridgeScope);
		}
		if (systemReference != null) {
			systemReference.setMyScope(bridgeScope);
		}
		formalParList.setMyScope(bridgeScope);
		if (block != null) {
			block.setMyScope(formalParList);
			bridgeScope.addSubScope(block.getLocation(), block);
		}
		bridgeScope.addSubScope(formalParList.getLocation(), formalParList);
	}

	public Component_Type getRunsOnType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return runsOnType;
	}

	public Reference getRunsOnReference(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return runsOnReference;
	}

	public Component_Type getSystemType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return systemType;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;

		runsOnType = null;
		systemType = null;

		if (runsOnReference != null) {
			runsOnType = runsOnReference.chkComponentypeReference(timestamp);
			if (runsOnType != null) {
				final Scope formalParlistPreviosScope = formalParList.getParentScope();
				if (formalParlistPreviosScope instanceof RunsOnScope
						&& ((RunsOnScope) formalParlistPreviosScope).getParentScope() == myScope) {
					((RunsOnScope) formalParlistPreviosScope).setComponentType(runsOnType);
				} else {
					final Scope tempScope = new RunsOnScope(runsOnType, myScope);
					formalParList.setMyScope(tempScope);
				}
			}
		}

		if (systemReference != null) {
			systemType = systemReference.chkComponentypeReference(timestamp);
		}

		if (formalParList.hasNotusedDefaultValue()) {
			formalParList.getLocation().reportSemanticError(DASHALLOWEDONLYFORTEMPLATES);
			return;
		}

		boolean canSkip = false;
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					canSkip = true;
				}
			}
		}

		if(!canSkip) {
			formalParList.reset();
		}
		formalParList.check(timestamp, getAssignmentType());

		if(canSkip) {
			return;
		}

		NamingConventionHelper.checkConvention(NamingConventionElement.Testcase, identifier, this);
 		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (block != null) {
			block.check(timestamp);

			block.postCheck();
		}
		
		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}
		
		checkDocumentComment();
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					return;
				}
			}
		}
		super.postCheck();
		formalParList.postCheck();
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;
			int result = 1;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					removeBridge();
					throw new ReParseException(result);
				}
			}

			if (formalParList != null) {
				if (enveloped) {
					formalParList.updateSyntax(reparser, false);
					reparser.updateLocation(formalParList.getLocation());
				} else if (reparser.envelopsDamage(formalParList.getLocation())) {
					try {
						formalParList.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(formalParList.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (runsOnReference != null) {
				if (enveloped) {
					runsOnReference.updateSyntax(reparser, false);
					reparser.updateLocation(runsOnReference.getLocation());
				} else if (reparser.envelopsDamage(runsOnReference.getLocation())) {
					try {
						runsOnReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(runsOnReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (systemReference != null) {
				if (enveloped) {
					systemReference.updateSyntax(reparser, false);
					reparser.updateLocation(systemReference.getLocation());
				} else if (reparser.envelopsDamage(systemReference.getLocation())) {
					try {
						systemReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(systemReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (block != null) {
				if (enveloped) {
					block.updateSyntax(reparser, false);
					reparser.updateLocation(block.getLocation());
				} else if (reparser.envelopsDamage(block.getLocation())) {
					try {
						block.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(block.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					try {
						withAttributesPath.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(withAttributesPath.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (!enveloped) {
				removeBridge();
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());
		if (formalParList != null) {
			formalParList.updateSyntax(reparser, false);
			reparser.updateLocation(formalParList.getLocation());
		}

		if (runsOnReference != null) {
			runsOnReference.updateSyntax(reparser, false);
			reparser.updateLocation(runsOnReference.getLocation());
		}

		if (systemReference != null) {
			systemReference.updateSyntax(reparser, false);
			reparser.updateLocation(systemReference.getLocation());
		}

		if (block != null) {
			block.updateSyntax(reparser, false);
			reparser.updateLocation(block.getLocation());
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	/**
	 * Removes the name bridging scope.
	 * */
	private void removeBridge() {
		if (bridgeScope != null) {
			bridgeScope.remove();
			bridgeScope = null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (formalParList != null) {
			formalParList.findReferences(referenceFinder, foundIdentifiers);
		}
		if (runsOnReference != null) {
			runsOnReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (systemReference != null) {
			systemReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (block != null) {
			block.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (formalParList != null && !formalParList.accept(v)) {
			return false;
		}
		if (runsOnReference != null && !runsOnReference.accept(v)) {
			return false;
		}
		if (systemReference != null && !systemReference.accept(v)) {
			return false;
		}
		if (block != null && !block.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();
		
		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}

		hoverContent.addStyledText(KIND, Ttcn3HoverContent.BOLD).addText(" ");
		hoverContent.addStyledText(getFullName());

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addParamsContent(hoverContent, formalParList);
			dc.addVerdictsContent(hoverContent);
			dc.addConfigContent(hoverContent);
			dc.addRequirementsContent(hoverContent);
			dc.addPurposeContent(hoverContent);
			dc.addPriorityContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		} else {
			addFormalParameterListToHoverContent(hoverContent, lastTimeChecked);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding);

		if (formalParList!= null && formalParList.size() > 0) {
			for (final FormalParameter param : formalParList) {
				sb.append(ind).append(ICommentable.PARAM_TAG).append(" ")
					.append(param.getIdentifier().getDisplayName()).append(lineEnding);
			}
		}

		sb.append(ind).append(ICommentable.VERDICT_TAG).append(" pass ").append(lineEnding)
			.append(ind).append(ICommentable.VERDICT_TAG).append(" fail ").append(lineEnding)
			.append(ind).append(ICommentable.VERDICT_TAG).append(" inconc ").append(lineEnding);

		sb.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}
	
	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = super.getCompletionItem();
		item.setKind(CompletionItemKind.Function);
		return item;
	}
	
	@Override
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(identifier.getLocation(), SemanticType.Testcase);
		super.setSemanticInformation();
	}
	
	@Override
	public void checkDocumentComment() {
		if (!hasDocumentComment()) {
			return;
		}
		
		getDocumentComment().checkNonApplicableTags(
			EnumSet.of(CommentTag.Exception, CommentTag.Member, CommentTag.Return), "testcases");
		
		super.checkDocumentComment();
	}
}
