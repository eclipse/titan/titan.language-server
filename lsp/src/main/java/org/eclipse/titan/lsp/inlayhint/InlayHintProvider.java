/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.inlayhint;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.lsp4j.InlayHint;
import org.eclipse.lsp4j.InlayHintParams;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.brokenpartsanalyzers.ReferenceCollector;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.Range;

public class InlayHintProvider {

	private Range range;
	private TextDocumentIdentifier doc;
	public InlayHintProvider(final InlayHintParams params) {
		range = new Range(params.getRange());
		doc = params.getTextDocument();
	}

	private Module getModule() {
		return Project.INSTANCE.getModuleByPath(IOUtils.getPath(doc.getUri()));
	}

	private Set<Reference> getReferencesFromTTCN3Module() {
		final Module module = getModule();
		if (module instanceof TTCN3Module) {
			final ReferenceCollector visitor = new ReferenceCollector();
			module.accept(visitor);
			return visitor.getReferences();
		}
		return Collections.emptySet();
	}

	public List<InlayHint> provideInlayHints() {
		final Set<Reference> references = getReferencesFromTTCN3Module();
		if (!references.isEmpty()) {
			final InlayHintsForFunctionParams hintsForFnParams = new InlayHintsForFunctionParams(references, range);
			return hintsForFnParams.getHints();
		}
		return Collections.emptyList();
	}
}
