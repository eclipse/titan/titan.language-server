/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.titan.lsp.AST.ICollection;

/**
 * Class to represent a (perhaps optional) sequence of OCS_Nodes.
 *
 * @author Kristof Szabados
 */
public final class ObjectClassSyntax_sequence extends ObjectClassSyntax_Node
	implements ICollection<ObjectClassSyntax_Node> {

	private final ArrayList<ObjectClassSyntax_Node> objectClassSyntaxes;
	private final boolean isOptional;
	/**
	 * This is used when default syntax is active. Then, if there is at
	 * least one parsed setting in the object, then the sequence must begin
	 * with a comma (',').
	 */
	private final boolean optionalFirstComma;

	public ObjectClassSyntax_sequence(final boolean isOptional, final boolean optionalFirstComma) {
		objectClassSyntaxes = new ArrayList<ObjectClassSyntax_Node>();
		this.isOptional = isOptional;
		this.optionalFirstComma = optionalFirstComma;
	}

	@Override
	/** {@inheritDoc} */
	public void accept(final IObjectClassSyntax_Visitor visitor) {
		visitor.visitSequence(this);
	}

	@Override
	public boolean add(final ObjectClassSyntax_Node node) {
		if (node != null) {
			return objectClassSyntaxes.add(node);
		}
		return false;
	}

	/**
	 * Trim the internal storage to the current number of elements.
	 */
	public void trimToSize() {
		objectClassSyntaxes.trimToSize();
	}

	@Override
	public int size() {
		return objectClassSyntaxes.size();
	}

	@Override
	public ObjectClassSyntax_Node get(final int index) {
		return objectClassSyntaxes.get(index);
	}

	/**
	 * @return {@code true} if the sequence is optional, {@code false}
	 *         otherwise.
	 */
	public boolean isOptional() {
		return isOptional;
	}

	public boolean getOptionalFirstComma() {
		return optionalFirstComma;
	}

	@Override
	/** {@inheritDoc} */
	public String getDisplayName() {
		final StringBuilder builder = new StringBuilder();

		if (isOptional) {
			builder.append('[');
		}

		for (int i = 0; i < objectClassSyntaxes.size(); i++) {
			if (i > 0) {
				builder.append(' ');
			}
			builder.append(objectClassSyntaxes.get(i).getDisplayName());
		}

		if (isOptional) {
			builder.append(']');
		}

		return builder.toString();
	}

	@Override
	public Iterator<ObjectClassSyntax_Node> iterator() {
		return objectClassSyntaxes.iterator();
	}
}
