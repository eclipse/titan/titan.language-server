/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.inlayhint;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.InlayHint;
import org.eclipse.lsp4j.Position;
import org.eclipse.titan.lsp.core.Range;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ActualParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ActualParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Default_ActualParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Value_ActualParameter;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

public class InlayHintsForFunctionParams {
	private Range displayedRange;
	private static final Set<Assignment_type> FUNCTION_TYPES = EnumSet.of(
		Assignment_type.A_EXT_FUNCTION,
		Assignment_type.A_EXT_FUNCTION_RTEMP,
		Assignment_type.A_EXT_FUNCTION_RVAL,
		Assignment_type.A_FUNCTION,
		Assignment_type.A_FUNCTION_RTEMP,
		Assignment_type.A_FUNCTION_RVAL
	);

	private static boolean isFunction(Assignment_type assType) {
		return FUNCTION_TYPES.contains(assType);
	}

	private Set<Reference> references;

	public InlayHintsForFunctionParams(Set<Reference> references, Range range) {
		this.references = references;
		displayedRange = range;
	}

	private List<Reference> filterDisplayedReferences() {
		return references.stream().filter(ref ->
			displayedRange.containsClosed(ref.getLocation().getRange()))
				.collect(Collectors.toList());
	}

	private List<Reference> filterFunctionReferences() {
		final List<Reference> displayedRefs = filterDisplayedReferences();
		return displayedRefs.stream().filter(ref -> {
			final CompilationTimeStamp lastTimeChecked = ref.getLastTimeChecked();
			final CompilationTimeStamp timestamp = lastTimeChecked != null ? lastTimeChecked : CompilationTimeStamp.getBaseTimestamp();
			final Assignment ass = ref.getRefdAssignment(timestamp, false);
			return ass != null && isFunction(ass.getAssignmentType());
		}).collect(Collectors.toList());
	}

	private List<ParameterisedSubReference> getParameterizedSubRefs(Reference functionRef) {
		return functionRef.getSubreferences()
				.stream()
				.filter(subRef -> subRef instanceof ParameterisedSubReference)
				.map(subRef -> (ParameterisedSubReference) subRef)
				.collect(Collectors.toList());
	}

	private List<InlayHint> collectInlayHints() {
		final List<InlayHint> hints = new ArrayList<>();
		final List<Reference> functionReferences = filterFunctionReferences();
		functionReferences
			.stream()
			.forEach(fn -> {
				final List<ParameterisedSubReference> parameterizedSubRefs = getParameterizedSubRefs(fn);

				parameterizedSubRefs
					.stream()
					.forEach(subRef -> {
						final ActualParameterList actualParamList = subRef.getActualParameters();
						final ParsedActualParameters parsedParamList = subRef.getParsedParameters();

						final FormalParameterList formalParList = parsedParamList.formalParList;
						if (formalParList == null) {
							return;
						}
						final int nofParams = formalParList.size();
						for (int i = 0; i < nofParams; i++) {
							final FormalParameter parsedParam = parsedParamList.formalParList.get(i);
							final ActualParameter actualParam = actualParamList.get(i);
							if (actualParam instanceof Value_ActualParameter) {
								if (((Value_ActualParameter)actualParam).getValue() == null) {
									continue;
								}
							}

							final CompilationTimeStamp lastTimeChecked = parsedParam.getLastTimeChecked();
							final CompilationTimeStamp timestamp = lastTimeChecked != null ? lastTimeChecked : CompilationTimeStamp.getBaseTimestamp();
							final Type paramType = parsedParamList.formalParList.get(i).getType(timestamp);
							if (paramType == null) {
								continue;
							}
							String paramTypeName = paramType instanceof Referenced_Type ? ((Referenced_Type)paramType).getReference().getDisplayName() : paramType.getTypename();
							if (actualParam == null || actualParam instanceof Default_ActualParameter) {
								continue;
							}
							final Location labelLocation = actualParam.getLocation();
							if (labelLocation != null && labelLocation != NULL_Location.INSTANCE) {
								final Position labelPosition = labelLocation.getStartPosition();
	
								final InlayHint hint = new InlayHint();
								hint.setLabel(paramTypeName);
								hint.setPosition(labelPosition);
								hint.setPaddingRight(true);
								hints.add(hint);
							}
						}
				});
		});

		return hints;
	}

	public List<InlayHint> getHints() {
		return collectInlayHints();
	}
}
