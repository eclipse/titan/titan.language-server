/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.common;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.Module.module_type;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.lspextensions.MetricDataParams;

public class MetricDataProvider {
	private final MetricData data;
	private Module module;
	private String scope;
	private String targetName;
	private String targetType;

	public MetricDataProvider(MetricDataParams params) {
		scope = params.getScope();
		data = MetricData.measure(Project.INSTANCE);
		targetName = params.getTargetName();
		targetType = params.getTargetType();

		if (scope.equals(Module.MODULE)) {
			final Path path = IOUtils.getPath(params.getIdentifier().getUri());
			if (path != null) {
				module = Project.INSTANCE.getModuleByPath(path);
			}
		}
	}

	public Either<MetricsResponse, MetricsStatsResponse> getData() {
		if (scope.equals(Module.MODULE)) {
			if (targetName != null) {
				return Either.forLeft(new MetricsResponse());
			}
			return Either.forRight(new FullModuleStats());
		} else {
			return Either.forRight(new FullProjectStats());
		}
	}

	private class ProjectData {
		private final String name;
		private final Map<String, Number> statistics;

		ProjectData(String name, String statKey, Number statValue) {
			this.name = name;
			this.statistics = Map.of(statKey, statValue);
		}
	}

	private class StatData {
		private final String name;
		private final Statistics statistics;

		StatData(String name, Statistics statistics) {
			this.name = name;
			this.statistics = statistics;
		}
	}

	public class MetricsStatsResponse {
		protected final Map<String, String> ttcn3Modules = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
		protected final Map<String, String> asn1Modules = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
		protected final List<StatData> testcaseMetrics = new ArrayList<>();
		protected final List<StatData> altstepMetrics = new ArrayList<>();
		protected final List<StatData> functionMetrics = new ArrayList<>();

		public MetricsStatsResponse() {
			collectModules();
			collectStatistics();
		}

		private void collectModules() {
			final List<Module> modules = data.getModules();
			modules.forEach(m -> {
				final String path = m.getLocation().getFile().getPath();
				if (m.getModuletype() == module_type.TTCN3_MODULE) {
					ttcn3Modules.put(m.getName(), path);
				} else {
					asn1Modules.put(m.getName(), path);
				}
			});
		}

		protected StatData getStatData(IMetricEnum metric) {
			Statistics stats = null;
			if (metric instanceof ModuleMetric) {
				stats = data.getStatistics((ModuleMetric)metric);
			}

			if (metric instanceof FunctionMetric) {
				stats = module != null ? data.getStatistics((FunctionMetric)metric, module) : data.getStatistics((FunctionMetric)metric);
			}

			if (metric instanceof TestcaseMetric) {
				stats = module != null ? data.getStatistics((TestcaseMetric)metric, module) : data.getStatistics((TestcaseMetric)metric);
			}

			if (metric instanceof AltstepMetric) {
				stats = module != null ? data.getStatistics((AltstepMetric)metric, module) : data.getStatistics((AltstepMetric)metric);
			}

			return new StatData(metric.getName(), stats);
		}

		protected <E extends Enum<E> & IMetricEnum> void collectStatsOfMetric(Class<E> metric, List<StatData> statContainer) {
			EnumSet.allOf(metric).forEach(metricItem -> statContainer.add(getStatData(metricItem)));
		}

		protected void collectStatistics() {
			collectStatsOfMetric(FunctionMetric.class, functionMetrics);
			collectStatsOfMetric(TestcaseMetric.class, testcaseMetrics);
			collectStatsOfMetric(AltstepMetric.class, altstepMetrics);
		}

	}

	private class FullProjectStats extends MetricsStatsResponse {
		private final List<ProjectData> projectMetrics = new ArrayList<>();
		private final List<StatData> moduleMetrics = new ArrayList<>();

		public FullProjectStats() {
			super();
			collectProjectData();
			collectStatsOfMetric(ModuleMetric.class, moduleMetrics);
		}

		private void collectProjectData() {
			final List<ProjectMetric> metrics = List.of(ProjectMetric.NOF_ASN1_MODULES, ProjectMetric.NOF_TTCN3_MODULES);
			metrics.forEach(metric -> {
				final Number result = data.get(metric);
				final ProjectData projectData = new ProjectData(metric.getName(), "TOTAL", result);
				projectMetrics.add(projectData);
			});
		}
	}

	private class FullModuleStats extends MetricsStatsResponse {
		private final String name;
		private final List<String> functions = new ArrayList<>();
		private final List<String> altsteps = new ArrayList<>();
		private final List<String> testcases = new ArrayList<>();

		public FullModuleStats() {
			super();
			name = module.getName();
			data.getFunctions().get(module).forEach(fn ->
				functions.add(fn.getIdentifier().getDisplayName())
			);
			
			data.getAltsteps().get(module).forEach(as ->
				altsteps.add(as.getIdentifier().getDisplayName())
			);
			
			data.getTestcases().get(module).forEach(ts ->
				testcases.add(ts.getIdentifier().getDisplayName())
			);
		}
	}

	public class MetricsResponse {
		private final Map<String, Number> metrics = new HashMap<>();
		private final String name = targetName;
		private final String type = targetType;

		public MetricsResponse() {
			collectMetrics();
		}

		private void collectMetrics() {
			if (type.equals("function")) {
				data.getFunctions().get(module).forEach(fn -> {
					final String fnName = fn.getIdentifier().getDisplayName();
					if (fnName.equals(name)) {
						EnumSet.allOf(FunctionMetric.class).forEach(metric ->
							metrics.put(metric.getName(), data.get(metric, fn))
						);
					}
				});
				return;
			}

			if (type.equals("altstep")) {
				data.getAltsteps().get(module).forEach(as -> {
					final String fnName = as.getIdentifier().getDisplayName();
					if (fnName.equals(name)) {
						EnumSet.allOf(AltstepMetric.class).forEach(metric ->
							metrics.put(metric.getName(), data.get(metric, as))
						);
					}
				});
				return;
			}

			if (type.equals("testcase")) {
				data.getTestcases().get(module).forEach(ts -> {
					final String fnName = ts.getIdentifier().getDisplayName();
					if (fnName.equals(name)) {
						EnumSet.allOf(TestcaseMetric.class).forEach(metric ->
							metrics.put(metric.getName(), data.get(metric, ts))
						);
					}
				});
				return;
			}
		}

	}
}
