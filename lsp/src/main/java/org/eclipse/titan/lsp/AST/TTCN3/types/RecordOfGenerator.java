/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

/**
 * Utility class for generating the value and template classes for
 * "record of/set of" types.
 *
 * @author Arpad Lovassy
 */
public final class RecordOfGenerator {

	private RecordOfGenerator() {
		// private to disable instantiation
	}
}
