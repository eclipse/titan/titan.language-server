/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.extensionattributeparser;

import java.io.StringReader;
import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.utils.LSPUtils;

/**
 * Extension attribute parser analyzer
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public final class ExtensionAttributeAnalyzer {

	private List<ExtensionAttribute> attributes;

	public List<ExtensionAttribute> getAttributes() {
		return attributes;
	}

	public void parse(final AttributeSpecification specification) {
		final Location location = specification.getLocation();

		final StringReader reader = new StringReader(specification.getSpecification());
		final CharStream charStream = new UnbufferedCharStream(reader);
		final ExtensionAttributeLexer lexer = new ExtensionAttributeLexer(charStream);
		lexer.setTokenFactory(new CommonTokenFactory(true));
		final TitanErrorListener lexerListener = new TitanErrorListener(location.getFile());
		lexer.removeErrorListeners();
		lexer.addErrorListener(lexerListener);

		// 1. Previously it was UnbufferedTokenStream(lexer), but it was changed to BufferedTokenStream, because UnbufferedTokenStream seems to be unusable. It is an ANTLR 4 bug.
		// Read this: https://groups.google.com/forum/#!topic/antlr-discussion/gsAu-6d3pKU
		// pr_PatternChunk[StringBuilder builder, boolean[] uni]:
		//   $builder.append($v.text); <-- exception is thrown here: java.lang.UnsupportedOperationException: interval 85..85 not in token buffer window: 86..341
		// 2. Changed from BufferedTokenStream to CommonTokenStream, otherwise tokens with "-> channel(HIDDEN)" are not filtered out in lexer.
		final CommonTokenStream tokenStream = new CommonTokenStream( lexer );

		final ExtensionAttributeParser parser = new ExtensionAttributeParser( tokenStream );
		parser.setBuildParseTree(false);

		final TitanErrorListener parserListener = new TitanErrorListener(location.getFile());
		parser.removeErrorListeners();
		parser.addErrorListener(parserListener);

		parser.setActualFile(location.getFile());
		parser.setOffsetPosition(location.getStartPosition());

		MarkerHandler.removeMarkers(location.getFile(),
			location.getStartPosition(), location.getEndPosition());

		attributes = null;
		attributes = parser.pr_ExtensionAttributeRoot().list;

		final DiagnosticSeverity severity = LSPUtils.getDiagnosticSeverity(Configuration.INSTANCE.getString(
				Configuration.REPORT_ERRORS_IN_EXTENSION_SYNTAX, GeneralConstants.WARNING));

		for (final SyntacticErrorStorage ses : lexerListener.getErrorsStored()) {
			ses.reportProblem(severity);
		}

		for (final SyntacticErrorStorage ses : parserListener.getErrorsStored()) {
			ses.reportProblem(severity);
		}
	}
}
