/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferenceChainElement;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Reference.Ref_Type;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.IModifiableVisibility;
import org.eclipse.titan.lsp.AST.TTCN3.TTCN3Scope;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_FunctionBase;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the body of a class type.
 *
 * @author Miklos Magyari
 * */
public final class ClassTypeBody extends TTCN3Scope implements IReferenceChainElement, ILocateableNode, IIncrementallyUpdatable {
	public static final String MEMBERNOTVISIBLE = "Member `{0}'' in class type `{1}'' is not visible in this scope";
	public static final String LOCALINHERITANCECOLLISION = "Local Definiton `{0}'' collides with definition inherited from class type `{1}''";
	public static final String INHERITEDLOCATION = "Inherited definition of `{0}'' is here";
	public static final String INHERITANCECOLLISION =
			"Definition `{0}'' inherited from class type `{1}'' collides with definition inherited from `{2}''";
	public static final String INHERITEDDEFINITIONLOCATION = "Definition `{0}'' inherited from class type `{1}'' is here";
	public static final String FORMALPARAMSDIFFER = "Formal parameter list differs from previous definition";
	public static final String OVERRIDDENFORMALPARAM = "Definition is overridden with different formal parameters";
	public static final String PUBLICOVERRIDEPUBLIC = "Public method can only be overriden by a public method";
	public static final String PROTECTEDOVERRIDE = "Protected method can only be overriden by a public or protected method";
	public static final String RETURNTYPEMISMATCH = "Return type differs from the overridden method's return type";
	public static final String OVERRIDDENRETURNTYPE = "Method is overridden with a different return type";
	public static final String VISIBILITYPRIVATEPROTECTED = "Class field visibility must be private or protected";
	public static final String TRAITMETHODSONLY = "Trait classes can only declare methods";
	public static final String TRAITMETHODHASBODY = "Trait method cannot have a function body";
	public static final String TRAITCONSTRUCTOR = "Trait classes cannot have a constructor";
	public static final String TRAITMETHODABSTRACT = "Trait classes can only declare abstract methods";
	public static final String ABSTRACTMETHODHASBODY = "Abstract method cannot have a function body";
	public static final String FIELDNOOVERRIDE = "A field of any visibility cannot be overriden by a subclass";
	public static final String BADSUPERREF = "Reference to `super' in class type `{0}'', which has no base class";
	public static final String DEFNOTVISIBLE = "The {0} definition `{1}'' in class type `{2}'' is not visible in this scope";

	private Class_Type parentClass;

	public ClassTypeBody(Class_Type parentClass) {
		this.parentClass = parentClass;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return parentClass.getLocation();
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		// Do nothing, location is stored by the parent class
	}

	public Class_Type getParentClass() {
		return parentClass;
	}

	public void setMyScope(final Scope scope) {
		setParentScope(parentClass.getMyScope());
//		if (location != null && scope != null) {
//			scope.addSubScope(location, this);
//		}
	} 

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {

		if (reference.getModuleIdentifier() != null) {
			return getParentScope().getAssBySRef(timestamp, reference);
		}

		final Identifier identifier = reference.getId();
		Definition definition = parentClass.getDefinitionMap().getLocalAssignmentByID(timestamp, identifier);
		if (definition != null) {
			return definition;
		}

		return getParentScope().getAssBySRef(timestamp, reference);
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasAssignmentWithId(final CompilationTimeStamp timestamp, final Identifier identifier) {
		Definition definition = parentClass.getDefinitionMap().getLocalAssignmentByID(timestamp, identifier);
		if (definition != null) {
			return true;
		}

		return super.hasAssignmentWithId(timestamp, identifier);
	}

	public Assignment getAssByIdentifier(final CompilationTimeStamp timestamp, final Identifier identifier) {
		Definition definition = parentClass.getDefinitionMap().getLocalAssignmentByID(timestamp, identifier);
		if (definition != null) {
			return definition;
		}
		return null;
	}

	@Override
	public List<Definition> getVisibleDefinitions() {
		return Collections.unmodifiableList(parentClass.getDefinitions());
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final Definition definition : parentClass.getDefinitions()) {
			definition.updateSyntax(reparser, false);
			reparser.updateLocation(definition.getLocation());
			if(!definition.getLocation().equals(definition.getCumulativeDefinitionLocation())) {
				reparser.updateLocation(definition.getCumulativeDefinitionLocation());
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}

		if (parentClass.getDefinitions() != null) {
			for (final Definition def : parentClass.getDefinitions()) {
				if (!def.accept(v)) {
					return false;
				}
			}
		}

		return v.leave(this) != ASTVisitor.V_ABORT;
	}

	@Override
	public Class_Type getScopeClass() {
		return parentClass;
	}

	@Override
	public String chainedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Location getChainLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Assignment getEnclosingAssignment(final Position offset) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void findReferences(ReferenceFinder referenceFinder, List<Hit> foundIdentifiers) {
		// Do nothing, class body cannot be referenced
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		if (parentClass.isBuiltIn()) {
			return null;
		}
		if (reference == null || getParentScope() == null) {
			// TODO: fatal
		}
		parentClass.check(timestamp);
		if (reference.getModuleIdentifier() == null) {
			final Identifier id = reference.getId();
			if (reference.getReferenceType() == Ref_Type.REF_SUPER) {
				if (parentClass.getBaseType() == null) {
					reference.getLocation().reportSemanticError( 
							MessageFormat.format(BADSUPERREF, parentClass.getIdentifier().getDisplayName()));
					return null;
				} else {
					// send the reference to the base type, with the reftype changed to 'this'
					reference.setReferenceType(Ref_Type.REF_THIS);
					final Assignment ass = parentClass.getBaseClass().getClassTypeBody().getAssBySRef(timestamp, reference);
					reference.setReferenceType(Ref_Type.REF_SUPER);
					return ass;
				}
			} else if (id == null && reference.getReferenceType() == Ref_Type.REF_THIS) {
				// reference is just 'this'
				return parentClass.getDefiningAssignment();
				// nothing special is needed for 'this.field' or 'this.method'
		        // (it's already been handled at the lower scopes)
			}

			if (id != null && parentClass.hasLocalAssignemtWithID(timestamp, id)) {
				final Assignment ass = parentClass.getLocalAssignmentByID(timestamp, id);
				if (ass == null) {
					// TODO: fatal
				}

				if (checkVisibility(timestamp, ass, reference.getLocation(), reference.getMyScope())) {
					return ass;
				} else {
					return null;
				}
			}
		}
		return getParentScope().getAssBySRef(timestamp, reference);
	}

	private boolean checkVisibility(CompilationTimeStamp timestamp, Assignment ass, Location usageLoc, Scope usageScope) {
		if (!(ass instanceof IModifiableVisibility)) {
			return false;
		}
		final IModifiableVisibility def = (IModifiableVisibility)ass;
		if (def.getVisibilityModifier() == VisibilityModifier.Public) {
			// it's public, so it doesn't matter where it's accessed from
		      return true;
		}

		final Class_Type refScopeClass = usageScope.getScopeClass();
		final Class_Type assScopeClass = ass.getMyScope().getScopeClass();

		if (refScopeClass != null && def.getVisibilityModifier() == VisibilityModifier.Protected 
				&& refScopeClass.isParentClass(timestamp, assScopeClass)) {
			return true;
		}

		usageLoc.reportSemanticError(
				MessageFormat.format(DEFNOTVISIBLE, ass instanceof Def_FunctionBase ? "method" : "member",
						ass.getIdentifier().getDisplayName(), parentClass.getIdentifier()));
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isClassScope() {
		return true;
	}

	public Assignment getAssFromParents(CompilationTimeStamp timestamp, Reference reference) {
		// TODO Auto-generated method stub
		return null;
	}
}