/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Sequence_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Set_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.Signature_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Sequence_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Set_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.NamedValue;
import org.eclipse.titan.lsp.AST.TTCN3.values.NamedValues;
import org.eclipse.titan.lsp.AST.TTCN3.values.Sequence_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represent a list of named templates.
 *
 * @author Kristof Szabados
 * */
public final class Named_Template_List extends TTCN3Template implements ICollection<NamedTemplate> {
	private static final String ALLARENOTUSED = "All elements of value list notation for type `{0}'' are not used symbols (`-'')";
	private static final String TOOMANYELEMENTS = "Too many elements in value list notation for type `{0}'': {1} was expected instead of {2}";
	private static final String UNBOUND_FIELD_REF = "Reference to unbound field `{0}''";

	private final NamedTemplates namedTemplates;

	/** cache storing the value form of this if already created, or {@code null} */
	private Sequence_Value asValue = null;

	public Named_Template_List(final NamedTemplates namedTemplates) {
		this.namedTemplates = namedTemplates;
		namedTemplates.setFullNameParent(this);
	}

	/**
	 * Function used to convert a template written without naming the fields into a template with all field names provided.
	 *
	 * @param timestamp the timestamp of the actual build cycle
	 * @param other the template to be converted
	 */
	public static Named_Template_List convert(final CompilationTimeStamp timestamp, final Template_List other) {
		final IType lastType = other.getMyGovernor().getTypeRefdLast(timestamp);
		final int nofTemplates = other.size();
		int nofTypeComponents = 0;
		switch (lastType.getTypetype()) {
		case TYPE_TTCN3_SEQUENCE:
			nofTypeComponents = ((TTCN3_Sequence_Type) lastType).getNofComponents();
			break;
		case TYPE_ASN1_SEQUENCE:
			nofTypeComponents = ((ASN1_Sequence_Type) lastType).getNofComponents();
			break;
		case TYPE_SIGNATURE:
			nofTypeComponents = ((Signature_Type) lastType).size();
			break;
		case TYPE_TTCN3_SET:
			nofTypeComponents = ((TTCN3_Set_Type) lastType).getNofComponents();
			break;
		case TYPE_ASN1_SET:
			nofTypeComponents = ((ASN1_Set_Type) lastType).getNofComponents();
			break;
		default:
		}

		if (nofTemplates > nofTypeComponents) {
			other.getLocation().reportSemanticError(MessageFormat.format(TOOMANYELEMENTS, lastType.getTypename(), nofTypeComponents, nofTemplates));
			other.setIsErroneous(true);
		}

		int upperLimit;
		boolean allNotUsed;
		if (nofTemplates <= nofTypeComponents) {
			upperLimit = nofTemplates;
			allNotUsed = true;
		} else {
			upperLimit = nofTypeComponents;
			allNotUsed = false;
		}

		final NamedTemplates namedTemplates = new NamedTemplates();
		for (int i = 0; i < upperLimit; i++) {
			final TTCN3Template template = other.get(i);
			if (!Template_type.TEMPLATE_NOTUSED.equals(template.getTemplatetype())) {
				allNotUsed = false;
				Identifier identifier = null;
				switch (lastType.getTypetype()) {
				case TYPE_TTCN3_SEQUENCE:
					identifier = ((TTCN3_Sequence_Type) lastType).getComponentIdentifierByIndex(i);
					break;
				case TYPE_ASN1_SEQUENCE:
					identifier = ((ASN1_Sequence_Type) lastType).getComponentIdentifierByIndex(i);
					break;
				case TYPE_SIGNATURE:
					identifier = ((Signature_Type) lastType).get(i);
					break;
				case TYPE_TTCN3_SET:
					identifier = ((TTCN3_Set_Type) lastType).getComponentIdentifierByIndex(i);
					break;
				case TYPE_ASN1_SET:
					identifier = ((ASN1_Set_Type) lastType).getComponentIdentifierByIndex(i);
					break;
				default:
					// can not reach here because of a previous check
					break;
				}

				if (identifier != null) {
					final NamedTemplate namedTemplate = new NamedTemplate(identifier.newInstance(), template);
					namedTemplate.setLocation(template.getLocation());
					namedTemplates.add(namedTemplate);
				}
			}
		}

		namedTemplates.setMyScope(other.getMyScope());
		namedTemplates.setFullNameParent(other);

		if (allNotUsed && nofTemplates > 0 && !Type_type.TYPE_SIGNATURE.equals(lastType.getTypetype())) {
			other.getLocation().reportSemanticWarning(MessageFormat.format(ALLARENOTUSED, lastType.getTypename()));
			other.setIsErroneous(true);
		}

		final Named_Template_List target = new Named_Template_List(namedTemplates);
		target.copyGeneralProperties(other);

		return target;
	}

	@Override
	public boolean add(final NamedTemplate template) {
		if (template != null && namedTemplates != null) {
			template.setMyScope(myScope);
			return namedTemplates.add(template);
		}
		return false;
	}

	/**
	 * Checks if there is a named template in the list with a given name.
	 *
	 * @param id {@link Identifier} to search for.
	 * @return {@code true} if the list has a template with the provided name,
	 *         {@code false} otherwise.
	 */
	@Override
	public boolean contains(final Object id) {
		return (id instanceof Identifier) && namedTemplates != null && namedTemplates.contains(id);
	}

	/**
	 * Returns the named template referred by the {@link Identifier}.
	 *
	 * @param id the {@link Identifier} of the named template
	 * @return the named template with the provided name,
	 *         or {@code null} otherwise.
	 */
	private NamedTemplate get(final Identifier id) {
		return (id == null || namedTemplates == null) ? null : namedTemplates.get(id);
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.NAMED_TEMPLATE_LIST;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		builder.append("{ ");
		for (int i = 0, size = namedTemplates.size(); i < size; i++) {
			if (i > 0) {
				builder.append(", ");
			}

			final NamedTemplate namedTemplate = namedTemplates.get(i);
			builder.append(namedTemplate.getName().getDisplayName());
			builder.append(" := ");
			builder.append(namedTemplate.getTemplate().createStringRepresentation());
		}
		builder.append(" }");

		return addToStringRepresentation(builder).toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (namedTemplates != null) {
			namedTemplates.setMyScope(scope);
		}
	}

	/**
	 * Remove all named templates that were not parsed,
	 * but generated during previous semantic checks.
	 */
	public void removeGeneratedValues() {
		if (namedTemplates != null) {
			namedTemplates.removeGeneratedValues();
		}
	}

	@Override
	public int size() {
		return namedTemplates.size();
	}

	@Override
	public NamedTemplate get(final int index) {
		return namedTemplates.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isValue(final CompilationTimeStamp timestamp) {
		if (lengthRestriction != null || isIfpresent || getIsErroneous(timestamp)) {
			return false;
		}

		for (final NamedTemplate nt : namedTemplates) {
			if (!nt.getTemplate().isValue(timestamp)) {
				return false;
			}
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IValue getValue() {
		if (asValue != null) {
			return asValue;
		}

		final NamedValues values = new NamedValues();
		for (NamedTemplate namedTemplate : namedTemplates) {
			final NamedValue namedValue = new NamedValue(namedTemplate.getName(), namedTemplate.getTemplate().getValue());
			namedValue.setLocation(namedTemplate.getLocation());
			values.add(namedValue);
		}
		asValue = new Sequence_Value(values);
		asValue.setLocation(getLocation());
		asValue.setMyScope(getMyScope());
		asValue.setFullNameParent(getNameParent());
		asValue.setMyGovernor(getMyGovernor());

		return asValue;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		for (final NamedTemplate nt : namedTemplates) {
			if (nt.getTemplate().checkExpressionSelfReferenceTemplate(timestamp, lhs)) {
				return true;
			}
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		if (isBuildCancelled()) {
			return;
		}

		for (final NamedTemplate nt : namedTemplates) {
			final ITTCN3Template temp = nt.getTemplate();
			if (temp != null) {
				temp.checkSpecificValue(timestamp, true);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (referenceChain.add(this)) {
			for (final NamedTemplate template : namedTemplates) {
				if (template != null) {
					referenceChain.markState();
					template.getTemplate().checkRecursions(timestamp, referenceChain);
					referenceChain.previousState();
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean chkRestrictionNamedListBaseTemplate(final CompilationTimeStamp timestamp, final String definitionName,
			final boolean omitAllowed, final Set<String> checkedNames, final int neededCheckedCnt, final Location usageLocation) {
		boolean needsRuntimeCheck = false;
		if (checkedNames.size() >= neededCheckedCnt) {
			return needsRuntimeCheck;
		}

		for (final NamedTemplate nt : namedTemplates) {
			final ITTCN3Template tmpl = nt.getTemplate();
			final String name = nt.getName().getName();
			if (!checkedNames.contains(name)) {
				if (tmpl.checkValueomitRestriction(timestamp, definitionName, true, usageLocation)) {
					needsRuntimeCheck = true;
				}
				checkedNames.add(name);
			}
		}
		if (baseTemplate instanceof Named_Template_List) {
			if (((Named_Template_List) baseTemplate).chkRestrictionNamedListBaseTemplate(timestamp, definitionName,
					omitAllowed,checkedNames, neededCheckedCnt, usageLocation)) {
				needsRuntimeCheck = true;
			}
		}
		return needsRuntimeCheck;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, omitAllowed, usageLocation);

		boolean needsRuntimeCheck = false;
		int neededCheckedCnt = 0;
		final HashSet<String> checkedNames = new HashSet<String>();
		if (baseTemplate != null && myGovernor != null) {
			switch (myGovernor.getTypetype()) {
			case TYPE_TTCN3_SEQUENCE:
				neededCheckedCnt = ((TTCN3_Sequence_Type) myGovernor).getNofComponents();
				break;
			case TYPE_ASN1_SEQUENCE:
				neededCheckedCnt = ((ASN1_Sequence_Type) myGovernor).getNofComponents();
				break;
			case TYPE_TTCN3_SET:
				neededCheckedCnt = ((TTCN3_Set_Type) myGovernor).getNofComponents();
				break;
			case TYPE_ASN1_SET:
				neededCheckedCnt = ((ASN1_Set_Type) myGovernor).getNofComponents();
				break;
			case TYPE_SIGNATURE:
				neededCheckedCnt = ((Signature_Type) myGovernor).size();
				break;
			default:
				// can not reach here because of a previous check
				break;
			}
		}

		for (final NamedTemplate temp : namedTemplates) {
			if (temp.getTemplate().checkValueomitRestriction(timestamp, definitionName, true, usageLocation)) {
				needsRuntimeCheck = true;
			}
			if (neededCheckedCnt > 0) {
				checkedNames.add(temp.getName().getName());
			}
		}
		if (neededCheckedCnt > 0) {
			if (baseTemplate instanceof Named_Template_List
					&& ((Named_Template_List) baseTemplate).chkRestrictionNamedListBaseTemplate(timestamp,
							definitionName, omitAllowed, checkedNames, neededCheckedCnt, usageLocation)) {
				needsRuntimeCheck = true;
			}
		}
		return needsRuntimeCheck;
	}

	@Override
	/** {@inheritDoc} */
	public ITTCN3Template getReferencedSetSequenceFieldTemplate(final CompilationTimeStamp timestamp, final Identifier fieldIdentifier,
			final Reference reference, final IReferenceChain referenceChain, final boolean silent) {
		if (contains(fieldIdentifier)) {
			return get(fieldIdentifier).getTemplate();
		} else if (baseTemplate != null) {
			// take the field from the base template
			final TTCN3Template temp = baseTemplate.getTemplateReferencedLast(timestamp, referenceChain);
			if (temp == null) {
				return null;
			}

			return temp.getReferencedFieldTemplate(timestamp, fieldIdentifier, reference, referenceChain, silent);
		} else {
			if (!reference.getUsedInIsbound() && !silent) {
				reference.getLocation().reportSemanticError(
						MessageFormat.format(UNBOUND_FIELD_REF, fieldIdentifier.getDisplayName()));
			}

			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);
		namedTemplates.updateSyntax(reparser, false);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (asValue != null) {
			asValue.findReferences(referenceFinder, foundIdentifiers);
			return;
		}
		if (namedTemplates == null) {
			return;
		}
		if (referenceFinder.assignment.getAssignmentType() == Assignment_type.A_TYPE && referenceFinder.fieldId != null && myGovernor != null) {
			// check if this is the type and field we are searching for
			final IType govLast = myGovernor.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
			if (referenceFinder.type == govLast) {
				final NamedTemplate nt = namedTemplates.get(referenceFinder.fieldId);
				if (nt != null) {
					foundIdentifiers.add(new Hit(nt.getName()));
				}
			}
		}
		namedTemplates.findReferences(referenceFinder, foundIdentifiers);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (asValue != null && !asValue.accept(v)) {
			return false;
		} else if (namedTemplates != null && !namedTemplates.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		return lengthRestriction != null || isIfpresent || !namedTemplates.isEmpty();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyDefinition(Definition definition) {
		for (final NamedTemplate nt : namedTemplates) {
			nt.getTemplate().setMyDefinition(definition);
		}
	}

	@Override
	public Iterator<NamedTemplate> iterator() {
		return namedTemplates.iterator();
	}
}
