/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.risk;

import org.eclipse.titan.lsp.titanium.metrics.common.IMetricEnum;

public class LinearRisk extends BaseRisk {
	public LinearRisk(final IMetricEnum metric) {
		super(metric);
	}

	@Override
	public double getRiskValue(final Number value) {
		double risk = 0;
		double limit1;
		double limit2;
		final double val = value == null ? 0.0 : value.doubleValue();
		switch (method) {
		case NEVER:
			risk = 1 - (1 / (val + 1));
			break;
		case NO_HIGH:
			limit1 = limits[0].doubleValue();
			if (val >= limit1) {
				risk = 3 - (1 / ((val - limit1) + 1));
			} else {
				risk = val / limit1;
			}
			break;
		case NO_LOW:
			limit1 = limits[0].doubleValue();
			if (val >= limit1) {
				risk = 2 - (1 / ((val - limit1) + 1));
			} else {
				risk = val / limit1;
			}
			break;
		case NO_LOW_HIGH:
			limit1 = limits[0].doubleValue();
			limit2 = limits[1].doubleValue();
			if (val < limit1) {
				risk = val / limit1;
			} else if (val >= limit2) {
				risk = 3 - (1 / (val - limit2 + 1));
			} else {
				risk = 1 + (val - limit1) / (limit2 - limit1);
			}
			break;
		}
		return risk;
	}
}
