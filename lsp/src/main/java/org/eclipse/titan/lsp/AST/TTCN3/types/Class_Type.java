/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.Assignment.Assignment_type;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.ITypeWithComponents;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.InvalidASTNode;
import org.eclipse.titan.lsp.AST.InvalidASTNodes;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.Reference.Ref_Type;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo.Chain;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction.Restriction_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Types;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_AbsFunction;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Const;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Constructor;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_FunctionBase;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Template;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Timer;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Var_Template;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter.parameterEvaluationType;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.IParameterisedAssignment;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Assignment_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.lsp.AST.TTCN3.templates.SpecificValue_Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.AST.TTCN3.values.Referenced_Value;
import org.eclipse.titan.lsp.codeAction.CodeActionsForClassType.CodeActionType_Class;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_reparse_ClassMemberContext;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_reparse_ClassMemberListContext;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * Represents the TTCN3 class type (TTCN-3 extension).
 *
 * @author Miklos Magyari
 * @author Arpad Lovassy
 * */
public final class Class_Type extends Type implements ITypeWithComponents {
	private static final String ONLYONEMODIFIER = "A class cannot have more than one of the @final, @abstract and @trait modifiers"; 
	private static final String EXTERNALABSTRACT = "External classes cannot be abstract";
	private static final String CLASSEXPECTED = "Class type expected instead of `{0}''";
	private static final String TRAITEXTENDTRAIT = "A trait class cannot extend a non-trait class";
	private static final String CANNOTEXTENDMORETHANONE = "A class cannot extend more than one non-trait class";
	private static final String PREVEXTENDED = "Previous extended non-trait class is here";
	private static final String BASECANNOTBEFINAL = "The superclass cannot be final";
	private static final String EXTERNALEXTENDINTERNAL = "An external class cannot extend an internal class";
	private static final String DUPLICATECLASSTYPE = "Duplicate class type in list of classes being extended";
	private static final String RUNSONINCOMPATIBLE = "The `runs on'' component type of the subclass, `{0}'', is not compatible with the `runs on'' component type of the superclass, `{1}''";
	private static final String MTCINCOMPATIBLE = "The `mtc'' component type of the subclass, `{0}'', is not compatible with the `mtc'' component type of the superclass, `{1}''";
	private static final String SYSTEMINCOMPATIBLE = "The `system'' component type of the subclass, `{0}'', is not compatible with the `system'' component type of the superclass, `{1}''";
	private static final String SUBCLASSHASNORUNSON = "Supertrait `{0}'' has a `runs on'' component type, but the subclass and its superclass does not";
	private static final String SUBCLASSHASNOMTC = "Supertrait `{0}'' has an `mtc'' component type, but the subclass and its superclass does not";
	private static final String SUBCLASSHASNOSYSTEM = "Supertrait `{0}'' has a `system'' component type, but the subclass and its superclass does not";
	private static final String RUNSONINCOMPATIBLETRAIT = "The `runs on'' component type of the subclass " +
            "(`{0}'') is not compatible with the `runs on'' component type of " +
            "supertrait `{1}'' (`{2}'')";
	private static final String MTCINCOMPATIBLETRAIT = "The `mtc'' component type of the subclass " +
            "(`{0}'') is not compatible with the `mtc'' component type of " +
            "supertrait `{1}'' (`{2}'')";
	private static final String SYSTEMINCOMPATIBLETRAIT = "The `system'' component type of the subclass " +
            "(`{0}'') is not compatible with the `system'' component type of " +
            "supertrait `{1}'' (`{2}'')";
	private static final String MEMBERCANNOTBEPUBLIC = "Class members cannot be public";
	private static final String SHADOWSRUNSON = "`{0}'' shadows a definition in runs-on component type `{1}''";
	private static final String SHADOWSMTC = "`{0}'' shadows a definition in mtc component type `{1}''";
	private static final String SHADOWSSYSTEM = "`{0}'' shadows a definition in system component type `{1}''";
	private static final String DIFFERSFROMOBJECTMETHOD = "The prototype of method `{0}'' is not identical " +
		    "to that of the method inherited from the `object'' class";
	private static final String SHADOWSOBJECTMETHOD = "`{0}'' shadows a method inherited from the `object'' class";
	private static final String PARAMNAMEDIFFERS = "One or more parameter names differ from previous definition";
	private static final String PROTOTYPENOTIDENTICAL = "The prototype of method `{0}'' is not identical " +
            "to that of inherited method `{1}''";
	private static final String PROTOTYPESDIFFER = "The prototypes of methods `{0}'' inherited from " +
            "classes `{1}'' and `{2}'' are not identical";
	private static final String CANNOTOVERRIDEFINAL = "Cannot override final method `{0}''";
	private static final String PUBLICCANBEOVERRIDDEN = "Public methods can be only overridden by public methods `{0}''";
	private static final String PROTECTEDCANBEOVERRIDDEN = "Protected methods can be only overridden by " +
            "public or protected methods `{0}''";
	private static final String SHADOWSINHERITEDMEMBER = "`{0}'' shadows inherited member `{1}''";
	private static final String SHADOWSINHERITED2 = "`{0}'' shadows inherited {1} `{2}''";
	private static final String TRAITNOCONSTRUCTOR = "Trait class type `{0}'' cannot have a constructor";
	private static final String TRAITCANNOTHAVEFINALLY = "Trait class type `{0}'' cannot have a destructor";
	private static final String EXTERNALCANNOTCONTAIN = "An external class cannot contain a {0}";
	private static final String EXTERNALCANNOTHAVEFINALLY = "An external class cannot have a destructor";
	private static final String MISSINGABSIMPLEMENTATION = "Missing implementation of abstract method `{0}''";

	private static final String UNKNOWNFIELD = "Unknown field reference";
	private static final String PRIVATEINACCESSIBLE = "Private member is inaccessible due to its protection level";

	/** 
	 * The superclass of this class
	 * (the only non-trait class that this class extends) 
	 **/
	//TODO: remove
	//private Class_Type superClass = null;

	private boolean isBuiltIn;  
	private final boolean isAbstract;
	private final boolean isFinal;
	private final boolean isTrait;
	private final boolean isExternal;
	private final Location modifierLocation;
	private final StatementBlock finallyBlock;
	private final Reference runsOnRef;
	private final Reference mtcRef;
	private final Reference systemRef;

	private Component_Type runsOnType;
	private Component_Type mtcType;
	private Component_Type systemType;

	private ClassTypeBody body;

	private Identifier classId;
	private Types baseTraits;
	private Type baseType;
	private Definitions members;
	private Class_Type baseClass;
	private Def_Constructor constructor;
	private Map<FormalParameter,String> defaultParameterList = new HashMap<>();
	private Map<String,Def_AbsFunction> abstractFunctions = new HashMap<>();
	private boolean defaultConstructor;
	private InvalidASTNodes invalidNodes;

	public enum ClassRelation {
		Identical, Related, Unrelated 
	}

	public Class_Type(Identifier identifier, boolean isAbstract, boolean isFinal, boolean isTrait, boolean isExternal, 
		Location modifierLocation, final Reference runsOnRef, final Reference mtcRef, final Reference systemRef,
		Definitions members, Types baseTraits, StatementBlock finallyBlock) {

		this.classId = identifier;
		this.isAbstract = isAbstract;
		this.isFinal = isFinal;
		this.isTrait = isTrait;
		this.isExternal = isExternal;
		this.modifierLocation = modifierLocation;
		this.finallyBlock = finallyBlock;
		this.runsOnRef = runsOnRef;
		this.mtcRef = mtcRef;
		this.systemRef = systemRef;
		this.members = members != null ? members : new Definitions();
		this.baseTraits = baseTraits;

		if (runsOnRef != null) {
			runsOnRef.setFullNameParent(this);
		}
		if (mtcRef != null) {
			mtcRef.setFullNameParent(this);
		}
		if (systemRef != null) {
			systemRef.setFullNameParent(this);
		}
		if (baseTraits != null) {
			baseTraits.setFullNameParent(this);
		}
		if (finallyBlock != null) {
			finallyBlock.setFullNameParent(this);
			finallyBlock.setOwnerIsFinally();
		}

		body = new ClassTypeBody(this);
		body.setFullNameParent(this);
	}
	
	/** 
	 * constructor used for classes defined using the 'object' keyword
	 */
	public Class_Type() {
		// TODO: name?
		this(new Identifier(Identifier_type.ID_NAME, "object"), true, false, false, false, null, null, null, null, null, null, null);
	}

	@Override
	public Type_type getTypetype() {
		return Type_type.TYPE_CLASS;
	}

	 @Override
    /** {@inheritDoc} */
    public Type_type getTypetypeTtcn3() {
    	if (isErroneous) {
    		return Type_type.TYPE_UNDEFINED;
    	}
    	return getTypetype();
    }

	 @Override
	 /** {@inheritDoc} */
	 public void setMyScope(final Scope scope) {
		 super.setMyScope(scope);
		 if (isBuiltIn) {
			 return;
		 }

		 body.setMyScope(scope);
		 if (baseType != null) {
			 baseType.setMyScope(scope);
		 }
		 if (baseTraits != null) {
			 baseTraits.setMyScope(scope);
		 }
		 if (runsOnRef != null) {
			 runsOnRef.setMyScope(scope);
		 }
		 if (mtcRef != null) {
			 mtcRef.setMyScope(scope);
		 }
		 if (systemRef != null) {
			 systemRef.setMyScope(scope);
		 }
		 
		 if (members != null) {
			 members.setParentScope(body);
		 }
		 if (finallyBlock != null) {
			 finallyBlock.setParentScope(scope);
		 }
		 if (invalidNodes != null) {
			 invalidNodes.setMyScope(scope);
		 }
	 }

	@Override
	/** {@inheritDoc} */
	public Identifier getIdentifier() {
		return classId;
	}

	@Override
	/** {@inheritDoc} */
	public IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final IReferenceChain refChain, final boolean interruptIfOptional) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return this;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);		

		Assignment assignment = getLocalAssignmentByID(timestamp, subreference.getId());
		if (assignment == null) {
			/* methods defined by 'object' */
			if (subreference.getReferenceType() == Subreference_type.parameterisedSubReference) {
				if (subreference.getId().getName().equals(GeneralConstants.TOSTRINGFUNCITON)) {
					Identifier id = new Identifier(Identifier_type.ID_NAME, GeneralConstants.TOSTRINGFUNCITON);
					Def_Function function = new Def_Function(id, new FormalParameterList(new ArrayList<FormalParameter>()), new UniversalCharstring_Type());
					function.setMyScope(reference.getMyScope());
					function.setLocation(NULL_Location.INSTANCE);
					return function.getType(timestamp);
				}
				if (subreference.getId().getName().equals(GeneralConstants.EQUALSFUNCTION)) {
					Identifier id = new Identifier(Identifier_type.ID_NAME, GeneralConstants.EQUALSFUNCTION);
					Identifier objid = new Identifier(Identifier_type.ID_NAME, "obj");
					List<FormalParameter> list = new ArrayList<FormalParameter>();
					list.add(new FormalParameter(null, Assignment_type.A_PAR_VAL_IN,
							new Class_Type(), objid, null, null));
					FormalParameterList paramList = new FormalParameterList(list);
					Def_Function function = new Def_Function(id, paramList, new Boolean_Type());
					function.setMyScope(reference.getMyScope());
					function.setLocation(NULL_Location.INSTANCE);
					return function.getType(timestamp);
				}
			}
			subreference.getLocation().reportSemanticError(UNKNOWNFIELD);
			return null;
		}
		final Definition def = (Definition)assignment;
		if (def.getVisibilityModifier() == VisibilityModifier.Private) {
			subreference.getLocation().reportSemanticError(PRIVATEINACCESSIBLE);
			return null;
		}

		if (assignment instanceof Def_Function) {
			final FormalParameterList fpl = ((Def_Function)assignment).getFormalParameterList();
			final ISubReference sr = reference.getSubreferences().get(actualSubReference);
			if (sr instanceof ParameterisedSubReference) {
				((ParameterisedSubReference)sr).checkParameters(timestamp, fpl);
			}
		}

		if (subreferences.size() == actualSubReference + 1) {
			return assignment.getType(timestamp);
		}

 		Type type = (Type)assignment.getType(timestamp);
		return type.getFieldType(timestamp, reference, actualSubReference + 1, expectedIndex, interruptIfOptional);
	}

	public Reference getRunsOnRef() {
		return runsOnRef;
	}

	public Reference getMtcRef() {
		return mtcRef;
	}

	public Reference getSystemRef() {
		return systemRef;
	}

	public int getNofAbstractFunctions() {
		return abstractFunctions.size();
	}

	public synchronized void addAbstractFunction(String key, Def_AbsFunction value) {
		abstractFunctions.put(key, value);
	}

	public Map<String, Def_AbsFunction> getAbstractFunctions() {
		return abstractFunctions;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		if (isBuiltIn)  {
			return;
		}
		if ((isFinal && isAbstract) || (isFinal && isTrait) || (isAbstract && isTrait)) {
			modifierLocation.reportSemanticError(ONLYONEMODIFIER);
		}
		if (isExternal && isAbstract) {
			modifierLocation.reportSemanticError(EXTERNALABSTRACT);
		}
		if (baseTraits != null) {
			for (int i = 0; i < baseTraits.size(); i++) {
				Type t  = baseTraits.get(i);
				if (t == null) {
					continue; // super class
				}
				t.check(timestamp);
				if (!(t.getTypeRefdLast(timestamp) instanceof Class_Type)) {
					if (! t.getIsErroneous(timestamp)) {
						t.getLocation().reportSemanticError(
							MessageFormat.format(CLASSEXPECTED, t.getTypename()));
					}
					baseTraits.extract(i);
				} else {
					final Class_Type tClass = t.getTypeRefdLast(timestamp).getClassType();
					if (! tClass.isTrait) {
						if (isTrait) {
							t.getLocation().reportSemanticError(TRAITEXTENDTRAIT);
						} else if (baseClass != null){
							t.getLocation().reportSemanticError(CANNOTEXTENDMORETHANONE);
							// baseClass.getLocation().reportSemanticError(PREVEXTENDED);
						} else {
							baseTraits.extract(i);
							baseType = t;
							baseClass = tClass;
							if (baseClass.isFinal) {
								baseClass.getModifierLocation().reportSemanticError(BASECANNOTBEFINAL);
							}
							if (isExternal && ! baseClass.isExternal) {
								baseClass.getLocation().reportSemanticError(EXTERNALEXTENDINTERNAL);
							}
						}
					}
					for (final Type t2 : baseTraits) {
						if (t2 != null && t2.getTypename().equals(t.getTypename())) {
							t.getLocation().reportSemanticError(DUPLICATECLASSTYPE);
						}
					}
				}
			}
		}

		if (baseClass != null && baseClass.isBuiltIn) {
			// if the base class is 'object', then just delete it and set it to null,
		    // so it functions the same way as not specifying a base class
			baseClass = null;
			baseType = null;
		}

		if (runsOnRef != null) {
 			runsOnType = runsOnRef.chkComponentypeReference(timestamp);
			if (baseClass != null) {
			final Component_Type baseRunsOnType = baseClass.getRunsOnType(timestamp);
				if (baseRunsOnType != null && ! baseRunsOnType.isCompatible(timestamp, runsOnType, null , null, null)) {
					runsOnRef.getLocation().reportSemanticError(
						MessageFormat.format(RUNSONINCOMPATIBLE, runsOnType.getTypename(), baseRunsOnType.getTypename()));
				}
			}
		} else if (baseClass != null) {
			runsOnType = baseClass.getRunsOnType(timestamp);
		}

		if (mtcRef != null) {
			mtcType = mtcRef.chkComponentypeReference(timestamp);
			if (baseClass != null) {
				final Component_Type baseMtcType = baseClass.getMtcType(timestamp);
				if (baseMtcType != null && ! baseMtcType.isCompatible(timestamp, mtcType, null , null, null)) {
					mtcRef.getLocation().reportSemanticError(
						MessageFormat.format(MTCINCOMPATIBLE, mtcType.getTypename(), baseMtcType.getTypename()));
				}
			}
		} else if (baseClass != null) {
			mtcType = baseClass.getMtcType(timestamp);
		}

		if (systemRef != null) {
			systemType = systemRef.chkComponentypeReference(timestamp);
 			if (baseClass != null) {
				final Component_Type baseSystemType = baseClass.getSystemType(timestamp);
				if (baseSystemType != null && ! baseSystemType.isCompatible(timestamp, systemType, null , null, null)) {
					systemRef.getLocation().reportSemanticError(
						MessageFormat.format(SYSTEMINCOMPATIBLE, systemType.getTypename(), baseSystemType.getTypename()));
				}
			}
		} else if (baseClass != null) {
			systemType = baseClass.getSystemType(timestamp);
		}

		if (baseTraits != null) {
			for (final Type baseTrait : baseTraits) {
				if (baseTrait != null) {
					final Class_Type baseTraitClass = (Class_Type)baseTrait.getTypeRefdLast(timestamp);
					final Component_Type baseRunsOnType = baseTraitClass.getRunsOnType(timestamp);
					if (baseRunsOnType != null) {
						if (runsOnType == null) {
							getLocation().reportSemanticError(
								MessageFormat.format(SUBCLASSHASNORUNSON, baseTraitClass.getFullName()));
						} else if (!baseRunsOnType.isCompatible(timestamp, runsOnType, null, null, null) ) {
							runsOnRef.getLocation().reportSemanticError(
								MessageFormat.format(RUNSONINCOMPATIBLETRAIT, runsOnType.getTypename(),
								baseTraitClass.getIdentifier().getDisplayName(),
								baseRunsOnType.getTypename()));
						}
					}
					final Component_Type baseMtcType = baseTraitClass.getMtcType(timestamp);
					if (baseMtcType != null) {
						if (mtcType == null) {
							getLocation().reportSemanticError(
								MessageFormat.format(SUBCLASSHASNOMTC, baseTraitClass.getFullName()));
						} else if (!baseMtcType.isCompatible(timestamp, mtcType, null, null, null) ) {
							mtcRef.getLocation().reportSemanticError(
								MessageFormat.format(MTCINCOMPATIBLETRAIT, mtcType.getTypename(),
								baseTraitClass.getIdentifier().getDisplayName(),
								baseMtcType.getTypename()));
						}
					}
					final Component_Type baseSystemType = baseTraitClass.getSystemType(timestamp);
					if (baseSystemType != null) {
						if (systemType == null) {
							getLocation().reportSemanticError(
								MessageFormat.format(SUBCLASSHASNOSYSTEM, baseTraitClass.getFullName()));
						} else if (!baseSystemType.isCompatible(timestamp, systemType, null, null, null) ) {
							systemRef.getLocation().reportSemanticError(
								MessageFormat.format(SYSTEMINCOMPATIBLETRAIT, systemType.getTypename(),
								baseTraitClass.getIdentifier().getDisplayName(),
								baseSystemType.getTypename()));
						}
					}
				}
			}
		}

		for (final Assignment def : members) {
			if (def.getAssignmentType() == Assignment_type.A_CONSTRUCTOR) {
				constructor = (Def_Constructor)def;
			}
		}

		// uniqueness is checked by check()
		members.check(timestamp);

		for (int i = 0; i < members.size(); i++) {
			final Definition def = members.get(i);
			if (def == null) {
				continue;
			}
			switch(def.getAssignmentType()) {
			case A_CONST:
			case A_VAR:
			case A_TEMPLATE:
			case A_VAR_TEMPLATE:
				if (def.isProperty()) {
					break;
				}
				if (def.getVisibilityModifier() == VisibilityModifier.Public) {
					final List<CodeActionType_Class> actionTypes = new ArrayList<>(Arrays.asList(
								CodeActionType_Class.CLASSMEMBERCHANGEPUBLICTOPRIVATE,
								CodeActionType_Class.CLASSMEMBERREMOVEPUBLIC
							));
					def.getVisibilityModifierLocation().reportSemanticError(MEMBERCANNOTBEPUBLIC, actionTypes, CodeActionProviderType.CLASS);
				}
				break;
			default:
				break;
			}
		}

		if (runsOnType != null || mtcType != null || systemType != null) {
			for (final Assignment localDef : members) {
				final Identifier localId = localDef.getIdentifier();
				if (runsOnType != null && runsOnType.getComponentBody().hasLocalAssignmentWithId(localId)) {
					localDef.getLocation().reportSemanticError(
						MessageFormat.format(SHADOWSRUNSON, localDef.getDescription(), runsOnType.getTypename()));
				}
				if (mtcType != null && mtcType.getComponentBody().hasLocalAssignmentWithId(localId)) {
					localDef.getLocation().reportSemanticError(
						MessageFormat.format(SHADOWSMTC, localDef.getDescription(), mtcType.getTypename()));
				}
				if (systemType != null && systemType.getComponentBody().hasLocalAssignmentWithId(localId)) {
					localDef.getLocation().reportSemanticError(
						MessageFormat.format(SHADOWSSYSTEM, localDef.getDescription(), systemType.getTypename()));
				}
			}
		}

		boolean nameClash = false;
		if (baseClass != null) {
			nameClash = compareMembers(this, baseClass, null, timestamp);
		}
		if (baseTraits != null) {
			for (int i = 0; i < baseTraits.size(); i++) {
				final Type baseTrait = baseTraits.get(i);
				if (baseTrait != null) {
					Class_Type baseTraitClass = baseTrait.getTypeRefdLast(timestamp).getClassType();
					nameClash |= compareMembers(this, baseTraitClass, null, timestamp);
					if (baseClass != null) {
						nameClash |= compareMembers(baseClass, baseTraitClass, getLocation() , timestamp);
					}
					for (int j = 0; j < i; j++) {
						final Type baseTrait2 = baseTraits.get(j);
						if (baseTrait2 != null) {
							final Class_Type baseTraitClass2 = baseTrait2.getTypeRefdLast(timestamp).getClassType();
							nameClash |= compareMembers(baseTraitClass, baseTraitClass2, getLocation(), timestamp);
						}
					}
				}
			}
		}

		for (int i = 0; i < members.size(); i++) {
			final Definition def = members.get(i);
			if (! (def instanceof IParameterisedAssignment)) {
				continue;
			}
			final Identifier id = def.getIdentifier();
			final FormalParameterList fpl = getObjectMethodFormalParameterList(id.getName());
			if (fpl != null) {
				IsIdenticalResult identical = IsIdenticalResult.RES_DIFFERS;
				final FormalParameterList fpl2 = ((IParameterisedAssignment)def).getFormalParameterList();
				if (fpl2 != null) {
					identical = fpl.isIdentical(timestamp, fpl2);
				} else if (fpl.size() == 0) {
					identical = IsIdenticalResult.RES_IDENTICAL;
				}
				switch(def.getAssignmentType()) {
				case A_FUNCTION_RVAL:
				case A_EXT_FUNCTION_RVAL:
					if (def.getVisibilityModifier() == VisibilityModifier.Public && identical != IsIdenticalResult.RES_DIFFERS) {
						if (! (def instanceof Def_AbsFunction)) {
							if (def.getType(timestamp).isIdentical(timestamp, getObjectMethodReturnType(id.getName()))) {
								if (identical == IsIdenticalResult.RES_NAME_DIFFERS) {
									fpl2.getLocation().reportSemanticWarning(PARAMNAMEDIFFERS);
								}
								break; // in order
							}
						}
					}
					// fallthrough
				case A_FUNCTION:
				case A_FUNCTION_RTEMP:
				case A_EXT_FUNCTION:
				case A_EXT_FUNCTION_RTEMP:
					// currently all 'object' methods return a value, so these are erroneous by default
					((Def_FunctionBase)def).getSignatureLocation().reportSemanticError(
						MessageFormat.format(DIFFERSFROMOBJECTMETHOD, def.getDescription()));
					break;
				default:
					((Def_FunctionBase)def).getSignatureLocation().reportSemanticError(
						MessageFormat.format(SHADOWSOBJECTMETHOD, def.getDescription()));
					nameClash = true;
					break;
				}
			}
		}

		if (constructor != null && isTrait) {
			constructor.getLocation().reportSemanticError(
				MessageFormat.format(TRAITNOCONSTRUCTOR, getTypename()));
		}

		if (constructor == null && !nameClash && !isTrait) {
			// creating default constructor
			Reference baseCall = null;
			FormalParameterList fpl = null;
			if (! isExternal && baseClass != null) {
				Def_Constructor baseConstructor = baseClass.getConstructor(timestamp);
				if (baseConstructor != null) {
					final FormalParameterList baseFpl = baseConstructor.getFormalParameterList();
					fpl = new FormalParameterList(baseFpl.stream().collect(Collectors.toList()));
					ParsedActualParameters parsedApList = new ParsedActualParameters();
					for (int i = 0; i < baseFpl.size(); i++) {
						Reference ref = new Reference(null, baseFpl.get(i).getIdentifier().newInstance());
						Referenced_Value val = new Referenced_Value(ref);
						SpecificValue_Template temp = new SpecificValue_Template(val);
						TemplateInstance instance = new TemplateInstance(null, null, temp);
						parsedApList.addUnnamedParameter(instance);
						
						// since the base constructor's formal parameters have already been checked
			            // (and their clones are also considered checked),
			            // the ones with default values need to be registered manually
						FormalParameter fp = fpl.get(i);
						if (fp.hasDefaultValue()) {
							addDefaultParameter(fp);
						}
					}
					final ParameterisedSubReference paramSubRef = new ParameterisedSubReference(classId, parsedApList);
					baseCall = new Reference(baseClass.getMyScope().getParentScope().getModuleScope().getIdentifier().newInstance(),
						List.of(paramSubRef));
				}
			}
			if (fpl == null) {
				fpl = new FormalParameterList(new ArrayList<FormalParameter>());
			}
			StatementBlock block = null;
			if (! isExternal) {
				block = new StatementBlock();
				for (final Assignment member : members) {
					boolean isTemplate = false;
					TemplateInstance defVal = null;
					switch(member.getAssignmentType()) {
					case A_CONST:
						if (((Def_Const)member).getValue() != null) {
							continue; // the constant has already been initialized at its definition
						}
						break;
					case A_TEMPLATE:
						if (((Def_Template)member).getTemplate(timestamp) != null) {
							continue; // the template has already been initialized at its definition
						}
						isTemplate = true;
						break;
					case A_VAR:
						if (((Def_Var)member).getInitialValue() != null) {
							// set the variable's initial value as the constructor parameter's default value
							final Def_Var varMember = (Def_Var)member;
							defVal = new TemplateInstance(null, null, new SpecificValue_Template(varMember.stealValue()));
						}
						break;
					case A_VAR_TEMPLATE:
						isTemplate = true;
						if (((Def_Var_Template)member).getInitialValue() != null) {
							// set the template variable's initial value as the constructor parameter's default value
							final Def_Var_Template varTemplateMember = (Def_Var_Template)member;
							defVal = new TemplateInstance(null, null, varTemplateMember.stealValue());
						}
						break;
					default:
						continue;
					}
					// add a formal parameter for this member if we've gotten this far
					final Identifier id = member.getIdentifier().newInstance();
					FormalParameter fp = new FormalParameter(
						Restriction_type.TR_NONE, isTemplate ? Assignment_type.A_PAR_TEMP_IN : Assignment_type.A_PAR_VAL_IN,
						(Type)member.getType(timestamp), id, defVal, parameterEvaluationType.NORMAL_EVAL);
					fpl.add(fp);
					// add a statement, that assigns the parameter's value to the member
					final Identifier id1 = id.newInstance();
					final Reference refLeft = new Reference(new Identifier(Identifier_type.ID_TTCN, id1.getName()), Ref_Type.REF_THIS);
					final FieldSubReference subref = new FieldSubReference(id1);
					refLeft.addSubReference(subref);
					final Reference refRight = new Reference(new Identifier(Identifier_type.ID_TTCN, id.newInstance().getName()));
					final Referenced_Value refdVal = new Referenced_Value(refRight);
					final SpecificValue_Template valTemplate = new SpecificValue_Template(refdVal);
					final Assignment_Statement assignment = new Assignment_Statement(refLeft, valTemplate);
					block.addStatement(assignment);
				}
			}
			final Identifier id = new Identifier(Identifier_type.ID_TTCN, "create");
			id.setLocation(classId.getLocation());
			constructor = new Def_Constructor(id, fpl, baseCall, block);
			constructor.setLocation(classId.getLocation());
			constructor.setSignatureLocation(classId.getLocation());
			constructor.setMyScope(getClassTypeBody());
			// TODO setFulName
			defaultConstructor = true;
			constructor.check(timestamp);
		}

		if (constructor != null && !defaultConstructor && !nameClash && !isTrait) {
			// make sure constants and templates are initialized
			for (final Assignment member : members) {
				boolean needsInitCheck = false;
				boolean isTemplate = false;
				switch(member.getAssignmentType()) {
				case A_CONST:
					needsInitCheck = ((Def_Const)member).getValue() == null;
					break;
				case A_TEMPLATE:
					needsInitCheck = ((Def_Template)member).getTemplate(timestamp) == null;
					isTemplate = true;
					break;
				default:
					break;	
				}
				if (needsInitCheck) {
					constructor.addUninitializedMember(member.getIdentifier(), isTemplate);
				}
			}
		}

		if (finallyBlock != null) {
			finallyBlock.check(timestamp);
			if (isTrait) {
				finallyBlock.getLocation().reportSemanticError(
					MessageFormat.format(TRAITCANNOTHAVEFINALLY, getTypename()));
			}
		}

		if (isExternal) {
			for (final Assignment member : members) {
				switch(member.getAssignmentType()) {
				case A_EXT_FUNCTION:
				case A_EXT_FUNCTION_RVAL:
				case A_EXT_FUNCTION_RTEMP:
				case A_CONSTRUCTOR:
					break;
				default:
					member.getLocation().reportSemanticError( 
						MessageFormat.format(EXTERNALCANNOTCONTAIN, member.getAssignmentName()));
					break;
				}
			}
			if (finallyBlock != null) {
				finallyBlock.getLocation().reportSemanticError(
					MessageFormat.format(EXTERNALCANNOTHAVEFINALLY, getTypename()));
			}
		}

		if (isAbstract || isTrait) {
			// create a map of all abstract functions (including inherited ones)
			if (baseClass != null && baseClass.isAbstract) {
				for (Map.Entry<String, Def_AbsFunction> absFunc : baseClass.getAbstractFunctions().entrySet()) {
					addAbstractFunction(absFunc.getKey(), absFunc.getValue());
				}
			}
			if (baseTraits != null) {
				for (final Type baseTrait : baseTraits) {
					if (baseTrait != null) {
						final Class_Type baseTraitClass = baseTrait.getTypeRefdLast(timestamp).getClassType(); 
						for (Map.Entry<String, Def_AbsFunction> traitAbs : baseTraitClass.getAbstractFunctions().entrySet()) {
							if (! abstractFunctions.containsKey(traitAbs.getKey())) {
								addAbstractFunction(traitAbs.getKey(), traitAbs.getValue());
							}
						}
					}
				}
			}
			for (final Assignment member : members) {
				switch(member.getAssignmentType()) {
				case A_FUNCTION:
				case A_FUNCTION_RVAL:
				case A_FUNCTION_RTEMP:
					if (member instanceof Def_AbsFunction) {
						final Def_AbsFunction defAbsFunc = (Def_AbsFunction)member;
						if (defAbsFunc != null) {
							final String defName = defAbsFunc.getIdentifier().getName();
							if (!abstractFunctions.containsKey(defName)) {
								addAbstractFunction(defName, defAbsFunc);
							}
						}
					}
					break;
				default:
					break;
				}
			}
		}

		if (!isAbstract && !isTrait) {
			// all abstract methods from the base class and base traits have to be implemented in this class
			if (baseClass != null && baseClass.isAbstract) {
				for (Map.Entry<String, Def_AbsFunction> absFuncs : baseClass.getAbstractFunctions().entrySet()) {
					final Def_AbsFunction defAbsFunc = absFuncs.getValue();
					final Assignment ass = getLocalAssignmentByID(timestamp, defAbsFunc.getIdentifier());
					switch(ass.getAssignmentType()) {
					case A_FUNCTION:
					case A_FUNCTION_RVAL:
					case A_FUNCTION_RTEMP: 
						if (ass instanceof Def_AbsFunction) {
							getLocation().reportSemanticError( 
								MessageFormat.format(MISSINGABSIMPLEMENTATION, defAbsFunc.getFullName()));
						}
						// whether the new function is identical to the abstract one has
			            // already been checked
						break;
					default:
						// it's either an external function (which is OK), or
			            // it's shadowed by a member (error has already been reported)
			            break;
					}
				}
			}
			if (baseTraits != null) {
				for (final Type baseTrait : baseTraits) {
					if (baseTrait != null) {
						final Class_Type baseTraitClass = baseTrait.getTypeRefdLast(timestamp).getClassType();
						for (Map.Entry<String, Def_AbsFunction> absFuncs : baseTraitClass.getAbstractFunctions().entrySet()) {
							final Assignment ass = getLocalAssignmentByID(timestamp, absFuncs.getValue().getIdentifier());
							switch(ass.getAssignmentType()) {
							case A_FUNCTION:
							case A_FUNCTION_RVAL:
							case A_FUNCTION_RTEMP:
								if (ass instanceof Def_AbsFunction) {
									getLocation().reportSemanticError( 
										MessageFormat.format(MISSINGABSIMPLEMENTATION, absFuncs.getValue().getFullName()));
								}
								// whether the new function is identical to the abstract one has
				                // already been checked
				                break; 
				              default:
				                // it's either an external function (which is OK), or
				                // it's shadowed by a member (error has already been reported)
				                break;
							}
						}
					}
				}
			}
		}

		if (invalidNodes != null) {
			invalidNodes.check(timestamp);
		}

		lastTimeChecked = timestamp;
	}

	private void addDefaultParameter(FormalParameter fp) {
		final String name = classId.getName() + "_defpar_type_" + defaultParameterList.size();
		defaultParameterList.put(fp, name);
	}

	private boolean compareMembers(Class_Type c1, Class_Type c2, Location subclassLoc, CompilationTimeStamp timestamp) {
		if (subclassLoc != null && (c1.isParentClass(timestamp, c2) || c2.isParentClass(timestamp, c1))) {
			return false;
		}
		boolean nameClash = false;
		for (final Assignment def1 : c1.members) {
			if (def1.getAssignmentType() == Assignment_type.A_CONSTRUCTOR) {
				continue;
			}
			final Identifier id1 = def1.getIdentifier();
			if (c2.hasLocalAssignemtWithID(timestamp, id1)) {
				final Assignment def2 = c2.getLocalAssignmentByID(timestamp, id1);
				Class_Type def2Class = def2.getMyScope().getScopeClass();
				if (subclassLoc != null && def2Class != c2 && def2Class != c1 && c1.isParentClass(timestamp, def2Class)) {
					continue;
				}
				switch(def1.getAssignmentType()) {
				case A_FUNCTION:
				case A_FUNCTION_RVAL:
				case A_FUNCTION_RTEMP:
				case A_EXT_FUNCTION:
				case A_EXT_FUNCTION_RVAL:
				case A_EXT_FUNCTION_RTEMP:
					switch(def2.getAssignmentType()) {
					case A_FUNCTION:
					case A_FUNCTION_RVAL:
					case A_FUNCTION_RTEMP:
					case A_EXT_FUNCTION:
					case A_EXT_FUNCTION_RVAL:
					case A_EXT_FUNCTION_RTEMP: {
						final Def_FunctionBase func1 = (Def_FunctionBase)def1;
						final Def_FunctionBase func2 = (Def_FunctionBase)def2;
						final boolean func1IsAbstract = def1 instanceof Def_AbsFunction;
						final boolean func2IsAbstract = def2 instanceof Def_AbsFunction;
						IsIdenticalResult identical = func1.isIdentical(timestamp, func2);
						if (identical == IsIdenticalResult.RES_NAME_DIFFERS) {
							def1.getLocation().reportSemanticWarning(PARAMNAMEDIFFERS);
						}
						if (func2.getVisibilityModifier() != VisibilityModifier.Private &&
							(identical == IsIdenticalResult.RES_DIFFERS || func1IsAbstract != func2IsAbstract)) {
							if (subclassLoc == null && identical == IsIdenticalResult.RES_DIFFERS) {
								def1.getLocation().reportSemanticError(
									MessageFormat.format(PROTOTYPENOTIDENTICAL, id1.getDisplayName(), def2.getFullName()));
							} else if (subclassLoc != null && func2.getVisibilityModifier() != VisibilityModifier.Private) {
								subclassLoc.reportSemanticError(
									MessageFormat.format(PROTOTYPESDIFFER, id1.getDisplayName(), c1.getIdentifier().getDisplayName(),
									c2.getIdentifier().getDisplayName()));
							}
						} else if (subclassLoc == null && func2.isFinal()) {
							func1.getSignatureLocation().reportSemanticError(
								MessageFormat.format(CANNOTOVERRIDEFINAL, def2.getFullName())); 
						} else if (subclassLoc == null && func1.isIdentical(timestamp, func2) != IsIdenticalResult.RES_DIFFERS) {
							if (func2.getVisibilityModifier() == VisibilityModifier.Public && func1.getVisibilityModifier() != VisibilityModifier.Public) {
								func1.getSignatureLocation().reportSemanticError(
									MessageFormat.format(PUBLICCANBEOVERRIDDEN, id1.getDisplayName()));
							} else if (func2.getVisibilityModifier() == VisibilityModifier.Protected &&
								func1.getVisibilityModifier() != VisibilityModifier.Public && func1.getVisibilityModifier() != VisibilityModifier.Protected) {
								func1.getSignatureLocation().reportSemanticError(
									MessageFormat.format(PROTECTEDCANBEOVERRIDDEN, id1.getDisplayName()));
							}
						}
						break; }
					default:
						def1.getLocation().reportSemanticError(
							MessageFormat.format(SHADOWSINHERITEDMEMBER, def1.getDescription(), def2.getFullName()));
						nameClash = true;
						break;
					}
					break;
				default:
					def1.getLocation().reportSemanticError(
						MessageFormat.format(SHADOWSINHERITED2, def1.getDescription(),
						(def2 instanceof Def_FunctionBase) ? "method" : "member",
						def2.getFullName()));
					nameClash = true;
					break;
				}
			}
		}
		if (subclassLoc != null) {
			// the code above only goes through the local members of c1,
		    // comparing them to local or inherited members of c2;
		    // the members in the superclass and supertraits of c1 must also be compared to c2
			if (c1.baseClass != null) {
				nameClash |= compareMembers(c1.baseClass, c2, subclassLoc, timestamp);
			}
			if (c1.baseTraits != null) {
				for (Type baseTrait : c1.baseTraits) {
					if (baseTrait != null) {
						Class_Type baseTraitClass = baseTrait.getTypeRefdLast(timestamp).getClassType();
						nameClash |= compareMembers(baseTraitClass, c2, subclassLoc, timestamp);
					}
				}
			}
		}
		return nameClash;
	}

	public Class_Type getScopeClass() {
		final Scope parentScope = getMyScope().getParentScope();
		if (parentScope != null) {
			return parentScope.getScopeClass();
		}
		return null;
	}

	/**
	 * Gets the formal parameter list of default object methods
	 * @param methodName
	 * @return
	 */
	public static FormalParameterList getObjectMethodFormalParameterList(final String methodName) {
		final List<FormalParameter> fplist = new ArrayList<>();
		switch (methodName) {
		case GeneralConstants.TOSTRINGFUNCITON:
			return new FormalParameterList(fplist);
		case GeneralConstants.EQUALSFUNCTION:
			FormalParameter param = new FormalParameter(Restriction_type.TR_NONE, Assignment_type.A_PAR_VAL_IN, 
				new Class_Type(), new Identifier(Identifier_type.ID_TTCN, "obj"), null, null);
			fplist.add(param);
			return new FormalParameterList(fplist);
		default:
			return null;
		}
	}

	public Def_Constructor getConstructor(CompilationTimeStamp timestamp) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			check(timestamp);
		}

		return constructor;
	}

	public Class_Type getBaseClass() {
		return baseClass;
	}

	public Types getBaseTraits() {
		return baseTraits;
	}

	/**
	 * Gets the return type of default object methods
	 * @param methodName
	 * @return
	 */
	public static Type getObjectMethodReturnType(final String methodName) {
		switch (methodName) {
		case GeneralConstants.TOSTRINGFUNCITON:
			return new UniversalCharstring_Type();
		case GeneralConstants.EQUALSFUNCTION:
			return new Boolean_Type();
		default:
			return null;
		}
	}

	public boolean isParentClass(final CompilationTimeStamp timestamp, final Class_Type pclass) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			check(timestamp);
		}

		if (this == pclass || (! isTrait && pclass.isBuiltIn)) {
			return true;
		}
		if (baseClass != null && baseClass.isParentClass(timestamp, pclass)) {
			return true;
		}
		if (baseTraits != null) {
			for (final Type baseTrait : baseTraits) {
				if (baseTrait != null && 
					baseTrait.getTypeRefdLast(timestamp).getClassType().isParentClass(timestamp, pclass)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean hasLocalAssignemtWithID(CompilationTimeStamp timestamp, final Identifier id) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			check(timestamp);
		}
		if (isBuiltIn) {
			return false;
		}
		if (members.hasLocalAssignmentWithID(timestamp, id)) {
			return true;
		}
		if (baseClass != null && baseClass.hasLocalAssignemtWithID(timestamp, id)) {
			return true;
		}
		if (baseTraits != null) {
			for (final Type baseTrait : baseTraits) {
				if (baseTrait != null &&
					baseTrait.getTypeRefdLast(timestamp).getClassType().hasLocalAssignemtWithID(timestamp, id)) {
					return true;
				}
			}
		}
		return false;
	}

	public Assignment getLocalAssignmentByID(CompilationTimeStamp timestamp, final Identifier id) {
		if (isBuiltIn) {
			// TODO fatal 
		}
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			check(timestamp);
		}
		Assignment ass = null;
		if (members.hasLocalAssignmentWithID(timestamp, id)) {
			ass = members.getLocalAssignmentByID(timestamp, id);
		}
		if (ass == null && baseClass != null) {
			ass = baseClass.getLocalAssignmentByID(timestamp, id);
		}
		if (ass == null && baseTraits != null) {
			for (final Type baseTrait : baseTraits) {
				if (baseTrait != null) {
					ass = baseTrait.getTypeRefdLast(timestamp).getClassType().getLocalAssignmentByID(timestamp, id);
					if (ass != null) {
						break;
					}
				}
			}	
		}
		return ass;
	}

	public ClassTypeBody getClassTypeBody() {
		return body;
	}

	public Definitions getDefinitionMap() {
		return members;
	}

	public List<Definition> getDefinitions() {
		List<Definition> definitions = new ArrayList<>();

		if (members.getDefinitionMap() != null) {
			for (Map.Entry<String,Definition> defs : members.getDefinitionMap().entrySet()) {
				definitions.add(defs.getValue());
			}
		}

		return definitions;
	}

	/**
	 * Gets all definitions of a class including both local and inherited members
	 * @return A map of definitions with an indication if the definitions are inherited
	 */
	private Map<Definition,Boolean> getAllDefinitions() {
		Map<Definition,Boolean> definitions = new HashMap<>();

		if (baseClass != null) {
			for (Definition def : baseClass.getDefinitions()) {
				definitions.put(def, true);
			}
		}

		if (baseTraits != null) {
			for (final Type baseTrait : baseTraits) {
				if (baseTrait instanceof Class_Type) {
					for (Definition def : ((Class_Type)baseTrait).getDefinitions()) {
						definitions.put(def, true);
					}
				}
			}
		}

		if (members.getDefinitionMap() != null) {
			for (Map.Entry<String,Definition> defs : members.getDefinitionMap().entrySet()) {
				definitions.put(defs.getValue(), false);
			}
		}

		return definitions;
	}

	public void setInvalidNodes(final List<InvalidASTNode> nodes) {
		invalidNodes = new InvalidASTNodes();
		invalidNodes.setInvalidNodes(nodes);
		invalidNodes.setMyScope(getMyScope());
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean handled = false;
			boolean enveloped = false;

			final Location temporalIdentifier = classId.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser idReparser = new IdentifierReparser(reparser);
				final int result = idReparser.parseAndSetNameChanged();
				classId = idReparser.getIdentifier();
				// damage handled
				if (result == 0 && classId != null) {
					enveloped = true;
				} else {
					throw new ReParseException(result);
				}
			}

			if (runsOnRef != null) {
				if (enveloped) {
					runsOnRef.updateSyntax(reparser, false);
					reparser.updateLocation(runsOnRef.getLocation());
				} else if (reparser.envelopsDamage(runsOnRef.getLocation())) {
					runsOnRef.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(runsOnRef.getLocation());
				}
			}

			if (mtcRef != null) {
				if (enveloped) {
					mtcRef.updateSyntax(reparser, false);
					reparser.updateLocation(mtcRef.getLocation());
				} else if (reparser.envelopsDamage(mtcRef.getLocation())) {
					mtcRef.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(mtcRef.getLocation());
				}
			}

			if (systemRef != null) {
				if (enveloped) {
					systemRef.updateSyntax(reparser, false);
					reparser.updateLocation(systemRef.getLocation());
				} else if (reparser.envelopsDamage(systemRef.getLocation())) {
					systemRef.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(systemRef.getLocation());
				}
			}

			if (subType != null) {
				subType.updateSyntax(reparser, false);
				handled = true;
			}

			boolean invalidNodeChanged = false;
			if (invalidNodes != null) {
				for (final InvalidASTNode invalidNode : invalidNodes.getInvalidNodes()) {
					if (reparser.isAffectedAppended(invalidNode.getLocation())) {
						reparser.extendDamagedRegion(members.getLocation());
						invalidNodeChanged = true;
					}
					reparser.updateLocation(invalidNode.getLocation());
				}
			}

			int insideMember = -1;
			if (members != null) {
				for (int i = 0; i < members.size(); i++) {
					final Definition definition = members.get(i);
					reparser.updateLocation(definition.getLocation());
					if (reparser.envelopsDamage(definition.getLocation())) {
						insideMember = i;
						
						final Location defLocation = definition.getLocation();
						reparser.extendDamagedRegion(defLocation);
						reparseMember(reparser, definition);
					}
				}
				handled = true;
			}

			if (insideMember == -1 || invalidNodeChanged) {
				final Location membersLocation = members.getLocation();
				MarkerHandler.removeMarkers(
						membersLocation.getFile(), 
						membersLocation.getStartPosition(), membersLocation.getEndPosition());
				reparser.updateLocation(membersLocation);
				reparser.extendDamagedRegion(membersLocation);
				
				reparseMemberList(reparser);
				handled = true;
			} else {
				final Location membersLocation = members.getLocation();
				MarkerHandler.removeMarkers(
					membersLocation.getFile(), 
					membersLocation.getStartPosition(), membersLocation.getEndPosition());
				members.get(insideMember).updateSyntax(reparser, isDamaged);
				handled = true;
			}
			
			if (handled) {
				return;
			}

			throw new ReParseException();
		}

		/* 'object' has a null classId */
		if (classId != null) {
			reparser.updateLocation(classId.getLocation());
		}
	
		if (runsOnRef != null) {
			runsOnRef.updateSyntax(reparser, false);
			reparser.updateLocation(runsOnRef.getLocation());
		}

		if (mtcRef != null) {
			mtcRef.updateSyntax(reparser, false);
			reparser.updateLocation(mtcRef.getLocation());
		}

		if (systemRef != null) {
			systemRef.updateSyntax(reparser, false);
			reparser.updateLocation(systemRef.getLocation());
		}

//		if (body != null) {
//			body.updateSyntax(reparser, false);
//			reparser.updateLocation(body.getLocation());
//		}

		if (subType != null) {
			subType.updateSyntax(reparser, false);
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		return getAllDefinitions().keySet().toArray();
	}

	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		if (Configuration.INSTANCE.getBoolean(Configuration.OUTLINE_SHOW_CLASSES, true)) {
			return SymbolKind.Class;
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return location.getRange();
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() { 
		return classId.getLocation().getRange();
	}

	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		if (Configuration.INSTANCE.getBoolean(Configuration.OUTLINE_SHOW_CLASSES, true)) {
			final DocumentSymbol symbol = new DocumentSymbol(classId.getDisplayName(), getOutlineSymbolKind(),
				getOutlineRange(), getOutlineSelectionRange());
			symbol.setChildren(getOutlineSymbolChildren());

			return symbol;
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public List<DocumentSymbol> getOutlineSymbolChildren() {
		final List<DocumentSymbol> symbols = new ArrayList<>();
		for (Map.Entry<Definition,Boolean> defs : getAllDefinitions().entrySet()) {
			if (Boolean.TRUE.equals(defs.getValue())) {
				continue;
			}
			final Definition definition = defs.getKey();
			if (definition.getLocation() instanceof NULL_Location) {
				continue;
			}
			if (definition.getOutlineSymbolKind() != null) {				
				final DocumentSymbol child = new DocumentSymbol(definition.getFullName(), definition.getOutlineSymbolKind(),
					definition.getOutlineRange(), definition.getOutlineSelectionRange());
				child.setChildren(definition.getOutlineSymbolChildren());
				symbols.add(child);
			}
		}
		return symbols;
	}

	@Override
	public String getTypename() {
		return getFullName();
	}

	/**
	 * Checks if the class is declared as external
	 * @return
	 */
	public boolean isExternal() {
		return isExternal;
	}

	/**
	 * Checks if the class is declared using the '@trait' modifier
	 * @return
	 */
	public boolean isTrait() {
		return isTrait;
	}

	/**
	 * Checks if the class is built in
	 * @return
	 */
	public boolean isBuiltIn() {
		return isBuiltIn;
	}

	/**
	 * Checks if the class is declared using the '@abstract' modifier
	 * @return
	 */
	public boolean isAbstract() {
		return isAbstract;
	}

	/**
	 * Checks if the class is declared using the '@final' modifier
	 * @return
	 */
	public boolean isFinal() {
		return isFinal;
	}

	public Type getBaseType() {
		return baseType;
	}

	public Component_Type getRunsOnType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return runsOnType;
	}

	public Component_Type getMtcType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return mtcType;
	}

	public Component_Type getSystemType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return systemType;
	}

	public Location getModifierLocation() {
		return modifierLocation;
	}

	@Override
	public Identifier getComponentIdentifierByName(Identifier identifier) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkThisTemplate(CompilationTimeStamp timestamp, ITTCN3Template template, boolean isModified,
			boolean implicitOmit, Assignment lhs) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCompatible(CompilationTimeStamp timestamp, IType otherType, TypeCompatibilityInfo info,
			Chain leftChain, Chain rightChain) {
		// TODO Auto-generated method stub
		return true;
	}

	public void addClassMembers(Assignment assignment, Ttcn3HoverContent content, DocumentComment dc) {
		content.addTag("\n#### Members:\n\n");
		Map<String,String> localMembers = null;
		if (dc != null) {
			localMembers = dc.getMembers();
		}
		for (Map.Entry<Definition,Boolean> defs : getAllDefinitions().entrySet()) {
			final Definition def = defs.getKey();
			switch (def.getVisibilityModifier()) {
			case Private:
				content.addText("\n - &#x1f534;");
				break;
			case Public:
				content.addText("\n - &#x1f7e2;");
				break;
			default:
				content.addText("\n - &#x1f7e1;");
			}
			String id = def.getIdentifier().getDisplayName();
			String memberText = null;
			if (localMembers != null) {
				memberText = localMembers.get(id); 
			}
			String memberTypeName = null;
			boolean isOverride = false;
			if (def instanceof Def_Function) {
				memberTypeName = "function";
				if (((Def_Function)def).isOverride()) {
					isOverride = true; 
				}
			} else if (def instanceof Def_AbsFunction) {
				memberTypeName = "abstract function";
			} else if (def instanceof Def_Constructor) {
				memberTypeName = "constructor";
			} else if (def instanceof Def_Timer) {
				memberTypeName = "timer";
			}
			else {
				final IType memberType = def.getType(lastTimeChecked);
				if (memberType != null) {
					memberTypeName = memberType.getTypename();
				}
			}
			if (memberTypeName != null) {
				content.addText(" ").addText(memberTypeName).addText(" ");
			}
			content.addText("**" + def.getIdentifier().getDisplayName() + "**");

			if (def instanceof IParameterisedAssignment) {
				final IParameterisedAssignment paramAssignment = (IParameterisedAssignment)def;
				final FormalParameterList fpl = paramAssignment.getFormalParameterList();
				if (fpl != null) {
					final List<String> params = fpl.stream()
							.map(param -> param.getType(lastTimeChecked).getTypename())
							.collect(Collectors.toList());
					content.addText("(" + String.join(", ", params) + ")");
				}
			}

			if (isOverride) {
				content.addText(" (overridden)");
			} else {
				if (Boolean.TRUE.equals(defs.getValue())) {
					content.addText(" (inherited)");
				}

				if (def.getNameParent() instanceof Class_Type) {
					final Class_Type parentBody = (Class_Type)def.getNameParent();
					if (! parentBody.equals(this)) {						
						final Class_Type parentClass = baseClass;
						final Def_Type parentType = (Def_Type)parentClass.getNameParent();
						if (parentType.hasDocumentComment()) {
							final Map<String,String> parentMembers = parentType.getDocumentComment().getMembers();
							if (parentMembers != null) {
								memberText = parentMembers.get(id);
							}
						}
					}
				}
			}
			content.addText(" ").addText(memberText != null ? memberText : "");
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (! super.memberAccept(v)) {
			return false;
		}
		if (baseType != null && !baseType.accept(v)) {
			return false;
		}
		if (baseTraits != null && !baseTraits.accept(v)) {
			return false;
		}
		if (body != null && !body.accept(v)) {
			return false;
		}
		if (invalidNodes != null && !invalidNodes.accept(v)) {
			return false;
		}
		return true;
	}

	public boolean hasDefaultConstructor() {
		return defaultConstructor;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain chain) {
		if (isBuildCancelled()) {
			return;
		}

		if (isBuiltIn) {
			return;
		}
		if (baseType != null) {
			chain.markState();
			baseType.checkRecursions(timestamp, chain);
			chain.previousState();
		}
		if (baseTraits != null) {
			for (final Type baseTrait : baseTraits) {
				if (baseTrait != null) {
					chain.markState();
					baseTrait.checkRecursions(timestamp, chain);
					chain.previousState();
				}
			}
		}

		for (final Assignment def : members) {
			switch(def.getAssignmentType()) {
			case A_VAR:
				if (def.getType(timestamp).getTypeRefdLast(timestamp).getTypetype() == Type_type.TYPE_CLASS) {
					break;
				}
				// fall through
			case A_CONST:
			case A_TEMPLATE:
			case A_VAR_TEMPLATE:
				chain.markState();
				def.getType(timestamp).checkRecursions(timestamp, chain);
				chain.previousState();
				break;
			default:
				break;
			}
		}
	}

	public Definitions getMembers() {
		return members;
	}

	@Override
	public void findReferences(ReferenceFinder referenceFinder, List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);

		if (members != null) {
			members.findReferences(referenceFinder, foundIdentifiers);
		}
		if (body != null) {
			body.findReferences(referenceFinder, foundIdentifiers);
		}
		if (finallyBlock != null) {
			finallyBlock.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	private int reparseMember(final TTCN3ReparseUpdater aReparser, final Definition reparsed) {
		return aReparser.parse(parser -> {
			final Location location = reparsed.getLocation();

			MarkerHandler.removeMarkersAndSemanticHighlighting(location);

			final List<Definition> definitions = new ArrayList<Definition>();
			for (int i = 0; i < members.size(); i++) {
				final Definition def = members.get(i);
				if (!def.equals(reparsed)) {
					aReparser.updateLocation(def.getLocation());
					definitions.add(def);
				}
			}

			final TTCN3Module module = (TTCN3Module) getMyScope().getModuleScope();
			parser.setModule(module);
			final Pr_reparse_ClassMemberContext root =
					parser.pr_reparse_ClassMember(definitions);
			ParserUtilities.logParseTree(root, parser);
		});
	}

	private int reparseMemberList(final TTCN3ReparseUpdater aReparser) {
		return aReparser.parse(parser -> {
			final TTCN3Module module = (TTCN3Module) getMyScope().getModuleScope();
			parser.setModule(module);

			final Location membersLocation = members.getLocation();
			MarkerHandler.removeMarkersAndSemanticHighlighting(membersLocation);

			final Pr_reparse_ClassMemberListContext root =
					parser.pr_reparse_ClassMemberList(members, invalidNodes);
			ParserUtilities.logParseTree(root, parser);
		});
	}

	@Override
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(getIdentifier().getLocation(), SemanticType.Class);
		super.setSemanticInformation();
	}
}
