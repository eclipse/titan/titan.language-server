/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types.subtypes;

/**
 * interface for all limit classes used by RangeListConstraint
 *
 * @author Adam Delic
 * @author Adam Knapp
 */
public interface ILimitType extends Comparable<ILimitType> {

	/** all possible kinds of limits, each one is implemented with a class */
	enum Type {
		SIZE, INTEGER, CHAR, UCHAR, REAL
	}

	ILimitType.Type getType();

	boolean isAdjacent(ILimitType other);

	ILimitType increment();

	ILimitType decrement();

	void toString(StringBuilder sb);

	static ILimitType getMaximum(final Type type) {
		switch (type) {
		case SIZE:
			return SizeLimit.MAXIMUM;
		case INTEGER:
			return IntegerLimit.MAXIMUM;
		case CHAR:
			return CharLimit.MAXIMUM;
		case UCHAR:
			return UCharLimit.MAXIMUM;
		case REAL:
			return RealLimit.MAXIMUM;
		default:
			return null;
		}
	}

	static ILimitType getMinimum(final Type type) {
		switch (type) {
		case SIZE:
			return SizeLimit.MINIMUM;
		case INTEGER:
			return IntegerLimit.MINIMUM;
		case CHAR:
			return CharLimit.MINIMUM;
		case UCHAR:
			return UCharLimit.MINIMUM;
		case REAL:
			return RealLimit.MINIMUM;
		default:
			return null;
		}
	}
}
