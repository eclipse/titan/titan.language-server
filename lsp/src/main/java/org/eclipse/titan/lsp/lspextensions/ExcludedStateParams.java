/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.generator.JsonRpcData;

/**
 * This class represents an entry on the list of 'excluded state' changes.
 * 
 * @author Miklos Magyari
 *
 */
@JsonRpcData
public class ExcludedStateParams {
	private boolean isExcluded;
	private TextDocumentIdentifier identifier;
	
	ExcludedStateParams(TextDocumentIdentifier identifier, boolean isExcluded) {
		this.identifier = identifier;
		this.isExcluded = isExcluded;
	}

	public TextDocumentIdentifier getTextDocument() {
		return identifier;
	}

	public boolean isExcluded() {
		return isExcluded;
	}
}