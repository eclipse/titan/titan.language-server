/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.core;

import org.eclipse.titan.lsp.common.logging.TitanLogger;

/**
 * @author Szabolcs Beres
 * */
public final class LoadBalancingUtilities {

	// LSPFIX
//	private static SubscribedInt tokensToProcessInARow;
//	private static SubscribedInt threadPriority;
//	private static SubscribedInt sleepBetweenFiles;
//	private static SubscribedBoolean yieldBetweenChecks;
	
	private static int tokensToProcessInARow;
	private static int threadPriority;
	private static int sleepBetweenFiles;
	private static boolean yieldBetweenChecks;

	static {
		tokensToProcessInARow = 100;
		threadPriority = Thread.MIN_PRIORITY;
		sleepBetweenFiles = 10;
		yieldBetweenChecks = true;
	}

	private LoadBalancingUtilities() {
		// Do nothing
	}

	public static int getThreadPriority() {
		return threadPriority;
	}

	public static int getTokensToProcessInARow() {
		return tokensToProcessInARow;
	}

	public static void syntaxAnalyzerProcessedAFile() {
		if (sleepBetweenFiles >= 0) {
			try {
				Thread.sleep(sleepBetweenFiles);
			} catch (InterruptedException e) {
				TitanLogger.logError(e);
				Thread.currentThread().interrupt();
			}
		}
	}

	public static void astNodeChecked() {
		if (yieldBetweenChecks) {
			Thread.yield();
		}
	}

}
