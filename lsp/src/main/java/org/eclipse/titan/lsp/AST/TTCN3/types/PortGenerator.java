/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.util.ArrayList;
import java.util.List;

/**
 * The code generator part for port types.
 *
 * @author Kristof Szabados
 * */
public final class PortGenerator {

	/** The kind of the testport */
	public enum TestportType {
		NORMAL, INTERNAL, ADDRESS
	}

	/** The kind of the port extension */
	public enum PortType {
		REGULAR, PROVIDER, USER
	}

	/**
	 * Structure to describe in messages.
	 *
	 * originally port_msg_type is something like this
	 * */
	public static class messageTypeInfo {
		/** Java type name of the message */
		private final String mJavaTypeName;

		/** Java template name of the message */
		private final String mJavaTemplateName;

		/** The name to be displayed to the user */
		private final String mDisplayName;

		/**
		 * @param messageType
		 *                the string representing the value type of this
		 *                message in the generated code.
		 * @param messageTemplate
		 *                the string representing the template type of
		 *                this message in the generated code.
		 * @param displayName
		 *                the string representing the name to be
		 *                displayed for the user.
		 * */
		public messageTypeInfo(final String messageType, final String messageTemplate, final String displayName) {
			mJavaTypeName = messageType;
			mJavaTemplateName = messageTemplate;
			mDisplayName = displayName;
		}
	}

	public enum MessageMappingType_type {
		SIMPLE, DISCARD, FUNCTION, ENCODE, DECODE
	}

	public enum FunctionPrototype_Type {
		CONVERT, FAST, BACKTRACK, SLIDING
	}

	/**
	 * Structure to describe message mapping targets of an out parameter.
	 *
	 * originally port_msg_type_mapping_target is something like this
	 * */
	public static class MessageTypeMappingTarget {
		private String targetName;
		private String targetDisplayName;
		public int targetIndex;
		private final MessageMappingType_type mappingType;

		//only relevant for function style mapping
		private String functionDisplayName;
		private String functionName;
		private FunctionPrototype_Type functionPrototype;

		//only relevant for encdec style mapping
		private String encdecTypedesriptorName;
		private String encdecEncodingType;
		private String encdecEncodingOptions;
		private String encdecErrorBehaviour;

		/**
		 * The constructor for discard mapping targets
		 * */
		public MessageTypeMappingTarget() {
			this.mappingType = MessageMappingType_type.DISCARD;
		}

		/**
		 * The constructor for simple mapping targets.
		 *
		 * @param targetType
		 *                the string representing the value type of this
		 *                message in the generated code.
		 * @param displayName
		 *                the string representing the name to be
		 *                displayed for the user.
		 * */
		public MessageTypeMappingTarget(final String targetType, final String displayName) {
			this.targetName = targetType;
			this.targetDisplayName = displayName;
			this.mappingType = MessageMappingType_type.SIMPLE;
		}

		/**
		 * The constructor for function mapping targets.
		 *
		 * @param targetType
		 *                the string representing the value type of this
		 *                message in the generated code.
		 * @param displayName
		 *                the string representing the name to be
		 *                displayed for the user.
		 * @param functionName
		 *                the string representing the name of the
		 *                function.
		 * @param functionDisplayName
		 *                the string representing the function in error
		 *                messages.
		 * @param functionPrototype
		 *                the prototype of the function.
		 * */
		public MessageTypeMappingTarget(final String targetType, final String displayName, final String functionName, final String functionDisplayName, final FunctionPrototype_Type functionPrototype) {
			this.targetName = targetType;
			this.targetDisplayName = displayName;
			this.functionName = functionName;
			this.functionDisplayName = functionDisplayName;
			this.functionPrototype = functionPrototype;

			this.mappingType = MessageMappingType_type.FUNCTION;
		}

		/**
		 * The constructor for function mapping targets.
		 *
		 * @param targetType
		 *                the string representing the value type of this
		 *                message in the generated code.
		 * @param displayName
		 *                the string representing the name to be
		 *                displayed for the user.
		 * @param typedescriptorName
		 *                the string representing the typedescriptor.
		 * @param encodingType
		 *                the string representing the encoding type.
		 * @param encodingOptions
		 *                the string representing the options of the
		 *                encoding type.
		 * @param errorbeviour
		 *                the string representing the errorbehaviour
		 *                setting code.
		 * @param mappingType
		 *                encode or decode
		 * */
		public MessageTypeMappingTarget(final String targetType, final String displayName, final String typedescriptorName, final String encodingType, final String encodingOptions, final String errorbeviour, final MessageMappingType_type mappingType) {
			this.targetName = targetType;
			this.targetDisplayName = displayName;
			this.encdecTypedesriptorName = typedescriptorName;
			this.encdecEncodingType = encodingType;
			this.encdecEncodingOptions = encodingOptions;
			this.encdecErrorBehaviour = errorbeviour;
			this.mappingType = mappingType;
		}
	}

	/**
	 * Structure to describe out messages.
	 *
	 * originally port_msg_mapped_type is something like this
	 * */
	public static class MessageMappedTypeInfo {
		/** Java type name of the message */
		private final String mJavaTypeName;

		/** Java template name of the message */
		private final String mJavaTemplateName;

		/** The name to be displayed to the user */
		public final String mDisplayName;

		private final List<MessageTypeMappingTarget> targets = new ArrayList<>();

		/**
		 * @param messageType
		 *                the string representing the value type of this
		 *                message in the generated code.
		 * @param messageTemplate
		 *                the string representing the template type of
		 *                this message in the generated code.
		 * @param displayName
		 *                the string representing the name to be
		 *                displayed for the user.
		 * */
		public MessageMappedTypeInfo(final String messageType, final String messageTemplate, final String displayName) {
			mJavaTypeName = messageType;
			mJavaTemplateName = messageTemplate;
			mDisplayName = displayName;
		}

		public void addTarget(final MessageTypeMappingTarget newTarget) {
			this.targets.add(newTarget);
		}

		public void addTargets(final List<MessageTypeMappingTarget> newTargets) {
			this.targets.addAll(newTargets);
		}
	}

	/**
	 * Structure to describe in and out messages.
	 *
	 * originally port_proc_signature is something like this
	 * */
	public static final class procedureSignatureInfo {
		private final String mJavaTypeName;
		private final String mDisplayName;
		private final boolean isNoBlock;
		private final boolean hasExceptions;
		private final boolean hasReturnValue;

		public procedureSignatureInfo(final String procedureType, final String displayName, final boolean isNoBlock, final boolean hasExceptions, final boolean hasReturnValue) {
			this.mJavaTypeName = procedureType;
			this.mDisplayName = displayName;
			this.isNoBlock = isNoBlock;
			this.hasExceptions = hasExceptions;
			this.hasReturnValue = hasReturnValue;
		}
	}

	/**
	 * Structure to describe message providers.
	 *
	 * originally port_msg_provider is something like this
	 * */
	public static class portMessageProvider {
		private final String name;
		private final List<String> outMessageTypeNames;
		private final boolean realtime;

		public portMessageProvider(final String name, final List<String> outMessageTypeNames, final boolean realtime) {
			this.name = name;
			this.outMessageTypeNames = outMessageTypeNames;
			this.realtime = realtime;
		}
	}

	/**
	 * Structure describing all data needed to generate the port.
	 *
	 * originally port_def
	 * */
	public static class PortDefinition {
		/** Java type name of the port */
		public String javaName;

		/** The original name in the TTCN-3 code */
		public String displayName;

		/** The name of address in the actual module */
		public String addressName;

		/** The list of incoming messages */
		public List<messageTypeInfo> inMessages = new ArrayList<>();

		/** The list of outgoing messages */
		public List<MessageMappedTypeInfo> outMessages = new ArrayList<>();

		public List<procedureSignatureInfo> inProcedures = new ArrayList<>();

		public List<procedureSignatureInfo> outProcedures = new ArrayList<>();

		/** The type of the testport */
		public TestportType testportType;
		public PortType portType;

		public List<portMessageProvider> providerMessageOutList;

		public List<MessageMappedTypeInfo> providerInMessages = new ArrayList<>();

		public boolean has_sliding;
		public boolean legacy;

		public String varDefs;
		public String varInit;
		public String translationFunctions;
		public boolean realtime;

		public PortDefinition(final String genName, final String displayName) {
			javaName = genName;
			this.displayName = displayName;
		}
	}

	private PortGenerator() {
		// private to disable instantiation
	}
}
