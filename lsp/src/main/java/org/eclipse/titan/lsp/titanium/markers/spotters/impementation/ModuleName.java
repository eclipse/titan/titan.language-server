/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Group;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;
import org.eclipse.titan.lsp.AST.Module;

public class ModuleName {
	private ModuleName() {
		throw new AssertionError("Noninstantiable");
	}

	public abstract static class Base extends BaseModuleCodeSmellSpotter {
		private static final String REPORT = "The name {1} of the {0} contains the module name {2} it is located in";

		protected Base() {
			super(CodeSmellType.MODULENAME_IN_DEFINITION);
		}

		protected void check(final Identifier identifier, final Identifier moduleID, final String description, final Problems problems) {
			final String displayName = identifier.getDisplayName();
			if (displayName.contains(moduleID.getDisplayName())) {
				final String msg = MessageFormat.format(REPORT, description, displayName, moduleID.getDisplayName());
				problems.report(identifier.getLocation(), msg);
			}
		}
	}

	public static class InDef extends Base {
		public InDef() {
			super();
			addStartNode(Definition.class);
		}
		
		@Override
		public void process(final IVisitableNode node, final Problems problems) {
			if ((node instanceof Definition) && !(node instanceof FormalParameter)) {
				final Definition s = (Definition) node;
				final Identifier identifier = s.getIdentifier();
				final Identifier moduleID = s.getMyScope().getModuleScope().getIdentifier();
				check(identifier, moduleID, s.getDescription(), problems);
			}
		}
	}

	public static class InGroup extends Base {
		public InGroup() {
			super();
			addStartNode(Group.class);
		}
		
		@Override
		public void process(final IVisitableNode node, final Problems problems) {
			if (node instanceof Group) {
				final Group group = (Group) node;
				final Identifier identifier = group.getIdentifier();

				final Module module = group.getModule();
				if (module == null) {
					return;
				}
				final Identifier moduleID = module.getIdentifier();
				check(identifier, moduleID, "group", problems);
			}
		}
	}
}
