/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a list of type mappings.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class TypeMappings extends ASTNode implements ILocateableNode, IIncrementallyUpdatable,
	ICollection<TypeMapping> {
	private static final String FULLNAMEPART = ".<mapping";

	private final List<TypeMapping> mappings = new ArrayList<TypeMapping>();
	private final Map<String, TypeMapping> mappingsMap = new HashMap<String, TypeMapping>();

	/** the time when this type mapping was check the last time. */
	private CompilationTimeStamp lastTimeChecked;

	private Location location = NULL_Location.getInstance();

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	public boolean add(final TypeMapping mapping) {
		mapping.setFullNameParent(this);
		return mappings.add(mapping);
	}

	@Override
	public int size() {
		return mappings.size();
	}

	@Override
	public TypeMapping get(final int index) {
		return mappings.get(index);
	}

	@Override
	public boolean contains(final Object t) {
		if (!(t instanceof IType)) {
			return false;
		}
		final IType type = (IType)t;
		return type.getIsErroneous() || mappingsMap.containsKey(type.getTypename());
	}

	public TypeMapping get(final IType type) {
		return mappingsMap.get(type.getTypename());
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = mappings.size(); i < size; i++) {
			if (mappings.get(i) == child) {
				return builder.append(FULLNAMEPART).append(i + 1).append(MORETHAN);
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (final TypeMapping tm : mappings) {
			tm.setMyScope(scope);
		}
	}

	/**
	 * Copy over the mappings from the provided mapping list.
	 * @param otherMappings the other list of mappings.
	 */
	public void copyMappings(final TypeMappings otherMappings) {
		for (final TypeMapping tm : otherMappings) {
			mappings.add(tm);
		}

		// join the locations
		getLocation().setEndPosition(otherMappings.getLocation().getEndPosition());
	}

	/**
	 * Does the semantic checking of the type mapping.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param portType the type of the mapping port.
	 * @param legacy is this the legacy behavior.
	 * @param incoming is it mapping in incoming direction?
	 */
	public void check(final CompilationTimeStamp timestamp, final Port_Type portType, final boolean legacy, final boolean incoming) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;

		mappingsMap.clear();
		for (final TypeMapping mapping : mappings) {
			mapping.check(timestamp, portType, legacy, incoming);
			final Type sourceType = mapping.getSourceType();

			if (sourceType != null && !sourceType.getTypeRefdLast(timestamp).getIsErroneous(timestamp)) {
				final String sourceName = sourceType.getTypename();
				if (mappingsMap.containsKey(sourceName)) {
					sourceType.getLocation().reportSemanticError(
							MessageFormat.format("Duplicate mapping for type `{0}''", sourceName));
					final String message = MessageFormat.format("The mapping of the type `{0}'' is already given here",
							sourceName);
					mappingsMap.get(sourceName).getLocation().reportSemanticWarning(message);
				} else {
					mappingsMap.put(sourceName, mapping);
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final TypeMapping mapping : mappings) {
			mapping.updateSyntax(reparser, false);
			reparser.updateLocation(mapping.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final TypeMapping tm : mappings) {
			tm.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final TypeMapping tm : mappings) {
			if (!tm.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<TypeMapping> iterator() {
		return mappings.iterator();
	}
}
