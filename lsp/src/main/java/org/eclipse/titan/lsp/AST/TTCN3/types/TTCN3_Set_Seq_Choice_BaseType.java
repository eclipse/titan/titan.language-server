/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.List;
import java.util.Set;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferenceableElement;
import org.eclipse.titan.lsp.AST.IReferencingType;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.ITypeWithComponents;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Choice_Type;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Set_Seq_Choice_BaseType;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST_Base;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.JsonAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST.rawAST_ext_bit_group;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST.rawAST_field_list;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST.rawAST_single_tag;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST.rawAST_tag_field_value;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.IMemberInfo;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * */
public abstract class TTCN3_Set_Seq_Choice_BaseType extends Type implements ITypeWithComponents, IReferenceableElement, IMemberInfo {
	private static final String LENGTHTO_ERROR_MISSING_RESTRICTION = "The length of this field must be specified either with `length()` subtyping or with the `FIELDLENGTH` attribute";
	private static final String LENGTHTO_ERROR_WRONG_TYPE = "The union type LENGTHTO field must contain only integer, bitstring or octetstring fields";
	private static final String OMITCROSSTAG = "Field member in RAW parameter CROSSTAG cannot be `omit'";
	private static final String INVALIDFIELDCROSSTAG = "Invalid field name in RAW parameter CROSSTAG for field {0}: {1}";
	private static final String INVALIDFIELDTYPECROSSTAG = "Invalid fieldmember type in RAW parameter CROSSTAG for field {0}";
	private static final String INVALIDFIELDFORCEOMIT = "Invalid field name `{0}'' in RAW parameter FORCEOMIT for field {1}";
	private static final String INVALIDFIELDLENGTHINDEX = "Invalid field name in RAW parameter LENGTHINDEX for field {0}: {1}";
	private static final String INVALIDFIELDEXTENSION_BIT_GROUP = "Invalid field name `{0}'' in RAW parameter EXTENSION_BIT_GROUP for type `{1}''";
	private static final String IGNORENAMEAS = "Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names";
	private static final String INVALIDNAMEAS = "Invalid attribute, 'name as ...' requires field of a record, set or union";

	protected CompFieldMap compFieldMap;

	private boolean componentInternal;
	protected BuildTimestamp rawLengthCalculated;
	protected int rawLength;

	private boolean insideCanHaveCoding = false;

	protected TTCN3_Set_Seq_Choice_BaseType(final CompFieldMap compFieldMap) {
		this.compFieldMap = compFieldMap;
		compFieldMap.setMyType(this);
		compFieldMap.setFullNameParent(this);
	}

	@Override
	/** {@inheritDoc} */
	public String getTypename() {
		return getFullName();
	}

	@Override
	/** {@inheritDoc} */
	public final void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		compFieldMap.setMyScope(scope);
	}

	/** @return the number of components */
	public final int getNofComponents() {
		if (compFieldMap == null) {
			return 0;
		}

		return compFieldMap.fields.size();
	}

	/**	 * Returns the element at the specified position.
	 *
	 * @param index index of the element to return
	 * @return the element at the specified position in this list
	 */
	public final CompField getComponentByIndex(final int index) {
		return compFieldMap.fields.get(index);
	}

	/**
	 * Returns whether a component with the name exists or not..
	 *
	 * @param name the name of the element to check
	 * @return true if there is an element with that name, false otherwise.
	 */
	public final boolean hasComponentWithName(final String name) {
		if (compFieldMap.componentFieldMap == null) {
			return false;
		}

		return compFieldMap.componentFieldMap.containsKey(name);
	}

	/**
	 * Returns the index of the element with the specified name.
	 *
	 * @param identifier
	 *                the name of the element to return
	 * @return the index of an element with the provided name,
	 *         -1 if there is no such element.
	 */
	public int getComponentIndexByName(final Identifier identifier) {
		for (int i = 0; i < compFieldMap.fields.size(); i++) {
			if (compFieldMap.fields.get(i).getIdentifier().equals(identifier)) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Returns the index of the element with the specified name.
	 *
	 * @param name
	 *                the name of the element to return
	 * @return the index of an element with the provided name,
	 *         -1 if there is no such element.
	 */
	public int getComponentIndexByName(final String name) {
		for (int i = 0; i < compFieldMap.fields.size(); i++) {
			if (compFieldMap.fields.get(i).getIdentifier().getName().equals(name)) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Returns the element with the specified name.
	 *
	 * @param name the name of the element to return
	 * @return the element with the specified name in this list, or null if none was found
	 */
	public final CompField getComponentByName(final String name) {
		if (compFieldMap.componentFieldMap == null) {
			return null;
		}

		return compFieldMap.componentFieldMap.get(name);
	}

	/**
	 * Returns the identifier of the element at the specified position.
	 *
	 * @param index index of the element to return
	 * @return the identifier of the element at the specified position in this
	 *         list
	 */
	public final Identifier getComponentIdentifierByIndex(final int index) {
		return compFieldMap.fields.get(index).getIdentifier();
	}

	@Override
	/** {@inheritDoc} */
	public final IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final IReferenceChain refChain, final boolean interruptIfOptional) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return this;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);
		switch (subreference.getReferenceType()) {
		case arraySubReference:
			subreference.getLocation().reportSemanticError(MessageFormat.format(ArraySubReference.INVALIDSUBREFERENCE, getTypename()));
			return null;
		case fieldSubReference:
			final Identifier id = subreference.getId();
			final CompField compField = compFieldMap.getCompWithName(id);
			if (compField == null) {
				subreference.getLocation().reportSemanticError(
						MessageFormat.format(FieldSubReference.NONEXISTENTSUBREFERENCE, ((FieldSubReference) subreference).getId().getDisplayName(),
								getTypename()));
				return null;
			}

			final IType fieldType = compField.getType();
			if (fieldType == null) {
				return null;
			}

			if (interruptIfOptional && compField.isOptional()) {
				return null;
			}

			final Expected_Value_type internalExpectation =
					expectedIndex == Expected_Value_type.EXPECTED_TEMPLATE ? Expected_Value_type.EXPECTED_DYNAMIC_VALUE : expectedIndex;
			//This is the recursive function call:
			return fieldType.getFieldType(timestamp, reference, actualSubReference + 1, internalExpectation, refChain, interruptIfOptional);
		case parameterisedSubReference:
			subreference.getLocation().reportSemanticError(
					MessageFormat.format(FieldSubReference.INVALIDSUBREFERENCE, ((ParameterisedSubReference) subreference).getId().getDisplayName(),
							getTypename()));
			return null;
		default:
			subreference.getLocation().reportSemanticError(ISubReference.INVALIDSUBREFERENCE);
			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean getSubrefsAsArray(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final List<Integer> subrefsArray, final List<IType> typeArray) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return true;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);
		switch (subreference.getReferenceType()) {
		case arraySubReference:
			subreference.getLocation().reportSemanticError(MessageFormat.format(ArraySubReference.INVALIDSUBREFERENCE, getTypename()));
			return false;
		case fieldSubReference:
			final Identifier id = subreference.getId();
			final CompField compField = compFieldMap.getCompWithName(id);
			if (compField == null) {
				subreference.getLocation().reportSemanticError(
						MessageFormat.format(FieldSubReference.NONEXISTENTSUBREFERENCE, ((FieldSubReference) subreference).getId().getDisplayName(),
								getTypename()));
				return false;
			}

			final IType fieldType = compField.getType();
			if (fieldType == null) {
				return false;
			}

			final int fieldIndex = compFieldMap.fields.indexOf(compField);
			subrefsArray.add(fieldIndex);
			typeArray.add(this);
			return fieldType.getSubrefsAsArray(timestamp, reference, actualSubReference + 1, subrefsArray, typeArray);
		case parameterisedSubReference:
			subreference.getLocation().reportSemanticError(
					MessageFormat.format(FieldSubReference.INVALIDSUBREFERENCE, ((ParameterisedSubReference) subreference).getId().getDisplayName(),
							getTypename()));
			return false;
		default:
			subreference.getLocation().reportSemanticError(ISubReference.INVALIDSUBREFERENCE);
			return false;
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean getFieldTypesAsArray(final Reference reference, final int actualSubReference, final List<IType> typeArray) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return true;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);
		switch (subreference.getReferenceType()) {
		case arraySubReference:
			return false;
		case fieldSubReference:
			if (compFieldMap == null) {
				return false;
			}

			final Identifier id = subreference.getId();
			final CompField compField = compFieldMap.getCompWithName(id);
			if (compField == null) {
				return false;
			}

			final IType fieldType = compField.getType();
			if (fieldType == null) {
				return false;
			}
			typeArray.add(this);
			return fieldType.getFieldTypesAsArray(reference, actualSubReference + 1, typeArray);
		case parameterisedSubReference:
			return false;
		default:
			return false;
		}
	}

	@Override
	/** {@inheritDoc} */
	public final boolean isComponentInternal(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return componentInternal;
	}

	@Override
	/** {@inheritDoc} */
	public final void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		MarkerHandler.removeMarkers(this, true);//TODO: Check its place!!
		lastTimeChecked = timestamp;
		componentInternal = false;
		isErroneous = false;

		initAttributes(timestamp);

		compFieldMap.check(timestamp);

		for (int i = 0, size = getNofComponents(); i < size; i++) {
			final IType type = getComponentByIndex(i).getType();
			if (type != null && type.isComponentInternal(timestamp)) {
				componentInternal = true;
				break;
			}
		}

		if (constraints != null) {
			constraints.check(timestamp);
		}

		checkSubtypeRestrictions(timestamp);

		if (myScope != null) {
			checkEncode(timestamp);
			checkVariants(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public final void checkComponentInternal(final CompilationTimeStamp timestamp, final Set<IType> typeSet, final String operation) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (typeSet.contains(this)) {
			return;
		}

		typeSet.add(this);
		for (int i = 0, size = getNofComponents(); i < size; i++) {
			final IType type = getComponentByIndex(i).getType();
			if (type != null && type.isComponentInternal(timestamp)) {
				type.checkComponentInternal(timestamp, typeSet, operation);
			}
		}
		typeSet.remove(this);
	}

	@Override
	/** {@inheritDoc} */
	public void getTypesWithNoCodingTable(final CompilationTimeStamp timestamp, final List<IType> typeList, final boolean onlyOwnTable) {
		super.getTypesWithNoCodingTable(timestamp, typeList, onlyOwnTable);

		for (int i = 0, size = getNofComponents(); i < size; i++) {
			final IType type = getComponentByIndex(i).getType();
			if (type != null) {
				type.getTypesWithNoCodingTable(timestamp, typeList, onlyOwnTable);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkMapParameter(final CompilationTimeStamp timestamp, final IReferenceChain refChain, final Location errorLocation) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (refChain.contains(this)) {
			return;
		}

		refChain.add(this);
		for (int i = 0, size = getNofComponents(); i < size; i++) {
			final IType type = getComponentByIndex(i).getType();
			type.checkMapParameter(timestamp, refChain, errorLocation);
		}
	}

	@Override
	/** {@inheritDoc} */
	public final Object[] getOutlineChildren() {
		return compFieldMap.getOutlineChildren();
	}

	@Override
	/** {@inheritDoc} */
	public final void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean handled = false;

			if (compFieldMap != null
					&& reparser.envelopsDamage(compFieldMap.getLocation())) {
				try {
					compFieldMap.updateSyntax(reparser, true);
				} catch (ReParseException e) {
					throw new ReParseException(null);
				}

				reparser.updateLocation(compFieldMap.getLocation());
				handled = true;
			}

			if (subType != null) {
				subType.updateSyntax(reparser, false);
				handled = true;
			}

			if (handled) {
				return;
			}

			throw new ReParseException();
		}

		reparser.updateLocation(compFieldMap.getLocation());
		compFieldMap.updateSyntax(reparser, false);

		if (subType != null) {
			subType.updateSyntax(reparser, false);
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void getEnclosingField(final Position offset, final ReferenceFinder rf) {
		compFieldMap.getEnclosingField(offset, rf);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (compFieldMap != null) {
			compFieldMap.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (compFieldMap!=null && !compFieldMap.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Identifier getComponentIdentifierByName(final Identifier identifier) {
		if(identifier == null){
			return null;
		}
		final CompField cf = getComponentByName(identifier.getName());
		return cf == null ? null : cf.getIdentifier();
	}

	@Override
	/** {@inheritDoc} */
	public IDeclaration resolveReference(final Reference reference, final int subRefIdx, final ISubReference lastSubreference) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		int localIndex = subRefIdx;
		while (localIndex < subreferences.size() && subreferences.get(localIndex) instanceof ArraySubReference) {
			++localIndex;
		}

		if (localIndex == subreferences.size()) {
			return null;
		}

		final CompField compField = getComponentByName(subreferences.get(localIndex).getId().getName());
		if (compField == null) {
			return null;
		}
		if (subreferences.get(localIndex) == lastSubreference) {
			return IDeclaration.createInstance(getDefiningAssignment(), compField.getIdentifier());
		}

		final IType compFieldType = compField.getType().getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
		if (compFieldType instanceof IReferenceableElement) {
			final IDeclaration decl = ((IReferenceableElement) compFieldType).resolveReference(reference, localIndex + 1, lastSubreference);
			return decl != null ? decl : IDeclaration.createInstance(getDefiningAssignment(), compField.getIdentifier());
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public boolean canHaveCoding(final CompilationTimeStamp timestamp, final MessageEncoding_type coding) {
		if (insideCanHaveCoding) {
			return true;
		}
		insideCanHaveCoding = true;

		for (int i = 0; i < codingTable.size(); i++) {
			final Coding_Type tempCodingType = codingTable.get(i);

			if (tempCodingType.builtIn && tempCodingType.builtInCoding.equals(coding)) {
				insideCanHaveCoding = false;
				return true; // coding already added
			}
		}

		if (coding == MessageEncoding_type.BER) {
			final boolean result = hasEncoding(timestamp, MessageEncoding_type.BER, null);

			insideCanHaveCoding = false;
			return result;
		}

		for ( final CompField compField : compFieldMap.fields ) {
			final Type fieldType = compField.getType();
			if (fieldType == null) {
				return false;
			}
			final IType refdLast = fieldType.getTypeRefdLast(timestamp);
			if (!refdLast.canHaveCoding(timestamp, coding)) {
				insideCanHaveCoding = false;
				return false;
			}
		}

		insideCanHaveCoding = false;
		return true;
	}

	protected void checkSetSeqJson(final CompilationTimeStamp timestamp) { // T_SEQ_T, T_SET_T
		if (isBuildCancelled()) {
			return;
		}
		
		if (jsonAttribute == null && !hasEncodeAttribute(MessageEncoding_type.JSON)) {
			return;
		}

		for (int i = 0; i < getNofComponents(); i++) {
			final Type fieldType = getComponentByIndex(i).getType();
			fieldType.forceJson(timestamp);
		}

		if (jsonAttribute == null) {
			return;
		}

		if (jsonAttribute.omit_as_null && !isOptionalField()) {
			getLocation().reportSemanticError("Invalid attribute, 'omit as null' requires optional field of a record or set.");
		}

		if (jsonAttribute.as_value && getNofComponents() != 1) {
			getLocation().reportSemanticError("Invalid attribute, 'as value' is only allowed for unions, the anytype, or records or sets with one field");
		}

		if (jsonAttribute.alias != null) {
			final IType parent = getParentType();
			if (parent == null) {
				// only report this error when using the new codec handling, otherwise
				// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
				getLocation().reportSemanticError(INVALIDNAMEAS);
			} else {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					break;
				default:
					// only report this error when using the new codec handling, otherwise
					// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
					getLocation().reportSemanticError(INVALIDNAMEAS);
					break;
				}
			}

			if (parent != null && parent.getJsonAttribute() != null && parent.getJsonAttribute().as_value) {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					// parent_type_name remains null if the 'as value' attribute is set for an invalid type
					getLocation().reportSemanticWarning(MessageFormat.format(IGNORENAMEAS, parent.getTypename()));
					break;
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
					if (((TTCN3_Set_Seq_Choice_BaseType)parent).getNofComponents() == 1) {
						// parent_type_name remains null if the 'as value' attribute is set for an invalid type
						getLocation().reportSemanticWarning(MessageFormat.format(IGNORENAMEAS, parent.getTypename()));
					}
					break;
				default:
					break;
				}
			}
		}

		if (jsonAttribute.parsed_default_value != null) {
			checkJsonDefault(timestamp);
		}

		//TODO: check schema extensions 

		if (jsonAttribute.metainfo_unbound) {
			if (jsonAttribute.as_value && getNofComponents() == 1) {
				getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'metainfo for unbound' will be ignored, because "
						+ "the {0} is encoded without field names.",
						getTypetype().getName()));
			} else {
				for (int i = 0; i < getNofComponents(); i++) {
					final CompField cField = getComponentByIndex(i);
					final Type fieldType = cField.getType();

					if (fieldType.jsonAttribute == null) {
						fieldType.setJsonAttributes(new JsonAST());
					}

					fieldType.jsonAttribute.metainfo_unbound = true;
				}
			}
		}

		if (jsonAttribute.as_number) {
			getLocation().reportSemanticError("Invalid attribute, 'as number' is only allowed for enumerated types");
		}

		//FIXME: check tag_list

		if (!jsonAttribute.enum_texts.isEmpty()) {
			getLocation().reportSemanticError("Invalid attribute, 'text ... as ...' requires an enumerated type");
		}
	}

	/**
	 * Check the raw coding attributes of TTCN3 record and set types.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * */
	protected void checkSetSeqRawCodingAttributes(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		//check raw attributes
		if (rawAttribute != null) {
			if (rawAttribute.taglist != null) {
				for (int c = 0; c < rawAttribute.taglist.size(); c++) {
					final rawAST_single_tag singleTag = rawAttribute.taglist.get(c);
					final Identifier fieldname = singleTag.fieldName;
					if (!hasComponentWithName(fieldname.getName())) {
						fieldname.getLocation().reportSemanticError(MessageFormat.format("Invalid field name `{0}'' in RAW parameter TAG for type `{1}''", fieldname.getDisplayName(), getTypename()));
						continue;
					}

					if (singleTag.keyList != null) {
						for (int a = 0; a < singleTag.keyList.size(); a++) {
							final Reference reference = new Reference(null);
							reference.addSubReference(new FieldSubReference(fieldname));
							for (int b = 0; b < singleTag.keyList.get(a).keyField.names.size(); b++) {
								reference.addSubReference(new FieldSubReference(singleTag.keyList.get(a).keyField.names.get(b)));
							}

							final IType t = getFieldType(timestamp, reference, 0, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
							if (t != null) {
								final Value v = singleTag.keyList.get(a).v_value;
								if (v != null) {
									v.setMyScope(getMyScope());
									v.setMyGovernor(t);
									final IValue tempValue = t.checkThisValueRef(timestamp, v);
									t.checkThisValue(timestamp, tempValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, false, false, false, false, false));
								}
							}
						}
					}
				}
			}
			if (rawAttribute.ext_bit_groups != null) {
				for (int a = 0; a < rawAttribute.ext_bit_groups.size(); a++) {
					final rawAST_ext_bit_group tempExtBitGroup = rawAttribute.ext_bit_groups.get(a);
					final Identifier fromIdentifier = tempExtBitGroup.from;
					final Identifier toIdentifier = tempExtBitGroup.to;
					boolean foundError = false;

					if (!hasComponentWithName(fromIdentifier.getName())) {
						fromIdentifier.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDEXTENSION_BIT_GROUP, fromIdentifier.getDisplayName(), getTypename()));
						foundError = true;
					}
					if (!hasComponentWithName(toIdentifier.getName())) {
						toIdentifier.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDEXTENSION_BIT_GROUP, toIdentifier.getDisplayName(), getTypename()));
						foundError = true;
					}
					if (!foundError) {
						boolean foundStart = false;
						for (int i = 0; i < getNofComponents(); i++) {
							final Identifier tempId = getComponentByIndex(i).getIdentifier();
							if (tempId.equals(fromIdentifier)) {
								foundStart = true;
							} else if (tempId.equals(toIdentifier)) {
								if (!foundStart) {
									getLocation().reportSemanticError(MessageFormat.format("Invalid field order in RAW parameter EXTENSION_BIT_GROUP for type `{0}'': `{1}'', `{2}''", getTypename(), fromIdentifier.getDisplayName(), toIdentifier.getDisplayName()));
								}
								break;
							}
						}
					}
				}
			}
			if (rawAttribute.paddall != RawAST_Base.XDEFDEFAULT) {
				for (int i = 0; i < getNofComponents(); i++) {
					final CompField cField = getComponentByIndex(i);
					final Type fieldType = cField.getType();
					RawAST fieldRawAttribute = fieldType.rawAttribute;
					if (fieldRawAttribute == null) {
						IType t = fieldType;
						if (t instanceof Referenced_Type) {
							final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
							t = ((Referenced_Type)t).getTypeRefd(timestamp, referenceChain);
							referenceChain.release();
						}
						while (t.getRawAttribute() == null && t instanceof Referenced_Type) {
							final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
							t = ((Referenced_Type)t).getTypeRefd(timestamp, referenceChain);
							referenceChain.release();
						}
						fieldRawAttribute = new RawAST(t.getRawAttribute(), fieldType.getDefaultRawFieldLength());
						fieldType.setRawAttributes(fieldRawAttribute);
					}
					if (fieldRawAttribute.padding == 0) {
						fieldRawAttribute.padding = rawAttribute.padding;
					}
					if (fieldRawAttribute.prepadding == 0) {
						fieldRawAttribute.prepadding = rawAttribute.prepadding;
					}
					if (fieldRawAttribute.padding_pattern_length == 0 && rawAttribute.padding_pattern_length > 0) {
						fieldRawAttribute.padding_pattern = rawAttribute.padding_pattern;
						fieldRawAttribute.padding_pattern_length = rawAttribute.padding_pattern_length;
					}
				}
			}
			if (rawAttribute.fieldorder != RawAST_Base.XDEFDEFAULT || rawAttribute.csn1lh) { // FIELDORDER or CSN.1 L/H
				for (int i = 0; i < getNofComponents(); i++) {
					final CompField cField = getComponentByIndex(i);
					final Type fieldType = cField.getType();
					RawAST fieldRawAttribute = fieldType.rawAttribute;
					if (fieldRawAttribute == null) {
						Type t = fieldType;
						while ( t.rawAttribute == null && t instanceof IReferencingType) {
							final IReferenceChain tempRefChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
							t = (Type)((IReferencingType)t).getTypeRefd(timestamp, tempRefChain);
							tempRefChain.release();
						}
						fieldRawAttribute = new RawAST(t.rawAttribute, fieldType.getDefaultRawFieldLength());
						fieldType.setRawAttributes(fieldRawAttribute);
					}
					if (rawAttribute.fieldorder != RawAST_Base.XDEFDEFAULT &&
						fieldRawAttribute.fieldorder == RawAST_Base.XDEFDEFAULT) {
						fieldRawAttribute.fieldorder = rawAttribute.fieldorder;
					}
					if (rawAttribute.csn1lh) {
						fieldRawAttribute.csn1lh = true;
					}
				}
			}
		}
		if (ownerType != TypeOwner_type.OT_COMP_FIELD) {
			if (rawAttribute != null && rawAttribute.presence != null && rawAttribute.presence.keyList != null) {
				for (int a = 0; a < rawAttribute.presence.keyList.size(); a++) {
					final rawAST_tag_field_value tempTagFieldValue = rawAttribute.presence.keyList.get(a);
					final Reference reference = new Reference(null);
					reference.addSubReference(new FieldSubReference(tempTagFieldValue.keyField.names.get(0)));
					for (int b = 1; b < tempTagFieldValue.keyField.names.size(); b++) {
						reference.addSubReference(new FieldSubReference(tempTagFieldValue.keyField.names.get(b)));
					}

					final IType t = getFieldType(timestamp, reference, 0, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
					if (t != null) {
						final Value v = tempTagFieldValue.v_value;
						if (v != null) {
							v.setMyScope(getMyScope());
							v.setMyGovernor(t);
							final IValue tempValue = t.checkThisValueRef(timestamp, v);
							t.checkThisValue(timestamp, tempValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, false, false, false, false, false));
						}
					}
				}
			}
		}
		int usedBits = 0; // number of bits used to store all previous fields
		for (int i = 0; i < getNofComponents(); i++) {
			final CompField cField = getComponentByIndex(i);
			final Type fieldType = cField.getType();
			fieldType.forceRaw(timestamp);
			final RawAST rawPar = fieldType.rawAttribute;
			if (rawPar != null) {
				final Identifier fieldId = cField.getIdentifier();
				final IType fieldTypeLast = fieldType.getTypeRefdLast(timestamp);
				if (rawPar.prepadding != 0) {
					usedBits = (usedBits + rawPar.prepadding - 1) / rawPar.prepadding * rawPar.prepadding;
				}
				if (rawPar.intX && fieldTypeLast.getTypetype() == Type_type.TYPE_INTEGER) {
					if (usedBits % 8 != 0 && (rawAttribute == null || rawAttribute.fieldorder != RawAST_Base.XDEFMSB)) {
						getLocation().reportSemanticError(MessageFormat.format("Using RAW parameter IntX in a record/set with FIELDORDER set to 'lsb' is only supported if the IntX field starts at the beginning of a new octet. There are {0} unused bits in the last octet before field {1}.", 8 - (usedBits % 8), fieldId.getDisplayName()));
					}
				} else if (rawPar.fieldlength > 0) {
					usedBits += rawPar.fieldlength;
				}
				if (rawPar.padding != 0) {
					usedBits = (usedBits + rawPar.padding - 1) / rawPar.padding * rawPar.padding;
				}
				if (rawPar.lengthto != null) {
					for (int j = 0; j < rawPar.lengthto.size(); j++) {
						final Identifier id = rawPar.lengthto.get(j);
						if (!hasComponentWithName(id.getName())) {
							id.getLocation().reportSemanticError(MessageFormat.format("Invalid field name in RAW parameter LENGTHTO for field {0}: {1}", fieldId.getDisplayName(), id.getDisplayName()));
						}
					}
				}
				if (rawPar.lengthto != null) {
					IType ft = fieldType;
					switch (ft.getTypeRefdLast(timestamp).getTypetype()) {
					case TYPE_INTEGER:
					case TYPE_INTEGER_A:
						break;
					case TYPE_BITSTRING:
					case TYPE_BITSTRING_A:
					case TYPE_OCTETSTRING: {
						boolean isOk = false;
						while (!isOk) {
							if (rawPar.fieldlength > 0 ||
								(ft.getSubtype() != null && ft.getSubtype().get_length_restriction() > 0)) {
								isOk = true;
								break;
							} else if (ft.isReference()) {
								final IReferenceChain tempRefChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
								ft = ((IReferencingType)ft).getTypeRefd(timestamp, tempRefChain);
								tempRefChain.release();
							} else {
								break;
							}
						}
						if (!isOk) {
							fieldId.getLocation().reportSemanticError(LENGTHTO_ERROR_MISSING_RESTRICTION);
						}
						}
						break;
					case TYPE_TTCN3_CHOICE: {
						final TTCN3_Choice_Type choiceType = (TTCN3_Choice_Type)fieldTypeLast;
						for (int fi = 0; fi < choiceType.getNofComponents(); fi++) {
							Type compType = choiceType.getComponentByIndex(fi).getType();
							final Identifier compFieldId = choiceType.getComponentByIndex(fi).getIdentifier();
							switch (compType.getTypeRefdLast(timestamp).getTypetype()) {
							case TYPE_INTEGER:
							case TYPE_INTEGER_A:
								break;
							case TYPE_BITSTRING:
							case TYPE_BITSTRING_A:
							case TYPE_OCTETSTRING: {
								boolean isOk = false;
								while (!isOk) {
									if ((compType.rawAttribute != null && compType.rawAttribute.fieldlength > 0) ||
										(choiceType.getSubtype() != null && choiceType.getSubtype().get_length_restriction() > 0)) {
										isOk = true;
										break;
									} else if (compType.isReference()) {
										final IReferenceChain tempRefChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
										compType = (Type)((IReferencingType)compType).getTypeRefd(timestamp, tempRefChain);
										tempRefChain.release();
									} else {
										break;
									}
								}
								if (!isOk) {
									compFieldId.getLocation().reportSemanticError(LENGTHTO_ERROR_MISSING_RESTRICTION);
								}
								}
								break;
							default:
								compFieldId.getLocation().reportSemanticError(LENGTHTO_ERROR_WRONG_TYPE);
								break;
							}
						}
						}
						break;
					case TYPE_ASN1_CHOICE: {
						final ASN1_Choice_Type choiceType = (ASN1_Choice_Type)fieldTypeLast;
						for (int fi = 0; fi < choiceType.getNofComponents(); fi++) {
							Type compType = choiceType.getComponentByIndex(fi).getType();
							final Identifier compFieldId = choiceType.getComponentByIndex(fi).getIdentifier();
							switch (compType.getTypeRefdLast(timestamp).getTypetype()) {
							case TYPE_INTEGER:
							case TYPE_INTEGER_A:
								break;
							case TYPE_BITSTRING:
							case TYPE_BITSTRING_A:
							case TYPE_OCTETSTRING: {
								boolean isOk = false;
								while (!isOk) {
									if ((compType.rawAttribute != null && compType.rawAttribute.fieldlength > 0) ||
										(choiceType.getSubtype() != null && choiceType.getSubtype().get_length_restriction() > 0)) {
										isOk = true;
										break;
									} else if (compType.isReference()) {
										final IReferenceChain tempRefChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
										compType = (Type)((IReferencingType)compType).getTypeRefd(timestamp, tempRefChain);
										tempRefChain.release();
									} else {
										break;
									}
								}
								if (!isOk) {
									compFieldId.getLocation().reportSemanticError(LENGTHTO_ERROR_MISSING_RESTRICTION);
								}
								}
								break;
							default:
								compFieldId.getLocation().reportSemanticError(LENGTHTO_ERROR_WRONG_TYPE);
								break;
							}
						}
						}
						break;
					case TYPE_ANYTYPE:
					case TYPE_OPENTYPE:
					case TYPE_TTCN3_SEQUENCE:
					case TYPE_ASN1_SEQUENCE:
					case TYPE_TTCN3_SET:
					case TYPE_ASN1_SET:
						if (rawPar.lengthindex != null) {
							// will be checked in the next step
							break;
						}
						// fallthrough
					default:
						fieldId.getLocation().reportSemanticError(MessageFormat.format("The LENGTHTO field must be an integer, bitstring, "
								+ "octetstring or union type instead of `{0}''", fieldTypeLast.getTypename()));
						break;
					}
				}
				if (rawPar.lengthto != null && rawPar.lengthindex != null) {
					final Identifier id = rawPar.lengthindex.names.get(0);
					switch (fieldTypeLast.getTypetype()) {
					case TYPE_TTCN3_CHOICE:
					case TYPE_TTCN3_SEQUENCE:
					case TYPE_TTCN3_SET:
						if(!((TTCN3_Set_Seq_Choice_BaseType)fieldTypeLast).hasComponentWithName(id.getName())) {
							id.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDLENGTHINDEX, fieldId.getDisplayName(), id.getDisplayName()));
						}
						break;
					case TYPE_ASN1_CHOICE:
					case TYPE_ASN1_SEQUENCE:
					case TYPE_ASN1_SET:
						if(!((ASN1_Set_Seq_Choice_BaseType)fieldTypeLast).hasComponentWithName(id)) {
							id.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDLENGTHINDEX, fieldId.getDisplayName(), id.getDisplayName()));
						}
						break;
					default:
						fieldId.getLocation().reportSemanticError(MessageFormat.format("Invalid fieldmember type in RAW parameter LENGTHINDEX for field {0}.", fieldId.getDisplayName()));
						break;
					}
				}
				if (rawPar.pointerto != null) {
					final Identifier id = rawPar.pointerto;
					boolean errorFound = false;
					int pointed = 0;
					if (!hasComponentWithName(id.getName())) {
						id.getLocation().reportSemanticError(MessageFormat.format("Invalid field name in RAW parameter POINTERTO for field {0}: {1}", fieldId.getDisplayName(), id.getDisplayName()));
						errorFound = true;
					}
					if (!errorFound) {
						pointed = getComponentIndexByName(id);
						if (pointed <= i) {
							id.getLocation().reportSemanticError(MessageFormat.format("Pointer must precede the pointed field. Incorrect field name `{0}'' in RAW parameter POINTERTO for field `{1}''", id.getDisplayName(), fieldId.getDisplayName()));
							errorFound = true;
						}
					}
					if (!errorFound && rawPar.ptrbase != null) {
						final Identifier idf2 = rawPar.ptrbase;
						if (!hasComponentWithName(idf2.getName())) {
							idf2.getLocation().reportSemanticError(MessageFormat.format("Invalid field name `{0}'' in RAW parameter PTROFFSET for field `{1}''", idf2.getDisplayName(), fieldId.getDisplayName()));
							errorFound = true;
						}
						if (!errorFound && getComponentIndexByName(idf2) > pointed) {
							idf2.getLocation().reportSemanticError(MessageFormat.format("Pointer base must precede the pointed field. Incorrect field name `{0}'' in RAW parameter PTROFFSET for field `{1}''", idf2.getDisplayName(), fieldId.getDisplayName()));
						}
					}
				}
				if (rawPar.presence != null && rawPar.presence.keyList != null) {
					for (int a = 0; a < rawPar.presence.keyList.size(); a++) {
						final rawAST_tag_field_value tempTagFieldValue = rawPar.presence.keyList.get(a);
						final Reference reference = new Reference(null);
						reference.addSubReference(new FieldSubReference(tempTagFieldValue.keyField.names.get(0)));
						for (int b = 1; b < tempTagFieldValue.keyField.names.size(); b++) {
							reference.addSubReference(new FieldSubReference(tempTagFieldValue.keyField.names.get(b)));
						}

						final IType t = getFieldType(timestamp, reference, 0, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
						if (t != null) {
							final Value v = tempTagFieldValue.v_value;
							if (v != null) {
								v.setMyScope(getMyScope());
								v.setMyGovernor(t);
								final IValue tempValue = t.checkThisValueRef(timestamp, v);
								t.checkThisValue(timestamp, tempValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, false, false, false, false, false));
							}
						}
					}
				}
				if (rawPar.forceOmit != null) {
					for (int j = 0; j < rawPar.forceOmit.lists.size(); j++) {
						final rawAST_field_list fieldList = rawPar.forceOmit.lists.get(j);
						IType t = fieldTypeLast;
						boolean erroneous = false;
						boolean isOptional = false;
						for (int k = 0; k < fieldList.names.size(); k++) {
							final Identifier idf = fieldList.names.get(k);
							t = t.getTypeRefdLast(timestamp);
							switch (t.getTypetype()) {
							case TYPE_TTCN3_CHOICE:
							case TYPE_TTCN3_SEQUENCE:
							case TYPE_TTCN3_SET:
								if(!((TTCN3_Set_Seq_Choice_BaseType)t).hasComponentWithName(idf.getName())) {
									idf.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDFORCEOMIT, idf.getDisplayName(), fieldId.getDisplayName()));
									erroneous = true;
								} else {
									final CompField cf = ((TTCN3_Set_Seq_Choice_BaseType)t).getComponentByName(idf.getName());
									t = cf.getType();
									isOptional = cf.isOptional();
								}
								break;
							case TYPE_ASN1_CHOICE:
							case TYPE_ASN1_SEQUENCE:
							case TYPE_ASN1_SET:
								if(!((ASN1_Set_Seq_Choice_BaseType)t).hasComponentWithName(idf)) {
									idf.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDFORCEOMIT, idf.getDisplayName(), fieldId.getDisplayName()));
									erroneous = true;
								} else {
									final CompField cf = ((ASN1_Set_Seq_Choice_BaseType)t).getComponentByName(idf);
									t = cf.getType();
									isOptional = cf.isOptional();
								}
								break;
							default:
								idf.getLocation().reportSemanticError(MessageFormat.format("Invalid field type in RAW parameter FORCEOMIT for field {0}.", fieldId.getDisplayName()));
								erroneous = true;
								break;
							}
						}
						if (!erroneous && !isOptional) {
							fieldId.getLocation().reportSemanticError(MessageFormat.format("RAW parameter FORCEOMIT for field {0} does not refer to an optional field.", fieldId.getDisplayName()));
						}
					}
				}
				if (rawPar.crosstaglist != null) {
					for (int c = 0; c < rawPar.crosstaglist.size(); c++) {
						final rawAST_single_tag singleTag = rawPar.crosstaglist.get(c);
						final Identifier idf = singleTag.fieldName;
						boolean errorFound = false;

						switch (fieldTypeLast.getTypetype()) {
						case TYPE_TTCN3_CHOICE:
						case TYPE_TTCN3_SEQUENCE:
						case TYPE_TTCN3_SET:
							if (idf == null) {
								getLocation().reportSemanticError(OMITCROSSTAG);
								errorFound = true;
							} else if(!((TTCN3_Set_Seq_Choice_BaseType)fieldTypeLast).hasComponentWithName(idf.getName())) {
								idf.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDCROSSTAG, fieldId.getDisplayName(), idf.getDisplayName()));
								errorFound = true;
							}
							break;
						case TYPE_ASN1_CHOICE:
						case TYPE_ASN1_SEQUENCE:
						case TYPE_ASN1_SET:
							if (idf == null) {
								getLocation().reportSemanticError(OMITCROSSTAG);
								errorFound = true;
							} else if(!((ASN1_Set_Seq_Choice_BaseType)fieldTypeLast).hasComponentWithName(idf)) {
								idf.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDCROSSTAG, fieldId.getDisplayName(), idf.getDisplayName()));
								errorFound = true;
							}
							break;
						default:
							fieldId.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDTYPECROSSTAG, fieldId.getDisplayName()));
							errorFound = true;
							break;
						}


						if (!errorFound && singleTag.keyList != null) {
							for (int a = 0; a < singleTag.keyList.size(); a++) {
								IType t2 = this;
								boolean errorFound2 = false;
								boolean allow_omit = false;
								final rawAST_tag_field_value tagField = singleTag.keyList.get(a);
								for (int b = 0; b < tagField.keyField.names.size() && !errorFound2; b++) {
									final Identifier idf2 = tagField.keyField.names.get(b);
									CompField cf2 = null;
									switch (t2.getTypetype()) {
									case TYPE_TTCN3_CHOICE:
									case TYPE_TTCN3_SEQUENCE:
									case TYPE_TTCN3_SET:
										if(!((TTCN3_Set_Seq_Choice_BaseType)t2).hasComponentWithName(idf2.getName())) {
											idf2.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDCROSSTAG, fieldId.getDisplayName(), idf2.getDisplayName()));
											errorFound2 = true;
										} else {
											cf2 = ((TTCN3_Set_Seq_Choice_BaseType)t2).getComponentByName(idf2.getName());
										}
										break;
									case TYPE_ASN1_CHOICE:
									case TYPE_ASN1_SEQUENCE:
									case TYPE_ASN1_SET:
										if(!((ASN1_Set_Seq_Choice_BaseType)t2).hasComponentWithName(idf2)) {
											idf2.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDCROSSTAG, fieldId.getDisplayName(), idf2.getDisplayName()));
											errorFound2 = true;
										} else {
											cf2 = ((ASN1_Set_Seq_Choice_BaseType)t2).getComponentByName(idf2);
										}
										break;
									default:
										fieldId.getLocation().reportSemanticError(MessageFormat.format(INVALIDFIELDTYPECROSSTAG, fieldId.getDisplayName()));
										errorFound2 = true;
										break;
									}
									if (b == 0) {
										final int fieldIndex = getComponentIndexByName(idf2);
										if (fieldIndex == i) {
											idf2.getLocation().reportSemanticError(MessageFormat.format("RAW parameter CROSSTAG for field `{0}'' cannot refer to the field itself", idf2.getDisplayName()));
											errorFound2 = true;
										} else if (fieldIndex > i) {
											if (cField.isOptional()) {//TODO || fieldType.getRawLength() < 0
												idf2.getLocation().reportSemanticError(MessageFormat.format("Field `{0}'' that CROSSTAG refers to must precede field `{1}'' or field `{1}'' must be mandatory with fixed length", idf2.getDisplayName(), fieldId.getDisplayName()));
												errorFound2 = true;
											}
										}
									}
									if (!errorFound2) {
										t2 = cf2.getType().getTypeRefdLast(timestamp);
										if (b == tagField.keyField.names.size() - 1 && cf2.isOptional()) {
											allow_omit = true;
										}
									}
								}
								if (!errorFound2) {
									final Value v = singleTag.keyList.get(a).v_value;
									if (v != null) {
										v.setMyScope(getMyScope());
										v.setMyGovernor(t2);
										final IValue tempValue = t2.checkThisValueRef(timestamp, v);
										t2.checkThisValue(timestamp, tempValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, false, allow_omit, false, false, false));
									}
								}
							}
						}
					}
				}
			}
		}
	}


	@Override
	public void addMembersContent(Ttcn3HoverContent content) {
		if (getNofComponents() > 0) {
			content.addTag("Members").addText("\n\n");
			for (int i = 0; i < getNofComponents(); i++) {
				final CompField field = getComponentByIndex(i);
				final StringBuilder sb = new StringBuilder(field.getIdentifier().getDisplayName());
				final IType memberType = field.getType();
				if (memberType != null) {
					String memberTypeName = memberType.getTypename();
					if (memberType instanceof Referenced_Type) {
						memberTypeName = ((Referenced_Type)memberType).getReference().getDisplayName();
					}
					sb.append(" (").append(memberTypeName).append(")");
				}

				sb.append(" ").append(field.isOptional() ? "opt" : "");
				content.addText("- ").addIndentedText(sb.toString(), "\n");
			}
		}
	}
}
