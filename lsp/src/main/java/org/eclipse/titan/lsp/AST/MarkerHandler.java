/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.io.File;
import java.nio.file.Path;

import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;

/**
 *  @author Miklos Magyari
 **/
public final class MarkerHandler {
	private MarkerHandler() {
		// Hide constructor
	}

	public static void removeMarkers(final Path resource) {
		Project.INSTANCE.removeMarkers(resource);
	}
	
	public static void removeMarkers(final Location location) {
		removeMarkers(location, false);
	}
	
	public static void removeMarkers(final Location location, final boolean semanticOnly) {
		if (location != null && location != NULL_Location.INSTANCE) {
			Project.INSTANCE.removeMarkers(location.getFile(), location.getStartPosition(), location.getEndPosition(), semanticOnly);
		}
	}

	public static void removeMarkers(final File resource) {
		Project.INSTANCE.removeMarkers(resource);
	}
	
	/**
	 * Removes all markers from this node.
	 * @param locatable
	 */
	public static void removeMarkers(final ILocateableNode locatable) {
		removeMarkers(locatable, false);
	}
	
	/**
	 * Removes markers from this node. If semanticOnly is true, syntactic markers are kept.
	 * @param locatable
	 * @param sematicOnly
	 */
	public static void removeMarkers(final ILocateableNode locatable, final boolean semanticOnly) {
		if(locatable == null) {
			return;
		}

		final Location location = locatable.getLocation();
		if (location != null && location != NULL_Location.INSTANCE) {
			Project.INSTANCE.removeMarkers(location.getFile(),location.getStartPosition(), location.getEndPosition(), semanticOnly);
		}
	}
	
	public static void removeMarkers(final File file, final Position start, final Position end) {
		Project.INSTANCE.removeMarkers(file, start, end);
	}
	
	public static void restoreMarkers(final Location location) {
		if (location != null && location != NULL_Location.INSTANCE) {
			Project.INSTANCE.restoreMarkers(location.getFile());
		}
	}
	
	public static void removeMarkersAndSemanticHighlighting(final Location location) {
		removeMarkers(location.getFile(), location.getStartPosition(), location.getEndPosition());
		AstSemanticHighlighting.clearSemanticTokensAndModifiers(location.getFile().toPath(), location.getStartPosition(), location.getEndPosition());
	}
}

