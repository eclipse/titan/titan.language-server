/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ErroneousAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

public class Update_Statement extends Statement {

	private static final String STATEMENT_NAME = "@update";
	private static final String FULLNAMEPART1 = ".ref";
	private static final String FULLNAMEPART2 = ".<attribpath>";

	private Reference ref;
	private MultipleWithAttributes attr;

	public Update_Statement(final Reference reference, final MultipleWithAttributes attributes) {
		if (reference != null) {
			this.ref = reference;
		}

		if (attributes != null) {
			this.attr = attributes;
		}
	}

	@Override
	public Statement_type getType() {
		return Statement_type.S_UPDATE;
	}

	@Override
	public String getStatementName() {
		return STATEMENT_NAME;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (ref == child) {
			return builder.append(FULLNAMEPART1);
		}
		builder.append(FULLNAMEPART2);

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (ref != null) {
			ref.setMyScope(scope);
		}
	}

	@Override
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (myStatementBlock.isInDynamicTemplate()) {
			getLocation().reportSemanticError("The @update statement is not allowed in the statement block of a dynamic template");
			return;
		}
		
		if (myStatementBlock.isInGetterScope()) {
			getLocation().reportSemanticError("The @update statement is not allowed in the statement block of a property's getter");
			return;
		}
		
		if (myStatementBlock.isInSetterScope()) {
			getLocation().reportSemanticError("The @update statement is not allowed in the statement block of a property's setter");
			return;
		}
		
		//TODO: check runtime later
		final Assignment refd_ass = ref.getRefdAssignment(timestamp, false);
		switch (refd_ass.getAssignmentType()) {
		case A_CONST:
		case A_TEMPLATE:
			break; //OK
		default:
			this.getLocation().reportSemanticError(MessageFormat.format("Reference to constant or template definition was expected instead of {0}", refd_ass.getAssignmentName()));
			return;
		}

		final IType refd_type = refd_ass.getType(timestamp);
		if (refd_type != null && refd_type.getIsErroneous(timestamp)) {
			ref.getLocation().reportSemanticError(MessageFormat.format("Type `{0}'' cannot have erroneous attributes", refd_type.getTypename()));
		}
		if (attr != null) {
			//TODO check only erroneous
			final ErroneousAttributes err_attrib = Definition.checkErroneousAttributes(attr, refd_type, myScope, refd_type.getFullName(), false, timestamp, ref);
			if (err_attrib != null) {
				//TODO add error descriptor handling
			}
		}

		if (ref.getSubreferences() != null) {
			ref.getLocation().reportSemanticError("Field names and array indexes are not allowed in this context");
		}
	}

	@Override
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (ref != null) {
			ref.updateSyntax(reparser, false);
			reparser.updateLocation(ref.getLocation());
		}

		if (attr != null) {
			attr.updateSyntax(reparser, false);
			reparser.updateLocation(attr.getLocation());
		}
	}

	@Override
	protected boolean memberAccept(final ASTVisitor v) {
		if (ref != null && !ref.accept(v)) {
			return false;
		}

		if (attr != null && !attr.accept(v)) {
			return false;
		}

		return true;
	}
}
