/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a template that matches everything but the elements in a list.
 *
 * @author Kristof Szabados
 * */
public final class ComplementedList_Template extends CompositeTemplate {
	private static final String ANYOROMITWARNING = "`*'' in complemented list. This template will not match anything.";
	private static final String PRESENTRESTRICTIONERROR2 = PRESENTRESTRICTIONERROR + " without omit or AnyValueOrNone in the list";

	public ComplementedList_Template(final ListOfTemplates templates) {
		super(templates);
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.COMPLEMENTED_LIST;
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (myGovernor != null) {
			return myGovernor;
		}

		for (final TTCN3Template t : templates) {
			final IType type = t.getExpressionGovernor(timestamp, expectedValue);
			if (type != null) {
				return type;
			}
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (getIsErroneous(timestamp)) {
			return Type_type.TYPE_UNDEFINED;
		}

		for (final TTCN3Template t : templates) {
			final Type_type type = t.getExpressionReturntype(timestamp, expectedValue);
			if (!Type_type.TYPE_UNDEFINED.equals(type)) {
				return type;
			}
		}

		return Type_type.TYPE_UNDEFINED;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		for (final TTCN3Template t : templates) {
			if (t.checkExpressionSelfReferenceTemplate(timestamp, lhs)) {
				return true;
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {

		if (type == null) {
			return false;
		}

		final boolean allowOmitInValueList = TTCN3Template.allowOmitInValueList(getLocation(), allowOmit);

		boolean selfReference = false;
		for (final TTCN3Template component : templates) {
			component.setMyGovernor(type);
			final ITTCN3Template temporalComponent = type.checkThisTemplateRef(timestamp, component);
			selfReference |= temporalComponent.checkThisTemplateGeneric(timestamp, type, false,
					allowOmitInValueList, true, subCheck, implicitOmit, lhs);

			if (Template_type.ANY_OR_OMIT.equals(temporalComponent.getTemplatetype())) {
				component.getLocation().reportSemanticWarning(ANYOROMITWARNING);
			}
		}

		checkThisTemplateGenericPostChecks(timestamp, type, allowOmit, subCheck);
		return selfReference;
	}

	/**
	 * If ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY is switched on
	 * and has AnyOrOmit (=AnyOrNone) or omit in the list then accepted, otherwise not
	 */
	@Override
	public boolean checkPresentRestriction(final CompilationTimeStamp timestamp, final String definitionName, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, TemplateRestriction.Restriction_type.TR_PRESENT, usageLocation);

		final boolean allowOmitInValueList = TTCN3Template.allowOmitInValueList(getLocation(), true);
		if (allowOmitInValueList) {
			boolean hasAnyOrOmit = false;
			for (final TTCN3Template component : templates) {
				if (Template_type.SPECIFIC_VALUE.equals(component.getTemplatetype())) {
					final IValue value = ((SpecificValue_Template) component).getSpecificValue();
					if (Value_type.OMIT_VALUE.equals(value.getValuetype())) {
						hasAnyOrOmit = true;
						break;
					}
				}

				final Template_type componentType =  component.getTemplatetype();
				if (Template_type.ANY_OR_OMIT.equals(componentType)) {
					hasAnyOrOmit = true;
					break;
				}
			}
			if (!hasAnyOrOmit) {
				location.reportSemanticError(MessageFormat.format(PRESENTRESTRICTIONERROR2, definitionName, getTemplateTypeName()));
				return false;
			}
		}
		// some basic check was performed, always needs runtime check
		return true;
	}

	@Override
	/** {@inheritDoc} */
	protected String getNameForStringRep() {
		return COMPLEMENT;
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		return true;
	}
}
