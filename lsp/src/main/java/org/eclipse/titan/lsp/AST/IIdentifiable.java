/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

/**
 * Represents an AST element that has an identifier
 * @see Identifier
 * @author Adam Knapp
 * */
public interface IIdentifiable {
	/**
	 * Returns the identifier of this AST element
	 * @return the identifier of this AST element
	 */
	Identifier getIdentifier();
}
