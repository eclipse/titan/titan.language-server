/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.definitions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.Assignments;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ModuleImportation;
import org.eclipse.titan.lsp.AST.ModuleImportationChain;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;

/**
 * Import module.
 * Models an asn.1 module of the section IMPORTS in the parent asn1.module
 *
 * @author Kristof Szabados
 */
public final class ImportModule extends ModuleImportation {
	public static final String MISSINGMODULE = "There is no ASN.1 module with name `{0}''";
	private static final String NOTASN1MODULE = "The module referred by `{0}'' is not an ASN.1 module";
	private static final String SYMBOLNOTEXPORTED = "Symbol `{0}'' is not exported from module `{1}''";

	/** imported symbols FROM this module */
	private final Symbols symbols;
	private final SelectionOption selectionOption;

	public ImportModule(final Identifier identifier, final Symbols symbols) {
		this(identifier, symbols, SelectionOption.NONE);
	}

	public ImportModule(final Identifier identifier, final Symbols symbols, final SelectionOption selectionOption) {
		super(identifier);
		this.identifier = identifier;
		this.symbols = (null == symbols) ? new Symbols() : symbols;
		this.selectionOption = selectionOption;
	}

	@Override
	/** {@inheritDoc} */
	public String chainedDescription() {
		return (null != identifier) ? identifier.getDisplayName() : null;
	}

	/** @return the symbols to be imported from this imported module */
	public Symbols getSymbols() {
		return symbols;
	}

	/**
	 * Checks if a given symbol is imported through this importation.
	 *
	 * @param identifier
	 *                the identifier to search for.
	 *
	 * @return true if a symbol with this identifier is imported, false
	 *         otherwise.
	 * */
	public boolean hasSymbol(final Identifier identifier) {
		return symbols.hasSymbol(identifier.getName());
	}

	@Override
	/** {@inheritDoc} */
	public void checkImports(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain, final List<Module> moduleStack) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (null != lastImportCheckTimeStamp && !lastImportCheckTimeStamp.isLess(timestamp)) {
			return;
		}

		symbols.checkUniqueness(timestamp);

		final ProjectSourceParser parser = GlobalParser.getProjectSourceParser(Project.INSTANCE);
		if (null == parser || null == identifier) {
			lastImportCheckTimeStamp = timestamp;
			//FIXME: is it correct? lastImportCheckTimeStamp will be set in extreme case only - very early running
			referredModule = null;
			return;
		}

		final Module temp = referredModule;
		referredModule = parser.getModuleByName(identifier.getName());
		if (temp != referredModule) {
			setUnhandledChange(true);
		}
		if (referredModule == null) {
			identifier.getLocation().reportSemanticError(MessageFormat.format(MISSINGMODULE, identifier.getDisplayName()));
		} else {
			if (!(referredModule instanceof ASN1Module)) {
				identifier.getLocation().reportSemanticError(MessageFormat.format(NOTASN1MODULE, identifier.getDisplayName()));
				lastImportCheckTimeStamp = timestamp;
				referredModule = null;
				return;
			}

			moduleStack.add(referredModule);
			if (!referenceChain.add(this)) {
				moduleStack.remove(moduleStack.size() - 1);
				lastImportCheckTimeStamp = timestamp;
				return;
			}

			referredModule.checkImports(timestamp, referenceChain, moduleStack);

			for (Identifier id : symbols.getSymbols()) {
				if (((ASN1Module) referredModule).getAssignments().hasAssignmentWithId(timestamp, id)
						|| referredModule.hasImportedAssignmentWithID(timestamp, id)) {
					if (!((ASN1Module) referredModule).exportsSymbol(timestamp, id)) {
						identifier.getLocation().reportSemanticError(
								MessageFormat.format(SYMBOLNOTEXPORTED, id.getDisplayName(), referredModule
										.getIdentifier().getDisplayName()));
					}
				} else {
					id.getLocation().reportSemanticError(
							MessageFormat.format(ASN1Module.NOASSIGNMENTORSYMBOL, id.getDisplayName(), identifier.getDisplayName()));
				}
			}

			moduleStack.remove(moduleStack.size() - 1);
		}

		lastImportCheckTimeStamp = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastImportCheckTimeStamp != null && !lastImportCheckTimeStamp.isLess(timestamp)) {
			return;
		}

		usedForImportation = false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasImportedAssignmentWithID(final CompilationTimeStamp timestamp, final Identifier identifier) {
		if (referredModule == null) {
			return false;
		}

		final Assignments assignments = referredModule.getAssignments();
		if (assignments != null && assignments.hasLocalAssignmentWithID(timestamp, identifier)) {
			return true;
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment importAssignment(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain,
			final Identifier moduleId, final Reference reference, final List<ModuleImportation> usedImports) {
		// referenceChain is not used since this can only be the
		// endpoint of an importation chain.
		if (referredModule == null) {
			return null;
		}

		final Assignment result = referredModule.importAssignment(timestamp, moduleId, reference);
		if (result != null) {
			usedImports.add(this);
			setUsedForImportation();
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		if (identifier != null && !identifier.accept(v)) {
			return false;
		}
		if (symbols != null && !symbols.accept(v)) {
			return false;
		}
		if (v.leave(this) == ASTVisitor.V_ABORT) {
			return false;
		}
		return true;
	}
}
