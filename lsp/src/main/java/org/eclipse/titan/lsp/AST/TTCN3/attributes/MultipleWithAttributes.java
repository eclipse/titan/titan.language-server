/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.IIdentifierContainer;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class MultipleWithAttributes implements ILocateableNode, IIncrementallyUpdatable,
	IIdentifierContainer, IVisitableNode, ICollection<SingleWithAttribute> {

	private final List<SingleWithAttribute> attributes = new ArrayList<SingleWithAttribute>(1);

	/**
	 * The location of the whole attributes. This location encloses the
	 * attributes fully, as it is used to report errors to.
	 **/
	private Location location = NULL_Location.INSTANCE;

	@Override
	public boolean add(final SingleWithAttribute attribute) {
		if (attribute != null) {
			return attributes.add(attribute);
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	public int size() {
		return attributes.size();
	}

	@Override
	public SingleWithAttribute get(final int index) {
		return attributes.get(index);
	}

	/**
	 * Handles the incremental parsing of this attribute set.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged {@code true} if the location contains the damaged area,
	 *                {@code false} if only its' location needs to be updated.
	 */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (int i = 0, size = attributes.size(); i < size; i++) {
			final SingleWithAttribute temp = attributes.get(i);
			temp.updateSyntax(reparser, false);
			reparser.updateLocation(temp.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final SingleWithAttribute attr : attributes) {
			attr.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final SingleWithAttribute attr : attributes) {
			if (!attr.accept(v)) {
				return false;
			}
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}

	@Override
	public Iterator<SingleWithAttribute> iterator() {
		return attributes.iterator();
	}
}
