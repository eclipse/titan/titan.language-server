/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.utils;

import java.util.Iterator;

/**
 * Helper class for creating the proper iterator for {@linkplain Iterable} objects
 * 
 * @author Adam Knapp
 * */
public final class IteratorFactory {

	private IteratorFactory() {
		// Do nothing
	}

	/**
	 * Returns an iterator that does not support the {@link Iterator#remove()} operator
	 * @param <E> Base type of the Iterable object
	 * @param iterable Iterable object, usually a collection containing a type of objects
	 * 				   extending the specified base type 
	 * @return Iterator for the specified base type
	 */
	public static <E> Iterator<E> getIterator(Iterable<? extends E> iterable) {
		return new Iterator<E>() {
			Iterator<? extends E> it = iterable.iterator();

			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public E next() {
				return it.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}
}
