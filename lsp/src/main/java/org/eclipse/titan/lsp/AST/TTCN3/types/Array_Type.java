/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferenceableElement;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TypeCompatibilityInfo;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Sequence_Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template.Template_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.IndexedTemplate;
import org.eclipse.titan.lsp.AST.TTCN3.templates.Indexed_Template_List;
import org.eclipse.titan.lsp.AST.TTCN3.templates.PermutationMatch_Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.Template_List;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.SubType;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimension;
import org.eclipse.titan.lsp.AST.TTCN3.values.Array_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ExpressionStruct;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * @author Gergo Ujhelyi
 * */
public final class Array_Type extends Type implements IReferenceableElement {
	private static final String ARRAYVALUEEXPECTED = "Array value was expected";
	private static final String TOOMANYEXPECTED = "Too many elements in the array value: {0} was expected instead of {1}";
	private static final String TOOFEWEXPECTED = "Too few elements in the array value: {0} was expected instead of {1}";
	private static final String TEMPLATENOTALLOWED = "{0} cannot be used for type `{1}''";
	private static final String REDUNDANTLENGTHRESTRICTION = "Redundant usage of length restriction with `omit''";
	private static final String TOOMANYTEMPLATEELEMENTS = "Too many elements in the array template: {0} was expected instead of {1}";
	private static final String TOOFEWTEMPLATEELEMENTS = "Too few elements in the array template: {0} was expected instead of {1}";
	private static final String NOTUSEDNOTALLOWED = "Not used symbol `-'' is not allowed in this context";

	private static final String FULLNAMEPART1 = ".<elementType>";
	private static final String FULLNAMEPART2 = ".<dimension>";

	private static final String BADARRAYDIMENSION = "Array types should have the same dimension";
	private static final String NOFFIELDSDONTMATCH =
			"The size of the array ({0}) must be >= than the number of mandatory fields in the record/SEQUENCE type ({1})";
	private static final String NOTCOMPATIBLESETSETOF = "set/SET and set of/SET OF types are compatible only with other set/SET and set of/SET OF types";
	private static final String NOTCOMPATIBLEUNIONANYTYPE = "union/CHOICE/anytype types are compatible only with other union/CHOICE/anytype types";

	private final Type elementType;
	private final ArrayDimension dimension;
	// used only in code generation
	private final boolean inTypeDefinition;

	private boolean componentInternal;

	private BuildTimestamp lastBuildTimestamp;
	private String lastGenName;

	private boolean insideCanHaveCoding = false;

	public Array_Type(final Type elementType, final ArrayDimension dimension, final boolean inTypeDefinition) {
		this.elementType = elementType;
		this.dimension = dimension;
		this.inTypeDefinition = inTypeDefinition;
		componentInternal = false;

		if (elementType != null) {
			elementType.setOwnertype(TypeOwner_type.OT_ARRAY, this);
			elementType.setFullNameParent(this);
		}
		if (dimension != null) {
			dimension.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetype() {
		return Type_type.TYPE_ARRAY;
	}

	public ArrayDimension getDimension() {
		return dimension;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (elementType == child) {
			return builder.append(FULLNAMEPART1);
		} else if (dimension == child) {
			return builder.append(FULLNAMEPART2);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (elementType != null) {
			elementType.setMyScope(scope);
		}
		if (dimension != null) {
			dimension.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isCompatible(final CompilationTimeStamp timestamp, final IType otherType, final TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain) {
		check(timestamp);
		otherType.check(timestamp);
		final IType temp = otherType.getTypeRefdLast(timestamp);

		if (getIsErroneous(timestamp) || temp.getIsErroneous(timestamp) || this == temp) {
			return true;
		}

		if (info == null || noStructuredTypeCompatibility) {
			return this == temp;
		}

		switch (temp.getTypetype()) {
		case TYPE_ASN1_SEQUENCE: {
			final ASN1_Sequence_Type tempType = (ASN1_Sequence_Type) temp;
			final int tempTypeNofComps = tempType.getNofComponents();
			if (tempTypeNofComps == 0) {
				return false;
			}

			int nofOptionalFields = 0;
			for (int i = 0; i < tempTypeNofComps; i++) {
				final CompField tempTypeCf = tempType.getComponentByIndex(i);
				if (tempTypeCf.isOptional()) {
					nofOptionalFields++;
				}
			}
			final long nofComps = getDimension().getSize();
			if (nofComps < tempTypeNofComps - nofComps) {
				info.setErrorStr(MessageFormat.format(NOFFIELDSDONTMATCH, nofComps, tempTypeNofComps - nofOptionalFields));
				return false;
			}
			TypeCompatibilityInfo.Chain lChain = leftChain;
			TypeCompatibilityInfo.Chain rChain = rightChain;
			if (lChain == null) {
				lChain = info.getChain();
				lChain.add(this);
			}
			if (rChain == null) {
				rChain = info.getChain();
				rChain.add(tempType);
			}
			for (int i = 0; i < tempTypeNofComps; i++) {
				final CompField tempTypeCf = tempType.getComponentByIndex(i);
				final IType tempTypeCfType = tempTypeCf.getType().getTypeRefdLast(timestamp);
				final IType tempElementType = getElementType().getTypeRefdLast(timestamp);
				lChain.markState();
				rChain.markState();
				lChain.add(tempElementType);
				rChain.add(tempTypeCfType);
				final TypeCompatibilityInfo infoTemp = new TypeCompatibilityInfo(tempElementType, tempTypeCfType, false);
				if (!tempElementType.equals(tempTypeCfType)
						&& !(lChain.hasRecursion() && rChain.hasRecursion())
						&& !tempElementType.isCompatible(timestamp, tempTypeCfType, infoTemp, lChain, rChain)) {
					info.appendOp1Ref(infoTemp.getOp1RefStr());
					info.appendOp2Ref(DOT + tempTypeCf.getIdentifier().getDisplayName() + infoTemp.getOp2RefStr());
					info.setOp1Type(infoTemp.getOp1Type());
					info.setOp2Type(infoTemp.getOp2Type());
					info.setErrorStr(infoTemp.getErrorStr());
					lChain.previousState();
					rChain.previousState();
					return false;
				}
				lChain.previousState();
				rChain.previousState();
			}
			info.setNeedsConversion(true);
			return true;
		}
		case TYPE_TTCN3_SEQUENCE: {
			final TTCN3_Sequence_Type tempType = (TTCN3_Sequence_Type) temp;
			final int tempTypeNofComps = tempType.getNofComponents();
			if (tempTypeNofComps == 0) {
				return false;
			}

			int nofOptionalFields = 0;
			for (int i = 0; i < tempTypeNofComps; i++) {
				final CompField tempTypeCf = tempType.getComponentByIndex(i);
				if (tempTypeCf.isOptional()) {
					nofOptionalFields++;
				}
			}
			final long nofComps = getDimension().getSize();
			if (nofComps < tempTypeNofComps - nofComps) {
				info.setErrorStr(MessageFormat.format(NOFFIELDSDONTMATCH, nofComps, tempTypeNofComps - nofOptionalFields));
				return false;
			}
			TypeCompatibilityInfo.Chain lChain = leftChain;
			TypeCompatibilityInfo.Chain rChain = rightChain;
			if (lChain == null) {
				lChain = info.getChain();
				lChain.add(this);
			}
			if (rChain == null) {
				rChain = info.getChain();
				rChain.add(tempType);
			}
			for (int i = 0; i < tempTypeNofComps; i++) {
				final CompField tempTypeCf = tempType.getComponentByIndex(i);
				final IType tempTypeCfType = tempTypeCf.getType().getTypeRefdLast(timestamp);
				final IType tempElementType = getElementType().getTypeRefdLast(timestamp);
				lChain.markState();
				rChain.markState();
				lChain.add(tempElementType);
				rChain.add(tempTypeCfType);
				final TypeCompatibilityInfo infoTemp = new TypeCompatibilityInfo(tempElementType, tempTypeCfType, false);
				if (!tempElementType.equals(tempTypeCfType)
						&& !(lChain.hasRecursion() && rChain.hasRecursion())
						&& !tempElementType.isCompatible(timestamp, tempTypeCfType, infoTemp, lChain, rChain)) {
					info.appendOp1Ref(infoTemp.getOp1RefStr());
					info.appendOp2Ref(DOT + tempTypeCf.getIdentifier().getDisplayName() + infoTemp.getOp2RefStr());
					info.setOp1Type(infoTemp.getOp1Type());
					info.setOp2Type(infoTemp.getOp2Type());
					info.setErrorStr(infoTemp.getErrorStr());
					lChain.previousState();
					rChain.previousState();
					return false;
				}
				lChain.previousState();
				rChain.previousState();
			}
			info.setNeedsConversion(true);
			return true;
		}
		case TYPE_SEQUENCE_OF: {
			final SequenceOf_Type tempType = (SequenceOf_Type) temp;
			if (!tempType.isSubtypeCompatible(timestamp, this)) {
				info.setErrorStr("Incompatible record of/SEQUENCE OF subtypes");
				return false;
			}

			final IType tempTypeOfType = tempType.getOfType().getTypeRefdLast(timestamp);
			final IType tempElementType = getElementType().getTypeRefdLast(timestamp);
			TypeCompatibilityInfo.Chain lChain = leftChain;
			TypeCompatibilityInfo.Chain rChain = rightChain;
			if (lChain == null) {
				lChain = info.getChain();
				lChain.add(this);
			}
			if (rChain == null) {
				rChain = info.getChain();
				rChain.add(tempType);
			}
			lChain.markState();
			rChain.markState();
			lChain.add(tempElementType);
			rChain.add(tempTypeOfType);
			final TypeCompatibilityInfo infoTemp = new TypeCompatibilityInfo(tempElementType, tempTypeOfType, false);
			if (!tempElementType.equals(tempTypeOfType)
					&& !(lChain.hasRecursion() && rChain.hasRecursion())
					&& !tempElementType.isCompatible(timestamp, tempTypeOfType, infoTemp, lChain, rChain)) {
				info.appendOp1Ref(infoTemp.getOp1RefStr());
				if (infoTemp.getOp2RefStr().length() > 0) {
					info.appendOp2Ref("[]");
				}
				info.appendOp2Ref(infoTemp.getOp2RefStr());
				info.setOp1Type(infoTemp.getOp1Type());
				info.setOp2Type(infoTemp.getOp2Type());
				info.setErrorStr(infoTemp.getErrorStr());
				lChain.previousState();
				rChain.previousState();
				return false;
			}
			info.setNeedsConversion(true);
			lChain.previousState();
			rChain.previousState();
			return true;
		}
		case TYPE_ARRAY: {
			final Array_Type tempType = (Array_Type) temp;
			if (this == tempType) {
				return true;
			}
			if (dimension != null && tempType.dimension != null && !dimension.isIdentical(timestamp, tempType.dimension)) {
				info.setErrorStr(BADARRAYDIMENSION);
				return false;
			}

			final IType tempElementType = getElementType().getTypeRefdLast(timestamp);
			final IType tempTypeElementType = tempType.getElementType().getTypeRefdLast(timestamp);
			TypeCompatibilityInfo.Chain lChain = leftChain;
			TypeCompatibilityInfo.Chain rChain = rightChain;
			if (lChain == null) {
				lChain = info.getChain();
				lChain.add(this);
			}
			if (rChain == null) {
				rChain = info.getChain();
				rChain.add(tempType);
			}
			lChain.markState();
			rChain.markState();
			lChain.add(tempElementType);
			rChain.add(tempTypeElementType);
			final TypeCompatibilityInfo infoTemp = new TypeCompatibilityInfo(tempElementType, tempTypeElementType, false);
			if (!tempElementType.equals(tempTypeElementType)
					&& !(lChain.hasRecursion() && rChain.hasRecursion())
					&& !tempElementType.isCompatible(timestamp, tempTypeElementType, infoTemp, lChain, rChain)) {
				info.appendOp1Ref(infoTemp.getOp1RefStr());
				info.appendOp2Ref(infoTemp.getOp2RefStr());
				info.setOp1Type(infoTemp.getOp1Type());
				info.setOp2Type(infoTemp.getOp2Type());
				info.setErrorStr(infoTemp.getErrorStr());
				lChain.previousState();
				rChain.previousState();
				return false;
			}
			info.setNeedsConversion(true);
			lChain.previousState();
			rChain.previousState();
			return true;
		}
		case TYPE_ASN1_CHOICE:
		case TYPE_TTCN3_CHOICE:
		case TYPE_ANYTYPE:
			info.setErrorStr(NOTCOMPATIBLEUNIONANYTYPE);
			return false;
		case TYPE_ASN1_SET:
		case TYPE_TTCN3_SET:
		case TYPE_SET_OF:
			info.setErrorStr(NOTCOMPATIBLESETSETOF);
			return false;
		default:
			return false;
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isIdentical(final CompilationTimeStamp timestamp, final IType type) {
		check(timestamp);
		type.check(timestamp);
		final IType temp = type.getTypeRefdLast(timestamp);

		if (getIsErroneous(timestamp) || temp.getIsErroneous(timestamp)) {
			return true;
		}

		if (!Type_type.TYPE_ARRAY.equals(temp.getTypetype())) {
			return false;
		}

		final Array_Type other = (Array_Type) temp;
		final boolean result = elementType != null && other.elementType != null && elementType.isIdentical(timestamp, other.elementType);
		return result && dimension != null && other.dimension != null && dimension.isIdentical(timestamp, other.dimension);
	}

	@Override
	/** {@inheritDoc} */
	public boolean isComponentInternal(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return componentInternal;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (elementType != null && referenceChain.add(this)) {
			referenceChain.markState();
			elementType.checkRecursions(timestamp, referenceChain);
			referenceChain.previousState();
		}
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;
		componentInternal = false;
		isErroneous = false;

		initAttributes(timestamp);

		if (elementType != null) {
			//elementType.setGenName(getGenNameOwn(), "0");
			elementType.setParentType(this);
			elementType.check(timestamp);
			elementType.checkEmbedded(timestamp, elementType.getLocation(), true, "embedded into an array type");
			componentInternal = elementType.isComponentInternal(timestamp);
		}

		if (dimension != null) {
			dimension.check(timestamp);
		}

		if (myScope != null) {
			checkEncode(timestamp);
			checkVariants(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkComponentInternal(final CompilationTimeStamp timestamp, final Set<IType> typeSet, final String operation) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (typeSet.contains(this)) {
			return;
		}

		if (elementType != null && elementType.isComponentInternal(timestamp)) {
			typeSet.add(this);
			elementType.checkComponentInternal(timestamp, typeSet, operation);
			typeSet.remove(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisValue(final CompilationTimeStamp timestamp, final IValue value, final Assignment lhs, final ValueCheckingOptions valueCheckingOptions) {
		boolean selfReference = super.checkThisValue(timestamp, value, lhs, valueCheckingOptions);

		IValue last = value.getValueRefdLast(timestamp, valueCheckingOptions.expected_value, null);
		if (last == null || last.getIsErroneous(timestamp)) {
			return selfReference;
		}

		// already handled ones
		switch (value.getValuetype()) {
		case OMIT_VALUE:
		case REFERENCED_VALUE:
			return selfReference;
		case UNDEFINED_LOWERIDENTIFIER_VALUE:
			if (Value_type.REFERENCED_VALUE.equals(last.getValuetype())) {
				return selfReference;
			}
			break;
		default:
			break;
		}

		switch (last.getValuetype()) {
		case SEQUENCEOF_VALUE:
			last = last.setValuetype(timestamp, Value_type.ARRAY_VALUE);
			selfReference = checkThisValueArray(timestamp, value, lhs, (Array_Value) last, valueCheckingOptions.expected_value,
					valueCheckingOptions.incomplete_allowed, valueCheckingOptions.implicit_omit, valueCheckingOptions.str_elem);
			break;
		case ARRAY_VALUE:
			selfReference = checkThisValueArray(timestamp, value, lhs, (Array_Value) last, valueCheckingOptions.expected_value,
					valueCheckingOptions.incomplete_allowed, valueCheckingOptions.implicit_omit, valueCheckingOptions.str_elem);
			break;
		case EXPRESSION_VALUE:
		case MACRO_VALUE:
			// already checked
			break;
		default:
			value.getLocation().reportSemanticError(ARRAYVALUEEXPECTED);
			value.setIsErroneous(true);
			break;
		}

		value.setLastTimeChecked(timestamp);

		return selfReference;
	}

	private boolean checkThisValueArray(final CompilationTimeStamp timestamp, final IValue originalValue, final Assignment lhs,
			final Array_Value lastValue, final Expected_Value_type expectedValue,
			final boolean incompleteAllowed, final boolean implicitOmit, final boolean strElem) {
		if (dimension == null) {
			return false;
		}

		boolean selfReference = false;
		final int nofValues = lastValue.getNofComponents();

		if (!dimension.getIsErroneous(timestamp) && dimension.getSize() < nofValues) {
			originalValue.getLocation().reportSemanticError(MessageFormat.format(TOOMANYEXPECTED, dimension.getSize(), nofValues));
			originalValue.setIsErroneous(true);
		}

		if (lastValue.isIndexed()) {
			boolean checkHoles = !dimension.getIsErroneous(timestamp) && Expected_Value_type.EXPECTED_CONSTANT.equals(expectedValue);
			final long arraySize = dimension.getSize();
			BigInteger maxIndex = BigInteger.valueOf(-1);
			final Map<BigInteger, Integer> indexMap = new HashMap<BigInteger, Integer>(lastValue.getNofComponents());
			for (int i = 0, size = lastValue.getNofComponents(); i < size; i++) {
				final IValue component = lastValue.getValueByIndex(i);
				final Value index = lastValue.getIndexByIndex(i);
				dimension.checkIndex(timestamp, index, expectedValue);

				final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
				final IValue indexLast = index.getValueRefdLast(timestamp, referenceChain);
				referenceChain.release();

				if (indexLast.getIsErroneous(timestamp) || !Value_type.INTEGER_VALUE.equals(indexLast.getValuetype())) {
					checkHoles = false;
				} else {
					final BigInteger tempIndex = ((Integer_Value) indexLast).getValueValue();
					if (tempIndex.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0) {
						index.getLocation().reportSemanticError(MessageFormat.format(
								"A integer value less than `{0}'' was expected for indexing type `{1}'' instead of `{2}''",
								Integer.MAX_VALUE, getTypename(), tempIndex));
						checkHoles = false;
					} else if (tempIndex.compareTo(BigInteger.ZERO) < 0) {
						index.getLocation().reportSemanticError(MessageFormat.format(
								"A non-negative integer value was expected for indexing type `{0}'' instead of `{1}''", getTypename(), tempIndex));
						checkHoles = false;
					} else if (indexMap.containsKey(tempIndex)) {
						index.getLocation().reportSemanticError(MessageFormat.format(
								"Duplicate index value `{0}'' for components {1} and {2}", tempIndex, indexMap.get(tempIndex),  i + 1));
						checkHoles = false;
					} else {
						indexMap.put(tempIndex, Integer.valueOf(i + 1));
						if (maxIndex.compareTo(tempIndex) < 0) {
							maxIndex = tempIndex;
						}
					}
				}

				component.setMyGovernor(elementType);
				final IValue tempValue2 = elementType.checkThisValueRef(timestamp, component);
				selfReference |= elementType.checkThisValue(timestamp, tempValue2, lhs,
						new ValueCheckingOptions(expectedValue, incompleteAllowed, false, true, implicitOmit, strElem));
			}
			if (checkHoles) {
				if (indexMap.size() < arraySize) {
					lastValue.getLocation().reportSemanticError("It's not allowed to create hole(s) in constant values");
					originalValue.setIsErroneous(true);
				}
			}
		} else {
			if (!dimension.getIsErroneous(timestamp)) {
				final long arraySize = dimension.getSize();
				if (arraySize > nofValues) {
					originalValue.getLocation().reportSemanticError(MessageFormat.format(
							TOOFEWEXPECTED, arraySize, nofValues));
					originalValue.setIsErroneous(true);
				} else if (arraySize < nofValues) {
					originalValue.getLocation().reportSemanticError(MessageFormat.format(
							TOOMANYEXPECTED, arraySize, nofValues));
					originalValue.setIsErroneous(true);
				}
			}

			for (int i = 0, size = lastValue.getNofComponents(); i < size; i++) {
				final IValue component = lastValue.getValueByIndex(i);
				component.setMyGovernor(elementType);
				if (Value_type.NOTUSED_VALUE.equals(component.getValuetype())) {
					if (!incompleteAllowed) {
						component.getLocation().reportSemanticError(AbstractOfType.INCOMPLETEPRESENTERROR);
					}
				} else {
					final IValue tempValue2 = elementType.checkThisValueRef(timestamp, component);
					selfReference |= elementType.checkThisValue(timestamp, tempValue2, lhs,
							new ValueCheckingOptions(expectedValue, incompleteAllowed, false, true, implicitOmit, strElem));
				}
			}
		}

		return selfReference;
	}

	public IType getElementType() {
		return elementType;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplate(final CompilationTimeStamp timestamp, final ITTCN3Template template, final boolean isModified,
			final boolean implicitOmit, final Assignment lhs) {
		registerUsage(template);
		template.setMyGovernor(this);

		boolean selfReference = false;
		switch (template.getTemplatetype()) {
		case OMIT_VALUE:
			if (template.getLengthRestriction() != null) {
				template.getLocation().reportSemanticWarning(REDUNDANTLENGTHRESTRICTION);
			}
			break;
		case PERMUTATION_MATCH: {
			for (ITTCN3Template templateComponent : (PermutationMatch_Template) template) {
				templateComponent.setMyGovernor(elementType);
				templateComponent = elementType.checkThisTemplateRef(timestamp, templateComponent);
				selfReference |= templateComponent.checkThisTemplateGeneric(timestamp, elementType, isModified, false, true, true, false, lhs);
			}
			break;
		}
		case TEMPLATE_LIST: {
			final Template_List listTemplate = (Template_List) template;
			ITTCN3Template baseTemplate = listTemplate.getBaseTemplate();
			int nofBaseComponents = 0;
			if (baseTemplate != null) {
				baseTemplate = baseTemplate.getTemplateReferencedLast(timestamp, null);
				if (Template_type.TEMPLATE_LIST.equals(baseTemplate.getTemplatetype())) {
					nofBaseComponents = ((Template_List) baseTemplate).size();
				} else {
					baseTemplate = null;
				}
			}

			if (!dimension.getIsErroneous(timestamp)) {
				final long arraySize = dimension.getSize();
				final int nofComponents = listTemplate.size();
				boolean fixedSize = true;
				int templateSize = 0;
				for (int i = 0; i < nofComponents && fixedSize; i++) {
					final ITTCN3Template templateComponent = listTemplate.get(i);
					switch (templateComponent.getTemplatetype()) {
					case PERMUTATION_MATCH: {
						final PermutationMatch_Template permutationTemplate = (PermutationMatch_Template) templateComponent;
						if(permutationTemplate.containsAnyornoneOrPermutation(timestamp)) {
							fixedSize = false;
						} else {
							templateSize += permutationTemplate.getNofTemplatesNotAnyornone(timestamp);
						}
						break;
					}
					default:
						templateSize++;
						break;
					}
				}

				if (fixedSize) {
					if (arraySize < templateSize) {
						listTemplate.getLocation().reportSemanticError(MessageFormat.format(TOOMANYTEMPLATEELEMENTS, arraySize, templateSize));
					} else if (arraySize > templateSize) {
						listTemplate.getLocation().reportSemanticError(MessageFormat.format(TOOFEWTEMPLATEELEMENTS, arraySize, templateSize));
					}
				}
			}

			final int nofComponents = ((Template_List) template).size();
			for (int i = 0; i < nofComponents; i++) {
				ITTCN3Template templateComponent = listTemplate.get(i);
				templateComponent.setMyGovernor(elementType);
				if (baseTemplate != null && i < nofBaseComponents) {
					templateComponent.setBaseTemplate(((Template_List) baseTemplate).get(i));
				}
				templateComponent = elementType.checkThisTemplateRef(timestamp, templateComponent);
				switch (templateComponent.getTemplatetype()) {
				case PERMUTATION_MATCH:
					selfReference |= templateComponent.checkThisTemplateGeneric(timestamp, this, isModified, false, true, true, implicitOmit, lhs);
					break;
				case TEMPLATE_NOTUSED:
					if (!isModified) {
						templateComponent.getLocation().reportSemanticWarning(NOTUSEDNOTALLOWED);
					}
					break;
				default:
					selfReference |= templateComponent.checkThisTemplateGeneric(timestamp, elementType, isModified, false, true, true, implicitOmit, lhs);
					break;
				}
			}
			break;
		}
		case INDEXED_TEMPLATE_LIST:	{
			final Map<Long, Integer> indexMap = new HashMap<Long, Integer>();
			final Indexed_Template_List indexedTemplateList = (Indexed_Template_List) template;
			for (int i = 0; i < indexedTemplateList.size(); i++) {
				final IndexedTemplate indexedTemplate = indexedTemplateList.get(i);
				final Value indexValue = indexedTemplate.getIndex().getValue();

				dimension.checkIndex(timestamp, indexValue, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
				ITTCN3Template templateComponent = indexedTemplate.getTemplate();

				final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
				final IValue lastValue = indexValue.getValueRefdLast(timestamp, chain);
				chain.release();
				if (Value_type.INTEGER_VALUE.equals(lastValue.getValuetype())) {
					final long index = ((Integer_Value) lastValue).getValue();
					if (index > Integer.MAX_VALUE) {
						indexValue.getLocation().reportSemanticError(MessageFormat.format(
								"An integer value less than `{0}'' was expected for indexing type `{1}'' instead of `{2}''",
								Integer.MAX_VALUE, getTypename(), index));
						indexValue.setIsErroneous(true);
					} else {
						if (indexMap.containsKey(index)) {
							indexValue.getLocation().reportSemanticError(MessageFormat.format(
									"Duplicate index value `{0}'' for component `{1}'' and `{2}''", index, i + 1, indexMap.get(index)));
							indexValue.setIsErroneous(true);
						} else {
							indexMap.put(index, i);
						}
					}
				}

				templateComponent.setMyGovernor(elementType);
				templateComponent = elementType.checkThisTemplateRef(timestamp, templateComponent);
				selfReference |= templateComponent.checkThisTemplateGeneric(timestamp, elementType, isModified, false, true, true, implicitOmit, lhs);
			}
			break;
		}
		default:
			template.getLocation().reportSemanticError(MessageFormat.format(TEMPLATENOTALLOWED, template.getTemplateTypeName(), getTypename()));
			break;
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean canHaveCoding(final CompilationTimeStamp timestamp, final MessageEncoding_type coding) {
		if (insideCanHaveCoding) {
			insideCanHaveCoding = false;
			return true;
		}
		insideCanHaveCoding = true;

		if (coding != MessageEncoding_type.JSON) {
			insideCanHaveCoding = false;
			return false;
		}

		final boolean result = elementType.getTypeRefdLast(timestamp).canHaveCoding(timestamp, coding);

		insideCanHaveCoding = false;
		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void checkCodingAttributes(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		//TODO add checks for other encodings.

		checkJson(timestamp);

		if (refChain.contains(this)) {
			return;
		}

		refChain.add(this);
		refChain.markState();

		elementType.checkCodingAttributes(timestamp, refChain);

		refChain.previousState();
	}

	@Override
	/** {@inheritDoc} */
	public void checkJson(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (jsonAttribute == null && !hasEncodeAttribute(MessageEncoding_type.JSON)) {
			return;
		}

		elementType.forceJson(timestamp);

		if (jsonAttribute == null) {
			return;
		}

		if (jsonAttribute.omit_as_null && !isOptionalField()) {
			getLocation().reportSemanticError("Invalid attribute, 'omit as null' requires optional field of a record or set.");
		}

		if (jsonAttribute.as_value) {
			getLocation().reportSemanticError("Invalid attribute, 'as value' is only allowed for unions, the anytype, or records or sets with one field");
		}

		if (jsonAttribute.alias != null) {
			final IType parent = getParentType();
			if (parent == null) {
				// only report this error when using the new codec handling, otherwise
				// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
				getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
			} else {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					break;
				default:
					// only report this error when using the new codec handling, otherwise
					// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
					getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
					break;
				}
			}

			if (parent != null && parent.getJsonAttribute() != null && parent.getJsonAttribute().as_value) {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					// parent_type_name remains null if the 'as value' attribute is set for an invalid type
					getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					break;
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
					if (((TTCN3_Set_Seq_Choice_BaseType)parent).getNofComponents() == 1) {
						// parent_type_name remains null if the 'as value' attribute is set for an invalid type
						getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					}
					break;
				default:
					break;
				}
			}
		}

		if (jsonAttribute.parsed_default_value != null) {
			checkJsonDefault(timestamp);
		}

		//TODO: check schema extensions

		if (jsonAttribute.as_number) {
			getLocation().reportSemanticError("Invalid attribute, 'as number' is only allowed for enumerated types");
		}

		//FIXME: check tag_list

		if (jsonAttribute.as_map) {
			getLocation().reportSemanticError("Invalid attribute, 'as map' requires record of or set of");
		}

		if (!jsonAttribute.enum_texts.isEmpty()) {
			getLocation().reportSemanticError("Invalid attribute, 'text ... as ...' requires an enumerated type");
		}
	}

	@Override
	/** {@inheritDoc} */
	public void getTypesWithNoCodingTable(final CompilationTimeStamp timestamp, final List<IType> typeList, final boolean onlyOwnTable) {
		super.getTypesWithNoCodingTable(timestamp, typeList, onlyOwnTable);
		elementType.getTypesWithNoCodingTable(timestamp, typeList, onlyOwnTable);
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getTypetypeTtcn3() {
		if (isErroneous) {
			return Type_type.TYPE_UNDEFINED;
		}

		return getTypetype();
	}

	@Override
	/** {@inheritDoc} */
	public String getTypename() {
		if (isErroneous || elementType == null || this == elementType) {
			return "Erroneous type";
		}

		final StringBuilder builder = new StringBuilder();
		builder.append(dimension.createStringRepresentation());
		IType temp = elementType;
		while (temp != null && Type_type.TYPE_ARRAY.equals(temp.getTypetype())) {
			final Array_Type tempArray = (Array_Type) temp;
			builder.append(tempArray.dimension.createStringRepresentation());
			temp = tempArray.elementType;
		}
		if (temp != null) {
			builder.insert(0, temp.getTypename());
		}

		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex,
			final IReferenceChain refChain, final boolean interruptIfOptional) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return this;
		}

		final Expected_Value_type internalExpectation = (expectedIndex == Expected_Value_type.EXPECTED_TEMPLATE) ? Expected_Value_type.EXPECTED_DYNAMIC_VALUE
				: expectedIndex;
		final ISubReference subreference = subreferences.get(actualSubReference);
		switch (subreference.getReferenceType()) {
		case arraySubReference:
			final Value indexValue = ((ArraySubReference) subreference).getValue();
			indexValue.setLoweridToReference(timestamp);

			IType indexingType = indexValue.getExpressionGovernor(timestamp, expectedIndex);
			if (indexingType == null) {
				//an error was already reported.
				return null;
			}

			indexingType = indexingType.getTypeRefdLast(timestamp);
			if (indexingType != null && (indexingType.getTypetype() == Type_type.TYPE_ARRAY || indexingType.getTypetype() == Type_type.TYPE_SEQUENCE_OF)) {
				// The indexer type must be of type integer
				long length = 0;
				if (indexingType.getTypetype() == Type_type.TYPE_ARRAY) {
					final Array_Type indexingArray = (Array_Type)indexingType;
					if (indexingArray.getElementType().getTypetype() != Type_type.TYPE_INTEGER) {
						subreference.getLocation().reportSemanticError("Only fixed length array or record of integer types are allowed for short-hand notation for nested indexes.");
						return null;
					}

					length = indexingArray.getDimension().getSize();
				} else if (indexingType.getTypetype() == Type_type.TYPE_SEQUENCE_OF) {
					final SequenceOf_Type indexingSequenceOf = (SequenceOf_Type)indexingType;
					if (indexingSequenceOf.getOfType().getTypetype() != Type_type.TYPE_INTEGER) {
						subreference.getLocation().reportSemanticError("Only fixed length array or record of integer types are allowed for short-hand notation for nested indexes.");
						return null;
					}

					final SubType subType = indexingSequenceOf.getSubtype();
					if (subType == null) {
						subreference.getLocation().reportSemanticError(MessageFormat.format("The type `{0}'' must have single size length restriction when used as a short-hand notation for nested indexes.", indexingSequenceOf.getTypename()));
						return null;
					}

					length = subType.get_length_restriction();
					if (length == -1) {
						subreference.getLocation().reportSemanticError(MessageFormat.format("The type `{0}'' must have single size length restriction when used as a short-hand notation for nested indexes.", indexingSequenceOf.getTypename()));
						return null;
					}
				}

				IType embeddedType = elementType.getTypeRefdLast(timestamp);
				int j = 0;
				while (j < length - 1) {
					switch(embeddedType.getTypetype()) {
					case TYPE_ARRAY:
						embeddedType = ((Array_Type)embeddedType).getElementType();
						break;
					case TYPE_SEQUENCE_OF:
						embeddedType = ((SequenceOf_Type)embeddedType).getOfType();
						break;
					case TYPE_SET_OF:
						embeddedType = ((SetOf_Type)embeddedType).getOfType();
						break;
					default:
						subreference.getLocation().reportSemanticError(MessageFormat.format("The type `{0}'' contains too many indexes ({1}) in the short-hand notation for nested indexes.", indexingType.getTypename(), length));
						return null;
					}
					j++;
				}

				return embeddedType;
			} else {
				if (dimension != null) {
					dimension.checkIndex(timestamp, indexValue, expectedIndex);
				}

				if (elementType != null) {
					return elementType.getFieldType(timestamp, reference, actualSubReference + 1, internalExpectation, refChain, interruptIfOptional);
				}
			}

			return null;
		case fieldSubReference:
			subreference.getLocation().reportSemanticError(
					MessageFormat.format(FieldSubReference.INVALIDSUBREFERENCE, ((FieldSubReference) subreference).getId().getDisplayName(),
							getTypename()));
			return null;
		case parameterisedSubReference:
			subreference.getLocation().reportSemanticError(
					MessageFormat.format(FieldSubReference.INVALIDSUBREFERENCE, ((ParameterisedSubReference) subreference).getId().getDisplayName(),
							getTypename()));
			return null;
		default:
			subreference.getLocation().reportSemanticError(ISubReference.INVALIDSUBREFERENCE);
			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkMapParameter(final CompilationTimeStamp timestamp, final IReferenceChain refChain, final Location errorLocation) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (refChain.contains(this)) {
			return;
		}

		refChain.add(this);
		if (elementType != null) {
			elementType.checkMapParameter(timestamp, refChain, errorLocation);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getProposalDescription(final StringBuilder builder) {
		builder.append("array of ");
		if (elementType != null) {
			elementType.getProposalDescription(builder);
		}
		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (elementType != null) {
			elementType.updateSyntax(reparser, false);
			reparser.updateLocation(elementType.getLocation());
		}

		if (dimension != null) {
			dimension.updateSyntax(reparser, false);
			reparser.updateLocation(dimension.getLocation());
		}

		if (subType != null) {
			subType.updateSyntax(reparser, false);
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void getEnclosingField(final Position offset, final ReferenceFinder rf) {
		if (elementType == null) {
			return;
		}

		elementType.getEnclosingField(offset, rf);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (elementType != null) {
			elementType.findReferences(referenceFinder, foundIdentifiers);
		}
		if (dimension != null) {
			dimension.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (elementType!=null && !elementType.accept(v)) {
			return false;
		}
		if (dimension!=null && !dimension.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IDeclaration resolveReference(final Reference reference, final int subRefIdx, final ISubReference lastSubreference) {
		if (elementType == null) {
			return null;
		}

		final IType refdLastOfType = elementType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
		if (refdLastOfType instanceof IReferenceableElement) {
			return ((IReferenceableElement) refdLastOfType).resolveReference(reference, subRefIdx + 1, lastSubreference);
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isPresentAnyvalueEmbeddedField(final ExpressionStruct expression, final List<ISubReference> subreferences, final int beginIndex) {
		if (subreferences == null || getIsErroneous(CompilationTimeStamp.getBaseTimestamp())) {
			return true;
		}

		if (beginIndex >= subreferences.size()) {
			return true;
		}

		final ISubReference subReference = subreferences.get(beginIndex);
		if (!(subReference instanceof ArraySubReference)) {
			TitanLogger.logFatal("Code generator reached erroneous type reference `" + getFullName() + "''");
			expression.expression.append("FATAL_ERROR encountered while processing `" + getFullName() + "''\n");
			return true;
		}

		if (elementType == null) {
			return true;
		}

		return elementType.isPresentAnyvalueEmbeddedField(expression, subreferences, beginIndex + 1);
	}

	@Override
	public String getDefaultSnippet(String lineEnding, int indentation, AtomicInteger placeholderIdx) {
		final IType et = getElementType().getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
		final long size = getDimension().getSize();
		final StringBuilder sb = new StringBuilder("{");
		if (size == 0) {
			final String elementSnippet = ((Type)et).getDefaultSnippet(lineEnding, indentation, placeholderIdx);
			sb.append(elementSnippet);
		} else {
			for (int i = 0; i < size; i++) {
				final String elementSnippet = ((Type)et).getDefaultSnippet(lineEnding, indentation, placeholderIdx);
				sb.append(elementSnippet);
				if(i < size - 1) {
					sb.append(", ");
				}
			}
		}

		sb.append("}");

		return sb.toString();
	}
	
	@Override
	public String getRestrictionDesc() {
		final long size = getDimension().getSize();
		String restrictions = null;
		if (size > 0) {
			restrictions = String.valueOf(size);
		}

		final IType et = getElementType().getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
		if (et == null) {
			return null;
		}

		final StringBuilder sb = new StringBuilder();
		if (restrictions != null) {
			sb.append(getTypename())
			.append(": ")
			.append("length(")
			.append(restrictions)
			.append(")");
		}

		final String ofTypeRestristictions = ((Type)et).getRestrictionDesc();
		if (ofTypeRestristictions != null) {
			sb.append("\n\n")
			.append(((Type)et).getTypename())
			.append(": ")
			.append(ofTypeRestristictions);
		}

		if (sb.length() > 0) {
			return sb.toString();
		}

		return null;
	}
}
