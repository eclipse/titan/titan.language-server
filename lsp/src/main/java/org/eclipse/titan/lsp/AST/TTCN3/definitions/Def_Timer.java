/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ArraySubReference;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.ISubReference.Subreference_type;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.types.ComponentTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimension;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimensions;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Real_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.SequenceOf_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;

/**
 * The Def_Timer class represents TTCN3 timer definitions.
 * <p>
 * Timers in TTCN3 does not have a type.
 *
 * @author Kristof Szabados
 * @author Farkas Izabella Ingrid
 * */
public final class Def_Timer extends Definition {
	private static final String NEGATIVDURATIONERROR = "A non-negative float value was expected as timer duration instead of {0}";
	private static final String INFINITYDURATIONERROR = "{0} can not be used as the default timer duration";
	private static final String OPERANDERROR = "The default timer duration should be a float value";

	private static final String FULLNAMEPART1 = ".<dimensions>";
	private static final String FULLNAMEPART2 = ".<default_duration>";
	private static final String KIND = "timer";

	private final ArrayDimensions dimensions;
	private final Value defaultDuration;

	public Def_Timer(final Identifier identifier, final ArrayDimensions dimensions, final Value defaultDuration) {
		super(identifier);
		this.dimensions = dimensions;
		this.defaultDuration = defaultDuration;

		if (dimensions != null) {
			dimensions.setFullNameParent(this);
		}
		if (defaultDuration != null) {
			defaultDuration.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_TIMER;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (dimensions != null) {
			dimensions.setMyScope(scope);
		}
		if (defaultDuration != null) {
			defaultDuration.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (dimensions == child) {
			return builder.append(FULLNAMEPART1);
		} else if (defaultDuration == child) {
			return builder.append(FULLNAMEPART2);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public String getDescription() {
		final StringBuilder builder = new StringBuilder();
		builder.append(getAssignmentName()).append(" `");

		if (isLocal()) {
			builder.append(identifier.getDisplayName());
		} else {
			builder.append(getFullName());
		}

		builder.append('\'');
		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		return getAssignmentName();
	}

	public ArrayDimensions getDimensions() {
		return dimensions;
	}

	/**
	 * Returns false if it is sure that the timer referred by array indices
	 * reference does not have a default duration. Otherwise it returns
	 * true.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle
	 * @param reference
	 *                might be NULL when examining a single timer.
	 *
	 * @return true if the timer has a default duration, false otherwise.
	 * */
	public boolean hasDefaultDuration(final CompilationTimeStamp timestamp, final Reference reference) {
		if (defaultDuration == null) {
			return false;
		} else if (dimensions == null || reference == null) {
			return true;
		}

		IValue v = defaultDuration;
		final List<ISubReference> subreferences = reference.getSubreferences();
		final int nofDimensions = dimensions.size();
		final int nofReferences = subreferences.size() - 1;
		final int upperLimit = (nofDimensions < nofReferences) ? nofDimensions : nofReferences;
		for (int i = 0; i < upperLimit; i++) {
			v = v.getValueRefdLast(timestamp, null);
			if (Value_type.SEQUENCEOF_VALUE.equals(v.getValuetype())) {
				final ISubReference ref = subreferences.get(i + 1);
				if (!Subreference_type.arraySubReference.equals(ref.getReferenceType())) {
					return true;
				}

				final IValue index = ((ArraySubReference) ref).getValue();
				if (!Value_type.INTEGER_VALUE.equals(index.getValuetype())) {
					return true;
				}

				final long realIndex = ((Integer_Value) index).getValue() - dimensions.get(i).getOffset();
				if (realIndex >= 0 && realIndex < ((SequenceOf_Value) v).getNofComponents()) {
					v = ((SequenceOf_Value) v).getValueByIndex((int) realIndex);
				}
			}
		}
		return !Value_type.NOTUSED_VALUE.equals(v.getValuetype());
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;

		if (getMyScope() instanceof ComponentTypeBody) {
			NamingConventionHelper.checkConvention(NamingConventionElement.ComponentTimer, identifier, this);
		} else if (isLocal()) {
			NamingConventionHelper.checkConvention(NamingConventionElement.LocalTimer, identifier, this);
		} else {
			NamingConventionHelper.checkConvention(NamingConventionElement.GlobalTimer, identifier, this);
		}
		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (dimensions != null) {
			dimensions.check(timestamp);
		}

		if (defaultDuration != null) {
			if (dimensions == null) {
				defaultDuration.setLoweridToReference(timestamp);
				final Type_type tempType = defaultDuration.getExpressionReturntype(timestamp,
						isLocal() ? Expected_Value_type.EXPECTED_DYNAMIC_VALUE : Expected_Value_type.EXPECTED_STATIC_VALUE);

				switch (tempType) {
				case TYPE_REAL:
					final IValue last = defaultDuration.getValueRefdLast(timestamp, null);
					if (!last.isUnfoldable(timestamp)) {
						final Real_Value real = (Real_Value) last;
						final double value = real.getValue();
						if (value < 0.0f) {
							defaultDuration.getLocation().reportSemanticError(
									MessageFormat.format(NEGATIVDURATIONERROR, value));
						} else if (real.isPositiveInfinity()) {
							final String message = MessageFormat.format(INFINITYDURATIONERROR, real.createStringRepresentation());
							defaultDuration.getLocation().reportSemanticError(message);
						}
					}
					return;
				default:
					defaultDuration.getLocation().reportSemanticError(OPERANDERROR);
					break;
				}
			} else {
				checkArrayDuration(timestamp,defaultDuration, 0);
			}
		}

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

		lastTimeChecked = timestamp;
	}

	private void checkSingleDuration(final CompilationTimeStamp timestamp, final IValue duration) {
		if (isBuildCancelled()) {
			return;
		}
		
		duration.setLoweridToReference(timestamp);

		if (duration.getExpressionReturntype(timestamp, Expected_Value_type.EXPECTED_STATIC_VALUE) == Type_type.TYPE_REAL) {
			final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			final Value v = (Value) duration.getValueRefdLast(timestamp, referenceChain);
			referenceChain.release();

			if (!duration.isUnfoldable(timestamp) && v.getValuetype() == Value_type.REAL_VALUE) {
				final Real_Value value = (Real_Value) v;
				final double valueReal = value.getValue();
				if (valueReal < 0.0 || value.isSpecialFloat()) {
					duration.getLocation().reportSemanticError("A non-negative float value was expected as timer duration instead of" + valueReal);
				}
			}
		} else {
			duration.getLocation().reportSemanticError("Value is not real");
		}
	}

	private void checkArrayDuration(final CompilationTimeStamp timestamp, final IValue duration, final int startDimension) {
		if (isBuildCancelled()) {
			return;
		}
		
		final ArrayDimension dim = dimensions.get(startDimension);
		final boolean arraySizeKnown = !dim.getIsErroneous(timestamp);
		int arraySize = 0;
		if (arraySizeKnown) {
			arraySize = (int) dim.getSize();
		}

		final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final Value v = (Value) duration.getValueRefdLast(timestamp, referenceChain);
		referenceChain.release();

		if(v.getIsErroneous(timestamp)) {
			//error
			return;
		}

		if (v.getValuetype() == Value_type.SEQUENCEOF_VALUE) {
			final SequenceOf_Value value = (SequenceOf_Value) v;
			final int nofComp = value.getNofComponents();

			// Value-list notation.
			if (!value.isIndexed()) {
				if (arraySizeKnown) {
					if (arraySize > nofComp) {
						duration.getLocation().reportSemanticError("Too few elements in the default duration of timer array: "
													+ arraySize + " was expected instead of " + nofComp);
					} else if (arraySize < nofComp) {
						duration.getLocation().reportSemanticError("Too many elements in the default duration of timer array: "
								+ arraySize + " was expected instead of " + nofComp );
					}
				}

				final boolean last_dim = startDimension + 1 >= dimensions.size();
				for (int i = 0; i < nofComp; ++i) {
					final IValue array_v = value.getValueByIndex(i);
					if (array_v.getValuetype() == Value_type.NOTUSED_VALUE) {
						continue;
					}
					if (last_dim) {
						checkSingleDuration(timestamp, array_v);
					} else {
						checkArrayDuration(timestamp, array_v, startDimension + 1);
					}
				}
			} else {
				// Indexed-notation.
				final boolean last_dim = startDimension + 1 >= dimensions.size();
				final Map<Integer, Integer> indexMap = new HashMap<Integer, Integer>();

				for (int i = 0; i < nofComp; ++i) {
					final IValue array_v = value.getValueByIndex(i);
					if (array_v.getValuetype() == Value_type.NOTUSED_VALUE) {
						continue;
					}
					if (last_dim) {
						checkSingleDuration(timestamp, array_v);
					} else {
						checkArrayDuration(timestamp, array_v, startDimension + 1);
					}

					final IValue array_index = value.getIndexByIndex(i);
					dim.checkIndex(timestamp, array_index, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
					final IValue tmp = array_index.getValueRefdLast(timestamp, referenceChain);
					if (tmp.getValuetype() == Value_type.INTEGER_VALUE) {
						final BigInteger index = ((Integer_Value) tmp).getValueValue();
						if (index.compareTo(BigInteger.valueOf( Integer.MAX_VALUE)) > 0) {
							array_index.getLocation().reportSemanticError(MessageFormat.format("An integer value less than {0} was expected for indexing timer array instead of {1}", Integer.MAX_VALUE, index));
							array_index.setIsErroneous(true);
						} else {
							final int IndexValue =  index.intValue();
							if (indexMap.containsKey(IndexValue)) {
								array_index.getLocation().reportSemanticError(MessageFormat.format("Duplicate index value {0} for timer array elements {1} and {2}", index, i+1, indexMap.get(IndexValue)));
								array_index.setIsErroneous(true);
							} else {
								indexMap.put(IndexValue, i+1);
							}
						}
					}
				}
				// It's not possible to have "indexMap.size() > arraySize", since we
				// add only correct constant-index values into the map.  It's possible
				// to create partially initialized timer arrays.
				indexMap.clear();
			}
		} else {
			if (arraySizeKnown) {
				duration.getLocation().reportSemanticError("An array value (with " + arraySize + " elements) was expected as default duration of timer array");
			} else {
				duration.getLocation().reportSemanticError("An array value was expected as default duration of timer array");
			}
			duration.setIsErroneous(true);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkIdentical(final CompilationTimeStamp timestamp, final Definition definition) {
		check(timestamp);
		definition.check(timestamp);

		if (!Assignment_type.A_TIMER.semanticallyEquals(definition.getAssignmentType())) {
			location.reportSemanticError(MessageFormat.format(
					"Local definition `{0}'' is a timer, but the definition inherited from component type `{1}'' is a {2}",
					identifier.getDisplayName(), definition.getMyScope().getFullName(), definition.getAssignmentName()));
			return false;
		}

		final Def_Timer otherTimer = (Def_Timer) definition;
		if (dimensions != null) {
			if (otherTimer.dimensions != null) {
				if (!dimensions.isIdenticial(timestamp, otherTimer.dimensions)) {
					location.reportSemanticError(MessageFormat
							.format("Local timer `{0}'' and the timer inherited from component type `{1}'' have different array dimensions",
									identifier.getDisplayName(), otherTimer.getMyScope().getFullName()));
					return false;
				}
			} else {
				location.reportSemanticError(MessageFormat
						.format("Local definition `{0}'' is a timer array, but the definition inherited from component type `{1}'' is a single timer",
								identifier.getDisplayName(), otherTimer.getMyScope().getFullName()));
				return false;
			}
		} else if (otherTimer.dimensions != null) {
			location.reportSemanticError(MessageFormat
					.format("Local definition `{0}'' is a single timer, but the definition inherited from component type `{1}'' is a timer array",
							identifier.getDisplayName(), otherTimer.getMyScope().getFullName()));
			return false;
		}

		if (defaultDuration != null) {
			if (otherTimer.defaultDuration != null) {
				if (!defaultDuration.isUnfoldable(timestamp) && !otherTimer.defaultDuration.isUnfoldable(timestamp)
						&& !defaultDuration.checkEquality(timestamp, otherTimer.defaultDuration)) {
					final String message = MessageFormat
							.format("Local timer `{0}'' and the timer inherited from component type `{1}'' have different default durations",
									identifier.getDisplayName(), otherTimer.getMyScope().getFullName());
					defaultDuration.getLocation().reportSemanticWarning(message);
				}
			} else {
				final String message = MessageFormat
						.format("Local timer `{0}'' has default duration, but the timer inherited from component type `{1}'' does not",
								identifier.getDisplayName(), otherTimer.getMyScope().getFullName());
				defaultDuration.getLocation().reportSemanticWarning(message);
			}
		} else if (otherTimer.defaultDuration != null) {
			location.reportSemanticWarning(MessageFormat.format(
					"Local timer `{0}'' does not have default duration, but the timer inherited from component type `{1}'' has",
					identifier.getDisplayName(), otherTimer.getMyScope().getFullName()));
		}

		return true;
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		final List<Integer> result = super.getPossibleExtensionStarterTokens();

		if (defaultDuration == null) {
			result.add(Ttcn3Lexer.ASSIGNMENTCHAR);
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			int result = 1;
			final Location tempIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(tempIdentifier) || reparser.isExtending(tempIdentifier)) {
				reparser.extendDamagedRegion(tempIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				if (result != 0) {
					throw new ReParseException(result);
				}

				if (dimensions != null) {
					dimensions.updateSyntax(reparser, false);
				}

				if (defaultDuration != null) {
					defaultDuration.updateSyntax(reparser, false);
					reparser.updateLocation(defaultDuration.getLocation());
				}

				if (withAttributesPath != null) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				}

				return;
			}

			throw new ReParseException();
		}

		reparser.updateLocation(identifier.getLocation());
		if (dimensions != null) {
			dimensions.updateSyntax(reparser, false);
		}

		if (defaultDuration != null) {
			defaultDuration.updateSyntax(reparser, false);
			reparser.updateLocation(defaultDuration.getLocation());
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (dimensions != null) {
			dimensions.findReferences(referenceFinder, foundIdentifiers);
		}
		if (defaultDuration != null) {
			defaultDuration.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (dimensions != null && !dimensions.accept(v)) {
			return false;
		}
		if (defaultDuration != null && !defaultDuration.accept(v)) {
			return false;
		}
		return true;
	}

	
	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}
		
		hoverContent.addStyledText(KIND, Ttcn3HoverContent.BOLD).addText(" ");
		hoverContent.addStyledText(getFullNameForDocComment());

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding)
			.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = super.getCompletionItem();
		item.setKind(CompletionItemKind.Event);
		return item;
	}
}
