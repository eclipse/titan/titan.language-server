/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;

/**
 * Type list used in port types to store attributes and in classes to store traits
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public final class Types extends ASTNode implements ICollection<Type> {
	private final List<Type> typesList = new ArrayList<Type>();

	@Override
	public synchronized boolean add(final Type type) {
		return typesList.add(type);
	}

	/**
	 * Returns the element at the specified position and overrides with {@code null}.
	 * @param index index of the element to return
	 * @return the element at the specified position
	 * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
	 */
	public synchronized Type extract(final int index) {
		final Type type = typesList.get(index);
		typesList.set(index, null);
		return type;
	}

	@Override
	public int size() {
		return typesList.size();
	}

	@Override
	public Type get(final int index) {
		return typesList.get(index);
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final Type t : typesList) {
			/** null check is needed here because class trait list is implemented with possible null entries on the list */
			if (t!= null && !t.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		for (Type type : typesList) {
			type.setMyScope(scope);
		}
	}

	@Override
	public Iterator<Type> iterator() {
		return typesList.iterator();
	}
}
