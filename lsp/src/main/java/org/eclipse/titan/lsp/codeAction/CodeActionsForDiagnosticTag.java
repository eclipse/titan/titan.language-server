/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionKind;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticTag;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.declarationsearch.IdentifierFinderVisitor;

public class CodeActionsForDiagnosticTag implements ICodeActionProvider {
	private TextDocumentIdentifier doc;
	private Diagnostic diagnostic;

	public CodeActionsForDiagnosticTag(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		this.doc = Objects.requireNonNull(doc);
		this.diagnostic = Objects.requireNonNull(diagnostic);
	}

	private IDeclaration getDeclaration() {
		final Module module = Project.INSTANCE.getModuleByPath(IOUtils.getPath(doc.getUri()));
		if (module == null) {
			return null;
		}
		
		final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(diagnostic.getRange().getStart());
		module.accept(visitor);
		return visitor.getReferencedDeclaration();
	}

	private Optional<CodeAction> removeUnusedBlock() {
		final CodeAction action = new CodeAction("Remove unused definition");
		action.setKind(CodeActionKind.QuickFix);

		final IDeclaration declaration = getDeclaration();
		if (declaration == null) {
			return Optional.empty();
		}

		final Location location = declaration.getLocation();

		final TextEditor editor = new TextEditor(doc);
		final WorkspaceEdit edit = editor
				.removeText(location.getRange())
				.getEdit();

		action.setEdit(edit);
		return Optional.of(action);
	}

	@Override
	public List<Either<Command, CodeAction>> provideCodeActions() {
		final List<Either<Command, CodeAction>> codeActions = new ArrayList<>();
		for (DiagnosticTag tag : diagnostic.getTags()) {
			if (tag == DiagnosticTag.Unnecessary) {
				removeUnusedBlock().ifPresent(action -> codeActions.add(Either.forRight(action)));
			}
		}
		return codeActions;
	}
}
