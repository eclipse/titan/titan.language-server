/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.types;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.types.CompField;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * ExtensionAdditionList.
 *
 * @author Kristof Szabados
 */
public final class ExtensionAdditions extends ASTNode implements ICollection<ExtensionAddition> {

	private final List<ExtensionAddition> extensionAdditionsList;

	public ExtensionAdditions() {
		extensionAdditionsList = new ArrayList<ExtensionAddition>();
	}

	public ExtensionAdditions(final List<ExtensionAddition> extensionAdditions) {
		this.extensionAdditionsList = extensionAdditions;
		for (final ExtensionAddition extensionAddition : extensionAdditions) {
			extensionAddition.setFullNameParent(this);
		}
	}

	@Override
	public boolean add(final ExtensionAddition extensionAddition) {
		if (null != extensionAddition) {
			extensionAddition.setFullNameParent(this);
			return extensionAdditionsList.add(extensionAddition);
		}
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (final ExtensionAddition extensionAddition : extensionAdditionsList) {
			extensionAddition.setMyScope(scope);
		}
	}

	@Override
	public int size() {
		int result = 0;
		for (final ExtensionAddition extensionAddition : extensionAdditionsList) {
			result += extensionAddition.getNofComps();
		}
		return result;
	}

	public CompField getCompByIndex(final int index) {
		int offset = index;
		for (ExtensionAddition ea : extensionAdditionsList) {
			final int subSize = ea.getNofComps();
			if (offset < subSize) {
				return ea.getCompByIndex(offset);
			}
			offset -= subSize;
		}
		// FATAL ERROR
		return null;
	}

	public void trCompsof(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain, final boolean isSet) {
		for (final ExtensionAddition extensionAddition : extensionAdditionsList) {
			extensionAddition.trCompsof(timestamp, referenceChain, isSet);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final ExtensionAddition ea : extensionAdditionsList) {
			ea.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final ExtensionAddition ea : extensionAdditionsList) {
			if (!ea.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public ExtensionAddition get(final int index) {
		return extensionAdditionsList.get(index);
	}

	@Override
	public Iterator<ExtensionAddition> iterator() {
		return extensionAdditionsList.iterator();
	}
}
