/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.text.MessageFormat;
import org.eclipse.titan.lsp.AST.IType.ValueCheckingOptions;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.CharString_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TypeFactory;
import org.eclipse.titan.lsp.AST.TTCN3.values.Charstring_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * The base of what a Value is.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public abstract class Value extends GovernedSimple implements IReferenceChainElement, IValue {
	protected static final String INACTIVEFIELD = "Reference to inactive field `{0}'' in a value of union type `{1}''. The active field is `{2}''";

	/** The type of the value, which also happens to be its governor. */
	protected IType myGovernor;

	/**
	 * This is the governor last set for this value.
	 * Should only be handle in code generation, and even there with care.
	 *
	 * During the semantic analysis this should not be used
	 * as it only serves a purpose to keep the temporary value of myGovernor around for longer.
	 * */
	protected IType myLastSetGovernor;

	@Override
	/** {@inheritDoc} */
	public Setting_type getSettingtype() {
		return Setting_type.S_V;
	}

	@Override
	/** {@inheritDoc} */
	public final void copyGeneralProperties(final IValue original) {
		location = original.getLocation();
		super.setFullNameParent(original.getNameParent());
		myGovernor = original.getMyGovernor();
		myLastSetGovernor = original.getMyGovernor();
		setMyScope(original.getMyScope());
	}

	@Override
	/** {@inheritDoc} */
	public final IType getMyGovernor() {
		return myGovernor;
	}

	@Override
	/** {@inheritDoc} */
	public final void setMyGovernor(final IType governor) {
		myGovernor = governor;
		if (governor != null) {
			myLastSetGovernor = governor;
		}
	}

	@Override
	/** {@inheritDoc} */
	public final CompilationTimeStamp getLastTimeChecked() {
		return lastTimeChecked;
	}

	@Override
	/** {@inheritDoc} */
	public final void setLastTimeChecked(final CompilationTimeStamp lastTimeChecked) {
		this.lastTimeChecked = lastTimeChecked;
	}

	@Override
	/** {@inheritDoc} */
	public String chainedDescription() {
		return getFullName();
	}

	@Override
	/** {@inheritDoc} */
	public Location getChainLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		IType type = getMyGovernor();

		if (type == null) {
			type = TypeFactory.createType(getExpressionReturntype(timestamp, expectedValue));
		}
		return type;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean isUnfoldable(final CompilationTimeStamp timestamp) {
		final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final boolean result = isUnfoldable(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, referenceChain);
		referenceChain.release();

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean isUnfoldable(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		return isUnfoldable(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, referenceChain);
	}

	/**
	 * Checks the current value for the role of string encoding in decode match templates.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param lhs an assignment to check against self referencing.
	 *
	 * @return true if the value contains a reference to lhs
	 * */
	public boolean checkStringEncoding(final CompilationTimeStamp timestamp, final Assignment lhs) {
		setLoweridToReference(timestamp);

		final boolean selfReference = new CharString_Type().checkThisValue(timestamp, this, lhs, new ValueCheckingOptions(Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false, false, false, false, false));
		if (!isUnfoldable(timestamp)) {
			final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			final IValue last = getValueRefdLast(timestamp, referenceChain);
			referenceChain.release();

			if (last.getValuetype() == Value_type.CHARSTRING_VALUE) {
				final String encodingName = ((Charstring_Value)last).getValue();
				if (!"UTF-8".equals(encodingName)
					&& !"UTF-16".equals(encodingName)
					&& !"UTF-16LE".equals(encodingName)
					&& !"UTF-16BE".equals(encodingName)
					&& !"UTF-32".equals(encodingName)
					&& !"UTF-32LE".equals(encodingName)
					&& !"UTF-32BE".equals(encodingName)) {
					getLocation().reportSemanticError(MessageFormat.format("`{0}'' is not a valid encoding format", encodingName));
				}
			} else {
				getLocation().reportSemanticError("Cannot use this value in charstring value context");
			}
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public Value setValuetype(final CompilationTimeStamp timestamp, final Value_type newType) {
		setIsErroneous(true);
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		final IReferenceChain tempReferencChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final IValue temp = getValueRefdLast(timestamp, tempReferencChain);
		tempReferencChain.release();

		if (!temp.getIsErroneous(timestamp) && this != temp && referenceChain.add(this)) {
			temp.checkRecursions(timestamp, referenceChain);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReference(final CompilationTimeStamp timestamp, final Assignment assignment) {
		//simple values can not self-reference
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceValue(final CompilationTimeStamp timestamp, final Assignment lhs) {
		IType governor = myGovernor;
		if (governor == null) {
			governor = getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
		}
		if (governor == null) {
			return false;
		}

		return governor.checkThisValue(timestamp, this, lhs, new ValueCheckingOptions(Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false, true, false, false, false));
	}

	@Override
	/** {@inheritDoc} */
	public final IValue getValueRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		return getValueRefdLast(timestamp, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, referenceChain);
	}

	@Override
	/** {@inheritDoc} */
	public IValue getValueRefdLast(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return this;
		}

		setIsErroneous(false);
		lastTimeChecked = timestamp;
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public IValue setLoweridToReference(final CompilationTimeStamp timestamp) {
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public void checkExpressionOmitComparison(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (getIsErroneous(timestamp)) {
			return;
		}

		getLocation().reportSemanticError("Only a referenced value can be compared with `omit'");
		setIsErroneous(true);
	}

	@Override
	/** {@inheritDoc} */
	public boolean evaluateIsvalue(final boolean fromSequence) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean evaluateIsbound(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean evaluateIspresent(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public final Definition getDefiningAssignment() {
		INamedNode parent = getNameParent();
		while (parent != null && !(parent instanceof Definition)) {
			parent = parent.getNameParent();
		}

		return (Definition) parent;

	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsShortCircuit () {
		//fatal error
		return true;
	}

	/**
	 * Returns whether the value can be represented by an in-line Java expression.
	 *
	 *  has_single_expr in the compiler
	 * */
	public boolean canGenerateSingleExpression() {
		//TODO this might be a good location to check for the need of conversion
		return false;
	}

	/**
	 * Returns whether the generated Java expression will return a native value or a Titan object.
	 *
	 * @return true if the expression returns a native value when generated.
	 * */
	public boolean returnsNative() {
		return false;
	}
}
