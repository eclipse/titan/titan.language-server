/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.ILocatableComment;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IReferencingElement;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.ITypeWithComponents;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ICompletionItem;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * A member of an enumeration.
 *
 * @author Kristof Szabados
 * */
public final class EnumItem extends ASTNode
	implements ILocateableNode, IIncrementallyUpdatable, IReferencingElement,
	ICompletionItem, ILocatableComment {

	private static final String PROPOSAL_KIND = "enumeration";

	private final Identifier identifier;
	private Value value;
	private final boolean originalValue;

	/**
	 * The location of the whole item. This location encloses the item fully, as
	 * it is used to report errors to.
	 */
	private Location location = Location.getNullLocation();

	private Location commentLocation = null;

	public EnumItem(final Identifier identifier, final Value value) {
		this.identifier = identifier;
		this.value = value;
		this.originalValue = value != null;

		if (value != null) {
			value.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (value != null) {
			value.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	public Location getCommentLocation() {
		return commentLocation;
	}

	@Override
	public void setCommentLocation(final Location commentLocation) {
		this.commentLocation = commentLocation;
	}
	/**
	 * {@inheritDoc}
	 * @return the identifier of this enumeration item
	 */
	@Override
	public Identifier getIdentifier() {
		return identifier;
	}

	/**
	 * @return whether the value was originally assigned in the enumeration
	 * 	or later at semantic checking.
	 * */
	public boolean isOriginal() {
		return originalValue;
	}

	/**
	 * @return the value of this enumeration item
	 * */
	public Value getValue() {
		return value;
	}

	/**
	 * Sets the default value of the enumeration item.
	 *
	 * @param value the value to be set
	 * */
	public void setValue(final Value value) {
		this.value = value;
	}

	/**
	 * Does the semantic checking of the enumerations.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		// Do nothing
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		reparser.updateLocation(identifier.getLocation());
		if (value != null) {
			value.updateSyntax(reparser, false);
			reparser.updateLocation(value.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (value != null) {
			value.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (identifier!=null && !identifier.accept(v)) {
			return false;
		}
		if (value!=null && !value.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IDeclaration getDeclaration() {
		if (getMyScope() == null) {
			return null;
		}

		final Module module = getMyScope().getModuleScope();
		final Assignment assignment = module.getEnclosingAssignment(getLocation().getStartPosition());
		final IType type = assignment.getType(CompilationTimeStamp.getBaseTimestamp());

		if(type instanceof ITypeWithComponents) {
			final Identifier id = ((ITypeWithComponents) type).getComponentIdentifierByName(getIdentifier());
			return IDeclaration.createInstance(assignment, id);
		}

		return null;
	}

	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = ICompletionItem.super.getCompletionItem();
		item.setKind(CompletionItemKind.EnumMember);
		return item;
	}

	@Override
	/** {@inheritDoc} */
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(location, SemanticType.EnumMember);
		super.setSemanticInformation();
	}
}
