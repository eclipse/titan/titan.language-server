/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.util.Arrays;

import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Testcase;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Alt_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Call_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Catch_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Check_Catch_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Check_Getcall_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Check_Getreply_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Check_Port_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Check_Receive_Port_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Done_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Getcall_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Getreply_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Killed_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Receive_Port_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Timeout_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Trigger_Port_Statement;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * This class marks the following code smell:
 * A shorthand timeout/receive/trigger/getcall/catch/check/check_receive/
 * check_getcall/check_getreply/check_catch/getreply/done/killed statement
 * is used inside a function, testcase, or altstep without the 'runs on'
 * clause, except for when the statement is located inside an alt statement.
 *
 * @author Viktor Varga
 */
public class Shorthand extends BaseModuleCodeSmellSpotter {

	private static final String ERROR_MESSAGE_PREFIX = "The shorthand ";
	private static final String ERROR_MESSAGE_SUFFIX = " statement should not be used, an activated default can change its behaviour";

	private static final String NAME_TIMEOUT = "timeout";
	private static final String NAME_RECEIVE = "receive";
	private static final String NAME_TRIGGER = "trigger";
	private static final String NAME_GETCALL = "getcall";
	private static final String NAME_CATCH = "catch";
	private static final String NAME_CHECK = "check";
	private static final String NAME_CHECK_RECEIVE = "check-receive";
	private static final String NAME_CHECK_GETCALL = "check-getcall";
	private static final String NAME_CHECK_GETREPLY = "check-getreply";
	private static final String NAME_CHECK_CATCH = "check-catch";
	private static final String NAME_GETREPLY = "getreply";
	private static final String NAME_DONE = "done";
	private static final String NAME_KILLED = "killed";

	private String typename = "";

	private final CompilationTimeStamp timestamp;

	protected Shorthand() {
		super(CodeSmellType.SHORTHAND);
		timestamp = CompilationTimeStamp.getBaseTimestamp();
		
		addStartNodes(Arrays.asList(Timeout_Statement.class, Receive_Port_Statement.class, Trigger_Port_Statement.class,
			Getcall_Statement.class, Catch_Statement.class, Check_Port_Statement.class, Check_Receive_Port_Statement.class,
			Check_Getcall_Statement.class, Check_Getreply_Statement.class, Check_Catch_Statement.class, Getreply_Statement.class,
			Done_Statement.class, Killed_Statement.class));
	}

	@Override
	protected void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof Timeout_Statement) {
			typename = NAME_TIMEOUT;
		} else if (node instanceof Receive_Port_Statement) {
			typename = NAME_RECEIVE;
		} else if (node instanceof Trigger_Port_Statement) {
			typename = NAME_TRIGGER;
		} else if (node instanceof Getcall_Statement) {
			typename = NAME_GETCALL;
		} else if (node instanceof Catch_Statement) {
			typename = NAME_CATCH;
		} else if (node instanceof Check_Port_Statement) {
			typename = NAME_CHECK;
		} else if (node instanceof Check_Receive_Port_Statement) {
			typename = NAME_CHECK_RECEIVE;
		} else if (node instanceof Check_Getcall_Statement) {
			typename = NAME_CHECK_GETCALL;
		} else if (node instanceof Check_Catch_Statement) {
			typename = NAME_CHECK_CATCH;
		} else if (node instanceof Check_Getreply_Statement) {
			typename = NAME_CHECK_GETREPLY;
		} else if (node instanceof Getreply_Statement) {
			typename = NAME_GETREPLY;
		} else if (node instanceof Done_Statement) {
			typename = NAME_DONE;
		} else if (node instanceof Killed_Statement) {
			typename = NAME_KILLED;
		} else {
			return;
		}
		final Statement s = (Statement)node;
		check(s, problems);
	}

	protected void check(final Statement statement, final Problems problems) {
		if (statement == null) {
			return;
		}
		
		// do not complain about shorthand statements if the <b>@nodefault</b> modifier is present
		if (statement.hasNoDefault()) {
			return;
		}
		
		//shorthand statements are ignored inside alt statements
		INamedNode curr = statement;
		while (curr != null) {
			if (curr instanceof Alt_Statement || curr instanceof Call_Statement) {
				return;
			}
			curr = curr.getNameParent();
		}
		final StatementBlock sb = statement.getMyStatementBlock();
		if (sb == null) {
			return;
		}
		final Definition definition = sb.getMyDefinition();
		if (definition == null) {
			return;
		}
		
		//shorthand statements are marked in functions, test cases and altsteps that have a 'runs on' clause
		if (definition instanceof Def_Function && ((Def_Function)definition).getRunsOnType(timestamp) != null) {
			problems.report(statement.getLocation(), ERROR_MESSAGE_PREFIX + typename + ERROR_MESSAGE_SUFFIX);
			return;
		}
		if (definition instanceof Def_Altstep || definition instanceof Def_Testcase) {
			problems.report(statement.getLocation(), ERROR_MESSAGE_PREFIX + typename + ERROR_MESSAGE_SUFFIX);
		}
	}
}
