/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.List;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.CompletionParams;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;

public class CompletionProvider {
	private final Position cursorPosition;
	private final TextDocumentIdentifier textDocument;
	private Module module;
	private String sourceText;
	private Scope scope;
	private CompletionContextInfo contextInfo;


 	public CompletionProvider(CompletionParams params) {
		cursorPosition = new Position(params.getPosition());
		textDocument = params.getTextDocument();
		final Project project = Project.INSTANCE;
		final ProjectItem projectItem = project.getProjectItem(IOUtils.getTextDocumentPath(params.getTextDocument()));
		if (projectItem != null) {
			sourceText = projectItem.getSource();
			module = projectItem.getModule();
			if (module != null) {
				scope = module.getSmallestEnclosingScope(cursorPosition);
			}
		}

		contextInfo = collectContextInfo();
	}

 	private CompletionContextInfo collectContextInfo() {
 		final ContextStringLocator locator = new ContextStringLocator(scope, module, sourceText, cursorPosition);
 		final String contextString = locator.getContextString();
 		final CompletionContextInfo info = new CompletionContextInfo(
 				cursorPosition,
 				textDocument,
				module,
				scope,
				contextString);
 		info.actualStatement = locator.getActualStatement();
 		info.actualDefinition = locator.getActualDefinition();
 		return info;
 	}

	public Either<List<CompletionItem>, CompletionList> getComplitions() {
		final CompletionContext completionContext = CompletionContextFactory.getContext(contextInfo);
		if (completionContext == null) {
			return null;
		}
		return completionContext.getCompletions();
	}
}
