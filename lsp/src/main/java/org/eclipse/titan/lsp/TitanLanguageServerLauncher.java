/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.eclipse.lsp4j.jsonrpc.Launcher;
import org.eclipse.lsp4j.launch.LSPLauncher;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.titan.lsp.TitanLanguageServer.ServerMode;
import org.eclipse.titan.lsp.commandline.CommandLineConfiguration;
import org.eclipse.titan.lsp.commandline.CommandLineExecutor;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.utils.IOUtils;
import org.eclipse.titan.lsp.common.utils.IOUtils.IOStreams;

/**
 *
 * @author Adam Knapp
 * @author Miklos Magyari
 *
 */
public final class TitanLanguageServerLauncher {
    public static void main( String[] args ) {
        InputStream in = System.in;
        OutputStream out = System.out;
        // Redirect System.out to System.err to prevent
        // System.out from receiving anything else than an LSP message
        System.setOut(new PrintStream(System.err));

    	boolean pipe = false, socket = false, clientMode = false;
    	int portNumber = 55555;
    	String pipeName = "", host = "";
   
    	final CommandLineConfiguration cmdConfig = new CommandLineConfiguration();

    	String[] tmpArgs = args;
    	if (tmpArgs.length == 1) {
    		tmpArgs = tmpArgs[0].split(" ");
    	}

    	ServerMode mode = ServerMode.LanguageServer;
    	for (int i = 0; i < tmpArgs.length; i++) {
    		switch (tmpArgs[i]) {
    		case "--pipe": {
    			if (pipe) {
    				TitanLogger.logInfo("pipe argument is already set: " + pipeName);
    				System.exit(-1);
    			}
    			final String tmp = tmpArgs[++i];
    			try {
    				IOUtils.checkPipe(tmp);
    				TitanLogger.logInfo("Pipe based communication between the language server and client is not supported.");
    				System.exit(-1);
    			} catch (IOException e) {
    				TitanLogger.logInfo("Invalid pipe: " + pipeName);
    				System.exit(-1);
				}
    			pipe = true;
    			pipeName = tmp;
    			break;
    		}
    		case "--socket": {
    			if (socket) {
    				TitanLogger.logInfo("socket argument is already set: "
    						+ (host.isEmpty() ? portNumber : host + ":" + portNumber));
    				System.exit(-1);
    			}
    			final String tmpArg = tmpArgs[++i];
				final String[] splitted = tmpArg.toLowerCase().split(":");
				if (splitted.length > 2 || splitted.length == 0) {
					TitanLogger.logInfo("Invalid socket argument: " + tmpArg);
					System.exit(-1);
				}
				if (splitted.length == 2) {
					host = splitted[0];
					clientMode = true;
				}
    			try {
    				portNumber = Integer.parseInt(splitted[splitted.length-1]);
    				if (portNumber < 0 || portNumber > 65535) {
    					TitanLogger.logInfo("Port number must be between 0 and 65535 instead of " + portNumber);
    					System.exit(-1);
    				}
    			} catch (NumberFormatException e) {
    				TitanLogger.logInfo("Port number is not a number: " + splitted[splitted.length-1]);
    				System.exit(-1);
    			}
    			socket = true;
    			break;
    		}
    		case "--cmd": {
    			mode = ServerMode.CommandLineAnalyzer;
    			break;
    		}
    		case "--config": {
    			if (i < tmpArgs.length - 1) {
    				cmdConfig.cfgFile = tmpArgs[++i];
    			} else {
    				TitanLogger.logInfo("Missing config file name");
    				System.exit(-1);
    			}
    			break;
    		}
    		case "--nosa": {
    			cmdConfig.titaniumErrorMarkers = false;
    			cmdConfig.titaniumWarningMarkers = false;
    			break;
    		}
    		case "--sa": {
    			cmdConfig.titaniumErrorMarkers = true;
    			cmdConfig.titaniumWarningMarkers = true;
    			break;
    		}
    		case "--nottcn": {
    			cmdConfig.ttcnErrorMarkers = false;
    			cmdConfig.ttcnWarningMarkers = false;
    			break;
    		}
    		case "--ttcn": {
    			cmdConfig.ttcnErrorMarkers = true;
    			cmdConfig.ttcnWarningMarkers = true;
    			break;
    		}
    		case "--oop": {
    			cmdConfig.oopEnabled = true;
    			break;
    		}
    		case "--nooop": {
    			cmdConfig.oopEnabled = false;
    			break;
    		}
    		case "--naming": {
    			cmdConfig.namingConventionsEnabled = true;
    			break;
    		}
    		case "--nonaming": {
    			cmdConfig.namingConventionsEnabled = false;
    			break;
    		}
    		case "--outfile": {
    			if (i < tmpArgs.length - 1) {
    				cmdConfig.outFile= tmpArgs[++i];
    			} else {
    				TitanLogger.logInfo("Missing out file name");
    				System.exit(-1);
    			}
    			break;
    		}
    		case "--root": {
    			if (i < tmpArgs.length - 1) {
    				cmdConfig.rootFolder = tmpArgs[++i];
    			} else {
    				TitanLogger.logInfo("Missing root folder name");
    				System.exit(-1);
    			}
    			break;
    		}
    		case "--ttcnFormat": {
    			if (i < tmpArgs.length - 1) {
    				cmdConfig.ttcnFormat = tmpArgs[++i];
    			}
    			break;
    		}
    		default: {
    			TitanLogger.logInfo("Unknown argument: " + tmpArgs[i]);
    			break;
    		}
    		}
    	}
    	if (mode == ServerMode.CommandLineAnalyzer) {
    		final CommandLineExecutor cmdExecutor = new CommandLineExecutor(cmdConfig);
    		cmdExecutor.runCommandLineAnalyzer();
    		System.exit(0);
    	}
    	if (pipe && socket) {
    		TitanLogger.logInfo("Both pipe and socket arguments are set, using pipe");
    		socket = false;
    	}

        IOStreams iostreams = null;
        Socket clientSocket = null;
        ServerSocket serverSocket = null;

        if (pipe) {
        	try {
        		iostreams = IOUtils.openPipe(pipeName);
        		in = iostreams.in;
        		out = iostreams.out;
        	} catch (IOException e) {
        		TitanLogger.logError(e);
        		System.exit(-1);
			}
        } else if (socket) {
        	if (clientMode) {
        		try {
        			TitanLogger.logInfo("Connecting to socket: " + host + ":" + portNumber);
        			clientSocket = new Socket(host, portNumber);
        			TitanLogger.logInfo("Connected to socket: " + host + ":" + portNumber);
        			in = clientSocket.getInputStream();
        			out = clientSocket.getOutputStream();
        		} catch (IOException e) {
        			TitanLogger.logError(e);
        			System.exit(-1);
        		}
        	} else {
        		try {
					serverSocket = new ServerSocket(portNumber, 10, InetAddress.getLoopbackAddress());
					TitanLogger.logInfo("Listening on socket: localhost:" + portNumber);
					clientSocket = serverSocket.accept();
					TitanLogger.logInfo("New client connected");
				} catch (IOException e) {
					TitanLogger.logError(e);
					System.exit(-1);
				}
        	}
        }

        try {
        	TitanLanguageServer server = new TitanLanguageServer();
            Launcher<LanguageClient> launcher = LSPLauncher.createServerLauncher(server, in, out);

            LanguageClient client = launcher.getRemoteProxy();
            server.connect(client);

            Future<?> startListening = launcher.startListening();

            startListening.get();
        } catch (ExecutionException e) {
        	TitanLogger.logError(e);
        } catch (InterruptedException e) {
        	TitanLogger.logError(e);
        	Thread.currentThread().interrupt();
        } finally {
        	IOUtils.closeQuietly(in, out);
        	if (pipe) {
        		IOUtils.closeQuietly(iostreams.closeable);
        	}
        	IOUtils.closeQuietly(clientSocket, serverSocket);
        }
    }
}
