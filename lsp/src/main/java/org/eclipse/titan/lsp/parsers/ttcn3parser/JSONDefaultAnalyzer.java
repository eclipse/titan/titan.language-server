/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.ttcn3parser;

import java.io.Reader;
import java.io.StringReader;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;

import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Parser.Pr_SingleExpressionContext;

/**
 * This is helper class, letting us parse JSON default attribute values as TTCN-3 values.
 * 
 * FIXME most probably not the final location for this function, but a good starting location.
 * 
 * @author Kristof Szabados
 */
public class JSONDefaultAnalyzer {
	
	public IValue parseJSONDefaultValue(final String defaultString, final Location location) {
		final Reader reader = new StringReader( defaultString );
		final CharStream charStream = new UnbufferedCharStream( reader );
		final Ttcn3Lexer lexer = new Ttcn3Lexer( charStream );
		lexer.setTokenFactory( new CommonTokenFactory( true ) );
		lexer.initRootInterval( defaultString.length() );
		lexer.removeErrorListeners();

		final CommonTokenStream tokenStream = new CommonTokenStream( lexer );
		final Ttcn3Parser parser = new Ttcn3Parser( tokenStream );
		ParserUtilities.setBuildParseTree( parser );

		lexer.setActualFile(location.getFile());
		parser.setActualFile(location.getFile());
		parser.setLine(location.getStartLine());
		
		final int offset = ParserUtilities.calculateOffset(location.getFile(), location.getStartPosition());
		parser.setOffset(offset + 1);
		final TitanErrorListener lexerListener = new TitanErrorListener(location.getFile());
		// remove ConsoleErrorListener
		lexer.removeErrorListeners();
		lexer.addErrorListener(lexerListener);
		parser.removeErrorListeners();
		final TitanErrorListener parserListener = new TitanErrorListener(location.getFile());
		parser.addErrorListener( parserListener );

		final Pr_SingleExpressionContext root = parser.pr_SingleExpression();
		parser.pr_EndOfFile();

		for (final SyntacticErrorStorage ses : lexerListener.getErrorsStored()) {
			ses.reportSyntacticError();
		}
		for (final SyntacticErrorStorage ses : parserListener.getErrorsStored()) {
			ses.reportSyntacticError();
		}

		//no syntax errors found
		return root.value;
	}
}
