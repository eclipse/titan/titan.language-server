/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction.data;

import org.eclipse.titan.lsp.AST.Location;

/**
 * This class represents extra data needed for a 'missing function call'
 * quick fix.
 * 
 * @author Miklos Magyari
 *
 */
public class MissingFunctionData {
	public String code;
	public Location definitionLocation;
	public Location memberLocation;
	
	public MissingFunctionData(final String code, final Location definitionLocation, final Location memberLocation) {
		this.code = code;
		this.definitionLocation = definitionLocation;
		this.memberLocation = memberLocation;
	}
}
