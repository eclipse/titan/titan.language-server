/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.util.EnumSet;
import java.util.List;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.DocumentComment.CommentTag;
import org.eclipse.titan.lsp.AST.TTCN3.statements.AltGuard;
import org.eclipse.titan.lsp.AST.TTCN3.statements.AltGuards;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The Def_Altstep class represents TTCN3 altstep definitions.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class Def_Altstep extends Definition implements ISignatureHelp {
	private static final String FULLNAMEPART1 = ".<formal_parameter_list>";
	private static final String FULLNAMEPART2 = ".<runs_on_type>";
	private static final String FULLNAMEPART3 = ".<block>";
	private static final String FULLNAMEPART4 = ".<guards>";
	private static final String FULLNAMEPART5 = ".<mtc_type>";
	private static final String FULLNAMEPART6 = ".<system_type>";
	private static final String FULLNAMEPART7 = ".<finally_block>"; 

	private static final String DASHALLOWEDONLYFORTEMPLATES = "Using not used symbol (`-') as the default parameter"
			+ " is allowed only for modified templates";

	private static final String KIND = "altstep";

	private final FormalParameterList formalParList;
	private final Reference runsOnRef;
	private Component_Type runsOnType = null;
	private final Reference mtcRef;
	private Component_Type mtcType = null;
	private final Reference systemRef;
	private Component_Type systemType = null;
	private final StatementBlock block;
	private final AltGuards altGuards;
	private NamedBridgeScope bridgeScope = null;

	public Def_Altstep(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final StatementBlock block, 
			final AltGuards altGuards) {
		super(identifier);
		this.formalParList = formalParameters;
		this.runsOnRef = runsOnRef;
		this.mtcRef = mtcReference;
		this.systemRef = systemReference;
		this.block = block;
		this.altGuards = altGuards;

		if (formalParList != null) {
			formalParList.setMyDefinition(this);
			formalParList.setFullNameParent(this);
		}
		if (runsOnRef != null) {
			runsOnRef.setFullNameParent(this);
		}
		if (mtcReference != null) {
			mtcReference.setFullNameParent(this);
		}
		if (systemReference != null) {
			systemReference.setFullNameParent(this);
		}
		if (block != null) {
			block.setMyDefinition(this);
		}
		if (altGuards != null) {
			altGuards.setMyDefinition(this);
		}
	}

	public AltGuards getAltGuards() {
		return altGuards;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_ALTSTEP;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (formalParList == child) {
			return builder.append(FULLNAMEPART1);
		} else if (runsOnRef == child) {
			return builder.append(FULLNAMEPART2);
		} else if (block == child) {
			return builder.append(FULLNAMEPART3);
		} else if (altGuards == child) {
			return builder.append(FULLNAMEPART4);
		} else if (mtcRef == child) {
			return builder.append(FULLNAMEPART5);
		} else if (systemRef == child) {
			return builder.append(FULLNAMEPART6);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public FormalParameterList getFormalParameterList() {
		return formalParList;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		return getAssignmentName();
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalDescription() {
		final StringBuilder nameBuilder = new StringBuilder(identifier.getDisplayName());
		nameBuilder.append('(');
		formalParList.getAsProposalDesriptionPart(nameBuilder);
		nameBuilder.append(')');
		return nameBuilder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		if (bridgeScope != null && bridgeScope.getParentScope() == scope) {
			return;
		}

		bridgeScope = new NamedBridgeScope();
		bridgeScope.setParentScope(scope);
		scope.addSubScope(getLocation(), bridgeScope);
		bridgeScope.setScopeMacroName(identifier.getDisplayName());

		super.setMyScope(bridgeScope);
		if (runsOnRef != null) {
			runsOnRef.setMyScope(bridgeScope);
		}
		if (mtcRef != null) {
			mtcRef.setMyScope(bridgeScope);
		}
		if (systemRef != null) {
			systemRef.setMyScope(bridgeScope);
		}
		formalParList.setMyScope(bridgeScope);
		if (block != null) {
			block.setMyScope(formalParList);
			altGuards.setMyScope(block);
			bridgeScope.addSubScope(block.getLocation(), block);
		}
		
		bridgeScope.addSubScope(formalParList.getLocation(), formalParList);
		if (altGuards != null) {
			for (int i = 0; i < altGuards.getNofAltguards(); i++) {
				final AltGuard ag = altGuards.getAltguardByIndex(i);
				final StatementBlock sb = ag.getStatementBlock();
				if (sb != null) {
					bridgeScope.addSubScope(sb.getLocation(), sb);
				}
			}
		}
	}

	public Component_Type getRunsOnType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return runsOnType;
	}

	public Reference getRunsOnReference(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return runsOnRef;
	}

	public Component_Type getMTCType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return mtcType;
	}

	public Component_Type getSystemType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return systemType;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		runsOnType = null;
		mtcType = null;
		systemType = null;
		lastTimeChecked = timestamp;

		if (runsOnRef != null) {
			runsOnType = runsOnRef.chkComponentypeReference(timestamp);
			if (runsOnType != null) {
				final Scope formalParlistPreviosScope = formalParList.getParentScope();
				if (formalParlistPreviosScope instanceof RunsOnScope
						&& ((RunsOnScope) formalParlistPreviosScope).getParentScope() == myScope) {
					((RunsOnScope) formalParlistPreviosScope).setComponentType(runsOnType);
				} else {
					final Scope tempScope = new RunsOnScope(runsOnType, myScope);
					formalParList.setMyScope(tempScope);
				}
			}
		}
		if (mtcRef != null) {
			mtcType = mtcRef.chkComponentypeReference(timestamp);
		}
		if (systemRef != null) {
			systemType = systemRef.chkComponentypeReference(timestamp);
		}

		if (formalParList.hasNotusedDefaultValue()) {
			formalParList.getLocation().reportSemanticError(DASHALLOWEDONLYFORTEMPLATES);
		}

		boolean canSkip = false;
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					canSkip = true;
				}
			}
		}

		if(!canSkip) {
			formalParList.reset();
		}
		formalParList.check(timestamp, getAssignmentType());

		if(canSkip) {
			return;
		}

		NamingConventionHelper.checkConvention(NamingConventionElement.Altstep, identifier, this);
		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (block != null) {
			block.check(timestamp);
		}
		
		if (altGuards != null) {
			altGuards.setIsAltstep();
			altGuards.setMyAltguards(altGuards);
			altGuards.setMyLaicStmt(altGuards, null);
			altGuards.check(timestamp);
		}

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
		}

		if (block != null) {
			block.postCheck();
		}
		if (altGuards != null) {
			altGuards.postCheck();
		}
		
		checkDocumentComment();
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					return;
				}
			}
		}

		super.postCheck();

		formalParList.postCheck();
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		final StringBuilder text = new StringBuilder(identifier.getDisplayName());
		if (formalParList == null || lastTimeChecked == null) {
			return text.toString();
		}

		text.append('(');
		for (int i = 0; i < formalParList.size(); i++) {
			if (i != 0) {
				text.append(", ");
			}
			final FormalParameter parameter = formalParList.get(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				text.append("timer");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					text.append("Unknown type");
				} else {
					text.append(type.getTypename());
				}
			}
		}
		text.append(')');
		return text.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					removeBridge();
					throw new ReParseException(result);
				}
			}

			if (formalParList != null) {
				if (enveloped) {
					formalParList.updateSyntax(reparser, false);
					reparser.updateLocation(formalParList.getLocation());
				} else if (reparser.envelopsDamage(formalParList.getLocation())) {
					try {
						formalParList.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(formalParList.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (runsOnRef != null) {
				if (enveloped) {
					runsOnRef.updateSyntax(reparser, false);
					reparser.updateLocation(runsOnRef.getLocation());
				} else if (reparser.envelopsDamage(runsOnRef.getLocation())) {
					try {
						runsOnRef.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(runsOnRef.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (mtcRef != null) {
				if (enveloped) {
					mtcRef.updateSyntax(reparser, false);
					reparser.updateLocation(mtcRef.getLocation());
				} else if (reparser.envelopsDamage(mtcRef.getLocation())) {
					try {
						mtcRef.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(mtcRef.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (systemRef != null) {
				if (enveloped) {
					systemRef.updateSyntax(reparser, false);
					reparser.updateLocation(systemRef.getLocation());
				} else if (reparser.envelopsDamage(systemRef.getLocation())) {
					try {
						systemRef.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(systemRef.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (altGuards != null) {
				if (enveloped) {
					altGuards.updateSyntax(reparser, false);
					reparser.updateLocation(altGuards.getLocation());
				} else if (reparser.envelopsDamage(altGuards.getLocation())) {
					try {
						altGuards.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(altGuards.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (block != null) {
				if (enveloped) {
					block.updateSyntax(reparser, false);
					reparser.updateLocation(block.getLocation());
				} else if (reparser.envelopsDamage(block.getLocation())) {
					try {
						block.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(block.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					try {
						withAttributesPath.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(withAttributesPath.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (!enveloped) {
				removeBridge();
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (formalParList != null) {
			formalParList.updateSyntax(reparser, false);
			reparser.updateLocation(formalParList.getLocation());
		}

		if (runsOnRef != null) {
			runsOnRef.updateSyntax(reparser, false);
			reparser.updateLocation(runsOnRef.getLocation());
		}

		if (mtcRef != null) {
			mtcRef.updateSyntax(reparser, false);
			reparser.updateLocation(mtcRef.getLocation());
		}

		if (systemRef != null) {
			systemRef.updateSyntax(reparser, false);
			reparser.updateLocation(systemRef.getLocation());
		}

		if (block != null) {
			block.updateSyntax(reparser, false);
			reparser.updateLocation(block.getLocation());
		}

		if (altGuards != null) {
			altGuards.updateSyntax(reparser, false);
			reparser.updateLocation(altGuards.getLocation());
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	/**
	 * Removes the name bridging scope.
	 * */
	private void removeBridge() {
		if (bridgeScope != null) {
			bridgeScope.remove();
			bridgeScope = null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (formalParList != null) {
			formalParList.findReferences(referenceFinder, foundIdentifiers);
		}
		if (runsOnRef != null) {
			runsOnRef.findReferences(referenceFinder, foundIdentifiers);
		}
		if (mtcRef != null) {
			mtcRef.findReferences(referenceFinder, foundIdentifiers);
		}
		if (systemRef != null) {
			systemRef.findReferences(referenceFinder, foundIdentifiers);
		}
		if (block != null) {
			block.findReferences(referenceFinder, foundIdentifiers);
		}
		if (altGuards != null) {
			altGuards.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (formalParList != null && !formalParList.accept(v)) {
			return false;
		}
		if (runsOnRef != null && !runsOnRef.accept(v)) {
			return false;
		}
		if (mtcRef != null && !mtcRef.accept(v)) {
			return false;
		}
		if (systemRef != null && !systemRef.accept(v)) {
			return false;
		}
		if (block != null && !block.accept(v)) {
			return false;
		}
		if (altGuards != null && !altGuards.accept(v)) {
			return false;
		}
		return true;
	}

	public int nofBranches() {
		return altGuards.getNofAltguards();
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}
		
		hoverContent.addStyledText(KIND, Ttcn3HoverContent.BOLD).addText(" ");
		hoverContent.addStyledText(getFullNameForDocComment());
		if (runsOnRef != null) {
			hoverContent.addText(" runs on ").addStyledText(runsOnRef.getDisplayName(), Ttcn3HoverContent.BOLD);
		}
		hoverContent.addText("\n\n");

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addParamsContent(hoverContent, formalParList);
			dc.addVerdictsContent(hoverContent);
			dc.addRequirementsContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		} else {
			addFormalParameterListToHoverContent(hoverContent, lastTimeChecked);
		}
		
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding);

		if (formalParList!= null && formalParList.size() > 0) {
			for (final FormalParameter param : formalParList) {
				sb.append(ind).append(ICommentable.PARAM_TAG).append(" ")
					.append(param.getIdentifier().getDisplayName()).append(lineEnding);
			}
		}

		sb.append(ind).append(ICommentable.VERDICT_TAG).append(" pass ").append(lineEnding)
			.append(ind).append(ICommentable.VERDICT_TAG).append(" fail ").append(lineEnding)
			.append(ind).append(ICommentable.VERDICT_TAG).append(" inconc ").append(lineEnding);

		sb.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}
	
	@Override
	public void checkDocumentComment() {
		if (!hasDocumentComment()) {
			return;
		}

		getDocumentComment().checkNonApplicableTags(
			EnumSet.of(CommentTag.Config, CommentTag.Exception, CommentTag.Member, CommentTag.Priority, CommentTag.Purpose), "altsteps");

		super.checkDocumentComment();
	}
}
