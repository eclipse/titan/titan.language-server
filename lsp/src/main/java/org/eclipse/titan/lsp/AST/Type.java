/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.parsers.CharstringExtractor;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.ASN1.Value_Assignment;
import org.eclipse.titan.lsp.AST.ASN1.types.ASN1_Set_Seq_Choice_BaseType;
import org.eclipse.titan.lsp.AST.ASN1.types.Open_Type;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.BerAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.JsonAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifier;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.RawAST;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Modifier_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Const;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Var_Template;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Group;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.IParameterisedAssignment;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.templates.ITTCN3Template.Template_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.SpecificValue_Template;
import org.eclipse.titan.lsp.AST.TTCN3.types.Anytype_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Array_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.BitString_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.CharString_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Float_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Function_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Integer_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.OctetString_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.SequenceOf_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.SetOf_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.TTCN3_Set_Seq_Choice_BaseType;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.IParsedSubType;
import org.eclipse.titan.lsp.AST.TTCN3.types.subtypes.SubType;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value.Operation_type;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Referenced_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.ExpressionStruct;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.JSONDefaultAnalyzer;
import org.eclipse.titan.lsp.parsers.variantattributeparser.VariantAttributeAnalyzer;
import org.eclipse.titan.lsp.semantichighlighting.ISemanticInformation;


/**
 * The Type class is the base class for types.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 * */
public abstract class Type extends Setting
	implements IType, IIncrementallyUpdatable, IOutlineElement, ISemanticInformation, ICommentable, ICancelBuild {

	private static final String INCOMPATIBLEVALUE = "Incompatible value: `{0}'' was expected";
	private static final String REFTOVALUEEXPECTED = "Reference to a value was expected instead of {0}";
	private static final String REFTOVALUEEXPECTED_INSTEADOFCALL = "Reference to a value was expected instead of a call of {0}, which return a template";
	private static final String TYPECOMPATWARNING = "Type compatibility between `{0}'' and `{1}''";
	private static final String PORTRETURN = "Port type `{0}'' cannot be the return type of a `{1}''";
	private static final String SIGNATURERETURN = "A value of signature `{0}'' cannot be the return type of a`{1}''";

	public static final int JSON_NONE       = 0x00; // no value type set (default)
	public static final int JSON_NUMBER     = 0x01; // integer and float
	public static final int JSON_STRING     = 0x02; // all string types, the objid type, the verdict type and enumerated values
	public static final int JSON_BOOLEAN    = 0x04; // boolean (true or false)
	public static final int JSON_OBJECT     = 0x08; // records, sets, unions and the anytype
	public static final int JSON_ARRAY      = 0x10; // record of, set of and array
	public static final int JSON_NULL       = 0x20; // ASN.1 null type
	public static final int JSON_ANY_VALUE  = 0x3F; // unions with the "as value" coding instruction

	/** the parent type of this type */
	private IType parentType;

	/** The constraints assigned to this type. */
	protected Constraints constraints = null;

	/** The with attributes assigned to the definition of this type */
	// TODO as this is only used for TTCN-3 types maybe we could save some
	// memory, by moving it ... but than we waste runtime.
	protected WithAttributesPath withAttributesPath = null;
	public List<MessageEncoding_type> codersToGenerate = new ArrayList<>();
	public RawAST rawAttribute = null;
	public JsonAST jsonAttribute = null;
	public BerAST berAttribute = null;

	private boolean hasDone = false;
	/** Indicates that the component array version (used with the help of the
	 * 'any from' clause) of the 'done' function needs to be generated for
	 *  this type. */
	boolean needs_any_from_done = false;

	/** The list of parsed sub-type restrictions before they are converted */
	protected List<IParsedSubType> parsedRestrictions = null;

	/** The sub-type restriction created from the parsed restrictions */
	protected SubType subType = null;

	protected List<Coding_Type> codingTable = new ArrayList<>();

	/** What kind of AST element owns the type.
	 *  It may not be known at creation type, so it's initially OT_UNKNOWN.
	 *  We want this information so we don't have to bother with XER
	 *  if the type is an ASN.1 construct, or it's the type in a "runs on" scope,
	 *  the type of a variable declaration/module par/const, etc. */
	protected TypeOwner_type ownerType = TypeOwner_type.OT_UNKNOWN;
	protected INamedNode owner = null;//TODO needs to check if this is the tightest interface once structure is ready to reveal all usage.

	/** The time when code for this type was generated. */
	protected BuildTimestamp lastTimeGenerated = null;

	/** The actual value of the severity level to report type compatibility on. */
	private static DiagnosticSeverity typeCompatibilitySeverity;

	/**
	 * If typeCompatibilitySeverity is set to Error this is true, in this
	 * case structured types must be nominally compatible
	 */
	protected static boolean noStructuredTypeCompatibility;

	/** Optional documentation comment according to ETSI ES 201 873-10 */
	protected DocumentComment documentComment = null;

	/** content of hover information for this type */
	protected Ttcn3HoverContent hoverContent = new Ttcn3HoverContent();

	static {
		final String severity = Configuration.INSTANCE.getString(Configuration.TYPE_COMPATIBILITY_SEVERITY, "warning");
		setTypeCompatibilitySeverity(severity);

		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.TYPE_COMPATIBILITY_SEVERITY,
				preference -> setTypeCompatibilitySeverity(severity)
		);
	}

	private static void setTypeCompatibilitySeverity(final String severity) {
		if (severity == null) {
			typeCompatibilitySeverity = DiagnosticSeverity.Warning;
		} else {
			switch (severity.toLowerCase()) {
			case "error":
				typeCompatibilitySeverity = DiagnosticSeverity.Error;
				break;
			case "warning":
			default:
				typeCompatibilitySeverity = DiagnosticSeverity.Warning;
			}
		}
		noStructuredTypeCompatibility = (typeCompatibilitySeverity == DiagnosticSeverity.Error);
	}

	@Override
	/** {@inheritDoc} */
	public Setting_type getSettingtype() {
		return Setting_type.S_T;
	}

	@Override
	/** {@inheritDoc} */
	public final IType getParentType() {
		return parentType;
	}

	@Override
	/** {@inheritDoc} */
	public final void setParentType(final IType type) {
		parentType = type;
	}

	@Override
	/** {@inheritDoc} */
	public final WithAttributesPath getAttributePath() {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		return withAttributesPath;
	}

	@Override
	/** {@inheritDoc} */
	public void setAttributeParentPath(final WithAttributesPath parent) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		withAttributesPath.setAttributeParent(parent);
	}

	@Override
	/** {@inheritDoc} */
	public final void clearWithAttributes() {
		if (withAttributesPath != null) {
			withAttributesPath.setWithAttributes(null);
		}
	}

	@Override
	/** {@inheritDoc} */
	public final void setWithAttributes(final MultipleWithAttributes attributes) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		withAttributesPath.setWithAttributes(attributes);
	}

	/**
	 * Returns a new type representing the encoded stream of the given
	 * encoding type.
	 *
	 * @param encodingType
	 *                the encoding type.
	 * @param streamVariant
	 *                the variant of the stream.
	 * @return the temporary type.
	 * */
	public static Type getStreamType(final MessageEncoding_type encodingType, final int streamVariant) {
		if (streamVariant == 0) {
			return new BitString_Type();
		}
		switch (encodingType) {
		case BER:
		case PER:
		case RAW:
		case XER:
		case JSON:
		case OER:
			return new OctetString_Type();
		case TEXT:
			if (streamVariant == 1) {
				return new CharString_Type();
			} else {
				return new OctetString_Type();
			}
		default:
			return null;
		}
	}

	@Override
	/** {@inheritDoc} */
	public final boolean hasDoneAttribute() {
		return hasDone;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isTagged() {
		//FIXME implement support for tags.
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean isConstrained() {
		return constraints != null;
	}

	@Override
	/** {@inheritDoc} */
	public final void addConstraints(final Constraints constraints) {
		if (constraints == null) {
			return;
		}

		this.constraints = constraints;
		constraints.setMyType(this);
		constraints.setFullNameParent(this);
	}

	@Override
	/** {@inheritDoc} */
	public final Constraints getConstraints() {
		return constraints;
	}

	@Override
	/** {@inheritDoc} */
	public final SubType getSubtype() {
		return subType;
	}

	@Override
	/** {@inheritDoc} */
	public final void setParsedRestrictions(final List<IParsedSubType> parsedRestrictions) {
		this.parsedRestrictions = parsedRestrictions;
	}

	@Override
	/** {@inheritDoc} */
	public String chainedDescription() {
		return "type reference: " + getFullName();
	}

	@Override
	/** {@inheritDoc} */
	public final Location getChainLocation() {
		return getLocation();
	}

	@Override
	/** {@inheritDoc} */
	public RawAST getRawAttribute() {
		return rawAttribute;
	}

	@Override
	/** {@inheritDoc} */
	public JsonAST getJsonAttribute() {
		return jsonAttribute;
	}

	@Override
	/** {@inheritDoc} */
	public BerAST getBerAttribute() {
		return berAttribute;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean hasRawAttributes(final CompilationTimeStamp timestamp) {
		if (rawAttribute != null) {
			return true;
		}

		if (withAttributesPath == null) {
			return false;
		}

		if (withAttributesPath.getHadGlobalVariants()) {
			return true;
		}

		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);
		for (final SingleWithAttribute att : realAttributes) {
			if (SingleWithAttribute.Attribute_Type.Variant_Attribute.equals(att.getAttributeType())) {
				return true;
			}
		}

		final MultipleWithAttributes localAttributes = withAttributesPath.getAttributes();
		if (localAttributes == null) {
			return false;
		}

		for (final SingleWithAttribute tempSingle : localAttributes) {
			if (Attribute_Type.Variant_Attribute.equals(tempSingle.getAttributeType()) &&
				(tempSingle.getQualifiers() == null || tempSingle.getQualifiers().size() == 0)) {
				withAttributesPath.setHadGlobalVariants(true);
				return true;
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public final boolean hasVariantAttributes(final CompilationTimeStamp timestamp) {
		if (withAttributesPath == null) {
			return false;
		}

		if (withAttributesPath.getHadGlobalVariants()) {
			return true;
		}

		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);
		for (final SingleWithAttribute att : realAttributes) {
			if (SingleWithAttribute.Attribute_Type.Variant_Attribute.equals(att.getAttributeType())) {
				return true;
			}
		}

		final MultipleWithAttributes localAttributes = withAttributesPath.getAttributes();
		if (localAttributes == null) {
			return false;
		}

		for (final SingleWithAttribute tempSingle : localAttributes) {
			if (Attribute_Type.Variant_Attribute.equals(tempSingle.getAttributeType())) {
				withAttributesPath.setHadGlobalVariants(true);
				return true;
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void initAttributes(final CompilationTimeStamp timestamp) {
		// codingTable.clear();
		hasDone = false;

		checkDoneAttribute(timestamp);
	}

	@Override
	/** {@inheritDoc} */
	public final void checkDoneAttribute(final CompilationTimeStamp timestamp) {
		hasDone = false;

		if (withAttributesPath == null) {
			return;
		}

		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);
		for (final SingleWithAttribute att : realAttributes) {
			if (Attribute_Type.Extension_Attribute.equals(att.getAttributeType())
					&& GeneralConstants.DONE.equals(att.getAttributeSpecification().getSpecification())) {
				hasDone = true;
			}
		}
	}

	/**
	 * Does the semantic checking of the type.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 */
	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	public void getTypesWithNoCodingTable(final CompilationTimeStamp timestamp, final List<IType> typeList, final boolean onlyOwnTable) {
		if (typeList.contains(this)) {
			return;
		}

		if ((onlyOwnTable && codingTable.isEmpty()) || (!onlyOwnTable && getTypeWithCodingTable(timestamp, false) == null)) {
			typeList.add(this);
		}
	}

	/**
	 * Checks the encodings supported by the type (when using new codec handling).
	 * TTCN-3 types need to have an 'encode' attribute to support an encoding.
	 * ASN.1 types automatically support BER, PER and JSON encodings, and XER
	 * encoding, if set by the compiler option.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 */
	public final void checkEncode(final CompilationTimeStamp timestamp) {
		rawAttribute = null;
		jsonAttribute = null; //FIXME: check this value
		berAttribute = null; //FIXME: check this value

		switch (getTypeRefdLast(timestamp).getTypetypeTtcn3()) {
		case TYPE_NULL:
		case TYPE_BOOL:
		case TYPE_INTEGER:
		case TYPE_REAL:
		case TYPE_TTCN3_ENUMERATED:
		case TYPE_BITSTRING:
		case TYPE_HEXSTRING:
		case TYPE_OCTETSTRING:
		case TYPE_CHARSTRING:
		case TYPE_UCHARSTRING:
		case TYPE_OBJECTID:
		case TYPE_TTCN3_CHOICE:
		case TYPE_SEQUENCE_OF:
		case TYPE_SET_OF:
		case TYPE_TTCN3_SEQUENCE:
		case TYPE_TTCN3_SET:
		case TYPE_VERDICT:
		case TYPE_ARRAY:
		case TYPE_ANYTYPE:
			if (!isAsn()) {
				final WithAttributesPath attributePath = getAttributePath();
				if (attributePath != null) {
					final MultipleWithAttributes multipleWithAttributes = attributePath.getAttributes();
					if (multipleWithAttributes != null) {
						for (final SingleWithAttribute singleWithAttribute : multipleWithAttributes) {
							if (singleWithAttribute.getAttributeType() == Attribute_Type.Encode_Attribute) {
								final Attribute_Modifier_type mod = singleWithAttribute.getModifier();
								final Qualifiers qualifiers = singleWithAttribute.getQualifiers();
								if (qualifiers != null && qualifiers.size() > 0) {
									for (final Qualifier qualifier : qualifiers) {
										final List<ISubReference> fieldsOrArrays = new ArrayList<ISubReference>();
										for (final ISubReference sr : qualifier) {
											fieldsOrArrays.add(sr);
										}
										final Reference reference = new Reference(null, fieldsOrArrays);
										final IType type = getFieldType(timestamp, reference, 0, Expected_Value_type.EXPECTED_CONSTANT, false);
										if (type != null) {
											if (type.getMyScope() != myScope) {
												qualifier.getLocation().reportSemanticWarning("Encode attribute is ignored, because it refers to a type from a different type definition");
											} else {
												type.addCoding(timestamp, singleWithAttribute.getAttributeSpecification().getSpecification(), mod, false);
											}
										}
									}
								} else {
									addCoding(timestamp, singleWithAttribute.getAttributeSpecification().getSpecification(), mod, false);
								}
							}
						}
					}

					if(ownerType != TypeOwner_type.OT_TYPE_DEF) {
						return;
					}

					WithAttributesPath globalAttributesPath;
					final Def_Type def = (Def_Type)owner;
					final Group nearest_group = def.getParentGroup();
					if (nearest_group == null) {
						// no group, use the module
						final Module myModule = myScope.getModuleScope();
						globalAttributesPath = ((TTCN3Module)myModule).getAttributePath();
					} else {
						globalAttributesPath = nearest_group.getAttributePath();
					}

					if (globalAttributesPath != null) {
						boolean hasGlobalOverride = false;
						boolean modifierConflict = false;
						Attribute_Modifier_type firstModifier = Attribute_Modifier_type.MOD_NONE;
						final List<SingleWithAttribute> realAttributes = globalAttributesPath.getRealAttributes(timestamp);
						for (int i = 0; i < realAttributes.size(); i++) {
							final SingleWithAttribute singleWithAttribute = realAttributes.get(i);
							if (singleWithAttribute.getAttributeType() == Attribute_Type.Encode_Attribute) {
								final Attribute_Modifier_type modifier = singleWithAttribute.getModifier();
								if (i == 0) {
									firstModifier = modifier;
								} else if (!modifierConflict && modifier != firstModifier) {
									modifierConflict = true;
									singleWithAttribute.getLocation().reportSemanticError("All 'encode' attributes of a group or module must have the same modifier ('override', '@local' or none)");
								}
								if (modifier == Attribute_Modifier_type.MOD_OVERRIDE) {
									hasGlobalOverride = true;
								}
								if (hasGlobalOverride && modifierConflict) {
									break;
								}
							}
						}
						// make a list of the type and its field and element types that inherit
						// the global 'encode' attributes
						// overriding global attributes are inherited by types with no coding
						// table (no 'encode' attributes) of their own
						// non-overriding global attributes are inherited by types that have
						// no coding table of their own and cannot use the coding table of any
						// other type
						final ArrayList<IType> typeList = new ArrayList<IType>();
						getTypesWithNoCodingTable(timestamp, typeList, hasGlobalOverride);
						if (!typeList.isEmpty()) {
							for (int i = 0; i < realAttributes.size(); i++) {
								final SingleWithAttribute singleWithAttribute = realAttributes.get(i);
								if (singleWithAttribute.getAttributeType() == Attribute_Type.Encode_Attribute) {
									for (int j = typeList.size() - 1; j >= 0; j--) {
										typeList.get(j).addCoding(timestamp, singleWithAttribute.getAttributeSpecification().getSpecification(), Attribute_Modifier_type.MOD_NONE, true);
									}
								}
							}
							typeList.clear();
						}
					}
				}
			} else {
				// ASN.1 types automatically have BER, PER, XER, OER and JSON encoding
				switch(ownerType) {
				case OT_TYPE_ASS:
				case OT_RECORD_OF:
				case OT_COMP_FIELD:
				case OT_SELTYPE:
				case OT_FIELDSETTING:
					//FIXME implement once PER, OER or XER gets supported
					addCoding(timestamp, "JSON", Attribute_Modifier_type.MOD_NONE, true);
					addCoding(timestamp, "BER:2002", Attribute_Modifier_type.MOD_NONE, true);
					break;
				default:
					break;
				}
			}
			break;
		default:
			// the rest of the types can't have 'encode' attributes
			break;
		}
	}

	/**
	 * Checks the type's variant attributes (when using the new codec handling).
	 */
	public void checkVariants(final CompilationTimeStamp timestamp) {
		if (isAsn() || ownerType != TypeOwner_type.OT_TYPE_DEF) {
			return;
		}

		WithAttributesPath globalAttributesPath;
		final Def_Type def = (Def_Type)owner;
		final Group nearest_group = def.getParentGroup();
		if (nearest_group == null) {
			// no group, use the module
			final Module myModule = myScope.getModuleScope();
			globalAttributesPath = ((TTCN3Module)myModule).getAttributePath();
		} else {
			globalAttributesPath = nearest_group.getAttributePath();
		}

		if (globalAttributesPath != null) {
			// process all global variants, not just the closest group
			final List<SingleWithAttribute> realAttributes = globalAttributesPath.getRealAttributes(timestamp);
			for (int i = 0; i < realAttributes.size(); i++) {
				final SingleWithAttribute singleWithAttribute = realAttributes.get(i);
				if (singleWithAttribute.getAttributeType() == Attribute_Type.Variant_Attribute) {
					checkThisVariant(timestamp, singleWithAttribute, true);
				}
			}
		}

		// check local variant attributes second, so they overwrite global ones if they
		// conflict with each other
		final WithAttributesPath attributePath = getAttributePath();
		if (attributePath != null) {
			final MultipleWithAttributes multipleWithAttributes = attributePath.getAttributes();
			if (multipleWithAttributes != null) {
				for (final SingleWithAttribute singleWithAttribute : multipleWithAttributes) {
					if (singleWithAttribute.getAttributeType() == Attribute_Type.Variant_Attribute) {
						final Qualifiers qualifiers = singleWithAttribute.getQualifiers();
						if (qualifiers != null && qualifiers.size() > 0) {
							for (final Qualifier qualifier : qualifiers) {
								final List<ISubReference> fieldsOrArrays = new ArrayList<ISubReference>();
								for (final ISubReference sr : qualifier) {
									fieldsOrArrays.add(sr);
								}
								final Reference reference = new Reference(null, fieldsOrArrays);
								final IType type = getFieldType(timestamp, reference, 0, Expected_Value_type.EXPECTED_CONSTANT, false);//?
								if (type != null) {
									type.checkThisVariant(timestamp, singleWithAttribute, false);
								}
							}
						} else {
							checkThisVariant(timestamp, singleWithAttribute, false);
						}
					}
				}
			}
		}

		// check the coding attributes set by the variants
		final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		checkCodingAttributes(timestamp, chain);
		chain.release();
	}

	@Override
	/** {@inheritDoc} */
	public void checkThisVariant(final CompilationTimeStamp timestamp, final SingleWithAttribute singleWithAttribute, final boolean global) {
		final IType type = getTypeWithCodingTable(timestamp, false);
		if (type == null) {
			//FIXME as only RAW is supported for now, we can not report this error
			//	if (!global) {
			//		singleWithAttribute.getLocation().reportSemanticError(MessageFormat.format("No encoding rules defined for type `{0}''", getTypename()));
			//	}
		} else {
			final List<String> codingStrings = singleWithAttribute.getAttributeSpecification().getEncodings();
			// gather the built-in codecs referred to by the variant's encoding strings
			final ArrayList<MessageEncoding_type> codings = new ArrayList<IType.MessageEncoding_type>();
			boolean erroneous = false;
			if (codingStrings == null) {
				if (type.getCodingTable().size() > 1) {
					if (!global) {
						singleWithAttribute.getLocation().reportSemanticError(MessageFormat.format("The encoding reference is mandatory for variant attributes of type  `{0}''", getTypename()));
					}
					erroneous = true;
				} else if (type.getCodingTable().get(0).builtIn) {
					codings.add(type.getCodingTable().get(0).builtInCoding);
				} else { // PER or custom encoding
					final MessageEncoding_type coding = "PER".equals(type.getCodingTable().get(0).customCoding.name) ? MessageEncoding_type.PER: MessageEncoding_type.CUSTOM;
					singleWithAttribute.getLocation().reportSemanticWarning(MessageFormat.format("Variant attributes related to `{0}'' encoding are ignored", coding.getEncodingName()));
				}
			} else {
				for (int i = 0; i < codingStrings.size(); i++) {
					final String encodingString = codingStrings.get(i);
					final MessageEncoding_type coding = MessageEncoding_type.getEncodingType(encodingString);
					if (!hasEncoding(timestamp, coding, encodingString)) {
						erroneous = true;
						//FIXME BER, RAW, JSON restriction only exists because that is the only supported encoding right now
						if (!global && (coding == MessageEncoding_type.RAW || coding == MessageEncoding_type.JSON || coding == MessageEncoding_type.BER)) {
							if (coding == MessageEncoding_type.CUSTOM) {
								singleWithAttribute.getLocation().reportSemanticError(MessageFormat.format("Type `{0}'' does not support {1} encoding", getTypename(), coding.getEncodingName()));
							} else {
								singleWithAttribute.getLocation().reportSemanticError(MessageFormat.format("Type `{0}'' does not support custom encoding `{1}''", getTypename(), encodingString));
							}
						}
					} else if (coding != MessageEncoding_type.PER && coding != MessageEncoding_type.CUSTOM) {
						codings.add(coding);
					} else { // PER or custom encoding
						singleWithAttribute.getLocation().reportSemanticWarning(MessageFormat.format("Variant attributes related to {0} encoding are ignored", coding.getEncodingName()));
					}
				}
			}
			//FIXME implement checks
			//TODO ber, raw, json data is extracted
			boolean newRaw = false;
			boolean newJson = false;
			boolean newBer = false;
			final AtomicBoolean rawFound = new AtomicBoolean(false);
			final AtomicBoolean jsonFound = new AtomicBoolean(false);
			final AtomicBoolean berFound = new AtomicBoolean(false);
			if (rawAttribute == null) {
				IType t_refd = this;
				while (!t_refd.getIsErroneous(timestamp) && t_refd.getRawAttribute() == null && t_refd instanceof Referenced_Type) {
					final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
					t_refd = ((Referenced_Type)t_refd).getTypeRefd(timestamp, referenceChain);
					referenceChain.release();
				}
				rawAttribute = new RawAST(t_refd.getRawAttribute(), getDefaultRawFieldLength());
				newRaw = true;
			}

			if (jsonAttribute == null) {
				IType t_refd = this;
				while (!t_refd.getIsErroneous(timestamp) && t_refd.getJsonAttribute() == null && t_refd instanceof Referenced_Type) {
					final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
					t_refd = ((Referenced_Type)t_refd).getTypeRefd(timestamp, referenceChain);
					referenceChain.release();
				}
				jsonAttribute = new JsonAST(t_refd.getJsonAttribute());
				newJson = true;
			}

			if (berAttribute == null) {
				berAttribute  = new BerAST();
				newBer = true;
			}
			VariantAttributeAnalyzer.parse(rawAttribute, jsonAttribute, berAttribute, singleWithAttribute.getAttributeSpecification(), getLengthMultiplier(),
					rawFound, jsonFound, berFound);
			//FIXME: check variant attribute is related to ... encoding
			if (!rawFound.get() && newRaw) {
				rawAttribute = null;
			}

			if (!jsonFound.get() && newJson) {
				jsonAttribute = null;
			}

			if (!berFound.get() && newBer) {
				berAttribute = null;
			}
		}
		if (global) {
			// send global variant attributes to field/element types
			switch (getTypetype()) {
			case TYPE_TTCN3_CHOICE:
			case TYPE_TTCN3_SEQUENCE:
			case TYPE_TTCN3_SET:
				for (int i = 0; i < ((TTCN3_Set_Seq_Choice_BaseType) this).getNofComponents(); i++) {
					((TTCN3_Set_Seq_Choice_BaseType) this).getComponentByIndex(i).getType().checkThisVariant(timestamp, singleWithAttribute, global);
				}
				break;
			case TYPE_ASN1_CHOICE:
			case TYPE_ASN1_SEQUENCE:
			case TYPE_ASN1_SET:
				for (int i = 0; i < ((ASN1_Set_Seq_Choice_BaseType) this).getNofComponents(); i++) {
					((ASN1_Set_Seq_Choice_BaseType) this).getComponentByIndex(i).getType().checkThisVariant(timestamp, singleWithAttribute, global);
				}
				break;
			case TYPE_ANYTYPE:
				for (int i = 0; i < ((Anytype_Type) this).getNofComponents(); i++) {
					((Anytype_Type) this).getComponentByIndex(i).getType().checkThisVariant(timestamp, singleWithAttribute, global);
				}
				break;
			case TYPE_OPENTYPE:
				for (int i = 0; i < ((Open_Type) this).getNofComponents(); i++) {
					((Open_Type) this).getComponentByIndex(i).getType().checkThisVariant(timestamp, singleWithAttribute, global);
				}
				break;
			case TYPE_ARRAY:
				((Array_Type)this).getElementType().checkThisVariant(timestamp, singleWithAttribute, global);
				break;
			case TYPE_SEQUENCE_OF:
				((SequenceOf_Type)this).getOfType().checkThisVariant(timestamp, singleWithAttribute, global);
				break;
			case TYPE_SET_OF:
				((SetOf_Type)this).getOfType().checkThisVariant(timestamp, singleWithAttribute, global);
				break;
			default:
				break;
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkCoding(final CompilationTimeStamp timestamp, final boolean encode, final Module usageModule, final boolean delayed, final Location errorLocation) {
		final IType type = getTypeWithCodingTable(timestamp, false);
		if (type == null) {
			errorLocation.reportSemanticError(MessageFormat.format("No coding rule specified for type `{0}''", getTypename()));
			return;
		}

		final List<Coding_Type> tempCodingTable = type.getCodingTable();
		for (int i = 0; i < tempCodingTable.size(); i++) {
			final Coding_Type tempCoding = tempCodingTable.get(i);
			if (!tempCoding.builtIn) {
				if (!delayed) {
					//FIXME delay support
					return;
				}

				final Map<IType, CoderFunction_Type> tempCoders = encode ? tempCoding.customCoding.encoders: tempCoding.customCoding.decoders;
				final CoderFunction_Type codingFunction = tempCoders.get(this);
				if (codingFunction == null) {
					if (!type.isAsn()) {
						errorLocation.reportSemanticWarning(MessageFormat.format("No `{0}'' {1}coder function defined for type `{2}''", tempCoding.customCoding.name, encode ? "en" : "de", getTypename()));
					}
				} else if (codingFunction.conflict){
					errorLocation.reportSemanticWarning(MessageFormat.format("Multiple `{0}'' {1}coder functions defined for type `{2}''", tempCoding.customCoding.name, encode ? "en" : "de", getTypename()));
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setRawAttributes(final RawAST newAttributes) {
		rawAttribute = newAttributes;
	}

	@Override
	/** {@inheritDoc} */
	public void setJsonAttributes(final JsonAST newAttributes) {
		jsonAttribute = newAttributes;
	}

	@Override
	/** {@inheritDoc} */
	public void checkJson(final CompilationTimeStamp timestamp) {
		if (jsonAttribute == null) {
			return;
		}

		if (jsonAttribute.omit_as_null && !isOptionalField()) {
			getLocation().reportSemanticError("Invalid attribute, 'omit as null' requires optional field of a record or set.");
		}

		if (jsonAttribute.as_value) {
			getLocation().reportSemanticError("Invalid attribute, 'as value' is only allowed for unions, the anytype, or records or sets with one field");
		}

		if (jsonAttribute.alias != null) {
			final IType parent = getParentType();
			if (parent == null) {
				// only report this error when using the new codec handling, otherwise
				// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
				getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
			} else {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					break;
				default:
					// only report this error when using the new codec handling, otherwise
					// ignore the attribute (since it can also be set by the XML 'name as ...' attribute)
					getLocation().reportSemanticError("Invalid attribute, 'name as ...' requires field of a record, set or union.");
					break;
				}
			}

			if (parent != null && parent.getJsonAttribute() != null && parent.getJsonAttribute().as_value) {
				switch (parent.getTypetype()) {
				case TYPE_TTCN3_CHOICE:
				case TYPE_ANYTYPE:
					// parent_type_name remains null if the 'as value' attribute is set for an invalid type
					getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					break;
				case TYPE_TTCN3_SEQUENCE:
				case TYPE_TTCN3_SET:
					if (((TTCN3_Set_Seq_Choice_BaseType)parent).getNofComponents() == 1) {
						// parent_type_name remains null if the 'as value' attribute is set for an invalid type
						getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'name as ...' will be ignored, because parent {0} is encoded without field names.", parent.getTypename()));
					}
					break;
				default:
					break;
				}
			}
		}

		if (jsonAttribute.parsed_default_value != null) {
			checkJsonDefault(timestamp);
		}

		//TODO: check schema extensions 

		if (jsonAttribute.metainfo_unbound) {
			if (getParentType() == null || (getParentType().getTypetype() != Type_type.TYPE_TTCN3_SEQUENCE &&
					getParentType().getTypetype() != Type_type.TYPE_TTCN3_SET)) {
				// only allowed if it's an array type or a field of a record/set
				getLocation().reportSemanticError("Invalid attribute 'metainfo for unbound', requires record, set, record of, set of, array or field of a record or set");
			}

			final IType last = getTypeRefdLast(timestamp);
			final IType parent = getParentType();
			if (Type_type.TYPE_TTCN3_SEQUENCE == last.getTypetype() || Type_type.TYPE_TTCN3_SET == last.getTypetype()) {
				// if it's set for the record/set, pass it onto its fields
				final int nof_comps = ((TTCN3_Set_Seq_Choice_BaseType)last).getNofComponents();
				if (jsonAttribute.as_value && nof_comps == 1) {
					getLocation().reportSemanticWarning(MessageFormat.format("Attribute 'metainfo for unbound' will be ignored, because the {0} is encoded without field names.",
							last.getTypetype().getName()));
				}
				else {
					for (int i = 0; i < nof_comps; i++) {
						final Type comp_type = ((TTCN3_Set_Seq_Choice_BaseType)last).getComponentByIndex(i).getType();
						if (null == comp_type.jsonAttribute) {
							comp_type.jsonAttribute = new JsonAST();
						}
						comp_type.jsonAttribute.metainfo_unbound = true;
					}
				}
			}
			else if (Type_type.TYPE_SEQUENCE_OF != last.getTypetype() && Type_type.TYPE_SET_OF != last.getTypetype() &&
					Type_type.TYPE_ARRAY != last.getTypetype() && (null == parent ||
					(Type_type.TYPE_TTCN3_SEQUENCE != parent.getTypetype() && Type_type.TYPE_TTCN3_SET != parent.getTypetype()))) {
				// only allowed if it's an array type or a field of a record/set
				getLocation().reportSemanticError("Invalid attribute 'metainfo for unbound', requires record, set, record of, set of, array or field of a record or set");
			}
		}

		if (jsonAttribute.as_number) {
			getLocation().reportSemanticError("Invalid attribute, 'as number' is only allowed for enumerated types");
		}

		//FIXME: check tag_list

		if (jsonAttribute.as_map) {
			getLocation().reportSemanticError("Invalid attribute, 'as map' requires record of or set of");
		}

		if (!jsonAttribute.enum_texts.isEmpty()) {
			getLocation().reportSemanticError("Invalid attribute, 'text ... as ...' requires an enumerated type");
		}
	}

	@Override
	/** {@inheritDoc} */
	public final void checkJsonDefault(final CompilationTimeStamp timestamp) {
		final String preparedString = jsonAttribute.parsed_default_value.replace("\\)", ")").replaceAll("^\\\\\"|\\\\\"$", "\"\"");
		final CharstringExtractor extractor = new CharstringExtractor(preparedString, true);
		final String defaultValue = extractor.getExtractedString();
		final JSONDefaultAnalyzer refAnalyzer = new JSONDefaultAnalyzer();
		final IValue parsedValue = refAnalyzer.parseJSONDefaultValue(defaultValue, jsonAttribute.defaultLocation);
		if (parsedValue != null) {
			parsedValue.setMyGovernor(this);
			parsedValue.setMyScope(getMyScope());
			parsedValue.setFullNameParent(new BridgingNamedNode(this, ".<JSON default value>"));

			final IType last = getTypeRefdLast(timestamp);
			final IValue temporalValue = last.checkThisValueRef(timestamp, parsedValue);
			// check as if it was a const.
			last.checkThisValue(timestamp, temporalValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, true, false, true,
					false,// can not have implicit omit as this is a type.
					false));

			jsonAttribute.default_value = temporalValue;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setBerAttributes(final BerAST newAttributes) {
		berAttribute = newAttributes;
	}

	@Override
	/** {@inheritDoc} */
	public void addCoding(final CompilationTimeStamp timestamp, final String name, final Attribute_Modifier_type modifier, final boolean silent) {
		boolean encodeAttributeModifierConflict = false;
		final MessageEncoding_type builtInCoding = MessageEncoding_type.getEncodingType(name);

		for (final Coding_Type tempCodingType : codingTable) {
			if (!encodeAttributeModifierConflict && modifier != tempCodingType.modifier) {
				encodeAttributeModifierConflict = true;
				getLocation().reportSemanticError("All 'encode' attributes of a type must have the same modifier ('override', '@local' or none)");
			}

			final String codingName = tempCodingType.builtIn ? tempCodingType.builtInCoding.getEncodingName() : tempCodingType.customCoding.name;
			final String currentName = tempCodingType.builtIn ? builtInCoding.getEncodingName() : name;
			if (currentName.equals(codingName)) {
				return; // coding already added
			}
		}

		switch (builtInCoding) {
		case CUSTOM:
		case PER: {
			final Coding_Type newCoding = new Coding_Type();
			newCoding.builtIn = false;
			newCoding.modifier = modifier;
			newCoding.customCoding = new Coding_Type.CustomCoding_type();
			newCoding.customCoding.name = name;
			newCoding.customCoding.encoders = new HashMap<IType, IType.CoderFunction_Type>();
			newCoding.customCoding.decoders = new HashMap<IType, IType.CoderFunction_Type>();
			codingTable.add(newCoding);
			break;
		}
		default:{
			final IType refdLast = getTypeRefdLast(timestamp);
			final boolean canHaveCoding = refdLast.canHaveCoding(timestamp, builtInCoding);
			if (canHaveCoding) {
				final Coding_Type newCoding = new Coding_Type();
				newCoding.builtIn = true;
				newCoding.modifier = modifier;
				newCoding.builtInCoding = builtInCoding;
				codingTable.add(newCoding);
//				refdLast.setGenerateCoderFunctions(timestamp, builtInCoding);
			} else if (!silent){
				getLocation().reportSemanticWarning(MessageFormat.format("Type `{0}'' cannot have {1} encoding. Encode attribute ignored.", getTypename(), name));
			}
			break;
		}
		}
	}

	@Override
	/** {@inheritDoc} */
	public IType getTypeWithCodingTable(final CompilationTimeStamp timestamp, final boolean ignoreLocal) {
		// 1st priority: if local attributes are not ignored, and if the type
		// has its own 'encode' attributes (its coding table is not
		// empty), then
		// return the type
		if (!ignoreLocal && !codingTable.isEmpty()) {
			return this;
		}

		// 2nd priority: if this is a field or element type, and one of its parents
		// has an 'encode' attribute with the 'override' modifier, then return the parent type
		IType parent = null;
		if (parentType != null && (ownerType ==TypeOwner_type.OT_COMP_FIELD || ownerType == TypeOwner_type.OT_RECORD_OF || ownerType == TypeOwner_type.OT_ARRAY)) {
			// note: if one of the parent types has an overriding 'encode' attribute,
			// then this returns the farthest parent with an overriding 'encode';
			// if none of the 'encode' attributes are overriding, then the nearest
			// parent with at least one 'encode' attribute is returned
			parent = parentType.getTypeWithCodingTable(timestamp, true);
		}
		if (parent != null) {
			final List<Coding_Type> tempCodingTable = parent.getCodingTable();
			for (final Coding_Type ct : tempCodingTable) {
				if (ct.modifier == Attribute_Modifier_type.MOD_OVERRIDE) {
					return parent;
				}
			}
		}

		// 3rd priority: if local attributes are ignored, and if the type has its
		// own (non-local) 'encode' attributes, then return the type
		if (ignoreLocal && !codingTable.isEmpty()) {
			boolean local = false;
			for (final Coding_Type ct : codingTable) {
				if (ct.modifier == Attribute_Modifier_type.MOD_LOCAL) {
					local = true;
					break;
				}
			}
			if (!local) {
				return this;
			}
		}

		// 4th priority, if a referenced type has an 'encode' attribute, then return the referenced type
		if (this instanceof Referenced_Type) {
			final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			IType tempType = ((Referenced_Type)this).getTypeRefd(timestamp, chain);
			chain.release();
			if (tempType == null || tempType.getIsErroneous(timestamp) ) {
				return parent;
			}

			tempType = tempType.getTypeWithCodingTable(timestamp, false);
			if (tempType != null) {
				return tempType;
			}
		}

		// otherwise return the parent type pointer (whether it's null or not)
		return parent;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasEncoding(final CompilationTimeStamp timestamp, final MessageEncoding_type coding, final String customEncoding) {
		if (coding == MessageEncoding_type.UNDEFINED || (coding == MessageEncoding_type.CUSTOM && customEncoding == null)) {
			// FATAL error
			return false;
		}

		switch (coding) {
		case BER:
		// TODO: if supported PER
		// case PER:
		{
			Type type = this;
			if (type.isAsn()) {
				return true;
			}
			switch (type.getTypetype()) {
			case TYPE_UNRESTRICTEDSTRING:
			case TYPE_OBJECTCLASSFIELDTYPE:
			case TYPE_EXTERNAL:
			case TYPE_EMBEDDED_PDV:
			case TYPE_REFERENCED:
			case TYPE_REFD_SPEC:
			case TYPE_SELECTION:
			case TYPE_ADDRESS: {
				if (type instanceof IReferencingType) {
					final IReferenceChain refChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
					final IType t = ((IReferencingType) type).getTypeRefd(timestamp, refChain);
					refChain.release();

					if (t == null || t == this) {
						return false;
					}
				}
				type = (Type) type.getTypeRefdLast(lastTimeChecked);
				return type.hasEncoding(timestamp, coding, customEncoding);
			}
			// FIXME: check this case
			// case T_ERROR: 
			case TYPE_BOOL:
			case TYPE_INTEGER:
			case TYPE_REAL:
			case TYPE_BITSTRING:
			case TYPE_CHARSTRING:
			case TYPE_OCTETSTRING:
			case TYPE_OBJECTID:
				return true;
			default:
				return false;
			}
		}
		case PER:
		case OER:
		case TEXT:
		case XER:
			//FIXME not yet supported
			return true;
		default: {
			final IType t = getTypeWithCodingTable(timestamp, false);
			if (t != null) {
				final boolean builtIn = coding != MessageEncoding_type.CUSTOM;
				final String encodingName = builtIn ? coding.getEncodingName() : customEncoding;
				final List<Coding_Type> ct = t.getCodingTable();
				for (int i = 0; i < ct.size(); i++) {
					final Coding_Type tempCodingType = ct.get(i);
					if (builtIn == tempCodingType.builtIn
							&& ((builtIn && tempCodingType.builtInCoding == coding) || (!builtIn && tempCodingType.customCoding.name
									.equals(encodingName)))) {
						return true;
					}
				}
			}

			final IType lastType = getTypeRefdLast(timestamp);
			if (coding == MessageEncoding_type.CUSTOM) {
				//  all types need an 'encode' attribute for user-defined codecs
				return false;
			}
			switch (getTypetype()) {
			case TYPE_ASN1_SEQUENCE:
			case TYPE_TTCN3_SEQUENCE:
			case TYPE_ASN1_SET:
			case TYPE_TTCN3_SET:
			case TYPE_SEQUENCE_OF:
			case TYPE_SET_OF:
			case TYPE_ARRAY:
			case TYPE_ASN1_CHOICE:
			case TYPE_TTCN3_CHOICE:
			case TYPE_ANYTYPE:
			case TYPE_ASN1_ENUMERATED:
			case TYPE_TTCN3_ENUMERATED:
				// these types need an 'encode' attribute for built-in codecs,
				return false;
			default:
				break;
			}

			return lastType.canHaveCoding(timestamp, coding);
		}
		}
	}

	@Override
	/** {@inheritDoc} */
	public List<Coding_Type> getCodingTable() {
		return codingTable;
	}

	protected void checkSubtypeRestrictions(final CompilationTimeStamp timestamp) {
		checkSubtypeRestrictions(timestamp, getSubtypeType(), null);
	}

	/** create and check subtype, called by the check function of the type */
	protected void checkSubtypeRestrictions(final CompilationTimeStamp timestamp, final SubType.SubType_type subtypeType,
			final SubType parentSubtype) {

		if (getIsErroneous(timestamp)) {
			return;
		}

		// if there is no own or parent sub-type then there's nothing to do
		if ((parsedRestrictions == null) && (parentSubtype == null)) {
			return;
		}

		// if the type has no subtype type
		if (subtypeType == SubType.SubType_type.ST_NONE) {
			getLocation().reportSemanticError(
					MessageFormat.format("TTCN-3 subtype constraints are not applicable to type `{0}''", getTypename()));
			setIsErroneous(true);
			return;
		}

		subType = new SubType(subtypeType, this, parsedRestrictions, parentSubtype);

		subType.check(timestamp);
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisValue(final CompilationTimeStamp timestamp, final IValue value, final Assignment lhs, final ValueCheckingOptions valueCheckingOptions) {
		value.setIsErroneous(false);

		final Assignment assignment = getDefiningAssignment();
		if (assignment instanceof Definition) {
			final Scope scope = value.getMyScope();
			if (scope != null) {
				final Module module = scope.getModuleScope();
				if (module != null) {
					final String referingModuleName = module.getName();
					if (!((Definition)assignment).referingHere.contains(referingModuleName)) {
						((Definition)assignment).referingHere.add(referingModuleName);
					}
				} else {
					TitanLogger.logError("The value `" + value.getFullName() + "' does not appear to be in a module");
					value.setIsErroneous(true);
				}
			} else {
				TitanLogger.logError("The value `" + value.getFullName() + "' does not appear to be in a scope");
				value.setIsErroneous(true);
			}
		}

		check(timestamp);
		final IValue last = value.getValueRefdLast(timestamp, valueCheckingOptions.expected_value, null);
		if (last == null || last.getIsErroneous(timestamp) || getIsErroneous(timestamp)) {
			return false;
		}

		if (Value_type.OMIT_VALUE.equals(last.getValuetype()) && !valueCheckingOptions.omit_allowed) {
			value.getLocation().reportSemanticError("`omit' value is not allowed in this context");
			value.setIsErroneous(true);
			return false;
		}

		boolean selfReference = false;
		switch (value.getValuetype()) {
		case UNDEFINED_LOWERIDENTIFIER_VALUE:
			if (Value_type.REFERENCED_VALUE.equals(last.getValuetype())) {
				final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
				selfReference = checkThisReferencedValue(timestamp, last, lhs, valueCheckingOptions.expected_value, chain, valueCheckingOptions.sub_check,
						valueCheckingOptions.str_elem);
				chain.release();
				return selfReference;
			}
			return false;
		case REFERENCED_VALUE: {
			final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			selfReference = checkThisReferencedValue(timestamp, value, lhs, valueCheckingOptions.expected_value, chain, valueCheckingOptions.sub_check,
					valueCheckingOptions.str_elem);
			chain.release();
			return selfReference;
		}
		case EXPRESSION_VALUE:
			selfReference = value.checkExpressionSelfReference(timestamp, lhs);
			if (value.isUnfoldable(timestamp, null)) {
				final Type_type temporalType = value.getExpressionReturntype(timestamp, valueCheckingOptions.expected_value);
				if (!Type_type.TYPE_UNDEFINED.equals(temporalType)
						&& !isCompatible(timestamp, this.getTypetype(), temporalType, false, value.isAsn())) {
					value.getLocation().reportSemanticError(MessageFormat.format(INCOMPATIBLEVALUE, getTypename()));
					value.setIsErroneous(true);
				}
			}
			return selfReference;
		case MACRO_VALUE:
			selfReference = value.checkExpressionSelfReference(timestamp, lhs);
			if (value.isUnfoldable(timestamp, null)) {
				final Type_type temporalType = value.getExpressionReturntype(timestamp, valueCheckingOptions.expected_value);
				if (!Type_type.TYPE_UNDEFINED.equals(temporalType)
						&& !isCompatible(timestamp, this.getTypetype(), temporalType, false, value.isAsn())) {
					value.getLocation().reportSemanticError(MessageFormat.format(INCOMPATIBLEVALUE, getTypename()));
					value.setIsErroneous(true);
				}
				return selfReference;
			}
			break;
		default:
			break;
		}

		return selfReference;
	}

	/**
	 * Checks the provided referenced value.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param value the referenced value to be checked.
	 * @param lhs the assignment to check against
	 * @param expectedValue the expectations we have for the value.
	 * @param referenceChain the reference chain to detect circular references.
	 * @param strElem {@code true} if the value to be checked is an element of a string
	 * @return {@code true} if the value contains a reference to lhs
	 */
	private boolean checkThisReferencedValue(final CompilationTimeStamp timestamp, final IValue value, final Assignment lhs, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain, final boolean subCheck, final boolean strElem) {
		final Reference reference = ((Referenced_Value) value).getReference();
		final Assignment assignment = reference.getRefdAssignment(timestamp, true, referenceChain);

		if (assignment == null) {
			value.setIsErroneous(true);
			return false;
		}

		final Assignment myAssignment = getDefiningAssignment();
		if (myAssignment instanceof Definition) {
			final String referingModuleName = value.getMyScope().getModuleScope().getName();
			if (!((Definition)myAssignment).referingHere.contains(referingModuleName)) {
				((Definition)myAssignment).referingHere.add(referingModuleName);
			}
		}

		assignment.check(timestamp);
		final boolean selfReference = assignment == lhs;

		boolean isConst = false;
		boolean errorFlag = false;
		boolean checkRunsOn = false;
		IType governor = null;
		if (assignment.getIsErroneous()) {
			value.setIsErroneous(true);
		} else {
			switch (assignment.getAssignmentType()) {
			case A_CONST:
				isConst = true;
				break;
			case A_OBJECT:
			case A_OS:
				final ISetting setting = reference.getRefdSetting(timestamp);
				if (setting == null || setting.getIsErroneous(timestamp)) {
					value.setIsErroneous(true);
					return selfReference;
				}

				if (!Setting_type.S_V.equals(setting.getSettingtype())) {
					reference.getLocation().reportSemanticError(
							MessageFormat.format("This InformationFromObjects construct does not refer to a value: {0}",
									value.getFullName()));
					value.setIsErroneous(true);
					return selfReference;
				}

				governor = ((Value) setting).getMyGovernor();
				if (governor != null) {
					isConst = true;
				}
				break;
			case A_EXT_CONST:
			case A_MODULEPAR:
				if (Expected_Value_type.EXPECTED_CONSTANT.equals(expectedValue)) {
					value.getLocation().reportSemanticError(
							MessageFormat.format(
									"Reference to an (evaluatable) constant value was expected instead of {0}",
									assignment.getDescription()));
					errorFlag = true;
				}
				break;
			case A_VAR:
			case A_PAR_VAL:
			case A_PAR_VAL_IN:
			case A_PAR_VAL_OUT:
			case A_PAR_VAL_INOUT:
				switch (expectedValue) {
				case EXPECTED_CONSTANT:
					value.getLocation().reportSemanticError(
							MessageFormat.format("Reference to a constant value was expected instead of {0}",
									assignment.getDescription()));
					errorFlag = true;
					break;
				case EXPECTED_STATIC_VALUE:
					value.getLocation().reportSemanticError(
							MessageFormat.format("Reference to a static value was expected instead of {0}",
									assignment.getDescription()));
					errorFlag = true;
					break;
				default:
					break;
				}
				break;
			case A_TEMPLATE:
			case A_MODULEPAR_TEMPLATE:
			case A_VAR_TEMPLATE:
			case A_PAR_TEMP_IN:
			case A_PAR_TEMP_OUT:
			case A_PAR_TEMP_INOUT:
				if (!Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue)) {
					value.getLocation().reportSemanticError(
							MessageFormat.format(REFTOVALUEEXPECTED,
									assignment.getDescription()));
					errorFlag = true;
				}
				break;
			case A_FUNCTION_RVAL:
				checkRunsOn = true;
				switch (expectedValue) {
				case EXPECTED_CONSTANT: {
					final String message = MessageFormat.format(
							"Reference to a constant value was expected instead of the return value of {0}",
							assignment.getDescription());
					value.getLocation().reportSemanticError(message);
					errorFlag = true;
				}
				break;
				case EXPECTED_STATIC_VALUE: {
					final String message = MessageFormat.format(
							"Reference to a static value was expected instead of the return value of {0}",
							assignment.getDescription());
					value.getLocation().reportSemanticError(message);
					errorFlag = true;
				}
				break;
				default:
					break;
				}
				break;
			case A_EXT_FUNCTION_RVAL:
				switch (expectedValue) {
				case EXPECTED_CONSTANT: {
					final String message = MessageFormat.format(
							"Reference to a constant value was expected instead of the return value of {0}",
							assignment.getDescription());
					value.getLocation().reportSemanticError(message);
					errorFlag = true;
				}
				break;
				case EXPECTED_STATIC_VALUE: {
					final String message = MessageFormat.format(
							"Reference to a static value was expected instead of the return value of {0}",
							assignment.getDescription());
					value.getLocation().reportSemanticError(message);
					errorFlag = true;
				}
				break;
				default:
					break;
				}
				break;
			case A_FUNCTION_RTEMP:
				checkRunsOn = true;
				if (!Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue)) {
					value.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									REFTOVALUEEXPECTED_INSTEADOFCALL,
									assignment.getDescription()));
					errorFlag = true;
				}
				break;
			case A_EXT_FUNCTION_RTEMP:
				if (!Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue)) {
					value.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									REFTOVALUEEXPECTED_INSTEADOFCALL,
									assignment.getDescription()));
					errorFlag = true;
				}
				break;
			case A_FUNCTION:
			case A_EXT_FUNCTION:
				value.getLocation()
				.reportSemanticError(
						MessageFormat.format(
								"Reference to a {0} was expected instead of a call of {1}, which does not have return type",
								Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue) ? GeneralConstants.VALUEORTEMPLATE
										: GeneralConstants.VALUE, assignment.getDescription()));
				value.setIsErroneous(true);
				return selfReference;
			default:
				value.getLocation().reportSemanticError(
						MessageFormat.format("Reference to a {0} was expected instead of {1}",
								Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue) ? GeneralConstants.VALUEORTEMPLATE
										: GeneralConstants.VALUE, assignment.getDescription()));
				value.setIsErroneous(true);
				return selfReference;
			}
		}

		if (checkRunsOn) {
			reference.getMyScope().checkRunsOnScope(timestamp, assignment, reference, GeneralConstants.CALL);
		}
		if (governor == null) {
			final IType type = assignment.getType(timestamp);
			if (type != null) {
				if (type instanceof Referenced_Type) {
					Referenced_Type rt = (Referenced_Type)type;
					IType refdtype = rt.getTypeRefd(timestamp, referenceChain);
					if (refdtype instanceof Class_Type) {
						governor = ((Class_Type)refdtype).getFieldType(timestamp, reference, 1, expectedValue, referenceChain, false);
					}
				}
				if (governor == null) {
					governor = type.getFieldType(timestamp, reference, 1, expectedValue, referenceChain, false);
				}
			}
		}
		if (governor == null) {
			value.setIsErroneous(true);
			return selfReference;
		}

		final TypeCompatibilityInfo info = new TypeCompatibilityInfo(this, governor, true);
		info.setStr1Elem(strElem);
		info.setStr2Elem(reference.refersToStringElement());
		final CompatibilityLevel compatibilityLevel = getCompatibility(timestamp, governor, info, null, null);
		if (compatibilityLevel != CompatibilityLevel.COMPATIBLE) {
			// Port or signature values do not exist at all. These
			// errors are already
			// reported at those definitions. Extra errors should
			// not be reported
			// here.
			final IType type = getTypeRefdLast(timestamp, null);
			switch (type.getTypetype()) {
			case TYPE_PORT:
				// neither port values nor templates exist
				break;
			case TYPE_SIGNATURE:
				if (Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue)) {
					final String message = MessageFormat.format(
							"Type mismatch: a signature template of type `{0}'' was expected instead of `{1}''",
							getTypename(), governor.getTypename());
					value.getLocation().reportSemanticError(message);
				}
				break;
			case TYPE_SEQUENCE_OF:
			case TYPE_ASN1_SEQUENCE:
			case TYPE_TTCN3_SEQUENCE:
			case TYPE_ARRAY:
			case TYPE_ASN1_SET:
			case TYPE_TTCN3_SET:
			case TYPE_SET_OF:
			case TYPE_ASN1_CHOICE:
			case TYPE_TTCN3_CHOICE:
			case TYPE_ANYTYPE:
				if (compatibilityLevel == CompatibilityLevel.INCOMPATIBLE_SUBTYPE) {
					value.getLocation().reportSemanticError(info.getSubtypeError());
				} else {
					value.getLocation().reportSemanticError(info.getErrorStringString());
				}
				break;
			default:
				if (compatibilityLevel == CompatibilityLevel.INCOMPATIBLE_SUBTYPE) {
					value.getLocation().reportSemanticError(info.getSubtypeError());
				} else {
					final String message = MessageFormat.format(
							"Type mismatch: a {0} of type `{1}'' was expected instead of `{2}''",
							Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue) ? GeneralConstants.VALUEORTEMPLATE : GeneralConstants.VALUE,
									getTypename(), governor.getTypename());
					value.getLocation().reportSemanticError(message);
				}
				break;
			}
			errorFlag = true;
		} else {
			if (DiagnosticSeverity.Warning.equals(typeCompatibilitySeverity) && info.getNeedsConversion()) {
				value.getLocation().reportSemanticWarning(
						MessageFormat.format(TYPECOMPATWARNING, this.getTypename(), governor.getTypename()));
			}
			if (info.getNeedsConversion()) {
				value.set_needs_conversion();
			}
		}

		if (errorFlag) {
			value.setIsErroneous(true);
			return selfReference;
		}

		// checking for circular references
		final IValue last = value.getValueRefdLast(timestamp, expectedValue, referenceChain);
		if (isConst && !last.getIsErroneous(timestamp)) {
			if (subCheck && (subType != null)) {
				subType.checkThisValue(timestamp, value);
			}
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public ITTCN3Template checkThisTemplateRef(final CompilationTimeStamp timestamp, final ITTCN3Template t, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		switch( t.getTemplatetype() ){
		case SUPERSET_MATCH:
		case SUBSET_MATCH:
			final IType it1 = getTypeRefdLast(timestamp);
			final Type_type tt = it1.getTypetype();
			if(Type_type.TYPE_SEQUENCE_OF.equals(tt) || Type_type.TYPE_SET_OF.equals(tt) ) {
				return t;
			} else {
				t.getLocation().reportSemanticError(MessageFormat.format(
						"{0} cannot be used for type {1}", t.getTemplateTypeName(), getTypename()));
				t.setIsErroneous(true);
				return t;
			}

		case SPECIFIC_VALUE:
			break; //cont below
		default:
			return t;
		}

		//Case of specific value:

		final ITTCN3Template template = t;
		IValue value = ((SpecificValue_Template) template).getSpecificValue();
		if (value == null) {
			return template;
		}

		value = checkThisValueRef(timestamp, value);

		switch (value.getValuetype()) {
		case REFERENCED_VALUE:
			final Assignment assignment = ((Referenced_Value) value).getReference().getRefdAssignment(timestamp, false, referenceChain); //FIXME: referenceChain or null?
			if (assignment == null) {
				template.setIsErroneous(true);
			} else {
				switch (assignment.getAssignmentType()) {
				case A_VAR_TEMPLATE:
					if(!Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue)){
						template.getLocation().reportSemanticError(
								MessageFormat.format(REFTOVALUEEXPECTED,
										assignment.getDescription()));
						template.setIsErroneous(true);
					}

					final IType type = ((Def_Var_Template) assignment).getType(timestamp);
					switch (type.getTypetype()) {
					case TYPE_BITSTRING:
					case TYPE_BITSTRING_A:
					case TYPE_HEXSTRING:
					case TYPE_OCTETSTRING:
					case TYPE_CHARSTRING:
					case TYPE_UCHARSTRING:
					case TYPE_UTF8STRING:
					case TYPE_NUMERICSTRING:
					case TYPE_PRINTABLESTRING:
					case TYPE_TELETEXSTRING:
					case TYPE_VIDEOTEXSTRING:
					case TYPE_IA5STRING:
					case TYPE_GRAPHICSTRING:
					case TYPE_VISIBLESTRING:
					case TYPE_GENERALSTRING:
					case TYPE_UNIVERSALSTRING:
					case TYPE_BMPSTRING:
					case TYPE_UTCTIME:
					case TYPE_GENERALIZEDTIME:
					case TYPE_OBJECTDESCRIPTOR: {
						final List<ISubReference> subReferences = ((Referenced_Value) value).getReference().getSubreferences();
						final int nofSubreferences = subReferences.size();
						if (nofSubreferences > 1) {
							final ISubReference subreference = subReferences.get(nofSubreferences - 1);
							if (subreference instanceof ArraySubReference) {
								template.getLocation().reportSemanticError(
										MessageFormat.format("Reference to {0} can not be indexed",
												assignment.getDescription()));
								template.setIsErroneous(true);
								return template;
							}
						}
						break;
					}
					default:
						break;
					}
					return template.setTemplatetype(timestamp, Template_type.TEMPLATE_REFD);
				case A_CONST:
					IType type1;
					if( assignment instanceof Value_Assignment){
						type1 = ((Value_Assignment) assignment).getType(timestamp);
					} else {
						type1 = ((Def_Const) assignment).getType(timestamp);
					}
					switch (type1.getTypetype()) {
					case TYPE_BITSTRING:
					case TYPE_BITSTRING_A:
					case TYPE_HEXSTRING:
					case TYPE_OCTETSTRING:
					case TYPE_CHARSTRING:
					case TYPE_UCHARSTRING:
					case TYPE_UTF8STRING:
					case TYPE_NUMERICSTRING:
					case TYPE_PRINTABLESTRING:
					case TYPE_TELETEXSTRING:
					case TYPE_VIDEOTEXSTRING:
					case TYPE_IA5STRING:
					case TYPE_GRAPHICSTRING:
					case TYPE_VISIBLESTRING:
					case TYPE_GENERALSTRING:
					case TYPE_UNIVERSALSTRING:
					case TYPE_BMPSTRING:
					case TYPE_UTCTIME:
					case TYPE_GENERALIZEDTIME:
					case TYPE_OBJECTDESCRIPTOR: {
						final List<ISubReference> subReferences = ((Referenced_Value) value).getReference().getSubreferences();
						final int nofSubreferences = subReferences.size();
						if (nofSubreferences > 1) {
							final ISubReference subreference = subReferences.get(nofSubreferences - 1);
							if (subreference instanceof ArraySubReference) {
								template.getLocation().reportSemanticError(
										MessageFormat.format("Reference to {0} can not be indexed",
												assignment.getDescription()));
								template.setIsErroneous(true);
								return template;
							}
						}
						break;
					}
					default:
						break;
					}
					break;
				case A_TEMPLATE:
				case A_MODULEPAR_TEMPLATE:
				case A_PAR_TEMP_IN:
				case A_PAR_TEMP_OUT:
				case A_PAR_TEMP_INOUT:
				case A_FUNCTION_RTEMP:
				case A_EXT_FUNCTION_RTEMP:
					if(!Expected_Value_type.EXPECTED_TEMPLATE.equals(expectedValue)){
						template.getLocation().reportSemanticError(
								MessageFormat.format(REFTOVALUEEXPECTED,
										assignment.getDescription()));
						template.setIsErroneous(true);
					}
					return template.setTemplatetype(timestamp, Template_type.TEMPLATE_REFD);
				default:
					break;
				}
			}
			break;
		case EXPRESSION_VALUE: {
			final Expression_Value expression = (Expression_Value) value;
			if (Operation_type.APPLY_OPERATION.equals(expression.getOperationType())) {
				IType type = expression.getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_TEMPLATE);
				if (type == null) {
					break;
				}

				type = type.getTypeRefdLast(timestamp);
				if (type != null && Type_type.TYPE_FUNCTION.equals(type.getTypetype()) && ((Function_Type) type).returnsTemplate()) {
					return template.setTemplatetype(timestamp, Template_type.TEMPLATE_INVOKE);
				}
			}
			break;
		}
		default:
			break;
		}

		return template;

	}

	/**
	 * Checks the provided index as a part of a string.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param indexValue the index value to be checked.
	 * @param expectedIndex the expected kind of the index value.
	 * @param refChain a chain of references used to detect circular references.
	 */
	protected void checkStringIndex(final CompilationTimeStamp timestamp, final Value indexValue, final Expected_Value_type expectedIndex, final IReferenceChain refChain) {
		if (indexValue != null) {
			indexValue.setLoweridToReference(timestamp);
			final Type_type tempType = indexValue.getExpressionReturntype(timestamp, expectedIndex);
			switch (tempType) {
			case TYPE_INTEGER:
				final IValue last = indexValue.getValueRefdLast(timestamp, expectedIndex, refChain);
				if (Value_type.INTEGER_VALUE.equals(last.getValuetype())) {
					final Integer_Value lastInteger = (Integer_Value) last;
					if (lastInteger.isNative()) {
						final long temp = lastInteger.getValue();
						if (temp < 0) {
							indexValue.getLocation().reportSemanticError(MessageFormat.format(SequenceOf_Type.NONNEGATIVINDEXEXPECTED, temp));
							indexValue.setIsErroneous(true);
						}
					} else {
						indexValue.getLocation().reportSemanticError(MessageFormat.format(SequenceOf_Type.TOOBIGINDEX, lastInteger.getValueValue(), getTypename()));
						indexValue.setIsErroneous(true);
					}
				}
				break;
			case TYPE_UNDEFINED:
				indexValue.setIsErroneous(true);
				break;
			default:
				indexValue.getLocation().reportSemanticError(SequenceOf_Type.INTEGERINDEXEXPECTED);
				indexValue.setIsErroneous(true);
				break;
			}
		}
	}

	/**
	 * Register the usage of this type in the provided template.
	 * @param template the template to use.
	 */
	protected void registerUsage(final ITTCN3Template template) {
		final Assignment assignment = getDefiningAssignment();
		if (assignment instanceof Definition) {
			final String referingModuleName = template.getMyScope().getModuleScope().getName();
			if (!((Definition)assignment).referingHere.contains(referingModuleName)) {
				((Definition)assignment).referingHere.add(referingModuleName);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public final void checkThisTemplateSubtype(final CompilationTimeStamp timestamp, final ITTCN3Template template) {
		if (ITTCN3Template.Template_type.PERMUTATION_MATCH.equals(template.getTemplatetype())) {
			// a permutation is just a fragment, in itself it has no type
			return;
		}

		if (subType != null) {
			subType.checkThisTemplateGeneric(timestamp, template);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isStronglyCompatible(final CompilationTimeStamp timestamp, final IType otherType, final TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain) {

		check(timestamp);
		otherType.check(timestamp);
		final IType thisTypeLast = this.getTypeRefdLast(timestamp);
		final IType otherTypeLast = otherType.getTypeRefdLast(timestamp);

		if (thisTypeLast == null || otherTypeLast == null || thisTypeLast.getIsErroneous(timestamp)
				|| otherTypeLast.getIsErroneous(timestamp)) {
			return true;
		}

		return thisTypeLast.getTypetype().equals(otherTypeLast.getTypetype());
	}

	@Override
	/** {@inheritDoc} */
	public CompatibilityLevel getCompatibility(final CompilationTimeStamp timestamp, final IType type, final TypeCompatibilityInfo info,
			final TypeCompatibilityInfo.Chain leftChain, final TypeCompatibilityInfo.Chain rightChain) {
		if (info == null) {
			TitanLogger.logFatal("info==null");
		}

		if (!isCompatible(timestamp, type, info, leftChain, rightChain)) {
			return CompatibilityLevel.INCOMPATIBLE_TYPE;
		}

		// if there is noStructuredTypeCompatibility and isCompatible then it should be strong compatibility:
		if( noStructuredTypeCompatibility ) {
			return CompatibilityLevel.COMPATIBLE;
		}

		final SubType otherSubType = type.getSubtype();
		if ((info != null) && (subType != null) && (otherSubType != null)) {
			if (info.getStr1Elem()) {
				if (info.getStr2Elem()) {
					// both are string elements -> nothing to do
				} else {
					// char <-> string
					if (!otherSubType.isCompatibleWithElem(timestamp)) {
						info.setSubtypeError("Subtype mismatch: string element has no common value with subtype "
								+ otherSubType.toString());
						return CompatibilityLevel.INCOMPATIBLE_SUBTYPE;
					}
				}
			} else {
				if (info.getStr2Elem()) {
					// string <-> char
					if (!subType.isCompatibleWithElem(timestamp)) {
						info.setSubtypeError("Subtype mismatch: subtype " + subType.toString()
								+ " has no common value with string element");
						return CompatibilityLevel.INCOMPATIBLE_SUBTYPE;
					}
				} else {
					// string <-> string
					if (!subType.isCompatible(timestamp, otherSubType)) {
						info.setSubtypeError("Subtype mismatch: subtype " + subType.toString()
								+ " has no common value with subtype " + otherSubType.toString());
						return CompatibilityLevel.INCOMPATIBLE_SUBTYPE;
					}
				}
			}
		}

		return CompatibilityLevel.COMPATIBLE;
	}
	
	public String getRestrictionDesc() {
		final SubType st = getSubtype();
		if (st == null) {
			return null;
		}
		return st.toString();
	}

	@Override
	/** {@inheritDoc} */
	public Identifier getIdentifier() {
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		return new Object[] {};
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		return GeneralConstants.EMPTY_STRING;
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		return getTypetype().ordinal();
	}

	/**
	 * Returns whether this type is compatible with type. Used if the other
	 * value is unfoldable, but we can determine its expression return type
	 * <p>
	 * Note: The compatibility relation is asymmetric. The function returns
	 * true if the set of possible values in type is a subset of possible
	 * values in this.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param typeType1 the type of the first type.
	 * @param typeType2 the type of the second type.
	 * @param isAsn11 {@code true} if the first type is from ASN.1
	 * @param isAsn12 {@code true} if the second type is from ASN.1
	 *
	 * @return {@code true} if the first type is compatible with the second,
	 *         {@code false} otherwise.
	 */
	public static final boolean isCompatible(final CompilationTimeStamp timestamp, final Type_type typeType1, final Type_type typeType2,
			final boolean isAsn11, final boolean isAsn12) {
		if (Type_type.TYPE_UNDEFINED.equals(typeType1) || Type_type.TYPE_UNDEFINED.equals(typeType2)) {
			return true;
		}

		switch (typeType1) {
		case TYPE_NULL:
		case TYPE_BOOL:
		case TYPE_REAL:
		case TYPE_HEXSTRING:
		case TYPE_SEQUENCE_OF:
		case TYPE_SET_OF:
		case TYPE_VERDICT:
		case TYPE_DEFAULT:
		case TYPE_COMPONENT:
		case TYPE_SIGNATURE:
		case TYPE_PORT:
		case TYPE_ARRAY:
		case TYPE_FUNCTION:
		case TYPE_ALTSTEP:
		case TYPE_TESTCASE:
			return typeType1.equals(typeType2);
		case TYPE_OCTETSTRING:
			return Type_type.TYPE_OCTETSTRING.equals(typeType2) || (!isAsn11 && Type_type.TYPE_ANY.equals(typeType2));
		case TYPE_UCHARSTRING:
			switch (typeType2) {
			case TYPE_UCHARSTRING:
			case TYPE_UTF8STRING:
			case TYPE_BMPSTRING:
			case TYPE_UNIVERSALSTRING:
			case TYPE_TELETEXSTRING:
			case TYPE_VIDEOTEXSTRING:
			case TYPE_GRAPHICSTRING:
			case TYPE_OBJECTDESCRIPTOR:
			case TYPE_GENERALSTRING:
			case TYPE_CHARSTRING:
			case TYPE_NUMERICSTRING:
			case TYPE_PRINTABLESTRING:
			case TYPE_IA5STRING:
			case TYPE_VISIBLESTRING:
			case TYPE_UTCTIME:
			case TYPE_GENERALIZEDTIME:
				return true;
			default:
				return false;
			}
		case TYPE_UTF8STRING:
		case TYPE_BMPSTRING:
		case TYPE_UNIVERSALSTRING:
			switch (typeType2) {
			case TYPE_UCHARSTRING:
			case TYPE_UTF8STRING:
			case TYPE_BMPSTRING:
			case TYPE_UNIVERSALSTRING:
			case TYPE_CHARSTRING:
			case TYPE_NUMERICSTRING:
			case TYPE_PRINTABLESTRING:
			case TYPE_IA5STRING:
			case TYPE_VISIBLESTRING:
			case TYPE_UTCTIME:
			case TYPE_GENERALIZEDTIME:
				return true;
			default:
				return false;
			}
		case TYPE_TELETEXSTRING:
		case TYPE_VIDEOTEXSTRING:
		case TYPE_GRAPHICSTRING:
		case TYPE_OBJECTDESCRIPTOR:
		case TYPE_GENERALSTRING:
			switch (typeType2) {
			case TYPE_TELETEXSTRING:
			case TYPE_VIDEOTEXSTRING:
			case TYPE_GRAPHICSTRING:
			case TYPE_OBJECTDESCRIPTOR:
			case TYPE_GENERALSTRING:
			case TYPE_CHARSTRING:
			case TYPE_NUMERICSTRING:
			case TYPE_PRINTABLESTRING:
			case TYPE_IA5STRING:
			case TYPE_VISIBLESTRING:
			case TYPE_UTCTIME:
			case TYPE_GENERALIZEDTIME:
			case TYPE_UCHARSTRING:
				return true;
			default:
				return false;
			}
		case TYPE_CHARSTRING:
		case TYPE_NUMERICSTRING:
		case TYPE_PRINTABLESTRING:
		case TYPE_IA5STRING:
		case TYPE_VISIBLESTRING:
		case TYPE_UTCTIME:
		case TYPE_GENERALIZEDTIME:
			switch (typeType2) {
			case TYPE_CHARSTRING:
			case TYPE_NUMERICSTRING:
			case TYPE_PRINTABLESTRING:
			case TYPE_IA5STRING:
			case TYPE_VISIBLESTRING:
			case TYPE_UTCTIME:
			case TYPE_GENERALIZEDTIME:
				return true;
			default:
				return false;
			}
		case TYPE_BITSTRING:
		case TYPE_BITSTRING_A:
			return Type_type.TYPE_BITSTRING.equals(typeType2) || Type_type.TYPE_BITSTRING_A.equals(typeType2);
		case TYPE_INTEGER:
		case TYPE_INTEGER_A:
			return Type_type.TYPE_INTEGER.equals(typeType2) || Type_type.TYPE_INTEGER_A.equals(typeType2);
		case TYPE_OBJECTID:
			return Type_type.TYPE_OBJECTID.equals(typeType2) || (!isAsn11 && Type_type.TYPE_ROID.equals(typeType2));
		case TYPE_ROID:
			return Type_type.TYPE_ROID.equals(typeType2) || (!isAsn12 && Type_type.TYPE_OBJECTID.equals(typeType2));
		case TYPE_TTCN3_ENUMERATED:
		case TYPE_ASN1_ENUMERATED:
			return Type_type.TYPE_TTCN3_ENUMERATED.equals(typeType2) || Type_type.TYPE_ASN1_ENUMERATED.equals(typeType2);
		case TYPE_TTCN3_CHOICE:
		case TYPE_ASN1_CHOICE:
		case TYPE_OPENTYPE:
			return Type_type.TYPE_TTCN3_CHOICE.equals(typeType2) || Type_type.TYPE_ASN1_CHOICE.equals(typeType2)
					|| Type_type.TYPE_OPENTYPE.equals(typeType2);
		case TYPE_TTCN3_SEQUENCE:
		case TYPE_ASN1_SEQUENCE:
			return Type_type.TYPE_TTCN3_SEQUENCE.equals(typeType2) || Type_type.TYPE_ASN1_SEQUENCE.equals(typeType2);
		case TYPE_TTCN3_SET:
		case TYPE_ASN1_SET:
			return Type_type.TYPE_TTCN3_SET.equals(typeType2) || Type_type.TYPE_ASN1_SET.equals(typeType2);
		case TYPE_ANY:
			return Type_type.TYPE_ANY.equals(typeType2) || Type_type.TYPE_OCTETSTRING.equals(typeType2);
		case TYPE_REFERENCED:
		case TYPE_OBJECTCLASSFIELDTYPE:
		case TYPE_ADDRESS:
			return false;
		case TYPE_CLASS:
		case TYPE_ANYTYPE:
			return Type_type.TYPE_CLASS.equals(typeType2);	// FIXME : implement - we need real class compatibility check
		default:
			return false;
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (constraints != null) {
			constraints.findReferences(referenceFinder, foundIdentifiers);
		}
		if (withAttributesPath != null) {
			withAttributesPath.findReferences(referenceFinder, foundIdentifiers);
		}
		if (parsedRestrictions != null) {
			for (final IParsedSubType parsedSubType : parsedRestrictions) {
				parsedSubType.findReferences(referenceFinder, foundIdentifiers);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (constraints != null && !constraints.accept(v)) {
			return false;
		}
		if (withAttributesPath != null && !withAttributesPath.accept(v)) {
			return false;
		}
		if (parsedRestrictions != null) {
			for (final IParsedSubType pst : parsedRestrictions) {
				if (!pst.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * @return whether the type needs an explicit Java alias and/or
	 * an alias to a type descriptor of another type. It returns true for those
	 * types that are defined in module-level type definitions hence are
	 * directly accessible by the users of Java API (in test ports, external
	 * functions)
	 */
	protected boolean needsAlias() {
		//TODO find a way without using fullname and change in synch with compiler
		final String name = getFullName();
		final int firstDot = name.indexOf(DOT_CHAR);
		if (firstDot == -1 || name.indexOf(DOT_CHAR, firstDot + 1) == -1) {
			return true;
		}
		return false;
	}

	/**
	 * Returns whether the type has the encoding attribute specified by
	 * the parameter. The function also checks the qualified attributes of
	 * parent types. Always returns true for ASN.1 types, when checking for a
	 * JSON encoding attribute.
	 */
	public boolean hasEncodeAttribute(final MessageEncoding_type encoding) {
		if (MessageEncoding_type.JSON.equals(encoding) && isAsn()) {
			// ASN.1 types automatically support JSON encoding
			return true;
		}

		final WithAttributesPath attributePath = getAttributePath();
		final CompilationTimeStamp timestamp = CompilationTimeStamp.getBaseTimestamp() ;
		final List<SingleWithAttribute> realAttributes = attributePath.getRealAttributes(timestamp);
		for (final SingleWithAttribute att : realAttributes) {
			if (SingleWithAttribute.Attribute_Type.Encode_Attribute.equals(att.getAttributeType())) {
				final String spec = att.getAttributeSpecification().getSpecification();
				if (MessageEncoding_type.getEncodingType(spec).equals(encoding)) {
					return true;
				}
			}
		}

		if ( (ownerType == TypeOwner_type.OT_COMP_FIELD || ownerType == TypeOwner_type.OT_RECORD_OF || ownerType == TypeOwner_type.OT_ARRAY) &&
				parentType != null && parentType.getAttributePath() != null &&
				parentType.hasEncodeAttributeForType(this, encoding)) {
			return true;
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasEncodeAttributeForType(final Type type, final MessageEncoding_type encoding) {
		// if this type has an encode attribute, that also extends to its fields/elements
		if (hasEncodeAttribute(encoding)) {
			return true;
		}
		// otherwise search this type's qualified attributes
		final MultipleWithAttributes multiWithAttributes = getAttributePath().getAttributes();
		if (multiWithAttributes != null) {
			for (final SingleWithAttribute singleWithAttribute : multiWithAttributes) {
				if (SingleWithAttribute.Attribute_Type.Encode_Attribute.equals(singleWithAttribute.getAttributeType())) {
					final String spec = singleWithAttribute.getAttributeSpecification().getSpecification();
					if (MessageEncoding_type.getEncodingType(spec).equals(encoding)) {
						// search the attribute's qualifiers for one that refers to the target type
						final Qualifiers qualifiers = singleWithAttribute.getQualifiers();
						if (qualifiers != null) {
							for (final Qualifier qualifier : qualifiers) {
								final List<ISubReference> fieldsOrArrays = new ArrayList<ISubReference>();
								for (final ISubReference sr : qualifier) {
									fieldsOrArrays.add(sr);
								}
								final Reference reference = new Reference(null, fieldsOrArrays);
								final IType typeQualifier = getFieldType(CompilationTimeStamp.getBaseTimestamp(), reference, 0, Expected_Value_type.EXPECTED_CONSTANT, false);
								if (typeQualifier == type) {
									return true;
								}
							}
						}
					}
				}
			}
		}

		if ( (ownerType == TypeOwner_type.OT_COMP_FIELD || ownerType == TypeOwner_type.OT_RECORD_OF || ownerType == TypeOwner_type.OT_ARRAY) &&
				parentType != null && parentType.getAttributePath() != null) {
			return parentType.hasEncodeAttributeForType(type, encoding);
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public int getJsonValueType() {
		final IType t = getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());
		final Type_type tt = t.getTypetype();
		switch(tt) {
		case TYPE_INTEGER:
		case TYPE_INTEGER_A:
			return JSON_NUMBER;
		case TYPE_REAL:
			return JSON_NUMBER | JSON_STRING;
		case TYPE_BOOL:
			return JSON_BOOLEAN;
		case TYPE_NULL:
			return JSON_NULL;
		case TYPE_BITSTRING:
		case TYPE_BITSTRING_A:
		case TYPE_HEXSTRING:
		case TYPE_OCTETSTRING:
		case TYPE_CHARSTRING:
		case TYPE_UCHARSTRING:
		case TYPE_UTF8STRING:
		case TYPE_NUMERICSTRING:
		case TYPE_PRINTABLESTRING:
		case TYPE_TELETEXSTRING:
		case TYPE_VIDEOTEXSTRING:
		case TYPE_IA5STRING:
		case TYPE_GRAPHICSTRING:
		case TYPE_VISIBLESTRING:
		case TYPE_GENERALSTRING:  
		case TYPE_UNIVERSALSTRING:
		case TYPE_BMPSTRING:
		case TYPE_VERDICT:
		case TYPE_TTCN3_ENUMERATED:
		case TYPE_ASN1_ENUMERATED:
		case TYPE_OBJECTID:
		case TYPE_ROID:
		case TYPE_ANY:
			return JSON_STRING;
		case TYPE_TTCN3_SEQUENCE:
		case TYPE_TTCN3_SET:
			if (t instanceof TTCN3_Set_Seq_Choice_BaseType) {
				final TTCN3_Set_Seq_Choice_BaseType bt = (TTCN3_Set_Seq_Choice_BaseType)t;
				if (bt.getNofComponents() > 1) {
					return JSON_OBJECT;
				} else {
					return JSON_ANY_VALUE;
				}
			} else {
				TitanLogger.logFatal("getJsonValueType(): invalid TTCN3 set or sequence type " + tt.name());
			}
			break;
		case TYPE_ASN1_SEQUENCE:
		case TYPE_ASN1_SET:
			if (t instanceof ASN1_Set_Seq_Choice_BaseType) {
				final ASN1_Set_Seq_Choice_BaseType bt = (ASN1_Set_Seq_Choice_BaseType)t;
				if (bt.getNofComponents() > 1) {
					return JSON_OBJECT;
				} else {
					return JSON_ANY_VALUE;
				}
			} else {
				TitanLogger.logFatal("getJsonValueType(): invalid ASN1 set or sequence type " + tt.name());
			}
			break;
		case TYPE_TTCN3_CHOICE:
		case TYPE_ASN1_CHOICE:
		case TYPE_ANYTYPE:
		case TYPE_OPENTYPE:
			return JSON_ANY_VALUE;
		case TYPE_SEQUENCE_OF:
		case TYPE_SET_OF:
		case TYPE_ARRAY:
			return JSON_ARRAY | JSON_OBJECT;
		default:
			TitanLogger.logFatal("getJsonValueType(): invalid field type " + tt.name());
		}
		return 0;
	}

	@Override
	public boolean isPresentAnyvalueEmbeddedField(final ExpressionStruct expression, final List<ISubReference> subreferences, final int beginIndex) {
		return true;
	}

	/** Set the owner and its type type */
	public void setOwnertype(final TypeOwner_type ownerType, final INamedNode owner) {
		this.ownerType = ownerType;
		this.owner = owner;
	}

	public TypeOwner_type getOwnertype() {
		return ownerType;
	}

	public INamedNode getOwner() {
		return owner;
	}

	/**
	 * Indicates that the type needs to have any from done support.
	 */
	public void set_needs_any_from_done() {
		needs_any_from_done = true;
	}
	
	public void checkAsReturnType(CompilationTimeStamp timestamp, boolean asValue, String what) {
		final IType type = getTypeRefdLast(timestamp);
		switch(type.getTypetype()) {
		case TYPE_PORT:
			getLocation().reportSemanticError(MessageFormat.format(PORTRETURN, type.getFullName(), what));
			break;
		case TYPE_SIGNATURE:
			if (asValue) {
				getLocation().reportSemanticError(MessageFormat.format(SIGNATURERETURN, type.getFullName(), what));
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Gets the name of types for quick fix code insertions
	 * FIXME add missing types
	 * 
	 * @param type
	 * @return
	 */
	public static String typeToSrcType(Type_type type) {
		switch (type) {
		case TYPE_INTEGER:
		case TYPE_CHARSTRING:
		case TYPE_BITSTRING:
		case TYPE_REAL:
			return type.getName();
		default:
			return GeneralConstants.EMPTY_STRING;
		}
	}

	/**
	 * Gets the default value of types for quick fix code insertions
	 * FIXME add missing types
	 * 
	 * @param type
	 * @return
	 */
	public static String typeToSrcTypeDefault(Type_type type) {
		switch (type) {
		case TYPE_BOOL:
			return GeneralConstants.FALSE;
		case TYPE_INTEGER:
			return Integer_Type.ZERO;
		case TYPE_REAL:
			return Float_Type.ZERO_DOT_ZERO;
		case TYPE_CHARSTRING:
			return "\"\"";
		case TYPE_BITSTRING:
			return "\"00B\"";
		case TYPE_OCTETSTRING:
			return "\"00O\"";
		default:
			return GeneralConstants.EMPTY_STRING;
		}
	}

	public String getDefaultSnippet(String lineEnding, int indentation, AtomicInteger placeholderIdx) {
		return GeneralConstants.EMPTY_STRING;
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean hasDocumentComment() {
		return documentComment != null;
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean hasOwnDocumentComment() {
		return documentComment != null && 
			documentComment.getOwner() != null &&
			documentComment.getOwner().equals(this);
	}

	@Override
	/** {@inheritDoc} */
	public DocumentComment getDocumentComment() {
		return documentComment;
	}

	@Override
	/** {@inheritDoc} */
	public void setDocumentComment(DocumentComment docComment) {
		if (documentComment != null) {
			return;
		}
		documentComment = docComment;

		if (this instanceof IParameterisedAssignment) {
			final FormalParameterList fpl = ((IParameterisedAssignment) this).getFormalParameterList();
			if (fpl != null) {
				for (final FormalParameter fp : fpl) {
					fp.setDocumentComment(docComment);
				}
			}
		}
	}
	
	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		// Do nothing
		return null;
	}
	
	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		hoverContent = new Ttcn3HoverContent();
		return hoverContent;
	}
}
