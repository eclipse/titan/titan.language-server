/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.attributes;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;

/**
 * Represent an error behavior setting in the codec API of the run-time
 * environment. The setting contains the error type identifier and the way of
 * error handling.
 *
 * @author Kristof Szabados
 */
public final class ErrorBehaviorSetting extends ASTNode implements IIncrementallyUpdatable {
	private final String errorType;
	private final String errorHandling;

	/**
	 * The location of the whole setting. This location encloses the setting
	 * fully, as it is used to report errors to.
	 **/
	private Location location = Location.getNullLocation();

	public ErrorBehaviorSetting(final String errorType, final String errorHandling) {
		this.errorType = errorType;
		this.errorHandling = errorHandling;
	}

	public void setLocation(final Location location) {
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

	public String getErrorType() {
		return errorType;
	}

	public String getErrorHandling() {
		return errorHandling;
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		// no members
		return true;
	}
}
