/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.List;

import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.core.Position;

public class NamedBridgeContextLocator implements IContextLocator {
	private final Position cursorPosition;
	private final NamedBridgeScope block;
	private final TTCN3Module module;
	private final String sourceText;
	private String contextString;
	private Position startPosBetweenSubScopes;

	public NamedBridgeContextLocator(final NamedBridgeScope block, final TTCN3Module module, final String sourceText, final Position cursorPosition) {
		this.cursorPosition = cursorPosition;
		this.block = block;
		this.module = module;
		this.sourceText = sourceText;
		contextString = findContextString();
	}

	private boolean isCursorBetweenSubScopes() {
		final List<Location> subScopeLocations = block.getSubScopeLocations();
		if (subScopeLocations == null) {
			return false;
		}

		Position startPos = null;

		for (Location loc : subScopeLocations) {
			final Position endPosition = loc.getEndPosition();
			if (cursorPosition.after(endPosition)) {
				startPos = endPosition;
			}
		}

		if (startPos != null) {
			startPosBetweenSubScopes = startPos;
			return true;
		}

		return false;
	}

	private String findContextString() {
		if (isCursorBetweenSubScopes()) {
			return extractContextStringFromSource(
					startPosBetweenSubScopes,
					cursorPosition,
					sourceText);
		}

		return extractContextStringFromSource(
				new Position(cursorPosition.getLine(), 0),
				cursorPosition,
				sourceText);
	}

	@Override
	public String getContextString() {
		return contextString;
	}

}
