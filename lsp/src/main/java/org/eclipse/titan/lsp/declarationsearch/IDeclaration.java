/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.declarationsearch;

import java.util.List;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.IIdentifiable;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;

/**
 * This interface represents a declaration which can be referenced. This class is
 * responsible for creating instances of the subclasses. The subclasses are not
 * accessible outside this package.
 * 
 * @author Szabolcs Beres
 * @author Adam Knapp
 */
public interface IDeclaration extends IIdentifiable, ILocateableNode {

	/**
	 * Factory method to create a {@link IDeclaration} from an {@link Assignment}.
	 */
	static IDeclaration createInstance(final Assignment assignment) {
		return new DefinitionDeclaration(assignment);
	}

	/**
	 * Factory method to create a {@link IDeclaration} from an
	 * {@link Assignment} and an {@link Identifier}. Returns {@code null}, if the
	 * assignment or the identifier is {@code null}. This declaration represents a
	 * field of a structured type.
	 */
	static IDeclaration createInstance(final Assignment ass, final Identifier id) {
		if (ass == null || id == null) {
			return null;
		}
		return new FieldDeclaration(ass, id);
	}

	/**
	 * Factory method to create a {@link IDeclaration} from an {@link Module}.
	 */
	static IDeclaration createInstance(final Module module) {
		if (module == null) {
			return null;
		}
		return new ModuleDeclaration(module);
	}

	/**
	 * Factory method to create a {@link IDeclaration} from an {@link ICommentable}.
	 */
	static IDeclaration createInstance(final ICommentable commentable) {
		if (commentable == null) {
			return null;
		}
		return new CommentableDeclaration(commentable);
	}

	/**
	 * Returns the references of this declaration.
	 * 
	 * @param module The module to find the references.
	 * @return The found references. This list includes the definition
	 *         itself as well if it is in the same module.
	 */
	List<Hit> getReferences(final Module module);

	/**
	 * Returns the references of this declaration.
	 * 
	 * @param module The module to find the references.
	 * @return The Reference Finder of given module.
	 */
	default ReferenceFinder getReferenceFinder(final Module module) {
		// No reference finder present, guard with null
		return null;
	}

	/**
	 * Returns the assignment of this declaration.  
	 * @return the assignment of this declaration. 
	 */
	default Assignment getAssignment() {
		return null;
	}

	@Override
	default void setLocation(Location location) {
		// Do nothing by default
	}

	/**
	 * Returns the node as an {@link ICommentable} object.
	 * @return the node as an {@link ICommentable} object.
	 */
	ICommentable getCommentable();
}
