/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.statements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class LogArguments extends ASTNode implements IIncrementallyUpdatable, ICollection<LogArgument> {
	private static final String FULLNAMEPART = ".logargs_";

	private final List<LogArgument> arguments;

	public LogArguments() {
		super();
		arguments = new ArrayList<LogArgument>();
	}

	public LogArguments(final List<LogArgument> arguments) {
		super();

		if (arguments == null) {
			this.arguments = Collections.emptyList();
		} else {
			for (final LogArgument logArgument : arguments) {
				logArgument.setFullNameParent(this);
			}
			this.arguments = arguments;
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = arguments.size(); i < size; i++) {
			if (arguments.get(i) == child) {
				return builder.append(FULLNAMEPART).append(Integer.toString(i + 1));
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		for (final LogArgument argument : arguments) {
			argument.setMyScope(scope);
		}
	}

	public int size() {
		return arguments.size();
	}

	public LogArgument get(final int index) {
		return arguments.get(index);
	}

	/**
	 * Does the semantic checking of the log arguments.
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		for (final LogArgument argument : arguments) {
			argument.check(timestamp);
		}
	}

	/**
	 * Checks whether this value is defining itself in a recursive way. This
	 * can happen for example if a constant is using itself to determine its
	 * initial value.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param referenceChain the ReferenceChain used to detect circular references.
	 */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}

		for (final LogArgument argument : arguments) {
			argument.checkRecursions(timestamp, referenceChain);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final LogArgument argument : arguments) {
			argument.updateSyntax(reparser, false);
			reparser.updateLocation(argument.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final LogArgument logArgument : arguments) {
			logArgument.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final LogArgument la : arguments) {
			if (!la.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean add(LogArgument e) {
		throw new UnsupportedOperationException("Add operation is not supported for LogArguments");
	}

	@Override
	public Iterator<LogArgument> iterator() {
		return arguments.iterator();
	}
}
