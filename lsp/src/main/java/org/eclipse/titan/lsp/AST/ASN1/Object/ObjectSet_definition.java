/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferenceChainElement;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.ASN1.ASN1Object;
import org.eclipse.titan.lsp.AST.ASN1.Block;
import org.eclipse.titan.lsp.AST.ASN1.IObjectSetElement;
import org.eclipse.titan.lsp.AST.ASN1.ObjectSet;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.compiler.BuildTimestamp;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.asn1parser.Asn1Parser;
import org.eclipse.titan.lsp.parsers.asn1parser.BlockLevelTokenStreamTracker;

/**
 * ObjectSet definition.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public final class ObjectSet_definition extends ObjectSet implements IReferenceChainElement {
	private static final String DUPLICATED_UNIQUE_FIELD_VALUE =
			"Two objects in the set have the same identifier (UNIQUE) field value";

	private final Block mBlock;

	private List<IObjectSetElement> objectSetElements;
	private ASN1Objects objects;

	/** the time when code for this type was generated. */
	private BuildTimestamp lastTimeGenerated = null;

	public ObjectSet_definition() {
		objectSetElements = new ArrayList<IObjectSetElement>();
		mBlock = null;
	}

	public ObjectSet_definition(final Block aBlock) {
		objectSetElements = new ArrayList<IObjectSetElement>();
		this.mBlock = aBlock;
	}

	public ObjectSet_definition(final ASN1Objects objects) {
		objectSetElements = new ArrayList<IObjectSetElement>();
		mBlock = null;
		this.objects = objects;
	}

	public ObjectSet_definition newInstance() {
		ObjectSet_definition temp;
		if (null != mBlock) {
			temp = new ObjectSet_definition(mBlock);
		} else if (null != objects) {
			temp = new ObjectSet_definition(objects);
		} else {
			temp = new ObjectSet_definition();
		}

		for (int i = 0; i < objectSetElements.size(); i++) {
			temp.addObjectSetElement(objectSetElements.get(i).newOseInstance());
		}
		temp.getObjectSetElements();

		return temp;
	}

	public final List<IObjectSetElement> getObjectSetElements() {
		return objectSetElements;
	}

	public final void setObjectSetElements(final List<IObjectSetElement> objectSetElements) {
		this.objectSetElements = objectSetElements;
	}

	@Override
	/** {@inheritDoc} */
	public final String chainedDescription() {
		return getFullName();
	}

	@Override
	/** {@inheritDoc} */
	public final Location getChainLocation() {
		return getLocation();
	}

	public final void steelObjectSetElements(final ObjectSet_definition definition) {
		if (null == definition) {
			return;
		}

		for (int i = 0; i < definition.getObjectSetElements().size(); i++) {
			addObjectSetElement(definition.getObjectSetElements().get(i).newOseInstance());
		}

		definition.getObjectSetElements().clear();
	}

	@Override
	/** {@inheritDoc} */
	public final StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0; i < objectSetElements.size(); i++) {
			if (objectSetElements.get(i) == child) {
				return builder.append(INamedNode.DOT).append(String.valueOf(i + 1));
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public final void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		for (final IObjectSetElement element : objectSetElements) {
			element.setMyScopeOse(scope);
		}
	}

	public final void addObjectSetElement(final IObjectSetElement element) {
		if (null != element) {
			element.setFullNameParent(this);
			objectSetElements.add(element);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		isErroneous = false;

		if (mBlock != null && lastTimeChecked == null) {
			parseBlockObjectSetSpecifications();
		}

		final ObjectSetElementVisitor_checker checker = new ObjectSetElementVisitor_checker(timestamp, location, myGovernor);
		for (final IObjectSetElement element : objectSetElements) {
			element.accept(checker);
		}

		lastTimeChecked = timestamp;

		createObjects(true);

		if (objectSetElements.size() != objects.size()) {
			return;
		}

		for (int i = 0; i < objects.size()-1; i++) {
			final IValue value = objects.get(i).getUniqueFieldSettingValue();
			if (value == null) {
				continue;
			}
			for (int j = i+1; j < objects.size(); j++) {
				final IValue value2 = objects.get(j).getUniqueFieldSettingValue();
				if (value2 == null) {
					continue;
				}
				if (value2.checkEquality(timestamp, value)) {
					final String severity = Configuration.INSTANCE
							.getString(Configuration.REPORT_DUPLICATED_UNIQUE_FIELD_VALUE, GeneralConstants.ERROR);
					final IObjectSetElement e1 = objectSetElements.get(i);
					if (e1 instanceof ILocateableNode) {
						((ILocateableNode)e1).getLocation()
							.reportConfigurableSemanticProblem(severity, DUPLICATED_UNIQUE_FIELD_VALUE);
					}
					final IObjectSetElement e2 = objectSetElements.get(j);
					if (e2 instanceof ILocateableNode) {
						((ILocateableNode)e2).getLocation()
							.reportConfigurableSemanticProblem(severity, DUPLICATED_UNIQUE_FIELD_VALUE);
					}
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public final ObjectSet_definition getRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (objectSetElements.size() != 1) {
			return this;
		}

		final IObjectSetElement element = objectSetElements.get(0);
		if (!(element instanceof Referenced_ObjectSet)) {
			return this;
		}

		final boolean newChain = null == referenceChain;
		IReferenceChain temporalReferenceChain;
		if (newChain) {
			temporalReferenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		} else {
			temporalReferenceChain = referenceChain;
		}

		temporalReferenceChain.add(this);
		final ObjectSet_definition result = ((Referenced_ObjectSet) element).getRefdLast(timestamp, temporalReferenceChain);

		if (newChain) {
			temporalReferenceChain.release();
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public final int size() {
		createObjects(false);
		return objects.size();
	}

	@Override
	/** {@inheritDoc} */
	public final ASN1Object get(final int index) {
		createObjects(false);

		return objects.get(index);
	}

	public final ASN1Objects getObjs() {
		createObjects(false);

		return objects;
	}

	@Override
	/** {@inheritDoc} */
	public final void accept(final ObjectSetElementVisitor_objectCollector v) {
		v.visitObjectSet(this, false);
	}

	private void parseBlockObjectSetSpecifications() {
		if (mBlock == null) {
			return;
		}

		final Asn1Parser parser = BlockLevelTokenStreamTracker.getASN1ParserForBlock(mBlock);
		if (parser == null) {
			return;
		}

		final ObjectSet_definition temporalDefinition = parser.pr_special_ObjectSetSpec().definition;
		// internalIndex += parser.nof_consumed_tokens();
		final List<SyntacticErrorStorage> errors = parser.getErrorStorage();
		if (errors != null && !errors.isEmpty()) {
			for (final SyntacticErrorStorage ses : errors) {
				ses.reportSyntacticError();
			}
		}

		if (temporalDefinition == null) {
			isErroneous = true;
			return;
		}

		setLocation(temporalDefinition.getLocation());

		final List<IObjectSetElement> elements = temporalDefinition.getObjectSetElements();
		for (int i = 0; i < elements.size(); i++) {
			addObjectSetElement(elements.get(i));
		}
		temporalDefinition.setObjectSetElements(null);

		setMyScope(getMyScope());
	}

	protected final void createObjects(final boolean force) {
		if (objects != null && !force) {
			return;
		}

		if (myGovernor == null) {
			return;
		}

		if (lastTimeChecked == null) {
			check(CompilationTimeStamp.getBaseTimestamp());
		}

		final ObjectSetElementVisitor_objectCollector visitor = new ObjectSetElementVisitor_objectCollector(this, lastTimeChecked);
		for (IObjectSetElement ose : objectSetElements) {
			ose.accept(visitor);
		}
		objects = visitor.getObjects();
	}

	@Override
	/** {@inheritDoc} */
	protected final boolean memberAccept(final ASTVisitor v) {
		if (objectSetElements != null) {
			for (final IObjectSetElement element : objectSetElements) {
				if (!element.memberAccept(v)) {
					return false;
				}
			}
		}

		if (objects != null && !objects.accept(v)) {
			return false;
		}

		return true;
	}
}
