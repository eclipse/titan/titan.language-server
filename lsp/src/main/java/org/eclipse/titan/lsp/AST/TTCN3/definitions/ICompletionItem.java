/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.titan.lsp.AST.IIdentifiable;

public interface ICompletionItem extends IIdentifiable {
	/**
	 * Returns the display name of this AST element included in a {@link CompletionItem}.
	 * The {@link CompletionItemKind} and the documentation are need to be set after
	 * calling this default implementation. 
	 * @return the display name of this AST element
	 */
	default CompletionItem getCompletionItem() {
		final CompletionItem item = new CompletionItem(getIdentifier().getDisplayName());
		item.setInsertText(getIdentifier().getDisplayName());
		return item;
	}
}
