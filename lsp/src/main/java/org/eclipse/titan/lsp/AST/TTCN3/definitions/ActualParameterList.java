/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a list of actual parameters.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class ActualParameterList extends ASTNode implements IIncrementallyUpdatable, ICollection<ActualParameter> {
	private static final String FULLNAMEPART = ".<parameter";

	private final ArrayList<ActualParameter> parameters = new ArrayList<ActualParameter>();

	@Override
	/** {@inheritDoc} */
	public synchronized void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		parameters.trimToSize();
		for (final ActualParameter ap : parameters) {
			ap.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0; i < parameters.size(); i++) {
			if (parameters.get(i) == child) {
				return builder.append(FULLNAMEPART).append(String.valueOf(i + 1)).append(INamedNode.MORETHAN);
			}
		}

		return builder;
	}

	public synchronized boolean add(final ActualParameter parameter) {
		if (parameter != null) {
			parameter.setFullNameParent(this);
			return parameters.add(parameter);
		}
		return false;
	}

	public int size() {
		return parameters.size();
	}

	public ActualParameter get(final int index) {
		return parameters.get(index);
	}

	/**
	 * Checks for circular references within the actual parameter list.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param referenceChain the ReferenceChain used to detect circular references,
	 *                must not be {@code null}
	 */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}

		for (final ActualParameter ap : parameters) {
			ap.checkRecursions(timestamp, referenceChain);
		}
	}

	/**
	 * Handles the incremental parsing of this list of actual parameters.
	 *
	 * @param reparser the parser doing the incremental parsing.
	 * @param isDamaged {@code true} if the location contains the damaged area,
	 *                {@code false} if only its' location needs to be updated.
	 */
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);
		for (final ActualParameter parameter : parameters) {
			parameter.updateSyntax(reparser, false);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final ActualParameter ap : parameters) {
			if (!ap.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<ActualParameter> iterator() {
		return parameters.iterator();
	}
}
