/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1;

import org.eclipse.titan.lsp.AST.IGoverned;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.Setting;
import org.eclipse.titan.lsp.AST.ASN1.Object.ObjectSetElementVisitor_objectCollector;
import org.eclipse.titan.lsp.AST.ASN1.Object.ObjectSet_definition;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Class to represent ObjectSet.
 *
 * @author Kristof Szabados
 */
public abstract class ObjectSet extends Setting implements ICancelBuild, IGoverned {
	protected ObjectClass myGovernor;

	@Override
	/** {@inheritDoc} */
	public final Setting_type getSettingtype() {
		return Setting_type.S_OS;
	}

	public abstract ObjectSet newInstance();

	@Override
	/** {@inheritDoc} */
	public final ObjectClass getMyGovernor() {
		return myGovernor;
	}

	/**
	 * Sets the ObjectClass of the object set.
	 * @param governor the object class to set as governor.
	 */
	public final void setMyGovernor(final ObjectClass governor) {
		myGovernor = governor;
	}

	/**
	 * Returns the last referenced {@linkplain ObjectSet_definition} in the ref. chain
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * @param referenceChain
	 * @return the last referenced ObjectSet_definition
	 */
	public abstract ObjectSet_definition getRefdLast(final CompilationTimeStamp timestamp, IReferenceChain referenceChain);

	/**
	 * The size of the underlying Collection
	 * @return the number of elements in the underlying Collection
	 */
	public abstract int size();

	/**
     * Returns the element at the specified position
     *
     * @param index index of the element to return
     * @return the element at the specified position
     */
	public abstract ASN1Object get(int index);

	public abstract void accept(ObjectSetElementVisitor_objectCollector v);

	public abstract void check(final CompilationTimeStamp timestamp);
}
