/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticRelatedInformation;
import org.eclipse.lsp4j.DiagnosticTag;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.TextDocumentContentChangeEvent;
import org.eclipse.lsp4j.TextDocumentItem;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.ProjectSourceParser;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticModifier;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * This class represents an item in an LSP project (source file, directory)
 * 
 * @author Miklos Magyari
 *
 */
public final class ProjectItem {
	public enum ItemType { Ttcn3, Asn1, Ttcn3pp, Directory }

	private static final Object lock = new Object();

	private Project project;
	private Path path;
	private TextDocumentItem documentItem;
	private ItemType itemtype;

	/** The item is untitled; it is created in the editor as a new file and can have edits, but not yet saved */
	private boolean isUntitled;

	/** The item is manually excluded in the client */
	private boolean isExcluded;

	/** Version number of the file. It is incremented with every change in the editor */
	private int version;
	/** Source code if the file is opened by the client */ 
	private String source;
	/** character sequence closing lines (eg. LF or CRLF) */
	private String lineEnding;

	/** Syntactical file analysis is up to date */
	private boolean isSyntacticallyUpToDate;

	/** Sets of problem markers for the current file */
	private Set<LspMarker> markerList = new ConcurrentSkipListSet<>();
	private Set<LspMarker> markerListBackup;

	/** List of code lenses for the current file */
	private List<CodeLens> codelenses = new CopyOnWriteArrayList<>();

	private Module ttcn3module;

	/** Helper variable for lambda method */
	private int line;

	public ProjectItem(final Project project, final Path path) {
		version = -1;
		lineEnding = "\n";
		this.project = project;
		this.path = path;
		this.setSyntacticallyUpToDate(false);
		if (path.toFile().isDirectory()) {
			itemtype = ItemType.Directory;
		} else {
			final String filename = path.getFileName().toString();
			if (filename.matches("^.*\\.ttcn3?$")) {
				itemtype = ItemType.Ttcn3;
			} else if (filename.matches("^.*\\.ttcn3?pp$")) {
				itemtype = ItemType.Ttcn3pp;
			} else if (filename.matches("^.*\\.asn1?$")) {
				itemtype = ItemType.Asn1;
			}
		}
	}

	/**
	 * Marks the item as open, stores its source code.
	 * Until the file is closed, the file content should be in synch with the client
	 * and the server should not try reading it from the file system. 
	 * 
	 * @param documentItem
	 */
	public void openFile(final TextDocumentItem documentItem) {
		if (this.documentItem != null) {
			return;
		}

		this.documentItem = documentItem;
		setSource(documentItem.getText());
		version = 1;

		detectLineEnding();
	}

	public void closeFile() {
		setSource(null);
		setVersion(-1);
		documentItem = null;
	}

	/**
	 * Tries to detect the line ending for a file.<br><br> 
	 * If it is not possible
	 * (like for an empty file or a file containing on line only) we set 
	 * <code>\n</code> as the default. In this case the line
	 * ending will be updated in the <code>textDocument/didChange</code>
	 * handler if needed.
	 */
	private void detectLineEnding() {
		if (source != null && source.contains("\r\n")) {
			lineEnding = "\r\n";
		} else {
			lineEnding = "\n";
		}
	}

	/**
	 * Updates the line ending for a file.
	 * This is needed for cases when the user changes the ending in the client.
	 * 
	 * @param changes A list of LSP change events. Practically the first change
	 * on the list is queried as client side line ending change triggers sending the whole source
	 * in the first (and only) change event on the list.
	 * 
	 * @return A boolean value indicating if the line ending has been changed.
	 */
	public boolean updateLineEnding(final List<TextDocumentContentChangeEvent> changes) {
		if (changes == null || changes.isEmpty()) {
			return false;
		}

		final String changeText = changes.get(0).getText();
		if (changeText == null || changeText.isEmpty()) {
			return false;
		}

		if (changeText.contains("\r\n")) {
			if (lineEnding.equals("\n")) {
				lineEnding = "\r\n";
				return true;
			}
		} 

		if (changeText.contains("\n") && !changeText.contains("\r")) {
			if (lineEnding.equals("\r\n")) {
				lineEnding = "\n";
				return true;
			}
		}

		return false;
	}

	private Range getUpdatedRange(final Range oldRange, final Range changeRange, final String text) {
		final int changeStartLine = changeRange.getStart().getLine();
		final int changeEndColumn = changeRange.getEnd().getCharacter();
		final int changeEndLine= changeRange.getEnd().getLine();
		final int oldStartLine = oldRange.getStart().getLine();
		final int oldEndLine = oldRange.getEnd().getLine();
		final int oldStartColumn = oldRange.getStart().getCharacter();
		final int oldEndColumn = oldRange.getEnd().getCharacter();
		int newStartColumn = oldStartColumn;
		int newEndColumn = oldEndColumn;
		int newStartLine = oldStartLine;
		int newEndLine = oldEndLine;
		int lineShift = getLineShift(changeRange, text);
		int columnShift = getColumnShift(text);

		if (oldStartLine > changeEndLine) {
			newStartLine += lineShift;
		}
		if (oldEndLine > changeEndLine) {
			newEndLine += lineShift;
		}

		if (changeStartLine == changeEndLine && changeStartLine == oldStartLine && oldStartColumn >= changeEndColumn) {
			newStartColumn = oldStartColumn + columnShift;
			newEndColumn = oldEndColumn + columnShift;
		}

		return new Range(
			new Position(newStartLine, newStartColumn),
			new Position(newEndLine, newEndColumn));
	}

	private int getLineShift(final Range changeRange, final String changeText) {
		final int insertedLines = (int)changeText.chars().filter(f -> f == '\n').count();
		return changeRange.getStart().getLine() - changeRange.getEnd().getLine() + insertedLines;
	}

	private int getColumnShift(final String changeText) {
		int columnShift = 0;
		if (changeText.contains(lineEnding)) {
			columnShift = changeText.substring(changeText.lastIndexOf(lineEnding)).length();
		} else {
			columnShift = changeText.length();
		}
		return columnShift;
	}

	/**
	 * Updates marker positions with line and column shifts caused by a change.
	 * 
	 * @param changeEvent the change event
	 */
	private void updateMarkers(final TextDocumentContentChangeEvent changeEvent) {
		final Range changeRange = new Range(changeEvent.getRange());
		final String text = changeEvent.getText();

		for (final LspMarker marker : markerList) {
			final Range oldRange = new Range(marker.getDiagnostic().getRange());
			marker.getDiagnostic().setRange(getUpdatedRange(oldRange, changeRange, text));
			if (marker.hasRelatedInformation()) {
				for (final DiagnosticRelatedInformation related : marker.getRelatedInformations()) {
					final org.eclipse.lsp4j.Location relatedLocation = related.getLocation();
					relatedLocation.setRange(getUpdatedRange(new Range(relatedLocation.getRange()), changeRange, text));
				}
			}
		}
	}

	/**
	 * Updates code lens positions with line and column shifts caused by a change.
	 * 
	 * @param changeEvent the change event
	 */
	private void updateCodeLenses(final TextDocumentContentChangeEvent changeEvent) {
		final Range changeRange = new Range(changeEvent.getRange());
		final String text = changeEvent.getText();

		for (final CodeLens lens : codelenses) {
			final Range oldRange = new Range(lens.getRange());
			lens.setRange(getUpdatedRange(oldRange, changeRange, text));
		}
	}

	/**
	 * Calculates a new location based on shifts caused by a document change.
	 * 
	 * @param oldLocation Location to be updated
	 * @param changeRange Range of the document change
	 * @param text New text inserted into the document
	 * @return A new location instance with the updated range 
	 */
	private Location updateLocation(final Location oldLocation, final Range changeRange, final String text) {
		final Location location = new Location(oldLocation.getFile(), oldLocation.getRange());
		final Range oldRange = new Range(location.getRange());
		final Range newRange = getUpdatedRange(oldRange, changeRange, text);
		location.setStartPosition(new Position(newRange.getStart()));
		location.setEndPosition(new Position(newRange.getEnd()));
		return location;
	}

	/**
	 * Recursively updates locations for DocumentSymbol items.<br><br>
	 * When changing a document, the server can receive the textDocument/documentSymbol request before 
	 * code analysis finishes, so symbols located after the changed region would have wrong positions.
	 *   
	 * @param changeEvent
	 */
	public void updateOutlineLocations(final TextDocumentContentChangeEvent changeEvent) {
		if (!isSyntacticallyUpToDate) {
			Module module = GlobalParser.getProjectSourceParser(Project.INSTANCE).containedModule(path);
			if (module != null) {
				DocumentSymbol symbols = module.getOutlineSymbol();
				updateDocumentSymbolChildren(changeEvent, symbols);
			}
		}
	}

	private void updateDocumentSymbolChildren(final TextDocumentContentChangeEvent changeEvent, final DocumentSymbol parent) {
		if (parent == null) {
			return;
		}
		final org.eclipse.lsp4j.Range range = parent.getRange();
		Location location = new Location(path.toFile(), range);
		Location newLocation = updateLocation(location, new Range(changeEvent.getRange()), changeEvent.getText());
		parent.setRange(new Range(newLocation));
		for (final DocumentSymbol child : parent.getChildren()) {
			updateDocumentSymbolChildren(changeEvent, child);
		}
	}

	/**
	 * Updates semantic highlighting positions with line and column shifts caused by a change.
	 * Handles both semantic tokens and modifiers.
	 * 
	 * @param changeEvent the change event
	 */
	private void updateSemanticHighlighting(final Path path, final TextDocumentContentChangeEvent changeEvent) {
		final Range changeRange = new Range(changeEvent.getRange());
		final String text = changeEvent.getText();

		final Map<Location,SemanticType> updatedTokens = new HashMap<>();
		final Map<Location,SemanticType> types = AstSemanticHighlighting.getSemanticTokens(path);
		if (types != null) {
			types.entrySet()
				.stream()
				.forEach(t ->
					updatedTokens.put(updateLocation(t.getKey(), changeRange, text), t.getValue())
				);
			AstSemanticHighlighting.setSemanticTokens(path, updatedTokens);
		}

		final Map<Location,List<SemanticModifier>> updatedModifiers = new HashMap<>();
		final Map<Location,List<SemanticModifier>> modifiers = AstSemanticHighlighting.getSemanticModifiers(path);
		if (modifiers != null) {
			modifiers.entrySet()
				.stream()
				.forEach(m ->
					updatedModifiers.put(updateLocation(m.getKey(), changeRange, text), m.getValue())
				);
			AstSemanticHighlighting.setSemanticModifiers(path, updatedModifiers);
		}
	}

	/**
	 * Updates the source code of this item by applying all the provided changes and
	 * modifies the version of the document. Additionally, diagnostic markers, code
	 * lenses and semantic highlighting tokens/modifiers are
	 * shifted by the line and column changes caused by each entry in the change set. 
	 * 
	 * @param changeEvents List of changes to apply to the source code
	 * @param version The new version of the document after all the changes are applied
	 * 
	 * @return Length difference of the old and new source code version
	 */
	public int applyChange(final List<TextDocumentContentChangeEvent> changeEvents, final int version) {
		final int oldLength = source.length();
		if (changeEvents != null) {
			final StringBuilder sbBefore = new StringBuilder();
			final StringBuilder sbAfter = new StringBuilder();

			for (TextDocumentContentChangeEvent changeEvent : changeEvents) {
				final Range range = new Range(changeEvent.getRange());
				final int beforeLine = range.getStart().getLine();
				final int startColumn = range.getStart().getCharacter();
				final int afterLine = range.getEnd().getLine();
				final int endColumn = range.getEnd().getCharacter();

				/* FIXME :: these are probably not needed */
				updateMarkers(changeEvent);
				updateCodeLenses(changeEvent);
				updateSemanticHighlighting(path, changeEvent);

				updateOutlineLocations(changeEvent);

				final BufferedReader reader = new BufferedReader(new StringReader(source));
				line = -1;
				reader.lines().forEach(str -> {
					line++;
					if (line < beforeLine) {
						if (line > 0 ) {
							sbBefore.append(lineEnding);
						}
						sbBefore.append(str);
						return;
					}
					if (line > afterLine) {
						if (line > 0 ) {
							sbAfter.append(lineEnding);
						}
						sbAfter.append(str);
						return;
					}
					if (line == beforeLine) {
						if (line > 0 ) {
							sbBefore.append(lineEnding);
						}
						sbBefore.append(str.substring(0, startColumn));
					}
					if (line == afterLine) {
						sbAfter.append(str.substring(endColumn));
					}
				});
				final String updatedSource = sbBefore.toString() + changeEvent.getText() + sbAfter.toString();				

				synchronized (lock) {
					setSource(updatedSource);
				}
				sbBefore.setLength(0);
				sbAfter.setLength(0);
			}
			this.version = version;
		}
		Project.fireResourceChanged(this);
		isSyntacticallyUpToDate = false;
		return source.length() - oldLength;
	}

	public boolean isOpen() {
		return documentItem instanceof TextDocumentItem;
	}

	public void setModule(final Module module) {
		ttcn3module = module;
	}

	/**
	 * @return the path
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(final Path path) {
		this.path = path;
	}

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

	/**
	 * @return the markerList
	 */
	public List<Diagnostic> getMarkerList() {
		return markerList.stream().map(LspMarker::getDiagnostic).collect(Collectors.toList());
	}

	/**
	 * @param marker
	 */
	public synchronized LspMarker addMarker(final LspMarker marker) {
		for (final LspMarker lspMarker : markerList) {
			final Diagnostic existingMarker = lspMarker.getDiagnostic();
			final Diagnostic newMarker = marker.getDiagnostic();
			if (existingMarker.getRange().equals(newMarker.getRange()) && (existingMarker.getMessage() == null || existingMarker.getMessage().equals(newMarker.getMessage()))) {
				return lspMarker;
			}
		}
		markerList.add(marker);
		return marker;
	}

	public synchronized LspMarker addMarker(final Diagnostic diagnostic) {
		return addMarker(new LspMarker(diagnostic));
	}

	public LspMarker getExistingMarker(final Location location, final String message) {
		for (final LspMarker marker : markerList) {
			if (marker.getDiagnostic().getRange().equals(location.getRange()) && message.equals(marker.getDiagnostic().getMessage())) {
				return marker;
			}
		}
		return null;
	}

	public void removeMarkers() {
		removeMarkers(false);
	}

	public void removeMarkers(final boolean semanticOnly) {
		markerListBackup = Set.copyOf(markerList);
		if (semanticOnly) {
			markerList.clear();
			for (final LspMarker marker : markerListBackup) {
				if (marker.isSyntactic()) {
					markerList.add(marker);
				}
			}
		} else {
			markerList.clear();
		}
	}

	public void removeMarkers(final Position start, final Position end) {
		removeMarkers(start, end, false);
	}

	public void removeMarkers(final Position start, final Position end, final boolean semanticOnly) {
		markerListBackup = Set.copyOf(markerList);
		
		final Range range = new Range(start, end);
		markerList.removeIf(f ->
			(! range.containsPositionClosed(new Position(f.getDiagnostic().getRange().getStart()))) && 
			(! range.containsPositionClosed(new Position(f.getDiagnostic().getRange().getEnd()))) &&
			(!semanticOnly || !f.isSyntactic())
		);
	}

	public void removeDiagnosticTag(final Position start, final Position end, final DiagnosticTag tag) {
		for (final LspMarker marker : markerList) {
			final Range range = new Range(marker.getDiagnostic().getRange());
			if (range.getStart().equals(start) && range.getEnd().equals(end)) {
				marker.getDiagnostic().getTags().remove(tag);
			}
		}
	}

	public void restoreMarkers() {
		if (markerListBackup != null && !markerListBackup.isEmpty()) {
			markerList.clear();
			for (final LspMarker marker : markerListBackup) {
				markerList.add(marker);
			}
		} else {
			markerList.clear();
		}
	}

	public void clearMarkers() {
		markerList.clear();
	}

	/**
	 * @return the ttcn3module
	 */
	public Module getModule() {
		return ttcn3module;
	}

	/**
	 * @return the item type
	 */
	public ItemType getItemtype() {
		return itemtype;
	}

	/**
	 * Gets the source of the project item.
	 * For open files, the source is kept in synch with the client and cached in memory.
	 * 
	 * Otherwise it is read from the file system. 
	 * 
	 * @return
	 */
	public String getSource() {
		if (source != null) {
			return source;
		}

		String sourceFromFile = null;
		try {
			sourceFromFile = new String(Files.readAllBytes(path));
		} catch (IOException e) {
			
		}
		return sourceFromFile;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
		isSyntacticallyUpToDate = false;
	}

	/**
	 * @return the isUntitled
	 */
	public boolean isUntitled() {
		return isUntitled;
	}

	/**
	 * @param isUntitled the isUntitled to set
	 */
	public void setUntitled(boolean isUntitled) {
		this.isUntitled = isUntitled;
	}

	/**
	 * @return the code lenses
	 */
	public List<CodeLens> getCodeLenses() {
//		final List<CodeLens> codeLensList = new ArrayList<>();
//		final Module module = getModule();
//		if (module != null && module instanceof TTCN3Module) {
//			((TTCN3Module)module).getDefinitions().forEach(def -> {
//				if (def instanceof Def_FunctionBase) {
//					final CodeLens codelens = ((Def_FunctionBase)def).getCodeLens();
//					if (codelens != null) {
//						codeLensList.add(codelens);
//					}
//				}
//			});
//		}
//
//		return codeLensList;
		return codelenses;
	}

	/**
	 * Adds a new CodeLens item. It is only added if the command does not yet exist at the given range.
	 * Consumers typically should use <code>updateCodeLens()</code> instead.
	 * 
	 * The collection of code lenses is thread safe, but this function should still be synchronized to prevent adding duplicates.
	 * 
	 * @param codelens
	 */
	public synchronized void addCodeLens(CodeLens codelens) {
		for (CodeLens lens : codelenses) {
			if (codelens.getRange().equals(lens.getRange()) && codelens.getCommand().getCommand().equals(lens.getCommand().getCommand())) {
				return;
			}
		}
		codelenses.add(codelens);
	}

	/**
	 * Updates an existing code lens item. It is added if the command does not yet exist at the given range.
	 * 
	 * The collection of code lenses is thread safe, but this function should still be synchronized to prevent adding duplicates.
	 * 
	 * @param codelens
	 */
	public synchronized void updateCodeLens(CodeLens codelens) {
		for (CodeLens lens : codelenses) {
			if (lens.getRange().equals(codelens.getRange())) {
				if (!lens.getCommand().getCommand().equals(codelens.getCommand().getCommand())) {
					lens.setCommand(codelens.getCommand());
					return;
				}
			}
		}
		addCodeLens(codelens);
	}

	/**
	 * Removes an existing codelens located at the specified range.
	 * @param range
	 */
	public synchronized void removeCodeLens(final Range range) {
		codelenses.removeIf(lens -> lens.getRange().equals(range));
	}

	public synchronized void clearCodeLenses() {
		codelenses.clear();
	}

	public Project getProject() {
		return project;
	}

	/**
	 * Sets the excluded state for this item.
	 * 
	 * @param isExcluded
	 */
	public void setExcluded(final boolean isExcluded) {
		this.isExcluded = isExcluded;
		if (isExcluded) {
			markerList.clear();
		}
	}

	/**
	 * Checks whether the resource is manually excluded.
	 * @return
	 */
	public boolean isExcluded() {
		return isExcluded;
	}

	/**
	 * Checks whether the resource is a directory.
	 * @return
	 */
	public boolean isDirectory() {
		return itemtype == ItemType.Directory;
	}

	public boolean isSyntacticallyUpToDate() {
		return isSyntacticallyUpToDate;
	}

	public void setSyntacticallyUpToDate(boolean isAnalyzed) {
		this.isSyntacticallyUpToDate = isAnalyzed;
	}

	public String getLineEnding() {
		return lineEnding;
	}

	public void reset() {
		final PublishDiagnosticsParams diagnostics = new PublishDiagnosticsParams();
		final LanguageClient client = TitanLanguageServer.getClient();

		removeMarkers();
		clearCodeLenses();
		setSyntacticallyUpToDate(false);

		final ProjectSourceParser projectParser = GlobalParser.getProjectSourceParser(project);
		projectParser.reportOutdating(path);
		AstSemanticHighlighting.clearSemanticTokensAndModifiers(path);
		diagnostics.setUri(path.toUri().toString());
		client.publishDiagnostics(diagnostics);
	}

	public static String getLineEnding(final Location location) {
		if (location == null || location == NULL_Location.INSTANCE || location.getFile() == null) {
			return "";
		}
		final ProjectItem item = Project.INSTANCE.getProjectItem(location.getFile().toPath());
		if (item == null) {
			return "";
		}
		return item.getLineEnding();
	}

	/**
	 * Returns the source code lines for a range of line numbers.
	 * 
	 * @param first First line to include
	 * @param last Last line to include
	 * @return
	 */
	public List<String> getSourceLines(int first, int last) {
		final List<String> lines = new ArrayList<>();
		if (source == null) {
			return lines;
		}
		final LineNumberReader reader = new LineNumberReader(new StringReader(source));
		while (reader.getLineNumber() <= last) {
			try {
				String lineText = reader.readLine();
				if (lineText == null) {
					return lines;
				}
				if (reader.getLineNumber() > first) {
					lines.add(lineText);
				}
			} catch (IOException e) {
				return lines;
			}
		}
		return lines;
	}

	@Override
	public String toString() {
		return path.toString();
	}
}
