/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.Set;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IGovernedSimple;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.TypeFactory;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public interface ITTCN3Template extends IGovernedSimple, IIncrementallyUpdatable {
	String ASTERIX = GeneralConstants.ASTERIX;
	String QUESTION_MARK = GeneralConstants.QUESTION_MARK;
	String IFPRESENT = "ifpresent";
	String ERRONEOUS_TEMPLATE = "<erroneous template>";

	enum Template_type {
		/** all from template type, hides its real type */
		ALL_FROM("all from"),
		/** any or omit (*). */
		ANY_OR_OMIT("any or omit"),
		/** any value (?). */
		ANY_VALUE("any value"),
		/** bitstring pattern. */
		BSTR_PATTERN("bitstring pattern"),
		/** complemented list match. */
		COMPLEMENTED_LIST("complemented list match"),
		/** advanced matching: conjunction match */
		CONJUNCTION_MATCH("conjunction match"),
		/** character string pattern. */
		CSTR_PATTERN("character string pattern"),
		/** decode match. */
		DECODE_MATCH("decode match"),
		/** advanced matching: dynamic match */
		DYNAMIC_MATCH("dynamic match"),
		/** hexstring pattern. */
		HSTR_PATTERN("hexstring pattern"),
		/** advanced matching: implication match */
		IMPLICATION_MATCH("implication match"),
		/** assignment notation with array indices. */
		INDEXED_TEMPLATE_LIST("indexed assignment notation"),
		/** assignment notation. */
		NAMED_TEMPLATE_LIST("assignment notation"),
		/** omit. */
		OMIT_VALUE("omit value"),
		/** octetstring pattern. */
		OSTR_PATTERN("octetstring pattern"),
		/** permutation match. */
		PERMUTATION_MATCH("permutation match"),
		/** specific value. */
		SPECIFIC_VALUE("specific value"),
		/** subset match. */
		SUBSET_MATCH("subset match"),
		/** superset match. */
		SUPERSET_MATCH("superset match"),
		// FIXME missing support for template concat
		/** concatenation of two templates */
		//TEMPLATE_CONCAT,
		/** template returning invoke. */
		TEMPLATE_INVOKE("template returning invoke"),
		/** value list notation. */
		TEMPLATE_LIST("value list notation"),
		/** not used symbol (-). */
		TEMPLATE_NOTUSED("not used symbol"),
		/** reference to another template. */
		TEMPLATE_REFD("referenced template"),
		/** universal charstring pattern. */
		USTR_PATTERN("universal character string pattern"),
		/** value list match. */
		VALUE_LIST("value list match"),
		/** value range match. */
		VALUE_RANGE("value range match");

		private static final String ERRONEOUS = "erroneous ";
		private final String name;

		private Template_type(final String name) {
			this.name = name;
		}

		/** @return the name of this template type */
		public String getName() {
			return name;
		}

		/** @return "erroneous" + the name of this template type */
		public String getErroneousName() {
			return ERRONEOUS + getName();
		}
	}

	enum Completeness_type {
		/** the body must be completely specified */
		MUST_COMPLETE,
		/** the body may be incompletely specified */
		MAY_INCOMPLETE,
		/**
		 * some part of the body may be incomplete, others must be complete
		 */
		PARTIAL
	}

	/**
	 * Copies the general template -ish properties of the template in
	 * parameter to the actual one.
	 * <p>
	 * This function is used to help writing conversion function without
	 * using a generic copy-constructor mechanism.
	 *
	 * @param original the original template, whose properties will be copied
	 */
	void copyGeneralProperties(final ITTCN3Template original);

	/** @return the internal type of the template */
	Template_type getTemplatetype();

	/** @return the name of type of the template. */
	default String getTemplateTypeName() {
		return getIsErroneous() ? getTemplatetype().getErroneousName() : getTemplatetype().getName();
	}

	/**
	 * Returns the governor type.
	 * @return the governor type.
	 */
	@Override
	IType getMyGovernor();

	/**
	 * Sets the governor type.
	 * @param governor the type to be set.
	 */
	void setMyGovernor(final IType governor);

	String chainedDescription();

	/**
	 * Creates and returns a string representation of the actual template.
	 * @return the string representation of the template.
	 */
	String createStringRepresentation();

	/**
	 * Creates template references from a template that is but a single word.
	 * This can happen if it was not possible to categorize it while parsing.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 *
	 * @return the reference that this lower identifier was converted to, or
	 *         this template.
	 */
	default ITTCN3Template setLoweridToReference(final CompilationTimeStamp timestamp) {
		return this;
	}

	/**
	 * Sets the length restriction of the template.
	 * @param lengthRestriction the length restriction to set
	 */
	void setLengthRestriction(final LengthRestriction lengthRestriction);

	/** @return the length restriction of the template */
	LengthRestriction getLengthRestriction();

	boolean hasLengthRestriction();

	void setIfpresent();

	boolean getIfPresent();

	/** @return the base template of the actual template */
	ITTCN3Template getBaseTemplate();

	/**
	 * Sets the base template.
	 * @param baseTemplate the template to set as the base template of this template.
	 */
	void setBaseTemplate(final ITTCN3Template baseTemplate);

	/**
	 * Checks the condition for the completeness of template body which is a
	 * 'record of' or 'set of' template.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param incompleteAllowed tells if incomplete list is allowed
	 *               			in the calling context or not.
	 *
	 * @return the type of completeness, that can be expected from this
	 *         template in the actual context
	 */
	Completeness_type getCompletenessConditionSeof(final CompilationTimeStamp timestamp, final boolean incompleteAllowed);

	/**
	 * Checks the condition for the completeness of template body which is a
	 * 'record of' or 'set of' template.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param incompleteAllowed tells if incomplete list is allowed
	 *                in the calling context or not.
	 * @param fieldName the name of the field to check for.
	 *
	 * @return the type of completeness, that can be expected from this template in the actual context
	 */
	Completeness_type getCompletenessConditionChoice(final CompilationTimeStamp timestamp, final boolean incompleteAllowed,
			final Identifier fieldName);

	/**
	 * Returns the template referred last in case of a referred template, or
	 * itself in any other case.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 *
	 * @return the actual or the last referred template
	 */
	ITTCN3Template getTemplateReferencedLast(final CompilationTimeStamp timestamp);

	/**
	 * Returns the template referred last in case of a referred template, or
	 * itself in any other case.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param referenceChain the ReferenceChain used to detect circular references
	 *
	 * @return the actual or the last referred template
	 */
	TTCN3Template getTemplateReferencedLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain);

	/**
	 * Creates a template of the provided type from the actual template if it is possible.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param newType the new template_type the new template should belong to.
	 *
	 * @return the new template of the provided kind if the conversion is
	 *         possible, or this template otherwise.
	 */
	default ITTCN3Template setTemplatetype(final CompilationTimeStamp timestamp, final Template_type newType) {
		setIsErroneous(true);
		return this;
	}

	/**
	 * Calculates the return type of the template when used in an expression.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param expectedValue the kind of the value to be expected.
	 *
	 * @return the Type_type of the template if it was used in an expression.
	 */
	default Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return Type_type.TYPE_UNDEFINED;
	}

	/**
	 * Calculates the governor of the template when used in an expression.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param expectedValue the kind of the value to be expected.
	 *
	 * @return the governor of the template if it was used in an expression.
	 */
	default IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return getMyGovernor() == null ? TypeFactory.createType(getExpressionReturntype(timestamp, expectedValue)) : getMyGovernor();
	}

	/**
	 * Checks for circular references within embedded templates.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param referenceChain the ReferenceChain used to detect circular references,
	 *                		 must not be null.
	 */
	default void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		// Do nothing by default
	}

	/**
	 * Check that the template - being used as the RHS - refers to the LHS of the assignment.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param lhs the assignment to check against
	 * @return true if self-assignment
	 */
	default boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		return false;
	}

	/**
	 * Checks whether the template (including embedded fields) contains no matching symbols.
	 * Allow_omit is used because omit is allowed in only in embedded fields.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param allowOmit whether the omit value is allowed at this point or not.
	 */
	void checkSpecificValue(final CompilationTimeStamp timestamp, boolean allowOmit);

	/**
	 * Calculates the referenced sub template, and also checks the reference itself.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param reference the reference used to find the sub template.
	 * @param referenceChain the reference chain used to detect circular references.
	 * @param silent {@code true} if errors are not to be reported.
	 */
	ITTCN3Template getReferencedSubTemplate(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain referenceChain, final boolean silent);

	/**
	 * Checks if the template is actually a value.
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @return {@code true} if the contents of the template can be handled as a value.
	 */
	default boolean isValue(final CompilationTimeStamp timestamp) {
		return false;
	}

	/**
	 * @return the value contained in this template if it can be handled as
	 *         a value, {@code null} otherwise
	 */
	default IValue getValue() {
		return null;
	}

	/**
	 * Checks the generic properties of the template, and serves as starting
	 * point for the more specific checks.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param type the type this template should be checked against
	 * @param isModified should be true if this template is a modified template.
	 * @param allowOmit enables the acceptance of omit templates.
	 * @param allowAnyOrOmit enables the acceptance of any or omit template.
	 * @param subCheck enables the checking of sub types.
	 * @param implicitOmit {@code true} if the implicit omit optional attribute was set
	 *                	   for the template, false otherwise
	 * @param lhs the assignment to check against
	 * @return {@code true} if the value contains a reference to lhs
	 */
	boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified, final boolean allowOmit,
			final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs);

	/**
	 * Checks template restriction using common data members of this class,
	 * every check_xxx_restriction() function must call this function.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param definitionName name for the error/warning message
	 * @param templateRestriction the template restriction to check
	 * @param usageLocation the location to be used for reporting errors
	 */
	void checkRestrictionCommon(final CompilationTimeStamp timestamp, final String definitionName, final TemplateRestriction.Restriction_type templateRestriction, final Location usageLocation);

	/**
	 * Checks if this template conforms to the restriction TR_OMIT or
	 * TR_VALUE This is the default behavior, override for special cases.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param definitionName name for the error/warning message
	 * @param omitAllowed {@code true} in case of TR_OMIT, {@code false} in case of TR_VALUE
	 * @param usageLocation the location to be used for reporting errors
	 *
	 * @return <li>false = always satisfies restriction -> no runtime check
	 *         needed or never satisfies restriction -> compiler error(s)</li>
	 *         <li>true = possibly violates restriction, cannot be determined at
	 *         compile time -> runtime check needed and compiler warning
	 *         given when inadequate restrictions are used, in other cases
	 *         there's no warning</li>
	 */
	boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation);

	/**
	 * Helper function for check_valueomit_restriction called by Named_Template_List instances.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param definitionName name for the error/warning message.
	 * @param checkedNames the names of the named templates already checked.
	 * @param neededCheckedCnt the number of elements left to be checked.
	 * @param usageLocation the location to be used for reporting errors
	 *
	 * @return {@code true} if a check at runtime is needed, {@code false} otherwise.
	 */
	default boolean chkRestrictionNamedListBaseTemplate(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed,
			final Set<String> checkedNames, final int neededCheckedCnt, final Location usageLocation) {
		return false;
	}

	/**
	 * Checks if this template conforms to the restriction TR_PRESENT.
	 * This is the default behavior, override for special cases.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * @param definitionName name for the error/warning message
	 * @param usageLocation the location to be used for reporting errors
	 *
	 * @return {@code true} if the template conforms to the restriction TR_PRESENT.
	 */
	boolean checkPresentRestriction(final CompilationTimeStamp timestamp, final String definitionName, final Location usageLocation);

	/**
	 * Sets the definition of the statement block of the dynamic match template
	 * @param definition the definition to be set
	 */
	default void setMyDefinition(final Definition definition) {
		// Do nothing by default
	}
}
