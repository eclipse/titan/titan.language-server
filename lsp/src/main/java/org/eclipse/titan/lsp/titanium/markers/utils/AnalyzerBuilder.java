/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.utils;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseProjectCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.spotters.impementation.StaticData;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;
import org.eclipse.titan.lsp.titanium.preferences.ProblemTypePreference;

/**
 * Builder class for {@link Analyzer} instances.
 * <p>
 * This class is used to configure and construct an immutable
 * <code>Analyzer</code>. Instances of this class are acquired via
 * {@link Analyzer#builder()}.
 *
 * @author poroszd
 * @author Adam Knapp
 */
public final class AnalyzerBuilder {
	private static final String ILLEGAL_STATE = "One must not use the builder after build() method is called";

	private final Map<Class<? extends IVisitableNode>, Set<BaseModuleCodeSmellSpotter>> actions;
	private final Set<BaseProjectCodeSmellSpotter> projectActions;
	private final Map<CodeSmellType, BaseModuleCodeSmellSpotter[]> spotters;
	private final Map<CodeSmellType, BaseProjectCodeSmellSpotter[]> projectSpotters;
	private boolean exhausted;

	AnalyzerBuilder() {
		actions = new HashMap<Class<? extends IVisitableNode>, Set<BaseModuleCodeSmellSpotter>>();
		projectActions = new HashSet<BaseProjectCodeSmellSpotter>();
		spotters = StaticData.newSpotters();
		projectSpotters = StaticData.newProjectSpotters();
		exhausted = false;
	}

	/**
	 * Create the <code>Analyzer</code>.
	 * <p>
	 * Constructing the product consumes <code>AnalyzerBuilder</code>
	 * instance, and no further methods are permitted to be called. Calling
	 * any modifier method of an exhausted <code>AnalyzerBuilder</code> will
	 * throw an <code>IllegalStateException</code>.
	 *
	 * @return the created <code>Analyzer</code> instance
	 */
	public Analyzer build() {
		if (exhausted) {
			throw new IllegalStateException(ILLEGAL_STATE);
		}

		exhausted = true;
		return new Analyzer(actions, projectActions);
	}

	public AnalyzerBuilder adaptPreferences() {
		if (exhausted) {
			throw new IllegalStateException(ILLEGAL_STATE);
		}

		actions.clear();
		projectActions.clear();

		// TODO: Temp. solution till the preferences of the code smells are implemented 
		if (Configuration.INSTANCE.getBoolean(Configuration.REPORT_KEYWORD_USED_AS_IDENTIFIER, false) &&
			!Configuration.INSTANCE.getBoolean(Configuration.ENABLE_TITANIUM, false)) {
			addPreferenceProblem(ProblemTypePreference.RESERVED_WORD_USAGE);
			return this;
		}

//		final IPreferencesService prefs = Platform.getPreferencesService();
		for (final ProblemTypePreference prefType : ProblemTypePreference.values()) {
			final String prefName = prefType.getPreferenceName();
//			final String warnLevel = prefs.getString(Activator.PLUGIN_ID, prefName, GeneralConstants.IGNORE, null);
//			if (!GeneralConstants.IGNORE.equals(warnLevel)) {
				addPreferenceProblem(prefType);
//			}
		}
		return this;
	}

	/**
	 * Add the set of code smells to the under-construction
	 * <code>Analyzer</code>, that is associated with this preference
	 * problem type.
	 *
	 * @param preferenceProblem the analyzer will use its code smells
	 * @return this for method chaining
	 */
	public AnalyzerBuilder addPreferenceProblem(final ProblemTypePreference preferenceProblem) {
		if (exhausted) {
			throw new IllegalStateException(ILLEGAL_STATE);
		}

		for (final CodeSmellType problemType : preferenceProblem.getRelatedProblems()) {
			addProblem(problemType);
		}
		return this;
	}

	/**
	 * @param problemType the analyzer will use this code smell
	 * @return this for method chaining
	 */
	public AnalyzerBuilder addProblem(final CodeSmellType problemType) {
		if (exhausted) {
			throw new IllegalStateException(ILLEGAL_STATE);
		}

		if (spotters.get(problemType) != null) {
			for (final BaseModuleCodeSmellSpotter spotter : spotters.get(problemType)) {
				final Queue<Class<? extends IVisitableNode>> subtypes = new ArrayDeque<Class<? extends IVisitableNode>>();
				subtypes.addAll(spotter.getStartNodes());
				while (!subtypes.isEmpty()) {
					final Class<? extends IVisitableNode> sub = subtypes.poll();
					if (StaticData.TYPE_HIERARCHY.get(sub) != null) {
						Collections.addAll(subtypes, StaticData.TYPE_HIERARCHY.get(sub));
					}
					if (actions.get(sub) == null) {
						actions.put(sub, new HashSet<BaseModuleCodeSmellSpotter>());
					}
					actions.get(sub).add(spotter);
				}
			}
		} else if (projectSpotters.get(problemType) != null) {
			Collections.addAll(projectActions, projectSpotters.get(problemType));
		}
		return this;
	}
}
