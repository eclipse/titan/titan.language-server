/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.common.usagestats;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.ProductIdentity;

/**
 * This class collects the platform related information.
 */
public final class UsageStatCollector {
	private static final Map<String, String> USAGE_DATA = new HashMap<String, String>();

	private UsageStatCollector() {
		// Do nothing
	}

	private static void collectData() {
		USAGE_DATA.put("plugin_id", ProductIdentity.TITAN_PREFIX + ProductIdentity.PRODUCT_ID_LS);
		USAGE_DATA.put("eclipse_version", "");
		USAGE_DATA.put("eclipse_version_qualifier", "");
		USAGE_DATA.put("plugin_version", ProductIdentity.getCurrentVersion());
		USAGE_DATA.put("plugin_version_qualifier", "");
		USAGE_DATA.put("info", "");
		try {
			USAGE_DATA.put("hostname", InetAddress.getLocalHost().getCanonicalHostName());
		} catch (final Exception e) {
			TitanLogger.logError("While resolving the local host's address", e);
			USAGE_DATA.put("hostname", "UNKNOWN");
		}
	}

	private static void collectSystemData() {
		try {
			USAGE_DATA.put("user_id", System.getProperty("user.name"));
			USAGE_DATA.put("os_name", System.getProperty("os.name"));
			USAGE_DATA.put("os_arch", System.getProperty("os.arch"));
			USAGE_DATA.put("os_version", System.getProperty("os.version"));
			USAGE_DATA.put("java_version", System.getProperty("java.version"));
			USAGE_DATA.put("java_vendor", System.getProperty("java.vendor"));
		} catch (final SecurityException e) {
			TitanLogger.logError("Could not access a system property", e);
		}
	}

	public static Map<String, String> getUsageData() {
		if (USAGE_DATA.isEmpty()) {
			collectSystemData();
			collectData();
		}
		return USAGE_DATA;
	}
}
