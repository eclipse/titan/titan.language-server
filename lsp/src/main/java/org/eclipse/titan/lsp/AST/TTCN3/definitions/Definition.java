/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.DiagnosticTag;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IAppendableSyntax;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.IModifiableVisibility;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ErroneousAttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ErroneousAttributeSpecification.Indicator_Type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ErroneousAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifier;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ParserUtilities;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3CharstringLexer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Reparser.Pr_ErroneousAttributeSpecContext;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticModifier;

/**
 * The definition class represents general TTCN3 definitions.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public abstract class Definition extends Assignment
	implements IAppendableSyntax, IIncrementallyUpdatable, ICommentable, ICompletionItem, IModifiableVisibility {

	protected WithAttributesPath withAttributesPath = null;
	protected ErroneousAttributes erroneousAttributes = null;

	protected String genName = EMPTY_STRING;

	/** Indicates if the definition is inherited from a parent class */
	private boolean isInherited = false;

	/**
	 * The cumulative location of a definition is the location of the definition if it is stand alone,
	 *  or the whole short hand notation it is enclosed into.
	 *
	 * TTCN-3 allows to write some definitions with a shorthand notation.
	 * for example: const integer a1:=1,a2:=2;
	 * In these cases the locations of the definitions overlap, creating various problems.
	 *
	 * To make the situation clear cumulative location stores the location of the whole shorthand notation
	 *  the definition is listed in.
	 * This way it becomes easy to identify which definitions can be affected by a change in the file.
	 * */
	private Location cumulativeDefinitionLocation = null;

	/** The visibility modifier of this definition,
	 *  the default is {@link VisibilityModifier#Public}. Should be set by the parser.
	 * */
	protected VisibilityModifier visibilityModifier = VisibilityModifier.Public;
	/** The location of visibility modifier of this definition */
	protected Location visibilityModifierLocation = Location.getNullLocation();

	//TODO this should be removed as the same functionality is present in Titanium as codesmell.
	@Deprecated
	public List<String> referingHere = new ArrayList<String>();

	/** Optional documentation comment according to ETSI ES 201 873-10 */
	protected DocumentComment documentComment = null;

	/** content of hover window for this definition */
	protected Ttcn3HoverContent hoverContent = new Ttcn3HoverContent();

	protected Definition(final Identifier identifier) {
		super(identifier);
	}

	@Override
	/** {@inheritDoc} */
	public final void setVisibilityModifier(final VisibilityModifier modifier, final Location location) {
		visibilityModifier = modifier;
		visibilityModifierLocation = location;
	}

	@Override
	/** {@inheritDoc} */
	public final VisibilityModifier getVisibilityModifier() {
		return visibilityModifier;
	}

	@Override
	/** {@inheritDoc} */
	public final Location getVisibilityModifierLocation() {
		return visibilityModifierLocation;
	}

	/**
	 * Sets the with attributes for this definition if it has any.
	 * Also creates the with attribute path, to store the attributes in.
	 *
	 * @param attributes the attribute to be added.
	 */
	public void setWithAttributes(final MultipleWithAttributes attributes) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}
		// let the qualifiers know which definition they belong to
		// TODO: this should be set while semantically analyzing qualifiers
		if (attributes != null) {
			withAttributesPath.setWithAttributes(attributes);
			for (final SingleWithAttribute swa : attributes) {
				final Qualifiers qs = swa.getQualifiers();
				if (qs == null) {
					continue;
				}
				for (final Qualifier q : qs) {
					q.setDefinition(this);
				}
			}
		}
	}

	/**
	 * In TTCN-3 it is possible to use a short hand notation
	 *  when defining several constants and variables of the same type.
	 *  for example: const integer x :=5, j:=6;
	 *
	 * In these cases the location attributed to each definition is their own minimal location.
	 * The ComulativeDefinitionLocation is the location of the whole set of same typed definition.
	 *
	 * @return the location of the same typed definition list the definition is located in.
	 * */
	public Location getCumulativeDefinitionLocation() {
		if (cumulativeDefinitionLocation != null) {
			return cumulativeDefinitionLocation;
		}

		return getLocation();
	}

	/**
	 * Set the cumulative location of this definition
	 * */
	public void setCumulativeDefinitionLocation(final Location location) {
		this.cumulativeDefinitionLocation = location;
	}

	/**
	 * @return the with attribute path element of this definition. If it did
	 *         not exist it will be created.
	 * */
	public WithAttributesPath getAttributePath() {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		return withAttributesPath;
	}

	/**
	 * Sets the parent path for the with attribute path element of this
	 * definition. Also, creates the with attribute path node if it did not
	 * exist before.
	 *
	 * @param parent
	 *                the parent to be set.
	 * */
	public void setAttributeParentPath(final WithAttributesPath parent) {
		if (withAttributesPath == null) {
			withAttributesPath = new WithAttributesPath();
		}

		withAttributesPath.setAttributeParent(parent);
	}

	/**
	 * Checks if this definition has an implicit omit attribute or not.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return true if it has an implicit omit attribute, false if no
	 *         attribute is given or explicit omit is specified.
	 * */
	public boolean hasImplicitOmitAttribute(final CompilationTimeStamp timestamp) {
		if (withAttributesPath == null) {
			return false;
		}

		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);
		SingleWithAttribute tempAttribute;
		for (int i = realAttributes.size() - 1; i >= 0; i--) {
			tempAttribute = realAttributes.get(i);
			if (tempAttribute != null && Attribute_Type.Optional_Attribute.equals(tempAttribute.getAttributeType())) {
				final String tempSpecification = tempAttribute.getAttributeSpecification().getSpecification();
				if ("implicit omit".equals(tempSpecification)) {
					return true;
				} else if ("explicit omit".equals(tempSpecification)) {
					return false;
				}
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public String getGenName() {
		if(genName.isEmpty()) {
			return identifier.getName();
		}

		return genName;
	}

	public void setGenName(final String genName) {
		this.genName = genName;
	}

	/**
	 * @return the kind of the object reported to the proposal collector.
	 * */
	public abstract String getProposalKind();

	@Override
	/** {@inheritDoc} */
	public String getProposalDescription() {
		return getProposalKind();
	}

	protected void checkErroneousAttributes(final CompilationTimeStamp timestamp) {
		erroneousAttributes = null;
		if (withAttributesPath != null) {
			final MultipleWithAttributes attribs = withAttributesPath.getAttributes();
			if (attribs == null) {
				return;
			}
			for (final SingleWithAttribute actualAttribute : attribs) {
				if (actualAttribute.getAttributeType() == Attribute_Type.Erroneous_Attribute) {
					final int nofQualifiers = (actualAttribute.getQualifiers() == null) ? 0
							: actualAttribute.getQualifiers().size();
					final List<IType> referencedTypeArray = new ArrayList<IType>(nofQualifiers);
					final List<ArrayList<Integer>> subrefsArrayArray = new ArrayList<ArrayList<Integer>>(nofQualifiers);
					final List<ArrayList<IType>> typeArrayArray = new ArrayList<ArrayList<IType>>(nofQualifiers);
					if (nofQualifiers == 0) {
						actualAttribute.getLocation().reportSemanticError(
								"At least one qualifier must be specified for the `erroneous' attribute");
					} else {
						// check if qualifiers point to existing fields
						for (int qi = 0; qi < nofQualifiers; qi++) {
							final Qualifier actualQualifier = actualAttribute.getQualifiers().get(qi);
							final IType definitionType = getType(timestamp);
							// construct a reference
							final Reference reference = new Reference(null);
							reference.addSubReference(new FieldSubReference(identifier));
							for (final ISubReference sr : actualQualifier) {
								reference.addSubReference(sr);
							}
							reference.setLocation(actualQualifier.getLocation());
							reference.setMyScope(getMyScope());
							IType fieldType = definitionType.getFieldType(timestamp, reference, 1,
									Expected_Value_type.EXPECTED_CONSTANT, false);
							ArrayList<Integer> subrefsArray = null;
							ArrayList<IType> typeArray = null;
							if (fieldType != null) {
								subrefsArray = new ArrayList<Integer>();
								typeArray = new ArrayList<IType>();
								final boolean validIndexes = definitionType.getSubrefsAsArray(timestamp, reference, 1,
										subrefsArray, typeArray);
								if (!validIndexes) {
									fieldType = null;
									subrefsArray = null;
									typeArray = null;
								}
								if (reference.refersToStringElement()) {
									actualQualifier.getLocation()
									.reportSemanticError(
											"Reference to a string element cannot be used in this context");
									fieldType = null;
									subrefsArray = null;
									typeArray = null;
								}
							}
							referencedTypeArray.add(fieldType);
							subrefsArrayArray.add(subrefsArray);
							typeArrayArray.add(typeArray);
						}
					}
					// parse the attr. spec.
					final ErroneousAttributeSpecification errAttributeSpecification = parseErrAttrSpecString(actualAttribute
							.getAttributeSpecification());
					if (errAttributeSpecification != null) {
						if (erroneousAttributes == null) {
							erroneousAttributes = new ErroneousAttributes(getType(timestamp));
						}
						erroneousAttributes.addSpecification(errAttributeSpecification);
						errAttributeSpecification.check(timestamp, getMyScope());
						// create qualifier -
						// err.attr.spec. pairs
						for (int qi = 0; qi < nofQualifiers; qi++) {
							if (referencedTypeArray.get(qi) != null
									&& errAttributeSpecification.getIndicator() != Indicator_Type.Invalid_Indicator) {
								final Qualifier actualQualifier = actualAttribute.getQualifiers().get(qi);
								erroneousAttributes.addFieldErr(actualQualifier, errAttributeSpecification,
										subrefsArrayArray.get(qi), typeArrayArray.get(qi));
							}
						}
					}
				}
			}
			if (erroneousAttributes != null) {
				erroneousAttributes.check(timestamp);
			}
		}
	}

	/**
	 * The static version of checkErroneousAttributes function.
	 *
	 * @param timestamp
	 */
	public static ErroneousAttributes checkErroneousAttributes(final MultipleWithAttributes p_attrib, final IType p_type, final Scope p_scope, final String p_fullname, final boolean in_update_stmt, final CompilationTimeStamp timestamp, final Reference ref) {
		if (p_attrib == null) {
			return null;
		}

		ErroneousAttributes erroneous_attrs = null;
		for (final SingleWithAttribute act_attr : p_attrib) {
			if (act_attr.getAttributeType() == Attribute_Type.Erroneous_Attribute) {
				boolean useRuntime2 = false;
// FIXLSP				try {
//					if ("true".equals(p_attrib.getLocation().getFile().getProject().getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
//							MakefileCreationData.FUNCTIONTESTRUNTIME_PROPERTY)))) {
//						useRuntime2 = true;
//					}
//				} catch (CoreException e) {
//					TitanLogger.logError("Error while reading persistent property", e);
//				}

				if (!useRuntime2) {
					p_attrib.getLocation().reportSemanticError("`erroneous' attributes can be used only with the Function Test Runtime");
					return null;
				}
			}
		}
			return null;
			//TODO: runtime 2
			/**final int nof_qualifiers = act_attr.getQualifiers() != null ? act_attr.getQualifiers().getNofQualifiers() : 0;
				final List<IType> referencedTypeArray = new ArrayList<IType>(nof_qualifiers);
				final List<ArrayList<Integer>> subrefsArrayArray = new ArrayList<ArrayList<Integer>>(nof_qualifiers);
				final List<ArrayList<IType>> typeArrayArray = new ArrayList<ArrayList<IType>>(nof_qualifiers);
				if (nof_qualifiers == 0) {
					act_attr.getLocation().reportSemanticError(
							"At least one qualifier must be specified for the `erroneous' attribute");
				} else {
					// check if qualifiers point to
					// existing fields
					for (int qi = 0; qi < nof_qualifiers; qi++) {
						final Qualifier actualQualifier = act_attr.getQualifiers().getQualifierByIndex(qi);
						final Reference reference = new Reference(null);
						reference.addSubReference(new FieldSubReference(ref.getId()));
						for (int ri = 0; ri < actualQualifier.getNofSubReferences(); ri++) {
							reference.addSubReference(actualQualifier.getSubReferenceByIndex(ri));
						}
						reference.setLocation(actualQualifier.getLocation());
						reference.setMyScope(p_scope);
						IType fieldType = p_type.getFieldType(timestamp, reference, 1,
								Expected_Value_type.EXPECTED_CONSTANT, false);
						ArrayList<Integer> subrefsArray = null;
						ArrayList<IType> typeArray = null;
						if (fieldType != null) {
							subrefsArray = new ArrayList<Integer>();
							typeArray = new ArrayList<IType>();
							final boolean validIndexes = p_type.getSubrefsAsArray(timestamp, reference, 1,
									subrefsArray, typeArray);
							if (!validIndexes) {
								fieldType = null;
								subrefsArray = null;
								typeArray = null;
							}
							if (reference.refersToStringElement()) {
								actualQualifier.getLocation()
								.reportSemanticError(
										"Reference to a string element cannot be used in this context");
								fieldType = null;
								subrefsArray = null;
								typeArray = null;
							}
						}
						referencedTypeArray.add(fieldType);
						subrefsArrayArray.add(subrefsArray);
						typeArrayArray.add(typeArray);
					}
				}
				final ErroneousAttributeSpecification errAttributeSpecification = parseErrAttrSpecString(act_attr.getAttributeSpecification());
				if (errAttributeSpecification != null) {
					errAttributeSpecification.check(timestamp, p_scope);
					erroneous_attrs = new ErroneousAttributes(p_type);
					erroneous_attrs.addSpecification(errAttributeSpecification);
				}
			}
		}
		return erroneous_attrs;**/
		}

	/**
	 * Check if two definitions are (almost) identical: the type and
	 * dimensions must always be identical, the initial values can be
	 * different depending on the definition type. If error was reported the
	 * return value is false. The initial values (if applicable) may be
	 * present/absent, different or un-foldable. The function must be
	 * overridden to be used.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param definition
	 *                the definition to compare against the actual one.
	 *
	 * @return true if they are (almost) identical, false otherwise.
	 */
	public boolean checkIdentical(final CompilationTimeStamp timestamp, final Definition definition) {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		final int refcount = getReferenceCount();
		if (refcount == 0) {
			final List<DiagnosticTag> tags = new ArrayList<>();
			tags.add(DiagnosticTag.Unnecessary);
			addDiagnosticTags(tags);
		}

		if (this instanceof Def_FunctionBase) {
			final List<org.eclipse.titan.lsp.AST.Location> referenceList = getDeclaration().getAssignment().getReferenceLocationList();
			if (referenceList == null) {
				return;
			}

			final List<org.eclipse.lsp4j.Location> refLocations = referenceList.stream()
					.map(m -> m.getLspLocation())
					.collect(Collectors.toList());

			final CodeLens lens = new CodeLens(identifier.getLocation().getRange());
			final Command command = new Command();

			final List<Object> args = new ArrayList<>();
			args.add(getLocation().getFile().toURI().toString());
			args.add(lens.getRange().getStart());
			args.add(refLocations);
			command.setTitle("Referenced " + refcount + " times");
			command.setCommand("titan.getReferencesForCodelens");
			command.setArguments(args);
			lens.setCommand(command);
			updateCodeLens(lens);
		}
	}

	@Override
	public void checkDocumentComment() {
		if (hasDocumentComment()) {
			if (documentComment.isDeprecated()) {
				final List<DiagnosticTag> tags = new ArrayList<>();
				tags.add(DiagnosticTag.Deprecated);
				addDiagnosticTags(tags);
			} 
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean isLocal() {
		if (myScope == null) {
			return false;
		}

		for (Scope scope = myScope; scope != null; scope = scope.getParentScope()) {
			if (scope instanceof StatementBlock) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Adds the definition to the list completion proposals, with some
	 * description of the definition.
	 * <p>
	 * Extending class only need to implement their
	 * {@link #getProposalKind()} function
	 *
	 * @param propCollector
	 *                the proposal collector.
	 * @param index
	 *                the index of a part of the full reference, for which
	 *                we wish to find completions.
	 * */

	/**
	 * @return template restriction for this definition
	 */
	public TemplateRestriction.Restriction_type getTemplateRestriction() {
		return TemplateRestriction.Restriction_type.TR_NONE;
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		final List<Integer> result = new ArrayList<Integer>();

		if (isLocal()) {
			return result;
		}

		if (withAttributesPath == null || withAttributesPath.getAttributes() == null) {
			result.add(Ttcn3Lexer.WITH);
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossiblePrefixTokens() {
		if (isLocal()) {
			return List.of(Ttcn3Lexer.CONST, Ttcn3Lexer.VAR);
		}
		return visibilityModifier == null ? List.of(Ttcn3Lexer.PUBLIC, Ttcn3Lexer.FRIEND, Ttcn3Lexer.PRIVATE)
				: IAppendableSyntax.super.getPossiblePrefixTokens();
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (withAttributesPath != null) {
			withAttributesPath.findReferences(referenceFinder, foundIdentifiers);
		}
		if (erroneousAttributes != null) {
			erroneousAttributes.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (identifier != null && !identifier.accept(v)) {
			return false;
		}
		if (withAttributesPath != null && !withAttributesPath.accept(v)) {
			return false;
		}
		if (erroneousAttributes != null && !erroneousAttributes.accept(v)) {
			return false;
		}
		return true;
	}

	private static ErroneousAttributeSpecification parseErrAttrSpecString(final AttributeSpecification aAttrSpec) {
		String code = aAttrSpec.getSpecification();
		if (code == null) {
			return null;
		}

		final Location location = aAttrSpec.getLocation();
		final File file = location.getFile();
		// code must be transformed, according to
		// compiler2/ttcn3/charstring_la.l
		code = Ttcn3CharstringLexer.parseCharstringValue(code, location); // TODO
		final Reader reader = new StringReader(code);
		final CharStream charStream = new UnbufferedCharStream(reader);
		final Ttcn3Lexer lexer = new Ttcn3Lexer(charStream);
		lexer.setTokenFactory( new CommonTokenFactory( true ) );
		// needs to be shifted by one because of the \" of the string
		lexer.setCharPositionInLine( 0 );
		lexer.setOffset(location);

		// lexer and parser listener
		final TitanErrorListener parserListener = new TitanErrorListener(file);
		// remove ConsoleErrorListener
		lexer.removeErrorListeners();
		lexer.addErrorListener(parserListener);

		// 1. Previously it was UnbufferedTokenStream(lexer), but it was changed to BufferedTokenStream, because UnbufferedTokenStream seems to be unusable. It is an ANTLR 4 bug.
		// Read this: https://groups.google.com/forum/#!topic/antlr-discussion/gsAu-6d3pKU
		// pr_PatternChunk[StringBuilder builder, boolean[] uni]:
		//   $builder.append($v.text); <-- exception is thrown here: java.lang.UnsupportedOperationException: interval 85..85 not in token buffer window: 86..341
		// 2. Changed from BufferedTokenStream to CommonTokenStream, otherwise tokens with "-> channel(HIDDEN)" are not filtered out in lexer.
		final CommonTokenStream tokenStream = new CommonTokenStream( lexer );

		final Ttcn3Reparser parser = new Ttcn3Reparser( tokenStream );
		ParserUtilities.setBuildParseTree( parser );
		parser.setActualFile(file);
		parser.setOffsetPosition( location.getStartPosition() );
		parser.setLine( location.getStartLine() );

		// remove ConsoleErrorListener
		parser.removeErrorListeners();
		parser.addErrorListener( parserListener );

		final Pr_ErroneousAttributeSpecContext root = parser.pr_ErroneousAttributeSpec();
		ParserUtilities.logParseTree( root, parser );

		// add markers
		for (SyntacticErrorStorage ses : parserListener.getErrorsStored()) {
			ses.reportSyntacticError();
		}

		return root.errAttrSpec;
	}

	public void setInherited(boolean isInherited) {
		this.isInherited = isInherited;
	}

	public boolean isInherited() {
		return isInherited;
	}

	/**
	 * This function removes the unnecessary parts of the full name of the Definition
	 * and adds the identifier to the end if it is not present
	 * @return full name of the Definition to display 
	 */
	public String getFullNameForDocComment() {
		final String fullName = getFullName();
		final int firstDot = fullName.indexOf(INamedNode.DOT);
		if (firstDot != fullName.lastIndexOf(INamedNode.DOT)) {
			final StringBuilder sb = new StringBuilder(fullName.substring(0, firstDot));
			sb.append(INamedNode.DOT).append(identifier.getDisplayName());
			return sb.toString();
		} else {
			return fullName;
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasDocumentComment() {
		return documentComment != null;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasOwnDocumentComment() {
		return documentComment != null && 
			documentComment.getOwner() != null &&
			documentComment.getOwner().equals(this);
	}

	@Override
	/** {@inheritDoc} */
	public DocumentComment getDocumentComment() {
		return documentComment;
	}

	@Override
	/** {@inheritDoc} */
	public void setDocumentComment(DocumentComment docComment) {
		if (documentComment != null) {
			return;
		}
		documentComment = docComment;

		if (this instanceof IParameterisedAssignment) {
			final FormalParameterList fpl = ((IParameterisedAssignment) this).getFormalParameterList();
			if (fpl != null) {
				for (final FormalParameter fp : fpl) {
					fp.setDocumentComment(docComment);
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		hoverContent = new Ttcn3HoverContent();
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		// Do nothing
		return null;
	}

	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = ICompletionItem.super.getCompletionItem();
		if (hasDocumentComment()) {
			item.setDocumentation(getHoverContent().getText(HoverContentType.INFO));
		}
		return item;
	}

	/**
	 * Clears all existing references pointing to this definition.
	 * It is needed to remove outdated instances of this definition so
	 * all references will be correctly updated and will refer to the
	 * new instance.
	 */
	public void clearExistingReferences() {
		final List<Reference> existingRefs = getReferences();
		for (final Reference reference : existingRefs) {
			reference.clear();
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticModifier(identifier.getLocation(), SemanticModifier.Definition);
		if (hasOwnDocumentComment() && getDocumentComment().isDeprecated()) {
			AstSemanticHighlighting.addSemanticModifier(identifier.getLocation(), SemanticModifier.Deprecated);
		}
	}
}
