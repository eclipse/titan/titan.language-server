/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.commandline;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Miklos Magyari
 */
public class CommandLineConfiguration {
	/** path of the configuration JSON file */
	public String cfgFile = "";
	/** root folder */
	public String rootFolder = "";
	/** output file **/
	public String outFile = "";
	/** files to be added to the project */
	public List<String> files = new ArrayList<>();
	public String ttcnFormat;
	
	/** print TTCN markers */
	public Boolean ttcnErrorMarkers = null;
	public Boolean ttcnWarningMarkers = null;
	
	/** print static analyzer markers */
	public Boolean titaniumErrorMarkers = null;
	public Boolean titaniumWarningMarkers = null;
	
	/** enable OOP extension */
	public Boolean oopEnabled = Boolean.FALSE;
	
	/** enable naming convention checks */
	public Boolean namingConventionsEnabled = Boolean.FALSE;
}
