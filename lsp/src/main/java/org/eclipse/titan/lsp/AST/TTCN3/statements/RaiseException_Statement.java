package org.eclipse.titan.lsp.AST.TTCN3.statements;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;

/**
 * Represents the raise exception statement (TTCN3 OOP extension)
 *  
 * @author Miklos Magyari
 * */
public class RaiseException_Statement extends Statement {
	private static final String RAISEINFINALLY = "Raise statement cannot be used in a finally block";
	private static final String STATEMENT_NAME = "raise exception";
	
	private final TemplateInstance exception;
	
	public RaiseException_Statement(TemplateInstance exception) {
		this.exception = exception;
		
	}

	@Override
	public Statement_type getType() {
		return Statement_type.S_RAISE;
	}
	
	@Override
	public String getStatementName() {
		return STATEMENT_NAME;
	}

	@Override	
	public void check(CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		
		if (myStatementBlock.hasDestructor()) {
			location.reportSemanticError(RAISEINFINALLY);
			return;
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean isTerminating(final CompilationTimeStamp timestamp) {
		return true;
	}
	
	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (exception != null) {
			exception.updateSyntax(reparser, isDamaged);
			reparser.updateLocation(exception.getLocation());
		}
	}

	@Override
	protected boolean memberAccept(ASTVisitor v) {
		// TODO Auto-generated method stub
		return false;
	}

}
