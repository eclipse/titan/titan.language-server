///******************************************************************************
// * Copyright (c) 2000-2025 Ericsson Telecom AB
// * All rights reserved. This program and the accompanying materials
// * are made available under the terms of the Eclipse Public License v2.0
// * which accompanies this distribution, and is available at
// * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
// ******************************************************************************/
package org.eclipse.titan.lsp.parsers.ttcn3parser;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.parsers.SyntacticErrorStorage;
import org.eclipse.titan.lsp.parsers.TITANMarker;
import org.eclipse.titan.lsp.parsers.TitanErrorListener;
import org.eclipse.titan.lsp.parsers.preprocess.PreprocessorDirective;

/**
 * Directive types for state machine transitions
 *
 */
enum ConditionalTransition {
	ELIF, ELSE, ENDIF;
}

/**
 * Conditionals state machine
 */
enum ConditionalState {
	BEGIN("#if") {
		@Override
		ConditionalState transition(final ConditionalTransition transition) {
			switch (transition) {
			case ELIF:
				return ELIF;
			case ELSE:
				return ELSE;
			case ENDIF:
				return END;
			default:
				return null;
			}
		}
	},
	ELIF("#elif") {
		@Override
		ConditionalState transition(final ConditionalTransition transition) {
			switch (transition) {
			case ELIF:
				return ELIF;
			case ELSE:
				return ELSE;
			case ENDIF:
				return END;
			default:
				return null;
			}
		}
	},
	ELSE("#else") {
		@Override
		ConditionalState transition(final ConditionalTransition transition) {
			switch (transition) {
			case ENDIF:
				return END;
			default:
				return null;
			}
		}
	},
	END("#endif") {
		@Override
		ConditionalState transition(final ConditionalTransition transition) {
			return null;
		}
	};
	String name;

	ConditionalState(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * Given the actual state and the incoming directive return the
	 * resulting state
	 *
	 * @param transition
	 *                the incoming directive
	 * @return resulting state or null if invalid state transition
	 */
	abstract ConditionalState transition(final ConditionalTransition transition);
}

class ConditionalStateMachine {
	PreprocessorDirective beginDirective;
	ConditionalState state;
	// true if act_cond was ever true previously
	boolean prevCond;
	// true if actual condition is true
	boolean actCond;

	/**
	 * Creates a new state for for the #IF stuff, the evaluated conditional
	 * is true or false
	 *
	 * @param if_condition
	 *                the evaluated condition of the initial #IF part of the
	 *                construct
	 */
	public ConditionalStateMachine(final PreprocessorDirective beginDirective) {
		this.beginDirective = beginDirective;
		state = ConditionalState.BEGIN;
		prevCond = false;
		actCond = beginDirective.type == PreprocessorDirective.Directive_type.IFNDEF ? !beginDirective.condition : beginDirective.condition;
	}

	public void transition(final PreprocessorDirective ppDirective, final List<TITANMarker> errors) {
		ConditionalTransition transition;
		boolean newCond;
		switch (ppDirective.type) {
		case ELIF:
			transition = ConditionalTransition.ELIF;
			newCond = ppDirective.condition;
			break;
		case ELSE:
			transition = ConditionalTransition.ELSE;
			newCond = true;
			break;
		case ENDIF:
			transition = ConditionalTransition.ENDIF;
			newCond = true;
			break;
		default:
			TitanLogger.logFatal();
			return;
		}

		final ConditionalState newState = state.transition(transition);
		if (newState == null) {
			// invalid transition was requested
			final TITANMarker marker = new TITANMarker(MessageFormat.format("Directive {0} after {1} is not a valid preprocessor conditional",
					ppDirective.type.getName(), state.getName()), ppDirective.line, -1, -1, 
					TITANMarker.SEVERITY_ERROR, TITANMarker.PRIORITY_NORMAL);
			errors.add(marker);
			return;
		}
		// execute transition
		state = newState;
		if (actCond) {
			prevCond = true;
		}
		actCond = newCond;
	}

	/**
	 * Returns true if this is not a filtering state, this is a passing
	 * state if no previous states were passing states.
	 *
	 * @return true if tokens can be passed on
	 */
	public boolean isPassing() {
		return actCond && !prevCond;
	}

	public boolean hasEnded() {
		return state == ConditionalState.END;
	}
}

class ConditionalStateStack {
	Stack<ConditionalStateMachine> stateStack = new Stack<ConditionalStateMachine>();
	List<TITANMarker> unsupportedConstructs;

	public ConditionalStateStack(final List<TITANMarker> unsupportedConstructs) {
		this.unsupportedConstructs = unsupportedConstructs;
	}

	/**
	 * Changes the state of the stack according to the directive. Creates
	 * error/warning markers if the directive is invalid in this state.
	 *
	 * @param ppDirective
	 *                the directive parsed
	 */
	public void processDirective(final PreprocessorDirective ppDirective) {
		switch (ppDirective.type) {
		case IF:
		case IFDEF:
		case IFNDEF: {
			final ConditionalStateMachine csm = new ConditionalStateMachine(ppDirective);
			stateStack.add(csm);
		}
			break;
		case ELIF:
		case ELSE:
		case ENDIF: {
			if (stateStack.isEmpty()) {
				final TITANMarker marker = new TITANMarker(MessageFormat.format(
						"Directive {0} without corresponding #if/#ifdef/#ifndef directive", ppDirective.type.getName()),
						ppDirective.line, -1, -1, TITANMarker.SEVERITY_ERROR, TITANMarker.PRIORITY_NORMAL);
				unsupportedConstructs.add(marker);
			} else {
				final ConditionalStateMachine topState = stateStack.peek();
				topState.transition(ppDirective, unsupportedConstructs);
				if (topState.hasEnded()) {
					stateStack.pop();
				}
			}
		}
			break;
		default:
			TitanLogger.logFatal();
		}
	}

	/**
	 * Check if the tokens can be passed on or must be filtered out from the
	 * stream
	 *
	 * @return true to not filter
	 */
	public boolean isPassing() {
		for (final ConditionalStateMachine csm : stateStack) {
			if (!csm.isPassing()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if at the EOF the state stack is empty, otherwise creates
	 * error marker(s).
	 */
	public void eofCheck() {
		for (final ConditionalStateMachine csm : stateStack) {
			final TITANMarker marker = new TITANMarker(MessageFormat.format("{0} directive was not terminated",
					csm.beginDirective.type.getName()), csm.beginDirective.line, -1, -1, TITANMarker.SEVERITY_ERROR,
					TITANMarker.PRIORITY_NORMAL);
			unsupportedConstructs.add(marker);
		}
	}
}

/**
 * Helper class to store data related to lexers in lexer stack
 */
class TokenStreamData extends CommonTokenStream {
	public File file;
	public Ttcn3Lexer lexer;
	public Reader reader;

	public TokenStreamData(final Ttcn3Lexer source, final File file, final Reader reader) {
		super (source);
		this.file = file;
		this.lexer = source;
		this.reader = reader;
	}
}

public class PreprocessedTokenStream extends CommonTokenStream {
	private static final int RECURSION_LIMIT = 20;

	Path actualFile;
	Ttcn3Lexer actualLexer;
	Ttcn3Parser parser;
	ConditionalStateStack condStateStack;
	// global, non-recursive macros (symbols)
	Map<String, String> macros = new Hashtable<String, String>();
	// #include files
	Stack<TokenStreamData> tokenStreamStack = new Stack<TokenStreamData>();

	Set<Path> includedFiles = new HashSet<Path>();
	List<Location> inactiveCodeLocations = new ArrayList<Location>();

	Position invalidStart = null;
	Position invalidEnd = null;

	Location lastPPDirectiveLocation = null;

	private List<SyntacticErrorStorage> errorsStored = new ArrayList<SyntacticErrorStorage>();
	private List<TITANMarker> warnings = new ArrayList<TITANMarker>();
	private List<TITANMarker> unsupportedConstructs = new ArrayList<TITANMarker>();

	public Set<Path> getIncludedFiles() {
		return includedFiles;
	}

	public List<Location> getInactiveCodeLocations() {
		return inactiveCodeLocations;
	}

	public List<SyntacticErrorStorage> getErrorStorage() {
		return errorsStored;
	}

	public void reportWarning(final TITANMarker marker) {
		warnings.add(marker);
	}

	public List<TITANMarker> getWarnings() {
		return warnings;
	}

	public void reportUnsupportedConstruct(final TITANMarker marker) {
		unsupportedConstructs.add(marker);
	}

	public List<TITANMarker> getUnsupportedConstructs() {
		return unsupportedConstructs;
	}

	public PreprocessedTokenStream(final TokenSource tokenSource) {
		super(tokenSource);
		condStateStack = new ConditionalStateStack(unsupportedConstructs);
	}

	public void setActualFile(final Path file) {
		actualFile = file;
	}

	public void setActualLexer(final Ttcn3Lexer lexer) {
		actualLexer = lexer;
	}

	public void setParser(final Ttcn3Parser parser) {
		this.parser = parser;
	}

	public void setMacros(final String[] definedList) {
		for (final String s : definedList) {
			macros.put(s, "");
		}
	}

	/**
	 * Adds a new lexer to the lexer stack to read tokens from the included
	 * file
	 *
	 * @param fileName
	 *                the file name parameter of the #include directive
	 */
	private void processIncludeDirective(final PreprocessorDirective ppDirective) {
		if (ppDirective.str == null || "".equals(ppDirective.str)) {
			final TITANMarker marker = new TITANMarker("File name was not provided", ppDirective.line, -1, -1, 
					TITANMarker.SEVERITY_ERROR, TITANMarker.PRIORITY_NORMAL);
			unsupportedConstructs.add(marker);
			return;
		}

		final Path includedFile = GlobalParser.getProjectSourceParser(Project.INSTANCE).getTTCN3IncludeFileByName(ppDirective.str);
		if (includedFile == null) {
			final TITANMarker marker = new TITANMarker(MessageFormat.format("Included file `{0}'' could not be found", ppDirective.str),
					ppDirective.line, -1, -1, TITANMarker.SEVERITY_ERROR, TITANMarker.PRIORITY_NORMAL);
			unsupportedConstructs.add(marker);
			return;
		}
		// check extension
		final String filename = includedFile.toString();
		String extension = "";
		if (filename.contains(".")) {
		    extension = filename.substring(filename.lastIndexOf(".") + 1);
		}
		if (!GlobalParser.isSupportedTTCNINExtension(extension)) {
			final TITANMarker marker = new TITANMarker(MessageFormat.format("File `{0}'' does not have the `{1}'' extension", ppDirective.str,
					GlobalParser.TTCNIN_EXTENSION), ppDirective.line, -1, -1, TITANMarker.SEVERITY_WARNING, TITANMarker.PRIORITY_NORMAL);
			warnings.add(marker);
		}
		// check if the file is already loaded into an editor
		final ProjectItem item = Project.INSTANCE.getProjectItem(includedFile);
		String code = item != null ? item.getSource() : null;

		// create lexer and set it up
		Reader reader = null;
		CharStream charStream = null;
		Ttcn3Lexer lexer = null;
		int rootInt;
		if (code != null) {
			reader = new StringReader(code);
			charStream = new UnbufferedCharStream(reader);
			lexer = new Ttcn3Lexer(charStream);
			lexer.setTokenFactory( new CommonTokenFactory( true ) );
			rootInt = code.length();
		} else {
			return;
		}
		lexer.setTokenFactory(new CommonTokenFactory(true));
		lexer.setTTCNPP();
		lexer.initRootInterval(rootInt);
		lexer.setActualFile(includedFile.toFile());
		// add the lexer to the stack of lexers
		tokenStreamStack.push(new TokenStreamData(lexer, includedFile.toFile(), reader));
		if (parser != null) {
			parser.setActualFile(includedFile.toFile());
		}
		includedFiles.add(includedFile);
	}

	@Override
	public int fetch(int n){
		if (fetchedEOF) {
			return 0;
		}
		int i = 0;
		do {
			Token t;
			if (tokenStreamStack.isEmpty()) {
				t = getTokenSource().nextToken();
			} else {
				t = tokenStreamStack.peek().getTokenSource().nextToken();
			}
			if (t == null) {
				return 0;
			}
			final int tokenType = t.getType();
			if (tokenType == Ttcn3Lexer.PREPROCESSOR_DIRECTIVE) {
				lastPPDirectiveLocation = new Location(
					actualFile.toFile(), t.getLine() - 1, t.getCharPositionInLine(), t.getLine() - 1, t.getChannel() + t.getText().length());
				// 1. the first # shall be discarded
				// 2. "\\\n" strings are removed, so multiline tokens, which are split by backslash are extracted to one line
				final String text = t.getText().substring(1).replace("\\\n", "");
				final Reader reader = new StringReader( text );
				final CharStream charStream = new UnbufferedCharStream(reader);
				final PreprocessorDirectiveLexer lexer = new PreprocessorDirectiveLexer(charStream);
				lexer.setTokenFactory(new PPDirectiveTokenFactory(true, t));
				final TitanErrorListener lexerListener = new TitanErrorListener(actualFile.toFile());
				lexer.removeErrorListeners();
				lexer.addErrorListener(lexerListener);
				lexer.setLine(t.getLine());
				lexer.setCharPositionInLine(t.getCharPositionInLine());

				// 1. Previously it was UnbufferedTokenStream(lexer), but it was changed to BufferedTokenStream, because UnbufferedTokenStream seems to be unusable. It is an ANTLR 4 bug.
				// Read this: https://groups.google.com/forum/#!topic/antlr-discussion/gsAu-6d3pKU
				// pr_PatternChunk[StringBuilder builder, boolean[] uni]:
				//   $builder.append($v.text); <-- exception is thrown here: java.lang.UnsupportedOperationException: interval 85..85 not in token buffer window: 86..341
				// 2. Changed from BufferedTokenStream to CommonTokenStream, otherwise tokens with "-> channel(HIDDEN)" are not filtered out in lexer.
				final CommonTokenStream tokenStream = new CommonTokenStream( lexer );

				final PreprocessorDirectiveParser localParser = new PreprocessorDirectiveParser( tokenStream );
				localParser.setBuildParseTree(false);
				final TitanErrorListener parserListener = new TitanErrorListener(actualFile.toFile(), localParser.getErrorStorage());
				localParser.removeErrorListeners();
				localParser.addErrorListener(parserListener);
				localParser.setIsActiveCode(condStateStack.isPassing());
				localParser.setMacros(macros);
				localParser.setLine(t.getLine());
				PreprocessorDirective ppDirective = null;
				ppDirective = localParser.pr_Directive().ppDirective;
				errorsStored.addAll(localParser.getErrorStorage());
				warnings.addAll(localParser.getWarnings());
				unsupportedConstructs.addAll(localParser.getUnsupportedConstructs());
				if (ppDirective != null) {
					ppDirective.line = t.getLine();
					if (ppDirective.isConditional()) {
						final boolean preIsPassing = condStateStack.isPassing();
						condStateStack.processDirective(ppDirective);
						final boolean postIsPassing = condStateStack.isPassing();
						if (preIsPassing != postIsPassing && tokenStreamStack.isEmpty() && getTokenSource() instanceof Ttcn3Lexer) {
							// included files are ignored because of ambiguity
							final Location ppLocation = lastPPDirectiveLocation;
							if (ppLocation != null) {
								if (preIsPassing) {
									// switched to inactive: begin a new inactive location
									invalidStart = new Position(ppLocation.getEndLine() + 1, 0);
								} else {
									/**
									 *  switched to active: end the current inactive location
									 *  
									 *  We need to create a location for every line. Multiline tokens does not seem to work.
									 */
									if (invalidStart != null) {
										ProjectItem item = Project.INSTANCE.getProjectItem(actualFile);
										if (item != null) {
											int actualLine = invalidStart.getLine();
											List<String> lines = item.getSourceLines(actualLine, ppLocation.getStartLine() - 1);
											for (String line : lines) {
												final Location inactiveLocation =
													new Location(actualFile.toFile(), actualLine, 0, actualLine, line.length());
												inactiveCodeLocations.add(inactiveLocation);
												actualLine++;
											}
											invalidStart = null;
										}
									}
								}
							}
						}
					} else {
						// other directive types
						if (condStateStack.isPassing()) {
							// do something with the
							// directive
							switch (ppDirective.type) {
							case INCLUDE: {
								if (tokenStreamStack.size() > RECURSION_LIMIT) {
									// dumb but safe defense against infinite recursion, default value from gcc
									final TITANMarker marker = new TITANMarker(
											"Maximum #include recursion depth reached", ppDirective.line,
											-1, -1, TITANMarker.SEVERITY_ERROR, TITANMarker.PRIORITY_NORMAL);
									unsupportedConstructs.add(marker);
								} else {
									processIncludeDirective(ppDirective);
								}
							}
								break;
							case ERROR: {
								final String errorMessage = ppDirective.str == null ? "" : ppDirective.str;
								final TITANMarker marker = new TITANMarker(errorMessage, ppDirective.line, -1, -1,
										TITANMarker.SEVERITY_ERROR, TITANMarker.PRIORITY_NORMAL);
								unsupportedConstructs.add(marker);
							}
								break;
							case WARNING: {
								final String warningMessage = ppDirective.str == null ? "" : ppDirective.str;
								final TITANMarker marker = new TITANMarker(warningMessage, ppDirective.line, -1, -1,
										TITANMarker.SEVERITY_WARNING, TITANMarker.PRIORITY_NORMAL);
								warnings.add(marker);
							}
								break;
							case LINECONTROL:
							case LINEMARKER:
							case PRAGMA:
							case NULL: {
								final String reportPreference = Configuration.INSTANCE.getString(Configuration.REPORT_IGNORED_PREPROCESSOR_DIRECTIVES,
										GeneralConstants.WARNING);
								if (!GeneralConstants.IGNORE.equals(reportPreference)) {
									final boolean isError = GeneralConstants.ERROR.equals(reportPreference);
									final TITANMarker marker = new TITANMarker(MessageFormat.format(
											"Preprocessor directive {0} is ignored",
											ppDirective.type.getName()), ppDirective.line, -1, -1,
											isError ? TITANMarker.SEVERITY_ERROR : TITANMarker.SEVERITY_WARNING,
													TITANMarker.PRIORITY_NORMAL);
									if (isError) {
										unsupportedConstructs.add(marker);
									} else {
										warnings.add(marker);
									}
								}
							}
								break;
							default:
								// ignore
							}
						}
					}
				}
			} else if (tokenType == Token.EOF) {
				if (!tokenStreamStack.isEmpty()) {
					// the included file ended, drop lexer
					// from the stack and ignore EOF token
					final TokenStreamData tsd = tokenStreamStack.pop();
					if (parser != null) {
						if (tokenStreamStack.isEmpty()) {
							parser.setActualFile(actualFile.toFile());
						} else {
							parser.setActualFile(tokenStreamStack.peek().file);
						}
					}
					if (tsd.reader != null) {
						try {
							tsd.reader.close();
						} catch (IOException e) {
						}
					}
				} else {
					fetchedEOF = true;
					condStateStack.eofCheck();
					tokens.add(t);
					((CommonToken) t).setTokenIndex(tokens.size()-1);
					--n;
					++i;
					if (n == 0) {
						return i;
					}
				}
			} else {
				if (condStateStack.isPassing()) {
					tokens.add(t);
					((CommonToken) t).setTokenIndex(tokens.size()-1);
					--n;
					++i;
					if (n == 0) {
						return i;
					}
				}
			}
		} while (true);
	}
}
