/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.Arrays;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Const;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_ExternalConst;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Extfunction;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_ModulePar;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Template;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

//TODO usage calculation should be moved here completely
public class NonprivatePrivate extends BaseModuleCodeSmellSpotter {
	private static final String SHOULD_BE_PRIVATE = "{0} is referenced only locally, it should be private";

	public NonprivatePrivate() {
		super(CodeSmellType.NONPRIVATE_PRIVATE);
		addStartNodes(Arrays.asList(Def_Altstep.class, Def_Const.class, Def_ExternalConst.class, Def_Extfunction.class,
			Def_Function.class, Def_ModulePar.class, Def_Template.class, Def_Type.class));
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (!(node instanceof Definition)) {
			return;
		}

		final Definition d = (Definition) node;
		if (d.referingHere != null && d.referingHere.size() == 1 &&
				!VisibilityModifier.Private.equals(d.getVisibilityModifier()) && !d.isLocal()) {
			final String moduleName = d.getMyScope().getModuleScope().getName();
			if (d.referingHere.get(0).equals(moduleName)) {
				final String msg = MessageFormat.format(SHOULD_BE_PRIVATE, d.getIdentifier().getDisplayName());
				problems.report(d.getIdentifier().getLocation(), msg);
			}
		}
	}
}
