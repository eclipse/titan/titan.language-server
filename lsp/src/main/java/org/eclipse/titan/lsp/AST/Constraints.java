/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.Constraint.Constraint_type;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.ASN1.TableConstraint;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents constraints.
 *
 * @author Kristof Szabados
 * */
public final class Constraints extends ASTNode {
	private List<Constraint> constraintsList;

	/** @return a new instance of this constraint list */
	public Constraints newInstance() {
		final Constraints temp = new Constraints();

		if (null != constraintsList) {
			for (final Constraint constraint : constraintsList) {
				final Constraint newConstraint = constraint.newInstance();
				temp.constraintsList.add(newConstraint);

				newConstraint.setFullNameParent(this);
			}
		}

		return temp;
	}

	public void addConstraint(final Constraint constraint) {
		if (null == constraint) {
			return;
		}

		if (null == constraintsList) {
			constraintsList = new ArrayList<Constraint>(1);
		}

		constraintsList.add(constraint);

		constraint.setFullNameParent(this);
	}

	public int getNofConstraints() {
		if (null == constraintsList) {
			return 0;
		}

		return constraintsList.size();
	}

	public Constraint getConstraintByIndex(final int index) {
		if (null == constraintsList) {
			return null;
		}

		return constraintsList.get(index);
	}

	/**
	 * @return the first table constraint in the list, or null if none was found.
	 * */
	public TableConstraint getTableConstraint() {
		if (null == constraintsList) {
			return null;
		}

		for (final Constraint tempConstraint : constraintsList) {
			if (Constraint_type.CT_TABLE.equals(tempConstraint.getConstraintType())) {
				return (TableConstraint) tempConstraint;
			}
		}
		return null;
	}

	public void setMyType(final IType type) {
		if (null == constraintsList) {
			return;
		}

		for (final Constraint tempConstraint : constraintsList) {
			tempConstraint.setMyType(type);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = constraintsList.size(); i < size; i++) {
			if (constraintsList.get(i) == child) {
				return builder.append(INamedNode.SQUAREOPEN).append(String.valueOf(i + 1)).append(INamedNode.SQUARECLOSE);
			}
		}

		return builder;
	}

	/**
	 * Does the semantic checking of the constraint.
	 *
	 * @param timestamp the time stamp of the actual semantic check cycle.
	 * */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (null == constraintsList) {
			return;
		}

		for (final Constraint tempConstraint : constraintsList) {
			tempConstraint.check(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (constraintsList == null) {
			return;
		}

		for (final Constraint c : constraintsList) {
			c.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (constraintsList != null) {
			for (final Constraint c : constraintsList) {
				if (!c.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}
}
