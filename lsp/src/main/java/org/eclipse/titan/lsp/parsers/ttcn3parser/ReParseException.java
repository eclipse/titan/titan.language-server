/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers.ttcn3parser;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public class ReParseException extends Exception {
	
	/** 
	 * All serializable objects should have a stable serialVersionUID
	 */
	private static final long serialVersionUID = 7721903800861921833L;

	/** 
	 * Depth in the AST, where the exception was thrown
	 */
	private final int depth;

	/**
	 * Default constructor, initializes the depth to {@code 1}.
	 */
	public ReParseException() {
		depth = 1;
	}

	/**
	 * Initializes the depth to the specified value.
	 * @param depth depth in the AST, where the exception was thrown
	 */
	public ReParseException(final int depth) {
		this.depth = depth;
	}

	/**
	 * Initializes the depth based on the specified ReParseException's depth.
	 * The depth is <em>decreased by one</em> during the initialization.
	 * Use when re-throw of the exception is needed.
	 * @param e ReParseException of which to use its depth
	 */
	public ReParseException(final ReParseException e) {
		this.depth = e.getDepth() - 1;
	}

	/**
	 * Returns the depth in the AST, where the exception was thrown
	 * @return the depth in the AST, where the exception was thrown
	 */
	public int getDepth() {
		return depth;
	}

}
