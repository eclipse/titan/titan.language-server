package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;

import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Connect_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Port_Utility;
import org.eclipse.titan.lsp.AST.TTCN3.types.PortTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.PortTypeBody.OperationModes;
import org.eclipse.titan.lsp.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

/**
 * 
 * @author jasla
 * This is class is used to detect whether connection is unable or not
 * when connect statement is used.
 */

public class ConnectionDetector extends BaseModuleCodeSmellSpotter {
	private static final String ERROR_MESSAGE = "Neither port type `{0}'' nor port type `{1}'' can send messages";

	public ConnectionDetector() {
		super(CodeSmellType.CONNECTION_DETECTOR);
		addStartNode(Connect_Statement.class);
	}

	@Override
	public void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof Connect_Statement) {
			final Connect_Statement s = (Connect_Statement) node;

			final IType portType1 = Port_Utility.checkConnectionEndpoint(CompilationTimeStamp.getBaseTimestamp(), s, s.getComponentReference1(), s.getPortReference1(), false);
			final IType portType2 = Port_Utility.checkConnectionEndpoint(CompilationTimeStamp.getBaseTimestamp(), s, s.getComponentReference2(), s.getPortReference2(), false);
			if (portType1 == null || portType2 == null) {
				return;
			}

			final PortTypeBody body1 = ((Port_Type) portType1).getPortBody();
			final PortTypeBody body2 = ((Port_Type) portType2).getPortBody();

			if ((OperationModes.OP_Message.equals(body1.getOperationMode()) || OperationModes.OP_Mixed.equals(body1.getOperationMode()))
					&& (OperationModes.OP_Message.equals(body2.getOperationMode()) || OperationModes.OP_Mixed.equals(body2.getOperationMode()))
					&& body1.getOutMessage() == null && body2.getOutMessage() == null) {
				problems.report(s.getLocation(), MessageFormat.format(ERROR_MESSAGE, portType1.getTypename(), portType2.getTypename()));
			}
		}
	}
}