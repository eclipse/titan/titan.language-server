/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.InitializedParams;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.eclipse.lsp4j.ProgressParams;
import org.eclipse.lsp4j.ServerInfo;
import org.eclipse.lsp4j.WorkDoneProgressBegin;
import org.eclipse.lsp4j.WorkDoneProgressCreateParams;
import org.eclipse.lsp4j.WorkDoneProgressEnd;
import org.eclipse.lsp4j.WorkDoneProgressReport;
import org.eclipse.lsp4j.WorkspaceFolder;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.jsonrpc.services.JsonDelegate;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageClientAware;
import org.eclipse.lsp4j.services.LanguageServer;
import org.eclipse.lsp4j.services.NotebookDocumentService;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;
import org.eclipse.titan.lsp.parsers.GlobalParser;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.product.FolderConfiguration;
import org.eclipse.titan.lsp.common.product.IPreferenceChangedListener;
import org.eclipse.titan.lsp.common.product.ProductIdentity;
import org.eclipse.titan.lsp.common.usagestats.UsageStatSender;
import org.eclipse.titan.lsp.core.Project;

public final class TitanLanguageServer implements LanguageServer, LanguageClientAware {
	public enum ServerMode {
		LanguageServer,
		CommandLineAnalyzer
	}

	/** Timeout in seconds until we wait for the initial configuration from the client. */
	private static final int WAIT_CONFIG_TIMEOUT = 5;

	private static LanguageClient client = null;
	private static FolderConfiguration folderConfiguration = new FolderConfiguration();
	private final TextDocumentService textDocumentService;
	private final NotebookDocumentService notebookDocumentService;
	private final WorkspaceService workspaceService;
	private final TitanLspExtensionsService titanService;

	/** 
	 * Indicates if a project analyzer job is finished. Only used to wait for the 
	 * first analysis to complete.<br>
	 * It is needed when a saved session is restored in vscode with open ttcn/asn1
	 * files. For these files, vscode can send text/documentSymbol request
	 * before code analysis is finished so the response will be empty, resulting
	 * in a blank outline.
	 */
	public static final CountDownLatch FIRST_ANALYSIS_LATCH = new CountDownLatch(1);

	/**
	 * Boolean used for requesting build cancellation. When set to true, most of parsing and semantic checks
	 * will return quickly.
	 * This seems to be a good way for canceling the build processes without the need of thread manipulations. 
	 */
	private static final AtomicBoolean CANCEL_BUILD = new AtomicBoolean(false);

	/**
	 * A temporary timestamp for measuring cancellation time. 
	 */
	private static long cancelTimestamp;

	/**
	 * A lock used to prevent parallel execution of project analyzer jobs
	 * and modification of project items during builds.
	 */
	public static final Lock BUILD_LOCK = new ReentrantLock();

	private static ServerMode mode;
	private List<String> rebuildTriggeringPrefs;

	/** 
	 * Common listener for preferences that require a full rebuild when changed.
	 * 
	 * Examples are enabling/disabling OOP and Realtime extensions or Titanium. 
	 **/
	public class RebuildListener implements IPreferenceChangedListener {
		@Override
		public void changed(String preferenceName) {
			Project.INSTANCE.outdateAllItems();
			analyzeProject(Project.INSTANCE);
		}
	}

	public TitanLanguageServer() {
		this(ServerMode.LanguageServer);
	}

	public TitanLanguageServer(final ServerMode mode) {
		TitanLanguageServer.setMode(mode);
		switch (mode) {
		case CommandLineAnalyzer:
			this.textDocumentService = null;
			this.notebookDocumentService = null;
			this.titanService = null;
			this.workspaceService = null;
			break;
		case LanguageServer:
		default:
			this.textDocumentService = new TitanTextDocumentService(this);
			this.notebookDocumentService = new TitanNotebookDocumentService(this);
			this.workspaceService = new TitanWorkspaceService(this);
			this.titanService = new TitanLspExtensionsService(this);
			break;
		}

		rebuildTriggeringPrefs = Arrays.asList(
			Configuration.ENABLE_TITANIUM,
			Configuration.ENABLE_OOP_EXTENSION,
			Configuration.ENABLE_REALTIME_EXTENSION
		);
	}

	@Override
	public CompletableFuture<InitializeResult> initialize(InitializeParams params) {
		final InitializeResult initializeResult = new InitializeResult(
				ServerCapabilityHelper.getServerCapabilites(), getServerInfo());

		final List<WorkspaceFolder> wsFolders = params.getWorkspaceFolders();
        /* workspace folder list can be empty */
		if (wsFolders != null) {
			final Project project = Project.INSTANCE;
			project.addWorkspaceFolders(wsFolders);
		}

        ClientCapabilityHelper.getClientCapabilities(params);

        return CompletableFuture.supplyAsync(() -> initializeResult);
	}

	@Override
	public void initialized(InitializedParams params) {
		CompletableFuture.runAsync(() ->
			UsageStatSender.send()
		);

		/*CompletableFuture.runAsync(() -> {
			CompilerVersionInformationCollector.refreshTITANInfo();
		});*/

		CompletableFuture.runAsync(() -> {
			try {
				Configuration.INITIALIZED_LATCH.await(WAIT_CONFIG_TIMEOUT, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} finally {
//				final boolean excludeByTpd = Configuration.INSTANCE.getBoolean(Configuration.EXCLUDE_BY_TPD, false);
				final boolean analyzeTpdOnly = Configuration.INSTANCE.getBoolean(Configuration.ANALYZE_ONLY_WHEN_TPD_IS_ACTIVE, false);
				if (!analyzeTpdOnly) {
					analyzeProject(Project.INSTANCE);
				}

				waitFirstAnalyzer();
				Configuration.INSTANCE.addPreferenceChangedListeners(rebuildTriggeringPrefs, new RebuildListener());
			}
		});
	}

	@Override
	public CompletableFuture<Object> shutdown() {
		return CompletableFuture.completedFuture(null);
	}

	@Override
	public void exit() {
		System.exit(0);
	}

	@Override
	public NotebookDocumentService getNotebookDocumentService() {
		return this.notebookDocumentService;
	}

	@Override
	public TextDocumentService getTextDocumentService() {
		return this.textDocumentService;
	}

	@Override
	public WorkspaceService getWorkspaceService() {
		return this.workspaceService;
	}

	@JsonDelegate
	public LspExtensionsService getTitanService() {
		return this.titanService;
	}

	@Override
	public void connect(LanguageClient client) {
		TitanLanguageServer.setClient(client);
	}

	private static void setClient(LanguageClient client) {
		TitanLanguageServer.client = client;	
	}

	public static LanguageClient getClient() {
		return client;
	}

	public static void analyzeProject(final Project project) {
		final String activeTpd = Configuration.INSTANCE.getString(Configuration.ACTIVATED_TPD, "");
		analyzeProject(project, !activeTpd.isEmpty(), activeTpd);
	}

	public static void analyzeProject(final Project project, final boolean isTpdActivated, final String activeTpd) {
		if (BUILD_LOCK.tryLock()) {
			BUILD_LOCK.unlock();
		} else {
			requestBuildCancellation();
		}
		BUILD_LOCK.lock();
		try {
			final boolean analyzeTpdOnly = Configuration.INSTANCE.getBoolean(Configuration.ANALYZE_ONLY_WHEN_TPD_IS_ACTIVE, false);

			if (!analyzeTpdOnly || (activeTpd != null && activeTpd.length() != 0)) {
				final boolean testMode = Configuration.INSTANCE.getBoolean(Configuration.TEST_MODE, false);
				if (!testMode) {
					GlobalParser.getProjectSourceParser(project).analyzeAll();
				}
			}
		} catch (Exception e) {
			TitanLogger.logError(e);
		} finally {
			FIRST_ANALYSIS_LATCH.countDown();
			if (isBuildCancelled()) {
				final long timestamp = System.nanoTime();
				final StringBuilder sb = new StringBuilder();
				sb.append(">>>>>>>>> Build cancelled in ").append((timestamp - cancelTimestamp) * (1e-9)).append(" seconds <<<<<<<<<<");
				TitanLogger.logDebug(sb.toString());
			}
			resetBuildStatus();
			BUILD_LOCK.unlock();
		}
	}

	public static CompletableFuture<Void> analyzeProjectUnconditionally(final Project project) {
		return CompletableFuture.runAsync(() -> {
			if (BUILD_LOCK.tryLock()) {
				BUILD_LOCK.unlock();
			} else {
				requestBuildCancellation();
			}
			BUILD_LOCK.lock();
			try {
				GlobalParser.getProjectSourceParser(project).analyzeAll();
			} finally {
				FIRST_ANALYSIS_LATCH.countDown();
				BUILD_LOCK.unlock();
				resetBuildStatus();
			}
		});
	}

	/**
	 * Asks the client to display a particular message in the user interface.
	 * In case of Visual Studio Code it is shown as a notification in the bottom right corner.
	 * 
	 * @param type Type of the message (eg. error, warning, info, log)
	 * @param message The message to be displayed
	 */
	public static void showMessage(final MessageType type, final String message) {
		if (type == null || message == null || getClient() == null) {
			return;
		}

		final MessageParams params = new MessageParams(type, message);
		getClient().showMessage(params);
	}

	/**
	 * Asks the client to log a particular message.
	 * 
	 * @param type Type of the message (eg. error, warning, info, log)
	 * @param message The message to be displayed
	 */
	public static void logMessage(final MessageType type, final String message) {
		if (type == null || message == null || getClient() == null) {
			return;
		}

		final MessageParams params = new MessageParams(type, message);
		getClient().logMessage(params);
	}

	public static String createProgress(final String message, final long timeout) {
		if (mode == ServerMode.LanguageServer && ClientCapabilityHelper.isProgressSupported()) {
			final String token = UUID.randomUUID().toString();
			final WorkDoneProgressBegin progressBegin = new WorkDoneProgressBegin();
			progressBegin.setMessage(message);
			progressBegin.setCancellable(true);
			final WorkDoneProgressCreateParams createParams = new WorkDoneProgressCreateParams(Either.forLeft(token));
			try {
//				getClient().createProgress(createParams).get();
				if (timeout == 0L) {
					getClient().createProgress(createParams).get();
				} else {
					getClient().createProgress(createParams).completeOnTimeout(null, timeout, TimeUnit.SECONDS).get();
				}
			} catch (InterruptedException e) {
				TitanLogger.logError(e);
				Thread.currentThread().interrupt();
			} catch (Exception e) {
				TitanLogger.logError(e);
			}

			return token;
		}
		return null;
	}

	public static void beginProgress(final String token, final String title, final int percentage, final String message) {
		if (mode == ServerMode.LanguageServer && ClientCapabilityHelper.isProgressSupported()) {
			final WorkDoneProgressBegin begin = new WorkDoneProgressBegin();
			begin.setPercentage(percentage);
			begin.setMessage(message);
			begin.setTitle(title);
			begin.setCancellable(true);
			getClient().notifyProgress(new ProgressParams(Either.forLeft(token), Either.forLeft(begin)));
		}
	}

	public static void reportProgress(final String token, final int percentage, final String message) {
		if (mode == ServerMode.LanguageServer && ClientCapabilityHelper.isProgressSupported()) {
			final WorkDoneProgressReport progressReport = new WorkDoneProgressReport();
			progressReport.setPercentage(percentage);
			progressReport.setMessage(message);
			getClient().notifyProgress(new ProgressParams(Either.forLeft(token), Either.forLeft(progressReport)));
		}
	}

	public static void endProgress(final String token) {
		if (mode == ServerMode.LanguageServer && ClientCapabilityHelper.isProgressSupported()) {
			getClient().notifyProgress(new ProgressParams(Either.forLeft(token), Either.forLeft(new WorkDoneProgressEnd())));
		}
	}

	public static void requestSemanticTokensRefresh() {
		if (mode == ServerMode.LanguageServer && ClientCapabilityHelper.isSemanticTokensSupported()) {
			getClient().refreshSemanticTokens();
			getClient().refreshCodeLenses();
		}
	}

	/**
	 * Waits for the first analyzer job to complete.
	 */
	public static void waitFirstAnalyzer() {
		try {
			FIRST_ANALYSIS_LATCH.await();
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		}
	}

	private ServerInfo getServerInfo() {
		return new ServerInfo(ProductIdentity.TITAN_LS_FRIENDLY_NAME,
			ProductIdentity.getCurrentVersion());
	}

	public static boolean isBuildCancelled() {
		return CANCEL_BUILD.get();
	}

	public static void requestBuildCancellation() {
		CANCEL_BUILD.set(true);
		cancelTimestamp = System.nanoTime();
	}

	private static void resetBuildStatus() {
		CANCEL_BUILD.set(false);
	}

	public static ServerMode getMode() {
		return mode;
	}

	private static void setMode(final ServerMode mode) {
		TitanLanguageServer.mode = mode;
	}

	public static FolderConfiguration getFolderConfiguration() {
		return folderConfiguration;
	}

	public static void setFolderConfiguration(FolderConfiguration folderConfiguration) {
		TitanLanguageServer.folderConfiguration = folderConfiguration;
	}
}
