/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;

/**
 * This interface represents the language element that can have document comment
 * (information, source code, etc.)
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public interface ICommentable {
	// standard tags
	String COMMENT_START = "/**\n";
	String COMMENT_END = " */\n";
	String LINE_START = " * ";
	String AT = "@";
	String AUTHOR_TAG = "@author";
	String CONFIG_TAG = "@config";
	String DESC_TAG = "@desc";
	String EXCEPTION_TAG = "@exception";
	String MEMBER_TAG = "@member";
	String PARAM_TAG = "@param";
	String PRIORITY_TAG = "@priority";
	String PURPOSE_TAG = "@purpose";
	String REFERENCE_TAG = "@reference";
	String REMARK_TAG = "@remark";
	String REQUIREMENT_TAG = "@requirement";
	String RETURN_TAG = "@return";
	String SEE_TAG = "@see";
	String SINCE_TAG = "@since";
	String STATUS_TAG = "@status";
	String URL_TAG = "@url";
	String VERDICT_TAG = "@verdict";
	String VERSION_TAG = "@version";
	// non-standard tags
	String CATEGORY_TAG = "@category";

	/**
	 * Returns whether the documentation comment exists for this node
	 * @return {@code true} if the documentation comment is available, {@code false} otherwise
	 */
	boolean hasDocumentComment();
	
	/**
	 * Returns whether documentation comment exists for this node that is not inherited
	 * from its parent. If not overridden, it returns <code>false</code>.
	 * @return {@code true} if the documentation comment is available, {@code false} otherwise
	 */
	default boolean hasOwnDocumentComment() {
		return false;
	}

	/**
	 * Returns the object containing the documentation comment
	 * @return the object containing the documentation comment
	 */
	DocumentComment getDocumentComment();

	/**
	 * Sets the documentation comment for this definition (ETSI ES 201 873-10 format)
	 * @param docComment the unparsed documentation comment object
	 */
	void setDocumentComment(DocumentComment docComment);
	
	/**
	 * Performs the semantic check of the document comment for this node. 
	 */
	default void checkDocumentComment() {
		// empty
	}

	/** 
	 * Gets information for the given node that could be displayed in a source editor hover
	 * @param editor 
	 * @return
	 */
	Ttcn3HoverContent getHoverContent();

	/**
	 * Generates the documentation comment skeleton for this node based on AST information
	 */
	String generateDocComment(String indentation, String lineEnding);
}
