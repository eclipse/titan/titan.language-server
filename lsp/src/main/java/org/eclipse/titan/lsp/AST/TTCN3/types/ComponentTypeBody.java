/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.BridgingNamedNode;
import org.eclipse.titan.lsp.AST.FieldSubReference;
import org.eclipse.titan.lsp.AST.IIdentifiable;
import org.eclipse.titan.lsp.AST.ILocatableComment;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferenceChainElement;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.TTCN3Scope;
import org.eclipse.titan.lsp.AST.TTCN3.VisibilityModifier;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute.ExtensionAttribute_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionsAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.extensionattributeparser.ExtensionAttributeAnalyzer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the body of a component type.
 *
 * @author Kristof Szabados
 * */
public final class ComponentTypeBody extends TTCN3Scope
	implements IReferenceChainElement, ILocateableNode, IIncrementallyUpdatable, ILocatableComment, IIdentifiable {

	private static final String FULLNAMEPART = "<extends>";

	public static final String CIRCULAREXTENSIONCHAIN = "Circular extension chain is not allowed: {0}";
	public static final String INHERITEDDEFINITIONLOCATION = "Definition `{0}'' inherited from component type `{1}'' is here";
	public static final String INHERITANCECOLLISSION =
			"Definition `{0}'' inherited from component type `{1}'' collides with definition inherited from `{2}''";
	public static final String LOCALINHERTANCECOLLISSION = "Local Definiton `{0}'' collides with definition inherited from component type `{1}''";
	public static final String INHERITEDLOCATION = "Inherited definition of `{0}'' is here";

	private static final String HIDINGSCOPEELEMENT = "The name of the inherited definition `{0}'' is not unique in the scope hierarchy";
	public static final String HIDDENSCOPEELEMENT = "Previous definition with identifier `{0}'' in higher scope unit is here";
	public static final String HIDINGMODULEIDENTIFIER = "Inherited definition with name `{0}'' hides a module identifier";

	public static final String MEMBERNOTVISIBLE = "The member definition `{0}'' in component type `{1}'' is not visible in this scope";

	private Location location = NULL_Location.getInstance();

	private Location commentLocation = null;

	/** the identifier of the component does not belong to the componentTypeBody naturally !*/
	private final Identifier identifier;
	/** component references from the extends part or null if none */
	private final ComponentTypeReferenceList extendsReferences;
	/** component references from the extend attributes null if none */
	private ComponentTypeReferenceList attrExtendsReferences = new ComponentTypeReferenceList();

	private final Set<ComponentTypeBody> compatibleBodies = new CopyOnWriteArraySet<ComponentTypeBody>();

	/** The component's own definitions */
	private final DefinitionContainer definitions = new DefinitionContainer();
	/**
	 * The list of definitions gained through extends references.
	 * Used only to speed up some operations.
	 * */
	private final Map<String, Definition> extendsGainedDefinitions = new HashMap<String, Definition>();
	/**
	 * The list of definitions gained through extends attribute.
	 * Used only to speed up some operations.
	 * */
	private final Map<String, Definition> attributeGainedDefinitions = new HashMap<String, Definition>();

	/** the with attributes of the definition does not belong to the componentTypeBody naturally !*/
	private WithAttributesPath withAttributesPath;

	// the component type to which this body belongs.
	private Component_Type myType;

	/**
	 * Holds the last time when these definitions were checked, or null if never.
	 */
	private CompilationTimeStamp lastCompilationTimeStamp;
	private CompilationTimeStamp lastUniquenessCheck;

	public ComponentTypeBody(final Identifier identifier, final ComponentTypeReferenceList extendsReferences) {
		this.identifier = identifier;
		this.extendsReferences = extendsReferences;

		if (extendsReferences != null) {
			extendsReferences.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (!definitions.isEmpty()) {
			Identifier tempIdentifier;
			for (final Definition definition : definitions) {
				if (definition == child) {
					tempIdentifier = definition.getIdentifier();
					return builder.append(INamedNode.DOT).append(tempIdentifier.getDisplayName());
				}
			}
		} else if (extendsReferences != child) {
			return builder.append(FULLNAMEPART);
		}

		return builder;
	}

	@Override
	public Identifier getIdentifier() {
		return identifier;
	}

	public Component_Type getMyType() {
		return myType;
	}

	public void setMyType(final Component_Type type) {
		myType = type;
	}

	public void setMyScope(final Scope scope) {
		setParentScope(scope);
		if (NULL_Location.getInstance().equals(location)) {
			scope.addSubScope(location, this);
		}

		if (extendsReferences != null) {
			extendsReferences.setMyScope(scope);
		}
	}

	/**
	 * Sets the parent path for the with attribute path element of this
	 * component type. Also, creates the with attribute path node if it did not
	 * exist before.
	 *
	 *@param parent the parent to be set.
	 * */
	public void setAttributeParentPath(final WithAttributesPath parent) {
		withAttributesPath = parent;
		for (final Definition def : definitions) {
			def.setAttributeParentPath(parent);
		}
	}

	/**
	 * Sets the genname of embedded definitions using the provided prefix
	 * */
	public void setGenName(final String prefix) {
		for (final Definition def : definitions) {
			def.setGenName(prefix + def.getIdentifier().getName());
		}
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	public Location getCommentLocation() {
		return commentLocation;
	}

	@Override
	public void setCommentLocation(final Location commentLocation) {
		this.commentLocation = commentLocation;
	}


	public ConcurrentMap<String, Definition> getDefinitionMap() {
		return definitions.getDefinitionMap();
	}

	/**
	 * @return the list of the extensions.
	 * */
	public ComponentTypeReferenceList getExtensions() {
		return extendsReferences;
	}

	/**
	 * @return The list of attribute extension or null if none given.
	 * */
	public ComponentTypeReferenceList getAttributeExtensions() {
		return attrExtendsReferences;
	}

	public List<Definition> getDefinitions() {
		return definitions.getDefinitions();
	}
	
	@Override
	public List<Definition> getVisibleDefinitions() {
		return Collections.unmodifiableList(getDefinitions());
	}

	/**
	 * Detects if an assignment with the provided identifier exists inside this
	 * component.
	 *
	 * @param identifier the identifier to be use to search for the assignment.
	 * @return true if an assignment with the same identifier exists, or false
	 *         otherwise.
	 * */
	public boolean hasLocalAssignmentWithId(final Identifier identifier) {
		if (lastUniquenessCheck == null) {
			checkUniqueness(CompilationTimeStamp.getBaseTimestamp());
		}

		final String name = identifier.getName();
		Definition definition = definitions.getDefinition(name);
		if (definition != null) {
			return true;
		}

		definition = extendsGainedDefinitions.get(name);
		if (definition != null) {
			return true;
		}

		return attributeGainedDefinitions.containsKey(name);
	}

	/**
	 * Returns the assignment of this component fitting the provided identifier.
	 *
	 * @param identifier the identifier of the assignment
	 * @return the assignment identified, or null in case of an error.
	 * */
	public Definition getLocalAssignmentById(final Identifier identifier) {
		if (lastUniquenessCheck == null) {
			checkUniqueness(CompilationTimeStamp.getBaseTimestamp());
		}

		final String name = identifier.getName();
		Definition definition = definitions.getDefinition(name);
		if (definition != null) {
			return definition;
		}

		definition = extendsGainedDefinitions.get(name);
		if (definition != null) {
			if (VisibilityModifier.Public.equals(definition.getVisibilityModifier())) {
				return definition;
			} else {
				identifier.getLocation().reportSemanticError(MessageFormat.format(
						MEMBERNOTVISIBLE, identifier.getDisplayName(), this.identifier.getDisplayName()));
			}
		}

		definition = attributeGainedDefinitions.get(name);
		if (definition != null) {
			if (VisibilityModifier.Public.equals(definition.getVisibilityModifier())) {
				return definition;
			} else {
				identifier.getLocation().reportSemanticError(MessageFormat.format(
						MEMBERNOTVISIBLE, identifier.getDisplayName(), this.identifier.getDisplayName()));
			}
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasAssignmentWithId(final CompilationTimeStamp timestamp, final Identifier identifier) {
		final String name = identifier.getName();
		Definition definition = definitions.getDefinition(name);
		if (definition != null) {
			return true;
		}

		definition = extendsGainedDefinitions.get(name);
		if (definition != null) {
			if (VisibilityModifier.Public.equals(definition.getVisibilityModifier())) {
				return true;
			}
		}

		definition = attributeGainedDefinitions.get(name);
		if (definition != null) {
			if (VisibilityModifier.Public.equals(definition.getVisibilityModifier())) {
				return true;
			}
		}

		return super.hasAssignmentWithId(timestamp, identifier);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		if (reference.getModuleIdentifier() != null) {
			return getParentScope().getAssBySRef(timestamp, reference);
		}

		if (lastUniquenessCheck == null) {
			checkUniqueness(timestamp);
		}

		final String name = reference.getId().getName();
		Definition definition = definitions.getDefinition(name);
		if (definition != null) {
			return definition;
		}

		definition = extendsGainedDefinitions.get(name);
		if (definition != null) {
			if (VisibilityModifier.Public.equals(definition.getVisibilityModifier())) {
				return definition;
			}

			reference.getLocation().reportSemanticError(MessageFormat.format(
					MEMBERNOTVISIBLE, reference.getId().getDisplayName(), identifier.getDisplayName()));
			return null;
		}

		definition = attributeGainedDefinitions.get(name);
		if (definition != null) {
			if (VisibilityModifier.Public.equals(definition.getVisibilityModifier())) {
				return definition;
			}

			reference.getLocation().reportSemanticError(MessageFormat.format(
					MEMBERNOTVISIBLE, reference.getId().getDisplayName(), identifier.getDisplayName()));
			return null;
		}

		return getParentScope().getAssBySRef(timestamp, reference);
	}

	/**
	 * Collect all component type bodies that can be reached, recursively, via extends.
	 *
	 * @return the collected component type bodies.
	 * */
	private CopyOnWriteArrayList<ComponentTypeBody> getExtendsInheritedComponentBodies() {
		final CopyOnWriteArrayList<ComponentTypeBody> result = new CopyOnWriteArrayList<ComponentTypeBody>();
		final LinkedList<ComponentTypeBody> toBeChecked = new LinkedList<ComponentTypeBody>(extendsReferences.getComponentBodies());
		while(!toBeChecked.isEmpty()) {
			final ComponentTypeBody body = toBeChecked.removeFirst();
			if(!result.contains(body)) {
				result.add(body);
				for(final ComponentTypeBody subBody : body.extendsReferences.getComponentBodies()) {
					if(!result.contains(subBody) && !toBeChecked.contains(subBody)) {
						toBeChecked.add(subBody);
					}
				}
			}
		}

		return result;
	}

	/**
	 * Collect all component type bodies that can be reached, recursively, via extends attributes.
	 *
	 * @return the collected component type bodies.
	 * */
	private List<ComponentTypeBody> getAttributeExtendsInheritedComponentBodies() {
		final List<ComponentTypeBody> result = new ArrayList<ComponentTypeBody>();
		final LinkedList<ComponentTypeBody> toBeChecked = new LinkedList<ComponentTypeBody>(attrExtendsReferences.getComponentBodies());
		while(!toBeChecked.isEmpty()) {
			final ComponentTypeBody body = toBeChecked.removeFirst();
			if(!result.contains(body)) {
				result.add(body);
				if(body.attrExtendsReferences != null) {
					for(final ComponentTypeBody subBody : body.attrExtendsReferences.getComponentBodies()) {
						if(!result.contains(subBody) && !toBeChecked.contains(subBody)) {
							toBeChecked.add(subBody);
						}
					}
				}
			}
		}

		return result;
	}

	/**
	 * Adds assignments (right now only definitions) the this component type
	 * body.
	 *
	 * @param assignments the assignments to be added.
	 * */
	public void addAssignments(final List<Definition> assignments) {
		for (final Definition def : assignments) {
			if (def != null && def.getIdentifier() != null && def.getIdentifier().getLocation() != null) {
				definitions.add(def);
				def.setFullNameParent(this);
				def.setMyScope(this);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public String chainedDescription() {
		return getFullName();
	}

	@Override
	/** {@inheritDoc} */
	public Location getChainLocation() {
		if (identifier != null && identifier.getLocation() != null) {
			return identifier.getLocation();
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public String toString() {
		return getFullName();
	}

	/**
	 * Checks if the references of expanded components contain any recursive
	 * references, which is not allowed.
	 *
	 * @param refChain the reference chain used to store the visited components.
	 * */
	protected void checkRecursion(final IReferenceChain refChain) {
		if (refChain.add(this)) {
			return;
		}

		if (extendsReferences != null && refChain.add(this)) {
			extendsReferences.checkRecursion(refChain);
		}
	}

	/**
	 * Collects the extends extension attributes, from the with attributes assigned to the component type.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * */
	private void collectExtensionAttributes(final CompilationTimeStamp timestamp) {
		if (withAttributesPath == null) {
			return;
		}

		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);

		SingleWithAttribute attribute;
		List<AttributeSpecification> specifications = null;
		for (int i = 0; i < realAttributes.size(); i++) {
			attribute = realAttributes.get(i);
			if (Attribute_Type.Extension_Attribute.equals(attribute.getAttributeType())) {
				final Qualifiers qualifiers = attribute.getQualifiers();
				if (qualifiers == null || qualifiers.size() == 0) {
					if (specifications == null) {
						specifications = new ArrayList<AttributeSpecification>();
					}
					specifications.add(attribute.getAttributeSpecification());
				}
			}
		}

		if (specifications == null) {
			return;
		}

		final List<ExtensionAttribute> attributes = new ArrayList<ExtensionAttribute>();
		AttributeSpecification specification;
		for (int i = 0; i < specifications.size(); i++) {
			specification = specifications.get(i);
			final ExtensionAttributeAnalyzer analyzer = new ExtensionAttributeAnalyzer();
			analyzer.parse(specification);
			final List<ExtensionAttribute> temp = analyzer.getAttributes();
			if (temp != null) {
				attributes.addAll(temp);
			}
		}

		if (attributes.isEmpty()) {
			return;
		}

		attrExtendsReferences = new ComponentTypeReferenceList();
		for (final ExtensionAttribute tempAttribute : attributes) {
			if (ExtensionAttribute_type.EXTENDS.equals(tempAttribute.getAttributeType())) {
				final ExtensionsAttribute extensionsAttribute = (ExtensionsAttribute) tempAttribute;
				for (int j = 0, size2 = extensionsAttribute.size(); j < size2; j++) {
					final IType tempType = extensionsAttribute.get(j);
					if (Type_type.TYPE_REFERENCED.equals(tempType.getTypetype())) {
						attrExtendsReferences.addReference(((Referenced_Type) tempType).getReference());
					}
				}
			}
		}
		attrExtendsReferences.setFullNameParent(new BridgingNamedNode(this, ".<extends attribute>"));
		attrExtendsReferences.setMyScope(parentScope);
	}

	/**
	 * Initializes the list of component type bodies with which the actual one is compatible.
	 * It can be done much faster like this, than doing the checks every time is_compatible is called.
	 *
	 * @param references the component type body references referred by the actual component.
	 * */
	private void initCompatibility(final ComponentTypeReferenceList references) {
		final List<ComponentTypeBody> componentBodies = references.getComponentBodies();
		for (final ComponentTypeBody componentBody : componentBodies) {
			compatibleBodies.add(componentBody);
			// compatible with all components which are compatible with the compatible component
			for (final ComponentTypeBody tempComponentBody : componentBody.compatibleBodies) {
				compatibleBodies.add(tempComponentBody);
			}
		}
	}

	/**
	 * Checks the uniqueness of the definitions, and also builds a hashmap of
	 * them to speed up further searches.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * */
	private void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (lastUniquenessCheck != null && !lastUniquenessCheck.isLess(timestamp)) {
			return;
		}

		lastUniquenessCheck = timestamp;

		compatibleBodies.clear();
		definitions.checkUniqueness();

		addDefinitionsOfExtendsParents(timestamp);
		addDefinitionsOfExtendAttributeParents(timestamp);
	}

	private void addDefinitionsOfExtendAttributeParents(final CompilationTimeStamp timestamp) {
		attributeGainedDefinitions.clear();
		if (attrExtendsReferences != null) {
			attrExtendsReferences.check(timestamp);

			final IReferenceChain referenceChain = ReferenceChain.getInstance(CIRCULAREXTENSIONCHAIN, true);
			if (referenceChain.add(this)) {
				attrExtendsReferences.checkRecursion(referenceChain);
			}
			referenceChain.release();

			initCompatibility(attrExtendsReferences);

			collectDefinitionsFromAttributeExtends();
		}
	}

	private void addDefinitionsOfExtendsParents(final CompilationTimeStamp timestamp) {
		extendsGainedDefinitions.clear();
		if (extendsReferences == null) {
			return;
		}

		extendsReferences.check(timestamp);

		final IReferenceChain referenceChain = ReferenceChain.getInstance(CIRCULAREXTENSIONCHAIN, true);
		if (referenceChain.add(this)) {
			extendsReferences.checkRecursion(referenceChain);
		}
		referenceChain.release();

		initCompatibility(extendsReferences);

		// collect definitions
		final CopyOnWriteArrayList<ComponentTypeBody> bodies = getExtendsInheritedComponentBodies();
		for (final ComponentTypeBody body : bodies) {
			final ConcurrentMap<String, Definition> subDefinitionMap = body.getDefinitionMap();
			for (final Definition definition : subDefinitionMap.values()) {
				final String name = definition.getIdentifier().getName();
				if (definitions.hasDefinition(name)) {
					final Definition localDefinition = definitions.getDefinition(name);
					localDefinition.getIdentifier().getLocation().reportSemanticError(MessageFormat.format(
							LOCALINHERTANCECOLLISSION, definition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName()));
				} else if (extendsGainedDefinitions.containsKey(name)) {
					final Definition previousDefinition = extendsGainedDefinitions.get(name);
					if (!previousDefinition.equals(definition)) {
						// it is not the same definition inherited on two paths
						if (this.equals(previousDefinition.getMyScope())) {
							previousDefinition.getLocation().reportSemanticError(MessageFormat.format(LOCALINHERTANCECOLLISSION,
									previousDefinition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName()));
							definition.getIdentifier().getLocation().reportSemanticWarning(
									MessageFormat.format(INHERITEDLOCATION, definition.getIdentifier().getDisplayName()));
						} else if (identifier != null && identifier.getLocation() != null) {
							identifier.getLocation().reportSemanticError(MessageFormat.format(INHERITANCECOLLISSION,
									definition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName(), previousDefinition.getMyScope().getFullName()));

							definition.getIdentifier().getLocation().reportSemanticWarning(MessageFormat.format(INHERITEDDEFINITIONLOCATION,
									definition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName()));
							previousDefinition.getIdentifier().getLocation().reportSemanticWarning(MessageFormat.format(INHERITEDDEFINITIONLOCATION,
									previousDefinition.getIdentifier().getDisplayName(), previousDefinition.getMyScope().getFullName()));
						}
					}
				} else {
					extendsGainedDefinitions.put(name, definition);

					if (!definition.getMyScope().getModuleScope().equals(parentScope.getModuleScope())) {
						if (parentScope.hasAssignmentWithId(timestamp, definition.getIdentifier())) {
							if (identifier != null && identifier.getLocation() != null) {
								identifier.getLocation().reportSemanticError(
										MessageFormat.format(HIDINGSCOPEELEMENT, definition.getIdentifier().getDisplayName()));
								final List<ISubReference> subReferences = new ArrayList<ISubReference>();
								subReferences.add(new FieldSubReference(definition.getIdentifier()));
								final Reference reference = new Reference(null, subReferences);
								final Assignment assignment = parentScope.getAssBySRef(timestamp, reference);
								if (assignment != null && assignment.getLocation() != null) {
									assignment.getLocation().reportSemanticError(
											MessageFormat.format(HIDDENSCOPEELEMENT, definition.getIdentifier().getDisplayName()));
								}
								definition.getIdentifier().getLocation().reportSemanticWarning(
										MessageFormat.format(INHERITEDDEFINITIONLOCATION, definition.getIdentifier().getDisplayName(),
												definition.getMyScope().getFullName()));
							}
						} else if (parentScope.isValidModuleId(definition.getIdentifier())) {
							final String severity = Configuration.INSTANCE.getString(Configuration.REPORT_MODULE_NAME_REUSE, GeneralConstants.ERROR);
							definition.getLocation().reportConfigurableSemanticProblem(severity,
								MessageFormat.format(HIDINGMODULEIDENTIFIER, definition.getIdentifier().getDisplayName()));
						}
					}
				}
			}
		}
	}

	/**
	 * Collects the definitions coming from extends attributes.
	 * In reality no collection is done, but it is checked that the actual component has all definitions from the extended component.
	 * */
	private void collectDefinitionsFromAttributeExtends() {
		final List<ComponentTypeBody> parents = getAttributeExtendsInheritedComponentBodies();
		for (final ComponentTypeBody parent : parents) {
			for (final Definition definition : parent.getDefinitions()) {
				final Identifier id = definition.getIdentifier();
				final String name = id.getName();

				// Check if we have inherited 2 different definitions with the same name
				if (extendsGainedDefinitions.containsKey(name)) {
					final Definition myDefinition = extendsGainedDefinitions.get(name);
					if (definition != myDefinition) {
						location.reportSemanticError(MessageFormat.format(
								INHERITANCECOLLISSION,
								id.getDisplayName(),
								definition.getMyScope().getFullName(),
								myDefinition.getMyScope().getFullName()));
					}
					continue;
				}

				if (!definitions.hasDefinition(name)) {
					location.reportSemanticError(MessageFormat.format(
							"Missing local definition of `{0}'', which was inherited from component type `{1}''",
							id.getDisplayName(), definition.getMyScope().getFullName()));
					continue;
				}

				attributeGainedDefinitions.put(name, definition);
			}
		}
	}

	/**
	 * Checks that the definitions inherited via extends attributes
	 *  are identical to ones contained in the component, or inherited via extends clause.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * */
	private void checkExtensionAttributes(final CompilationTimeStamp timestamp) {
		if (attributeGainedDefinitions.isEmpty()) {
			return;
		}

		for (final Definition originalDefinition : definitions) {
			final Definition inheritedDefinition = attributeGainedDefinitions.get(originalDefinition.getIdentifier().getName());
			if (inheritedDefinition != null) {
				originalDefinition.checkIdentical(timestamp, inheritedDefinition);
			}
		}
	}

	/**
	 * Does the semantic checking of the contained definitions.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * */
	public void check(final CompilationTimeStamp timestamp) {
		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;

		attrExtendsReferences = null;
		collectExtensionAttributes(timestamp);
		checkUniqueness(timestamp);

		definitions.checkAll(timestamp);

		checkExtensionAttributes(timestamp);
	}

	/**
	 * Checks if the provided component is compatible with this one.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * @param other the other component to check against
	 *
	 * @return true if the other component is compatible with this, false
	 *         otherwise.
	 * */
	public boolean isCompatible(final CompilationTimeStamp timestamp, final ComponentTypeBody other) {
		if (this == other) {
			return true;
		}

		check(timestamp);
		other.check(timestamp);

		if (definitions.isEmpty() && extendsReferences == null && attrExtendsReferences == null) {
			// empty component
			return true;
		}

		return other.compatibleBodies.contains(this);
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final Definition definition : definitions) {
			definition.updateSyntax(reparser, false);
			reparser.updateLocation(definition.getLocation());
			if(!definition.getLocation().equals(definition.getCumulativeDefinitionLocation())) {
				reparser.updateLocation(definition.getCumulativeDefinitionLocation());
			}
		}

		if (extendsReferences != null) {
			extendsReferences.updateSyntax(reparser, false);
			reparser.updateLocation(extendsReferences.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		for (final Definition definition : definitions) {
			if (definition.getLocation().containsPosition(offset)) {
				return definition;
			}
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final Definition def : definitions) {
			def.findReferences(referenceFinder, foundIdentifiers);
		}
		if (extendsReferences != null) {
			extendsReferences.findReferences(referenceFinder, foundIdentifiers);
		}
		if (attrExtendsReferences != null) {
			attrExtendsReferences.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		for (final Definition def : definitions) {
			if (!def.accept(v)) {
				return false;
			}
		}
		if (extendsReferences != null && !extendsReferences.accept(v)) {
			return false;
		}
		if (attrExtendsReferences != null && !attrExtendsReferences.accept(v)) {
			return false;
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}
}
