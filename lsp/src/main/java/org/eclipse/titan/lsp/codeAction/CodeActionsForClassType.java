/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import static java.util.Map.entry;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionKind;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;

public class CodeActionsForClassType extends CodeActionBase {
	public enum CodeActionType_Class {
		CLASSMEMBERCHANGEPUBLICTOPRIVATE,
		CLASSMEMBERREMOVEPUBLIC
	}

	private static final Map<CodeActionType_Class, BiFunction<TextDocumentIdentifier, Diagnostic, CodeAction>> codeActionList = Map.ofEntries(
			entry(CodeActionType_Class.CLASSMEMBERCHANGEPUBLICTOPRIVATE, CodeActionsForClassType::fixClassMemberModifierChangePublicToPrivate),
			entry(CodeActionType_Class.CLASSMEMBERREMOVEPUBLIC, CodeActionsForClassType::fixClassMemberModifierRemovePublic)
		);

	private static final String PRIVATE_MODIFIER = "private";


	public CodeActionsForClassType(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		super(doc, diagnostic);
	}
	
	private static CodeAction fixClassMemberModifierChangePublicToPrivate(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final CodeAction action = new CodeAction("Change to private");
		action.setKind(CodeActionKind.QuickFix);

		final WorkspaceEdit edit = new TextEditor(doc).replaceText(diagnostic.getRange(), PRIVATE_MODIFIER).getEdit();
		action.setEdit(edit);

		return action;
	}

	private static CodeAction fixClassMemberModifierRemovePublic(final TextDocumentIdentifier doc, final Diagnostic diagnostic) {
		final CodeAction action = new CodeAction("Remove public modifier");
		action.setKind(CodeActionKind.QuickFix);

		final Range publicKeywordRange = diagnostic.getRange();
		final int line = publicKeywordRange.getStart().getLine();
		final Position removeFrom = new Position(line, 0);
		final Position removeTo = new Position(line, publicKeywordRange.getEnd().getCharacter() + 1);

		final TextEditor editor = new TextEditor(doc);
		final WorkspaceEdit edit = editor
				.removeText(new Range(removeFrom, removeTo))
				.insertTab(removeFrom)
				.getEdit();

		action.setEdit(edit);

		return action;
	}


	@Override
	public List<Either<Command, CodeAction>> provideCodeActions() {
		return provideCodeActions(codeActionList, CodeActionType_Class.class);
	}
}
