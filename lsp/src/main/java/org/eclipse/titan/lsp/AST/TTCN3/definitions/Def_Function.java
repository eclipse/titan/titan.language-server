/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.NULL_Location;
import org.eclipse.titan.lsp.AST.NamedBridgeScope;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.ExtensionAttribute.ExtensionAttribute_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.PrototypeAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.SignatureExceptions;
import org.eclipse.titan.lsp.codeAction.CodeActionHelpers;
import org.eclipse.titan.lsp.codeAction.CodeActionForMissingReturn.CodeActionType_MissingReturn;
import org.eclipse.titan.lsp.codeAction.DiagnosticData.CodeActionProviderType;
import org.eclipse.titan.lsp.codeAction.data.MissingReturnData;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.extensionattributeparser.ExtensionAttributeAnalyzer;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;
import org.eclipse.titan.lsp.semantichighlighting.ISemanticInformation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Def_Function class represents TTCN3 function definitions.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public final class Def_Function extends Def_FunctionBase implements ISemanticInformation {
	private static final String FULLNAMEPART1 = ".<runs_on_type>";
	private static final String FULLNAMEPART2 = ".<formal_parameter_list>";
	private static final String FULLNAMEPART3 = ".<type>";
	private static final String FULLNAMEPART4 = ".<statement_block>";
	private static final String FULLNAMEPART5 = ".<mtc_type>";
	private static final String FULLNAMEPART6 = ".<system_type>";
	private static final String FULLNAMEPART7 = ".<port_type>";
	private static final String FULLNAMEPART8 = ".<finally_block>";
	private static final String PORTRETURNNOTALLOWED = "Functions can not return ports";

	private static final String DASHALLOWEDONLYFORTEMPLATES = "Using not used symbol (`-') as the default parameter"
			+ " is allowed only for modified templates";

	private static final String CLASSNORUNSON = "Class functions cannot have a `runs on` clause";
	private static final String CLASSNOMTC = "Class functions cannot have an `mtc` clause";
	private static final String CLASSNOSYSTEM = "Class functions cannot have a `system` clause";
	private static final String EXCEPTIONSOOP = "Function exceptions are only supported in the OOP extension";

	private static final String KIND = "function";

	private final Reference runsOnRef;
	private Component_Type runsOnType = null;
	private final Reference mtcReference;
	private Component_Type mtcType = null;
	private final Reference systemReference;
	private Component_Type systemType = null;
	private final Reference portReference;
	private IType portType = null;

	private final boolean returnsTemplate;
	private final TemplateRestriction.Restriction_type templateRestriction;
	private final StatementBlock finallyBlock;
	private EncodingPrototype_type prototype;
	private Type inputType;
	private Type outputType;

	private final SignatureExceptions exceptions;
	private final boolean isClassFunction;

	private final Location classModifierLocation;
	private final Location funcModifierLocation;

	private boolean isConstructor = false;

	/** Indicates if this function is a class function overriding a function of the parent class */
	private boolean isOverride = false;

	// stores whether this function can be started or not
	private boolean isStartable;

	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final Reference portReference,
			final Type returnType, final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction,
			final SignatureExceptions exceptions, final StatementBlock block, final StatementBlock finallyBlock,
			final boolean isClassFunction, final boolean isAbstract, final boolean isFinal, final Location classModifierLocation,
			final boolean isDeterministic, final boolean isControl, final Location funcModifierLocation) {
		super(identifier, block, isAbstract, isFinal, isDeterministic, isControl, formalParameters, returnType);
		assignmentType = (returnType == null) ? Assignment_type.A_FUNCTION : (returnsTemplate ? Assignment_type.A_FUNCTION_RTEMP
				: Assignment_type.A_FUNCTION_RVAL);
		if (formalParameterList != null) {
			formalParameterList.setMyDefinition(this);
			formalParameterList.setFullNameParent(this);
		}
		this.runsOnRef = runsOnRef;
		this.mtcReference = mtcReference;
		this.systemReference = systemReference;
		this.portReference = portReference;
		this.returnsTemplate = returnsTemplate;
		this.templateRestriction = templateRestriction;
		this.finallyBlock = finallyBlock;
		this.isClassFunction = isClassFunction;
		this.classModifierLocation = classModifierLocation;
		this.funcModifierLocation = funcModifierLocation;
		this.exceptions = exceptions;

		this.prototype = EncodingPrototype_type.NONE;
		inputType = null;
		outputType = null;

		if (block != null) {
			block.setMyDefinition(this);
			block.setFullNameParent(this);
		}
		if (finallyBlock != null) {
			finallyBlock.setMyDefinition(this);
			finallyBlock.setFullNameParent(this);
			finallyBlock.setOwnerIsFinally();
		}
		if (runsOnRef != null) {
			runsOnRef.setFullNameParent(this);
		}
		if (mtcReference != null) {
			mtcReference.setFullNameParent(this);
		}
		if (systemReference != null) {
			systemReference.setFullNameParent(this);
		}
		if (portReference != null) {
			portReference.setFullNameParent(this);
		}
		if (returnType != null) {
			returnType.setOwnertype(TypeOwner_type.OT_FUNCTION_DEF, this);
			returnType.setFullNameParent(this);
		}
		if (isClassFunction && identifier.getName().equals("create")) {
			isConstructor = true;
		}
	}

	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final Reference portReference,
			final Type returnType, final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction,
			final SignatureExceptions exceptions, final StatementBlock block, final StatementBlock finallyBlock,
			final boolean isDeterministic, final boolean isControl, final Location funcModifierLocation) {
		this(identifier, formalParameters, runsOnRef, mtcReference, systemReference, portReference, returnType,
			 returnsTemplate, templateRestriction, exceptions, block, finallyBlock, false, false, false, null,
			 isDeterministic, isControl, funcModifierLocation);
	}

	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final Reference portReference,
			final Type returnType, final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction,
			final SignatureExceptions exceptions, final StatementBlock block, final StatementBlock finallyBlock,
			final boolean isDeterministic, final boolean isControl, final Location funcModifierLocation, final boolean isClassFunction) {
		this(identifier, formalParameters, runsOnRef, mtcReference, systemReference, portReference, returnType,
			 returnsTemplate, templateRestriction, exceptions, block, finallyBlock, true, false, false, null,
			 isDeterministic, isControl, funcModifierLocation);
	}

	/**
	 * Constructor for functions defined by 'object'
	 * @param identifier
	 * @param returnType
	 */
	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Type returnType) {
		this(identifier, formalParameters, null, null, null, null, returnType, false, null, null, null, null, false, false, null);
	}

	public boolean isStartable() {
		return isStartable;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (runsOnRef == child) {
			return builder.append(FULLNAMEPART1);
		} else if (formalParameterList == child) {
			return builder.append(FULLNAMEPART2);
		} else if (returnType == child) {
			return builder.append(FULLNAMEPART3);
		} else if (statementBlock == child) {
			return builder.append(FULLNAMEPART4);
		} else if (mtcReference == child) {
			return builder.append(FULLNAMEPART5);
		} else if (systemReference == child) {
			return builder.append(FULLNAMEPART6);
		} else if (portReference == child) {
			return builder.append(FULLNAMEPART7);
		} else if (finallyBlock == child) {
			return builder.append(FULLNAMEPART8);
		}

		return builder;
	}

	public EncodingPrototype_type getPrototype() {
		return prototype;
	}

	public Type getInputType() {
		return inputType;
	}

	public Type getOutputType() {
		return outputType;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		return getAssignmentName();
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalDescription() {
		final StringBuilder nameBuilder = new StringBuilder(identifier.getDisplayName());
		nameBuilder.append('(');
		formalParameterList.getAsProposalDesriptionPart(nameBuilder);
		nameBuilder.append(')');
		return nameBuilder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		if (bridgeScope != null && bridgeScope.getParentScope() == scope) {
			return;
		}

		bridgeScope = new NamedBridgeScope();
		bridgeScope.setParentScope(scope);
		scope.addSubScope(getLocation(), bridgeScope);
		bridgeScope.setScopeMacroName(identifier.getDisplayName());

		super.setMyScope(bridgeScope);
		if (runsOnRef != null) {
			runsOnRef.setMyScope(bridgeScope);
		}
		if (mtcReference != null) {
			mtcReference.setMyScope(bridgeScope);
		}
		if (systemReference != null) {
			systemReference.setMyScope(bridgeScope);
		}
		if (portReference != null) {
			portReference.setMyScope(bridgeScope);
		}
		if (formalParameterList != null) {
			formalParameterList.setMyScope(bridgeScope);
		}
		if (returnType != null) {
			returnType.setMyScope(bridgeScope);
		}
		if (statementBlock != null) {
			statementBlock.setMyScope(formalParameterList);
		}
		if (finallyBlock != null) {
			finallyBlock.setMyScope(formalParameterList);
		}
		if (statementBlock != null) {
			bridgeScope.addSubScope(statementBlock.getLocation(), statementBlock);
		}
		if (formalParameterList != null) {
			bridgeScope.addSubScope(formalParameterList.getLocation(), formalParameterList);
		}
	}

	public Component_Type getRunsOnType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return runsOnType;
	}

	public Reference getRunsOnReference(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return runsOnRef;
	}

	public Component_Type getMtcType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return mtcType;
	}

	public Component_Type getSystemType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return systemType;
	}

	public IType getPortType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return portType;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;

		runsOnType = null;
		mtcType = null;
		systemType = null;
		portType = null;
		isStartable = false;

		if (exceptions != null) {
			final boolean isOopEnabled = Configuration.INSTANCE.getBoolean(Configuration.ENABLE_OOP_EXTENSION, true);
			if (! isOopEnabled) {
				exceptions.getLocation().reportSemanticError(EXCEPTIONSOOP);
			} 
		}

		if (isClassFunction) {
			if (runsOnRef != null) {
				runsOnRef.getLocation().reportSemanticError(CLASSNORUNSON);
			}
			if (mtcReference != null) {
				mtcReference.getLocation().reportSemanticError(CLASSNOMTC);
			}
			if (systemReference!= null) {
				systemReference.getLocation().reportSemanticError(CLASSNOSYSTEM);
			}
		} else {
			if (runsOnRef != null && portReference != null) {
				runsOnRef.getLocation().reportSemanticError("A `runs on' and a `port' clause cannot be present at the same time.");
			}
	
			if (runsOnRef != null) {
				runsOnType = runsOnRef.chkComponentypeReference(timestamp);
				if (runsOnType != null) {
					final Scope formalParlistPreviosScope = formalParameterList.getParentScope();
					if (formalParlistPreviosScope instanceof RunsOnScope
							&& ((RunsOnScope) formalParlistPreviosScope).getParentScope() == myScope) {
						((RunsOnScope) formalParlistPreviosScope).setComponentType(runsOnType);
					} else {
						final Scope tempScope = new RunsOnScope(runsOnType, myScope);
						formalParameterList.setMyScope(tempScope);
					}
				}
			}
	
			if (mtcReference != null) {
				mtcType = mtcReference.chkComponentypeReference(timestamp);
			}
	
			if (systemReference != null) {
				systemType = systemReference.chkComponentypeReference(timestamp);
			}
		}

		boolean canSkip = false;
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					canSkip = true;
				}
			}
		}

		if(!canSkip) {
			formalParameterList.reset();
		}

		formalParameterList.check(timestamp, getAssignmentType());

		if (returnType != null) {
			if (! isConstructor) {
				returnType.check(timestamp);
			}
			final IType returnedType = returnType.getTypeRefdLast(timestamp);
			if (Type_type.TYPE_PORT.equals(returnedType.getTypetype()) && returnType.getLocation() != null) {
				returnType.getLocation().reportSemanticError(PORTRETURNNOTALLOWED);
			}
		}

		if (formalParameterList.hasNotusedDefaultValue()) {
			formalParameterList.getLocation().reportSemanticError(DASHALLOWEDONLYFORTEMPLATES);
			return;
		}

		prototype = EncodingPrototype_type.NONE;
		inputType = null;
		outputType = null;

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
			analyzeExtensionAttributes(timestamp);
			checkPrototype(timestamp);
		}

		if (portReference != null) {
			final Assignment assignment = portReference.getRefdAssignment(timestamp, false);
			if (assignment != null) {
				portType = assignment.getType(timestamp);
				if (portType != null) {
					if (portType.getTypetype() == Type_type.TYPE_PORT) {
						final Scope tempScope = new PortScope((Port_Type)portType, myScope);
						formalParameterList.setMyScope(tempScope);
					} else {
						portReference.getLocation().reportSemanticError(MessageFormat.format("Reference `{0}'' does not refer to a port type.", portReference.getDisplayName()));
					}
				}
			}
		}

		// decision of startability
		isStartable = runsOnRef != null;
		isStartable &= formalParameterList.getStartability();
		if (isStartable && returnType != null && returnType.isComponentInternal(timestamp)) {
			isStartable = false;
		}

		if(canSkip) {
			return;
		}

		NamingConventionHelper.checkConvention(NamingConventionElement.Function, identifier, this);
		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (statementBlock != null) {
			statementBlock.check(timestamp);

			if (returnType != null && !isConstructor) {
				// check the presence of return statements
				switch (statementBlock.hasReturn(timestamp)) {
				case RS_NO:
					final String data = getMissingReturnData(timestamp);
					if (data != null) {
						final List<CodeActionType_MissingReturn> actionTypes = new ArrayList<>(Arrays.asList(
								CodeActionType_MissingReturn.ADDMISSINGRETURN
								));
						identifier.getLocation().reportSemanticError(
								"The function has a return type, but it does not have any return statement",
								actionTypes, CodeActionProviderType.MISSINGRETURN, data);
					} else {
						TitanLogger.logWarning("Invalid MissingReturnData: Diagnostic for function definition ("
								+ identifier.getDisplayName() + ") is not reported");
					}
					break;
				case RS_MAYBE:
					identifier.getLocation()
					.reportSemanticError(
							"The function has return type, but control might leave it without reaching a return statement");
					break;
				default:
					break;
				}
			}

			statementBlock.postCheck();
		}

		if (finallyBlock != null) {
			finallyBlock.check(timestamp);
		}

		checkDocumentComment();
	}

	/**
	 * Checks and returns whether the function is startable. Reports the appropriate error messages.
	 *
	 * @param timestamp the timestamp of the actual build cycle.
	 * @param errorLocation the location to report the error to, if needed.
	 * @return {@code true} if startable, {@code false} otherwise
	 */
	public boolean checkStartable(final CompilationTimeStamp timestamp, final Location errorLocation) {
		check(timestamp);

		if (runsOnRef == null) {
			errorLocation.reportSemanticError(MessageFormat.format(
					"Function `{0}'' cannot be started on parallel test component because it does not have a `runs on'' clause",
					getFullName()));
		}

		formalParameterList.checkStartability(timestamp, "Function", this, errorLocation);

		if (returnType != null && returnType.isComponentInternal(timestamp)) {
			final Set<IType> typeSet = new HashSet<IType>();
			final String errorMessage = "the return type or embedded in the return type of function `" + getFullName()
					+ "' if it is started on a parallel test component";
			returnType.checkComponentInternal(timestamp, typeSet, errorMessage);
		}

		if (isStartable) {
			return true;
		}

		return false;
	}

	/**
	 * Convert and check the encoding attributes applied to this function.
	 * @param timestamp the timestamp of the actual build cycle.
	 */
	public void analyzeExtensionAttributes(final CompilationTimeStamp timestamp) {
		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);

		List<AttributeSpecification> specifications = null;
		for (final SingleWithAttribute attribute : realAttributes) {
			if (Attribute_Type.Extension_Attribute.equals(attribute.getAttributeType())) {
				final Qualifiers qualifiers = attribute.getQualifiers();
				if (qualifiers == null || qualifiers.size() == 0) {
					if (specifications == null) {
						specifications = new ArrayList<AttributeSpecification>();
					}
					specifications.add(attribute.getAttributeSpecification());
				}
			}
		}

		if (specifications == null) {
			return;
		}

		final List<ExtensionAttribute> attributes = new ArrayList<ExtensionAttribute>();
		for (final AttributeSpecification specification: specifications) {
			final ExtensionAttributeAnalyzer analyzer = new ExtensionAttributeAnalyzer();
			analyzer.parse(specification);
			final List<ExtensionAttribute> temp = analyzer.getAttributes();
			if (temp != null) {
				attributes.addAll(temp);
			}
		}

		for (final ExtensionAttribute extensionAttribute: attributes) {
			if (ExtensionAttribute_type.PROTOTYPE.equals(extensionAttribute.getAttributeType())) {
				final PrototypeAttribute realAttribute = (PrototypeAttribute) extensionAttribute;
				if (EncodingPrototype_type.NONE.equals(prototype)) {
					prototype = realAttribute.getPrototypeType();
				} else {
					realAttribute.getLocation().reportSingularSemanticError("Duplicate attribute `prototype'");
				}
			}
		}
	}

	/**
	 * Checks the prototype attribute set for this function definition.
	 *
	 * @param timestamp
	 *                the timestamp of the actual build cycle.
	 * */
	public void checkPrototype(final CompilationTimeStamp timestamp) {
		if (EncodingPrototype_type.NONE.equals(prototype)) {
			return;
		}

		// checking formal parameter list
		if (EncodingPrototype_type.CONVERT.equals(prototype)) {
			if (formalParameterList.size() == 1) {
				final FormalParameter parameter = formalParameterList.get(0);
				final Assignment_type assignmentType = parameter.getRealAssignmentType();
				if (Assignment_type.A_PAR_VAL_IN.semanticallyEquals(assignmentType)) {
					inputType = parameter.getType(timestamp);
				} else {
					parameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The parameter must be an `in'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), parameter.getAssignmentName()));
				}
			} else {
				formalParameterList.getLocation()
				.reportSemanticError(
						MessageFormat.format(
								"The function must have one parameter instead of {0} for attribute `prototype({1})''",
								formalParameterList.size(), prototype.getName()));
			}
		} else if (formalParameterList.size() == 2) {
			final FormalParameter firstParameter = formalParameterList.get(0);
			if (EncodingPrototype_type.SLIDING.equals(prototype)) {
				if (Assignment_type.A_PAR_VAL_INOUT.semanticallyEquals(firstParameter.getRealAssignmentType())) {
					final Type firstParameterType = firstParameter.getType(timestamp);
					final IType last = firstParameterType.getTypeRefdLast(timestamp);
					if (last.getIsErroneous(timestamp)) {
						inputType = firstParameterType;
					} else {
						switch (last.getTypetypeTtcn3()) {
						case TYPE_OCTETSTRING:
						case TYPE_CHARSTRING:
							inputType = firstParameterType;
							break;
						default:
							firstParameter.getLocation()
							.reportSemanticError(
									MessageFormat.format(
											"The type of the first parameter must be `octetstring'' or `charstring'' for attribute `prototype({0})''",
											prototype.getName()));
							break;
						}
					}
				} else {
					firstParameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The first parameter must be an `inout'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), firstParameter.getAssignmentName()));
				}
			} else {
				final Assignment_type assignmentType = firstParameter.getRealAssignmentType();
				if (Assignment_type.A_PAR_VAL_IN.semanticallyEquals(assignmentType) || Assignment_type.A_PAR_VAL.semanticallyEquals(assignmentType)) {
					inputType = firstParameter.getType(timestamp);
				} else {
					firstParameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The first parameter must be an `in'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), firstParameter.getAssignmentName()));
				}
			}

			final FormalParameter secondParameter = formalParameterList.get(1);
			if (Assignment_type.A_PAR_VAL_OUT.semanticallyEquals(secondParameter.getRealAssignmentType())) {
				outputType = secondParameter.getType(timestamp);
			} else {
				secondParameter.getLocation()
				.reportSemanticError(
						MessageFormat.format(
								"The second parameter must be an `out'' value parameter for attribute `prototype({0})'' instead of {1}",
								prototype.getName(), secondParameter.getAssignmentName()));
			}
		} else {
			formalParameterList.getLocation().reportSemanticError(
					MessageFormat.format("The function must have two parameters for attribute `prototype({0})'' instead of {1}",
							prototype.getName(), formalParameterList.size()));
		}

		// checking the return type
		if (EncodingPrototype_type.FAST.equals(prototype)) {
			if (returnType != null) {
				returnType.getLocation().reportSemanticError(
						MessageFormat.format("The function cannot have return type for attribute `prototype({0})''",
								prototype.getName()));
			}
		} else {
			if (returnType == null) {
				location.reportSemanticError(MessageFormat.format(
						"The function must have a return type for attribute `prototype({0})''", prototype.getName()));
			} else {
				if (Assignment_type.A_FUNCTION_RTEMP.semanticallyEquals(assignmentType)) {
					returnType.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The function must return a value instead of a template for attribute `prototype({0})''",
									prototype.getName()));
				}

				if (EncodingPrototype_type.CONVERT.equals(prototype)) {
					outputType = returnType;
				} else {
					final IType last = returnType.getTypeRefdLast(timestamp);

					if (!last.getIsErroneous(timestamp) && !Type_type.TYPE_INTEGER.equals(last.getTypetypeTtcn3())) {
						returnType.getLocation()
						.reportSemanticError(
								MessageFormat.format(
										"The return type of the function must be `integer'' instead of `{0}'' for attribute `prototype({1})''",
										returnType.getTypename(), prototype.getName()));
					}
				}
			}
		}

		// checking the runs on clause
		if (runsOnType != null && runsOnRef != null) {
			runsOnRef.getLocation().reportSemanticError(
					MessageFormat.format("The function cannot have `runs on'' clause for attribute `prototype({0})''",
							prototype.getName()));
		}
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					return;
				}
			}
		}

		super.postCheck();
		formalParameterList.postCheck();
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		final StringBuilder text = new StringBuilder(identifier.getDisplayName());
		if (formalParameterList == null || lastTimeChecked == null) {
			return text.toString();
		}

		text.append('(');
		for (int i = 0; i < formalParameterList.size(); i++) {
			if (i != 0) {
				text.append(", ");
			}
			final FormalParameter parameter = formalParameterList.get(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				text.append("timer");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					text.append("Unknown type");
				} else {
					text.append(type.getTypename());
				}
			}
		}
		text.append(')');
		return text.toString();
	}

	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		if (Configuration.INSTANCE.getBoolean(Configuration.OUTLINE_SHOW_FUNCTIONS, true)) {
			return SymbolKind.Function;
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return location.getRange();
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() {
		return identifier.getLocation().getRange();
	}

	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		if (Configuration.INSTANCE.getBoolean(Configuration.OUTLINE_SHOW_FUNCTIONS, true)) {
			final DocumentSymbol symbol = new DocumentSymbol(getOutlineText(), getOutlineSymbolKind(),
				getOutlineRange(), getOutlineSelectionRange());
			symbol.setChildren(getOutlineSymbolChildren());
			
			return symbol;
		}
		return null;
	}

	public String getProposalText() {
		StringBuilder styled = new StringBuilder(identifier.getDisplayName());
		if (formalParameterList == null || lastTimeChecked == null) {
			return styled.toString();
		}

		styled.append(" (");
		for (int i = 0; i < formalParameterList.size(); i++) {
			if (i != 0) {
				styled.append(",");
			}
			styled.append("<br>");
			final FormalParameter parameter = formalParameterList.get(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				styled.append("<pre>    timer</pre>");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					styled.append("<pre>    Unknown type</pre>");
				} else {
					styled.append("<pre>    " + type.getTypename() + "</pre>");
					styled.append(' ');
					styled.append(parameter.getGenName());
				}
			}
		}
		styled.append("<br>)");
		return styled.toString();
	}

	@Override
	/** {@inheritDoc} */
	public TemplateRestriction.Restriction_type getTemplateRestriction() {
		return templateRestriction;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;

			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					removeBridge();
					throw new ReParseException(result);
				}
			}
			if (formalParameterList != null) {
				if (enveloped) {
					formalParameterList.updateSyntax(reparser, false);
					reparser.updateLocation(formalParameterList.getLocation());
				} else if (reparser.envelopsDamage(formalParameterList.getLocation())) {
					try {
						formalParameterList.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(formalParameterList.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}
			if (runsOnRef != null) {
				if (enveloped) {
					runsOnRef.updateSyntax(reparser, false);
					reparser.updateLocation(runsOnRef.getLocation());
				} else if (reparser.envelopsDamage(runsOnRef.getLocation())) {
					try {
						runsOnRef.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(runsOnRef.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}
			if (mtcReference != null) {
				if (enveloped) {
					mtcReference.updateSyntax(reparser, false);
					reparser.updateLocation(mtcReference.getLocation());
				} else if (reparser.envelopsDamage(mtcReference.getLocation())) {
					try {
						mtcReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(mtcReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}
			if (systemReference != null) {
				if (enveloped) {
					systemReference.updateSyntax(reparser, false);
					reparser.updateLocation(systemReference.getLocation());
				} else if (reparser.envelopsDamage(systemReference.getLocation())) {
					try {
						systemReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(systemReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}
			if (portReference != null) {
				if (enveloped) {
					portReference.updateSyntax(reparser, false);
					reparser.updateLocation(portReference.getLocation());
				} else if (reparser.envelopsDamage(portReference.getLocation())) {
					try {
						portReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(portReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}
			if (returnType != null) {
				if (enveloped) {
					returnType.updateSyntax(reparser, false);
					reparser.updateLocation(returnType.getLocation());
				} else if (reparser.envelopsDamage(returnType.getLocation())) {
					try {
						returnType.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(returnType.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}
			if (statementBlock != null) {
				if (enveloped) {
					statementBlock.updateSyntax(reparser, false);
					reparser.updateLocation(statementBlock.getLocation());
				} else if (reparser.envelopsDamage(statementBlock.getLocation())) {
					try {
						statementBlock.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(statementBlock.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}
			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					try {
						withAttributesPath.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(withAttributesPath.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (!enveloped) {
				removeBridge();
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (formalParameterList != null) {
			formalParameterList.updateSyntax(reparser, false);
			reparser.updateLocation(formalParameterList.getLocation());
		}
		if (runsOnRef != null) {
			runsOnRef.updateSyntax(reparser, false);
			reparser.updateLocation(runsOnRef.getLocation());
		}
		if (mtcReference != null) {
			mtcReference.updateSyntax(reparser, false);
			reparser.updateLocation(mtcReference.getLocation());
		}
		if (systemReference != null) {
			systemReference.updateSyntax(reparser, false);
			reparser.updateLocation(systemReference.getLocation());
		}
		if (portReference != null) {
			portReference.updateSyntax(reparser, false);
			reparser.updateLocation(portReference.getLocation());
		}
		if (returnType != null) {
			if (! (identifier.getName().equals("create") && returnType instanceof Class_Type)) {
				returnType.updateSyntax(reparser, false);
				reparser.updateLocation(returnType.getLocation());
			}
		}
		if (statementBlock != null) {
			statementBlock.updateSyntax(reparser, false);
			reparser.updateLocation(statementBlock.getLocation());
		}
		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (formalParameterList != null) {
			formalParameterList.findReferences(referenceFinder, foundIdentifiers);
		}
		if (returnType != null) {
			returnType.findReferences(referenceFinder, foundIdentifiers);
		}
		if (runsOnRef != null) {
			runsOnRef.findReferences(referenceFinder, foundIdentifiers);
		}
		if (mtcReference != null) {
			mtcReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (systemReference != null) {
			systemReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (portReference != null) {
			portReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (statementBlock != null) {
			statementBlock.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (formalParameterList != null && !formalParameterList.accept(v)) {
			return false;
		}
		if (returnType != null) {
			if (returnType instanceof Class_Type) {
				if (!isConstructor && !returnType.accept(v)) {
					return false;
				}
			} else if (!returnType.accept(v)) {
				return false;
			}
		}
		if (runsOnRef != null && !runsOnRef.accept(v)) {
			return false;
		}
		if (mtcReference != null && !mtcReference.accept(v)) {
			return false;
		}
		if (systemReference != null && !systemReference.accept(v)) {
			return false;
		}
		if (portReference != null && !portReference.accept(v)) {
			return false;
		}
		if (statementBlock != null && !statementBlock.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}

		hoverContent.addStyledText(KIND, Ttcn3HoverContent.BOLD).addText(" ");

		if (isDeterministic) hoverContent.addStyledText(" @deterministic", Ttcn3HoverContent.BOLD);
		if (isControl) hoverContent.addStyledText(" @control", Ttcn3HoverContent.ITALIC);

		hoverContent
			.addText("\n\n")
			.addText(getFullName())
			.addText("\n\n");

		if (runsOnRef != null) {
			hoverContent.addText(" runs on ").addStyledText(runsOnRef.getDisplayName(), Ttcn3HoverContent.BOLD).addText("\n\n");
		}
		if (portReference != null) {
			hoverContent.addText(" port ").addStyledText(portReference.getDisplayName(), Ttcn3HoverContent.BOLD).addText("\n\n");
		}
		if (mtcReference != null) {
			hoverContent.addText(" mtc ").addStyledText(mtcReference.getDisplayName(), Ttcn3HoverContent.BOLD).addText("\n\n");
		}
		if (systemReference != null) {
			hoverContent.addText(" system ").addStyledText(systemReference.getDisplayName(), Ttcn3HoverContent.BOLD).addText("\n\n");
		}
		if (returnType != null) {
			hoverContent.addText(" return ").addStyledText(returnType.getTypename(), Ttcn3HoverContent.BOLD).addText("\n\n");
		}
		hoverContent.addText("\n\n");
		getHoverContentFromComment(hoverContent, dc, formalParameterList);

		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		return generateCommonDocComment(indentation, formalParameterList, returnType);
	}

	public boolean isOverride() {
		return isOverride;
	}

	public void setOverride(boolean isOverride) {
		this.isOverride = isOverride;
	}

	@Override
	public IsIdenticalResult isIdentical(CompilationTimeStamp timestamp, Def_FunctionBase other) {
		Def_FunctionBase otherf = null;
		SignatureExceptions otherExceptions = null;
		if (other instanceof Def_Function) {
			otherf = other;
			otherExceptions = ((Def_Function)other).exceptions;
		} else if (other instanceof Def_AbsFunction) {
			otherf = other;
		}

		if (otherf == null) {
			return IsIdenticalResult.RES_DIFFERS;
		}

		final Assignment_type asstype = getAssignmentType();
		final Assignment_type asstype2 = otherf.getAssignmentType();

		if (getAssignmentType() != asstype2) {
			if ((asstype == Assignment_type.A_FUNCTION && asstype2 != Assignment_type.A_EXT_FUNCTION) ||
				(asstype == Assignment_type.A_EXT_FUNCTION && asstype2 != Assignment_type.A_FUNCTION) ||
				(asstype == Assignment_type.A_FUNCTION_RVAL && asstype2 != Assignment_type.A_EXT_FUNCTION_RVAL) ||
				(asstype == Assignment_type.A_EXT_FUNCTION_RVAL && asstype2 != Assignment_type.A_FUNCTION_RVAL) ||
				(asstype == Assignment_type.A_FUNCTION_RTEMP && asstype2 != Assignment_type.A_EXT_FUNCTION_RTEMP) ||
				(asstype == Assignment_type.A_EXT_FUNCTION_RTEMP && asstype2 != Assignment_type.A_FUNCTION_RTEMP)) {
				return IsIdenticalResult.RES_DIFFERS;
			}
		}
		// FIXME :: this causes a SO
//		if (returnType != null && ! otherf.returnType.isIdentical(timestamp, returnType)) {
//			return IsIdenticalResult.RES_DIFFERS;
//		}
		final FormalParameterList otherFpl = otherf.getFormalParameterList();
		IsIdenticalResult identical = getFormalParameterList().isIdentical(timestamp, otherFpl);
		if (identical == IsIdenticalResult.RES_DIFFERS) {
			return IsIdenticalResult.RES_DIFFERS;
		}
		if (exceptions != null || otherExceptions != null) {
			if (exceptions == null || otherExceptions == null) {
				return IsIdenticalResult.RES_DIFFERS;
			}
			if (exceptions.size() != otherExceptions.size()) {
				return IsIdenticalResult.RES_DIFFERS;
			}
			for (int i = 0; i < exceptions.size(); i++) {
				if (otherExceptions.contains(timestamp, exceptions.get(i))) {
					return IsIdenticalResult.RES_DIFFERS;
				}
			}
		}
		return identical;
	}

	private final String getMissingReturnData(final CompilationTimeStamp timestamp) {
		String retval = Type.typeToSrcTypeDefault(getType(timestamp).getTypetypeTtcn3());
		final CodeActionHelpers helper = new CodeActionHelpers();
		final StringBuilder sb = new StringBuilder();
		final String lineEnding = ProjectItem.getLineEnding(location);
		sb.append(lineEnding);
		helper.addExtraIndentedText(sb, "return ");
		sb.append(retval)
			.append(';')
			.append(lineEnding);
		Location insertLocation = statementBlock.getLocation();
		if (insertLocation == null || insertLocation == NULL_Location.INSTANCE) {
			return null;
		}
		insertLocation.setEndColumn(insertLocation.getEndColumn() - 1);
		insertLocation.setStartPosition(location.getStartPosition());

//		final int nrOfStatements = statementBlock.getSize();
//		if (nrOfStatements != 0) {
//			insertLocation = statementBlock.getLocation();
//			insertLocation.setEndColumn(insertLocation.getEndColumn() - 1);
//			insertLocation.setStartPosition(location.getStartPosition());
//		}

		final MissingReturnData returnData = new MissingReturnData(sb.toString(), insertLocation, getLocation()); 
		GsonBuilder builder = new GsonBuilder();
		CodeActionHelpers.registerTypeAdapters(builder); 
		Gson gson = builder.create();
		return gson.toJson(returnData);
	}

	@Override 
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(identifier.getLocation(), SemanticType.Function);
		super.setSemanticInformation();
	}
}
