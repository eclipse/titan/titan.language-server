/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * This interface represents a parameterized assignment. One that has formal parameters.
 *
 * @author Kristof Szabados
 * */
public interface IParameterisedAssignment {

	/**
	 * @return The list of formal parameters the assignment has
	 */
	FormalParameterList getFormalParameterList();

	/**
	 * Adds the list of formal parameters to hover content as bullet points
	 */
	default void addFormalParameterListToHoverContent(final Ttcn3HoverContent hoverContent, final CompilationTimeStamp lastTimeChecked) {
		final FormalParameterList formalParameterList = getFormalParameterList();
		if (formalParameterList != null && formalParameterList.size() > 0) {
			hoverContent.addStyledText("Parameters", Ttcn3HoverContent.BOLD).addText("\n\n");
			
			for (final FormalParameter param : formalParameterList) {
				String paramType = param.getFormalParamType();
				final StringBuilder sb = new StringBuilder(param.getIdentifier().getDisplayName());
				sb.append(" (").append(paramType != null ? paramType : "<?>").append(")");

				final Type argType = param.getType(lastTimeChecked);
				String argTypeName = argType != null ? argType.getTypename() : "<?>";
				if (argType instanceof Referenced_Type) {
					argTypeName = ((Referenced_Type) argType).getReference().getDisplayName();
				}
				sb.append(" ").append(argTypeName);
				hoverContent.addText("- ").addIndentedText(sb.toString(), "\n");
			}
		}
	}
}
