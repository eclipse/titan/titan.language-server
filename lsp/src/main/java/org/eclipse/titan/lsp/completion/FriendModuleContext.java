/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.completion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.InsertReplaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FriendModule;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.core.Range;

public class FriendModuleContext extends CompletionContext {

	public FriendModuleContext(CompletionContextInfo contextInfo) {
		super(contextInfo);
	}

	private List<String> parseFriendModules() {
		List<String> friendModuleList = new ArrayList<>();
		final String friendModules = contextInfo.matcher.group(1);
		if (friendModules != null) {
			friendModuleList = new ArrayList<>(List.of(friendModules.split(",")))
					.stream()
					.map(mName -> mName.replaceAll("\\s", ""))
					.collect(Collectors.toList());
		}
		return friendModuleList;
	}

	private boolean isProperProjectItem(ProjectItem projectItem, TTCN3Module actualModule) {
		if (projectItem.isExcluded()) {
			return false;
		}
		final Module importableModule = projectItem.getModule();
		if (importableModule == null || importableModule == actualModule) {
			return false;
		}

		if (actualModule.getImportedModules().contains(importableModule)) {
			return false;
		}
		
		return true;
	}

	private List<CompletionItem> collectCompletionItems(TTCN3Module actualModule) {
		final Project project = actualModule.getProject();
		final List<CompletionItem> completions = new ArrayList<>();
		final List<FriendModule> parsedFriendModules = actualModule.getFriendModules();
		final List<String> friendModuleList = parseFriendModules();
	
		project.getAllProjectItems().entrySet().forEach(entry -> {
			final ProjectItem projectItem = entry.getValue();
			if (!isProperProjectItem(projectItem, actualModule)) {
				return;
			}

			final String importableModuleName = projectItem.getModule().getIdentifier().getDisplayName();
			if (friendModuleList.contains(importableModuleName)) {
				return;
			}

			FriendModule onParsedFriendModule = null;
			for (FriendModule friend : parsedFriendModules) {
				if (friend.getIdentifier().getDisplayName().equals(importableModuleName)) {
					return;
				}

				if (friend.getLocation().containsPosition(contextInfo.cursorPosition)) {
					onParsedFriendModule = friend;
				}
			}

			Range replacementRange = null;
			if (onParsedFriendModule != null) {
				replacementRange = onParsedFriendModule.getIdentifier().getLocation().getRange();
			} else if (!friendModuleList.isEmpty()) {
				final Position replacementStart = new Position(
						contextInfo.cursorPosition.getLine(),
						contextInfo.cursorPosition.getCharacter() - friendModuleList.get(friendModuleList.size() - 1).length()
					);
				replacementRange = new Range(replacementStart, contextInfo.cursorPosition);
			}

			final CompletionItem completionItem = new CompletionItem();
			completionItem.setLabel(importableModuleName);
			completionItem.setInsertText(importableModuleName);
			completionItem.setKind(CompletionItemKind.Module);

			if (replacementRange != null) {
				final InsertReplaceEdit insRepEdit = new InsertReplaceEdit();
				insRepEdit.setInsert(replacementRange);
				insRepEdit.setReplace(replacementRange);
				insRepEdit.setNewText(completionItem.getInsertText());
				completionItem.setTextEdit(Either.forRight(insRepEdit));
			}

			completions.add(completionItem);
		});

		return completions;
	}

	@Override
	public Either<List<CompletionItem>, CompletionList> getCompletions() {
		final Module module = contextInfo.module;
		if (module == null) {
			return null;
		}

		if (module instanceof TTCN3Module) {
			final List<CompletionItem> completionList = collectCompletionItems((TTCN3Module)module);
			return Either.forLeft(completionList);
		}

		return null;
	}
}
