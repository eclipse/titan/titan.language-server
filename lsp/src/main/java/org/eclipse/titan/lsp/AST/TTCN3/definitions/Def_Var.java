/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.DocumentComment;
import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.IType.ValueCheckingOptions;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.NamingConventionHelper;
import org.eclipse.titan.lsp.AST.NamingConventionHelper.NamingConventionElement;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.MultipleWithAttributes;
import org.eclipse.titan.lsp.AST.TTCN3.attributes.WithAttributesPath;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.FormalParameter.parameterEvaluationType;
import org.eclipse.titan.lsp.AST.TTCN3.types.AbstractOfType;
import org.eclipse.titan.lsp.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.lsp.AST.TTCN3.types.ComponentTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.hover.HoverContentType;
import org.eclipse.titan.lsp.hover.Ttcn3HoverContent;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * The Def_Var class represents TTCN3 variable definitions.
 *
 * @author Kristof Szabados
 * @author Arpad Lovassy
 */
public class Def_Var extends Definition {
	private static final String FULLNAMEPART1 = ".<type>";
	private static final String FULLNAMEPART2 = ".<initial_value>";
	private static final String PORTNOTALLOWED = "Variable can not be defined for port type `{0}''";
	private static final String SIGNATURENOTALLOWED = "Variable can not be defined for signature `{0}''";

	private static final String KIND = " variable definition";

	protected final Type type;
	private Value initialValue;

	private boolean wasAssigned;

	/**
	 * normal, lazy or fuzzy evaluation should be used.
	 */
	private final parameterEvaluationType evaluationType;

	public Def_Var( final Identifier identifier, final Type type, final Value initialValue, final parameterEvaluationType evaluationType ) {
		super(identifier);
		this.type = type;
		this.initialValue = initialValue;
		this.evaluationType = evaluationType;

		if (type != null) {
			type.setOwnertype(TypeOwner_type.OT_VAR_DEF, this);
			type.setFullNameParent(this);
		}
		if (initialValue != null) {
			initialValue.setFullNameParent(this);
		}
	}

	@Override
	/** {@inheritDoc} */
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_VAR;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (type == child) {
			return builder.append(FULLNAMEPART1);
		} else if (initialValue == child) {
			return builder.append(FULLNAMEPART2);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (type != null) {
			type.setMyScope(scope);
		}
		if (initialValue != null) {
			initialValue.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return "variable";
	}

	@Override
	/** {@inheritDoc} */
	public String getDescription() {
		final StringBuilder builder = new StringBuilder();
		builder.append(getAssignmentName()).append(" `");

		if (isLocal()) {
			builder.append(identifier.getDisplayName());
		} else {
			builder.append(getFullName());
		}

		builder.append('\'');
		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public SymbolKind getOutlineSymbolKind() {
		return SymbolKind.Variable;
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineRange() {
		return location.getRange();
	}

	@Override
	/** {@inheritDoc} */
	public Range getOutlineSelectionRange() {
		return identifier.getLocation().getRange();
	}

	@Override
	/** {@inheritDoc} */
	public DocumentSymbol getOutlineSymbol() {
		final DocumentSymbol symbol = new DocumentSymbol(identifier.getDisplayName(), getOutlineSymbolKind(),
			getOutlineRange(), getOutlineSelectionRange());
		symbol.setChildren(getOutlineSymbolChildren());
		
		return symbol;
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		int result = super.category();
		if (type != null) {
			result += type.category();
		}
		return result;
	}

	@Override
	/** {@inheritDoc} */
	public Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return type;
	}

	public Value getInitialValue() {
		return initialValue;
	}

	@Override
	/** {@inheritDoc} */
	public void setWithAttributes(final MultipleWithAttributes attributes) {
		// variable should not have with attributes
	}

	@Override
	/** {@inheritDoc} */
	public void setAttributeParentPath(final WithAttributesPath parent) {
		// variable should not have with attributes
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;

		wasAssigned = false;

		if (! (getMyScope() instanceof ClassTypeBody)) {
			if (getMyScope() instanceof ComponentTypeBody) {
				NamingConventionHelper.checkConvention(NamingConventionElement.ComponentVariable, identifier, this);
			} else {
				NamingConventionHelper.checkConvention(NamingConventionElement.LocalVariable, identifier, this);
			}
			NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());
		} else {
			NamingConventionHelper.checkConvention(NamingConventionElement.LocalVariable, identifier, this);
		}

		if (type == null) {
			return;
		}

		type.check(timestamp);

		final IType lastType = type.getTypeRefdLast(timestamp);
		switch (lastType.getTypetype()) {
		case TYPE_PORT:
			location.reportSemanticError(MessageFormat.format(PORTNOTALLOWED, lastType.getFullName()));
			break;
		case TYPE_SIGNATURE:
			location.reportSemanticError(MessageFormat.format(SIGNATURENOTALLOWED, lastType.getFullName()));
			break;
		default:
			break;
		}

		if (initialValue != null) {
			initialValue.setMyGovernor(type);
			final IValue temporalValue = type.checkThisValueRef(timestamp, initialValue);
			if (isLocal()) {
				type.checkThisValue(timestamp, temporalValue, this, new ValueCheckingOptions(Expected_Value_type.EXPECTED_DYNAMIC_VALUE,
						true, false, true, false, false));
			} else {
				type.checkThisValue(timestamp, temporalValue, this, new ValueCheckingOptions(Expected_Value_type.EXPECTED_STATIC_VALUE,
						true, false, true, false, false));
			}
		}
	}

	/**
	 * Indicates that this variable was used in a way where its value can be
	 * changed.
	 * */
	public void setWritten() {
		wasAssigned = true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkIdentical(final CompilationTimeStamp timestamp, final Definition definition) {
		check(timestamp);
		definition.check(timestamp);

		if (!Assignment_type.A_VAR.semanticallyEquals(definition.getAssignmentType())) {
			location.reportSemanticError(MessageFormat.format(
					"Local definition `{0}'' is a variable, but the definition inherited from component type `{1}'' is a {2}",
					identifier.getDisplayName(), definition.getMyScope().getFullName(), definition.getAssignmentName()));
			return false;
		}

		final Def_Var otherVariable = (Def_Var) definition;
		if (!type.isIdentical(timestamp, otherVariable.type)) {
			final String message = MessageFormat
					.format("Local variable `{0}'' has type `{1}'', but the variable inherited from component type `{2}'' has type `{3}''",
							identifier.getDisplayName(), type.getTypename(), otherVariable.getMyScope().getFullName(),
							otherVariable.type.getTypename());
			type.getLocation().reportSemanticError(message);
			return false;
		}

		if (initialValue != null) {
			if (otherVariable.initialValue != null) {
				if (!initialValue.isUnfoldable(timestamp) && !otherVariable.initialValue.isUnfoldable(timestamp)
						&& !initialValue.checkEquality(timestamp, otherVariable.initialValue)) {
					final String message = MessageFormat
							.format("Local variable `{0}'' and the variable inherited from component type `{1}'' have different values",
									identifier.getDisplayName(), otherVariable.getMyScope().getFullName());
					initialValue.getLocation().reportSemanticWarning(message);
				}
			} else {
				initialValue.getLocation()
				.reportSemanticWarning(
						MessageFormat.format(
								"Local variable `{0}'' has initial value, but the variable inherited from component type `{1}'' does not",
								identifier.getDisplayName(), otherVariable.getMyScope().getFullName()));
			}
		} else if (otherVariable.initialValue != null) {
			location.reportSemanticWarning(MessageFormat
					.format("Local variable `{0}'' does not have initial value, but the variable inherited from component type `{1}'' has",
							identifier.getDisplayName(), otherVariable.getMyScope().getFullName()));
		}

		return true;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		final StringBuilder builder = new StringBuilder();
		if (type != null) {
			type.getProposalDescription(builder);
		}
		builder.append(KIND);
		return builder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		final List<Integer> result = super.getPossibleExtensionStarterTokens();

		if (initialValue == null) {
			result.add(Ttcn3Lexer.ASSIGNMENTCHAR);
		}

		return result;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean enveloped = false;
			int result = 1;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					throw new ReParseException(result);
				}
			}

			if (type != null) {
				if (enveloped) {
					type.updateSyntax(reparser, false);
					reparser.updateLocation(type.getLocation());
				} else if (reparser.envelopsDamage(type.getLocation())) {
					type.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(type.getLocation());
				}
			}

			if (initialValue != null) {
				if (enveloped) {
					initialValue.updateSyntax(reparser, false);
					reparser.updateLocation(initialValue.getLocation());
				} else if (reparser.envelopsDamage(initialValue.getLocation())) {
					initialValue.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(initialValue.getLocation());
				}
			}

			if (!enveloped) {
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());
		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}

		if (initialValue != null) {
			initialValue.updateSyntax(reparser, false);
			reparser.updateLocation(initialValue.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (type != null) {
			type.findReferences(referenceFinder, foundIdentifiers);
		}
		if (initialValue != null) {
			initialValue.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (type != null && !type.accept(v)) {
			return false;
		}
		if (initialValue != null && !initialValue.accept(v)) {
			return false;
		}
		return true;
	}

	public boolean getWritten() {
		return wasAssigned;
	}

	/**
	 * @return how this variable should be evaluated.
	 */
	public parameterEvaluationType get_eval_type() {
		return evaluationType;
	}
	
	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent() {
		super.getHoverContent();

		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = getDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}

		IType assType = getType(getLastTimeChecked());
		if (assType != null) {
			String typeName = assType.getTypename();
			String typeRestrictions = ((Type)assType).getRestrictionDesc();

			if (assType instanceof Referenced_Type) {
				typeName = ((Referenced_Type) assType).getReference().getDisplayName();
				assType = assType.getTypeRefdLast(lastTimeChecked);
			}

			final String propDesc = ((Type)assType).getProposalDescription(new StringBuilder()).toString();

			hoverContent.addStyledText("var", Ttcn3HoverContent.BOLD).addText(" ");
			hoverContent.addStyledText(typeName);
			hoverContent.addText(" - ").addStyledText(propDesc);
			if (typeRestrictions != null) {
				hoverContent.addText("\n\n")
				.addStyledText("Restrictions:", Ttcn3HoverContent.ITALIC)
				.addText("\n\n")
				.addText(typeRestrictions);
			}
			hoverContent.addText("\n\n");
		}

		if (assType instanceof Class_Type) {
			((Class_Type)assType).addClassMembers(this, hoverContent, dc);
		}

		if (assType instanceof IMemberInfo) {
			final IMemberInfo mInfo = (IMemberInfo)assType;
			mInfo.addMembersContent(hoverContent);
		}

		if (assType instanceof AbstractOfType) {
			final IType ofType = ((AbstractOfType)assType).getOfType();
			final IType elementType = ((Type)ofType).getTypeRefdLast(lastTimeChecked);
			hoverContent
				.addText("\n\n")
				.addStyledText("Element:", Ttcn3HoverContent.BOLD)
				.addText("\n\n")
				.addText(((Type)elementType).getTypename())
				.addText("\n\n");

			if (elementType instanceof IMemberInfo) {
				((IMemberInfo)elementType).addMembersContent(hoverContent);
			}
		}

		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		}
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation, final String lineEnding) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append(lineEnding)
			.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}

	public Value stealValue() {
		final Value retval = initialValue;
		initialValue = null;
		return retval;
	}
	
	@Override
	public CompletionItem getCompletionItem() {
		final CompletionItem item = super.getCompletionItem();
		item.setKind(CompletionItemKind.Variable);
		return item;
	}
	
	@Override
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(getIdentifier().getLocation(), SemanticType.Variable);
		super.setSemanticInformation();
	}
}
