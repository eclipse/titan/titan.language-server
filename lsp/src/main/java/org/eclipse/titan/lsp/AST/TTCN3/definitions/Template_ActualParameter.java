/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.ISubReference;
import org.eclipse.titan.lsp.AST.ParameterisedSubReference;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.lsp.AST.TTCN3.TemplateRestriction.Restriction_type;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents an actual parameter that has a Template as its actual value.
 *
 * @author Kristof Szabados
 * */
public final class Template_ActualParameter extends ActualParameter {

	private final TemplateInstance template;
	private TemplateRestriction.Restriction_type genRestrictionCheck = Restriction_type.TR_NONE;

	public Template_ActualParameter(final TemplateInstance template) {
		this.template = template;
	}

	public TemplateInstance getTemplateInstance() {
		return template;
	}

	//FIXME needs to be called from the right places for the code generation to work
	public void setGenRestrictionCheck(final TemplateRestriction.Restriction_type tr) {
		genRestrictionCheck = tr;
	}

	public TemplateRestriction.Restriction_type getGenRestrictionCheck() {
		return genRestrictionCheck;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (template != null) {
			template.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (template == null) {
			return;
		}

		final Reference derivedReference = template.getDerivedReference();
		if (derivedReference != null) {
			final ISubReference subReference = derivedReference.getSubreferences().get(0);
			if (subReference instanceof ParameterisedSubReference) {
				final ActualParameterList parameterList = ((ParameterisedSubReference) subReference).getActualParameters();
				if (parameterList != null) {
					parameterList.checkRecursions(timestamp, referenceChain);
				}
			}
		}

		referenceChain.markState();
		template.getTemplateBody().checkRecursions(timestamp, referenceChain);
		referenceChain.previousState();
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);

		if (template != null) {
			template.updateSyntax(reparser, false);
			reparser.updateLocation(template.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (template != null) {
			if (!template.accept(v)) {
				return false;
			}
		}
		return true;
	}
}
