package org.eclipse.titan.lsp.parsers;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.titan.lsp.AST.ASN1.Ass_pard;
import org.eclipse.titan.lsp.AST.ASN1.definitions.ASN1Module;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.lsp.AST.brokenpartsanalyzers.BrokenPartsChecker;
import org.eclipse.titan.lsp.AST.brokenpartsanalyzers.BrokenPartsViaReferences;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.MarkerHandler;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ModuleImportationChain;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;

/**
 * Helper class to separate the responsibility of the source parser into smaller
 * parts. This class is responsible for handling the semantic checking of the
 * source code of the projects
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 **/
public class ProjectSourceSemanticAnalyzer {
	private static final String DUPLICATEMODULE = "Modules must be unique, but `{0}'' was declared multiple times";
	public static final String CIRCULARIMPORTCHAIN = "Circular import chain is not recommended: {0}";

	/**
	 * file to Module map received from the syntax analysis.
	 * Serves as the main/reliable source of information.
	 * */
	private final Map<Path, Module> fileModuleMap;

	/**
	 * module name to module mapping to speed up searching in the list of uptodate modules.
	 *
	 * Calculated during semantic check, maintained in-between semantic checks.
	 * */
	private final Map<String, Module> moduleMap;

	/**
	 * module name to module mapping to speed up searching in the list of outdated modules.
	 *
	 * Cleared during semantic check, maintained in-between semantic checks.
	 * */
	private final Map<String, Module> outdatedModuleMap;

	/**
	 * The names of the modules, which were checked at the last semantic check.
	 *
	 * Caculated during the semantic check, maintained in-between semantic checks.
	 * */
	private final Set<String> semanticallyUptodateModules;

	/** List of files analyzed during the semantic analysis */ 
	private List<Path> analyzedFiles;

	public ProjectSourceSemanticAnalyzer() {
		fileModuleMap = new ConcurrentHashMap<Path, Module>();
		moduleMap = new ConcurrentHashMap<String, Module>();
		outdatedModuleMap = new ConcurrentHashMap<String, Module>();
		semanticallyUptodateModules = new HashSet<String>();
		analyzedFiles = new ArrayList<>();
	}

	public void reset() {
		fileModuleMap.clear();
		moduleMap.clear();
		outdatedModuleMap.clear();
		semanticallyUptodateModules.clear();
		analyzedFiles.clear();
	}

	/**
	 * Checks whether the internal data belonging to the provided file is
	 * semantically out-dated.
	 *
	 * @param file
	 *            the file to check.
	 *
	 * @return true if the data was reported to be out-dated since the last
	 *         analysis.
	 * */
	public boolean isOutdated(final Path file) {
		final Module module = fileModuleMap.get(file);

		return module == null || !semanticallyUptodateModules.contains(module.getName());
	}

	/**
	 * Returns the module with the provided name, or null.
	 *
	 * @param name
	 *            the name of the module to return.
	 * @param uptodateOnly
	 *            allow finding only the up-to-date modules.
	 *
	 * @return the module having the provided name
	 * */
	Module internalGetModuleByName(final String name, final boolean uptodateOnly) {
		if (moduleMap.containsKey(name)) {
			return moduleMap.get(name);
		}

		if (!uptodateOnly && outdatedModuleMap.containsKey(name)) {
			return outdatedModuleMap.get(name);
		}

		return null;
	}

	/**
	 * Returns the actually known module's names.
	 *
	 * @return a set of the module names known in this project or in the
	 *         ones referenced.
	 * */
	Set<String> internalGetKnownModuleNames() {
		final Set<String> temp = new HashSet<String>();
		temp.addAll(moduleMap.keySet());
		return temp;
	}

	Collection<Module> internalGetModules() {
		return moduleMap.values();
	}

	Module getModulebyFile(final Path file) {
		return fileModuleMap.get(file);
	}

	/**
	 * Reports that the provided file has changed and so it's stored
	 * information became out of date.
	 * <p>
	 * Stores that this file is out of date for later
	 * <p>
	 *
	 * <p>
	 * Files which are excluded from the build should not be reported.
	 * </p>
	 *
	 * @param outdatedFile
	 *            the file which seems to have changed
	 * */
	public void reportOutdating(final Path outdatedFile) {
		final Module module = fileModuleMap.get(outdatedFile);
		if (module == null) {
			return;
		}

		final Path moduleFile = module.getIdentifier().getLocation().getFile().toPath();
		if (!outdatedFile.equals(moduleFile)) {
			return;
		}

		final String moduleName = module.getName();
		moduleMap.remove(moduleName);
		fileModuleMap.remove(moduleFile);

		synchronized (outdatedModuleMap) {
			outdatedModuleMap.put(moduleName, module);
		}

		synchronized (semanticallyUptodateModules) {
			semanticallyUptodateModules.remove(moduleName);
		}
	}

	/**
	 * Reports that the semantic meaning of the provided file might have
	 * changed and so it's stored information became out of date.
	 * <p>
	 * Stores that this file is semantically out of date for later
	 * <p>
	 *
	 * @param outdatedFile
	 *            the file which seems to have changed
	 * */
	public void reportSemanticOutdating(final Path outdatedFile) {
		final Module module = fileModuleMap.get(outdatedFile);
		if (module != null) {
			synchronized (semanticallyUptodateModules) {
				semanticallyUptodateModules.remove(module.getName());
			}
		}
	}

	/**
	 * Force the next semantic analysis to reanalyze everything.
	 * */
	void clearSemanticInformation() {
		synchronized (semanticallyUptodateModules) {
			semanticallyUptodateModules.clear();
		}
	}

	/**
	 * Adds a module to the set of semantically analyzed modules.
	 *
	 * @param module
	 *            the module to be added.
	 * @return true if it was successfully added, false otherwise.
	 * */
	public void addModule(final Module module) {
		fileModuleMap.put(module.getLocation().getFile().toPath(), module);
	}

	/**
	 * Removes a module from the set of semantically analyzed modules.
	 *
	 * @param moduleName
	 *            the name of the module to be removed.
	 * */
	void removeModule(final String moduleName) {
		final Module module = internalGetModuleByName(moduleName, false);
		if (module == null) {
			return;
		}

		fileModuleMap.remove(module.getLocation().getFile().toPath());
		synchronized (outdatedModuleMap) {
			outdatedModuleMap.remove(moduleName);
		}

		moduleMap.remove(moduleName);
	}

	public static BrokenPartsViaReferences analyzeMultipleProjectsSemantically(final Project project, final CompilationTimeStamp compilationCounter) {
		final String progressToken = TitanLanguageServer.createProgress("Building project", 5L);
		final String progressLabel = "Semantic analysis";
		BrokenPartsViaReferences selectionMethod = null;

		TitanLanguageServer.beginProgress(progressToken, progressLabel, 0, "");

		final ProjectSourceSemanticAnalyzer semanticAnalyzer = GlobalParser.getProjectSourceParser(project).getSemanticAnalyzer();
		synchronized (semanticAnalyzer.outdatedModuleMap) {
			semanticAnalyzer.outdatedModuleMap.clear();
		}
		semanticAnalyzer.moduleMap.clear();

		try {

			// clean the instantiated parameterized assignments,
			// from their instances
			Ass_pard.resetAllInstanceCounters();

			//check for duplicated module names
			final HashMap<String, Module> uniqueModules = new HashMap<String, Module>();
			final Set<String> duplicatedModules = new HashSet<String>();

			// collect all modules and semantically checked modules to work on.
			final List<Module> allModules = new ArrayList<Module>();
			final List<String> semanticallyChecked = new ArrayList<String>();
			
			//remove module name duplication markers. It shall be done before starting the next for-loop!		
			for (final Module module: semanticAnalyzer.fileModuleMap.values()) {
				if(module instanceof TTCN3Module){
					MarkerHandler.removeMarkers(module.getIdentifier().getLocation(), true);
				}
			}
			
			for (final Module module: semanticAnalyzer.fileModuleMap.values()) {
				final String name = module.getIdentifier().getName();
				allModules.add(module);
				//ASN1 modules are not been analyzed incrementally, therefore their markers can be removed in one step:
				if (module instanceof ASN1Module){
					MarkerHandler.removeMarkers(module.getLocation(), true);
				}
				if (uniqueModules.containsKey(name)) {
					final Location location = uniqueModules.get(name).getIdentifier().getLocation();
					final Location location2 = module.getIdentifier().getLocation();
					location.reportSemanticError(MessageFormat.format(DUPLICATEMODULE, module.getIdentifier().getDisplayName()));
					location2.reportSemanticError(MessageFormat.format(DUPLICATEMODULE, module.getIdentifier().getDisplayName()));
					duplicatedModules.add(name);
					semanticAnalyzer.semanticallyUptodateModules.remove(name);
				} else {
					uniqueModules.put(name, module);
					semanticAnalyzer.moduleMap.put(name, module);
					if(semanticAnalyzer.semanticallyUptodateModules.contains(name)) {
						semanticallyChecked.add(name);
					}
				}
			}

			if (allModules.size() > semanticallyChecked.size()) {

				// check and build the import hierarchy of the modules
				final ModuleImportationChain referenceChain = new ModuleImportationChain(CIRCULARIMPORTCHAIN, false);

				// remove markers from import lines
				for (final Module module : allModules) {
					if (module instanceof TTCN3Module) {
						final ProjectItem item = Project.INSTANCE.getProjectItem(module.getLocation().getFile().toPath());
						if (item != null) {
							item.removeMarkers(true);
						}
					}
					// markers are removed in one step in ASN1 modules
				}

				for (final Module module : allModules) {
					module.checkImports(compilationCounter, referenceChain, new ArrayList<Module>());
					referenceChain.clear();
				}

				selectionMethod = new BrokenPartsViaReferences(compilationCounter);
				selectionMethod.setModules(allModules, semanticallyChecked);
				selectionMethod.execute();

				// FIXME : check OOM

				final BrokenPartsChecker brokenPartsChecker = new BrokenPartsChecker(compilationCounter, selectionMethod);
				brokenPartsChecker.doChecking();

				// re-enable the markers on the skipped modules.
				for (final Module module2 : selectionMethod.getModulesToSkip()) {
					MarkerHandler.restoreMarkers(module2.getLocation());
				}

				semanticAnalyzer.analyzedFiles.clear();

				selectionMethod.getModulesToCheck().stream().forEach(module -> {
					if (semanticAnalyzer.fileModuleMap.containsValue(module)) {
						semanticAnalyzer.fileModuleMap.entrySet().stream()
							.filter(f -> f.getValue().equals(module))
							.forEach(fe ->
								semanticAnalyzer.analyzedFiles.add(fe.getKey())
							);
					}
				});

				synchronized (semanticAnalyzer.semanticallyUptodateModules) {
					semanticAnalyzer.semanticallyUptodateModules.clear();
					semanticAnalyzer.semanticallyUptodateModules.addAll(semanticAnalyzer.moduleMap.keySet());
					for (final String name: duplicatedModules) {
						semanticAnalyzer.semanticallyUptodateModules.remove(name);
					}
				}
			}

			final ProjectSourceParser parser = GlobalParser.getProjectSourceParser(project);
			parser.setLastTimeChecked(compilationCounter);

		} catch (Exception e) {
			// This catch is extremely important, as it is supposed
			// to protect the project parser, from whatever might go
			// wrong inside the analysis.
			TitanLogger.logError(e);
		}
		TitanLanguageServer.endProgress(progressToken);
		return selectionMethod;
	}

	public List<Path> getAnalyzedFiles() {
		return analyzedFiles;
	}
}
