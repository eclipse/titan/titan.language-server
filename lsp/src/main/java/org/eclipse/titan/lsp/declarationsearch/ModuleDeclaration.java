/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.declarationsearch;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.lsp.AST.ICommentable;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;

/**
 * @author Szabolcs Beres
 * */
class ModuleDeclaration implements IDeclaration {
	private final Module module;

	public ModuleDeclaration(final Module module) {
		this.module = module;
	}

	@Override
	public List<Hit> getReferences(final Module module) {
		final List<Hit> result = new ArrayList<ReferenceFinder.Hit>();
		// TODO The reference search for modules is not implemented
		if (module == this.module) {
			result.add(new Hit(this.module.getIdentifier()));
		}
		return result;
	}

	@Override
	public Identifier getIdentifier() {
		return module.getIdentifier();
	}

	@Override
	public Location getLocation() {
		return module.getLocation();
	}

	@Override
	public ICommentable getCommentable() {
		return module;
	}
}
