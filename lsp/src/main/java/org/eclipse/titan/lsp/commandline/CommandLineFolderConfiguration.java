/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.commandline;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.json.Json;
import javax.json.stream.JsonParser;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.TitanLanguageServer;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.common.product.FolderConfiguration;
import org.eclipse.titan.lsp.lspextensions.FolderConfigParams.CodeSmells;
import org.eclipse.titan.lsp.lspextensions.FolderConfigParams.NamingConventions;

/**
 * Parse folder configuration from <code>.TITAN_config.json</code> when running in command line mode.
 * 
 * @author Miklos Magyari
 */
public class CommandLineFolderConfiguration {
	private Map<String,Object> preferences = new ConcurrentHashMap<>();
	
	public CommandLineFolderConfiguration(final String rootPath) {
		readFolderConfig(rootPath);
	}
	
	private void readFolderConfig(String rootPath) {
		final String configFile = rootPath + File.separator + GeneralConstants.FOLDER_CONFIG;
		final Path path = Path.of(configFile);
		try (Stream<String> lines = Files.lines(path)) {
			final String json = lines.collect(Collectors.joining("\n"));
			parseConfiguration(json);
			final FolderConfiguration config = TitanLanguageServer.getFolderConfiguration();
			
			config.setFolderNamingConventions((NamingConventions)getConfigSection(new NamingConventions(), "namingConventions"));
			config.setCodeSmellSeverities((CodeSmells)getConfigSection(new CodeSmells(), "codeSmells"));
		} catch (IOException e) {
			return;
		}
	}
	
	private Object getConfigSection(Object configObject, String configSection) {
		final List<Field> fields = Arrays.asList(configObject.getClass().getFields());
		for (final Field field : fields) {
			try {
				final String value = getString(configSection + "." + field.getName(), null); 
				if (value != null) {
					field.set(configObject, value);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return configObject;
	}
	
	private boolean parseConfiguration(final String json) {
		try {
			final Map<String,Object> entryMap = new HashMap<>();
			JsonParser parser = Json.createParser(new StringReader(json));
			Configuration.iterateJson(parser, "", entryMap);
			
			entryMap.entrySet().stream().forEach(entry -> {
				if (!preferences.containsKey(entry.getKey()) || !preferences.get(entry.getKey()).equals(entry.getValue())) {
					preferences.put(entry.getKey(), entry.getValue());
				}
			});
		} catch (Exception e) {
			TitanLogger.logError(e);
			return false;
		}

		return true;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		preferences.entrySet().stream()
			.sorted(Map.Entry.<String,Object>comparingByKey())
			.forEach(pref ->
				sb.append(pref.getKey()).append(": ").append(pref.getValue()).append('\n')
			);
		return sb.toString();
	}
	
	/**
	 * Gets a String value of the given preference.
	 * @param key
	 * @param defaultValue
	 * @return Value of the preference or the default value if the preference does not exist or not a string.
	 */
	private String getString(final String key, final String defaultValue) {
		final Object value = preferences.get(key);
		if (value instanceof String) {
			return (String)value;
		}
		return defaultValue;
	}
}
