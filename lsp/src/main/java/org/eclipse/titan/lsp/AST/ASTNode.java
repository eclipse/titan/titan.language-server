/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.compiler.ICancelBuild;
import org.eclipse.titan.lsp.semantichighlighting.ISemanticInformation;

/**
 * Base class for AST-classes.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 */
public abstract class ASTNode
	implements IASTNode, IIdentifierContainer, IVisitableNode, ISemanticInformation,
			   ICancelBuild, ICompilerInfo, ILocatableComment {

	/**
	 * Version of TTCN3 supported by the compiler, specified by the year of publication.<br>
	 * Default version is v4.3.1 (published in 2011).
	 **/
	private static Ttcn_version ttcnVersion;
	/** the scope of the declaration of this node. */
	protected Scope myScope;
	/** the naming parent of the node. */
	private INamedNode nameParent;

	static {
		ttcnVersion = Ttcn_version.createTtcnVersion(Configuration.INSTANCE.getString(Configuration.TTCN_VERSION, "2011"));

		Configuration.INSTANCE.addPreferenceChangedListener(Configuration.TTCN_VERSION, pref ->
			ttcnVersion = Ttcn_version.createTtcnVersion(Configuration.INSTANCE.getString(Configuration.TTCN_VERSION, "2011")) 
		);
	}

	@Override
	public Ttcn_version getTtcnVersion() {
		return ttcnVersion;
	}

	@Override
	public final void setFullNameParent(final INamedNode nameParent) {
		this.nameParent = nameParent;
	}

	@Override
	public INamedNode getNameParent() {
		return nameParent;
	}

	@Override
	public void setMyScope(final Scope scope) {
		myScope = scope;
	}

	@Override
	public final Scope getMyScope() {
		return myScope;
	}

	/**
	 * Called by accept(), AST objects have to call accept() of their members in this function
	 * @param v the visitor object
	 * @return false to abort, will be returned by accept()
	 */
	protected abstract boolean memberAccept(final ASTVisitor v);

	@Override
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		if (!memberAccept(v)) {
			return false;
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}
}
