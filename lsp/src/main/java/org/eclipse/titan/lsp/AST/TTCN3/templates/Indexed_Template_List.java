/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IValue.Value_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceChain;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.Array_Type;
import org.eclipse.titan.lsp.AST.TTCN3.values.ArrayDimension;
import org.eclipse.titan.lsp.AST.TTCN3.values.IndexedValue;
import org.eclipse.titan.lsp.AST.TTCN3.values.Integer_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.SequenceOf_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Values;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a list of templates given with indexed notation.
 *
 * @author Kristof Szabados
 * */
public final class Indexed_Template_List extends TTCN3Template {

	private final IndexedTemplates indexedTemplates;

	// cache storing the value form of this if already created, or null
	private SequenceOf_Value asValue = null;

	public Indexed_Template_List(final IndexedTemplates indexedTemplates) {
		this.indexedTemplates = indexedTemplates;
		indexedTemplates.setFullNameParent(this);
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.INDEXED_TEMPLATE_LIST;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		builder.append("{ ");
		for (int i = 0, size = indexedTemplates.size(); i < size; i++) {
			if (i > 0) {
				builder.append(", ");
			}

			final IndexedTemplate indexedTemplate = indexedTemplates.get(i);
			builder.append(" [").append(indexedTemplate.getIndex().getValue().createStringRepresentation());
			builder.append("] := ");
			builder.append(indexedTemplate.getTemplate().createStringRepresentation());
		}
		builder.append(" }");

		return addToStringRepresentation(builder).toString();
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final IndexedTemplate template : indexedTemplates) {
			if (template == child) {
				final IValue index = template.getIndex().getValue();
				return builder.append(INamedNode.SQUAREOPEN).append(index.createStringRepresentation())
						.append(INamedNode.SQUARECLOSE);
			}
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (indexedTemplates != null) {
			indexedTemplates.setMyScope(scope);
		}
	}
	
	/** @return the number of templates in the list */
	public int size() {
		return indexedTemplates == null ? 0 : indexedTemplates.size();
	}

	/**
	 * @param index the index of the element to return.
	 * @return the template on the indexed position.
	 */
	public IndexedTemplate get(final int index) {
		return indexedTemplates.get(index);
	}

	@Override
	/** {@inheritDoc} */
	protected ITTCN3Template getReferencedArrayTemplate(final CompilationTimeStamp timestamp, final IValue arrayIndex,
			final IReferenceChain referenceChain, final boolean silent) {
		IValue indexValue = arrayIndex.setLoweridToReference(timestamp);
		indexValue = indexValue.getValueRefdLast(timestamp, referenceChain);
		if (indexValue.getIsErroneous(timestamp)) {
			return null;
		}

		long index = 0;
		if (!indexValue.isUnfoldable(timestamp)) {
			if (Value_type.INTEGER_VALUE.equals(indexValue.getValuetype())) {
				index = ((Integer_Value) indexValue).getValue();
			} else {
				if (!silent) {
					arrayIndex.getLocation().reportSemanticError("An integer value was expected as index");
				}
				return null;
			}
		} else {
			return null;
		}

		final IType tempType = myGovernor.getTypeRefdLast(timestamp);
		if (tempType.getIsErroneous(timestamp)) {
			return null;
		}

		switch (tempType.getTypetype()) {
		case TYPE_SEQUENCE_OF: {
			if (index < 0) {
				if (!silent) {
					final String message = MessageFormat
						.format("A non-negative integer value was expected instead of {0} for indexing a template of `sequence of'' type `{1}''",
								index, tempType.getTypename());
					arrayIndex.getLocation().reportSemanticError(message);
				}
				return null;
			}
			break;
		}
		case TYPE_SET_OF: {
			if (index < 0) {
				if (!silent) {
					final String message = MessageFormat
						.format("A non-negative integer value was expected instead of {0} for indexing a template of `set of'' type `{1}''",
								index, tempType.getTypename());
					arrayIndex.getLocation().reportSemanticError(message);
				}
				return null;
			}
			break;
		}
		case TYPE_ARRAY: {
			final ArrayDimension dimension = ((Array_Type) tempType).getDimension();
			dimension.checkIndex(timestamp, indexValue, Expected_Value_type.EXPECTED_DYNAMIC_VALUE);
			if (!dimension.getIsErroneous(timestamp)) {
				// re-base the index
				index -= dimension.getOffset();
				if (index < 0 || index > size()) {
					if (!silent) {
						arrayIndex.getLocation().reportSemanticError(
							MessageFormat.format("The index value {0} is outside the array indexable range", index
									+ dimension.getOffset()));
					}
					return null;
				}
			} else {
				return null;
			}
			break;
		}
		default:
			if (!silent) {
				arrayIndex.getLocation().reportSemanticError(
					MessageFormat.format("Invalid array element reference: type `{0}'' cannot be indexed",
							tempType.getTypename()));
			}
			return null;
		}

		for (int i = 0, size = indexedTemplates.size(); i < size; i++) {
			final IndexedTemplate template = indexedTemplates.get(i);
			IValue lastValue = template.getIndex().getValue();

			final IReferenceChain chain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
			lastValue = lastValue.getValueRefdLast(timestamp, chain);
			chain.release();

			if (Value_type.INTEGER_VALUE.equals(lastValue.getValuetype())) {
				final long tempIndex = ((Integer_Value) lastValue).getValue();
				if (index == tempIndex) {
					final ITTCN3Template realTemplate = template.getTemplate();
					if (Template_type.TEMPLATE_NOTUSED.equals(realTemplate.getTemplatetype())) {
						if (baseTemplate != null) {
							return baseTemplate.getTemplateReferencedLast(timestamp, referenceChain)
									.getReferencedArrayTemplate(timestamp, indexValue, referenceChain, silent);
						}

						return null;
					}

					return realTemplate;
				}
			}
		}

		switch (tempType.getTypetype()) {
		case TYPE_SEQUENCE_OF:
		case TYPE_SET_OF:
		case TYPE_ARRAY:
			// unfoldable (for now)
			break;
		default:
			// the error was reported earlier
			break;
		}

		return null;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isValue(final CompilationTimeStamp timestamp) {
		if (lengthRestriction != null || isIfpresent || getIsErroneous(timestamp)) {
			return false;
		}

		for (final IndexedTemplate t : indexedTemplates) {
			if (!t.getTemplate().isValue(timestamp)) {
				return false;
			}
		}

		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IValue getValue() {
		if (asValue != null) {
			return asValue;
		}

		final Values values = new Values(true);
		for (final IndexedTemplate indexedTemplate : indexedTemplates) {
			final IndexedValue indexedValue = new IndexedValue(indexedTemplate.getIndex(), indexedTemplate.getTemplate().getValue());
			indexedValue.setLocation(indexedTemplate.getLocation());
			values.addIndexedValue(indexedValue);
		}
		asValue = new SequenceOf_Value(values);
		asValue.setLocation(getLocation());
		asValue.setMyScope(getMyScope());
		asValue.setFullNameParent(getNameParent());
		asValue.setMyGovernor(getMyGovernor());

		return asValue;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		for (final IndexedTemplate t : indexedTemplates) {
			if(t.getTemplate().checkExpressionSelfReferenceTemplate(timestamp, lhs)) {
				return true;
			}
		}

		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		if (isBuildCancelled()) {
			return;
		}

		for (final IndexedTemplate t : indexedTemplates) {
			final ITTCN3Template temp = t.getTemplate();
			if (temp != null) {
				temp.checkSpecificValue(timestamp, true);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (referenceChain.add(this)) {
			for (final IndexedTemplate template : indexedTemplates) {
				if (template != null) {
					referenceChain.markState();
					template.getTemplate().checkRecursions(timestamp, referenceChain);
					referenceChain.previousState();
				}
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkValueomitRestriction(final CompilationTimeStamp timestamp, final String definitionName, final boolean omitAllowed, final Location usageLocation) {
		checkRestrictionCommon(timestamp, definitionName, omitAllowed, usageLocation);

		for (final IndexedTemplate t : indexedTemplates) {
			t.getTemplate().checkValueomitRestriction(timestamp, definitionName, true, usageLocation);
		}

		// complete check was not done, always needs runtime check
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		super.updateSyntax(reparser, isDamaged);
		indexedTemplates.updateSyntax(reparser, false);
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (asValue != null) {
			asValue.findReferences(referenceFinder, foundIdentifiers);
		} else if (indexedTemplates != null) {
			indexedTemplates.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (asValue != null && !asValue.accept(v)) {
			return false;
		} 
		if (indexedTemplates != null && !indexedTemplates.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyDefinition(Definition definition) {
		for (final IndexedTemplate t : indexedTemplates) {
			t.getTemplate().setMyDefinition(definition);
		}
	}
}
