/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.implementation;

import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.lsp.titanium.metrics.common.FunctionMetric;
import org.eclipse.titan.lsp.titanium.metrics.common.MetricData;
import org.eclipse.titan.lsp.titanium.metrics.visitors.Counter;
import org.eclipse.titan.lsp.titanium.metrics.visitors.CounterVisitor;

public class FMLinesOfCode extends BaseFunctionMetric {
	public FMLinesOfCode() {
		super(FunctionMetric.LINES_OF_CODE);
	}

	@Override
	public Number measure(final MetricData data, final Def_Function function) {
		final Counter count = new Counter(0);
		function.accept(new CounterVisitor(count) {
			@Override
			public int visit(final IVisitableNode node) {
				if (node instanceof Def_Function) {
					return V_CONTINUE;
				} else if (node instanceof StatementBlock) {
					count.set((((StatementBlock) node).getLocation()).getEndLine());
				}
				return V_SKIP;
			}
		});
		return count.val() - function.getLocation().getEndLine() + 1; // getStartLine ?
	}
}
