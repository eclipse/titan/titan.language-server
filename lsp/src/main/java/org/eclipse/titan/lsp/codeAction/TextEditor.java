/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.TextDocumentIdentifier;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.titan.lsp.common.utils.LSPUtils;

public class TextEditor {
	private static final String TAB = "\t";
	private static final String EMPTY = "";
	private static String newLine;

	private TextDocumentIdentifier doc;
	private List<TextEdit> textEditList = new ArrayList<>();
	private WorkspaceEdit edit = new WorkspaceEdit();

	public TextEditor(final TextDocumentIdentifier doc) {
		this.doc = Objects.requireNonNull(doc);
		TextEditor.setNewLine(doc);
	}

	public TextEditor insertText(final Position pos, final String text) {
		final Range range = new Range(pos, pos);
		final TextEdit insertion = new TextEdit(range, text);
		textEditList.add(insertion);
		return this;
	}

	public TextEditor insertTab(final Position pos) {
		return insertText(pos, TAB);
	}

	public TextEditor insertNewLine(final Position pos) {
		return insertText(pos, newLine);
	}

	public TextEditor removeText(final Range range) {
		final TextEdit remove = new TextEdit(range, EMPTY);
		textEditList.add(remove);
		return this;
	}

	public TextEditor replaceText(final Range range, final String newText) {
		final TextEdit replace = new TextEdit(range, newText);
		textEditList.add(replace);
		return this;
	}

	public WorkspaceEdit getEdit() {
		final Map<String, List<TextEdit>> changes = new LinkedHashMap<>();
		changes.put(doc.getUri(), textEditList);
		edit.setChanges(changes);
		return edit;
	}

	private static void setNewLine(final TextDocumentIdentifier doc) {
		newLine = LSPUtils.getLineEnding(doc);
	}
}
