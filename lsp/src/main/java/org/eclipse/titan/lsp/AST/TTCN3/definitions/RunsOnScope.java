/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.definitions;

import java.util.List;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Reference;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.TTCN3Scope;
import org.eclipse.titan.lsp.AST.TTCN3.types.ComponentTypeBody;
import org.eclipse.titan.lsp.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * The RunsOnScope class represents the TTCN3 specific 'runs on' scope, which is
 * a link to the contents of the Component type used as 'runs on' component.
 *
 * @author Kristof Szabados
 * */
public final class RunsOnScope extends TTCN3Scope {
	private Component_Type componentType;
	private ComponentTypeBody componentDefinitions;

	public RunsOnScope(final Component_Type componentType, final Scope parentScope) {
		this.componentType = componentType;
		if (componentType == null) {
			componentDefinitions = null;
		} else {
			componentType.setOwnertype(TypeOwner_type.OT_RUNSON_SCOPE, this);
			componentDefinitions = componentType.getComponentBody();
		}
		setParentScope(parentScope);
	}

	/**
	 * Sets the component type for this runs on scope.
	 * @param componentType the component type creating this runs on clause.
	 */
	public void setComponentType(final Component_Type componentType) {
		this.componentType = componentType;
		if (componentType == null) {
			componentDefinitions = null;
		} else {
			componentDefinitions = componentType.getComponentBody();
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasAssignmentWithId(final CompilationTimeStamp timestamp, final Identifier identifier) {
		if (componentDefinitions != null && componentDefinitions.hasAssignmentWithId(timestamp, identifier)) {
			return true;
		}
		return super.hasAssignmentWithId(timestamp, identifier);
	}

	@Override
	/** {@inheritDoc} */
	public RunsOnScope getScopeRunsOn() {
		return this;
	}

	/**
	 * @return the component type
	 */
	public Component_Type getComponentType() {
		return componentType;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		if (componentDefinitions != null && componentDefinitions.hasLocalAssignmentWithId(reference.getId())) {
			return componentDefinitions.getLocalAssignmentById(reference.getId());
		}
		if (parentScope != null) {
			return parentScope.getAssBySRef(timestamp, reference);
		}
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getEnclosingAssignment(final Position offset) {
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (componentType != null) {
			componentType.findReferences(referenceFinder, foundIdentifiers);
		}
		if (componentDefinitions != null) {
			componentDefinitions.findReferences(referenceFinder, foundIdentifiers);
		}
	}
	
	@Override
	public List<Definition> getVisibleDefinitions() {
		if (componentDefinitions == null) {
			return super.getVisibleDefinitions();
		}
		return componentDefinitions.getVisibleDefinitions();
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		case ASTVisitor.V_CONTINUE:
		default:
			break;
		}
		if (componentType != null) {
			if (!componentType.accept(v)) {
				return false;
			}
		}
		if (componentDefinitions != null) {
			if (!componentDefinitions.accept(v)) {
				return false;
			}
		}
		return v.leave(this) != ASTVisitor.V_ABORT;
	}
}
