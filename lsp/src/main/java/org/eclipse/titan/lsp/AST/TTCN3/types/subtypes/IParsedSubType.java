/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types.subtypes;

import org.eclipse.titan.lsp.AST.IIdentifierContainer;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;

/**
 * Represents a sub-type restriction as it was parsed.
 *
 * @author Adam Delic
 * @author Adam Knapp
 * */
public interface IParsedSubType extends IIncrementallyUpdatable, IIdentifierContainer, IVisitableNode, ILocateableNode {
	enum ParsedSubType_type {
		SINGLE_PARSEDSUBTYPE, RANGE_PARSEDSUBTYPE, LENGTH_PARSEDSUBTYPE, PATTERN_PARSEDSUBTYPE
	}

	ParsedSubType_type getSubTypetype();

	@Override
	default void setLocation(Location location) {
		// Do nothing by default
	}
}
