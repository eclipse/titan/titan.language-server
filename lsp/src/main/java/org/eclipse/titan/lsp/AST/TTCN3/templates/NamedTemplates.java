/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Class to represent NamedTemplateList.
 *
 * @author Kristof Szabados
 */
public final class NamedTemplates extends ASTNode
	implements IIncrementallyUpdatable, ICollection<NamedTemplate> {
	public static final String DUPLICATEFIELDNAMEFIRST = "Duplicate field name `{0}'' was first declared here";
	public static final String DUPLICATEFIELDNAMEREPEATED = "Duplicate field name `{0}'' was declared here again";

	private final ArrayList<NamedTemplate> named_templates = new ArrayList<>();

	private HashMap<String, NamedTemplate> namedTemplateMap;
	private List<NamedTemplate> duplicatedNames;
	private CompilationTimeStamp lastUniquenessCheck;

	public NamedTemplates() {
		super();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		named_templates.trimToSize();
		for (final NamedTemplate template : named_templates) {
			template.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final NamedTemplate template : named_templates) {
			if (template == child) {
				return builder.append(INamedNode.DOT).append(template.getName().getDisplayName());
			}
		}

		return builder;
	}

	@Override
	public boolean add(final NamedTemplate template) {
		if (template != null && template.getName() != null) {
			template.setFullNameParent(this);
			return named_templates.add(template);
		}
		return false;
	}

	/**
	 * Remove all named values that were not parsed, but generated during
	 * previous semantic checks.
	 */
	public void removeGeneratedValues() {
		named_templates.removeIf(t -> !t.isParsed());
	}

	@Override
	public int size() {
		return named_templates.size();
	}

	@Override
	public NamedTemplate get(final int index) {
		return named_templates.get(index);
	}

	/**
	 * Checks if there is a named template in the list, with a given name.
	 *
	 * @param id the name to search for.
	 * @return {@code true} if the list has a template with the provided name,
	 *         {@code false} otherwise.
	 */
	@Override
	public boolean contains(final Object id) {
		if (!(id instanceof Identifier)) {
			return false;
		}
		if (lastUniquenessCheck == null) {
			checkUniqueness(CompilationTimeStamp.getBaseTimestamp());
		}
		return namedTemplateMap.containsKey(((Identifier)id).getName());
	}

	/**
	 * Checks if there is a template with the provided name, and if found
	 * returns it.
	 *
	 * @param id the name to search for.
	 * @return the template with the provided name position if such exists,
	 *         otherwise {@code null}.
	 */
	public NamedTemplate get(final Identifier id) {
		if (lastUniquenessCheck == null) {
			checkUniqueness(CompilationTimeStamp.getBaseTimestamp());
		}
		return namedTemplateMap.containsKey(id.getName()) ? namedTemplateMap.get(id.getName()) : null;
	}

	/**
	 * Checks the uniqueness of the named templates.
	 * @param timestamp the timestamp of the actual build cycle
	 */
	public void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastUniquenessCheck != null && !lastUniquenessCheck.isLess(timestamp)) {
			return;
		}

		Identifier identifier;
		String name;

		if (lastUniquenessCheck == null) {
			namedTemplateMap = new HashMap<String, NamedTemplate>(named_templates.size());
			duplicatedNames = new ArrayList<NamedTemplate>();

			for (final NamedTemplate template : named_templates) {
				identifier = template.getName();
				name = identifier.getName();
				if (namedTemplateMap.containsKey(name)) {
					duplicatedNames.add(template);
				} else {
					namedTemplateMap.put(name, template);
				}
			}

			for (final NamedTemplate template : duplicatedNames) {
				named_templates.remove(template);
			}
		}

		if (duplicatedNames != null) {
			for (final NamedTemplate template : duplicatedNames) {
				identifier = template.getName();
				name = identifier.getName();
				final Location namedLocation = namedTemplateMap.get(name).getName().getLocation();
				namedLocation.reportSingularSemanticError(MessageFormat.format(DUPLICATEFIELDNAMEFIRST, identifier.getDisplayName()));
				template.getLocation().reportSemanticError(
						MessageFormat.format(DUPLICATEFIELDNAMEREPEATED, identifier.getDisplayName()));
			}
		}

		lastUniquenessCheck = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final NamedTemplate template : named_templates) {
			template.updateSyntax(reparser, false);
			reparser.updateLocation(template.getLocation());
		}

		if (duplicatedNames != null) {
			for (final NamedTemplate template : duplicatedNames) {
				template.updateSyntax(reparser, false);
				reparser.updateLocation(template.getLocation());
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final NamedTemplate namedTemp : named_templates) {
			namedTemp.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final NamedTemplate nt : named_templates) {
			if (!nt.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<NamedTemplate> iterator() {
		return named_templates.iterator();
	}
}
