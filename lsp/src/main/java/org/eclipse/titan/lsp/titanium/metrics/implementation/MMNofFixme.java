/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.metrics.implementation;

import org.eclipse.titan.lsp.titanium.metrics.common.MetricData;
import org.eclipse.titan.lsp.titanium.metrics.common.ModuleMetric;
import org.eclipse.titan.lsp.AST.Module;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;

public class MMNofFixme extends BaseModuleMetric {
	public MMNofFixme() {
		super(ModuleMetric.NOF_FIXME);
	}

	@Override
	public Number measure(final MetricData data, final Module module) {
		final Project project = Project.INSTANCE;
		final ProjectItem res = project.getProjectItem(module.getLocation().getFile().toPath());
		if (res == null) {
			return 0;
		}
		int count = 0;
		try {
//			count = res.findMarkers(IMarker.TASK, true, IResource.DEPTH_ZERO).length;
			count = res.getMarkerList().size();
		} catch (final Exception e) {
			TitanLogger.logError("Error while counting markers of " + res.getModule().getName(), e);
		}
		return count;
	}
}
