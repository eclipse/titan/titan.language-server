/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.types;

import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ILocatableComment;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IOutlineElement;
import org.eclipse.titan.lsp.AST.IReferencingElement;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.TypeOwner_type;
import org.eclipse.titan.lsp.AST.IType.ValueCheckingOptions;
import org.eclipse.titan.lsp.AST.ITypeWithComponents;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Type;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.IAppendableSyntax;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.declarationsearch.IDeclaration;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.lsp.parsers.ttcn3parser.Ttcn3Lexer;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting;
import org.eclipse.titan.lsp.semantichighlighting.AstSemanticHighlighting.SemanticType;

/**
 * Component field.
 * <p>
 * Used to contain data about a field of a structured type.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class CompField extends ASTNode
	implements IOutlineElement, ILocateableNode, IAppendableSyntax, IIncrementallyUpdatable,
	IReferencingElement, ILocatableComment {
	private static final String FULLNAMEPART = ".<defaultValue>";

	private Identifier name;
	private final IType type;
	private final boolean optional;
	private final Value defaultValue;

	private Location commentLocation = null;

	/**
	 * The location of the whole component field. This location encloses the
	 * component field fully, as it is used to report errors to.
	 */
	private Location location = Location.getNullLocation();

	public CompField(final Identifier name, final IType type, final boolean optional, final Value defaultValue) {
		this.name = name;
		this.type = type;
		this.optional = optional;
		this.defaultValue = defaultValue;

		if (type != null) {
			type.setOwnertype(TypeOwner_type.OT_COMP_FIELD, this);
			type.setFullNameParent(this);
		}
		if (defaultValue != null) {
			defaultValue.setFullNameParent(this);
		}
	}

	public CompField newInstance() {
		return new CompField(name, type, optional, defaultValue);
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		builder.append(INamedNode.DOT).append(name.getDisplayName());

		if (child == defaultValue) {
			return builder.append(FULLNAMEPART);
		}
		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	/**
	 * @return the identifier of this component field
	 * */
	@Override
	public Identifier getIdentifier() {
		return name;
	}

	/**
	 * @return the type of this component field
	 * */
	public Type getType() {
		return (Type) type;
	}

	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		if (type instanceof IOutlineElement) {
			return ((IOutlineElement) type).getOutlineChildren();
		}
		return new Object[] {};
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		final StringBuilder text = new StringBuilder(name.getDisplayName());
		text.append(" : ");
		text.append(type.getTypename());
		return text.toString();
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		if (type instanceof IOutlineElement) {
			return ((IOutlineElement) type).category();
		}
		return 0;
	}

	/**
	 * @return true if the component field is optional
	 * */
	public boolean isOptional() {
		return optional;
	}

	/**
	 * @return true if the component field has a default value
	 * */
	public boolean hasDefault() {
		return defaultValue != null;
	}

	/** @return the default value, or null if there is none set*/
	public Value getDefault() {
		return defaultValue;
	}

	@Override
	public Location getCommentLocation() {
		return commentLocation;
	}

	@Override
	public void setCommentLocation(final Location commentLocation) {
		this.commentLocation = commentLocation;
	}

	/**
	 * Sets the actual scope of this component field.
	 *
	 * @param scope the scope to be set
	 * */
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (type != null) {
			type.setMyScope(scope);
		}
		if (defaultValue != null) {
			defaultValue.setMyScope(scope);
		}
	}

	/**
	 * Does the semantic checking of this field.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * */
	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (type == null) {
			return;
		}

		type.check(timestamp);
		type.checkEmbedded(timestamp, type.getLocation(), true, "embedded into another type");

		if (defaultValue == null) {
			return;
		}

		defaultValue.setMyGovernor(type);
		final IType lastType = type.getTypeRefdLast(timestamp);
		final IValue tempValue = lastType.checkThisValueRef(timestamp, defaultValue);
		lastType.checkThisValue(timestamp, tempValue, null, new ValueCheckingOptions(Expected_Value_type.EXPECTED_CONSTANT, false, false, true, false, false));
	}

	@Override
	/** {@inheritDoc} */
	public List<Integer> getPossibleExtensionStarterTokens() {
		return optional ? IAppendableSyntax.super.getPossibleExtensionStarterTokens()
				: List.of(Ttcn3Lexer.OPTIONAL, Ttcn3Lexer.LPAREN, Ttcn3Lexer.LENGTH, Ttcn3Lexer.SQUAREOPEN);
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			boolean enveloped = false;
			if(name != null) {
				final Location tempIdentifier = name.getLocation();
				if (reparser.envelopsDamage(tempIdentifier) || reparser.isExtending(tempIdentifier)) {
					reparser.extendDamagedRegion(tempIdentifier);
					final IIdentifierReparser r = new IdentifierReparser(reparser);
					final int result = r.parse();
					name = r.getIdentifier();
					// damage handled
					if (result == 0) {
						enveloped = true;
					} else {
						throw new ReParseException(result);
					}
				}
			}

			if (type != null) {
				if (enveloped) {
					type.updateSyntax(reparser, false);
					reparser.updateLocation(type.getLocation());
				} else if (reparser.envelopsDamage(type.getLocation())) {
					type.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(type.getLocation());
				}
			}

			if (defaultValue != null) {
				if (enveloped) {
					defaultValue.updateSyntax(reparser, false);
					reparser.updateLocation(defaultValue.getLocation());
				} else if (reparser.envelopsDamage(defaultValue.getLocation())) {
					defaultValue.updateSyntax(reparser, true);
					enveloped = true;
					reparser.updateLocation(defaultValue.getLocation());
				}
			}

			if (enveloped) {
				return;
			}

			throw new ReParseException();
		}

		reparser.updateLocation(name.getLocation());
		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}
		if (defaultValue != null) {
			defaultValue.updateSyntax(reparser, false);
			reparser.updateLocation(defaultValue.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (type != null) {
			type.findReferences(referenceFinder, foundIdentifiers);
		}
		if (defaultValue != null) {
			defaultValue.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (type!=null && !type.accept(v)) {
			return false;
		}
		if (name != null && !name.accept(v)) {
			return false;
		}
		if (defaultValue!=null && !defaultValue.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public IDeclaration getDeclaration() {

		INamedNode inamedNode = getNameParent();

		while (!(inamedNode instanceof Definition)) {
			if( inamedNode == null) {
				return null; //FIXME: this is just a temp solution! find the reason!
			}
			inamedNode = inamedNode.getNameParent();
		}

		final Definition namedTemplList = (Definition) inamedNode;

		IType tempType = namedTemplList.getType(CompilationTimeStamp.getBaseTimestamp());
		if (tempType == null) {
			return null;
		}

		tempType = tempType.getTypeRefdLast(CompilationTimeStamp.getBaseTimestamp());

		if (tempType instanceof ITypeWithComponents) {
			final Identifier resultId = ((ITypeWithComponents) tempType).getComponentIdentifierByName(getIdentifier());
			return IDeclaration.createInstance(tempType.getDefiningAssignment(), resultId);
		}

		return null;
	}
	
	@Override
	/** {@inheritDoc} */
	public void setSemanticInformation() {
		AstSemanticHighlighting.addSemanticToken(getIdentifier().getLocation(), SemanticType.Property);
		super.setSemanticInformation();
	}
}
