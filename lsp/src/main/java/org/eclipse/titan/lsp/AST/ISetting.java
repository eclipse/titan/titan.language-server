/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import org.eclipse.titan.lsp.AST.Module.module_type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * @author Kristof Szabados
 * */
public interface ISetting extends IASTNode, ILocateableNode {

	enum Setting_type {
		/** Reference to non-existent stuff. */ S_UNDEF,
		/** ObjectClass. */                     S_OC ,
		/** Object. */                          S_O,
		/** ObjectSet. */                       S_OS,
		/** Reference to non-existent stuff. */ S_ERROR,
		/** Type. */                            S_T,
		/**< Template. */                       S_TEMPLATE,
		/**< Value. */                          S_V,
		/**< ValueSet. */                       S_VS
	}

	/**
	 * Checks whether the setting was reported erroneous or not.
	 * @return true if the setting is erroneous, or false otherwise
	 */
	boolean getIsErroneous();

	/**
	 * Checks whether the setting was reported erroneous or not.
	 * TODO: the parameter is not used for anything 
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @return true if the setting is erroneous, or false otherwise
	 */
	boolean getIsErroneous(final CompilationTimeStamp timestamp);

	/**
	 * Sets the erroneousness of the setting.
	 * @param isErroneous the value to set.
	 */
	void setIsErroneous(final boolean isErroneous);

	/**
	 * @return the internal type of the setting
	 */
	Setting_type getSettingtype();

	/**
	 * Returns whether this ISetting is ASN.1 based on the module type
	 * @return {@code true} if this ISetting is ASN.1 and<br>{@code false} otherwise
	 */
	default boolean isAsn() {
		return getMyScope() != null &&
				module_type.ASN_MODULE.equals(getMyScope().getModuleScope().getModuletype());
	}

	/**
	 * Returns whether this ISetting is TTCN-3 based on the module type
	 * @return {@code true} if this ISetting is TTCN-3 and<br>{@code false} otherwise
	 */
	default boolean isTtcn() {
		return getMyScope() != null &&
				module_type.TTCN3_MODULE.equals(getMyScope().getModuleScope().getModuletype());
	}
}
