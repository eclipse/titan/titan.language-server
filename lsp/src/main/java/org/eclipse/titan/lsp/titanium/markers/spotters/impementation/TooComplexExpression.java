/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.IValue;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.statements.Assignment_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.DoWhile_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.For_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.If_Clause;
import org.eclipse.titan.lsp.AST.TTCN3.statements.While_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.templates.TTCN3Template;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class TooComplexExpression {

	private TooComplexExpression() {
		throw new AssertionError("Noninstantiable");
	}

	private abstract static class Base extends BaseModuleCodeSmellSpotter {
		private static final String COMPLEXITY = "The complexity of this expression `{0}'' is bigger than allowed `{1}''";
		private final int reportTooComplexExpressionSize;

		public Base() {
			super(CodeSmellType.TOO_COMPLEX_EXPRESSIONS);
			reportTooComplexExpressionSize = 7;
		}

		protected static class ExpressionVisitor extends ASTVisitor {
			private int count = 0;

			public int getCount() {
				return count;
			}

			@Override
			public int visit(final IVisitableNode node) {
				if (node instanceof Expression_Value) {
					count++;
				}

				return V_CONTINUE;
			}
		}

		protected void check(final IValue expression, final Problems problems) {
			if (expression instanceof Expression_Value) {
				final ExpressionVisitor visitor = new ExpressionVisitor();
				expression.accept(visitor);
				if (visitor.getCount() > reportTooComplexExpressionSize) {
					final String msg = MessageFormat.format(COMPLEXITY, visitor.getCount(), reportTooComplexExpressionSize);
					problems.report(expression.getLocation(), msg);
				}
			}
		}

	}

	public static class For extends Base {
		public For() {
			super();
			addStartNode(For_Statement.class);
		}
		
		@Override
		public void process(final IVisitableNode node, final Problems problems) {
			if (node instanceof For_Statement) {
				final For_Statement s = (For_Statement) node;
				final Value expression = s.getFinalExpression();
				check(expression, problems);
			}
		}
	}

	public static class While extends Base {
		public While() {
			super();
			addStartNode(While_Statement.class);
		}
		@Override
		public void process(final IVisitableNode node, final Problems problems) {
			if (node instanceof While_Statement) {
				final While_Statement s = (While_Statement) node;
				final Value expression = s.getExpression();
				check(expression, problems);
			}
		}
	}

	public static class DoWhile extends Base {
		public DoWhile() {
			super();
			addStartNode(DoWhile_Statement.class);
		}
		
		@Override
		public void process(final IVisitableNode node, final Problems problems) {
			if (node instanceof DoWhile_Statement) {
				final DoWhile_Statement s = (DoWhile_Statement) node;
				final Value expression = s.getExpression();
				check(expression, problems);
			}
		}
	}

	public static class If extends Base {
		public If() {
			super();
			addStartNode(If_Clause.class);
		}
		
		@Override
		public void process(final IVisitableNode node, final Problems problems) {
			if (node instanceof If_Clause) {
				final If_Clause s = (If_Clause) node;
				final Value expression = s.getExpression();
				check(expression, problems);
			}
		}
	}

	public static class Assignments extends Base {
		public Assignments() {
			super();
			addStartNode(Assignment_Statement.class);
		}
		
		@Override
		public void process(final IVisitableNode node, final Problems problems) {
			if (node instanceof Assignment_Statement) {
				final Assignment_Statement s = (Assignment_Statement) node;
				final CompilationTimeStamp ct = CompilationTimeStamp.getBaseTimestamp();
				final Assignment assignment = s.getReference().getRefdAssignment(ct, false);
				final TTCN3Template template = s.getTemplate();
				if (assignment == null || template == null) {
					// semantic error in the code.
					return;
				}
				switch (assignment.getAssignmentType()) {
				case A_PAR_VAL_IN:
				case A_PAR_VAL_OUT:
				case A_PAR_VAL_INOUT:
				case A_PAR_VAL:
				case A_VAR:
					if (template.isValue(ct)) {
						final IValue tempValue = template.getValue();
						check(tempValue, problems);
					}
					break;
				default:
					break;
				}
			}
		}
	}
}
