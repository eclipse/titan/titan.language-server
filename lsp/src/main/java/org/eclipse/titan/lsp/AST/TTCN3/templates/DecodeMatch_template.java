/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import org.eclipse.titan.lsp.AST.Assignment;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.IReferenceChain;
import org.eclipse.titan.lsp.AST.IReferencingType;
import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IType.Type_type;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Represents a decode match template.
 *
 * @author Kristof Szabados
 * */
public class DecodeMatch_template extends TTCN3Template {
	private static final String TEMPLATE_INST_TYPE_ERROR = "Type of template instance cannot be determined";
	private static final String ENC_FORMAT_PARAM_ERROR =
			"The encoding format parameter is only available to universal charstring templates";

	final Value stringEncoding;
	final TemplateInstance target;

	public DecodeMatch_template(final Value stringEncoding, final TemplateInstance target) {
		this.stringEncoding = stringEncoding;
		this.target = target;

		if (stringEncoding != null) {
			stringEncoding.setFullNameParent(this);
		}
		if (target != null) {
			target.setFullNameParent(this);
		}
	}

	/**
	 * @return the encoding parameter of the decmatch template.
	 * */
	public Value getStringEncoding() {
		return stringEncoding;
	}

	/**
	 * @return the target parameter of the decmatch template.
	 * */
	public TemplateInstance getDecodeTarget() {
		return target;
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.DECODE_MATCH;
	}

	@Override
	/** {@inheritDoc} */
	public String createStringRepresentation() {
		if (target == null) {
			return ERRONEOUS_TEMPLATE;
		}

		final StringBuilder builder = new StringBuilder();
		builder.append("decmatch ");

		if (stringEncoding != null) {
			builder.append(LEFTPARENTHESES);
			builder.append(stringEncoding.createStringRepresentation());
			builder.append(RIGHTPARENTHESES);
		}

		builder.append(target.getExpressionGovernor(CompilationTimeStamp.getBaseTimestamp(), Expected_Value_type.EXPECTED_TEMPLATE).getTypename());
		builder.append(": ");
		target.getTemplateBody().createStringRepresentation();

		return addToStringRepresentation(builder).toString();
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (stringEncoding == child) {
			return builder.append(".<string_encoding>");
		} else if (target == child) {
			return builder.append(".<decoding_target>");
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (stringEncoding != null) {
			stringEncoding.setMyScope(scope);
		}
		if (target != null) {
			target.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (isBuildCancelled()) {
			return;
		}
		
		if (target != null) {
			target.checkRecursions(timestamp, referenceChain);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {
		final boolean selfRef = type.checkThisTemplate(timestamp, this, isModified, implicitOmit, lhs);
		checkThisTemplateGenericPostChecks(timestamp, type, allowOmit, subCheck);
		return selfRef;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		if (target != null) {
			return target.getTemplateBody().checkExpressionSelfReferenceTemplate(timestamp, lhs);
		}

		return false;
	}

	/**
	 * Checks if this template is valid for the provided type.
	 * <p>
	 * The type must be equivalent with the TTCN-3 string type
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle.
	 * @param type the string type used for the check.
	 * @param implicitOmit true if the implicit omit optional attribute was set for the template, false otherwise.
	 * @param lhs the assignment to check against.
	 *
	 * @return true if the value contains a reference to lhs
	 * */
	public boolean checkThisTemplateString(final CompilationTimeStamp timestamp, final IType type, final boolean implicitOmit, final Assignment lhs) {
		final TTCN3Template targetBody = target.getTemplateBody();

		targetBody.setLoweridToReference(timestamp);
		IType targetType = target.getExpressionGovernor(timestamp, Expected_Value_type.EXPECTED_TEMPLATE);
		if (targetType == null) {
			target.getLocation().reportSemanticError(TEMPLATE_INST_TYPE_ERROR);
			return false;
		}

		targetType.check(timestamp);
		if (target.getType() != null && targetType instanceof IReferencingType) {
			targetType = targetType.getTypeRefdLast(timestamp);
		}

		targetBody.setMyGovernor(targetType);
		boolean selfReference = targetBody.checkThisTemplateGeneric(timestamp, targetType,
				target.getDerivedReference() != null, false, true, true, implicitOmit, lhs);
		targetType.checkCoding(timestamp, false, getMyScope().getModuleScope(), false, target.getLocation());

		if (stringEncoding != null) {
			if (type.getTypetype() != Type_type.TYPE_UCHARSTRING) {
				stringEncoding.getLocation().reportSemanticError(ENC_FORMAT_PARAM_ERROR);
				return selfReference;
			}
			selfReference |= stringEncoding.checkStringEncoding(timestamp, lhs);
		}

		return selfReference;
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyDefinition(Definition definition) {
		if (target != null) {
			target.getTemplateBody().setMyDefinition(definition);
		}
	}
}
