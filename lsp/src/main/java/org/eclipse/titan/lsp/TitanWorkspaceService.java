/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DidChangeConfigurationParams;
import org.eclipse.lsp4j.DidChangeWatchedFilesParams;
import org.eclipse.lsp4j.DidChangeWorkspaceFoldersParams;
import org.eclipse.lsp4j.FileEvent;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.WorkspaceDiagnosticParams;
import org.eclipse.lsp4j.WorkspaceDiagnosticReport;
import org.eclipse.lsp4j.WorkspaceDocumentDiagnosticReport;
import org.eclipse.lsp4j.WorkspaceFolder;
import org.eclipse.lsp4j.WorkspaceFullDocumentDiagnosticReport;
import org.eclipse.lsp4j.WorkspaceSymbol;
import org.eclipse.lsp4j.WorkspaceSymbolParams;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.services.WorkspaceService;
import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.common.product.Configuration;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;
import org.eclipse.titan.lsp.common.product.DefaultPreferences;
import org.eclipse.titan.lsp.common.utils.IOUtils;

public final class TitanWorkspaceService implements WorkspaceService {
	public TitanWorkspaceService(TitanLanguageServer languageServer) {

	}

	@Override
    public CompletableFuture<Either<List<? extends SymbolInformation>, List<? extends WorkspaceSymbol>>> symbol(WorkspaceSymbolParams workspaceSymbolParams) {
        return null;
    }

	@Override
	public void didChangeConfiguration(DidChangeConfigurationParams params) {
		if (!DefaultPreferences.isInitiated()) {
			DefaultPreferences.initialize();
		}
		if (params.getSettings() != null) {
			Configuration.INSTANCE.parseConfiguration(params.getSettings().toString());
			final List<Object> excludeList = Configuration.INSTANCE.getList(Configuration.EXCLUDED_FROM_BUILD, new ArrayList<>());
			for (final Object excluded: excludeList) {
				if (excluded instanceof String) {
					final String excludedNameOrig = (String)excluded;
					final ProjectItem projectItem = Project.INSTANCE.getProjectItem(
							IOUtils.getPath(excludedNameOrig));
					/* Otherwise probably trying to exclude a file outside of the project */
					if (projectItem != null) {
						projectItem.setExcluded(true);
					}
				} 
			}
		}
	}
	
	@Override
	public void didChangeWorkspaceFolders(DidChangeWorkspaceFoldersParams params) {
		final List<WorkspaceFolder> added = params.getEvent().getAdded();
		final List<WorkspaceFolder> removed = params.getEvent().getRemoved();
		
		/**
		 * Reanalyze the whole project. TODO: check if it has to be done all the time.
		 * FIXME: this should be synchronized with project analysis 
		 */
		if (TitanLanguageServer.BUILD_LOCK.tryLock()) {
			TitanLanguageServer.BUILD_LOCK.unlock();
		} else {
			TitanLanguageServer.requestBuildCancellation();
		}
		TitanLanguageServer.BUILD_LOCK.lock();
		try {
			Project.INSTANCE.addWorkspaceFolders(added);
			Project.INSTANCE.removeWorkspaceFolders(removed);
			Project.INSTANCE.resetProject();
		} finally {
			TitanLanguageServer.BUILD_LOCK.unlock();
		}

		try {
			TitanLanguageServer.FIRST_ANALYSIS_LATCH.await();
			CompletableFuture.runAsync(() ->
				TitanLanguageServer.analyzeProject(Project.INSTANCE)
			);
		} catch (InterruptedException e) {
			TitanLogger.logError(e);
			Thread.currentThread().interrupt();
		} catch (Exception e) {
			TitanLogger.logError(e);
		}
	}


	@Override
	public CompletableFuture<WorkspaceDiagnosticReport> diagnostic(WorkspaceDiagnosticParams params) {
		// FIXME:
		// Sending publishDiagnostics notifications and response for diagnostic pull requests
		// causes duplicated diagnostic report on the client.
		// The diagnostic provider has been temporarily commented out in the server's capabilities.
		// This endpoint won't be reached.
		return CompletableFuture.supplyAsync(()-> {
			WorkspaceDiagnosticReport finalReport = new WorkspaceDiagnosticReport();
			final List<WorkspaceDocumentDiagnosticReport> reportList = new ArrayList<>();
			Project.INSTANCE.getAllProjectItems().entrySet().stream().forEach(item -> {
				final ProjectItem projItem = item.getValue();
				if (! projItem.isExcluded() && !projItem.isDirectory()) {
					final List<Diagnostic> diags = projItem.getMarkerList();
					WorkspaceFullDocumentDiagnosticReport full = new WorkspaceFullDocumentDiagnosticReport(diags, projItem.getPath().toUri().toString(), projItem.getVersion());
					reportList.add(new WorkspaceDocumentDiagnosticReport(full));
				}
			});
			finalReport.setItems(reportList);
			return finalReport;
		});
	}
	
	@Override
	public void didChangeWatchedFiles(DidChangeWatchedFilesParams params) {
		/**
		 * TODO :: handle TPD
		 * TODO :: send empty diagnostics
		 */
		boolean shouldRebuild = false;
		for (FileEvent event: params.getChanges()) {
			switch (event.getType()) {
			case Deleted:
				final ProjectItem projectItem = Project.INSTANCE.getProjectItem(
						IOUtils.getPath(event.getUri()));
				if (projectItem != null) {
					projectItem.closeFile();
					if (!projectItem.isExcluded()) {
						shouldRebuild = true;
					}
					Project.INSTANCE.removeProjectItem(projectItem);
				}
				break;
			default:
				break;
			}
		}
		if (shouldRebuild) {
			TitanLanguageServer.analyzeProject(Project.INSTANCE);
		}
	}
}
