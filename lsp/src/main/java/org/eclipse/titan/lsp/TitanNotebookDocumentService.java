/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp;

import org.eclipse.lsp4j.DidChangeNotebookDocumentParams;
import org.eclipse.lsp4j.DidCloseNotebookDocumentParams;
import org.eclipse.lsp4j.DidOpenNotebookDocumentParams;
import org.eclipse.lsp4j.DidSaveNotebookDocumentParams;
import org.eclipse.lsp4j.services.NotebookDocumentService;

public final class TitanNotebookDocumentService implements NotebookDocumentService {

	private final TitanLanguageServer languageServer;

	public TitanNotebookDocumentService(TitanLanguageServer languageServer) {
		this.languageServer = languageServer;
	}

	@Override
	public void didOpen(DidOpenNotebookDocumentParams params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void didChange(DidChangeNotebookDocumentParams params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void didSave(DidSaveNotebookDocumentParams params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void didClose(DidCloseNotebookDocumentParams params) {
		// TODO Auto-generated method stub

	}

}
