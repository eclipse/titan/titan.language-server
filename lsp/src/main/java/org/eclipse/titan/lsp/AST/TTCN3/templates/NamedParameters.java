/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.TTCN3.templates;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.ILocateableNode;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.AST.ReferenceFinder;
import org.eclipse.titan.lsp.AST.ReferenceFinder.Hit;
import org.eclipse.titan.lsp.AST.Scope;
import org.eclipse.titan.lsp.AST.TTCN3.IIncrementallyUpdatable;
import org.eclipse.titan.lsp.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.lsp.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the list of named actual parameters. For example in a function call.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class NamedParameters extends ASTNode
	implements ILocateableNode, IIncrementallyUpdatable, ICollection<NamedParameter> {

	private final List<NamedParameter> namedParams;

	private Location location = Location.getNullLocation();

	public NamedParameters() {
		super();
		namedParams = new ArrayList<NamedParameter>();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		for (final NamedParameter parameter : namedParams) {
			parameter.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (int i = 0, size = namedParams.size(); i < size; i++) {
			if (namedParams.get(i) == child) {
				return builder.append(INamedNode.SQUAREOPEN).append(String.valueOf(i + 1)).append(INamedNode.SQUARECLOSE);
			}
		}

		return builder;
	}

	public String createStringRepresentation() {
		final StringBuilder sb = new StringBuilder();
		for (final NamedParameter n : namedParams) {
			sb.append(n.createStringRepresentation()).append(", ");
		}
		if (!namedParams.isEmpty()) {
			sb.setLength(sb.length() - 2);
		}
		return sb.toString();
	}

	@Override
	public boolean add(final NamedParameter parameter) {
		if (parameter != null && parameter.getName() != null) {
			parameter.setFullNameParent(this);
			return namedParams.add(parameter);
		}
		return false;
	}

	@Override
	public int size() {
		return namedParams.size();
	}

	@Override
	public NamedParameter get(final int index) {
		return namedParams.get(index);
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		IIncrementallyUpdatable.super.updateSyntax(reparser, isDamaged);

		for (final NamedParameter parameter : namedParams) {
			parameter.updateSyntax(reparser, false);
			reparser.updateLocation(parameter.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		for (final NamedParameter namedParam : namedParams) {
			namedParam.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final NamedParameter np : namedParams) {
			if (!np.accept(v)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<NamedParameter> iterator() {
		return namedParams.iterator();
	}
}
