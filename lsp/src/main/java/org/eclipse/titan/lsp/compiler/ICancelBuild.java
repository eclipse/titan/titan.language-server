/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.compiler;

import org.eclipse.titan.lsp.TitanLanguageServer;

/**
 * Simple interface to support build cancellation.
 * 
 * @author Miklos Magyari
 */
public interface ICancelBuild {
	/**
	 * Checks if the build process should be cancelled. To ensure quick cancellation, 
	 * methods calling this function should return as quickly as possible if cancellation is requested.
	 * <br><br>
	 * The default implementation should be suitable for
	 * all classes implementing this interface and there will be no need to override this method.
	 * 
	 * @return if cancellation of build is requested.
	 */
	default boolean isBuildCancelled() {
		return TitanLanguageServer.isBuildCancelled();
	}
}
