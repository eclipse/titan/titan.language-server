/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction.data;

import java.util.List;

import org.eclipse.titan.lsp.AST.Location;

public class MissingParamTagData {
	public static class MissingParamTagEntry {
		public String paramName;
		public Location location;

		public MissingParamTagEntry(final String code, final Location location) {
			this.paramName = code;
			this.location = location;
		}
	}
	
	public List<MissingParamTagEntry> missingParams;
	public MissingParamTagEntry currentEntry;
	
	public MissingParamTagData(final MissingParamTagEntry currentEntry, final List<MissingParamTagEntry> allEntries) {
		this.currentEntry = currentEntry;
		missingParams = allEntries;
	}
}
