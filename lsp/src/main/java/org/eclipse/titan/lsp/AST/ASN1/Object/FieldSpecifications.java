/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST.ASN1.Object;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.titan.lsp.AST.ASTNode;
import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.ICollection;
import org.eclipse.titan.lsp.AST.INamedNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.Identifier.Identifier_type;
import org.eclipse.titan.lsp.AST.Location;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;

/**
 * Class to represent FieldSpecs.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 */
public final class FieldSpecifications extends ASTNode implements ICollection<FieldSpecification> {
	public static final String MISSINGNAMEDFIELDSPECIFICATION = "No field specification with name `{0}''";
	public static final String MISSINGINDEXEDFIELDSPECIFICATION = "No field specification at index `{0}''";
	public static final String DUPLICATEDFIELDSPECIFICATIONFIRST = "Duplicate field specification with name `{0}'' was first declared here";
	public static final String DUPLICATEDFIELDSPECIFICATIONREPEATED = "Duplicate field specification with name `{0}'' was declared here again";
	public static final String DUPLICATED_UNIQUE_FIELD_SPECIFICATION_FIRST = "Duplicate identifier (UNIQUE) field specification with name `{0}'' was first declared here";
	public static final String DUPLICATED_UNIQUE_FIELD_SPECIFICATION_REPEATED = "Duplicate identifier (UNIQUE) field specification with name `{0}'' was declared here again";

	private Map<String, FieldSpecification> fieldSpecificationsMap;
	private final List<FieldSpecification> fieldSpecificationsList;
	private Erroneous_FieldSpecification fsError;
	private ObjectClass_Definition myObjectClass;
	/** caching the unique field spec., if any */
	private FieldSpecification uniqueFs;

	/** the time when these field specifications were checked the last time. */
	private CompilationTimeStamp lastTimeChecked;

	public FieldSpecifications() {
		this(false);
	}

	/** 
	 * Temp. solution as the ASN.1 parser does not work properly
	 * for Undefined_FieldSpecification and FixedTypeValue_FieldSpecification
	 */
	public FieldSpecifications(final boolean isEmpty) {
		fieldSpecificationsList = isEmpty ? Collections.emptyList() : new ArrayList<FieldSpecification>();
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		for (final FieldSpecification fieldSpecification : fieldSpecificationsList) {
			if (fieldSpecification == child) {
				return builder.append(INamedNode.DOT).append(fieldSpecification.getIdentifier().getDisplayName());
			}
		}

		return builder;
	}

	public void setMyObjectClass(final ObjectClass_Definition objectClassDefinition) {
		myObjectClass = objectClassDefinition;
		for (final FieldSpecification fieldSpecification : fieldSpecificationsList) {
			fieldSpecification.setMyObjectClass(objectClassDefinition);
		}
	}

	@Override
	public boolean add(final FieldSpecification fieldSpecification) {
		if (fieldSpecification != null && fieldSpecification.getIdentifier() != null
				&& fieldSpecification.getIdentifier().getLocation() != null) {

			fieldSpecification.setFullNameParent(this);
			if (myObjectClass != null) {
				fieldSpecification.setMyObjectClass(myObjectClass);
			}
			return fieldSpecificationsList.add(fieldSpecification);
		}
		return false;
	}

	@Override
	public boolean contains(final Object identifier) {
		if (lastTimeChecked == null) {
			check(CompilationTimeStamp.getBaseTimestamp());
		}
		if (identifier instanceof Identifier) {
			return fieldSpecificationsMap.containsKey(((Identifier)identifier).getName());
		}
		return false;
	}

	public FieldSpecification get(final Identifier identifier) {
		if (lastTimeChecked == null) {
			check(CompilationTimeStamp.getBaseTimestamp());
		}

		if (fieldSpecificationsMap.containsKey(identifier.getName())) {
			return fieldSpecificationsMap.get(identifier.getName());
		}

		identifier.getLocation().reportSemanticError(MessageFormat.format(MISSINGNAMEDFIELDSPECIFICATION, identifier.getDisplayName()));
		return getFieldSpecificationError();
	}

	@Override
	public FieldSpecification get(final int index) {
		if (index < fieldSpecificationsList.size() && index >= 0) {
			return fieldSpecificationsList.get(index);
		}
		myObjectClass.getLocation().reportSemanticError(MessageFormat.format(MISSINGINDEXEDFIELDSPECIFICATION, Integer.toString(index)));
		return null;
	}

	public FieldSpecification getFieldSpecificationError() {
		if (fsError == null) {
			fsError = new Erroneous_FieldSpecification(new Identifier(Identifier_type.ID_ASN, "<error>"), true, false);
		}
		return fsError;
	}

	@Override
	public int size() {
		return fieldSpecificationsList.size();
	}

	public void check(final CompilationTimeStamp timestamp) {
		if (isBuildCancelled()) {
			return;
		}

		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		if (fieldSpecificationsMap == null) {
			fieldSpecificationsMap = new HashMap<String, FieldSpecification>(fieldSpecificationsList.size());
		}

		lastTimeChecked = timestamp;

		fieldSpecificationsMap.clear();

		String hasUnique = null;

		for (final FieldSpecification fieldSpecification : fieldSpecificationsList) {
			final String name = fieldSpecification.getIdentifier().getName();
			if (fieldSpecificationsMap.containsKey(name)) {
				final String displayName = fieldSpecification.getIdentifier().getDisplayName();
				final Location oldLocation = fieldSpecificationsMap.get(name).getIdentifier().getLocation();
				oldLocation.reportSingularSemanticError(MessageFormat.format(DUPLICATEDFIELDSPECIFICATIONFIRST, displayName));
				final Location newLocation = fieldSpecification.getIdentifier().getLocation();
				newLocation.reportSemanticError(MessageFormat.format(DUPLICATEDFIELDSPECIFICATIONREPEATED, displayName));
			} else {
				fieldSpecificationsMap.put(name, fieldSpecification);
			}
			if (fieldSpecification.isUnique()) {
				if (hasUnique != null) {
					final Location oldLocation = fieldSpecificationsMap.get(hasUnique).getIdentifier().getLocation();
					oldLocation.reportSingularSemanticError(MessageFormat.format(DUPLICATED_UNIQUE_FIELD_SPECIFICATION_FIRST, hasUnique));
					final Location newLocation = fieldSpecification.getIdentifier().getLocation();
					newLocation.reportSemanticError(MessageFormat.format(DUPLICATED_UNIQUE_FIELD_SPECIFICATION_REPEATED, name));
				} else {
					hasUnique = name;
					uniqueFs = fieldSpecification;
				}
			}
		}

		for (final FieldSpecification fieldSpecification : fieldSpecificationsList) {
			fieldSpecification.check(timestamp);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		for (final FieldSpecification fs : fieldSpecificationsList) {
			if (!fs.accept(v)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns whether these field specifications have identifier field
	 * @return whether these field specifications have identifier field 
	 */
	public boolean hasUnique() {
		if (lastTimeChecked == null) {
			check(CompilationTimeStamp.getBaseTimestamp());
		}
		return uniqueFs != null;
	}

	/**
	 * Returns the identifier field of these field specifications,
	 * or {@code null} if it does not have any 
	 * @return the identifier field of these field specifications
	 */
	public FieldSpecification getUnique() {
		if (lastTimeChecked == null) {
			check(CompilationTimeStamp.getBaseTimestamp());
		}
		return uniqueFs;
	}

	@Override
	public Iterator<FieldSpecification> iterator() {
		return fieldSpecificationsList.iterator();
	}
}
