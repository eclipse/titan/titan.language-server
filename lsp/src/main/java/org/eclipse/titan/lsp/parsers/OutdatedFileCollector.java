/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.titan.lsp.GeneralConstants;
import org.eclipse.titan.lsp.core.Project;
import org.eclipse.titan.lsp.core.ProjectItem;

/**
 * This class is a resource visitor collecting every file in the project which is not:
 * <ul>
 * <li>under the working directory.
 * <li>excluded from build.
 * <li>uptodate.
 * </ul>
 * Should be started only on a project.
 *
 * @see #analyzeAll(IFile, TTCN3Editor, IProgressMonitor)
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public class OutdatedFileCollector {
	private final Project project;

	private final Map<Path, String> uptodateFiles;
	private final Set<Path> highlySyntaxErroneousFiles;
	private final List<Path> asn1FilesToCheck;
	private final List<Path> cfgFilesToCheck;
	private final List<Path> ttcn3FilesToCheck;
	private final List<Path> ttcninFilesModified;
	//private final Path[] workingDirectories;

	public OutdatedFileCollector(/*final Path[] workingDirectories,*/ final Map<Path, String> uptodateFiles,
			final Set<Path> highlySyntaxErroneousFiles) {
		this.uptodateFiles = uptodateFiles;
		this.highlySyntaxErroneousFiles = highlySyntaxErroneousFiles;
		this.asn1FilesToCheck = new ArrayList<>();
		this.cfgFilesToCheck = new ArrayList<>();
		this.ttcn3FilesToCheck = new ArrayList<>();
		this.ttcninFilesModified = new ArrayList<>();
		//this.workingDirectories = workingDirectories;

		project = Project.INSTANCE;
	}

	public List<Path> getASN1FilesToCheck() {
		return asn1FilesToCheck;
	}

	public List<Path> getCFGFilesToCheck() {
		return cfgFilesToCheck;
	}

	public List<Path> getTTCN3FilesToCheck() {
		return ttcn3FilesToCheck;
	}

	public List<Path> getTtcninFilesModified() {
		return ttcninFilesModified;
	}

	public void visitFile(Path file) {
		final Path filepath = file.getFileName();
		if (filepath == null) {
			return;
		}
		final String filename = filepath.toString();
		if (filename.startsWith(GeneralConstants.DOT)) {
			return;
		}
		final ProjectItem item = project.getProjectItem(file);
		if (item == null || item.isExcluded()) {
			return;
		}

		String extension = "";
		final String fullpath = file.toString();
		if (fullpath.contains(GeneralConstants.DOT)) {
		    extension = fullpath.substring(fullpath.lastIndexOf(GeneralConstants.DOT) + 1);
		}
		if (!uptodateFiles.containsKey(file) && !highlySyntaxErroneousFiles.contains(file)) {
			if (GlobalParser.isSupportedTTCN3Extension(extension)) {
				ttcn3FilesToCheck.add(file);
			} else if (GlobalParser.isSupportedConfigFileExtension(extension)) {
				cfgFilesToCheck.add(file);
			} else if (GlobalParser.isSupportedASN1Extension(extension)) {
				asn1FilesToCheck.add(file);
			} else if (GlobalParser.isSupportedTTCNINExtension(extension)) {
				ttcninFilesModified.add(file);
			}
		}

		return;
	}
}
