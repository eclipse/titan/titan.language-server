/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.codeAction;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.titan.lsp.common.logging.TitanLogger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

/**
 * Class for handle {@Diagnostic.data}
 *
 * @author Csilla Farkas
 * @author Miklos Magyari
 * 
 */
public class DiagnosticData {
	public enum CodeActionProviderType {
		CLASS,
		MISSINGFUNCTION,
		MISSINGRETURN,
		MISSINGPARAMTAG
	}

	public static DiagnosticData fromJson(JsonObject json) {
		try {
			return new Gson().fromJson(json, DiagnosticData.class);
		} catch(JsonSyntaxException e) {
			TitanLogger.logError(e);
			return new DiagnosticData();
		}
	}

	private String codeActionProviderType;
	private List<String> codeActionTypes;
	private String codeActionData;

	public DiagnosticData() {}

	public <E extends Enum<E>> DiagnosticData(CodeActionProviderType codeActionProviderType, List<E> codeActions) {
		this(codeActionProviderType, codeActions, null);
	}

	/** codeActionData must be a JSON encoded object. See <code>getCodeActionData</code> for details. */
	public <E extends Enum<E>> DiagnosticData(CodeActionProviderType codeActionProviderType, List<E> codeActions, String codeActionData) {
		this.codeActionProviderType = Objects.requireNonNull(codeActionProviderType.name());
		this.codeActionTypes = Objects.requireNonNull(codeActions.stream().map(E::name).collect(Collectors.toList()));
		this.codeActionData = codeActionData;
	}

	public <E extends Enum<E>> Optional<List<E>> getCodeActionTypes(Class<E> enumClass) {
		if (codeActionTypes != null) {
			try {
				final List<E> actionTypes = codeActionTypes.stream().map(action -> Enum.valueOf(enumClass, action)).collect(Collectors.toList());
				return Optional.of(actionTypes);
			} catch (IllegalArgumentException e) {
				TitanLogger.logError(e);
				return Optional.empty();
			}
		}
		return Optional.empty();
	}

	public Optional<CodeActionProviderType> getCodeActionProviderType() {
		if (codeActionProviderType != null) {
			try {
				return Optional.of(CodeActionProviderType.valueOf(codeActionProviderType));
			} catch (IllegalArgumentException e) {
				TitanLogger.logError(e);
				return Optional.empty();
			}
		}

		return Optional.empty();
	}
	
	/**
	 * Returns the JSON encoded optional data for the Diagnostic. 
	 * Data should be a primitive data type, a class with primitive type
	 * members or with subclasses containing primitive types only.<br>
	 * <br>
	 * If class members are non-primitive types, a <code>TypeAdapter</code> must be used.
	 * 
	 * @return
	 */
	public Optional<Object> getCodeActionData() {
		return codeActionData != null ? Optional.of(codeActionData) : Optional.empty();
	}
}
