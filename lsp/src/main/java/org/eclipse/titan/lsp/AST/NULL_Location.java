/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.AST;

import java.io.File;

import org.eclipse.titan.lsp.common.logging.TitanLogger;
import org.eclipse.titan.lsp.core.Position;
import org.eclipse.titan.lsp.core.Range;

/**
 * The NULL_Location class represents a location which was not set, or was set
 * incorrectly.
 * <p>
 * This class is used to handle cases when the location of an object can not be
 * determined, but is referred to frequently. For example the location of an
 * identifier might not be determinable if there are syntactic errors near by.
 * <p>
 * Please note that because of the purpose of this class, none of its methods
 * does any real work.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class NULL_Location extends Location {
	/** The only instance. */
	public static final NULL_Location INSTANCE = new NULL_Location();

	private NULL_Location() {
		// Do nothing
	}

	@Override
	public int getEndColumn() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getEndColumn();
	}

	@Override
	public int getEndLine() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getEndLine();
	}

	@Override
	public Position getEndPosition() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getEndPosition();
	}

	@Override
	public org.eclipse.lsp4j.Location getLspLocation() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getLspLocation();
	}

	@Override
	public Range getRange() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getRange();
	}

	@Override
	public int getStartColumn() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getStartColumn();
	}

	@Override
	public int getStartLine() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getStartLine();
	}

	@Override
	public Position getStartPosition() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getStartPosition();
	}

	@Override
	public void setEndColumn(int column) {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
	}

	@Override
	public void setEndLine(int line) {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
	}

	@Override
	public void setStartColumn(int column) {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
	}

	@Override
	public void setStartLine(int line) {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
	}

	@Override
	public void setStartPosition(Position position) {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
	}

	@Override
	public File getFile() {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
		return super.getFile();
	}

	@Override
	public void setFile(File file) {
		TitanLogger.logMixedDebugAndTrace(new UnsupportedOperationException());
	}

	@Override
	/** {@inheritDoc} */
	public boolean containsPosition(Position position) {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void reportSemanticError(final String reason) {
		TitanLogger.logFatal("The following semantic error was reported on a non-existent location: " + reason);
	}

	@Override
	/** {@inheritDoc} */
	public void reportSemanticWarning(final String reason) {
		TitanLogger.logFatal("The following semantic warning was reported on a non-existent location: " + reason);
	}

	@Override
	/** {@inheritDoc} */
	public void reportSyntacticError(final String reason) {
		TitanLogger.logFatal("The following syntactic error was reported on a non-existent location: " + reason);
	}

	@Override
	/** {@inheritDoc} */
	public void reportSyntacticWarning(final String reason) {
		TitanLogger.logFatal("The following syntactic warning was reported on a non-existent location: " + reason);
	}

	/**
	 * Returns the only instance of this singleton NULL_Location
	 * @return the only instance of this singleton NULL_Location
	 */
	public static NULL_Location getInstance() {
		return NULL_Location.INSTANCE;
	}

	@Override
	public String toString() {
		return "NULL_Location";
	}
}
