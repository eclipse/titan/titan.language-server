/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.Arrays;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Value;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.ControlPart;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Altstep;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Const;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Testcase;
import org.eclipse.titan.lsp.AST.TTCN3.values.Bitstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Charstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Hexstring_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.Octetstring_Value;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class MagicString extends BaseModuleCodeSmellSpotter {
	private static final String MAGIC_STRING = "The magic string `{0}'' should be extracted into a local constant";

	public MagicString() {
		super(CodeSmellType.MAGIC_STRINGS);
		addStartNodes(Arrays.asList(Def_Function.class, Def_Testcase.class, Def_Altstep.class, ControlPart.class));
	}

	@Override
	public void process(final IVisitableNode n, final Problems problems) {
		n.accept(new ASTVisitor() {
			@Override
			public int visit(final IVisitableNode node) {
				if (node instanceof Bitstring_Value || node instanceof Charstring_Value || node instanceof Hexstring_Value
						|| node instanceof Octetstring_Value) {
					final String msg = MessageFormat.format(MAGIC_STRING, ((Value) node).createStringRepresentation());
					problems.report(((Value) node).getLocation(), msg);
				} else if (node instanceof Def_Const) {
					// should not reach there
					return V_SKIP;
				}
				return V_CONTINUE;
			}
		});
	}
}
