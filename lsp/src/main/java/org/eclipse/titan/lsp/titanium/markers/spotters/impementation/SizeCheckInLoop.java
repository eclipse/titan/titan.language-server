/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.util.Arrays;

import org.eclipse.titan.lsp.AST.ASTVisitor;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.TTCN3.statements.DoWhile_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.For_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.statements.While_Statement;
import org.eclipse.titan.lsp.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.LengthofExpression;
import org.eclipse.titan.lsp.AST.TTCN3.values.expressions.SizeOfExpression;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class SizeCheckInLoop extends BaseModuleCodeSmellSpotter {
	private static final String ERROR_MESSAGE = "Length check operation in loop condition";

	public SizeCheckInLoop() {
		super(CodeSmellType.SIZECHECK_IN_LOOP);
		addStartNodes(Arrays.asList(For_Statement.class, While_Statement.class, DoWhile_Statement.class));
	}

	@Override
	protected void process(final IVisitableNode node, final Problems problems) {
		if (node instanceof For_Statement) {
			final For_Statement s = (For_Statement)node;
			s.getFinalExpression().accept(new LoopVisitor(problems));
		} else if (node instanceof While_Statement) {
			final While_Statement s = (While_Statement)node;
			s.getExpression().accept(new LoopVisitor(problems));
		} else if (node instanceof DoWhile_Statement) {
			final DoWhile_Statement s = (DoWhile_Statement)node;
			s.getExpression().accept(new LoopVisitor(problems));
		} else {
			return;
		}
	}

	protected static class LoopVisitor extends ASTVisitor {
		private final Problems problems;

		public LoopVisitor(final Problems problems) {
			this.problems = problems;
		}

		@Override
		public int visit(final IVisitableNode node) {
			if (node instanceof LengthofExpression) {
				final LengthofExpression temp = (LengthofExpression) node;
				problems.report(temp.getLocation(), ERROR_MESSAGE);
			} else if (node instanceof SizeOfExpression) {
				final SizeOfExpression temp = (SizeOfExpression) node;
				problems.report(temp.getLocation(), ERROR_MESSAGE);
			} else if (node instanceof Expression_Value) {
				return V_CONTINUE;
			}
			return V_SKIP;
		}
	}
}
