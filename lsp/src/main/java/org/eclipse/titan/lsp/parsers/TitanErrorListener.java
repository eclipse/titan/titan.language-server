/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.parsers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public final class TitanErrorListener extends BaseErrorListener {
	protected List<SyntacticErrorStorage> errorsStored;
	protected File file;

	public TitanErrorListener(final File file) {
		this.errorsStored = new ArrayList<SyntacticErrorStorage>();
		this.file = file;
	}

	public TitanErrorListener(final File file, final List<SyntacticErrorStorage> storage) {
		this.errorsStored = storage;
		this.file = file;
	}

	public void reset() {
		errorsStored.clear();
	}

	@Override
	public void syntaxError(final Recognizer<?, ?> recognizer, final Object offendingSymbol, final int line, final int charPositionInLine,
			final String msg, final RecognitionException e) {
		SyntacticErrorStorage errorStorage;
		if (offendingSymbol instanceof CommonToken) {
			final CommonToken token = (CommonToken) offendingSymbol;
			final int tokenlen = token.getText() == null ? 0 : token.getText().length();
			errorStorage = new SyntacticErrorStorage(file, line, token.getCharPositionInLine(), token.getCharPositionInLine() + tokenlen, msg);
		} else {
			errorStorage = new SyntacticErrorStorage(file, line, charPositionInLine, charPositionInLine + 1, msg);
		}

		errorsStored.add(errorStorage);
	}

	public List<SyntacticErrorStorage> getErrorsStored() {
		return errorsStored;
	}

	public boolean addAll(final List<SyntacticErrorStorage> errorStore) {
		return errorsStored.addAll(errorStore);
	}
}