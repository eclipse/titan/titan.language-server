/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.titanium.markers.spotters.impementation;

import java.text.MessageFormat;
import java.util.Arrays;

import org.eclipse.titan.lsp.AST.IType;
import org.eclipse.titan.lsp.AST.IVisitableNode;
import org.eclipse.titan.lsp.AST.Identifier;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Const;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_ExternalConst;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Extfunction;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_ModulePar;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Template;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Def_Var_Template;
import org.eclipse.titan.lsp.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.lsp.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.lsp.parsers.CompilationTimeStamp;
import org.eclipse.titan.lsp.titanium.markers.spotters.BaseModuleCodeSmellSpotter;
import org.eclipse.titan.lsp.titanium.markers.types.CodeSmellType;

public class TypenameInDef extends BaseModuleCodeSmellSpotter {
	private static final String REPORT = "The name `{1}'' of the {0} contains it''s type''s name `{2}''";

	protected final CompilationTimeStamp timestamp;

	public TypenameInDef() {
		super(CodeSmellType.TYPENAME_IN_DEFINITION);
		timestamp = CompilationTimeStamp.getBaseTimestamp();
		
		addStartNodes(Arrays.asList(Def_Const.class, Def_ExternalConst.class, Def_Extfunction.class, Def_Function.class,
			Def_ModulePar.class, Def_Template.class, Def_Var_Template.class, Def_Var.class));
	}

	@Override
	protected void process(final IVisitableNode node, final Problems problems) {
		if (!(node instanceof Def_Const) &&
				!(node instanceof Def_ExternalConst) &&
				!(node instanceof Def_Extfunction) &&
				!(node instanceof Def_Function) &&
				!(node instanceof Def_ModulePar) &&
				!(node instanceof Def_Template) &&
				!(node instanceof Def_Var_Template) &&
				!(node instanceof Def_Var)) {
			return;
		}
		final Definition s = (Definition)node;
		check(s.getIdentifier(), s.getType(timestamp), s.getDescription(), problems);
	}

	private void check(final Identifier identifier, final IType type, final String description, final Problems problems) {
		if (type == null) {
			return;
		}

		final String displayName = identifier.getDisplayName();
		Identifier typeId = null;
		if (type instanceof Referenced_Type) {
			final Referenced_Type referencedType = (Referenced_Type) type;
			typeId = referencedType.getReference().getId();
		}

		String typeName;
		if (typeId == null) {
			typeName = type.getTypename();
		} else {
			typeName = typeId.getDisplayName();
		}

		if (displayName.contains(typeName)) {
			final String msg = MessageFormat.format(REPORT, description, displayName, typeName);
			problems.report(identifier.getLocation(), msg);
		}
	}
}
