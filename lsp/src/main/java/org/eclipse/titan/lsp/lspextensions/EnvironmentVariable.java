/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.lspextensions;

import org.eclipse.lsp4j.generator.JsonRpcData;

/**
 * @author Miklos Magyari
 */
@JsonRpcData
public class EnvironmentVariable {
	private String name;
	private String value;
	
	public EnvironmentVariable(final String name, final String value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}
}
