# Titan Language Server

Thanks for your interest in this project.

## Project description
This language server provides TTCN-3 and ASN.1 language support using [LSP4j](https://projects.eclipse.org/projects/technology.lsp4j).
The project is in experimental phase.

[More about Titan](https://projects.eclipse.org/projects/tools.titan)

## Features
The list of supported and planned features are available [here](https://gitlab.eclipse.org/eclipse/titan/titan.vs-code-extension#features).

## Related projects
Projects / IDEs / editors that uses this language server:

:heavy_check_mark: [Visual Studio Code](https://gitlab.eclipse.org/eclipse/titan/titan.vs-code-extension)\
:heavy_check_mark: [Eclipse Theia](https://gitlab.eclipse.org/eclipse/titan/titan.vs-code-extension)\
:x: [ntt](https://nokia.github.io/ntt/)\
:x: [Eclipse IDE](https://github.com/eclipse/lsp4e)\
:x: [vim](https://github.com/prabirshrestha/vim-lsp)\
:x: [emacs](https://github.com/emacs-lsp/lsp-mode)

## Minimum requirements
This project requires Java 11.

## Building the code from source
Follow this high level instructions to build the language server from source
1. Install [Maven](https://maven.apache.org/index.html)\
    `apt-get install -y mvn`

2. Clone this repository\
    `git clone https://gitlab.eclipse.org/eclipse/titan/titan.language-server.git`

4. Build and package the language server using Maven\
    `cd titan.language-server/lsp`\
    `mvn package`

At the end you will have `titan.language-server/lsp/target/org.eclipse.titan.lsp.jar` Java archive file. The Java dependencies of the language server are collected in `titan.language-server/lsp/target/lib` during the build.

## Integration options
stdio and socket (both in client and server modes) are supported as communication channels:

- stdio is the default option and requires no argument:\
    `java -jar org.eclipse.titan.lsp.jar`

- Socket in server mode requires the port number:\
    `java -jar org.eclipse.titan.lsp.jar --socket 55555`

- Socket in client mode requires the host name or IP address and the port number:\
    `java -jar org.eclipse.titan.lsp.jar --socket localhost:55555`